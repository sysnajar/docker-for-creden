FROM ubuntu:18.04

# Docker Build Arguments
ENV RESTY_VERSION="1.15.8.2" \
    RESTY_LUAROCKS_VERSION="3.2.1" \
    PDFTK_VERSION="2.02"

RUN apt-get update && \
    apt-get install -y libpcre3-dev perl make cmake build-essential poppler-utils curl \
        wget lua5.1 lua5.1-dev zlib1g-dev zlib1g unzip imagemagick vim libbarcode-zbar-perl \
        redis-tools software-properties-common python3-pip git zip
RUN alias python='python3'
RUN ln -s /usr/bin/python3 /usr/bin/python
RUN apt-get install -y libssl1.0-dev


#java
RUN apt-get install -y openjdk-11-jdk
COPY target.tar.gz /home/creden/work/app/PDFTimestamping/target.tar.gz
RUN tar -xvzf /home/creden/work/app/PDFTimestamping/target.tar.gz
#todo check chmod 755
RUN chmod -R 777 /home/creden/work/app/PDFTimestamping/

#cert
COPY cert.tar.gz /home/creden/work/app/cert.tar.gz
RUN tar -xvzf /home/creden/work/app/cert.tar.gz

#todo check chmod 755
RUN mkdir -p /home/creden/work/app/cert
RUN chmod -R 777 /home/creden/work/app/cert



COPY tmp/*.ttf /home/creden/work/app/tmp/


RUN apt-get install -y mongodb-clients redis-tools 

RUN curl -sL https://deb.nodesource.com/setup_14.x | bash -
RUN apt -y install nodejs

# install luarock
RUN cd /tmp && \
    wget https://luarocks.org/releases/luarocks-${RESTY_LUAROCKS_VERSION}.tar.gz && \
    tar zxpf luarocks-${RESTY_LUAROCKS_VERSION}.tar.gz && \
    cd luarocks-${RESTY_LUAROCKS_VERSION} && \
    ./configure; make install

# install openresty
RUN PATH=/usr/local/openresty/bin:$PATH
RUN cd /tmp && \
    wget https://openresty.org/download/openresty-${RESTY_VERSION}.tar.gz && \
    tar -zxvf openresty-${RESTY_VERSION}.tar.gz && \
    cd openresty-${RESTY_VERSION} && \
    ./configure -j2 && \
    make -j2 && \
    make install
    
#mongo c driver for luarock lua-mongo
RUN cd /tmp && \
    git clone https://github.com/mongodb/mongo-c-driver.git && \
    cd mongo-c-driver && \
    python build/calc_release_version.py > VERSION_CURRENT && \
    mkdir cmake-build && \
    cd cmake-build && \
    cmake -DENABLE_AUTOMATIC_INIT_AND_CLEANUP=OFF .. && \
    make && \
    make install

RUN rm -rf /tmp/*
RUN apt-get clean

RUN git config --global url."https://github.com/".insteadOf git://github.com/

RUN luarocks install luasocket && \
    luarocks install redis-lua && \
    luarocks install luacrypto && \
    luarocks install resty-mongol && \
    luarocks install md5 && \
    luarocks install lua-cjson && \
    luarocks install lua-resty-exec && \
    luarocks install lua-resty-session && \
    luarocks install luabitop && \
    luarocks install copas && \
    luarocks install lua-websockets && \
    luarocks install luafilesystem && \
    luarocks install luasec && \
    luarocks install luasocket && \
    luarocks install netstring && \
    luarocks install date && \
    luarocks install lua-resty-cookie && \
    luarocks install htmlparser && \
    luarocks install luautf8 && \
    luarocks install utf8 && \
    luarocks install lua-resty-validation && \
    luarocks install lua-mongo 
    luarocks install lua-pdf

COPY app/server/docker/openresty /home/creden/work/app/server/docker/openresty
COPY app/server/docker/openresty/logs /tmp/logs

#install libs
RUN apt-get update && apt-get install -y ack && \
    apt-get install -y lsof && \
    apt-get install libpoppler-dev


# client , server and conf dir
COPY tmp/lua-scrypt/scrypt.lua /usr/local/share/lua/5.1/resty/
COPY app/client/signature_bla/ /home/creden/work/app/client/signature_bla
COPY app/client/signature_bla/ /home/creden/work/app/client/signature_bla_git
COPY app/server/capi_bla/ /home/creden/work/app/server/capi
COPY app/server/capi_bla/ /home/creden/work/app/server/capi_git
COPY app/client/signature_bla/pj/ /home/creden/work/app/client/pj
COPY app/client/signature_bla/pj/ /home/creden/work/app/client/pj_git
RUN  cd /home/creden/work/app/client/pj_git/eKYC-dipchip && \
     tar -xvzf ekyc.tar.gz

RUN chmod -R 777 /home/creden/work/app/client/pj
RUN chmod -R 777 /home/creden/work/app/client/signature_bla
RUN chmod -R 777 /home/creden/work/app/server/capi
RUN chmod -R 777 /home/creden/work/app/client/pj_git
RUN chmod -R 777 /home/creden/work/app/client/signature_bla_git
RUN chmod -R 777 /home/creden/work/app/server/capi_git

COPY app/server/docker/openresty/conf/ /tmp/conf
COPY tmp/sarabun-webfont-master/ /home/creden/work/app/client/face/images/card/sarabun-webfont-master

#COPY /tmp/cpdf /tmp
COPY /tmp /tmp
#COPY tmp/setting.sh /tmp
RUN chmod +x /tmp/setting.sh
RUN /tmp/setting.sh
RUN chmod +x /tmp/pdftk_setup/install_pdftk.sh
RUN /tmp/pdftk_setup/install_pdftk.sh

#old java

#install restry libs
COPY tmp/lua-scrypt/lua_lib.tar.gz /usr/local/share/
RUN cd /usr/local/share && \
    tar -xvzf lua_lib.tar.gz
COPY tmp/lua-scrypt/scrypt.lua /usr/local/share/lua/5.1/resty/
COPY tmp/lua-scrypt/scrypt.so /usr/local/share/lua/5.1/resty/
COPY tmp/session.lua /usr/local/share/lua/5.1/resty/
COPY app/server/capi_bla/common.lua /usr/local/share/lua/5.1/
COPY tmp/sockexec /usr/bin/
RUN chmod 777 /usr/bin/sockexec
COPY tmp/sock.sh /tmp/
COPY tmp/run_proxy.sh /tmp/
#RUN chmod +x /tmp/sock.sh

#RUN chmod +x /usr/bin/sockexec

#copy persistence dir  
#COPY tmp/lua-scrypt /tmp/lua-scrypt/

run mv /tmp/edoc /home/creden/work/app/client/
#run  mkdir -p /home/creden/work/app/client/face/images/card2
#run chmod -R 777 /home/creden/work/app/client/face/images/card2
run chmod -R 777 /home/creden/work/app/client/edoc

RUN chmod +x /home/creden/work/app/server/capi/*.lua
RUN chmod +x /home/creden/work/app/server/capi/*.sh
RUN chmod +x /home/creden/work/app/server/capi/*.js
 

RUN mv /etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/policy.xml.off


EXPOSE 8080:8080
RUN chmod -R 777 /tmp

CMD ["/usr/local/openresty/bin/openresty", "-p", "/tmp",  "-c",  "conf/nginx.conf",  "-g", "daemon off;"]
