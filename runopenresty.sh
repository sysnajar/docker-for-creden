#!/bin/bash
sleep 3;

rm -rf /usr/local/share/lua/5.1/resty/common.lua;
echo 'Starting OpenResty'
/usr/local/openresty/bin/openresty -p /tmp -c conf/nginx.conf &
echo 'Done'
sleep 3;
tail -f /tmp/error.log
