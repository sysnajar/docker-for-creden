curl -v -X POST https://api.line.me/v2/bot/message/push \
-H 'Content-Type:application/json' \
-H 'Authorization: Bearer QTKVkhnoiSzGzTpr8tlPp2NI1NX+jDS2srLD6rYpfRrk0pWMllCY49dmtNAJp3nvEIf9OXkb+WJAxTl07voNdqPXZwg47D3kgY+xkxMPL67hVn9rYviCwUqL2S9yw1Vo8rP7vigurZjvXeBWi2OrwwdB04t89/1O/w1cDnyilFU=' \
-d '{
  "to": "'$1'",
  "messages": [
    {      
      "type": "flex",
      "altText": "create chatbot?",
      "contents": {
        "type": "bubble",
        "body": {
          "type": "box",
          "layout": "vertical",
          "spacing": "md",
          "contents": [
            {
              "type": "box",
              "layout": "vertical",
              "contents": [
                {
                  "type": "text",
                  "text": "สามารถใช้งานได้",
                  "align": "center",
                  "size": "md",
                  "weight": "bold"
                },
                {
                  "type": "text",
                  "text": "'$2'",
                  "wrap": true,
                  "weight": "bold",
                  "margin": "lg"
                }
              ]
            },
            {
              "type": "separator"
            },
            {
              "type": "box",
              "layout": "vertical",
              "margin": "lg",
              "contents": [
                {
                  "type": "box",
                  "layout": "baseline",
                  "contents": [
                    {
                      "type": "text",
                      "text": "user:",
                      "flex": 3,
                      "size": "lg",
                      "weight": "bold",
                      "color": "#666666"
                    },
                    {
                      "type": "text",
                      "text": "'$3'",
                      "wrap": true,
                      "flex": 8
                    }
                   
                  ]
                }  
              ]
            },{
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "pass:",
                  "flex": 3,
                  "size": "lg",
                  "weight": "bold",
                  "color": "#666666"
                },
                {
                  "type": "text",
                  "text": "'$4'",
                  "wrap": true,
                  "flex": 8
                }
              ]
            },{
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "key:",
                  "flex": 3,
                  "size": "lg",
                  "weight": "bold",
                  "color": "#666666"
                },
                {
                  "type": "text",
                  "text": "'$5'",
                  "wrap": true,
                  "flex": 8
                }
              ]
            }
          ]
        }
      }
    }
  ]
}'
