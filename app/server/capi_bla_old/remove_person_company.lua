#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
-- bson = require "resty-mongol.bson"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else    
    db = conn:new_db_handle("dopa")
    col = db:get_col("company_members")
    col2 = db:get_col("esig_user")

    person = { _id = object_id.new(convertHexStringToNormal(body._id)) or "N/A" }
    p, err = col:find_one(person)

    if p then
        if p.role == "USER" and tostring(p.company_id) == body.company_id then
            i, err = col:delete({_id = object_id.new(convertHexStringToNormal(body._id))},1,0)
            local update_cmd = {["$unset"] = { 
                company_id = ""
            }}
            n,err = col2:update({ adminEmail = body.email or "N/A" }, update_cmd, 0, 0, true)

            if i and n then
                ret = {success = true, data = "Remove user in company success"}
            else
                ret = {success = false , data = "Not remove user in company"}
            end
        end

    else
        ret = { success = false, error_msg = "No user in company"}
    end
end
print(cjson.encode(ret))
