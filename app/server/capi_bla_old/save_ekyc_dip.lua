#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
res = {success = false}
rc2 = redis.connect('172.31.7.255', 6379)
rc2:auth('noRedisPass1010')
HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    apiKey = ngx.req.get_headers().apiKey
else
   body = {code = 'esig'} 
end
ngx.log(ngx.NOTICE, '====================='..body.code)
ngx.log(ngx.NOTICE, '====================='..ngx.req.get_body_data())

conn = mongo()
conn:set_timeout(1000)
local ok_conn,  err_conn = conn:connect('127.0.0.1',27017)
if err_conn then
    local ret = {success = false, error_msg = err_conn}
    print(cjson.encode(ret))
    ngx.exit(ngx.HTTP_OK)
end

function chk_tx(usr)
        local ret = {success = false, error_msg = 'Your credit is not enough'}
        local counter_key = 'counter.'..usr..'.tx'
        local max_counter = 'counter.'..usr..'.txMax'
        c = rc:get(counter_key)  
        m = rc:get(max_counter)
        if not c and not m then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            if tonumber(c) >= tonumber(m) then
                print(cjson.encode(ret))    
                ngx.exit(ngx.HTTP_OK)
            else
                rc:incr(counter_key)      
            end
        end
end

local db = conn:new_db_handle("dopa")
if body.username ~= 'creden' then chk_tx(body.username) end
body.createDate = os.date("%x")
body.createTime = os.date("%X")
body.createdDtm = bson.get_utc_date(ngx.now() * 1000)
body.compCode = body.username

ngx.log(ngx.NOTICE, 'kyc results:' .. tostring(body.ekyc_level) ..', process_status :' .. tostring(body.process_status))
if body.ekyc_level and tostring(body.ekyc_level) == '2.2' then
    body.process_status = 'finished'
elseif body.ekyc_level and tostring(body.ekyc_level) == '2.3' then
    body.isValid = false
    body.process_status = 'processing'
end
local col = db:get_col("ekyc")
n, err_n = col:insert({body})
if not n then
    res.error_msg = err_n
else
    res.success = true
    res.txid = body.txid
    res.usr = body.username
end

if tostring(body.isValidDopa) == 'true' and tostring(body.isFaceIdentical) == 'true' then
    --move vdo job to queue 
    local json = rc2:get('vdo.queue.' .. body.txid)
    ngx.log(ngx.NOTICE, json)
    if json then 
        rc2:rpush('vdo.queue', json) 
    else
        ngx.log(ngx.NOTICE, 'error vdo data nil')
    end

    
	--check ghb_id
	if(body.card_reader_id and body.card_reader_id:sub(1,3)=='ghb')then
		local id_only = body.card_reader_id:gsub("ghb","")
		local link_key = 'ghb_id' .. id_only
        ngx.log(ngx.NOTICE, 'Looking for saved ghb vdo#' .. link_key)

        local rc2 = redis.connect('172.31.7.255', 6379)
        rc2:auth('noRedisPass1010')

        body.ghb_id = rc2:get(link_key)
        ngx.log(ngx.NOTICE, 'oldtxid = ' .. tostring(body.ghb_id))
	end


    --move vdo job to queue (ghb)
	if(body.ghb_id and body.ghb_id~='')then
		--todo1 face match
        --[[local selfie_file = 'selfie_'..body.ghb_id
        local face_match_cmd = "lua /home/creden/work/app/server/capi/face_match2.lua '"..selfie_file.."' '"..body.ghb_id.."' '"..body.txid.."' '"..body.username.."' &"
        ngx.log(ngx.NOTICE, 'face_match_cmd: '..face_match_cmd)
        os.execute(face_match_cmd)]]

		local json1 = rc2:get('vdo.queue.' .. body.ghb_id)
		ngx.log(ngx.NOTICE, 'getting info from vdo.queue.' .. body.ghb_id , json1)
		if json1 then 
			local obj = cjson.decode(json1)
			--todo2 convert txid from  ghb_id to txid
			obj.txid = body.txid
			obj.ghb_id = body.ghb_id
			local json2 = cjson.encode(obj)
            ngx.log(ngx.NOTICE, 'converting vdo req from ' .. body.ghb_id ..' to ' .. body.txid)
			rc2:rpush('vdo.queue', json2) 


			--check tensorflow
			local tkey = body.ghb_id .. ".tensorflow.result"
		    ngx.log(ngx.NOTICE, 'getting tensorflow result from [' .. tkey ..']')
			local tval = rc:get(tkey)
		    ngx.log(ngx.NOTICE, tkey .. ' = '..tostring(tval))



            --todo3 check audio
            --[[local audio_cmd = "lua /home/creden/work/app/server/capi/submit_vdo_ghb.lua '"..json2.."' &"
            ngx.log(ngx.NOTICE, 'audio_cmd: '..audio_cmd)
            os.execute(audio_cmd)
			]]

		else
			ngx.log(ngx.NOTICE, 'error vdo data nil')
		end
	end
    -- end ghb

else
    -- line noti
    ngx.log(ngx.NOTICE, 'kyc valid = false')
    ngx.log(ngx.NOTICE, 'txid = ' .. body.txid)
    local sdk_tx  = db:get_col("sdk_code"):find_one({txid = body.txid})
    local ekyc_tx = db:get_col("ekyc"):find_one({txid = body.txid})
    if (sdk_tx) then
        os.execute('/home/creden/work/app/server/capi/linekyc.sh "Failed liveness ekyc attempt:' .. sdk_tx.ref_no ..'" ' .. tostring(ekyc_tx._id) .. ' ' .. HOST_NAME ..' &' )
    end
end

print(cjson.encode(res))
   
