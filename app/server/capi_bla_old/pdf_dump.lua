#!/usr/bin/env lua
local pdf_file  = arg[1] 
local redis_key = 'pdf_dump:' .. pdf_file

os.execute('pdftk "' .. pdf_file .. '" dump_data | /home/creden/scripts/filter_pdf_dump.lua | /home/creden/scripts/rset.lua '.. redis_key)
--os.execute('redis-cli expire "' .. redis_key ..'" 10')
