#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
-- local line_esign = require "line_esign_noti"
-- bson = require "resty-mongol.bson"
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { id = arg[1] , msg_cancel_doc = arg[2] }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function send_line_noti(type, params)
    local res_noti, user_id = line_esign.get_line_user_id(params.to)
    if res_noti then
        local document_link = os.getenv('HOST_NAME') .. "/bla/#/document?id=" .. params.id
        params.url = document_link
        local message = line_esign.message(type, params)
        line_esign.push(user_id, message)
    end
end

function update_viewed_cancel(doc) 
	if doc.status ~= 0 then return false end

	-- local time = bson.get_utc_date(os.time() * 1000)

	for i, v in ipairs(doc.signers) do

		if v.is_group == true then

			for i2, v2 in ipairs(v.user_group) do 
				-- if not v2.is_viewed and v2.email == body.email then
					v2.is_viewed = false
					v2.is_viewed_date = nil
				-- end
			end

		else
			-- if not v.is_viewed and v.email == body.email then
				v.is_viewed = false
				v.is_viewed_date = nil
			-- end
		end

	end

	u, err = col:update({ id = doc.id }, {["$set"] = { signers = doc.signers }}, 0, 0, true)
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else    
    local session = require "resty.session".open({secret = "4321"})

    if body.owner == session.data.username then

        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
    
        doc = { id = body.id, owner = body.owner, status = 0 }
        d,err = col:find_one(doc)
        if d then
            update_viewed_cancel(d)
            d.status = 3
            d.msg_cancel_doc = body.msg_cancel_doc
            local str_id = tostring(d._id)
            local id = {_id = object_id.new(convertHexStringToNormal(str_id))}
            update = {["$set"] = {status = d.status , msg_cancel_doc = d.msg_cancel_doc}}
            document, err = col:update(id, update, 0, 0, true)
            if document then
                ret = {error_msg = true, data = "cancle success"}

                for i, v in pairs(d.signers) do
                    if v.email ~= d.owner then
                        prog("node", "/home/creden/work/app/server/capi/cancel_doc_mail_bla.js", v.email, d.subject, d.id, d.owner, body.msg_cancel_doc, d.name, d.msg, d.secret_level or "")
                        ngx.log(ngx.NOTICE, "send_mail_to_" .. tostring(v.email) )
                        local line_noti_params = {subject = d.subject, originalFile = d.originalFile, remark = d.msg_cancel_doc, id = d.id, to = v.email}
                        -- send_line_noti('cancel', line_noti_params)
                    end
                end

                -- sent noti to owner
                prog("node", "/home/creden/work/app/server/capi/cancel_doc_mail_bla.js", d.owner, d.subject, d.id, d.owner, body.msg_cancel_doc, d.name, d.msg, d.secret_level or "")
                local line_noti_params = {subject = d.subject, originalFile = d.originalFile, remark = d.msg_cancel_doc, id = d.id, to = d.owner}
               -- send_line_noti('cancel', line_noti_params)
            else                
                ret = {error_msg = false , data = "cancle unsuccess" , error_code='P02'}
            end
        else
            ret = {error_msg = "document not found"}
        end

    else
        ret = {error_msg = false , data = "cancle unsuccess", error_code='P02'}
    end
    
end

print(cjson.encode(ret))
