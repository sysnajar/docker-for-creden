#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
-- common = require "common_amway"
local bson = require "resty-mongol.bson"
local body = nil
local object_id = require "resty-mongol.object_id"
local conn = mongo()
conn:set_timeout(5000)
local ok,err = conn:connect('127.0.0.1',27017)
if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {status_creden=arg[1],id_deal=arg[2],status_deal=arg[3],status_symphony=arg[4],updated_dtm=arg[5]}
end


if err then
    ret = {success = false, error_msg = err}
else
    ret = {}
    local db_deal = conn:new_db_handle("symphony")
    local deal = db_deal:get_col("deal")

    ngx.log(ngx.NOTICE,body.status_deal)
    
    local update_cmd = {["$set"] = {
        doc_number=body.id_deal,
            status_creden=body.status_creden,
            status_deal =body.status_deal,
            status_symphony = body.status_symphony,
            updated_dtm = body.updated_dtm
        }}
    
    
    a, err = deal:update({doc_number=body.id_deal}, update_cmd, 1, 0, true)
    if not err then
        ret = {success = true, error_msg = "Update Success"}
    else
    ret = {success = false, error_msg = err}
    end

    print(cjson.encode(ret))
end




   
