#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local common = require "common"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local pdf_info = nil -- store pdf_dump infomation eg: see rc:get('pdf_dump:gridA4.pdf')

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {} 
end

header = ngx.req.get_headers()

-- sign fn
function getX(xp, width, f)
    local ret = 0
    --local width = 560--595
    local x2= (width*xp)/100

    return x2
    end

    function getY(yp, height, f)
    local sigHeight =  0

    yp = 100 - yp
    local y2 = ( (height-sigHeight) * yp)/100
    
    if( (not f.toolName) or (f.toolName=='Signature') )then
        local offset = 45
        if(f.scale and f.scale~=100)then
          offset = (f.scale*offset)/100
        end

        y2 = y2 - offset
    end

    return y2
    end

    function getYText(yp, height, f)
    local sigHeight =  0

    yp = 100 - yp
    local y2 = ( (height-sigHeight) * yp)/100
    
     y2 = y2 - 10

    return y2
    end
    
    function do_field(s, f)
        local is_sign  = (not f.toolName) or (f.toolName=='Signature')
        local is_text  = (f.toolName) and (f.toolName =='Name' or  f.toolName == 'Email' or f.toolName =='Tel' or f.toolName ==    'Text' or f.toolName =='Date')
        local is_chk   = (f.toolName) and (f.toolName =='Check')
        
        if(is_sign) then sign(s, f) end
        if(is_text) then do_text(s, f) end  
        if(is_chk)  then check(s, f) end
 
    end

    obj_txt = {}
    function do_text(s, f) 
    local pdfFile = s.pdfFile .. '.pdf'
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. pdfFile
    local page = tostring(f.page+1)

    local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
    local y2 = tostring(getYText(f.yp ,pdf_info.pages[tonumber(page)].height, f))

	--globlex specific case
	--if(s.is_globlex==true or s.subject=='globlex') then
    --    ngx.log(ngx.NOTICE,'changed x2,y2 from ' .. x2 ..',' .. y2)
	--	x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].height, f))
	--	y2 = tostring(getYText(f.yp ,pdf_info.pages[tonumber(page)].width, f))
    --    ngx.log(ngx.NOTICE,'changed x2,y2 to ' .. x2 ..',' .. y2)
	--end



    t = {page=page,x=x2,y=y2,text=f.toolData,size=11,font="/home/creden/work/app/tmp/Taviraj-Regular.ttf"}
    table.insert(obj_txt,t) 
    end
    -- end text fn

    function do_text2(s, f) 
    local pdfFile = s.pdfFile .. '.pdf'
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. pdfFile
    local page = tostring(f.page+1)

    local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
    local y2 = tostring(getYText(f.yp ,pdf_info.pages[tonumber(page)].height, f))

    local img = 'text_of_' .. tostring(f.id) .. '.png'
    img = dir .. img
 
      
 
    end
    -- end text fn

    
    function sign(s, f)
    local pdfFile = s.pdfFile .. '.pdf'
    local file2 = string.gsub(pdfFile , '%.pdf', '_stamped.pdf')
    local sig  = '/home/creden/work/app/client/edoc/app-assets/images/signature/'..f.email..'_'..f.type..'.png' 
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. pdfFile
    local pdfFile2  = dir .. file2
    local page = tostring(f.page+1)

    local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
    local y2 = tostring(getY(f.yp ,pdf_info.pages[tonumber(page)].height, f))

	--globlex specific case
	--if(s.is_globlex==true or s.subject=='globlex') then
    --   x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].height, f))
    --   y2 = tostring(getY(f.yp ,pdf_info.pages[tonumber(page)].width, f))
	--end

    if(f.scale and f.scale ~= 100)then
       local sig2  = sig .. '.scale.png'
       local scale_cmd = 'convert '.. sig ..' -resize '.. tostring(f.scale) ..'% ' .. sig2
       os.execute(scale_cmd)
       sig = sig2
    end 

   
    local cmd  = 'java -jar /home/creden/scripts/pdfstamp.jar -i '.. sig.. ' -l '.. x2 ..','.. y2 ..' -d 200 -o '.. dir ..' -p '.. page .. ' ' .. pdfFile
    
    local cmd2 = 'cp ' ..pdfFile2 .. ' '.. pdfFile
    
    os.execute(cmd)
    os.execute(cmd2)
    --ngx.log(ngx.NOTICE,'--------------------------PDF INFO IS ' .. cjson.encode(pdf_info))
    --ngx.log(ngx.NOTICE,'--------------------------'..cmd);ngx.log(ngx.NOTICE,'--------------------------'..cmd2)
    rc:set('last_sign', cmd)
    end
    -- end sign fn

    function check(s, f)
	ngx.log(ngx.INFO, 'CHECK', tostring(f.n))
    local sig  = '/home/creden/work/app/client/esig/img/check.png'  --checked image

    local pdfFile = s.pdfFile .. '.pdf'
    local file2 = string.gsub(pdfFile , '%.pdf', '_stamped.pdf')
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. pdfFile
    local pdfFile2  = dir .. file2
    local page = tostring(f.page+1)

    local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
    local y2 = tostring(getY(f.yp ,pdf_info.pages[tonumber(page)].height, f))


    if(f.scale and f.scale ~= 100)then
       local sig2  = sig .. '.scale.png'
       local scale_cmd = 'convert '.. sig ..' -resize '.. tostring(f.scale) ..'% ' .. sig2
       os.execute(scale_cmd)
       sig = sig2
    end 

   
    local cmd  = 'java -jar /home/creden/scripts/pdfstamp.jar -i '.. sig.. ' -l '.. x2 ..','.. y2 ..' -d 200 -o '.. dir ..' -p '.. page .. ' ' .. pdfFile
    local cmd2 = 'cp ' ..pdfFile2 .. ' '.. pdfFile
    
    os.execute(cmd)
    os.execute(cmd2)
    rc:set('last_check', cmd)
    rc:set('last_check2', cmd)
    end
    -- end chk fn ---------------------------------------------------------
	

            --update mongo
            conn = mongo()
            conn:set_timeout(60 *1000)
            ok, err = conn:connect('127.0.0.1',27017)
			is_gb = true
            if err then
                ret = {success = false, error_msg = err}
            else
                db = conn:new_db_handle("edoc")
                col = db:get_col("edoc_list")
                s, err = col:find_one({id=body.id})
                s._id = nil
                s._company = nil
				
				local notification_list = {}
				local noti_check = {}

                for i,v in ipairs(s.fields) do
                    ngx.log(ngx.INFO,i , ') ' , v.id)
                    for i2,v2 in ipairs(body.fields) do
                        ngx.log(ngx.INFO,'    ' , i2 , ') ' , tostring(v2.id) ..' VS '.. tostring(v.id))
                        if (v.id == v2.id) then 
							
                            v.status = 1 v.time = os.date("%x %X")
                            v.type = v2.type
                            v.toolData = v2.toolData
                            v.ip_field = header["x-real-ip"] 
                            --v.sign_now = true
                            ngx.log(ngx.INFO,' set signing type for ' .. v.id .. ' >> ' , v.type)
							local n = {doc_id = s.id,
							filename = s.originalFile, 
							dtm = ngx.now()*1000,
							signer = v.email,
							email  = s.owner,
						    id     = ngx.now()*1000,
							status = 0,
							description = 0
						    } 
							
							if(not noti_check[v.email])then --make sure we sent noti once
							   table.insert(notification_list, n)
							   noti_check[v.email] = true
                            end
                            common.activity_history(s.id ,v.email, 'Sign', 'เซ็นเอกสาร', v.email..' sign document '.. s.originalFile, v.email..' เซ็นเอกสาร '..s.originalFile, 'Sign', s.originalFile, nil, nil)
                        end
                    end
                end

				if(#notification_list>0)then
                   ncol = db:get_col("notification")
                   _i, err = ncol:insert(notification_list, nil, true)
 				end

                -- update fields information
                ngx.log(ngx.INFO,'REACHED $SET '.. body.id .. type(body.id))

                local update = {["$set"] = {fields=s.fields}}
                u, err = col:update({id=body.id}, update, 0, 0, true)
                -- end



                -- update current_order information 
				local new_signing_order = nil
				
				if(s.use_signing_order==true) then
                    local s2, err2 = col:find_one({id=body.id})
					local fields_2 = s2.fields
					local t1 = 0
					local t2 = 0

					for _i, v in ipairs(s2.fields) do
						 if(s.current_order==v.signOrder)then
							t1 = t1+1	
						 
							if(v.status==1)then
							   t2 = t2+1	
						    end

						 end
					end

                    ngx.log(ngx.NOTICE, "t1 = " .. tostring(t1) ..", t2 = " .. tostring(t2) )                   
				    if(t1>0 and t1==t2)then
					   new_signing_order = s.current_order+1	
                        ngx.log(ngx.NOTICE,"new_signing_order = " .. (new_signing_order))                   

					end

				end

				if(new_signing_order)then
                   ngx.log(ngx.NOTICE,"UPDATE SIGNING ORDER to " .. tostring(new_signing_order))                   
                   local order_update = {["$set"] = {current_order=new_signing_order}}
                   u3, err3 = col:update({id=body.id}, order_update, 0, 0, true)
				end
                -- end

                count_sign = 0
                for i3,v3 in ipairs(s.fields) do
                    if v3.status == 1 then count_sign = count_sign + 1 end
                end
                ngx.log(ngx.NOTICE,"count_sign = " .. count_sign)                   


                local pdf_file = '/home/creden/work/app/client/face/images/card/' .. s.pdfFile .. '.pdf'
                local pdf_dump_key = 'pdf_dump:'..pdf_file

                if table.getn(s.fields) == count_sign then 
                    s.status = 1 
                    -- Prepare pdf_dump
					local pdf_dump_cmd = '/home/creden/scripts/pdf_dump.lua "' ..  pdf_file .. '"'
                    os.execute(pdf_dump_cmd)
                    pdf_info = cjson.decode(rc:get(pdf_dump_key))
                    rc:set('last_pdf_dump', cjson.encode(pdf_info))
                    rc:set('last_pdf_dump_cmd', pdf_dump_cmd)
                    -- end

                    --update = {["$set"] = {complete_dtm = bson.get_utc_date(ngx.now() * 1000)}}
                    --u, err = col:update({id=body.id}, update, 0, 0, true)



					local do_field_count = 0
                    for i,v in ipairs(s.fields) do  
						   do_field_count = do_field_count +1
						   if(v.id)then
                              ngx.log(ngx.NOTICE,' start signing for ' .. tostring(v.id) .. ' ' , v.type , 'do_field_count' ,do_field_count)
                              do_field(s, v) 
						  else
                              ngx.log(ngx.NOTICE,' skip signing for ' .. tostring(v.id) .. ' ' , v.type , 'do_field_count' ,do_field_count)
                              ngx.log(ngx.NOTICE, cjson.encode(v))


						  end
                    end

                    if table.getn(obj_txt) > 0 then
                        data_add_text = {input_file = pdf_file , output_file = pdf_file, field = obj_txt} 
                        a = cjson.encode(data_add_text)
                        rc:set('test_add_text',a) 
                        cmd = "java -cp '/home/creden/work/app/PDFTimestamping/target/*' PdfBoxTool.AddText '"..a.."'"   
                        ngx.log(ngx.NOTICE,'=========java add text===========') 
                        ngx.log(ngx.NOTICE,cmd)                   
                        os.execute("java -cp '/home/creden/work/app/PDFTimestamping/target/*' PdfBoxTool.AddText '"..a.."'")
                    end

                    s.ip_stamp = header["x-real-ip"]
                    update = {["$set"] = {fields=s.fields, status=s.status, ip_stamp = header["x-real-ip"], complete_dtm = bson.get_utc_date(ngx.now() * 1000) }}
                    u, err = col:update({id=body.id}, update, 0, 0, true)

                    local res0, err0 = prog('cpdf', '-add-text', "DOCUMENT ID: "..s.id, '-topleft', 30, '-font-size', 15, pdf_file , '-o', '/home/creden/work/app/client/face/images/card/'..s.id..'-stamp2.pdf')
                    
					local tc_edoc_2 = cjson.encode(s)
					assert(tc_edoc_2~=nil, "tc_edoc_2 is nil")
					ngx.log(ngx.NOTICE, 'adding tc_edoc_2 ' .. tc_edoc_2 )
					rc:set('tc_edoc_2', tc_edoc_2)
                    local res, err = prog('lua', '/home/creden/work/app/server/capi/tc_edoc_2.lua', tc_edoc_2)


                    if err then
                        ret = {success = false, error_msg = err}
                    else
                        ret = {success = true, error_msg = err}
                    end
    
                    

                    os.execute('cp /home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp2.pdf /home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp-backup.pdf')
                    cmd3 = 'pdftk /home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp-backup.pdf /home/creden/work/app/client/face/images/card/' .. s.id .. '.pdf cat output /home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp.pdf'
                    ngx.log(ngx.NOTICE,cmd3)
                    os.execute(cmd3)

                    -- Add attachments
                    for i,v in ipairs(s.fields) do  
                        --ngx.log(ngx.NOTICE,' add attachments for ' .. v.id .. ' ' , v.type)
                        if(v.toolName and v.toolName=='Attachment')then
                           local cmd = 'cpdf -merge /home/creden/work/app/client/face/images/card/'..s.id..'-stamp.pdf /home/creden/work/app/client/face/images/card/'..v.toolData..' -o /home/creden/work/app/client/face/images/card/'..s.id..'-stamp.pdf'
                           ngx.log(ngx.NOTICE,' add attachments cmd: ' .. cmd)
                           os.execute(cmd)
                         end   
                    end
                    -- end add attachments
    

                    --ETDA Timestamp
					local ts_file = '/home/creden/work/app/client/face/images/card/' .. s.id .. '-stamp.pdf'
                    local ts_cmd = 'sh /home/creden/scripts/ts.sh ' .. s.id .. '-stamp.pdf'
                    os.execute(ts_cmd)
                    os.execute('cp /home/creden/work/app/client/face/images/card/' .. s.id ..'-stamp_timestamped.pdf ' ..     ts_file  )
                    os.execute('redis-cli expire '.. pdf_dump_key  ..' 16')
                    --END ETDA Timestamp

					--Blockchain
					local bc_cmd = 'lua /home/creden/scripts/put_tx.lua '.. s.id .. ' "' .. ts_file  ..'"' ..' | node /home/creden/scripts/stellar_tx.js | /home/creden/scripts/rset.lua bc:' .. s.id .. ' &'
                    rc:set('last_bc',bc_cmd)
                    os.execute(bc_cmd)
					--end Blockchain

                    local res, err = prog('sh', '/home/creden/work/app/server/capi/create_hash.sh', s.id..'-stamp')
                    
                    if err then
                        ret = {success = false, error_msg = err}
                    else
                        str = res.stdout
                        str = str:gsub('\n', '')
                        update = {["$set"] = {hash=str}}
                        u, err = col:update({id=body.id}, update, 0, 0, true)
                    end


					--globlex session 
					if(is_gb)then
                    ngx.log(ngx.NOTICE, "created globlex session ...")
  					local session = require "resty.session".start{ secret = "4321" }      
					  session.data.username = s.fields[1].email
					  session:save()
				    end  
					--end globlex session

                    
                else

                end

               
                
               


                
            end

print(cjson.encode(ret))


