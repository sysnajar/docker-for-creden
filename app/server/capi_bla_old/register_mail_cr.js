var mailer = require("nodemailer");

var html = `
<html>
 <head>
    <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Confirmation</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
  a {
      color:#1667B2;
  }
  .redius{
    border-radius:  30px 30px 0 0;;
  }
  .redius2{
    border-radius:  0 0 30px 30px;;
  }
  .redius3{
    border-radius:  30px;
  }
  
  </style>

</head>
<body style="margin: 0; padding: 0;font-family: 'Prompt', sans-serif;" bgcolor="#F5F5F7">
 <table border="0"  cellpadding="0" cellspacing="0" width="100%" background="`+process.env.HOST_NAME+`/signature/img/bg-01.png" >
  <tr>
   <td >
     <table class="redius3" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;box-shadow: 5px 5px 5px 1px rgba(50,50,50,.1);margin-top: 50px;margin-bottom: 70px;">
       <tr  >
         <td class="redius"  align="center" bgcolor="#ffffff" style="padding: 40px 0 0px 0;">
          <img src="`+process.env.HOST_NAME+`/signature/img/Creden_logos_Logo_C.png" alt="Creating Email Magic" width="300" height="100" style="display: block; border: 0px solid #ffffff;" />
         </td>
       </tr>
       <tr>
         <td bgcolor="#ffffff" style="padding: 0px 30px 40px 30px;" class="redius2">
           <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
             <td>
               <!-- ตรง creden ให้ คิวรี่ชื่อลูกค้ามาด้วย -->
              <b>เรียนลูกค้า `+process.argv[3]+`</b> 
             </td>
            </tr>
            <tr>
             <td style="padding: 20px 0 10px 0;">
              <p>ขอขอบพระคุณเป็นอย่างสูงที่คุณได้ไว้วางใจใช้บริการ Creden ระบบลายเซ็นออนไลน์ของเรา</p>
             </td>
            </tr>
            <tr>
              <td style="padding: 0px 0 10px 0;">
                <a href="`+process.argv[4]+`"> กดลิ้งก์นี้เพื่อยืนยันอีเมลของคุณ </a> 
              </td>
             </tr>

             
             <tr>
              <td style="padding: 0px 0 0 0;">
                <!-- ตรงนี้คิวรี่ อีเมลลูกค้ามาต่อด้วยนะคะ ต่อจากคำว่า อีเมล -->
                โปรดตรวจสอบว่าอีเมล `+process.argv[2]+`  เป็นอีเมลที่สามารถติดต่อคุณได้
                <br> <br> หากต้องการสอบถามข้อมูลเพิ่มเติม
                <br> โปรดติดต่อฝ่ายบริการลูกค้า  <a href="">info@creden.co</a>
              </td>
             </tr>
             <tr>
                <td style="padding: 20px 0px 0px 0;">
                 <p>ขอแสดงความนับถือ</p>
                </td>
               </tr>
               <tr>
                  <td style="padding: 0px 0px 0px 0;">
                   <img src="`+process.env.HOST_NAME+`/signature/img/Creden_logos_Logo_C.png"  width="150" height="50" style="display: block; padding-left:px;" />
                  </td>
                 </tr>
                 <tr>
                    <td style="padding: 0px 0px 0px 0;">
                    <p>บริษัท ครีเดน เอเชีย จำกัด</p>หมายเหตุ : โปรดอย่าตอบกลับอีเมลนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
                    
                    </td>
                   </tr>
                   <tr>
                      <td style="padding: 30px 0px 0px 0;">
                          <hr width="99%"> 
                      
                      </td>
                     </tr>



                     <!-- en -->
                     <tr>
                        <td style="padding: 20px 0 20px 0;">
                          <!-- ตรง creden ให้ คิวรี่ชื่อลูกค้ามาด้วย -->
                         <b>Dear `+process.argv[3]+`,</b> 
                        </td>
                       </tr>
                       <tr>
                        <td style="padding: 0px 0 10px 0;">
                         <p>We highly appreciate the confidence and trust that you have placed in Creden.co</p> 
                         
                        </td>
                       </tr>
                       <tr>
                         <td style="padding: 0px 0 0 0;">
                           <a href="`+process.argv[4]+`"> Please click here </a> 
                         </td>
                        </tr>
           
                        
                        <tr>
                         <td style="padding: 10px 0 0 0;">
                           <!-- ตรงนี้คิวรี่ อีเมลลูกค้ามาต่อด้วยนะคะ ต่อจากคำว่า อีเมล -->
                           to verify that your e-mail address : `+process.argv[2]+` is valid and can be used
                           <br> to communicatewith you.
                           <br>
                           <br> if you have any inquiry, please do not hesitate to contact our customer support
                           <br> at  <a href="">info@creden.co</a>
                         </td>
                        </tr>
                        <tr>
                           <td style="padding: 20px 0px 0px 0;">
                            <p>Yours sincerely, </p>
                           </td>
                          </tr>
                          <tr>
                             <td style="padding: 0px 0px 0px 0;">
                              <img src="`+process.env.HOST_NAME+`/signature/img/Creden_logos_Logo_C.png"  width="150" height="50" style="display: block; padding-left:px;" />
                             </td>
                            </tr>
                            <tr >
                               <td style="padding: 0px 0px 0px 0;">
                               <p>Creden.co</p>This is an automated; please do not replay.
                               
                               </td>
                              </tr>
           </table>
         </td>
       </tr>
       <tr>
 
       </tr>
     </table>
   </td>
  </tr>
 </table>
</body>
</html>
`

var smtp = {
    host: 'smtp.gmail.com', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      user: 'credenmailer@gmail.com', //user account
      pass: 'creden2018' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);

  var mail = {
    from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
    to: process.argv[2], //to email (require)
    cc: '',
    subject: '[CREDEN] code authen', //subject
    html: html//head + body + end
 }

 smtpTransport.sendMail(mail, function(error, response){
    smtpTransport.close();
    if(error){
       //error handler
       var res = {"success":false, "message":error}
       console.log(res);
    }else{
       //success handler 
       var res = {"success":true}
       console.log(res);
    }
    process.exit();
 });
