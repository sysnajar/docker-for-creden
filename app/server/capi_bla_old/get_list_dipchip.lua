#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("slot_certificates")
    list , err = col:find({})

    if err then
        ret = {success = false, error_msg = "find plan unsuccess", error_code = "" }
    else
        
    end
end