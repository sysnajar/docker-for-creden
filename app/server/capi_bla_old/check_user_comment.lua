#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local ret = {success = false}
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    db2 = conn:new_db_handle("dopa")
    col2 = db2:get_col("esig_user")

    if session.data.username then 
        local new_data = {}
        for k,v in ipairs(body.user) do 
            user_data = col2:find_one({adminEmail = v.email})
            if user_data then
                v.name = user_data.adminFisrtname
                if user_data.adminLastname then
                    v.name = v.name.." "..user_data.adminLastname 
                end
            end

            table.insert(new_data,v) 
        end
        ret.success = true
        ret.data = new_data
    else
        ret = {success = false, error_msg = err, msg = "no session" }
    end

end

print(cjson.encode(ret))