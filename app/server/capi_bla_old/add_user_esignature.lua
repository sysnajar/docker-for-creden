#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local scrypt = require "resty.scrypt"
local md5 = require 'md5'
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")

    local col_company_members = db:get_col("company_members")
    local s_company, err_s_company = col_company_members:find_one({email = body.email})
    local comp_id = col:find_one({adminEmail = body.created_by})
    user = { adminEmail = body.email or "N/A" }
    u, err = col:find_one(user)

    if u then
        ret = { success = false, error_msg = "email user duplicate"}
    else
        local date = os.date('%d/%m/%Y %H:%M:%S', os.time() + (7*60*60))
        local new_hash = scrypt.crypt(tostring(body.password))
        local encrypt = md5.sumhexa(body.password) 
        
        -- body.role = nil
        local data = { 
            created_dtm = date,
            adminEmail = body.email,
            adminFisrtname = body.fullname,
            activeStatus = 0,
            hashedPassword = new_hash,
            encryptPass = encrypt,
            created_by = body.created_by,
            created_status = false,
        }

        d,err = col:insert({data})

        if d then
            ret = { success = true, error_msg = "add user success" }
        else
            ret = { success = false, error_msg = "add user unsuccess" }
        end

        
    end


end

print(cjson.encode(ret))
