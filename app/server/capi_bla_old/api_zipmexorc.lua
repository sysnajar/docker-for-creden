#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local HOST_NAME = os.getenv("HOST_NAME")
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
prog.timeout_fatal = false
ret = {success = false}

if(ngx)then  
    ngx.save_log({desc = "api_kyc"})
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    if body == nil then 
        ret.error_msg = 'Invalid Data'
        print(cjson.encode(ret))
        return 0
    end
    header = ngx.req.get_headers()
else
   body   = {}
end
function trim5(s)
    return s:match'^%s*(.*%S)' or ''
end
  
function getVal(key)
    local ret = body[key]
    ret = ret and trim5(ret) or nil
    if(ret=='')then ret = nil end
    return ret
end
function decode_loadBase64(photo1)
    -- body
   local key = "base64_zipmex"
   local dir = '/home/creden/work/app/client/face/images/card/'
    redis = require 'redis'
    rc = redis.connect('127.0.0.1', 6379)
    local cnt = tostring(rc:incr('kyc.counter'))
    local dir = '/home/creden/work/app/client/face/images/card/'
    for i, k in ipairs({'photo1'}) do
        local v = getVal(k)
        if(v)then
            local filename = cnt..k 
            chk = string.sub(v, 1, 4)
            if string.match(chk, "http") then
                local cmd = 'wget '.. v ..' -O ' .. dir .. filename .. '.png'
                os.execute('sh /home/creden/work/app/server/capi/sh_download_img.sh')
            else
            local tmp = 'tmp.kyc.img' .. filename   
            rc:set(tmp , v)
            local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
            os.execute(cmd)
            rc:del(tmp) 
        end     
  
        body[k] = filename .. '.png'
      end
    end
    body.cardImg = body.cardImg1
    ocr(body.cardImg)




end

function ocr(cardImg)
    -- body
    local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_demo.lua', cardImg)
    print(res.stdout)

end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    ok,  err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end

function chk_apiKey(apiKey)
    local chk = nil
    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
            s._id = nil
            chk = true 
            body.username = s.adminEmail
            if body.username == "creden" then
                body.createDate = os.date("%x")
                body.createTime = os.date("%X")
              
            end
        end
    end
    return chk
end

function chk_tx(usr)
    local ret = {success = false, error_msg = 'No Credit'}
    local counter_key = 'counter.'..usr..'.tx'
    local max_counter = 'counter.'..usr..'.txMax'
    c = rc:get(counter_key)  
    m = rc:get(max_counter)
    if not r and not m then
        print(cjson.encode(ret))    
        ngx.exit(ngx.HTTP_OK)
    else
        if tonumber(c) >= tonumber(m) then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            rc:incr(counter_key)      
        end
    end
end



if(chk_apiKey(header.apiKey)) then
    chk_tx(body.username)
    decode_loadBase64(body.photo1)





    
else
    ret.success = false 
    ret.error_msg = 'Authentication Fail'
end

print(cjson.encode(ret))
   

