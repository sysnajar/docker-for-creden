#!/usr/bin/env lua

function init()
if(#arg > 0  )then
  cjson = require 'cjson'
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379)
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(id)
	local key = 'PREP:BUS:' .. id
	local t1 = cjson.decode(rc:get(key))
	local t2 = {
		DBD_com = (type(t1.data)=='table') and #t1.data or 0
	}

	return t2
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
