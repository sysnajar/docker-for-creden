curl -v -X POST https://api.line.me/v2/bot/message/push \
-H 'Content-Type:application/json' \
-H 'Authorization: Bearer QTKVkhnoiSzGzTpr8tlPp2NI1NX+jDS2srLD6rYpfRrk0pWMllCY49dmtNAJp3nvEIf9OXkb+WJAxTl07voNdqPXZwg47D3kgY+xkxMPL67hVn9rYviCwUqL2S9yw1Vo8rP7vigurZjvXeBWi2OrwwdB04t89/1O/w1cDnyilFU=' \
-d '{
  "to": "'$1'",
  "messages": [
    {
      
  "type": "flex",
  "altText": "Q1. Which is the API to create chatbot?",
  "contents": {
    "type": "bubble",
    "body": {
      "type": "box",
      "layout": "vertical",
      "spacing": "md",
      "contents": [
        {
          "type": "box",
          "layout": "vertical",
          "contents": [
            {
              "type": "text",
              "text": "กรุณา",
              "align": "center",
              "size": "xxl",
              "weight": "bold"
            },
            {
              "type": "text",
              "text": "เลือกรายการที่จะทำ",
              "wrap": true,
              "weight": "bold",
              "margin": "lg"
            }
          ]
        },
        {
          "type": "separator"
        },
        {
          "type": "box",
          "layout": "vertical",
          "margin": "lg",
          "contents": [
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "1.",
                  "flex": 1,
                  "size": "lg",
                  "weight": "bold",
                  "color": "#666666"
                },
                {
                  "type": "text",
                  "text": "create usernae kyc",
                  "wrap": true,
                  "flex": 9
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "2.",
                  "flex": 1,
                  "size": "lg",
                  "weight": "bold",
                  "color": "#666666"
                },
                {
                  "type": "text",
                  "text": "crate usernae signature",
                  "wrap": true,
                  "flex": 9
                }
              ]
            },
            {
              "type": "box",
              "layout": "baseline",
              "contents": [
                {
                  "type": "text",
                  "text": "3.",
                  "flex": 1,
                  "size": "lg",
                  "weight": "bold",
                  "color": "#666666"
                },
                {
                  "type": "text",
                  "text": "crate signature & kyc",
                  "wrap": true,
                  "flex": 9
                }
              ]
            }
            
          ]
        }
      ]
    },
    "footer": {
      "type": "box",
      "layout": "horizontal",
      "spacing": "sm",
      "contents": [
        {
          "type": "button",
          "style": "primary",
          "height": "sm",
          "action": {
            "type": "message",
            "label": "1",
            "text": "kyc"
          }
        },
        {
          "type": "button",
          "style": "primary",
          "height": "sm",
          "action": {
            "type": "message",
            "label": "2",
            "text": "signature"
          }
        },
        {
          "type": "button",
          "style": "primary",
          "height": "sm",
          "action": {
            "type": "message",
            "label": "3",
            "text": "crate signature & kyc "
          }
        }
      ]
    }
  }

    }
  ]
}'
