#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local redis = require 'redis'
local scrypt = require "resty.scrypt"
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    json = ngx.req.get_body_data()
    body = cjson.decode(json)
else
    body  = {adminEmail = arg[1] , adminPassword = arg[2] ,adminCode = arg[3]} 
end

    ret = {success = false, err = "N/A"}


   local real_code = rc:get('resetpwd:' .. body.adminEmail)
   if(real_code == body.adminCode) then
        
	        --mongo boilet 
	        conn = mongo()
            conn:set_timeout(1000)
            ok, err = conn:connect('127.0.0.1',27017)
            db = conn:new_db_handle("dopa")
            if body.login_service == 'esig' then
              col = db:get_col("esig_user")
            else
              col = db:get_col("company")
            end

			local update_cmd = {["$set"] = {
			hashedPassword  = scrypt.crypt(tostring(body.adminPassword))
		
		    }}

            u, err = col:update({adminEmail=body.adminEmail}, update_cmd, 0, 0, true)
            s, err = col:find_one({adminEmail=body.adminEmail}, nil, true)
            if not err and s.activeStatus ~= '0' then
                --session
                local session = require "resty.session".start{ secret = "4321" }
                math.randomseed(os.clock()*100000000000)
                local extend_key=''
                for i=1,2 do
                    a = math.random(10, 99)       
                    extend_key = extend_key .. a
                end
                rc:set('extend_key'..':'..body.adminEmail..':'..extend_key, "true")
                rc:expire('extend_key'..':'..body.adminEmail..':'..extend_key, 60*30)
                session.data.username = body.adminEmail
                session.data.custCode = body.adminEmail
                session:save()
                --end session
                s._id = nil
                ret.data = s
                ret.extend_key = extend_key
            end
            ret.success = true
   else
	        ret.err = "Invalid code" 
   end


print(cjson.encode(ret))
