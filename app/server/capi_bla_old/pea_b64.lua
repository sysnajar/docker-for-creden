#!/usr/bin/env lua
local cjson = require 'cjson.safe'
-- local mongo = require 'resty-mongol'
-- local bson = require 'resty-mongol.bson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout = 1000 * 300
prog.timeout_fatal = false
--local srcpath = '/home/creden/work/app/client/face/images/card/'

function file_exists(file)
    local result = false
    local f = io.open(tostring(file),'r')
    if f then
        result = true
        f:close() 
    end
    return result
end

print('Checking file base64')
while true do
    os.execute("sleep 1")
    local fiPdf = rc:lpop('pdflist')

    if (fiPdf) then
        if file_exists(fiPdf) then
            local n = string.len(tostring(fiPdf))
            local b64file = string.sub(tostring(fiPdf),1,n-3) .. 'b64'

            if not file_exists(b64file) then
                local res,err = prog('sh','/home/creden/work/app/server/capi/pea_decode_b64.sh',fiPdf,b64file)            
                if (not err) then
                    print(b64file)
                else
                    print('Error : ' .. tostring(err))
                end
            end
        end
    end
end