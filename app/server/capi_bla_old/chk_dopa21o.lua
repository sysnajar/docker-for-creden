#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = 'nutp10.1@gmail.com', password = '1234'} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
	local session = require "resty.session".open({secret = "4321"})
	
    if(body.compCode == session.data.custCode) then
		if tostring(body.mode) == 'all' then 
			s = col:find({compCode = body.compCode},{cardImg1=0,cardImg2=0,cardImg3=0})    
		else
			s = col:find({status = 'wait', compCode = body.compCode})
		end
	else

		err = 'Invalid Login , please re-login and try again' 
	end	

    if s then 
        data = {}
		local filter = body.filter

        for k, v in s:pairs() do
            v._id = tostring(v._id)
            v.cardImg1 = nil
			v.cardImg2 = nil
            v.cardImg3 = nil
            local blink = rc:get('liveness.'..v.compCode..'.'..v.txid..'.vdo')
            local voice = rc:get('liveness.'..v.compCode..'.'..v.txid..'.audio')
            local face = rc:get('liveness.'..v.compCode..'.'..v.txid..'.face')
            --v.livenessResult = {isValidBlink = (blink == '1' and true or false), isValidVoice = (voice == '1' and true or false), isValidFace = (face == 'true' and true or false)}
            v.livenessResult = {isValidBlink = (blink == '1' and true or false), isValidVoice = (voice == '1' and true or false), isValidFace = (face == 'true' and true or false)}
			v.livenessResult.isValidVoice = nil
			v.livenessResult.isValidFace  = nil


            local tensor = nil
			if( v.card_reader_id and string.find(v.card_reader_id,'ghb')==1)then 
			   local tkey = v.card_reader_id .. '.tensorflow.result'	
			   local tval = rc:get(tkey)
               ngx.log(ngx.NOTICE, 'get tensorflow result ' .. tkey  .. tostring(tval))
               if(tval)then
					v.tfResult = cjson.decode(tval)
					v.livenessResult.TensorFlow_is_fake = v.tfResult.is_fake
					v.livenessResult.TensorFlow_confidence = v.tfResult.confidence
                    ngx.log(ngx.NOTICE, 'set tensorflow result to ' .. v.txid )

			   end
			   
			end

			if(filter ~= '')then
				if( 
					string.find(tostring(v.cardNo), filter)	
					or string.find(tostring(v.fnameTH), filter)	
					or string.find(tostring(v.lnameTH), filter)	
					or string.find(tostring(v.fnameEN), filter)	
					or string.find(tostring(v.lnameEN), filter)	
					or string.find(tostring(v.channel), filter)	
					or string.find(tostring(v.status), filter)	
				    	
				  )then
                   table.insert(data, v)
				end

			else
               table.insert(data, v)
			end
			
            --table.insert(data, k, v)
        end
        ret = {success = true, error_msg = "complete", data = data}
    else
        ret = {success = false, error_msg = err}
    end
end
print(cjson.encode(ret))
