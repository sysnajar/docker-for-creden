#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {} 
end

    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        local r = rc:get('system_register')
        ret = {success = true, data = r }
    end

print(cjson.encode(ret))


