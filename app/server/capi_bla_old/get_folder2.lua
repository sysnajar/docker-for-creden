#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}


function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        -- body.email = session.data.username
        body.owner_folder = session.data.username
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {owner_folder = arg[1]} 
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else
    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders2")
    local not_want = { "INBOX", "SEND", "SELF", "CANCEl"  }
    list = { owner_folder = body.owner_folder or "N/A", cat = {['$nin'] = not_want} }
    l, err = col:find(list)

    if err then 
        ret = { success = false, error_msg = "folder not find", error_code = ""}
    else

        local list_folder = {}
        for i, v in l:pairs() do
            v._id = tostring(v._id)
            table.insert(list_folder, v)
        end

        ret = { success = true, error_msg = "get folder success", error_code = "", data = list_folder }

    end
end
print(cjson.encode(ret))