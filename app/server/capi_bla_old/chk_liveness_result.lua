#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local bson = require "resty-mongol.bson"
local body = nil
local object_id = require "resty-mongol.object_id"
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {adminEmail=body.username,txid=body.txid}
end

    local key_audio = 'liveness.'..body.username..'.'..body.txid..'.audio'
    local key_vdo = 'liveness.'..body.username..'.'..body.txid..'.vdo'
    local key_status_vdo = 'liveness_status_vdo.'..body.username..'.'..body.txid
    local get_vdo = rc:get(key_vdo) 
    local get_audio = rc:get(key_audio)
    local get_vdo_status = rc:get(key_status_vdo)
    local score = 0
    local text

    if(get_vdo == nil or get_audio == nil) then

        score = -1
    

    else
        if(get_vdo == "1" and get_audio == "0" and get_vdo_status == 'true') then
            text = 'พบการกระพริบตา ไม่พบเสียง'
        elseif(get_vdo == "0" and get_audio == "1" and get_vdo_status == 'true') then
            text = 'พบเสียง ไม่พบการกระพริบตา'
        elseif(get_vdo == "0" and get_audio == "0" and get_vdo_status == 'true') then
            text = 'ไม่พบเสียง ไม่พบการกระพริบตา'
        elseif(get_vdo == "0" and get_audio == "0" and get_vdo_status == 'false') then
            text = 'พบข้อผิดพลาด กรุณาเชื่อต่อระบบ WiFi แล้วลองทำใหม่อีกครั้ง'
        elseif(get_vdo == "1" and get_audio == "0" and get_vdo_status == 'false') then
            text = 'พบข้อผิดพลาด กรุณาเชื่อต่อระบบ WiFi แล้วลองทำใหม่อีกครั้ง'
        elseif(get_vdo == "0" and get_audio == "1" and get_vdo_status == 'false') then
            text = 'พบข้อผิดพลาด กรุณาเชื่อต่อระบบ WiFi แล้วลองทำใหม่อีกครั้ง'
        elseif(get_vdo == "1" and get_audio == "1" and get_vdo_status == 'false') then
            text = 'พบข้อผิดพลาด กรุณาเชื่อต่อระบบ WiFi แล้วลองทำใหม่อีกครั้ง'
        end
        score = get_vdo + get_audio
        key =  'score.' .. body.txid
        rc:set(key, tostring(score))
        rc:del(key_audio)
        rc:del(key_vdo)
        rc:del(key_status_vdo)
        

    end

    
    
    ret = { success = true , score = score ,text = text}

print(cjson.encode(ret))
    -- conn:set_timeout(5000)
    -- -- ok, err = conn:connect('10.127.1.21',27017)
    --     ok, err = conn:connect('127.0.0.1',27017)

    -- if err then
    --     ret = {success = false, error_msg = err}
    -- else
    --     ret = {}
    --     local sess = chk_session_admin(body)
       

    --     if sess  then

            
    --          ret = { success = true, error_msg = "มีสิทธิ์" }
    --     else
    --         ret = { success = false, error_msg = "คุณยังไม่ได้เข้าสู่ระบบ กรุณาเข้าสู่ระบบเพื่อดูข้อมูล" }
        
    --     end
    --     -- ret = { success = true , sess = sess, member = member, password = password, role_user = role_user }

    --     print(cjson.encode(ret))
    -- end




   
