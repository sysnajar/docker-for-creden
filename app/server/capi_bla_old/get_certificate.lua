#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {owner_folder = arg[1]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("certificate")
    list = {cer_owner = body.cer_owner or ""}
    l,err = col:find(list)

    if l then
            local list_cer = {}
            for i,v in l:pairs() do 
            v._id = tostring(v._id)
            table.insert(list_cer,v)
            -- print(i,v.name_folders .. "   " .. v.owner_folder  )
            end
        ret = {data = list_cer or {}}

    else
        ret = { data = {} }
    end
print(cjson.encode(ret))
end





