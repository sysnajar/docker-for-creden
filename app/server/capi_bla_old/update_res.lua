#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local txid = ''
local vdo_score = 0
local audio_score = 0
local total_score = 0
local isValidLiveness = false
local isFaceVerified = false
local res = {success = true}
HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = cjson.decode(arg[1])
end

--body = {success = true, username = 'creden', txid = '', score = 1, maxScore = 3, type = 'vdo'}
--body = {success = true, username = 'creden', txid = '', score = 2, maxScore = 3, type = 'audio'}

function chk_res() 
    local status = nil
    vdo_score = rc:get('liveness.'..body.username..'.'..body.txid..'.vdo')
    audio_score = rc:get('liveness.'..body.username..'.'..body.txid..'.audio')
    face_ver = rc:get('liveness.'..body.username..'.'..body.txid..'.face')
    -- if vdo_score and audio_score and face_ver then 
            if vdo_score and audio_score  then 

        status = true 
        -- total_score = vdo_score + audio_score
        -- isFaceVerified = face_ver
    end
    return status
end

function update_res() 
    local key = 'liveness.'..body.username..'.'..body.txid..'.'..body.type
    local chk = rc:get(key)
    --if not chk then
        rc:set(key, body.score)
    --end
end

function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical, isValidLiveness)
    -- if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidAML) == 'true' and tostring(isValidLiveness) == 'true' then
    if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidLiveness) == 'true' then
        body.isValid = true
    else
        body.isValid = false
        -- line noti
		ngx.log(ngx.NOTICE, 'txid = ' .. body.txid)
		local sdk_tx  = db:get_col("sdk_code"):find_one({txid = body.txid})
		local ekyc_tx = db:get_col("ekyc"):find_one({txid = body.txid})
		if (sdk_tx) then
			os.execute('/home/creden/work/app/server/capi/linekyc.sh "Failed liveness ekyc attempt:' .. sdk_tx.ref_no ..'" ' .. tostring(ekyc_tx._id) .. ' ' .. HOST_NAME ..' &' )
    end
    end 
end

update_res()

if chk_res() then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")
        selector = {txid=body.txid, compCode = body.username}
        s = col:find_one(selector)
        if s then
            -- isValidLiveness = true
            -- body.isValid = true

            --by pass
            local get_email  = db:get_col("sdk_code"):find_one({txid = body.txid})
            local pass_email = 'sysnajar@gmail.com'..'nutp10.1@gmail.com'-- format is --> 'sysnajar@gmail.com'..'nutp10.1@gmail.com'..
            if string.find(pass_email, get_email.ref_no) then 
                audio_score = 1
            end
            total_score = vdo_score + audio_score
            isFaceVerified = face_ver
            ---------------------

            if total_score == body.maxScore and tostring(isFaceVerified) == 'true' then isValidLiveness = true end
            chk_is_valid(s.isValidDopa, s.isValidAML, s.isFaceIdentical, isValidLiveness)
            update = {["$set"] = {process_status = 'finished', isValid = body.isValid, isValidLiveness = isValidLiveness, livenessResult = {isFaceVerified = isFaceVerified, score = total_score, maxScore = body.maxScore, pc_id=body.pc_id, error_msg=body.error_msg, error_code=body.error_code, fps=body.fps}}}
            i, err = col:update(selector, update, 0, 0, true)

			-- NOTI
			     local sess_col = db:get_col('sdk_code')
			     local kyc_session  = sess_col:find_one({txid = body.txid, username = 'creden'})
				 if(kyc_session and kyc_session.ref_no) then 
					  local notifications = {
							   {id     = ngx.now()*1000,
								dtm = ngx.now()*1000,
								status = 0,
								description = 11,
								txid = body.txid,
                                email = kyc_session.ref_no,
                                code = kyc_session.code,
							    isValid = body.isValid
							    } 
							}
        
					   local db2 = conn:new_db_handle("edoc")
					   local ncol       = db2:get_col("notification")
					   local i2, err2   = ncol:insert(notifications, nil, true)


					     --resurrect kyc links if liveness test fail
					   if(body.isValidLiveness ~= true) then
                          local u3, err3 = sess_col:update({txid = body.txid}, {["$set"] = {status = 'EDIT'}}, 0, 0, true)
                       end

                          local u4, err4 = db:get_col('esig_user'):update({adminEmail = kyc_session.ref_no}, {["$set"] = {verified_ekyc = body.isValid}}, 0, 0, true)

					  
					   
				 end
			-- END NOTI

        end
    end
end
res.score = body.score
print(cjson.encode(res))
