#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}
local data = {}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id or session.data.role ~= "SUPER ADMIN" then
        print(cjson.encode({success = false, error_msg = "not super admin", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()    
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("esig_user")
    i, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(body.id)) })

    if i then
        key = { _id = i._id }
        local update_cmd = { role = body.role }
        local u, err = col:update(key, {["$set"] = update_cmd}, 0, 0, true)

        if err then
            ret = { success = false, error_msg = "update role unsuccess", error_code = "2008" }
        else
            ret = { success = true, error_msg = "update role success", error_code = "2009" }
        end

    else
        ret = { success = false, error_msg = "not found esign user", error_code = "2007" }
    end
end
print(cjson.encode(ret))