#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')

prog.timeout = 1000 * 300
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)

else
    body = {ChipNo = '3077231718156',pid='1103700530312',bp1no='12994434164'}
end
function check_dopa(ChipNo,pid,bp1no)
    local t = {}
    res, err = prog('sh', '/home/creden/work/app/server/capi/rdVatService3.sh', ChipNo, pid, bp1no)
    -- print(res.stdout)
    if(not err) then
        local tmp = cjson.encode(res.stdout)
        if(tmp) then
            find_desc_start = string.find(res.stdout,"<Desc>")
            find_desc_end = string.find(res.stdout,"</Desc>")
            cut_desc_find = string.sub(res.stdout,find_desc_start+6,find_desc_end-1)
            find_code_start = string.find(res.stdout,"<Code>")
            find_code_end = string.find(res.stdout,"</Code>")
            cut_code_find = string.sub(res.stdout,find_code_start+6,find_code_end-1)
            
            if(cut_code_find == "0") then
                t.isValidDopa = true
                t.success = true
                t.remark_code = "000"
                t.status = 'pass'
            elseif(cut_code_find == "1") then
                t.success = false
                t.isValidDopa = false
                t.remark_code = "001"
                t.status = 'fail'
            elseif(cut_code_find == "2") then
                t.success = false
                t.isValidDopa = false
                t.remark_code = "002"
                t.status = 'fail'
            elseif(cut_code_find == "3") then
                t.success = false
                t.isValidDopa = false
                t.remark_code = "003"
                t.status = 'fail'
            elseif(cut_code_find == "4") then
                t.success = false
                t.isValidDopa = false
                t.remark_code = "004"
                t.status = 'fail'
            elseif(cut_code_find == "5") then
                t.remark_code = "005"
                t.success = false
                t.isValidDopa = false
                t.status = 'fail'
            end
            t.remarks = cut_desc_find

        end
        -- end check tmp
    else
        t.remark_code = "006"
        t.remarks = "dopa มีปัญหา กรุณาติดต่อเจ้าหน้าที่"
    end
    return t
    -- end check prog--
end
if( body.ChipNo ~='' or body.pid~='' or body.bp1no~='') then
    local result = check_dopa(body.ChipNo,body.pid,body.bp1no)
 
    ret = {success = true,data=result}

end
print(cjson.encode(ret))
