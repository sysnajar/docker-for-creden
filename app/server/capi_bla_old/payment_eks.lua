#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

local body = nil

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
	body = ngx.req.get_post_args()
	if (body.supermankey ~= "supermanvalue") then
    	prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', 'header key worng pls check payment eks =>>'.. cjson.encode(body) )
		ngx.exit(401)
	end
else
    body = { id = arg[1] }
end

local conn = mongo()
conn:set_timeout(5000)
local ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', 'cannot connect mongo after payment =>'.. cjson.encode(body) )
else
	
    local db = conn:new_db_handle("dopa")
	
    ngx.log(ngx.INFO, 'got EKS payment callback', body.refno)
    --cert
	local col_packcert = db:get_col("package_cert")
    local key = {ref_id = body.refno or "N/A", status = false}
	local cert_rec = col_packcert:find_one(key)
	if(cert_rec)then
		local update_cmd = {["$set"] = {status = true}}
		local update_old = col_packcert:update(key, update_cmd)

	    ngx.log(ngx.INFO, 'got cert callback', body.refno)
	    ngx.exit(200)
         
	end
	--end cert

	local col_pack = db:get_col("package_ss")
	local col_history_payload_payment = db:get_col("history_payload_payment")

	local get_detail = col_pack:find_one({ ref_id = body.refno or "N/A", status = false })
	if get_detail then

		if get_detail.package ~= "addon" and get_detail.package ~= "dipchip" then
			local old_pack = col_pack:find({ company_id = get_detail.company_id or "N/A", active_package = true, package = {['$ne']="addon"} })
			local list_old_pack = {}
			for i, v in old_pack:pairs() do
				table.insert(list_old_pack, v)
			end	

			if #list_old_pack > 0 then
				local current_pack_active = list_old_pack[#list_old_pack] 
				for i, v in ipairs(list_old_pack) do
					-- if v._id  then
						local key = { _id = v._id, ref_id = v.ref_id  }
						local update_cmd = {
							active_package = false,
							end_active_package = bson.get_utc_date(os.time() * 1000)
						}
						local update_o ={["$set"] = update_cmd}
						local update_old = col_pack:update(key, update_o)
					-- end
				end
			end
		end

		body.supermankey = nil
		time = bson.get_utc_date(os.time() * 1000)
		local exp_day = 30
		if body.plan == "annual" then
			exp_day = 365
		end
		local tmpdata_update = {
			status = true,
			active_package = true,
			date_pay = time,
			active_start_date = time,
			active_expire_date = bson.get_utc_date((os.time()*1000) + (1000*60*60*24*exp_day))  
		}

		local cond = { ref_id = body.refno, status = false }
		local update_d ={["$set"] = tmpdata_update}
		local update_data = col_pack:update(cond, update_d)
		local inst,err = col_history_payload_payment:insert({body})
		--noti sucess line	
		text = "status: Sucess payment\nRefno: " .. body.refno .. "\ncustomeremail: " .. body.customeremail .. "\nTotal: " .. body.total .. "\nCard Type: " .. body.cardtype .. "\nProduct Detail: " .. body.productdetail .. "\nMerchant ID: " .. body.merchantid
    	-- prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', 'sucess payment =>'.. cjson.encode(body) )
		prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', text )

		local get_detail2 = col_pack:find_one({ ref_id = body.refno or "N/A" })

		--insert more info if dipchip
		if(get_detail.package == "dipchip" )then
			db2 = conn:new_db_handle("edoc")
			col2 = db2:get_col("booked_slot") 
			
			--1.1 find certificate expiration date
			ngx.log(ngx.INFO, 'BOOKING refno' .. tostring(get_detail2.booking_id)..',owner='..get_detail2.email)
			booking = col2:find_one({ booking_id = get_detail2.booking_id, booking_owner= get_detail2.email or "N/A"})
			ngx.log(ngx.INFO, 'BOOKING booking = ' .. tostring(booking))
			get_detail2.exp_month = booking.booking_exp_month

			--1.2 gen dipchip code
             res, err = prog('sh','/home/creden/work/app/server/capi/generate_qrcode.sh',
			 'qr'.. body.refno,  get_detail2.qr_refno )


			 ngx.log(ngx.INFO, "GEN QR sh /home/creden/work/app/server/capi/generate_qrcode.sh qr".. body.refno .. ' '.. get_detail2.qr_refno)

		end
		--end

		get_detail2._id = tostring(get_detail2._id)
		local json = cjson.encode(get_detail2)
		rc:set("package_refid." .. body.refno, json)
		rc:expire("package_refid." .. body.refno, 60*60*24*7)
		local col_esig = db:get_col("esig_user")
		local list_user = col_esig:find_one({ adminEmail = get_detail2.email or "N/A" })
		list_user._id = tostring(list_user._id)
		list_user = cjson.encode(list_user)
		if(get_detail.package == "dipchip" )then
			--send mail dipchip
			prog('node' ,'/home/creden/work/app/server/capi/payment_dipchip_mail.js', body.refno, list_user)
        else
			--send mail other
			prog('node' ,'/home/creden/work/app/server/capi/payment_eks_mail.js', body.refno, list_user)
		end


	else
		--noti err miss refno in db
		text = "status: Not found ref_no is status or missing ref_no\nRefno: " .. body.refno .. "\ncustomeremail: " .. body.customeremail .. "\nTotal: " .. body.total .. "\nCard Type: " .. body.cardtype .. "\nProduct Detail: " .. body.productdetail .. "\nMerchant ID: " .. body.merchantid
    	-- prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', 'Not found ref_no is status or missing ref_no =>'.. cjson.encode(body) )
		prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', text )
	end
end
