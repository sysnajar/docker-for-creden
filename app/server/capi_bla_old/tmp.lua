#!/usr/bin/env lua
local redis = require 'resty.redis'
local red = redis:new()
red:set_timeout(5000)
red:connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout_fatal = false

--os.execute('curl https://creden.co/xapi/api_dbd > /home/creden/work/app/server/dbd_res.txt')

local res, err = prog('curl', HOST_NAME..'/xapi/api_dbd')

if not err then
    red:set('demo.dbd',res.stdout)
    print(1)
end 
