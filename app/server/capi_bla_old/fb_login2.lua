#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local https=require 'ssl.https'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {accessToken = 'EAACEdEose0cBACJEZCF2TrA5QPpY5gqhDlerjzjZCZBLY3wuexcZBwnDpG2MMPPxPD1hglEiC0V9shbGqrzAEM6JTjgrLJivltT6vwIZCpia4b0N2LRdN3BZCECwuPZB0BQhgBAh0lS6ZC1YZCHg2k8RvieYZAeiZAvCAVmOpkbd8MZA8zmuwyQSrFDOjJU17IOgtzcZD'} 
end

res = {success = false, error_msg = '', data = {}}

if not body.error then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        res.error_msg = err
    else
        db = conn:new_db_handle("creden")
        user = db:get_col("users")
        if tostring(body.type) == 'facebook' then s = user:find_one({fb_id=body.fb_id}) body.profilePicture = 'https://graph.facebook.com/v2.10/'..body.fb_id..'/picture?type=large&redirect=true'
        else s = user:find_one({gl_id=body.gl_id}) end
        if s then
            --old user
            s._id = s._id:tostring()
            res_pic = string.match(s.profilePicture ,"https:")
            if not res_pic then
                local res_base64, err_base64 = prog('sh', '/home/creden/work/app/server/capi/img2base64.sh', s.profilePicture)
                if not err_base64 then
                    s.profilePicture = rc:get('img_tmp')
                    res = {success = true, error_msg = "complete", data = s}
                else
                    res = {success = false, error_msg = "err_base64", data = s}
                end
            else
                res = {success = true, error_msg = "complete", data = s}
            end

        else
            --new user
            --if body.type == 'facebook' then body.email = body.fb_id..'@fb.creden.co' end
            doc = body
            i, err = user:insert({doc}, nil, true)
            if err then
                res = {success = false, error_msg = err}
            else
                res = {success = true, error_msg = "complete", data = doc}
            end
        end
    end
else
    res.error_msg = "Login Fail!!"
end

print(cjson.encode(res))
