#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local HOST_NAME = os.getenv("HOST_NAME")
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
prog.timeout_fatal = false
ret = {success = false}

if(ngx)then  
    ngx.save_log({desc = "api_kyc"})
    ngx.req.read_body()
    print = ngx.say
    rc:set('last_ekyc_api', ngx.req.get_body_data())
    body = cjson.decode(ngx.req.get_body_data())
    if body == nil then 
        ret.error_msg = 'Invalid Data'
        print(cjson.encode(ret))
        return 0
    end
    header = ngx.req.get_headers()
else
   body   = {}
end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    ok,  err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end

function chk_apiKey(apiKey)
    local chk = nil
    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
            s._id = nil
            chk = true 
            body.username = s.adminEmail
            if body.username == "creden" then
                body.createDate = os.date("%x")
                body.createTime = os.date("%X")
                rc:lpush('hack.kyc.api', cjson.encode(body))
                print("")
                ngx.exit(ngx.HTTP_OK)
            end
        end
    end
    return chk
end

function chk_tx(usr)
    local ret = {success = false, error_msg = 'No Credit'}
    local counter_key = 'counter.'..usr..'.tx'
    local max_counter = 'counter.'..usr..'.txMax'
    c = rc:get(counter_key)  
    m = rc:get(max_counter)
    if not r and not m then
        print(cjson.encode(ret))    
        ngx.exit(ngx.HTTP_OK)
    else
        if tonumber(c) >= tonumber(m) then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            rc:incr(counter_key)      
        end
    end
end

function insert_mongo()
    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("ekyc")
        body.channel = 'api'
        body.createDate = os.date("%x")
        body.createTime = os.date("%X")
        body.createdDtm = bson.get_utc_date(ngx.now() * 1000)
        body.compCode = body.username
        body.process_status = 'processing'
        i, err_i = col:insert({body})
    end
end

if(chk_apiKey(header.apiKey)) then
    chk_tx(body.username)
    body.txid = tostring(rc:incr('kyc.tx.counter'))
    insert_mongo()
    ret.success = true
    ret.txid = body.txid
    --Todo queue
    rc:lpush('api.kyc.active', cjson.encode(body))
else
    ret.success = false 
    ret.error_msg = 'Authentication Fail'
end

print(cjson.encode(ret))
   

