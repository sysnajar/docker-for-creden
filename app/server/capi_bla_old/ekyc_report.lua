#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
else
   body  = {email = 'nutp10.1@gmail.com', password = '1234'} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("test")
    col = db:get_col("vdo")
    col1 = db:get_col("kyc")
    s = col:find()
    if s then 
        data = {}
        for k, v in s:pairs() do
            v._id = v._id:tostring()
            kyc = col1:find_one({id = v.id})
            if kyc then
                v.kyc = kyc.kyc
                v.isVerified = kyc.isVerified
            end
            table.insert(data, k, v)
        end
        ret = {success = true, error_msg = "complete", data = data}
    else
        ret = {success = false, error_msg = "err"}
    end
end
print(cjson.encode(ret))