#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function remove_unit_arr (arr)
    local hash = {}
    local res = {}
    for _,v in ipairs(arr) do
        if (not hash[v]) then
            res[#res+1] = v -- you could print here instead of saving to result table if you wanted
            hash[v] = true
        end
     end
     return res
end

function insert_folder (cat, doc)
    local date = bson.get_utc_date(ngx.now() * 1000)
    local new_folder = {
        owner_folder = body.email,
        name_folders = cat,
        created_Dtm = date,
        cat = cat,
        status_folder = "1",
        list_doc = doc
    }
    u, err = col:insert({new_folder})
    if err then
        ret = { success = false, error_msg = "insert folder and document unsuccess", error_code = "2106" } 
        print(cjson.encode(ret))
        ngx.exit(ngx.HTTP_OK)
    else
        ret = { success = true, error_msg = "insert folder and document success", error_code = "2105" } 
        print(cjson.encode(ret))
        ngx.exit(ngx.HTTP_OK)
    end
end

function remove_doc_before_move_folder(before, doc)
    if before ~= "N/A" then

        local folder = { owner_folder = body.email, _id = object_id.new(convertHexStringToNormal(body.before)) }
        local f, err = col:find_one(folder)

        local list = {}

        for i, v in ipairs(doc) do
            for i2,v2 in ipairs(f.list_doc) do 
                if v ~= v2 then
                    table.insert(list, v2)
                end
            end
        end

        local update_cmd ={["$set"] = { 
            list_doc = list, 
            updated_Dtm = bson.get_utc_date(ngx.now() * 1000)
        }}
        local u, err = col:update( folder, update_cmd, 1, 0, true)
    end
end

function move_document_to_folder(folder, doc)
    local list = {}

    for i2, v2 in ipairs(doc) do
        table.insert(folder.list_doc, v2)
    end

    folder.list_doc = remove_unit_arr(folder.list_doc)

    local key = { _id = folder._id }
    local update_cmd ={["$set"] = { 
        list_doc = folder.list_doc, 
        updated_Dtm = bson.get_utc_date(ngx.now() * 1000)
    }}
    local u, err = col:update( key, update_cmd, 1, 0, true)

    if err then
        ret = { success = false, error_msg = "update move document to folder unsuccess", error_code = "2104" } 
        print(cjson.encode(ret))
        ngx.exit(ngx.HTTP_OK)
    else
        ret = { success = true, error_msg = "update move document to folder success", error_code = "2103" } 
        print(cjson.encode(ret))
        ngx.exit(ngx.HTTP_OK)
    end

end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { id_doc = arg[1], id_folder = arg[2] }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1', 27017)
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else    

    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders2")

    -- if body.cat == "INBOX" or body.cat == "SEND" or body.cat == "SELF" or  body.cat == "CANCEL" then

    --     folder = { owner_folder = body.email, cat = body.cat }
    --     f, err = col:find_one(folder)

    --     if f then
    --         move_document_to_folder(f, body.list_doc)
    --     else
    --         insert_folder(body.cat , body.list_doc)
    --     end

    -- else

        folder = { owner_folder = body.email, cat = body.cat }
        f, err = col:find_one(folder)

        if err then
            ret = { success = false, error_msg = "folder not find", error_code = "2205"}
        else
            remove_doc_before_move_folder(body.before, body.list_doc)
            move_document_to_folder(f, body.list_doc)
        end
    -- end
end
print(cjson.encode(ret))
