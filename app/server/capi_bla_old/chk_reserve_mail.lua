local cjson  = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = { adminEmail = "N/A" } 
end
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    user = col:find_one({adminEmail = body.adminEmail or "N/A"}, nil, true) 
    
    reserve = rc:get("reserve." .. body.adminEmail) or nil
    
    if user or reserve then
        ret = { success = true, error_msg = "email valid" }
    else
        ret = { success = false, error_msg = "email invalid" }
    end

print(cjson.encode(ret))