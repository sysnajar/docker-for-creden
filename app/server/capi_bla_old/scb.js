Nightmare = require ('nightmare');
var ret = {success:false, error:null}
var app = {step : 1, acc_count :0, AI : 0 , MI : 0}
//var app_data = null;
var accounts = [];
var FN  = {}
var TotalMonth = 6
setTimeout(function(){ process.exit(0); }, 600000);


//-------- test redis ------
 function saveToRedis( k , v)
{
 var redis = require('redis')
 var rc  = redis.createClient();
 rc.set(k, v, function(){
     console.log('saved redis ['+k+']')

	 if(IsPublish) {  
		console.log('publishing events') 
	    rc.publish(k, v) 	
		//after publishing this'rc' can not send non [pubsub] command
	   }
	 
     process.exit(0);
 })
}
//--------------------------

var show = process.argv.length>2 && process.argv[2]=='true'
var waitTimeout = parseInt(process.argv[3])
if(isNaN(waitTimeout))waitTimeout = 7000;

var Key = process.argv[4];
var Username = process.argv[5];
var Password = process.argv[6];
var IsPublish = process.argv[7] == 'true';

console.log('show = '+show)
console.log('waitTimeout = '+waitTimeout)
console.log('Key = '+Key)
console.log('Username = '+Username)
console.log('Password = '+Password)

function handleError(ex)
{
   var data = {success:false, job_type : 'scb', key : Key,  error_msg : ex}
   saveToRedis(Key, JSON.stringify(data))
}


const nightmare = Nightmare({ show: show , waitTimeout : waitTimeout });
nightmare.on('console', (log, msg) => {
        console.log(msg)
   })

function execute()
{
   var NAMES = ['Login', 'Choose Account', 'Choose Month', 'Go to first page' , 'Grab Data'];
   var name = NAMES[app.step-1];

   console.log("============= Execute [ "+ app.step +" ] "+ name +' ' +JSON.stringify(app));
   console.log("=======================================================")
   FN[app.step+""]();	
}

FN["1"] = function(){
var url = 'https://www.scbeasy.com/v1.4/site/presignon/index.asp'
Promise.resolve()
  .then(() =>
	   nightmare
       .goto(url)
	   .type('[name="LOGIN"]',Username) 
	   .type('[name="PASSWD"]',Password) 
	   .click('[name="lgin"]')
       .wait('.welcome-tx')
	   .evaluate(function(){
 		  LoadHM('1','acc/acc_mpg.aspx');
       })
       .wait(2000)
	   .wait('#DataProcess_ltLinkEditNicknameClick') 
	   .evaluate(function(){
		  var links = document.querySelectorAll('.lnk_th_blk_11');
		  var ret   = []; // return array of account_no to nodejs

		   for(var i=0;i<links.length;i++){ ret.push(links[i].text); }
		   return ret;
	   })
	   .then(function(accList){ app.acc_count = accList.length ; console.log("Finished step1 , Found "+app.acc_count+" accounts"); 
	      for(var i=0;i<accList.length;i++)
		     {
				accounts.push({account_no : accList[i], data : []});  
			 }
	   })
	   .then( () => {app.step = 2;  execute();  } )
	   .catch( (ex) => { console.log(ex); handleError("Invalid Login or TOC")  })
   )
}
//--------- End FN1

FN["2"] = function()
{
	chooseAccount(app.AI, app.acc_count)	
}

FN["3"] = function()
{
  chooseMonth(app.MI, TotalMonth,  app.acc_count, true)	
}



FN["4"] = function()
{
  nightmare
  .evaluate(()=>{
	  var ret = {data:[], hasNext:false};

	  // set "hasNext"
	  var links    = document.querySelectorAll('#DataProcess_GridView a');
      links.forEach(function(ele){
	      if(ele.innerText=='Next')
		    {ret.hasNext = true;}
	  });
	  
	  // set "data"
	  var t = document.getElementById("DataProcess_GridView") 	
	  if(t!=null) {
	  	    var fields = ['date', 'time',	'desc', 	'via',	'note', 	'cheque'	, 'withdraw',	'deposit','balance'];
			var to_obj = function(row){
	            var ret = {};
	            for(var i=0;i<9;i++)
	               {
	               	   
	               	   var c = row.cells[i];
	               	   ret[ fields[i] ] =  c?c.textContent:"n/a";
					   ret.isHeader = isNaN(parseInt(ret.time)); 
	               }
	            return ret;   
			}  

			var arr = [].reduce.call(t.rows, function(ret,row){ var obj = to_obj(row); if(!obj.isHeader)ret.push(obj);  return ret } , []);
			ret.data = arr; 
	  }
	  return JSON.stringify(ret);
  })
  .then((json)=>{
     var res = JSON.parse(json);
     console.log('received additional stmt '+res.data.length+' entries');
	 res.data.forEach(function(row){
	    accounts[app.AI].data.push(row);
	 })
	//console.log("app_data.length = "+app_data.length);  
	if(res.hasNext){
	   clickNextStatementPage();
	}
	  else {
		//Finished Loading selected month data
        if(++app.MI <= TotalMonth){
		  app.step = 3;	 // next month
		}else if(++app.AI < app.acc_count){
		    app.MI   = 1 // next account, 1st month
		    app.step = 2;
		}else {
		    app.step = 5;
		}
		 execute(); 


	}
       
	 
  })
  .catch( (ex) => { console.log(ex); handleError("Error In Step4")  })
}

FN["5"] = function()
{
   console.log("DONE found "+ accounts.length + " accounts.") 
   var data = {success:true, job_type : 'scb', key : Key,  data : accounts}
   saveToRedis(Key, JSON.stringify(data))
}

function clickNextStatementPage()
{
  nightmare
  .evaluate(()=>{

	  //remove Footer
      var elem = document.getElementById('Footer');
      elem.parentNode.removeChild(elem);
	  
	  var links    = document.querySelectorAll('#DataProcess_GridView a');
	  var next = null;

      links.forEach(function(ele){
	      if(ele.innerText=='Next')
		    {next = ele;}
	  });

	  next.click();
  })
  .wait('#Footer')
  .then(() => {
	console.log("Finished opening next statement")  
    FN["4"](); 
  })	
  .catch( (ex) => { console.log(ex); handleError("Error innext statement")  })
}

function chooseAccount(i, count){
  console.log('ChooseAccount #' + i);
  var my_acc_url = 'https://www.scbeasy.com/online/easynet/page/acc/acc_mpg.aspx';
  
  nightmare	
   .evaluate(() => {
	   var myacc = document.querySelector('#SubMenuPanel a')
	   console.log(myacc)
	   myacc.click()
   })
   .wait(2000)
   .wait('#DataProcess_ltLinkEditNicknameClick') 	
   .evaluate(function(_i){
    console.log("selecting view link")   
	//click selected account link 
    var acc_links = document.querySelectorAll('.lnk_th_blk_11');
	   console.log("acc_link#"+ (_i+1)  + '/' + acc_links.length)
	  acc_links[_i].click();   
  }, i)
  .wait(2000)
  .wait('#DataProcess_Back_LinkButton , #DataProcess_Link3') //we may see either a [back btn] or [Historical Statement] link
  .evaluate(() => {
	var histLink = document.getElementById('DataProcess_Link3')  
    //console.log('hitsoty Link = '+ histLink);
	if(histLink==null)
	  { //console.log('Click back to myaccount'); 
		document.getElementById('DataProcess_Back_LinkButton').click();  
	  } //to myaccount
	  else
	  { 
		  //console.log('Click history link');
		  histLink.click(); 
	  } 
  })
  .wait(2000)
  .wait('#DataProcess_ddlMonth , #DataProcess_ltLinkEditNicknameClick')	
  .evaluate(() => {
	 var month = document.getElementById('DataProcess_ddlMonth');
	 
	 //just return : if account is inactive 
	 if(month==null){ console.log('No month dropdown for this account'); return 0;}
     else return month.length
   })	
  .then((month_count) => {
	   console.log("month_count = "+month_count)
       if(month_count>0){
		  app.step = 3;
		  app.MI = 1;  
	   }
	    else {
		  app.step = 2;
		  app.AI = app.AI+1	
	      app.MI = 1;
	   }
	   execute();  
  })
  .catch( (ex) => { console.log(ex); handleError("Error In ChooseAccount")  })

}
//----- END 

function chooseMonth(m, month_count, a, chooseEmptyMonth){
	var M = m ;	  
    var nm = null;
	
	
	if(chooseEmptyMonth)
	{
	  nm =	nightmare
		.evaluate(() =>{
			//remove Footer
			var elem = document.getElementById('Footer');
			elem.parentNode.removeChild(elem);

			console.log("opening First tab")
			//$('#DataProcess_Link1').click();
			__doPostBack('ctl00$DataProcess$Link1','')
		})
		.wait('#Footer') 
		.evaluate(() =>{
			//remove Footer
			var elem = document.getElementById('Footer');
			elem.parentNode.removeChild(elem);

			//open Historical Tab
			console.log("opening historicial tab")
			//$('#DataProcess_Link3').click();
			__doPostBack('ctl00$DataProcess$Link3','')
			
		})
		.wait('#Footer') 
		.then(() => {
		   console.log('Finished choosing EMPTY MONTH instead of #'+m)
		   setTimeout(function(){	 
		   chooseMonth(m, month_count, a, false)
		   },5000);
			
		})
		.catch( (ex) => { console.log(ex); handleError("Error In ChooseMonth")  })
	}
	else
	{
	  nm =	nightmare
		.evaluate((_m) =>{
			
			console.log('choose Month#'+_m);
			//remove Footer
			var elem = document.getElementById('Footer');
			elem.parentNode.removeChild(elem);

			var mm = document.getElementById('DataProcess_ddlMonth');
			mm.selectedIndex = _m
			setTimeout('__doPostBack(\'ctl00$DataProcess$ddlMonth\',\'\')', 0)
		}, M)
		.wait('#Footer') 
		.then(() => {
		   console.log('Finished choosing month #'+m)
		   setTimeout(function(){	 
		   app.step = 4
		   execute();
		   },5000);
			
		})
		.catch( (ex) => { console.log(ex); handleError("Error In ChooseMonth")  })
	
	}
}



execute();

