#!/usr/bin/env lua
cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
redis = require 'redis'
common = require "common"
exec = require'resty.exec'
prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
-- session = require "resty.session".open({secret = "4321"})
rc = redis.connect('127.0.0.1', 6379)
body = nil

function rand(len)
    local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
    local t = {}
    local cnt = 0
     
    local i = 1
    repeat
        local f = assert (io.popen (cmd, 'r'))
        local line = f:read('*all'):match("%d+")
        f:close()
        cnt = cnt + #line
        table.insert(t, line)
    until cnt>len
             
    local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
    return ret
end


if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body  = {} 
end

fullname = body.fullname
id_field1 =  rand(4) .. tostring(os.time())
id_field2 =  rand(4) .. tostring(os.time())
-- id_field1 = tonumber(string.sub(id_field1,1,13))
-- id_field2 = tonumber(string.sub(id_field2,1,13))
id_field1 = tonumber(id_field1)
id_field2 = tonumber(id_field2)
-- signer and fields
data = {
    originalFile = "ใบเสนอราคา.pdf",
    pdfFile = body.id,
    status = 0,
    id = body.id,
    imageFiles = {{ fileName = "pdf288707161582709294.png", aspectRatio = 0.7071571143427431 }},
    width = 0,
    height = 0,
    signers = {{ name = fullname , email = body.email, position_id = "", needToSign = true, eKyc = false, usePassword = false, password = "", title = "", order_set = "", countfildePeople = 2 }},
    fields = { 
        { id = id_field1, xp = 62.2600074708726, yp = 77.85585040501226, scale = 100,status = 0, time = 0, email = body.email, n = 1, field_no = "field1", page = 0, toolName = "Signature", toolData = "Signature", signOrder = 1, is_bold = false, is_italics = false, is_underline = false },
        { id = id_field2, xp = 21.718909665323064, yp = 78.36451954028617, scale = 100, status = 0, time = 0, email = body.email, n = 2, field_no = "field2", page = 0, toolName = "Name", toolData = "", signOrder = 1, is_bold = false, is_italics = false, is_underline = false, styleFont = "THSarabunNew", fontSize = "16" } 
    },
    subject = "Please sign ",
    msg = "",
    owner = body.email ,
    current_order = 1,
    use_signing_order = false,
    owner_name = fullname,
    imageMerge = "pdf288707161582709294.png",
    is_template = false,
    document_type_id = "",
    compcodex = body.CompCodex,
    }


-- conn = mongo()
-- conn:set_timeout(5000)
-- ok, err = conn:connect('127.0.0.1',27017)
-- local session = require "resty.session".start{ secret = "4321" }
-- myusername = session.data.username
-- if body.email == myusername and body.ekyc_key == "1234" then
--    list = { list_file = {"pdf288707161582709294.png"}, list_file_pdf = {"288707161582709294.pdf"} }

    -- before_name = "1582605610.pdf"
    -- after_name = tostring(os.time()) .. ".pdf" 
    
    -- local copy_pdf = "cp /home/creden/work/app/client/face/images/card/" .. before_name .. " /home/creden/work/app/client/face/images/card/" .. after_name  
    -- print(copy_pdf)

    -- print(cjson.encode(data))

   
    
--     local cmd = "lua /home/creden/work/app/server/save_edoc_company.lua " .. data

-- else
--     success = false
-- end
ret = { success = true, data = data }
print(cjson.encode(ret))