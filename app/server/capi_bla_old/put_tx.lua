#!/usr/bin/env lua
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false


local doc_id = arg[1] or '123'
local file = arg[2] or '/home/creden/work/app/client/face/images/card/1535789078-stamp_timestamped.pdf'
local file_hash = nil

local res, err = prog('md5sum', file)
if res.stdout then
    file_hash = string.match(res.stdout, "%w+")
	--print(file_hash)
else
	os.exit(1)
end


prog.stdin = doc_id .. '_' .. file_hash
local res, err = prog('md5sum')
if res.stdout then
    local hash = string.match(res.stdout, "%w+")
	print(hash)
else
	os.exit(1)
end
