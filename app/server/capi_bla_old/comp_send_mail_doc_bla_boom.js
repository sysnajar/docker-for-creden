var mailer = require("nodemailer");
send_mail()


function send_mail() {
  process.env.HOST_NAME = "https://hjkl.ninja"
  var logo = process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
//  var bgImage = process.env.HOST_NAME + "/bla/images/bg_bla.png"
  var btn_bla = process.env.HOST_NAME + "/bla/images/btn_bla.png"
  // var logo = "https://www.bangkoklife.com/online/assets/images/bla_logo_2021.svg"
  var bg = "#1667B2"
  var wTable = "100%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"
  var msg = process.argv[3]
  var private_mail = process.argv[4]
  var doc_id = process.argv[5]
  var owner_email = process.argv[6]
  var owner_name = process.argv[7]
  var subject = process.argv[8]
  var link = process.env.HOST_NAME + "/bla/#/document?id=" + doc_id + "&signer=" + process.argv[2]
  var secret_level = process.argv[9] || 1
  var secret_text = ""
  if (secret_level == 4) { secret_text = "ความลับที่สุด (Top Secret)" }
  else if (secret_level == 3) { secret_text = "ความลับมาก (Secret)" }
  else if (secret_level == 2) { secret_text = "ความลับ (Confidential)" }


  var html2 = ``
  if (private_mail != '') {
    html2 = `<div style="color:#2F80ED">
    &nbsp;&nbsp;&nbsp;<b>Private Message/ข้อความส่วนตัว:</b>
          <br>&nbsp;&nbsp;&nbsp;
          "`+ private_mail + `"
        </div>
         <br><br>`
  } else {
    html2 = ``
  }

  var html_secret = ``
  if (secret_level > 1) {
    html2 = `<div style="font-size:10px;">
    &nbsp;&nbsp;&nbsp;<b>Document Classification/ระดับความลับของเอกสาร :</b>
          "`+ secret_text + `"
        </div>
          <br>`
  } else {
    html_secret = ``
  }

  var html = `
    
    <html lang="th-TH" dir="ltr">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
      <link rel="icon" type="image/x-icon" href="images/Creden_logos_ICON_2-Tones.ico">
    </head>
    <!-- <body style="background-image: url('`+ bgImage +`');height:100%;"><br> -->
    <!-- <body background ="`+ bgImage +`"><br> -->
    <body><br>
      <div style="background-color: #0058A9;height: 350px; float: left;"></div>
        <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 50px 50px 50px;width: auto;border-radius: 15px;">
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="`+ logo +`" alt="logo website" style="width: 300px;margin-top: 30px;">
            </td>
        </tr>
        <tr>
          <td style="width: 100%;color: #000;padding: 0px 30px 30px 30px;margin-top: 30px;">
            <div style="font-size: 16px;">
            &nbsp;&nbsp;&nbsp;<label style="font-weight: bold;">Document ID/รหัสเอกสาร :</label>  ${doc_id}
            </div>
            <div style="margin-top:15px;font-size: 16px;">
            &nbsp;&nbsp;&nbsp;<label style="font-weight: bold;">From/จาก :</label> ${owner_name} ( ${owner_email} )
            </div>
            <div style="margin-top:10px;font-size: 16px;">
            &nbsp;&nbsp;&nbsp;<label style="font-weight: bold;">Message/ข้อความ :</label> ${msg}
            </div>
            <div>
            ${html2}
            ${html_secret}
            </div>
            </td>
        </tr>
        <tr>
          <td style="display: flex;justify-content: center;">
              <div style="background: #0058A9;width: 95%;display: block;padding-bottom: 115px;">
                  <div style="text-align: center ;color: white; ">
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                  <img src="`+ process.env.HOST_NAME +`/bla/images/mail/mail_comp.png">
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;">
                  &nbsp;&nbsp;&nbsp;Document has been completed by all participants&nbsp;&nbsp;&nbsp;
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;">
                  &nbsp;&nbsp;&nbsp;เอกสารถูกลงนามเสร็จสิ้นแล้ว&nbsp;&nbsp;&nbsp;&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                    <br>&nbsp;
                  </div>
                  <div style="text-align: center;">
                    <a href="`+ link + `" style="text-decoration: none;color: #000;background: #FFF;padding: 15px 30px;font-size: 16px;"> 
                    <img src="`+ btn_bla +`">
                  </a>
                  </div>
                  <br>&nbsp;
                  <br>&nbsp;
                  <br>&nbsp;
              </div>
          </td>
        </tr>
        </table>
        </body>
        </html>`


 var smtp = {
    host: 'smtp.gmail.com', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      user: 'credenmailer@gmail.com', //user account
      // pass: 'creden2018' //user password
      pass: 'dnkrakkaxnmnakpr'
    }
  };
//   var smtpTransport = mailer.createTransport(smtp);
//   var mail = {
//     from: 'Bangkok Life - eSinature', //from email (option)
//     to: process.argv[2], //to email (require)
//     cc: '',
//     subject: subject, //subject
//     html: html//head + body + end
//   }

var smtpTransport = mailer.createTransport(smtp);
var mail = {
  from: 'Bangkok Life - eSinature', //from email (option)
  to: process.argv[2], //to email (require)
  cc: process.argv[10],//'chaluemchat@gmail.com,sysnajar@gmail.com'
  subject: 'Sign Completed:' + subject, //subject
  html: html//head + body + end
}
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


