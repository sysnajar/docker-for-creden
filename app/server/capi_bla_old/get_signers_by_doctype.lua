#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {document_type_id = '0'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
    local col_doc_type = db_dopa:get_col("document_type")
    local doc_type, err_doc_type = col_doc_type:find_one({_id=object_id.new(convertHexStringToNormal(body.document_type_id))})
    if doc_type then
        doc_type._id = tostring(doc_type._id)
        doc_type.company_id = tostring(doc_type.company_id)
        ret.data = doc_type
    end
end

print(cjson.encode(ret))