cjson = require 'cjson.safe'
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then
   print = ngx.say
   ngx.req.read_body()
end

json = ngx.req.get_body_data()
ngx.log(ngx.INFO ,'json', json)
t = cjson.decode(json)

local key = t.upload_progress_key
local value = rc:get(key) or '50'

local ret = {success = true , percent = tonumber(value)}


-- ret2 
local key2 = key .. '_result'
local value2 = rc:get(key2) 
if(value2)then
    rc:del(key2) 
   ret.finished = true
   local t2 = cjson.decode(value2)
   for k,v in pairs(t2) do
      ret[k] = v
   end
else
   ret.finished = false
end
--end ret2

print(cjson.encode(ret))
