local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {token = args[1],otp = args[2]} 
end

token = body.token
otp = body.otp
key = 'otp_'..token..'_'..otp
check = rc:get(key)
if check then
	conn = mongo()
	conn:set_timeout(1000)
	ok, err = conn:connect('127.0.0.1',27017)
	if not ok then
	    ret = {success = false, error_msg = err}
	else
	    db = conn:new_db_handle("dopa")
	    col={}
	    if body.login_service == "esig" then
          col = db:get_col("esig_user")              
        else
          col = db:get_col("company")
        end
	    s = col:find_one({adminEmail=body.adminEmail})
	    if s then
			s._id = nil
	        selector = {adminEmail=body.adminEmail}
	        update = {["$set"] = {activeStatus = 4, comTel = body.tel}}
	        i, err = col:update(selector, update, 0, 0, true)

	        local session = require "resty.session".start{ secret = "4321" }
              ngx.log(ngx.NOTICE,"C SET SESSION TO " .. tostring(selector.adminEmail))
		      session.data.username = selector.adminEmail
		      session.data.custCode = selector.adminEmail
	          session:save()
	          
	        if not i then
	            ret = {success = false, error_msg = err}
	        else
	            ret = {success = true, error_msg = "complete", data = s}
	            rc:del(key)
	        end
	    else
	        ret = {success = false, error_msg = "not valid code1"}
	    end
	end
else
	ret = {success = false, error_msg = "not valid code2"}
end
print(cjson.encode(ret))
