#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}
--1
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {company_id = '5c2d92cc778fdc6a5f91c0e1', doc_type = {}, mode = 'add'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

-- CHECK SESSION
if not session.data.company_id then
    print(cjson.encode({success = false, error_msg = "no company", error_code = '001'}))
    return 0
else
    body.company_id = session.data.company_id
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
    local col_doc_list = db_dopa:get_col("document_type")
    data.template_name = body.doc_type.template_name
    data.create_by = body.doc_type.create_by
    data.signers = body.doc_type.signers
    data.remark = body.doc_type.remark
    data.use_signing_order = body.doc_type.use_signing_order
    data.company_id = object_id.new(convertHexStringToNormal(body.company_id))

    if body.mode == 'add' then
        data.prefix = body.doc_type.prefix
        data.init_number = (tonumber(body.doc_type.init_number) or 0)
        data.current_number = data.init_number
        i, err_i = col_doc_list:insert({data})
    elseif body.mode == 'edit' then
        i, err_i = col_doc_list:update({_id=object_id.new(convertHexStringToNormal(body.doc_type._id))}, {["$set"] = {template_name = data.template_name, prefix = data.prefix, use_signing_order = data.use_signing_order, remark = data.remark, signers = data.signers}}, 0, 0, true)
    elseif body.mode == 'delete' then
        i, err_i = col_doc_list:remove({_id=object_id.new(convertHexStringToNormal(body._id))})
    elseif body.mode == 'get' then
        i, err_i = col_doc_list:find({company_id=object_id.new(convertHexStringToNormal(body.company_id))})
    else
        ret.error_msg = 'Invalid mode'
        print(cjson.encode(ret))
        return 0
    end

    if not err_i then
        ret.success = true
        if body.mode == 'get' then
            local get_table = {}
            for k, v in i:pairs() do 
                v._id = tostring(v._id)
                v.company_id = tostring(v.company_id)
                if v.current_number then
                    v.current_number = v.prefix..'.'..v.current_number..'/'..(tonumber(os.date("%Y")) + 543)
                end
                table.insert(get_table, v) 
            end
            ret.data = get_table
        end
    else
        ret.error_msg = err_i
    end
    

end

print(cjson.encode(ret))