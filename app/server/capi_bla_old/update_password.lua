#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local args = nil
local scrypt = require "resty.scrypt"



if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    args = cjson.decode(ngx.req.get_body_data())
else
    args   = {email="kitchinvuttinunt@gmail.com"} 
end
    user_data = {}
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
        print(cjson.encode(user_data))
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        ret = {}
        selector = {adminEmail=args.adminEmail}
	
        s = col:find_one(selector) 
        
        if (s and s.hashedPassword and scrypt.check(args.password, s.hashedPassword))then 
			
			local new_hash = scrypt.crypt(tostring(args.adminPassword))
            update_cmd = {["$set"] = {hashedPassword=new_hash}}
            u, err = col:update(selector, update_cmd, 0, 0, true)
            if not err then
               

                local update_cmd = {["$set"] = { 
                    created_status = true
                }}
                n,err = col:update({ adminEmail = args.adminEmail }, update_cmd, 0, 0, true)
    
                ret = {success = true, error_msg = "complete"}
            else
                ret = {success = false, error_msg = err}
            end
        else
            ret = {success = false, error_msg = "Invalid password"}
        end
        print(cjson.encode(ret))
    end

   
