#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {success=false}
local conn = nil
prog.timeout = 1000 * 300
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    --header = ngx.req.get_headers()
else
    body = {action='company'}
end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    local ok,err = conn:connect('127.0.0.1',27017)
    local result = false
    if err then
        result = false
        ret = {success = result, msg = err}        
    else
        result = true
    end
    return result
end

-- function chk_apiKey(apiKey)
--     local result = false
--     local db = conn:new_db_handle("dopa")
--     local col = db:get_col("company")
--     local s = col:find_one({apiKey = apiKey})   
--     if s then
--         result = true  
--     end
--     return result
-- end

function CheckCompany()
    local db = conn:new_db_handle("db_dipchip")
    local col = db:get_col("mt_company")
    local doc = col:find_one({company_code=body.company_code})
    if (doc) then
        ret = {success = true, company_code=doc.company_code, lock_sn=doc.lock_sn}
    else
        ret = {success = false, msg = 'Company code not exists'}        
    end
end

function CheckSN()
    local db = conn:new_db_handle("db_dipchip")
    local col = db:get_col("mt_cardreader")       
    local t, err = col:find_one({sn=body.sn})
    if (t) then
        ret={success=true}
    else
        ret = {success=false,msg='Serial number not exists'}        
    end
    return result
end

--{"company_code" : "*", "logtime" : null, "sn" : null, "ip" : null }
function AddLogDipchip()
    local db = conn:new_db_handle("db_dipchip")
    local col = db:get_col("log_dipchip")
    local doc = {txid=body.txid,
        company_code=body.company_code,
        sn=body.sn,
        ip=body.ip,
        logtime=bson.get_utc_date(os.time() * 1000)}

    local i, err_i = col:insert({doc})
    if (i) then
        ret = {success = true}
    else
        ret = {success = false, msg = err_i}
    end
end

if (body.action ~= nil) then
    if connect_mongo() then
        if (body.action == 'CheckCompany') then --body = {action='CheckCompany',company_code='orisma'}
            CheckCompany()
        elseif (body.action == 'CheckSN') then   --body = {action='CheckSN',sn='123456'}
            CheckSN()
        elseif (body.action == 'AddLogDipchip') then  --body = {action='AddLogDipchip', txid="xxxx", company_code='orisma', sn='123456', ip='192.168.0.1'}            
            AddLogDipchip()
        end
    end

else
    ret.success = false
    ret.msg = 'Not data'
end

 -- print(findxx3)
print(cjson.encode(ret))
