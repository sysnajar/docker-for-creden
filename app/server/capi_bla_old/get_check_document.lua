#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
-- local bson = require "resty-mongol.bson"
-- local object_id = require "resty-mongol.object_id"j
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    local h = ngx.req.get_headers()

    -- set and get key ip for limit user defense attack
    local ip_key = 'ips_'..h["x-real-ip"] or nil
    local counter = rc:incr(ip_key)

    if(counter == 1)then
        rc:expire(ip_key,60 * 60 * 24)
    end

    if( counter >= 300)then
        ret.error_msg = "session expired" .. tostring(counter) ..' ' ..ip_key
        print(cjson.encode({ret}))
        ngx.exit(401)
    end

    body = cjson.decode(ngx.req.get_body_data())

    function check_number_doc( text )
        local text = tonumber(text)
        if(text == nil or text == "")then
            ret.error_msg = "doc id missing"
            print(cjson.encode(ret))
            ngx.exit(401)
        end
        return text
    end

else
    function check_number_doc( text )
        local text = tonumber(text)
        if(text == nil or text == "")then
            ret.error_msg = "doc id missing"
            print(cjson.encode(ret))
            os.exit(1)
        end
        return text
    end

   body   = {doc_id = '158590912390'}
end

local check_data_json = check_number_doc(body.doc_id)

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_edoc = conn:new_db_handle("edoc")
    local col_edoc_list = db_edoc:get_col("edoc_list")

    local get_check_doc = col_edoc_list:find_one({id=body.doc_id})
    if(get_check_doc)then
        local data = {}
		data.owner = get_check_doc.owner or ''
		data.createdDtm = get_check_doc.createdDtm or ''
		data.status = get_check_doc.status or 'N/A'
		data.complete_dtm = get_check_doc.complete_dtm or 'N/A'
		data.id = get_check_doc.id
		ret.success = true
		ret.data = data

    else
        ret.error_msg = "missing document id"
    end
    
end

print(cjson.encode(ret))
