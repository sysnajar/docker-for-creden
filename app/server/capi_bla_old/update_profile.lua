#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = 'kjunchean@gmail.com', password = '1'} 
end

if body.email ~= '' then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("test")
        col = db:get_col("user")
        s = col:find_one({email=body.email})
        if s then
            local picture_tmp = body.profilePicture
            body._id = nil
            body.password = s.password
            body.activated = s.activated
            body.profilePicture = s.profilePicture
            --body.score = (tonumber(body.age) or 0) * 10 --s.score
            local cmd = '~/scripts/update_score.lua ' ..body.email.. ' DEM,CARE,CRIM &'
			os.execute(cmd)
            update = body
            u, err = col:update({email=body.email}, update, 0, 0, true)
            if not err then
                update.profilePicture = picture_tmp
                ret = {success = true, error_msg = "complete", data = update}
            else
                ret = {success = false, error_msg = err}
            end
        else
            ret = {success = false, error_msg = "Not a valid email"}
        end
    end
else
    body.score = (tonumber(body.age) or 0) * 10 --s.score
    ret = {success = true, error_msg = "complete", data = body}
end

print(cjson.encode(ret))
