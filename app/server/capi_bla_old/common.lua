return {

	 validate_name = function( text , assert_msg )
		local text2 = string.match(text,'[%a%d]*') 
		local ret = (#text == #text2)

		if(assert_msg and not ret and ngx)then ngx.exit(400) end
		return ret
	 end,

	 validate_selfie_img = function( text , assert_msg )
		if(#text > 20)then
			ngx.exit(400)
			return ret
		end

		text = string.gsub(text,'%$','',20)
		text = string.gsub(text,'%(','',20)	
		local text2 = string.match(text,'[%d]*')  

		local ret = text2
		if(assert_msg and not ret and ngx)then ngx.exit(400) end
			return ret
	 end,

	 validate_faceId1 = function( text , assert_msg )
		if(#text > 40)then
			ngx.exit(400)
			return ret
		end

		text = string.gsub(text,'%$','',40)
		text = string.gsub(text,'%(','',40)
		
		-- local text2 = string.match(text,'[%w]*_[%w]*.[%w]*')  

		local text2 = string.match(text,'[%w]*-[%w]*-[%w]*-[%w]*-[%w]*')  
		-- [\w]+-[\w]+-[\w]+-[\w]+ 
		local ret = text2
		if(assert_msg and not ret and ngx)then ngx.exit(400) end
			return ret
	 end,


		validate_alphanumeric = function( text , assert_msg )
		   local text2 = string.match(text,'[%a%d%.%@%_*]*')   

		   local ret = (#text == #text2)
		   if(assert_msg and not ret and ngx)then 
			   ngx.log(ngx.INFO, assert_msg) 
			   ngx.exit(400) 
		   end

		   return ret
        end,

		validate_number = function( text , assert_msg )
			local text2 = string.match(text,'%d*')   
			local ret = (#text == #text2)
 
			if(assert_msg and not ret and ngx)then ngx.exit(400) end
			return ret
		 end,

		validate_sign_type = function( text , assert_msg )
			local ret = (text=='draw' or text=='gen' 
			or text =='draw_initial' or text == 'gen_initial')
 
			if(assert_msg and not ret and ngx)then ngx.exit(400) end
			return ret
		 end,
	


	    err = function(text)
          print(cjson.encode({success = false, error_msg = text}))
          ngx.exit(ngx.HTTP_OK)
		end,

        trim = function(s)
              return (s:gsub("^%s*(.-)%s*$", "%1"))
           end,

	tprint = function(t, _prefix)
	local fn = pairs
	if(t[1])then fn = ipairs end
	for k,v in fn(t) do

	  if(type(v)=='table')then
		common.tprint(v,k)
	 else
	    if(_prefix) then
		   print('',_prefix, k, v)
		else
		   print(k , v)
	    end
	  end

	end
	end,
--group signing function
is_in_group = function(t, email)
	for _,person in ipairs(t) do
	   if(person.email==email) then return true end
	end
	return false
end,


init_post = function(default_t) 
local ret = default_t

exec = exec or require'resty.exec'
prog = prog or exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false

if(ngx)then  
    print = ngx.say
    ngx.req.read_body()
	local json = ngx.req.get_body_data()
    local cjson = require 'cjson.safe'
	ngx.log(ngx.NOTICE, json)
	ret = cjson.decode(json)
end
return ret
end,

read_text = function (filename)
	local ngx_io = require "ngx.io"
	local file, err = ngx_io.open(filename, "r")
	assert(file and not err)

	local t = {}
	-- iterates the file by reading one line every time.
	for line in file:lines() do
		table.insert(t, line)
	end

	local ok, err = file:close()
	return table.concat(t,'\n')
end,

	split = function ( str, exp )
	    t = {}
	    for i in string.gmatch(str, exp) do
		table.insert(t,i)
	    end
	    return t
	end,

	sum = function(t)
	local ret = 0
	local fn  = t[1] and ipairs or pairs

	 for k,v in fn(t)do
		local val = (type(v)=='number') and v or 0 
		ret = ret + val
	 end

	return ret
	end,

	removeStr = function( line )
	    res = ''
	    for match in string.gmatch(line, "[^:]+") do
	      res = match
	    end
	    res = res:gsub('"', "")
	    res = res:gsub(',', "")
	    res = res:gsub("%d.", "")
	    res = res:gsub("%dd.", "")
	    res = res:gsub("/", "")
	    return res
	end,

	    set_cookie = function(t)
	       local ck = require 'resty.cookie'
	       local cookie, err = ck:new()
	       cookie:set(t)
	    end,

		get_all_cookies = function()
		  local ck = require 'resty.cookie'
		  local cookie, err = ck:new()
		  return cookie:get_all()
		end,

		get_cookie = function(k)
		  local ck = require 'resty.cookie'
		  local cookie, err = ck:new()
		  return cookie:get(k)
		end,

		restore_login_from_cookie = function()
            ngx.log(ngx.NOTICE, 'restore_login_from_cookie 1')

            local token = common.get_cookie('login_token')
			if(token)then
               ngx.log(ngx.NOTICE, 'restore_login_from_cookie 2 ' .. token)
			   local h = ngx.req.get_headers()
			   local usr = common.login_from_persistence_token(token,
			   h['x-real-ip'] or '1.1.2.2')
               ngx.log(ngx.NOTICE, 'restore_login_from_cookie 3 ' .. usr)

			   local ret = (usr ~= nil)
		       if(ret)then
    		      local session = require "resty.session".start{ secret = "4321" }
    			  session.data.username = usr
                  session.data.custCode = usr
                  session:save()
			   end

			   return ret, usr
		    end
		
		    return false
		end,


		login_from_persistence_token = function(token, ip)
	      assert(token and ip)
		  local ctx   = ctx  or common.mongo_ctx()
		  local col = ctx.col['dopa.login_token']
		  
		  local rec = col:find_one({token = token, valid = true}) --todo query using exp
          if(rec and rec.username)then
		    local _id = rec._id		
			col:update({_id = _id}, {['$set']={valid = false, note = 'used'}})

            local bson  = bson or require "resty-mongol.bson"
			local new_token = common.create_persistence_token(rec.username,
			                  ip, bson.get_utc_date(rec.exp))
			return rec.username, new_token
		  end

		  return nil
		end,

		gen_persistence_token = function(username, ip, exp)
	      assert(username and ip)
		  local raw   = username .. '.' .. common.rand(20)
		  local token = ngx.md5(raw)
		  local t    = {
			token = token,
	  		username = username,
			ip    = ip,
            exp   = exp,
			valid = true
	  	  }
		  return t
		end,

		create_persistence_token = function(username, ip, _exp)
          local exp = _exp

		  if(not exp)then
          local bson  = bson or require "resty-mongol.bson"
                exp   = bson.get_utc_date(os.time() * 1000)
		  end

		  local t = common.gen_persistence_token(username, ip, exp)
		  local ctx   = ctx  or common.mongo_ctx()
		  local col = ctx.col['dopa.login_token']
		  local res = col:insert({t})
		  common.send_persistence_token(t)	
         return t,res 
		end,

		send_persistence_token = function(t)
		 local c = {
		  key = "login_token", value = t.token, path = "/",
		  secure = true, httponly = true,
		  max_age = 60*60*24*14
		 }
		 common.set_cookie(c)
		end,

		rand  = function(len)
		local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
		local t = {}
		local cnt = 0
		 
		 local i = 1
		 repeat
			 local f = assert (io.popen (cmd, 'r'))
			 local line = f:read('*all'):match("%d+")
			 f:close()
			 cnt = cnt + #line
			 table.insert(t, line)
		 until cnt>len

				
		 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
		 return ret
		end,
   
		find_or = function (cfg, or_cond, _options)
		local mongo2 = require 'mongo'
		local client = mongo2.Client('mongodb://127.0.0.1')
		local collection = client:getCollection(cfg.db, cfg.collection)
		local ret = {}
		     or_cond.__array  =  true
		     local bson_arr   = mongo.BSON(or_cond)
		     or_cond.__array  =  nil
		     local bson_or    = mongo.BSON {['$or'] = bson_arr }
		 
			 for v in collection:find(bson_or, _options or nil ):iterator() do
			     table.insert(ret, v)	 
			     --print(v.JP_NO, v.PARTNER_MANAGER)
			 end 
		     return ret
		end,
		search_collections = function (db,collection_name,filter)
		local mongo = (mongo) and mongo or require "resty-mongol"
        local conn = mongo()
			conn:set_timeout(1000)
			ok, err = conn:connect('127.0.0.1',27017)
			if not ok then
			    return false,nil
			else
			    db = conn:new_db_handle(db)
			    col = db:get_col(collection_name)
			    
				list = col:find(filter)    
				--list = col:find({})    

			    if list then 
			        data = {}

			        for _i, v in list:pairs() do
			            v._id = tostring(v._id)
			            table.insert(data, v)
			        end

			         return true ,data
			    else
			         return false,nil
			    end
			end  
		end,

		test_init_ngx = function()
			if(ngx)then  
			    ngx.req.read_body()
			    print = ngx.say
				body  =  ngx.req.get_post_args()
			else
			    if(no_ngx)then no_ngx() end    
			end
        end,
        
        activity_history = function(docID ,owner, actionEN, actionTH, activityEN, activityTH, status, fileName, subject, msg, toolName, toolData, ipAddress)
            local mongo = (mongo) and mongo or require "resty-mongol"
            local bson = (bson) and bson or require "resty-mongol.bson"
            local conn = mongo()
            conn:set_timeout(1000)
            local ok, err = conn:connect('127.0.0.1',27017)
            if ok then
                local createDate = os.date("%x") 
                local createTime = os.date("%X")
                local createdDtm = bson.get_utc_date(ngx.now() * 1000) 
                local log = { docID = docID, owner = owner, fileName = fileName, subject = subject, msg = msg, actionEN = action, actionTH = actionTH, activityEN = activityEN, activityTH = activityTH, toolName = toolName, toolData = toolData, status = status, createDate = createDate, createTime = createTime, createdDtm = createdDtm, ipAddress = ipAddress }
			    local db = conn:new_db_handle('edoc')
                local col = db:get_col('activity_history')
                local i, err = col:insert({log}, nil, true)
            end
		end ,
		
		activity_history_action = function(ipAddress, username, actionEN, actionTH)
            local mongo = (mongo) and mongo or require "resty-mongol"
            local bson = (bson) and bson or require "resty-mongol.bson"
            local conn = mongo()
            conn:set_timeout(5000)
            local ok, err = conn:connect('127.0.0.1',27017)
            if ok then
                local createDate = os.date("%x") 
                local createTime = os.date("%X")
                local createdDtm = bson.get_utc_date(ngx.now() * 1000) 
                local log = { ipAddress = ipAddress, username = username, actionEN = actionEN, actionTH = actionTH, createDate = createDate, createTime = createTime, createdDtm = createdDtm }
			    local db = conn:new_db_handle('edoc')
                local col = db:get_col('activity_history')
                local i, err = col:insert({log}, nil, true)
            end
        end ,
        
		add_all = function (t1 , t2) --add all items in t2 to t1
        for _,v in ipairs(t2) do
           table.insert(t1, v)
		end
		return t1
		end,

map = function (fn , t)
local ret = {}
for _, v in ipairs(t) do
   table.insert(ret, fn(v))
end
return ret
end,

shallow_copy = function (src, _dest)
local ret = _dest or {}
for k, v in pairs(src) do
	ret[k] = v
end
return ret
end,

by_id = function (col, _id)
  if( type(_id)=="string" ) then
     _id = object_id.new(common.convertHexStringToNormal(_id))
  end

  local res = col:find_one({_id = _id})
  return res
end,

mongo_ctx = function()
    local ret = {}
    local mongo = mongo or require "resty-mongol"
	local conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
	--	assert(false)
    else
		ret.dopa = conn:new_db_handle("dopa")
		ret.edoc = conn:new_db_handle("edoc")
		local col = {}

		-- if k = xxx.yyy then col_name = yyy 
		-- if k = xxx     then col_name= xxx
		local mt = {__index = function(t, k)  
			local db = ret.edoc
			if(string.find(k, 'dopa.')==1) then db = ret.dopa end
			if(string.find(k, 'smartcity.')==1) then db = conn:new_db_handle("smartcity") end

			local i = string.find(k, '%.')
			local col_name = (i==nil) and k or (k:sub(i+1))
			return db:get_col(col_name)
		    end
		    }
	     
		 setmetatable(col, mt)
		 ret.col = col
		 ret.by_id = function(col_name, _id)	 
		 return common.by_id( ret.col[col_name], _id )
		 end
     end

    return ret		 
end,

convertHexStringToNormal = function( str )
ngx.log(ngx.NOTICE,"convertHexStringToNormal#" .. tostring(str) .. 'type#' .. type(str))
	
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end,

object_id = function( _id )
	if( type(_id)=="string" ) then
		_id = object_id.new(common.convertHexStringToNormal(_id))
	end
	return _id
end,

		edoc_get_signers_by_position = function(_id) 
           ngx.log(ngx.INFO,'ldd1 pos_id = '.. tostring(_id) )

           local  people =  common.edoc_get_persons_by_position(_id)

           ngx.log(ngx.INFO,'ldd1 people len = '.. #people )
		   
		   return common.map(function(u) 
			   local signer = {
			   name  = u.adminFisrtname or '',
			   email = u.adminEmail or '',
			   position_id = tostring(_id)
		      }
			  return signer
		   end, people)
		end,

		edoc_get_positions_id_by_email = function(email) 
		assert(email, 'Email can not be nil!!!')	
		local ret   = {}
		local ctx   = ctx or common.mongo_ctx()
        local members = ctx.col['dopa.company_members']:find({email=email})

	   for _, m in members:pairs() do
		    for _, _pos in ipairs(m.positions or {}) do
                table.insert(ret, tostring(_pos.position_id))     
			end --for pos
       end

		return ret
		end,

		edoc_get_persons_by_position = function(_id) 
		if(not _id or (_id==''))then return {} end	
		local ret   = {}
		local ctx   = ctx or common.mongo_ctx()

		if( type(_id)=="string" ) then
            ngx.log(ngx.NOTICE,"ldd1 edoc_get_persons_by_position#" .. tostring(_id))
			
		    _id = object_id.new(common.convertHexStringToNormal(_id))
		end
        
        local query = {positions={['$elemMatch']={position_id=_id}}}
        --local query = {}
        local members = ctx.col['dopa.company_members']:find(query)

       ngx.log(ngx.NOTICE,"ldd1 listing members")
	   local _cnt=0
	   for _, m in members:pairs() do
		    _cnt = _cnt+1 
            local u = ctx.by_id('dopa.esig_user', m.esig_user_id)
			if(u) then  table.insert(ret, u)  end
       end
       ngx.log(ngx.NOTICE,"ldd1 found ".. _cnt  .." members")

		return ret
		end,

		edoc_get_real_signers = function(doc) --returns actual signers even if some of the signer are specified using position_id
            ngx.log(ngx.NOTICE,"edoc_get_real_signers company_id#" .. tostring(doc.company_id))

			local ret = nil
			if(not doc.company_id) then 
			   return doc.signers
		   else
			   ret = {}
			   for _i, v in ipairs(doc.signers) do
                   if(v.email and v.email~="") then 
					   table.insert(ret, v) 
				   else
					   local all_persons = common.edoc_get_persons_by_position(v.position_id)
            		   ngx.log(ngx.NOTICE,"find ".. #all_persons .." persons for position_id#" .. tostring(v.position_id) .."(" .. tostring(v.title)  ..")")

					   ret = common.add_all(ret, common.map(function(u) 
					   local signer = common.shallow_copy(v) 
					   signer.name  = u.adminFisrtname or ''
					   signer.email = u.adminEmail or ''
            		   ngx.log(ngx.NOTICE," - " .. signer.name ..' (' .. signer.email ..')')
					   return signer
					   end, all_persons))


				   end
			   end --for
			end

			return ret
        end,
}
