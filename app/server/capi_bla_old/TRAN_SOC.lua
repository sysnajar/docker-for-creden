#!/usr/bin/env lua

function init()
if(#arg > 0  )then
  cjson = require 'cjson'
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379)
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(id)
	local key = 'PREP:DEM:' .. id
	local t1 = cjson.decode(rc:get(key))
	local t2 = {
	ID_sex = nil,
	ID_age = nil,
	ID_name    = t1.user.firstname,
	ID_lname   = t1.user.lastname,
	ID_dob     = t1.user.dob,
	ID_citino  = t1.kyc and t1.kyc.id_card.citizenIdF or nil,
	DOPA_death = t1.kyc and t1.kyc.death.valid or nil
	}

	return t2
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
