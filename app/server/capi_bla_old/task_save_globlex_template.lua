#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local redis = require 'redis'
local common = require "common"
local object_id = require "resty-mongol.object_id"

rc = redis.connect('127.0.0.1', 6379)
local body = nil


---------------------MONGODB FUNCTIONS----------------------------------

function convertHexStringToNormal( str )
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end

function by_id(col, _id)
  
  if(type(_id == string)) then
     _id = object_id.new(convertHexStringToNormal(_id))
  end

  return  col:find_one({_id = _id})
end

-------------------------------------------------------


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
end

     data = [[
{"pdfFile":"1544007982","id":"1544007982","imageFiles":[{"fileName":"pdf1544007941-0.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-1.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-2.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-3.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-4.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-5.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-6.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-7.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-8.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-9.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-10.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-11.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-12.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-13.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-14.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-15.png","aspectRatio":1.4159292035398},{"fileName":"pdf1544007941-16.png","aspectRatio":1.4159292035398}],
"current_order":1,"use_signing_order":false, "bc_url":false,"status":0} 
     ]]
	 local globlex_id = "5c063d9f86cff27897d5eeb5"

    doc = cjson.decode(data)
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        t_col = db:get_col("edoc_template")
        c_col = db:get_col("user_company")
		

        local company = by_id(c_col, globlex_id)

		doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 
		doc._company = company._id

        i, err = t_col:insert({doc}, nil, true)

    end
