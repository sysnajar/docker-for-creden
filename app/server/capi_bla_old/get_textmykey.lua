#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
rc = redis.connect( '127.0.0.1', 6379)
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body  = {} 
end

data = rc:get(body.text_key) or ''
print(cjson.encode({ success = true, text = data }))
