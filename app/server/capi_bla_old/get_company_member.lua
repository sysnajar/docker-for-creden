#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}
local data = {}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()    
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("company_members")
    i, err = col:find({ company_id = object_id.new(convertHexStringToNormal(body.company_id)) })

    if err then
        ret = { success = false, error_msg = "find member unsuccess", error_code = "2001" } 
    else

        local list = {}
        local col_esig_user = db:get_col("esig_user")
        local col_user_com = db:get_col("user_company")
        local col_group = db:get_col("group_sign")
        local myusername = session.data.username
        local myRole = "USER"
        local u_com, err_u_com = col_user_com:find_one({ _id = object_id.new(convertHexStringToNormal(body.company_id)) })

        for k, v in i:pairs() do 
            ngx.log(ngx.INFO, "Got AAAAAAAAAAAAAAAAAAAAA")
            local pos_t = {}
            v._id = tostring(v._id)
            v.company_id = tostring(v.company_id)

            if myusername == v.email then
                 myRole = v.role
            end

            if u_com.create_by == v.email then
                v.is_root = true
            else
                v.is_root = false
            end
            ngx.log(ngx.INFO, "Got BBBBBBBBBBBBBB")
            if v.esig_user_id then 
                ngx.log(ngx.INFO, "Got ccccccccccccccccc")
                local user, err_user = col_esig_user:find_one({ _id = v.esig_user_id })
            
                if user then
                    v.fName = user.adminFisrtname or ""
                    v.lName = user.adminLastname or ""
                    ngx.log(ngx.INFO, "Got dddddddddddddddddddd")

                    local g, err_g = col_group:find({ company_id = body.company_id, group_sign = {['$elemMatch']={ email = user.adminEmail }} })
                    local group = {}
                    for k2, v2 in g:pairs() do 
                        v2._id = nil
                        table.insert(group, v2.group_name)
                        ngx.log(ngx.INFO, "Got EEEEEEEEEEEEEEEEEEEEEE")
                    end
                        v.group = group
                else
                    v.fName = ""
                    v.lName = ""
                end
                    v.esig_user_id = tostring(v.esig_user_id)

                end
                ngx.log(ngx.INFO, "Got GGGGGGGGGGGGG")
                for k1, v1 in ipairs(v.positions or {}) do 
                    v1.position_id = tostring(v1.position_id)
                    table.insert(pos_t, v1.position_name) 
                end
                ngx.log(ngx.INFO, "Got HHHHHHHHHHHHHHHHHHHHHHHHHHH")
                v.positions_name = pos_t
                v.fav = false
                table.insert(list, v) 
            end
            ret = { success = true, error_msg = "find member success", error_code = "2002", data = list, myRole = myRole, is_root = is_root } 

        end
end
print(cjson.encode(ret))