#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end


function match_pos(doc, pos_id) 

  for i,signer in ipairs(doc.signers or {}) do
      if(signer.position_id and signed.position_id==pos_id)then
        return true
      end
  end

  return false
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else

	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list_draft")

  if(not body.email or body.email=='' or type(body.email) == 'userdata')then  
       print(cjson.encode({success = false, error_msg = "session expired"}))
	    ngx.exit(ngx.HTTP_OK)
  end	

	if(not session.data.username) then
		print(cjson.encode({success = false, error_msg = "session expired"}))
	  ngx.exit(ngx.HTTP_OK)
	else
		if(body.email~=session.data.username) then
			ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
			body.email = session.data.username
		end
	end	

  s = {}  
  -- query = { owner = body.email }
  query = { owner = body.email, status_draft_delete={ ['$ne'] = true} }

  s  = col:find(query)
	s2 = {} 
	for k, v in s:pairs() do 
    table.insert(s2, v) 
  end

	tmp = {}
	for k, v in ipairs(s2) do
    v._id = tostring(v._id)
    -- v._company = tostring(v._company)
    if v.createdDtm then
      v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
    end
      v.imageFiles = nil
      v.imageMerge = nil
      table.insert(tmp,v)
  end

  ret = { success = true, error_msg = "complete", data = tmp, cat = body.cat, query = query }
  print(cjson.encode(ret))
end
