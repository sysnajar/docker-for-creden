#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')

if(ngx)then  
   ngx.req.read_body()
   print = ngx.say
   args  =  ngx.req.get_post_args()
else
   args  = {txtPid = arg[1], cvv_code = arg[2], date_of_birth = arg[3]}    
end



function chk_kyc()
    local result = nil
    local params   = 'national_id=' .. body.cardNo ..'&first_name=' .. body.fnameTH .. '&last_name=' .. body.lnameTH .. '&date_of_birth=' ..  body.dob  .. '&cvv_code=' ..body.cvv_code   
    local res, err = prog('curl', '-s' , 
    '-k',
    '-A', "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36", 
    '-H', "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
    '-H', "Origin: https://empui.doe.go.th",
    '-H', "Host: empui.doe.go.th",
    '-H', "Referer: https://empui.doe.go.th/auth/register_member/2",
    '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

    if(not err)then
	--print('[' .. res.stdout .. ']')
        local tmp = cjson.decode(res.stdout)
            if(tmp) then
				body.message = tmp.message or 'N/A'
                if tostring(tmp.status) == 'true' then
                    result = tmp.status
                    body.success = true
                end             
            end 
    end
   
    return result
end

--run
card = args.txtPid
body = {success= false,message='-', cardNo = card, fnameTH = args.FNAME, lnameTH = args.LNAME, dob = args.date_of_birth, cvv_code = args.cvv_code}
chk_kyc()

--output
t = {}
t.success = body.success

if(t.success)
  then
        t.data = {}
        t.data.TITLE =  ""
        t.data.PID   =  card
        t.data.FNAME =  args.FNAME
        t.data.LNAME =  args.LNAME
        t.data.SEX   =  "-"
        t.data.DOB   =  args.date_of_birth:sub(7,10) .. args.date_of_birth:sub(4,5) .. args.date_of_birth:sub(1,2) --12/45/7890
   end
		
print(cjson.encode(t))
