#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {key = arg[1]} 
end

local all_accounts = cjson.decode(rc:get(body.key)).data
local accounts = body.accounts
local ret = {accounts = {}, capital = body.capital, cashflow = body.cashflow}

for i,acc in ipairs(all_accounts) do

	for _j, t in ipairs(accounts) do
          if(acc.account_no == t) then
		      table.insert(ret.accounts, acc)
			  break;
	      end
    end

end

-- Capital to Cashflow Ratio
local sum_mon_fn = function(accounts)
    local mon = {}
    for i,acc in ipairs(accounts) do
        for _j, t in ipairs(acc.data) do
            local period = t.date:sub(4,10)
            local withdraw = 0
            local deposit = 0
            t.deposit = t.deposit:gsub("+", "")
            t.deposit = t.deposit:gsub(",", "")
            t.withdraw = t.withdraw:gsub(",", "")
            t.withdraw = t.withdraw:gsub("-", "")
            withdraw = tonumber(t.withdraw) or 0
            deposit = tonumber(t.deposit) or 0

            mon[period] = mon[period] or {withdraw = 0, deposit = 0, balance = 0}
            mon[period].withdraw = mon[period].withdraw + withdraw
            mon[period].deposit = mon[period].deposit + deposit
            mon[period].balance = mon[period].balance + (deposit - withdraw)
        end
    end 
    return mon
end


local cashflow_fn =  function(accounts)
    local mon = sum_mon_fn(accounts)
    local sum_w = 0
    local sum_d = 0
    for k, v in pairs(mon) do
        sum_d = sum_d + v.deposit
        sum_w = sum_w + v.withdraw
    end 
    return sum_d - sum_w
end

local capital_fn = function(accounts)
    local mon = sum_mon_fn(accounts)
    local min = nil
    for k, v in pairs(mon) do
        if not min then min = v.balance end
        if v.balance < min then min = v.balance end
    end 
    return min
end

ret.cashflow = cjson.encode(cashflow_fn(ret.accounts))
ret.capital = capital_fn(ret.accounts)

rc:set(body.key..':comp', cjson.encode(ret))
print(cjson.encode({success = true, selected_accounts = ret}))
--rc:set(body.key, cjson.encode(ret.accounts))
--print(cjson.encode({success = true, selected_accounts = ret.accounts}))


