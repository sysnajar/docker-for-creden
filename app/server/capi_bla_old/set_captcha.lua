#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'resty.redis'
local rc = redis:new()
rc:connect('127.0.0.1', 6379)
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {captcha = arg[1]} 
end
rc:set(body.key..'ans',body.captcha)
t={success=true}
print(cjson.encode(t))
