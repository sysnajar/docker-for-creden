#!/usr/bin/env lua
--Ex1 how get args or query string
-- Ex url https://hjkl.ninja/capi/ex_lua_train1?img=11111111&id=222222222
-- local cjson = require 'cjson.safe'

-- if(ngx)then
--     body = ngx.req.get_uri_args()
--     print = ngx.say
-- else
--   body  = {img = arg[1], id = arg[2]} 
-- end

-- print(cjson.encode(body))
-- Result = {"img":"11111111","id":"222222222"}
-- End ex1

--Ex2 how get json
-- curl --location --request POST 'https://hjkl.ninja/capi/ex_lua_train1' \
-- header 'Content-Type: application/json' \
-- data-raw '{"id":"135667777","name":"Poae"}'
-- local cjson = require 'cjson.safe'
-- if(ngx)then  
--     ngx.req.read_body()
--     print = ngx.say
--     body = ngx.req.get_body_data()
--     ngx.log(ngx.NOTICE,body)
--     body = cjson.decode(body)
-- else
--     body = {id = arg[1], name = arg[2]}
-- end

-- print(cjson.encode(body))

-- Result = {"name":"Poae","id":"135667777"}

--end Ex2



--Ex3 connect mongo find one 
-- curl --location --request POST 'https://hjkl.ninja/capi/ex_lua_train1' \
-- header 'Content-Type: application/json' \
-- data-raw '{"id":"135667777","username":"pathapong@creden.co"}'

-- local cjson = require 'cjson.safe'
-- local mongo = require "resty-mongol"
-- local ret = {success= false}
-- if(ngx)then  
--     ngx.req.read_body()
--     print = ngx.say
--     body = ngx.req.get_body_data()
--     ngx.log(ngx.NOTICE,body)
--     body = cjson.decode(body)
-- else
--     body = {id = arg[1], username = arg[2]}
-- end


-- local conn = mongo()
-- conn:set_timeout(1000)
-- local ok, err = conn:connect('127.0.0.1',27017)

-- if not err then
--     local tmp = {}
--     local db = conn:new_db_handle("dopa")
--     local col = db:get_col("esig_user")
--     --local s = col:find({ ['$ne'] = { status = 'Complete'}  })
--     local s = col:find_one({adminEmail=body.username})
--     if s then
--         ret.success = true
--         ret.data = s.adminEmail
        
--     else
--         ret.success = true
--         ret.msg = 'not found username'
--     end
-- else
--     ret.success = false
--     ret.msg = err
-- end

-- print(cjson.encode(ret))
-- if find success display
--result = {"success":true,"data":"pathapong@creden.co"}
-- if not not found
--result = {"success":true,"msg":"not found username"}
--end Ex3




--Ex4 connect mongo find loop 
-- curl --location --request POST 'https://hjkl.ninja/capi/ex_lua_train1' \
-- header 'Content-Type: application/json' \
-- data-raw '{"id":"135667777","username":"huaseng@creden.co"}'
-- local cjson = require 'cjson.safe'
-- local mongo = require "resty-mongol"
-- local ret = {success= false}
-- if(ngx)then  
--     ngx.req.read_body()
--     print = ngx.say
--     body = ngx.req.get_body_data()
--     ngx.log(ngx.NOTICE,body)
--     body = cjson.decode(body)
-- else
--     body = {id = arg[1], username = arg[2]}
-- end
-- conn = mongo()
-- conn:set_timeout(5000)
-- local ok,  err = conn:connect('127.0.0.1',27017)

-- if  not err then
--     db = conn:new_db_handle("dopa")
--     col = db:get_col("esig_user")

--     local time = os.time()*1000
--     query = { adminEmail = body.username }

--     q = col:find(query)
--     local data_tmp = {}

--     for i, v in q:pairs() do
--         v._id = tostring(v._id)
--         table.insert(data_tmp, v)
--     end 

--     ret = { success = true, data = data_tmp }
-- else

--     ret = { success = false, error_msg = "not connect mongo"}
-- end
-- print(cjson.encode(ret))
--result = {"success":true,"data":"pack_exp data in json"}
--end Ex4




--Ex5 redis set and check expire
-- local cjson = require 'cjson.safe'
-- local mongo = require "resty-mongol"
-- local bson = require "resty-mongol.bson"
-- local redis = require 'redis'
-- local rc = redis.connect('127.0.0.1', 6379)

-- local ret = {success= false}
-- if(ngx)then  
--     ngx.req.read_body()
--     print = ngx.say
--     body = ngx.req.get_uri_args()
-- else
--     body = {id = arg[1], username = arg[2]}
-- end

--  local key = 'LEDstart.'..'0001'
--  rc:set('LEDstart.0001','aaaa')
--  timeo = rc:expire('LEDstart.0001', 60*1)
-- -- print(timeo)

-- local rc_check = rc:ttl('LEDstart.0001')
--     if(tonumber(rc_check) > 0) then
--         print(
-- 'not expire'
--         )
--     else
--         print(
-- 'expire'
--         )
--     end
--end Ex5


--Ex6 ใช้ os.execute

local cjson = require 'cjson.safe'

local ret = {success= false}
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_uri_args()
else
    body = {id = arg[1], username = arg[2]}
end
cmd = 'cat /home/creden/work/app/server/capi/text.txt'
os.execute(cmd)

--end Ex6


--Ex7 ใช้ prog

-- local cjson = require 'cjson.safe'
-- local exec = require'resty.exec'
-- local prog = exec.new('/tmp/exec.sock')
-- prog.timeout_fatal = false
-- local ret = {success= false}
-- if(ngx)then  
--     ngx.req.read_body()
--     print = ngx.say
--     body = ngx.req.get_uri_args()
-- else
--     body = {id = arg[1], username = arg[2]}
-- end
-- local res, err = prog('cat', '/home/creden/work/app/server/capi/text.txt')
-- if not err then
--     print(res.stdout)
-- else
--     print('not found')
-- end

--end Ex7