#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

t_province = {}
local res, err = prog('cat', '/home/creden/work/app/server/capi/province.txt')
if not err then
  str = string.gsub(res.stdout, "%s+", "")
  str = '{'..str..'}'
  t_province = cjson.decode(str)
else
  print(err)
end
imgkit = HOST_NAME..'/face/images/kit1234.jpeg'
province = ''
local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh',HOST_NAME..'/face/images/out.jpg')
local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
if not err then
    ocr = cjson.decode(res.stdout)
    long_text = ocr.responses[1].fullTextAnnotation.text
    row_address = 1
    long_text = string.gsub(long_text, "ทีอยู่\n", "ที่อยู่ ")
    long_text = string.gsub(long_text, "ที่อยู\n", "ที่อยู่ ")
    long_text = string.gsub(long_text, "ที่อย่\n", "ที่อยู่ ")
    long_text = string.gsub(long_text, "ที่อยู่\n", "ที่อยู่ ")
    for i in string.gmatch(long_text, "[^\r\n]+") do
       if row_address == 2 then
        t_add = string.gmatch(i,"%S+")
        province = ''
        for j in t_add do
          province = j
        end
        break
       end 

       if string.match(i, "ทีอยู่") or string.match(i, "ที่อยู") or string.match(i, "ที่อย่") or string.match(i, "ที่อยู่") then
         row_address = 2
       end
    end
else
    print(err)
end

if province ~= '' then
  go_google = true
  for k,v in pairs(t_province) do
    if string.match(k, province) then
        go_google = false
        print('result from dic')
        print(v)
      break
    end
  end

  if go_google then
    local res, err = prog('xvfb-run', '-a', 'node', '/home/creden/google_chk_word.js',province)
    if not err then
      province_google = string.gsub(res.stdout, "%s+", "")
      if t_province[province_google] then
        os.execute('echo \',"'..province..'":"'..t_province[province_google]..'"\' >> /home/creden/work/app/server/capi/province.txt')
        print('result from text recognize')
        print(t_province[province_google])
      else
        print('ไม่เจอจังหวัดใน dict')
      end   
    else
      print(err)
    end
  end
else
  print('ไม่เจอที่อยู่')
end
