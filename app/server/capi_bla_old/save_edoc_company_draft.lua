#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local common = require "common"
local session = require "resty.session".open({secret = "4321"})
local ret = {}
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {} 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function trim5(s)
    return s:match'^%s*(.*%S)' or ''
end

local conn = mongo()
conn:set_timeout(1000)
local ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    local col = db:get_col("edoc_list_draft")
    local doc = body
    doc.company_id = session.data.company_id 
    doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 
    local u, u_err = col:update({id=doc.id}, doc, 1, 0, true)

    if u_err then
        ret = {success = false, error_msg = u_err}
    else
        ret = {success = true, id = doc.id}
    end
end

print(cjson.encode(ret))

