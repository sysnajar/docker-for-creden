#!/usr/bin/env lua

function EN(p, text)
if text == nil then return '' end
local redis = redis or require 'redis'
local rc    = rc or redis.connect('127.0.0.1', 6379)
local md5   = md5 or require 'md5'
local cjson   = cjson or require 'cjson'

local key = 'lang.' .. md5.sumhexa(text)
local val = rc:get(key)

--ngx.log(ngx.NOTICE, "EARK val in = " .. tostring(text))
if(not val) then 
    local lang, lang_err = p('sh', '/home/creden/work/app/server/sapi/get_en2.sh', text)
    local txt = cjson.decode(lang.stdout)
    if(txt and txt and txt.data)
    then
         val =  txt.data.translations[1].translatedText
        rc:set(key, val)
        rc:lpush("logs.lang", text.. " = " .. val)
    else
         val =  text
    end
else
   --ngx.log(ngx.NOTICE, "EARK found cache")    
end

--ngx.log(ngx.NOTICE, "EARK val out = " .. tostring(val))
return val
end
