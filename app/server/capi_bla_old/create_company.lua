#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {name_company = arg[1], email = arg[2]}
end

function chk_exist_member(col_member_list, email)
    local exist_member = col_member_list:find_one({email = email})
        if exist_member then
            print(cjson.encode({success = false, error_msg = "User already exist a company"}))
            --ngx.exit(ngx.HTTP_OK)
			os.exit(0)
        end
end

--check data
if not body.name_company or not body.email then 
    ret.error_msg = 'Invalid data'
    print(cjson.encode(ret))
    return 0
end

--u = esig_user
--p = new position
function add_org_node(company_id, u, new_pos)	
    new_pos.position_id = nil
	local new_node = {
	  id  = new_pos.node_id,
	  title_id  = new_pos.position_id,
	  title     = new_pos.position_name,
	  person_id = tostring(u._id),
	  name = u.adminFisrtname,
	  img = "xxx",
	  email = u.adminEmail
	 }

	local key = "orgchart." ..company_id
	local json = rc:get(key)
	local ret = nil

	if(json)then
	  ret = cjson.decode(json)
	else
	  ret = {is_empty = false, company_id = company_id, nodes = {}}
	end

	table.insert(ret.nodes, new_node)
	ret.is_empty = false
	ret.success  = true
	rc:set(key, cjson.encode(ret))
end
-- END EARK FN

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
    local col_company_list = db_dopa:get_col("user_company")
    local col_esig_user = db_dopa:get_col("esig_user")
    local col_member_list = db_dopa:get_col("company_members")
    local col_title_list = db_dopa:get_col("title")
    chk_exist_member(col_member_list, body.email)
    local s_eisg_user, err_s = col_esig_user:find_one({adminEmail = body.email})

    local chk_com, err_chk_com = col_company_list:find_one({companyName = body.name_company})
    if chk_com then 
        ret.error_msg = 'Existing Company'
        print(cjson.encode(ret))
        return 0
    end

    if s_eisg_user then
        data.companyName = body.name_company
        data.esig_user_id = object_id.new(convertHexStringToNormal(tostring(s_eisg_user._id)))
        data.create_by = body.email
        data.createdDtm = bson.get_utc_date(os.time() * 1000)
        local i, err_i = col_company_list:insert({data})
        if i then
            local s, err_s = col_company_list:find_one({companyName = body.name_company, create_by = body.email})
            if s then
                ret.success = true
                ret.company_detail = {company_id = tostring(s._id), companyName = s.companyName, image = s.image}
                --add member
                local data_title = {title = 'Unspecified', company = s._id, created_dtm = data.createdDtm}
                local i_title, err_i_title = col_title_list:insert({data_title})
                local title, err_title = col_title_list:find_one({title = 'Unspecified'})
                if title then
                    local positions = {}
                    positions[1] = {position_name = 'Unspecified', node_id = tostring(s._id)..os.time(), position_id = title._id}
                    local data_member = {email = body.email, status = 'APPROVE', esig_user_id = data.esig_user_id, created_by = body.email, role = 'ADMIN', createdDtm = data.createdDtm, company_id = s._id, positions = positions}
                    local member, err_member = col_member_list:insert({data_member})
                    add_org_node(tostring(s._id), s_eisg_user , positions[1]) 
                end
            else
                ret.error_msg = err_s
            end
        else
            ret.error_msg = err_i
        end
    else
        ret.error_msg = 'Invalid user'
    end
end

print(cjson.encode(ret))
