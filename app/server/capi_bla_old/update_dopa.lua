#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {fnameTH='คุณากร', 
            lnameTH='จันทร์ชื่น',
            fnameEN='', 
            lnameEN='', 
            cardNo='1100701382072', 
            laserCode='ME0117526468', 
            dob='11/08/2534', 
            createDate='13/03/2561',
            cardImg='1100701382071.png',
            status='wait',
            custCode='rads',
            _id='5aaf78dfce6d233d2ed26dd0'} 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

   --kyc
   local params   = 'national_id=' .. body.cardNo ..'&first_name=' .. body.fnameTH .. '&last_name=' .. body.lnameTH .. '&date_of_birth=' ..  body.dob  .. '&cvv_code=' ..body.laserCode
   local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

   if(not err)then
		local tmp = cjson.decode(res.stdout)
        if(tmp) then
            if tostring(tmp.status) == 'true' then
                body.status = 'pass'	
            else
                body.status = 'fail'   
            end             
			body.remarks = tmp.message:gsub('java.lang.RuntimeException: %[4%]','')
            local callback_url = rc:get("kyc.settings.rads.callback_url")
            local params = {txID = body.txid, isValid = tmp.status, remarks = body.remarks }
            local res, err = prog('curl', '-X' , 'POST', callback_url, '-d', cjson.encode(params))
        end 

        --update mongo
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if err then
            ret = {success = false, error_msg = err}
        else
            db = conn:new_db_handle("dopa")
            col = db:get_col("ekyc")
            update = {["$set"] = {remarks=body.remarks,
                                    status=body.status, 
                                    fnameTH=body.fnameTH, 
                                    lnameTH=body.lnameTH,
                                    fnameEN=body.fnameEN, 
                                    lnameEN=body.lnameEN, 
                                    cardNo = body.cardNo, 
                                    laserCode=body.laserCode, 
                                    dob=body.dob, 
                                    createDate=body.createDate,
                                    cardImg1=body.cardImg1}}

            u, err = col:update({_id=object_id.new(convertHexStringToNormal(body._id))}, update, 0, 0, true)
            
            if err then
                ret = {success = false, error_msg = err}
            else
                ret = {success = tmp.status, error_msg = err, status = body.status, remarks = body.remarks}
            end
        end

    else 
        ret = {success = false, error_msg = err}
    end
    


print(cjson.encode(ret))


