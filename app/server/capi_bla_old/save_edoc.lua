#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {} 
end
    -- data = [[
    -- {
    --     "pdfFile":"test.pdf",
    --     "imageFiles":[{"fileName":"page1.png"},{"fileName":"page2.png"}],
    --     "signers":[{"email":"nutp10.1@gmail.com"},{"email":"kjunchean@gmail.com"}],
    --     "id":"1",
    --     "status":"0",
    --     "fields":[
    --         {"id":"1","email":"nutp10.1@gmail.com","x":"10","y":"10","status":"0","page":"1","time":"xx"},
    --         {"id":"2","email":"kjunchean@gmail.com","x":"20","y":"20","status":"0","page":"1","time":"xx"}
    --     ]
    -- }
    -- ]]
    -- print(data)
    -- body = cjson.decode(data)
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        doc = body
		doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 

        i, err = col:insert({doc}, nil, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            ret = {success = true, error_msg = err}
        end
    end

print(cjson.encode(ret))


