#!/usr/bin/env lua
local cjson = require 'cjson'
str = [[
{
  "textAngle": 0.0,
  "orientation": "NotDetected",
  "language": "en",
  "regions": [
    {
      "boundingBox": "296,392,1121,627",
      "lines": [
        {
          "boundingBox": "572,392,741,86",
          "words": [
            {
              "boundingBox": "572,392,158,70",
              "text": "u.<."
            },
            {
              "boundingBox": "1037,411,276,67",
              "text": "€nvua"
            }
          ]
        },
        {
          "boundingBox": "615,503,652,62",
          "words": [
            {
              "boundingBox": "615,512,135,40",
              "text": "Name"
            },
            {
              "boundingBox": "807,503,146,55",
              "text": "Miss"
            },
            {
              "boundingBox": "985,507,282,58",
              "text": "Techinee"
            }
          ]
        },
        {
          "boundingBox": "615,601,585,69",
          "words": [
            {
              "boundingBox": "615,601,102,41",
              "text": "Last"
            },
            {
              "boundingBox": "923,601,277,69",
              "text": "Rakphon"
            }
          ]
        },
        {
          "boundingBox": "679,690,607,65",
          "words": [
            {
              "boundingBox": "679,690,169,57",
              "text": "u;aäutl"
            },
            {
              "boundingBox": "898,696,68,53",
              "text": "16"
            },
            {
              "boundingBox": "994,702,124,51",
              "text": "\"fl."
            },
            {
              "boundingBox": "1146,702,140,53",
              "text": "2529"
            }
          ]
        },
        {
          "boundingBox": "672,788,745,59",
          "words": [
            {
              "boundingBox": "672,797,106,40",
              "text": "Date"
            },
            {
              "boundingBox": "798,797,48,41",
              "text": "of"
            },
            {
              "boundingBox": "860,799,122,42",
              "text": "Birth"
            },
            {
              "boundingBox": "1015,788,65,54",
              "text": "16"
            },
            {
              "boundingBox": "1109,790,140,56",
              "text": "Mar."
            },
            {
              "boundingBox": "1282,794,135,53",
              "text": "1986"
            }
          ]
        },
        {
          "boundingBox": "296,978,157,41",
          "words": [
            {
              "boundingBox": "296,978,157,41",
              "text": "578/25"
            }
          ]
        }
      ]
    },
    {
      "boundingBox": "331,188,1589,769",
      "lines": [
        {
          "boundingBox": "1315,188,605,78",
          "words": [
            {
              "boundingBox": "1315,188,301,68",
              "text": "National"
            },
            {
              "boundingBox": "1650,199,78,59",
              "text": "ID"
            },
            {
              "boundingBox": "1758,203,162,63",
              "text": "card"
            }
          ]
        },
        {
          "boundingBox": "904,263,636,76",
          "words": [
            {
              "boundingBox": "904,263,29,58",
              "text": "1"
            },
            {
              "boundingBox": "975,265,156,63",
              "text": "1014"
            },
            {
              "boundingBox": "1162,272,203,63",
              "text": "99124"
            },
            {
              "boundingBox": "1396,279,74,58",
              "text": "91"
            },
            {
              "boundingBox": "1506,282,34,57",
              "text": "8"
            }
          ]
        },
        {
          "boundingBox": "331,293,526,54",
          "words": [
            {
              "boundingBox": "331,293,320,48",
              "text": "kientification"
            },
            {
              "boundingBox": "666,304,191,43",
              "text": "Numi*r"
            }
          ]
        },
        {
          "boundingBox": "1823,922,73,35",
          "words": [
            {
              "boundingBox": "1823,922,73,35",
              "text": "-150"
            }
          ]
        }
      ]
    },
    {
      "boundingBox": "150,1143,299,227",
      "lines": [
        {
          "boundingBox": "159,1143,282,41",
          "words": [
            {
              "boundingBox": "159,1143,28,41",
              "text": "9"
            },
            {
              "boundingBox": "208,1145,101,39",
              "text": "W.D."
            },
            {
              "boundingBox": "331,1143,110,41",
              "text": "2560"
            }
          ]
        },
        {
          "boundingBox": "155,1201,230,52",
          "words": [
            {
              "boundingBox": "155,1201,230,52",
              "text": "5%DDnfiBt"
            }
          ]
        },
        {
          "boundingBox": "154,1270,289,43",
          "words": [
            {
              "boundingBox": "154,1272,27,41",
              "text": "9"
            },
            {
              "boundingBox": "199,1270,113,43",
              "text": "Nov."
            },
            {
              "boundingBox": "335,1271,108,42",
              "text": "2017"
            }
          ]
        },
        {
          "boundingBox": "150,1331,299,39",
          "words": [
            {
              "boundingBox": "150,1333,103,37",
              "text": "Date"
            },
            {
              "boundingBox": "272,1331,43,38",
              "text": "of"
            },
            {
              "boundingBox": "330,1331,119,38",
              "text": "Issue"
            }
          ]
        }
      ]
    },
    {
      "boundingBox": "1120,1126,304,247",
      "lines": [
        {
          "boundingBox": "1125,1126,281,60",
          "words": [
            {
              "boundingBox": "1125,1145,48,40",
              "text": "15"
            },
            {
              "boundingBox": "1192,1126,92,60",
              "text": "i.a."
            },
            {
              "boundingBox": "1304,1147,102,39",
              "text": "2569"
            }
          ]
        },
        {
          "boundingBox": "1123,1254,295,58",
          "words": [
            {
              "boundingBox": "1123,1270,48,41",
              "text": "15"
            },
            {
              "boundingBox": "1189,1270,106,41",
              "text": "Mar."
            },
            {
              "boundingBox": "1317,1254,101,58",
              "text": "2026'"
            }
          ]
        },
        {
          "boundingBox": "1120,1327,304,46",
          "words": [
            {
              "boundingBox": "1120,1328,96,37",
              "text": "Date"
            },
            {
              "boundingBox": "1233,1327,40,38",
              "text": "of"
            },
            {
              "boundingBox": "1290,1328,134,45",
              "text": "Expiry"
            }
          ]
        }
      ]
    },
    {
      "boundingBox": "1507,918,384,433",
      "lines": [
        {
          "boundingBox": "1536,918,70,33",
          "words": [
            {
              "boundingBox": "1536,918,70,33",
              "text": "60."
            }
          ]
        },
        {
          "boundingBox": "1507,1310,384,41",
          "words": [
            {
              "boundingBox": "1507,1310,384,41",
              "text": "1045-02-11091544"
            }
          ]
        }
      ]
    }
  ]
}
]]

res = ''
t = cjson.decode(str)
for key,value in pairs(t.regions) do
  for key2,value2 in pairs(value.lines) do
    for key3,value3 in pairs(value2.words) do
      res = res .. value3.text 
    end
    res = res .. '\n'    
  end
end

print(string.match(res, '%d%d%d%d%d%d%d%d%d%d%d%d%d'))
