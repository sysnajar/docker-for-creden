#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end


local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
rc:set('tmp_card2', body.img)
body.id = os.time()
--body.id = 'nut1'
--base64 to image
local res, err = prog('node', '/home/creden/work/app/server/capi/image.js', body.id)
local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
if not err then
    --[[data = string.gsub(res.stdout, "%s+", "")
    data = string.gsub(data, "%.", "")
    data = string.gsub(data, "%;", "")
    a = string.match(data, '%d%d%d%d%d%d%d%d%d%d%d%d%d')]]--
    ocr = cjson.decode(res.stdout)
    long_text = '' 
    i = 1
    for key,value in pairs(ocr.regions) do 
      for key2,value2 in pairs(value.lines) do 
        if i < 10 then
            for key3,value3 in pairs(value2.words) do 
              long_text = long_text .. value3.text
            end
            long_text = long_text..'#'
        end       
        i = i + 1 
      end
    end

    a = string.match(long_text, '%d%d%d%d%d%d%d%d%d%d%d%d%d')
    dob = string.match(long_text, '%d%d%a%a%a%d%d%d%d') or string.match(long_text, '%d%d%a%a%a%.%d%d%d%d') or string.match(long_text, '%d%a%a%a%.%d%d%d%d') or string.match(long_text, '%d%a%a%a%d%d%d%d') or string.match(long_text, '%d%d%a%a%a,%d%d%d%d') or string.match(long_text, '%d%a%a%a,%d%d%d%d')  or 'bad format'
    --print('long_text',long_text)
    --print('dob',dob)
    dob_th = string.gsub(long_text,dob,'')
    dob_th = string.match(long_text, '.25%d%d') or string.match(long_text, '.25%d%d') or string.match(long_text, '.25%d%d') or string.match(long_text, '.25%d%d')  or 'bad format'
    dd_th = string.gsub(dob_th,"%.","")
    dob = string.gsub(''..dob,",","")
    dob = string.gsub(''..dob,"%.","")

    dd = ''
    mm = ''
    yy = ''
    if dob ~= 'bad format' then
        dd_th = string.gsub(dob_th,"%.","")
        dob = string.gsub(''..dob,",","")
        dob = string.gsub(''..dob,"%.","")
        dd = string.gsub(dob,"%.","")
        yy = dd:sub(-4)

        yy_th = ''
        if dd_th and dd_th ~= 'bad format' then
            yy_th = dd_th:sub(-4) - 543
            yy_th = ''..yy_th
        end 
     
        dd = string.gsub(dd,yy,"")
        mm = dd:sub(-3)
        dd = string.gsub(dd,mm,"")
        dd = ((string.len(dd) == 1) and '0'..dd or dd)
        if yy_th ~= yy then
            t.dob = {day = dd, mon = mm, year = ''}
        else
            t.dob = {day = dd, mon = mm, year = yy_th+543}
        end
    else
        t.dob = {day = dd, mon = mm, year = yy}
    end
     
	t.card_id = a
    t.success = true
    --os.execute('mogrify -rotate -90 /home/creden/work/app/client/face/images/card/'..body.id..'.png')
    local face, err = prog('node', '/home/creden/work/app/server/capi/test_curl.js', body.id)
    if face.stdout then
        t.face = cjson.decode(face.stdout)
    end
else
    print(err)
end

print(cjson.encode(t))