#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local txid = ''
local vdo_score = 0
local audio_score = 0
local total_score = 0
local isValidLiveness = false
local isFaceVerified = false
local res = {success = true}
prog.timeout_fatal = false
HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = cjson.decode(arg[1])
end

--body = {success = true, username = 'ekycdemo1', txid = '60', score = 1, maxScore = 2, type = 'vdo'}
--body = {success = true, username = 'creden', txid = '', score = 2, maxScore = 3, type = 'audio'}

function chk_res() 
    local status = nil
    vdo_score = rc:get('liveness.'..body.username..'.'..body.txid..'.vdo')
    audio_score = rc:get('liveness.'..body.username..'.'..body.txid..'.audio')
    --face_ver = rc:get('liveness.'..body.username..'.'..body.txid..'.face')
    face_ver = true
    if vdo_score and audio_score and face_ver then 
        status = true 
    end
    return status
end

function update_res() 
    local key = 'liveness.'..body.username..'.'..body.txid..'.'..body.type
    local chk = rc:get(key)
    rc:set(key, body.score)
end

function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical, isValidLiveness)
    if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidAML) == 'true' and tostring(isValidLiveness) == 'true' then
        body.isValid = true
    else
        body.isValid = false
    end
end

update_res()

if chk_res() then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")
        selector = {txid=body.txid, compCode = body.username}
        s = col:find_one(selector)
        if s then
            total_score = vdo_score + audio_score
            isFaceVerified = face_ver
            ---------------------

            if body.maxScore > 0 and total_score == body.maxScore and tostring(isFaceVerified) == 'true' then isValidLiveness = true end
            chk_is_valid(s.isValidDopa, s.isValidAML, s.isFaceIdentical, isValidLiveness)
            update = {["$set"] = {process_status = 'finished', isValid = body.isValid, isValidLiveness = isValidLiveness, livenessResult = {isFaceVerified = isFaceVerified, score = total_score, maxScore = body.maxScore, pc_id=body.pc_id, error_msg=body.error_msg, error_code=body.error_code, fps=body.fps}}}
            i, err = col:update(selector, update, 0, 0, true)
        end
    end
end
res.score = body.score
print(cjson.encode(res))
