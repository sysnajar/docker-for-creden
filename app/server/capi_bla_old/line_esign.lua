#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local common = require "common"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {}
local body = nil

prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
end

local function reply(reply_token, message, quick_reply)
    local send_message = {
        replyToken = reply_token,
        messages = {
            {
                type = "text",
                text = message,
                quickReply = quick_reply
            }
        }
    }
    ngx.log(ngx.NOTICE,cjson.encode(send_message))
    local res, err = prog('sh', '/home/creden/work/app/server/capi/line_esign_send_reply.sh', cjson.encode(send_message))
    if err then
        ngx.log(ngx.NOTICE,cjson.encode(err))
    end
end

local function load_image(user_id, message_id)
    local ret = true
    local path = '/home/creden/work/app/client/face/images/card/line_connect_qr_'..user_id..'.png'
    local url = 'https://api-data.line.me/v2/bot/message/'..message_id..'/content'
    local res, err = prog('sh', '/home/creden/work/app/server/capi/line_esign_image_load.sh', path, url)
    if err then
        ret = false
        ngx.log(ngx.NOTICE,'load_image_error'..cjson.encode(err))
    end
    return ret, path
end

local function decode_qr(path)
    local ret = true
    local out = ''
    local res, err = prog('sh', '/home/creden/work/app/server/capi/line_esign_decode_qr.sh', path)
    if err or res.stdout=='' then
        ret = false
        ngx.log(ngx.NOTICE,'decode_qr_error please check' .. cjson.encode(err))
	local cmd = 'sh /home/creden/work/app/server/capi/line_esign_decode_qr.sh ' .. path
	ngx.log(ngx.NOTICE, cmd)
    end

    if res and res.stdout ~= '' then
        out = res.stdout:gsub("\n","")
    end

    return out
end

local function check_connection(user_id)
    local connection_status = false
    local account_key = 'esign_line.'..'connect.*.'..user_id
    local account_list = rc:keys(account_key)
    local account = {}
    if table.getn(account_list) > 1 then
        for k, v in ipairs(account_list) do
            rc:del(v)
        end
    elseif table.getn(account_list) == 1 then            
        account = cjson.decode(rc:get(account_list[1]))
        if account.line_user_id == user_id then
            connection_status = true
        end
    end
    return connection_status, account
end

local function disconnect(user_id)
    local account_key = 'esign_line.'..'connect.*.'..user_id
    local account_list = rc:keys(account_key)
    for k, v in ipairs(account_list) do
        rc:del(v)
    end
end

if body then
    local message_type = body.events[1].message.type
    local message_id = body.events[1].message.id
    local user_id = body.events[1].source.userId
    local reply_token = body.events[1].replyToken
    local connect_key = 'esign_line.'..'handshake.'..user_id
    local connection_status, account = check_connection(user_id)
    ngx.log(ngx.NOTICE,"message_type==="..message_type)

    if message_type == 'text' then
        local text = body.events[1].message.text
       
        ngx.log(ngx.NOTICE,"message_text==="..text)
        ngx.log(ngx.NOTICE,"connection_status==="..tostring(connection_status))

        if text == 'connect' then
            if connection_status == true then
                reply(reply_token, 'Successfully connected with account: ' .. account.email)
            else
                rc:set(connect_key, '1')
                local quick_reply = {items = { {type = "action", action = { type = "cameraRoll", label = "Gallery" } },{type = "action", action = { type = "camera", label = "Camera" } } }}
                reply(reply_token, 'กรุณาถ่ายรูป QR Code ของท่านจากเว็ปไซต์ Creden e-Signature ในขั้นตอนที่สอง', quick_reply)
            end
        elseif text == 'disconnect' then
            if connection_status == true then
                disconnect(user_id)
                reply(reply_token, 'ยกเลิกการเชื่อมต่อแล้ว')
            else
                reply(reply_token, 'คุณยังไม่ได้ทำการเชื่อมต่อ')
            end
        end
    elseif message_type == 'image' then
        connect_handshake = rc:get(connect_key)
        if connect_handshake and connect_handshake == '1' then
            ngx.log(ngx.NOTICE,'connect_key[['..connect_key..']] is valid ' .. user_id ..','..  message_id)
            local ret, path = load_image(user_id, message_id)
            if ret == true then
                local token = decode_qr(path)
                ngx.log(ngx.NOTICE,'[[Invalid token >>>> '..token..']]')
                if token and token ~= '' then
                    local token_key = 'esign_line.'..'connect.*.'..token
                    local token_key_list = rc:keys(token_key)
                    ngx.log(ngx.NOTICE,'token_key_list count = ' .. table.getn(token_key_list))
                    if table.getn(token_key_list) > 1 then
                        for k, v in ipairs(token_key_list) do
                            rc:del(v)
                        end
                        reply(reply_token, 'ไม่สามารถเชื่อมต่อได้\nQR Code ไม่ถูกต้อง. (token>1)')
                    elseif table.getn(token_key_list) == 1 then
                        redis_data = cjson.decode(rc:get(token_key_list[1]))
                        if token == redis_data.token then
                            local token_key = 'esign_line.'..'connect.'..redis_data.email..'.'..user_id
                            redis_data.line_user_id = user_id
                            rc:set(token_key, cjson.encode(redis_data))

                            rc:del(token_key_list[1])
                            rc:del(connect_key)

                            reply(reply_token, 'Successfully connected with account: ' .. redis_data.email)
                        else
                            ngx.log(ngx.NOTICE,'Tokens does not match ['.. token ..'] vs [' .. redis_data.token ..'] line:' .. user_id)
                            reply(reply_token, 'ไม่สามารถเชื่อมต่อได้\nQR Code ไม่ถูกต้อง..')
                        end
                    else
                        ngx.log(ngx.NOTICE,'No token found ('.. #token_key_list  ..') line:' .. user_id)
                        reply(reply_token, 'ไม่สามารถเชื่อมต่อได้\nQR Code ไม่ถูกต้อง...please generate new token')
                    end
                else
                    ngx.log(ngx.NOTICE,'Invalid connect_key[['..connect_key..']] line:' .. user_id)
                    reply(reply_token, 'ไม่สามารถเชื่อมต่อได้\nQR Code ไม่ถูกต้อง....Unnable to read OR')
                end
            end
        end
    end
end
