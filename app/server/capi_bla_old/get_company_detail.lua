#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = { success = false,error_msg='error' }
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {company_id = ''}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

-- CHECK SESSION
-- if not session.data.company_id then
--     ret.error_msg = 'Invalid Data'
--     print(cjson.encode(ret))
-- 	ngx.exit(ngx.HTTP_OK)
--     --return 0
-- else
--     body.company_id = session.data.company_id
-- end

ret = {}
conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "" }
else

    if not session.data.company_id then
        ret = { success = false, error_msg = "not session company id", error_code = "" }
    else

        local db_dopa = conn:new_db_handle("dopa")
        local col_company = db_dopa:get_col("user_company")
        local company, err_company = col_company:find_one({_id=object_id.new(convertHexStringToNormal(session.data.company_id))})
       
        if company then
            company.company_id = tostring(company._id)
            company.esig_user_id = nil
            company._id = nil
    
            ret = { success = true, error_msg = "find company success", error_code = "", data = company }
        else
            ret = { success = false, error_msg = "find company unsucess", error_code = "" }
        end

    end
end
print(cjson.encode(ret))


