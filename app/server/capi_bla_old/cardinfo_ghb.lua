#!/usr/bin/env lua
local cjson = require 'cjson'
local bson = require "resty-mongol.bson"
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
  --  args  =  ngx.req.get_uri_args()
     json = ngx.req.get_body_data()
     ngx.log(ngx.NOTICE, json)
     args = cjson.decode(json)
else
    args  = {id  = arg[1]} 
end
 
ret = {success = false, error_msg = "N/A"}
ok = true
if not ok then
	ret = {success = false, error_msg = err}
else
	local key = args.card_reader_id_tx
	local json = rc:get(key)
	local s = cjson.decode(json)
	if json and s then --case I. found email address
		 t = {}
		 local p1 = cjson.decode(s.body.page1)
		 local p2 = cjson.decode(s.body.page2)
		 --local p3 = cjson.decode(s.body.page3)
		 local fkey = 'face.' .. key
		 local fjson = rc:get(fkey)
         ngx.log(ngx.NOTICE, 'get fkey  ' .. fkey .. ' = '  .. tostring(fjson))
		 local p3 = cjson.decode(fjson)

		 t.firstname = p1.name
		 t.lastname = p1.lastname
		 t.address =  ''
		 t.province = p1.province or ''
		 t.sex = p1.sex or ''
		 t.Number_card = p1.card_id

         t.img1 = s.body.img1
         t.img2 = s.body.img2
         t.img3 = s.body.img3
		 t.video = s.body.video

		 t.day = ''
		 t.mon = ''
		 t.year = ''
		 if(p1.dob)then
			t.day  = p1.dob.day or ''
			t.mon  = p1.dob.mon or ''
			t.year = p1.dob.year or ''
		 end

		 --t.laser_card_id = "123456789012"--s.laser_card_id

		 --page2
		 t.laser_code = {lc1="a", lc2="b",lc3="c"}
		 if(p2.laser_code)then
			t.laser_code = p2.laser_code
		 end

		 --page3
		 t.face_pass = "ไม่ผ่าน"
		 if(p3.face and p3.face.isIdentical)then
			t.face_pass = "ผ่าน"
		 end

		 if(p3.face and p3.face.confidence)then
            t.face_pct = p3.face.confidence * 100
		 end

	--[[	 if(p3.face) then
             t.selfie_file = "68494043874499116042" -- p3.face.selfie_file
             t.faceid_file = "selfie_Glosec1562575998588" -- p3.face.faceid_file
		 end
		 ]]
		 

		ret = {success = true , data=t}
	end
end

print(cjson.encode(ret))
