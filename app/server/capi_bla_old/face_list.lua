#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

local body = nil

if(ngx)then  
   ngx.req.read_body()
   print = ngx.say
   local tbx = require "pl.tablex"
   body = tbx.union(ngx.req.get_post_args() or {error = "no data"},ngx.req.get_uri_args())
else
   body  = {param1 = arg[1], param2 = arg[2]} 
end

local t = rc:lrange('save_detect_face', 0, -1)
local t2 = {}
for i,v in ipairs(t) do table.insert(t2, cjson.decode(v)) end
print(cjson.encode(t2))

