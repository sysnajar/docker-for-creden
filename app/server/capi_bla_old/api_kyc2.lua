#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--get non-empty value
function getVal(key)
local ret = body[key]
ret = ret and trim5(ret) or nil
ret = (ret=='') and nil or ret
return ret
end

--save cardImg1,2 and 3 to disk
function uploadImgs()
 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)
 local cnt = tostring(rc:incr('kyc.counter'))
 local dir = '/home/creden/work/app/client/face/images/card'

 local k = 'cardImg1'
 local v = getVal(k)
 
 if(v)then
   local tmp = 'tmp.kyc.img' .. cnt .. k	 
   rc:set(tmp , v)   
   --rc:expire(tmp , 10)   
 end
  
end

--todo cust_code may not == username in the future
function get_cust_code(usr)
return usr
end

--todo cust_code may not == username in the future
function chk_pwd(usr, pwd)
local map = {rads = '1234'}

return usr ~= nil and pwd ~= nil and map[usr] == pwd
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
	body  = ngx.req.get_post_args()
else
   body   = {} 
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")

		if(chk_pwd(body.username, body.password)) then
		   body.username = nil
		   body.password = nil
		   body.status = 'wait'
		   body.custCode = get_cust_code(body.username)
		   uploadImgs()
           n, err = col:insert({body})

		   if not n then
              ret = {success = false, error_msg = err}
           else
              ret = {success = true, error_msg = err, status = body.status}
           end
	   else
		   ret = {success = false, error_msg = 'Invalid Login'}
		end

	   	

    end

   print(cjson.encode(ret))
   print(cjson.encode(body))
   print(cjson.encode(n))
   
