#!/usr/bin/env lua
cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
ret = {success = false}
local check_time1 
function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end



function check_time(time)
    local status = false
    -- local current_time = bson.get_utc_date(ngx.now() * 1000).v
    local current_time = os.time()*1000
    local exp = time + (24*60*60*1000)
    if current_time > exp then
        status = true
    end
    return status
end



if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body  = ngx.req.get_post_args()
    -- body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

	-- ngx.log(ngx.INFO, "check expire documents")
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    
    q = col:find({ status = 0 })
    -- q = col:find({owner="amorrut@creden.co"})

    for i, v in q:pairs() do

        d = v.exp_date or 'N/A'
        -- print(d)
        -- d2 = "04/25/2020"
        chk_fotmat_date = string.find(d,'-') or 0

        if chk_fotmat_date > 0 then
            y = d:sub(1,4)
            m = d:sub(6,7)
            d1 = d:sub(9,10)
            time1 = (os.time({year=y, month=m, day=d1}) * 1000)
            check_time1 = check_time(time1)

            if check_time1 then
                local update = {["$set"] = {status=5}}
                u, err = col:update({id= tostring(v.id) }, update, 0, 0, true)
                print( "id----1"..tostring(v.id).."update========")
            end
            
        else
            -- print( "dayyy"..d.."========")
            chk_d = string.find(d,'/') or 0
            if d ~= 'N/A' and chk_d > 0  then
                y = d:sub(7,10)
                m = d:sub(1,2)
                d1 = d:sub(4,5)
                if d1 then
                    time1 = (os.time({year=y, month=m, day=d1}) * 1000)
                    check_time1 = check_time(time1)

                    if check_time1 then
                        local update = {["$set"] = {status=5}}
                        u, err = col:update({id= tostring(v.id)}, update, 0, 0, true)
                        print( "id----2"..tostring(v.id).."update========")
                    end
                end
            end
        end
     end 


end
