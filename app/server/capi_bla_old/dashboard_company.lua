#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {company_id = '5c2c7339778fdc6a5f91c0d3'}
end

--check data
-- if not body.company_id or body.company_id == 0 then 
--     ret.error_msg = 'Invalid data'
--     print(cjson.encode(ret))
--     return 0
-- end

--prevent email param
if(not session.data.username ) then
    print(cjson.encode({success = false, error_msg = "session expired"}))
    ngx.exit(ngx.HTTP_OK)
else
    if(body.email~=session.data.username) then
        ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
        body.email = session.data.username
    end
end	
--prevent email param

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
    local db_edoc = conn:new_db_handle("edoc")
    local col_edoc_list = db_edoc:get_col("edoc_list")
    local col_member_list = db_dopa:get_col("company_members")
    -- local documents = col_edoc_list:count({company_id=body.company_id})

    local deleted_docs = db_edoc:get_col('deleted_doc'):find({email = body.email})
    local deleted_ids = {0}
    local count_trash = 0
    
    for k, v in deleted_docs:pairs() do
        if (tostring(v.id) ~= "nil") then
            -- count_trash = count_trash+1
            table.insert(deleted_ids, tostring(v.id))
        end
    end

    local arr = {}
    local map = {}
    if(#deleted_ids > 0 ) then
    for i,v in ipairs(deleted_ids)do
        if(not map[v])then
            map[v] =  "123"
            table.insert(arr,v)
            count_trash = #arr
        end
    end
    end
    
    function count(cur)
	local ret = 0

	for i,v in cur:pairs() do
	    ret = ret + 1
	end

	return ret
    end
    
    local status1 =0
    local query_not_sign = count(col_edoc_list:find( {fields={['$elemMatch']={email=body.email, status=0}}}, {_id = 1} ))
    local query_signer = count(col_edoc_list:find({owner={['$ne']=body.email}, signers={['$elemMatch']={email=body.email}}, status = {['$ne']=3},   id = {['$nin'] = deleted_ids }, is_template={['$ne']=true}} , {_id =1} ))
    local query_owner = count(col_edoc_list:find({owner=body.email,status = {['$ne']=3}, id = {['$nin'] = deleted_ids}, is_template={['$ne']=true}} , {_id=1}))
    local query_owner_cancle = count(col_edoc_list:find({owner=body.email,status = 3}))
    local query_sinner_cancle = count(col_edoc_list:find({signers={['$elemMatch']={email=body.email}},status = 3,owner = {['$ne'] = body.email} }))
    local documents_all = (tonumber(query_signer) or 0) + (tonumber(query_owner) or 0)
    local members = count(col_member_list:find({company_id=object_id.new(convertHexStringToNormal(body.company_id))} , {_id=1}))
    local query_cancle_doc = count(col_edoc_list:find({owner=body.email,status=3} , {_id = 1}))
    local cancledoc = query_owner_cancle + query_sinner_cancle
    
    ret.success = true
    ret.documents_all = documents_all 
    ret.documents_owner = query_owner 
    ret.documents_signer = query_signer 
    ret.documents_not_sign = query_not_sign
    ret.members = members
    ret.documents_cancle = query_cancle_doc
    ret.count_trash = count_trash-1
    
end

print(cjson.encode(ret))
