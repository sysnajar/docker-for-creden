#!/usr/bin/env lua
local session = require "resty.session".open({secret = "4321"})

--ngx.say("<html><body>Session was started by <strong>", session.data.username or "Anonymous", "from", session.data.custCode or "Unknown Company")

local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    body = ngx.req.get_uri_args()
else
   body  = {adminEmail = arg[1],file = "1530097604161",stamp = false} 
end


have_file = false 
err_msg = ''
tmp = {}
ngx.log(ngx.NOTICE, "username = " .. session.data.username)
if(not session.data.username ) then
    err_msg="No Session! Plase Login"
else
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = "connection error"}
    else
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        s = col:find({signers={['$elemMatch']={email=session.data.username }}})
        s2 = col:find({owner=session.data.username})
        if s then
            err_msg = 'no_file'
            for k, v in s:pairs() do
               v._id = tostring(v._id)
               if v.id == body.file then
                have_file = true
                break
               end
               --table.insert(tmp,v)
            end

            if not have_file then
                for k, v in s2:pairs() do
                   v._id = tostring(v._id)
                   if v.id == body.file then
                    have_file = true
                    break
                   end
                end
            end
            
        else
            err_msg = "no permission"
        end
    end
end
file_name = body.file
if body.stamp then
    file_name =  file_name..'-stamp'
end
if have_file then
    ngx.exec('/face/images/card/'..file_name..'.pdf')
else
    ngx.say(cjson.encode(tmp))
    --ngx.say("<html><body>"..err_msg.."/"..session.data.username.."Please Login or Permission not found</body></html> ")
end
