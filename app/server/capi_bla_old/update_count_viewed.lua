#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}


function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function update_viewed(doc) 
	if doc.status == 0 or doc.status == 3 then

        local time = bson.get_utc_date(os.time() * 1000)

        for i, v in ipairs(doc.signers) do

            if v.is_group == true then

                for i2, v2 in ipairs(v.user_group) do 
                    if not v2.is_viewed and v2.email == body.email then
                        v2.is_viewed = true
                        v2.is_viewed_date = time
                    end
                end

            else
                if not v.is_viewed and v.email == body.email then
                    v.is_viewed = true
                    v.is_viewed_date = time
                end
            end

        end

        u, err = col:update({ id = doc.id }, {["$set"] = { signers = doc.signers }}, 0, 0, true)
    end
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {owner_folder = arg[1]} 
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    d, err = col:find_one({ id = body.id })

    if err then 
        ret = { success = false, error_msg = "update viewed user unsuccess", error_code = "" }
    else
        update_viewed(d)
        ret = { success = true, error_msg = "update viewed user success", error_code = "" }
    end
end
print(cjson.encode(ret))