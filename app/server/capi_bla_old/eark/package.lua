#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local line_esign = require "line_esign_noti"
prog.timeout = 1000 * 60
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end
function count(cur)
	local ret = 0

	for i,v in cur:pairs() do
	    ret = ret + 1
	end

	return ret
end
function set_dupicate_array(arr) 
    local hash = {}
    local res = {}   
    for _, v in ipairs(arr) do
       if (not hash[v]) then
           res[#res+1] = v
           hash[v] = true
       end
    end
    local total = {}
    for key, value in pairs(hash) do table.insert(total, key) end
    return total
end

function round(x, n)
    n = math.pow(10, n or 0)
    x = x * n
    if x >= 0 then x = math.floor(x + 0.5) else x = math.ceil(x - 0.5) end
    return x / n
end

function check_packages(t)
	local ret = nil
        ekyc_user = 0
        storage_size = 0
        use_form = {form1 = false,form2 = false,form3 = false,form4 = false,form5 = false,form6 = false,form7 = false}
        advancedFunctions = false
	for k, v in t:pairs() do
	 
        --ngx.log(ngx.INFO, '['.. v.package..'] = '..tostring(v.advance_func))
		--ngx.say('['.. v.package..'] = '..tostring(v.advance_func))
        --ngx.exit(200)

		-- if(v.package~='addon')then
		-- 	ret = v
	    -- else 
		--     -- compute option 1  advance
		-- 	if(v.advance_func == true)then
        --       advancedFunctions = true
		-- 	end
        -- end
        if(v.package ~= 'addon' and v.package ~= 'dipchip' )then
            ret = v
        end

        
        if(v.package ~= 'dipchip')then
            -- advance
            if(v.advance_func == true)then
                advancedFunctions = true
            end
            --  ekyc 
            ekyc_user = ekyc_user + (v.user_ekyc or 0) 

            -- storage
            if(#v.storage > 0)then
                storage_size = storage_size + tonumber(v.storage[1].size) 
            end

            -- form
            if(#v.form > 0)then
                if(v.form[1].form_1) then
                    use_form.form1 = v.form[1].form_1
                end
                if(v.form[1].form_2) then
                    use_form.form2 = v.form[1].form_2
                end
                if(v.form[1].form_3) then
                    use_form.form3 = v.form[1].form_3
                end
                if(v.form[1].form_4) then
                    use_form.form4 = v.form[1].form_4
                end
                if(v.form[1].form_5) then
                    use_form.form5 = v.form[1].form_5
                end
                if(v.form[1].form_6) then
                    use_form.form6 = v.form[1].form_6
                end
                if(v.form[1].form_7) then
                    use_form.form7 = v.form[1].form_7
                end
            end

        end --  ~= dipchip
        
    end
 
	return ret
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("package_ss")
        col_member = db:get_col("company_members")
    end




-- get member company
local verified_ekyc = 0

get_member = col_member:find({ company_id = object_id.new(convertHexStringToNormal(session.data.company_id)) })
for a,n in get_member:pairs() do
    n._id = nil
    
    col_ekyc = db:get_col("esig_user")
    check_ekyc = col_ekyc:find_one({adminEmail = n.email})
    
    if check_ekyc then
        if check_ekyc.verified_ekyc then
            verified_ekyc = verified_ekyc + 1
        end
    else
        verified_ekyc = verified_ekyc + 0
    end

end

-- end member

local pack_user = {}
packages_count = col:find({company_id = assert(session.data.company_id) , active_package = true, package = {['$ne']='dipchip'}})
for i, v in packages_count:pairs() do
    v._id = nil
    table.insert(pack_user, v)
end

packages = col:find({company_id = assert(session.data.company_id) , active_package = true , package = {['$ne']='dipchip'}})

if #pack_user > 0 then
    mainPack = check_packages(packages) 
  
else
    mainPack ={package=""}
    mainPack.package  = "free"
    ekyc_user = 0
    storage_size = 0
    use_form = {form1 = false,form2 = false,form3 = false,form4 = false,form5 = false,form6 = false,form7 = false}
    max_user_pack = 3
    advancedFunctions = false
end

        -- check storage
        local storage_used = 0
        local doc_used = 0     
        local member = 0
        ctx       = ctx or common.mongo_ctx()
        local _u  = ctx.dopa:get_col('esig_user'):find_one({adminEmail = session.data.username}) 
        docs, err = ctx.col['edoc_list']:find({company_id=_u.company_id})

        db_edoc = conn:new_db_handle("edoc")
	    col_edoc = db_edoc:get_col("edoc_list")
        query_owner = { owner = session.data.username, is_template={['$ne']=true}}
        query_signer = {signers={['$elemMatch']={email = session.data.username}}}
        query_signer_group = {signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = session.data.username }}}}}



        now_month = os.date('%m',(os.time())+ (60 * 60))
        now_year = os.date('%Y',(os.time())+ (60 * 60))
        now = os.time{year = now_year, month = now_month, day = '00',hour = "17",min = "00",sec = "00"}
        start_date_of_month = os.date("%Y-%m-%d %H:%M:%S",numStart)
        
        now_date_of_month = (os.time()*1000)
       
        start_day =  bson.get_utc_date(now * 1000)
        last_day = bson.get_utc_date(now_date_of_month)

        query_doc_montt = {owner = session.data.username ,createdDtm = {['$gte'] = start_day , ['$lte'] = last_day} }
        doc_per_month = count(col_edoc:find(query_doc_montt))
        
        -- a = col_edoc:find(query_doc_montt)
        
        -- aa = {} 
        -- for k, v in a:pairs() do 
        --     v._id = nil
        --     ngx.say(v.createdDtm)
        -- end
        
        

        s = col_edoc:find(query_owner)
        s2 = col_edoc:find(query_signer)
        s3 = col_edoc:find(query_signer_group)
        t = {}

        for k, v in s:pairs() do 
            table.insert(t, v.id) 
        end
        for k, v in s2:pairs() do 
            table.insert(t, v.id) 
        end
        for k, v in s3:pairs() do 
            table.insert(t, v.id) 
        end
        local count_owner = count(col_edoc:find(query_owner))
        local count_signer = count(col_edoc:find(query_signer))
        local count_signer_group = count(col_edoc:find(query_signer_group))
        -- ngx.say(count_owner)
        -- ngx.say(count_signer)
        -- ngx.say(count_signer_group)
        get_total_doc = set_dupicate_array(t)
        -- ngx.say(#total_doc)
        total_doc = #get_total_doc
    --    total_doc = count_owner + count_signer + count_signer_group
        local user_com  = ctx.dopa:get_col('esig_user'):find({company_id = session.data.company_id}) 

        for i, v in docs:pairs() do
            doc_used = doc_used + 1
            storage_used = storage_used + (v.filesize or 0)
        end

        for i, v in user_com:pairs() do
            member = member + 1
        end

        local is_project = false
        local time = os.time()*1000
        local is_exp = ( mainPack.package == "PROJECT" and mainPack.active_expire_date and time > mainPack.active_expire_date)
        if mainPack.package == "PROJECT" and is_exp == true then
            is_project = true
        end

        local count_date = 0
        if mainPack.package ~= "free" then
            -- count_date = math.ceil(math.abs((mainPack.active_expire_date - os.time()*1000) / (1000*60*60*24))) or 0
            count_date = round((mainPack.active_expire_date - os.time()*1000) / (1000*60*60*24)) or 0
            if count_date < 0 then count_date = 0 end
        end

        level_map = { free = 0 ,basic = 1, advanced = 2, premium = 3, FREE_TRIAL = 3, PROJECT = 3 }
        user_pack = { free = 3,basic = 5, advanced = 10, premium = 20, FREE_TRIAL = 3, PROJECT = 999 }
        storage_pack = { free = 0, basic = 20, advanced = 20, premium = 50, FREE_TRIAL = 5, PROJECT = 20 }

        package_level = level_map[mainPack.package] or 0
        ekyc_user = ekyc_user + (user_pack[mainPack.package] or 3)
        storage_size = storage_size + (storage_pack[mainPack.package] or 0)
        max_user_pack = user_pack[mainPack.package] or 3
        if mainPack.old_coupon == "PROJECT14" then
            max_user_pack = 10
        end

        max_stor = 1073741824 * storage_size

        -- check use storage
        if(storage_used < max_stor) then
            used_storage = true
        else
            used_storage = false
        end

        -- ngx.say(advancedFunctions)

        --assert(mainPack, 'No main package')

        html = [[

        var _package =  {
            package : ':package' ,
            package_level : :level,
            used_doc : :doc_used,
            member : :member,
            storage_used : :storage_used,
            verified_ekyc : :verified_ekyc,
            doc_per_month: :doc_per_month,
            purchasedOptions : {
                advancedFunctions : :advancedFunctions
            },
            isAdvancedFunctionEnabled : function(){
            return (this.package=='premium' || this.purchasedOptions.advancedFunctions === true)
        } ,
            isAdvancedSignerFieldsEnabled : function(){
            return (this.package_level>=2 || this.purchasedOptions.advancedFunctions === true)
        },
        MaxUserEkyc : {
            user : :ekyc_user
            },
        MaxStorage : {
            storage : :storage_size
            },
        Form:{
            form1: :use_form.form1,
            form2: :use_form.form2,
            form3: :use_form.form3,
            form4: :use_form.form4,
            form5: :use_form.form5,
            form6: :use_form.form6,
            form7: :use_form.form7,
        },
        Storageusage:{
                used: :used_storage
        },
        UserPackage:{
            user: :max_user_pack
        },
        is_exp_project: :is_project,
        countDate: :count_date,
        }

        ]]


        html = string.gsub(html , ":package" , mainPack.package or "free")
        html = string.gsub(html , ":level" , tostring(package_level))
        html = string.gsub(html , ":doc_used" , tostring(total_doc))
        html = string.gsub(html , ":member" , tostring(member))
        html = string.gsub(html , ":doc_per_month" , tostring(doc_per_month))
        html = string.gsub(html , ":verified_ekyc" , tostring(verified_ekyc))
        html = string.gsub(html , ":storage_used" , tostring(storage_used))
        html = string.gsub(html , ":advancedFunctions" , tostring(advancedFunctions))
        html = string.gsub(html , ":ekyc_user" , tostring(ekyc_user))
        html = string.gsub(html , ":storage_size" , tostring(storage_size))
        html = string.gsub(html , ":use_form.form1" , tostring(use_form.form1))
        html = string.gsub(html , ":use_form.form2" , tostring(use_form.form2))
        html = string.gsub(html , ":use_form.form3" , tostring(use_form.form3))
        html = string.gsub(html , ":use_form.form4" , tostring(use_form.form4))
        html = string.gsub(html , ":use_form.form5" , tostring(use_form.form5))
        html = string.gsub(html , ":use_form.form6" , tostring(use_form.form6))
        html = string.gsub(html , ":use_form.form7" , tostring(use_form.form7))
        html = string.gsub(html , ":used_storage" , tostring(used_storage))
        html = string.gsub(html , ":max_user_pack" , tostring(max_user_pack))
        html = string.gsub(html , ":is_project" , tostring(is_project))
        html = string.gsub(html , ":count_date" , tostring(count_date))
        
        ngx.say(html)
        
        
 
