#!/usr/bin/env lua
local cjson = require 'cjson'
local template = require "resty.template"      -- OR
local template = require "resty.template.safe" -- return nil, err on errors


local output = template.process ("css/style_checkdoc.css", {
    CI_HEADER = "#222222",
    -- CI_COLOR = "#F05522",
    -- CI_CIRCLE_ACTIVE = "#222222",
    -- CI_IN_CIRCLE_ACTIVE = "#ffffff",
    -- CI_NUMBER="#F05522",
    -- CI_BUTTON = "#222222",
    -- CI_TEXT="#F05522" 
})
-- local output2 = template.process ("ekyc/css/index2_template.css", {  
--     CI_HEADER = "#222222",
--     CI_COLOR = "#F05522",
--     CI_CIRCLE_ACTIVE = "#222222",
--     CI_IN_CIRCLE_ACTIVE = "#ffffff",
--     CI_NUMBER="#F05522",
--     CI_BUTTON = "#222222",
--     CI_TEXT="#F05522" 
-- })
ngx.say(output)