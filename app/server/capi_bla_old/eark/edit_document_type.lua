#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { name_document_type = arg[1] }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("document_type")
    doc = { _id = object_id.new(convertHexStringToNormal(body._id)) or "N/A" }
    d, err = col:find_one(doc)

    local date = os.date('%d/%m/%Y %H:%M:%S', os.time() + (7*60*60))

    if d then

        local update_cmd = {["$set"] = { 
            name_doc_type = body.name_doc_type, 
            signers = body.signers,
            use_signing_order = body.use_signing_order,
            user_update = body.user_update,
            updateDtm = date
        }}
        n,err = col:update({ _id = d._id }, update_cmd, 0, 0, true)

        if n then 
            ret = { success = true, error_msg = "Edit document type success" }
        else
            ret = { success = false, error_msg = "Edit document type unsuccess" }
        end

    else 
        ret = { success = false, error_msg = "Not document type" }
    end


    

   


end

print(cjson.encode(ret))
