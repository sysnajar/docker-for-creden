#!/usr/bin/env lua
local bson = require "resty-mongol.bson"
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local md5 = require 'md5'
local scrypt = require "resty.scrypt"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil
local redis = require 'redis'
common = require "common"
rc = redis.connect('127.0.0.1', 6379)
prog.timeout_fatal = false

function has_sso_acount(email)
    db  = conn:new_db_handle("sso") 
    col_pre = db:get_col("auth")
    local ret = false

    return ret
end

function insert_auth(body) 
    db  = conn:new_db_handle("sso") 
    col_pre = db:get_col("auth")

    --body = {email = "", hashedPwd = "", firstname = "", lastname = ""}

    body.created_dtm = bson.get_utc_date(ngx.now() * 1000)
    i, err = col_pre:insert({body}, nil, true)
    if err then
        ret = {success = false, error_msg = "insert user unsuccess", error_code = ""}
    else
        

      
    end

end

function insert_pre_register(body) 
    db = conn:new_db_handle("dopa")
    col_pre = db:get_col("pre_register")

    body.created_dtm = bson.get_utc_date(ngx.now() * 1000)
    i, err = col_pre:insert({body}, nil, true)
    if err then
        ret = {success = false, error_msg = "insert user unsuccess", error_code = ""}
    else
        math.randomseed(os.clock()*100000000000)
        otp = ''
        token = ''
	    for i=1,3 do
		    a = math.random(10, 99)       
		    otp = otp .. a
        end
        for i=1,10 do
            a = math.random(10, 99)       
            token = token .. a
        end

        key = 'otp_' .. body.adminEmail .. '_' .. token .. '_' .. otp
        rc:set(key, body.activeCode)
        rc:expire(key, 600)

        local l, err = prog('sh', '/home/creden/work/app/server/capi/register_line.sh', body.adminEmail) -- Notification Line
        local m, err = prog('node', '/home/creden/work/app/server/capi/register_mail_bla.js', body.adminEmail, otp) -- Send email register
        ret = { success = true, error_msg = "insert user success", error_code = "", email = body.adminEmail, token = token }
    end

end

function user_not_active(body)
    math.randomseed(os.clock()*100000000000)
    otp = ''
    token = ''
    for i=1,3 do
        a = math.random(10, 99)       
        otp = otp .. a
    end
    for i=1,10 do
        a = math.random(10, 99)       
        token = token .. a
    end

    key = 'otp_' .. body.adminEmail .. '_' .. token .. '_' .. otp
    rc:set(key, body.activeCode)
    rc:expire(key, 600)

    local m, err = prog('node', '/home/creden/work/app/server/capi/register_mail_bla.js', body.adminEmail, otp) -- Send email register
    ret = { success = true, error_msg = "send mail active success", error_code = "", email = body.adminEmail, token = token }
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body  = { adminEmail = arg[1] } 
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1', 27017)
if err then
    ret = { success = false, error_msg = "connect mongo fail", error_code = "" }
else

    body.adminEmail = string.lower(body.adminEmail)
    math.randomseed(os.clock()*100000000000)
    code = ''
    for i=1,2 do
        code = math.random(10, 99) .. code
    end
    body.activeCode = code
    body.activeStatus = 1
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    s, err = col:find_one({ adminEmail = body.adminEmail or "N/A" }, nil, true) 

    -- Facebook Login and Register
    if body.is_fb then

        if body.social == true then
            body.login_service = "esig"
            body.login_in_service = "signature"
            body.social = nil
            insert_pre_register(body)

        elseif not s then

            math.randomseed(os.clock()*100000000000)
            id_social = ''
            for i=1,2 do
                id_social = math.random(10, 99) .. id_social
            end
            local key_social = "social_" .. body.adminEmail .. "_" .. id_social
            rc:set(key_social , cjson.encode(body))
            rc:expire(key_social, 600)
            local data = { id_social = id_social, email = body.adminEmail }        
            ret = { success = false, error_msg = 'facebook register', error_code = "", data =  data }

        elseif s and s.activeStatus ~= 4 then
            user_not_active(s)
        else

            s._id = nil
            s.adminPassword = nil;
            s.adminPasswordC = nil;
            s.encryptPass = nil
            s.hashedPassword = nil
            s.cvv_code = nil
            s.txtPid = nil
            s.date_of_birth = nil

            math.randomseed(os.clock()*100000000000)
            extend_key = ''
            for i=1,2 do
                a = math.random(10, 99)       
                extend_key = extend_key .. a
            end
            rc:set('extend_key' .. ':' .. s.adminEmail .. ':' .. extend_key, "true")
            rc:expire('extend_key' .. ':' .. s.adminEmail .. ':' .. extend_key, 60*30)
            ngx.log(ngx.NOTICE,"C SET SESSION TO " .. tostring(s.adminEmail))
            local session = require "resty.session".start{ secret = "4321" }
            session.data.username = s.adminEmail
            session.data.custCode = s.adminEmail
            session.data.company_id = s.company_id
            ngx.log(ngx.NOTICE,"body.username company_id fb = " .. tostring(s.company_id))
            session:save()
            local h = ngx.req.get_headers()
            --Persistence login
            if(true)then
              common.create_persistence_token(s.adminEmail, h['x-real-ip'] or '2.1.1.1')
            end
            ret = { success = true, error_msg = 'facebook login success', error_code = "", data = s, username = s.adminEmail, custCode = s.adminEmail, extend_key = extend_key }

        end

    --  Google Login and Register
    elseif body.is_gamil then

        if body.social == true then
            body.login_service = "esig"
            body.login_in_service = "signature"
            body.social = nil
            insert_pre_register(body)

        elseif not s then

            math.randomseed(os.clock()*100000000000)
            id_social = ''
            for i=1,2 do
                id_social = math.random(10, 99) .. id_social
            end
            local key_social = "social_" .. body.adminEmail .. "_" .. id_social
            rc:set(key_social , cjson.encode(body))
            rc:expire(key_social, 600)
            local data = { id_social = id_social, email = body.adminEmail }        
            ret = { success = false, error_msg = 'google register', error_code = "", data =  data }

        elseif s and s.activeStatus ~= 4 then
            user_not_active(s)

        else

            s._id = nil
            s.adminPassword = nil;
            s.adminPasswordC = nil;
            s.encryptPass = nil
            s.hashedPassword = nil
            s.cvv_code = nil
            s.txtPid = nil
            s.date_of_birth = nil

            math.randomseed(os.clock()*100000000000)
            extend_key = ''
            for i=1,2 do
                a = math.random(10, 99)       
                extend_key = extend_key .. a
            end
            rc:set('extend_key' .. ':' .. s.adminEmail .. ':' .. extend_key, "true")
            rc:expire('extend_key' .. ':' .. s.adminEmail .. ':' .. extend_key, 60*30)
            ngx.log(ngx.NOTICE,"C SET SESSION TO " .. tostring(s.adminEmail))
            local session = require "resty.session".start{ secret = "4321" }
            session.data.username = s.adminEmail
            session.data.custCode = s.adminEmail
            session.data.company_id = s.company_id
            ngx.log(ngx.NOTICE,"body.username company_id gamil = " .. tostring(s.company_id))
            session:save()
            local h = ngx.req.get_headers()
            --Persistence login
            if(true)then
              common.create_persistence_token(s.adminEmail, h['x-real-ip'] or '2.1.1.1')
            end
            ret = { success = true, error_msg = 'google login success', error_code = "", data = s, username = s.adminEmail, custCode = s.adminEmail, extend_key = extend_key }
            
        end

    -- Creden Register 
    else

        if not s then
            local key_reserve = "reserve." .. body.adminEmail
            rc:set(key_reserve, 1)
            rc:expire(key_reserve, 600)
        
            body.login_service = "esig"
            body.login_in_service = "signature"
            body.adminPassword = body.adminPassword or ''
            if body.adminPassword ~= '' then
                body.adminPasswordC = ''
                body.encryptPass = md5.sumhexa(body.adminPassword) 
                body.hashedPassword = scrypt.crypt(tostring(body.adminPassword))
                body.adminPassword = ''
                body.adminPasswordC = ''
            end
            insert_pre_register(body)
            if(not has_sso_acount(body.adminEmail))then
                insert_auth(body)
            end
            
        else 
            ret = { success = false, error_msg = 'Email already exists.', error_code = "" }
        end
    
    end
        
end


print(cjson.encode(ret))


