#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson.safe'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      exec = require'resty.exec'
      prog = exec.new('/tmp/exec.sock')
      prog.timeout_fatal = false
      id = arg[1]
      return true
    else
      return false	
    end 
end
    --id = 'demo@creden.co'
    --{"EBANK_sumcashin6mth":353871.14,"EBANK_finBalance":2785.49,"EBANK_sumcashout6mth":351335.75,"EBANK_iniBalance":51.1}
    local fn =  function(id)
        local key = 'TRAN:SOC:' .. id
        local t = cjson.decode(rc:get(key))
		local ABOVE_5_YEARS = 'false'

        --cjson = require 'cjson.safe'
        --local t = '{"year":10,"friend":411}'
        --t = cjson.decode(t)
        local score = 0 --total = 190

    		if(t==nil) then
               return 0
            end
        
        t.FB_netsize = tonumber(t.FB_netsize)

        --if (t.FB_year or 0) > 5 then score = 20 end

        if t.FB_netsize > 300 then score = score + 30 end
        if t.FB_netsize > 500 then score = score + 50 end

        local res, err = prog('xvfb-run','-a','node', '/home/creden/facebook.js', t.FB_id)
        if res.stdout then
          r = res.stdout
          r = r:gsub("^%s*(.-)%s*$", "%1")
          y = ''
          for i in string.gmatch(r, "%S+") do
             y = i
          end
          if 2018 - tonumber(y) >= 5 then
            score = score + 20
			ABOVE_5_YEARS = 'true'
          end
        end

		--demo data
		if(id=='demo@creden.co') then
		  rc:set(id .. '.fb5year', ABOVE_5_YEARS)
		end
		--demo data

        return score
    end
    
    --print(fn(1))
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
