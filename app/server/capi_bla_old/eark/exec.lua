#!/usr/bin/env lua
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')

--local res, err = prog('curl https://y1fnlipapa.execute-api.ap-southeast-1.amazonaws.com/Testing/function5?email=nutp10.1@gmail.com&code=456')
prog.timeout_fatal = false
--local res, err = prog('curl', 'https://y1fnlipapa.execute-api.ap-southeast-1.amazonaws.com/Testing/function5?email='..arg[1]..'&code='..arg[2])
--local res, err = prog('wget https://google.com')

local res, err = prog('uname')

if not err then
    print('exit code',res.exitcode)
	print('response', res.stdout)
else
    print('err', err)
end
