#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require 'resty-mongol'
local bson = require 'resty-mongol.bson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout = 1000 * 300
prog.timeout_fatal = false
local rowsObj = {}
local docObj = {}
local srcpath = '/home/creden/work/app/client/face/images/card/'
local check_apiKey = 'TdDVFZPbo8uZW3Qb2IZ0tUPSGdJe5BiV'
local apiKey = nil
local ret = {success = false}
local body = nil

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    local ok,err = conn:connect('127.0.0.1',27017)
    local result = false
    if err then
        result = false
        ret = {success = result, msg = err}        
    else
        result = true
    end
    return result
end

function chk_apiKey()
    if not apiKey or apiKey ~= check_apiKey then
        ret = {success = false, msg = 'Authentication Fail'} 
        ngx.exit(ngx.HTTP_BAD_REQUEST)
        return false
    end
end

function get_ekyc()
    rowsObj = {}    
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("ekyc")
    local qry = {process_status='finished',api_chk={['$ne']=1}}    

    local rows,err = col:find(qry)
    if (rows) then
        local c = 0;
        for i,v in rows:pairs() do
            docObj = {}
            local time3 = os.date('%d-%m-%Y %H:%M:%S', tonumber(v.createdDtm)/1000)

            --docObj.createtime = v.createTime
            docObj.txid = v.txid
            docObj.id = tostring(v._id) 
            docObj.isvalid = v.isValid
            docObj.lasercode = v.laserCode
            docObj.ekyc_level_desc = v.ekyc_level_desc
            docObj.faceconfidence = v.faceConfidence
            docObj.lname = v.lnameTH
            docObj.address = v.address
            docObj.card_reader_id = v.card_reader_id
            docObj.isfaceIdentical = v.isFaceIdentical
            docObj.isvaliddopa = v.isValidDopa
            docObj.province = v.province
            docObj.compcode = v.compCode
            docObj.birthdate = v.dob
            docObj.created_time = time3
            docObj.username = v.username
            docObj.cardNo = v.cardNo
            docObj.channel = v.channel
            docObj.ekyc_level = v.ekyc_level
            --docObj.createdate = v.createDate
            docObj.fname = v.fnameTH
            docObj.admin_email = v.adminEmail
            docObj.imgfile = v.cardImg3

            table.insert(rowsObj,docObj)
            local upd_doc = {['$set'] = {api_chk=1,api_dtm=bson.get_utc_date(ngx.now() * 1000)}}
            u, err = col:update({txid=v.txid}, upd_doc, 1, 0, true)

            c=c+1
            if (c >= tonumber(body.limit)) then
                break
            end
        end

        if (c > 0) then
            ret = {success = true, count = c, rows=rowsObj}
        else
            ret = {success = false, msg = "Zero rows"}
        end

    else
        ret = {success = false, msg = err}
    end
end

if(ngx)then
    apiKey = ngx.req.get_headers().apiKey
    print = ngx.say
else
   body  = {limit=5} 
end

chk_apiKey()

if( ngx.var.request_method =='GET') then
    body = ngx.req.get_uri_args()
    if connect_mongo() then
        get_ekyc()
    end
end

print(cjson.encode(ret))