local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
bson = require "resty-mongol.bson"
common = require "common"
ctx   = ctx or common.mongo_ctx()
rc = redis.connect( '127.0.0.1', 6379)

function create_userCompany(email)

	user = ctx.dopa:get_col("esig_user"):find_one({ adminEmail = email})	
	del_id = '9999' .. user.adminEmail .. os.time()
	
	local doc = {
		del_id       = del_id,
		companyName  = "",
		create_by    = user.adminEmail,
		api_key      = '9999'.. user.adminEmail,
		createdDtm   = bson.get_utc_date(ngx.now() * 1000)
	}

	if user.usability == "company" then 
		doc.companyName = user.company_name or ""
	end

	ctx.dopa:get_col("user_company"):insert({doc}) 
	company = ctx.dopa:get_col("user_company"):find_one({ create_by = user.adminEmail })
	
	if company._id then
		add_member(user.adminEmail, company._id, del_id, user._id)
		ctx.dopa:get_col("esig_user"):update({adminEmail = user.adminEmail},{['$set']={ company_id = tostring( company._id) }})	
		return tostring(company._id)
	end

end
	
function add_member(email, c_id, d_id, u_id)
	local doc = {
		del_id       = d_id,
		status       = "APPROVE",
		email = email,
		created_by   = email,
		role         = "ADMIN",
		createdDtm   = bson.get_utc_date(ngx.now() * 1000),
		positions    = {{position_name = "N/A", position_id = del_id}},
		company_id = c_id,
		esig_user_id = u_id,
	}
	
	ctx.dopa:get_col("company_members"):insert({doc}) 
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {token = args[1],otp = args[2]} 
end

 
	token = body.token
	otp = body.otp
	body.adminEmail = body.adminEmail:lower()
	key = 'otp_'.. body.adminEmail .. '_' .. token .. '_' ..otp
	code = rc:get(key)

	if code then

		conn = mongo()
		conn:set_timeout(5000)
		ok, err = conn:connect('127.0.0.1',27017)

		if not ok then
			ret = { success = false, error_msg = "connect mongo fail", error_code = "" }
		else
			db = conn:new_db_handle("dopa")
			col_pre = db:get_col("pre_register") 
			col_esig = db:get_col("esig_user") 
	   
			body.adminEmail = body.adminEmail:lower()

			s = col_pre:find_one({ adminEmail = body.adminEmail or "N/A", activeCode = code or "N/A" })
			esig = col_esig:find_one({ adminEmail = body.adminEmail or "N/A", activeCode = code or "N/A" })
			if s then

				if(s.activeStatus ~= 4) then

					s._id = nil
					s.activeStatus = 4
					i, err = col_esig:insert({s}, nil, true)
					d, err2 = col_pre:delete({ adminEmail = s.adminEmail })
					s.adminPassword = nil;
					s.adminPasswordC = nil;
					s.encryptPass = nil
					s.hashedPassword = nil
					s.cvv_code = nil
					s.txtPid = nil
					s.date_of_birth = nil

					if err then
						ret = {success = false, error_msg = "insert user unsuccess", error_code = ""}
					else
						local company_id = create_userCompany(s.adminEmail)

						math.randomseed(os.clock()*100000000000)
						local extend_key=''
						for i=1,2 do
							a = math.random(10, 99)       
							extend_key = extend_key .. a
						end
						rc:set('extend_key'..':'..body.adminEmail..':'..extend_key, "true")
						rc:expire('extend_key'..':'..body.adminEmail..':'..extend_key, 60*30)

						ngx.log(ngx.NOTICE,"C SET SESSION TO " .. tostring(s.adminEmail))
						local session = require "resty.session".start{ secret = "4321" }
						session.data.username = s.adminEmail
						session.data.custCode = s.adminEmail
						session.data.company_id = company_id
						s.company_id = company_id
              			ngx.log(ngx.NOTICE,"body.username company_id = " .. company_id)
						session:save()
						ret = {success = true, error_msg = "insert user success", error_code = "", data = s, custCode = body.adminEmail , username = body.adminEmail, extend_key = extend_key, c_id = session.data.company_id }
						rc:del(key)
					end

				else
					ret = {success = false, error_msg = "activeStatus error", error_code = ""}
				end
	
			
			elseif esig and esig.activeStatus ~= 4 then

				local update = {["$set"] = { activeStatus = 4 }}
				u, err = col_esig:update({ adminEmail = esig.adminEmail }, update, 0, 0, true)

				if err then
					ret = {success = false, error_msg = "update user unsuccess", error_code = ""}
				else
					local company_id = create_userCompany(esig.adminEmail)
					esig._id = nil
					esig.adminPassword = nil;
					esig.adminPasswordC = nil;
					esig.encryptPass = nil
					esig.hashedPassword = nil
					esig.cvv_code = nil
					esig.txtPid = nil
					esig.date_of_birth = nil

					math.randomseed(os.clock()*100000000000)
					local extend_key=''
					for i=1,2 do
						a = math.random(10, 99)       
						extend_key = extend_key .. a
					end
					rc:set('extend_key'..':'..esig.adminEmail..':'..extend_key, "true")
					rc:expire('extend_key'..':'..esig.adminEmail..':'..extend_key, 60*30)

					ngx.log(ngx.NOTICE,"C SET SESSION TO " .. tostring(esig.adminEmail))
					local session = require "resty.session".start{ secret = "4321" }
					session.data.username = esig.adminEmail
					session.data.custCode = esig.adminEmail
					session.data.company_id = company_id
					esig.company_id = company_id
					session:save()
					ret = {success = true, error_msg = "insert user success", error_code = "", data = esig, custCode = esig.adminEmail , username = esig.adminEmail, extend_key = extend_key }
					rc:del(key)
				end

			else
				ret = {success = false, error_msg = "not valid code1", error_code = ""}
			end

		end

	else
	ret = { success = false, error_msg = "not valid code2", error_code = "" }
end
print(cjson.encode(ret))
