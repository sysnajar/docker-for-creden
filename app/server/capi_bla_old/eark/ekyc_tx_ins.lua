#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local bson = require 'resty-mongol.bson'
local exec = require'resty.exec'
local md5 = require 'md5'
local conn = mongo()
local ret = {success=false}

function getConnectMongo()
    conn:set_timeout(5000)
    local ok,err = conn:connect('127.0.0.1',27017)
    if (not ok) then
        ret = {success = false, msg = err}
    end
    return ok;
end

--Insert Tx
function InsertDoc()
    local db = conn:new_db_handle('dopa')
    local col = db:get_col('ekyc_tx')
    local dat = {}
    dat = body
    dat.created_dtm = bson.get_utc_date(ngx.now() * 1000)
    --print(cjson.encode(dat))
    local ins_res, err = col:insert({dat})
    if not err then
        ret = {success = true}
    else
        ret = {success = false, msg = err}
    end    
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {txid='9b7076a59eea41ecb4586a97ee80409b',
        cid='1234567890123',
        fname='FirstName',
        lname='LastName',
        bdate='2000-1-1',
        fpercent='50.00',
        remark='test1234567890'}
end
        ngx.log(ngx.NOTICE,"dat  connect===== " .. cjson.encode(body))
		
if getConnectMongo() then
    InsertDoc()    
end

print(cjson.encode(ret))
