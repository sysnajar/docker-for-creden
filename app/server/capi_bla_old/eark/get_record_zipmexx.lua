#!/usr/bin/env lua

local body = nil
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local redis = require 'redis'
local object_id = require "resty-mongol.object_id"

local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local isValidLED = false
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local led_desc

local max_update   = 1000
local update_count = 0

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)

else
    body = {capcha='1103700530312',ref_no='พัทธพงศ์'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function chk_led(id,cardno)
    local idcard = cardno
    local ref_no = id
	local ret  = false
	local desc = nil
    local table_LED = nil

    print("Go del key===",idcard,ref_no)
    
    local key1 = 'user_capture_cardNo'..ref_no
    local key2 = 'image_cap'..ref_no
    local key3 = 'settext_detail_cardNo'..ref_no
    rc:del(key1)
    rc:del(key2)
    rc:del(key3)
    
    print("Go to===",idcard,ref_no)

    local res1,err1 = prog("node", "/home/creden/work/app/server/capi/auto_LED_bot.js", ref_no, idcard , "true")
    local res2,err2 = prog("lua", "/home/creden/work/app/server/capi/update_LED_text_API.lua", ref_no)

    if ( (not err1) and (not err2) ) then
        local led_res = tostring(res2.stdout)    
            print('LED  result = ' .. led_res)
        local tmp = cjson.decode(led_res)
        
        table_LED = tmp.data_LED or 'N/A'
        desc   = (tmp and tmp.data_LED and tmp.data_LED[1]) and tostring(tmp.data_LED[1].title) or 'N/A'
        local index    = desc:find('ไม่พบรายการ')
        if(index) then ret = true end
    else

   
        
    end

	return ret, desc,table_LED
end



    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    tmp = {}
    data = {}
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("ekyc_zipmex_col")
        
        --local s = col:find( {status = { ['$ne'] = 'complete' }} )
       local s = col:find( {status = 'complete', data_LED='N/A' } )

        local count = 0
        local status = false
        local createdDtm2
        -- local isValidLED = false
        local x = 0
        local tmp_csv = {"rererence no","id","isValidDopa","dopaRemarks" ,"isValidFace" , "facePct","isValidLED","ledDesc"}
        for k, v in s:pairs() do 
            count = count + 1
	        
	        if(v.cardNo ~= '1103700530312') then  --check dummy ID card
            
                isValidLED,led_desc,data_full_LED = chk_led(tostring(v._id),v.cardNo)
                createdDtm2 = bson.get_utc_date(os.time() * 1000)
		
		
		--print(v.cardNo, isValidLED, led_desc)
		--if(count>10)then break end
               

		
                update = {["$set"] = {data_LED = data_full_LED,status = 'complete',createdDtm_led = createdDtm2}}
                --local query = { _id = object_id.new(convertHexStringToNormal(session.data.company_id))}
                u, err_u = col:update({_id=object_id.new(convertHexStringToNormal(tostring(v._id)))},update, 0, 0, true)
                if not u then
                    ret.error_msg = err_u
                else
                    
                    ret.msg2 = 'update success'
                    ret.success = true
                    print(cjson.encode(ret))
                end
               
		-- check max update
		update_count = update_count+1
		print('updated' , update_count .. '/' .. max_update, v._id)
		print(isValidLED, led_desc)

		if(max_update ~= -1 and update_count >= max_update)then
		   print('exit code A')
		   break; --eark test
		end
		

	      else
		   print('skipped LED dummy ID card ['.. v.cardNo..']')   

               end -- 1103700530312

          
        end
   
	print('exit code B')
       

            
            -- if(count > 0 and count < 11) then
            -- print(k,tostring(v._id))
            -- end
            --print(count)
            
            -- if(v.ref_no ~= nil and v.ref_no ~= "555555" and v.data_LED ~= nil) then
            --     if(count > 1 and count < 560) then
            --         x = x +1
            --     if(v.remark == "สถานะปกติ (ข้อมูลถูกต้อง)") then
            --         status = true
            --     end
            --     if(v.data_LED ~= nil) then
            --         isValidLED = true
            --     end
            --     local dd = os.date("%d/%m/%Y",tonumber(v.createdDtm_led)/1000)
            --    -- print(v.data_LED)
            --      local s = tostring(x) .. "," .. v.ref_no .. "," .. v.cardNo .. "," ..tostring(status).. "," ..tostring(v.remark)..","..tostring(v.isIdentical)..","..tostring(v.faceMatchPercent)..","..tostring(isValidLED)..","..cjson.encode(v.data_LED[1].title)..","..tostring(dd)
            --     -- v._id = tostring(v._id)
           
          
            --      print(s..'\n')
            --     end
            -- end


    

--end


