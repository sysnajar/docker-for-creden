#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { company_id = "", email = "",  }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    local key = "invite_company_" .. body.email
    local invite = rc:get(key)

    if invite then
        db = conn:new_db_handle("dopa")
        col_esig = db:get_col("esig_user")
        col_member = db:get_col("company_members")
        m, err = col_member:find_one({ company_id = object_id.new(convertHexStringToNormal(body.company_id)), email = body.email })
        e, err = col_esig:find_one({ adminEmail = body.email })

        if m and e then

            m_key = { company_id = object_id.new(convertHexStringToNormal(body.company_id)), email = body.email }
            if body.confirm == "YES" then            

                -- Update company member  
                m_update_cmd = {
                    status = "APPROVE",
                    updatedDtm = bson.get_utc_date(ngx.now() * 1000),
                    esig_user_id = e._id
                }
                local u_member, err_member = col_member:update(m_key, {["$set"] = m_update_cmd}, 0, 0, true)

                -- Update Esig user
                e_key = { adminEmail = body.email }
                e_update_cmd = { company_id = tostring(body.company_id) }
                local u_esig, err_esig = col_esig:update(e_key, {["$set"] = e_update_cmd}, 0, 0, true)

                local key2 = "companyof." .. body.email
                rc:del(key2)

                if u_member and u_esig then
                    -- e._id = nil
                    -- e.adminPassword = nil;
                    -- e.adminPasswordC = nil;
                    -- e.encryptPass = nil
                    -- e.hashedPassword = nil
                    -- e.cvv_code = nil
                    -- e.txtPid = nil
                    -- e.date_of_birth = nil
                    -- e.company_id = session.data.company_id
                    ret = { success = true, error_msg = "update company and esig user success", error_code = "" }
                else
                    ret = { success = false, error_msg = "update company and esig user unsuccess", error_code = "" }
                end

            else 

                m_update_cmd = {
                    status = "UNAPPROVE",
                    updatedDtm = bson.get_utc_date(ngx.now() * 1000),
                    esig_user_id = e._id
                }
                local u_member, err_member = col_member:update(m_key, {["$set"] = m_update_cmd}, 0, 0, true)         
                ret = { success = true, error_msg = "reject company", error_code = "" }

            end

            rc:del(key)

        else
            ret = { success = false, error_msg = "not have member or user", error_code = "" }
        end

    else
        ret = { success = false, error_msg = "not have invite", error_code = "" }
    end

end
print(cjson.encode(ret))