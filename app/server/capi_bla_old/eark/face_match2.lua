#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body = {selfie_file = arg[1], old_txid = arg[2], txid = arg[3], usr = arg[4]}
end

-- body.selfie_file = body.selfie_file:gsub(".png", "")

function chk_face(rec_face_file, i)
    local t = {}
    local faceId1 = detect_face(rec_face_file)
    local faceId2 = detect_face(body.selfie_file)
    t = verify_face(faceId1, faceId2)
    rc:set('face.recognize.'..body.txid..'.'..i, (t.faceMatchPercent or 0) * 100)
end
  
function detect_face(img)
    local faceId = ''
    local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
    if not err then
        local tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end
  
function verify_face(faceId1,faceId2)
    local t = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
    if not err then
        local tmp = cjson.decode(res.stdout)
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
    end
    return t
end

local pass = 3
local percent = 70
local count = 0
local isFaceVerified = false
for i = 1,4 do
	local dir = '/home/creden/work/app/client/face/images/card/rec_'..body.txid..'-'..i..'.png'
	local vdo_file = '/home/creden/work/app/client/vdo/ex/'..body.old_txid..'.webm'
    local time = {'00:00:03.00', '00:00:05.00', '00:00:07.00', '00:00:09.00'}
    print(dir)
	print(vdo_file)
	print(time[i])
	--os.execute('ffmpeg -i '..vdo_file..' -ss '..time[i]..' -vframes 1 '..dir)
	local res, err = prog('ffmpeg', '-i', vdo_file, '-ss', time[i], '-vframes', '1', dir)
	if not err then
		local rec_face_file = 'rec_'..body.txid..'-'..i
		chk_face(rec_face_file, i)
		local key = 'face.recognize.'..body.txid..'.'..i
		local res = (rc:get(key) or 0)
		print(res)
		if tonumber(res) > percent then count = count + 1 end
	end
end

if count >= pass then 
	isFaceVerified = true 
end

-- rc:set('liveness.'..body.usr..'.'..body.txid..'.face', isFaceVerified)

local update_res = {success = true, username = body.usr, txid = body.txid, score = isFaceVerified, maxScore = 2, type = "face"}
local update_cmd = "lua /home/creden/work/app/server/capi/update_res.lua '"..cjson.encode(update_res).."' &"
os.execute(update_cmd)

local ret = {success = true, isFaceVerified = isFaceVerified}
print(cjson.encode(ret))
