#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'

local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = { company_id = arg[1] } 
--    body  = { company_id = "5c80af657695fdd90b9fe28c" } 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function get_count_use_ekyc() 
    -- Get user company
    local count = 0
    db = conn:new_db_handle("dopa")
    col = db:get_col("company_members")
    list = { company_id =  object_id.new(convertHexStringToNormal(body.company_id)) }
    l,err = col:find(list)

    local list_member = {}  
    for i,v in l:pairs() do 
        v._id = tostring(v._id)
        if v.email then
            table.insert(list_member, v.email)
        end 
    end

    col2 = db:get_col("ekyc")
    for i2,v2 in ipairs(list_member) do
        list2 = { adminEmail =  v2 }
        l2,err = col2:find(list2)

        for i3,v3 in l2:pairs() do 
            if v3.adminEmail and v3.isValidDopa == true and v3.isFaceIdentical == true then
                count = count + 1
            end          
        end
    end
    return count or 0
end


conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = {success = false, error_msg = err}
else

    -- 5e9d590da1fb1ce3475bf3e1 TV Direct
    -- 5c80af657695fdd90b9fe28c Creden Dev
    -- 5c597eaee0ca72081a1d4ca9 Creden Productionn
    -- 5f056252e00a1547a19367f2 Thaibispa Dev
    -- 5f0d7b3c8a2f8d0d136d8ca1 Thaibispa Production
    -- 5f2a2b7a96b733539154122f PEA Dev
    -- 5f2a2bed8a2f8d0d136e8c50 PEA Production
    -- 5f32cc12591cb630ebfd6504 Siamcompressor Dev
    -- 5f32cd6a8a2f8d0d136ee3bd Siamcompressor Production
    -- 5f59f6f5bb0b32a4320785a2 Decathlon Production
    local company = {
        { id = "5c597eaee0ca72081a1d4ca9", name = "creden" },
        { id = "5e9d590da1fb1ce3475bf3e1", name = "tvdirect" },
        { id = "5f0d7b3c8a2f8d0d136d8ca1", name = "thaibispa" },
        { id = "5f2a2bed8a2f8d0d136e8c50", name = "pea" },
        { id = "5f32cd6a8a2f8d0d136ee3bd", name = "siamcompressor" },
        { id = "5f59f6f5bb0b32a4320785a2", name = "decathlon" }
    }
    local name_company =  "N/A"
    local chk_company = false
    for i, v in ipairs(company) do
        if v.id == body.company_id then
            name_company = v.name
            chk_company = true
        end
    end

    -- data.credit_ekyc = rc:get('tvdirect.credit_ekyc')
    -- data.count_use_ekyc = rc:get('tvdirect.use_ekyc')

    data = {}
    rc = redis.connect('127.0.0.1', 6379)

    -- if body.company_id == "5e9d590da1fb1ce3475bf3e1" and body.mode == "get" then
    if chk_company == true and body.mode == "get" then
        
        data.credit_ekyc = rc:get(name_company .. '.credit_ekyc')
        data.count_use_ekyc = get_count_use_ekyc()
        data.credit_signature = rc:get(name_company .. '.credit_signature')
        data.count_use_signature = rc:get(name_company .. '.use_signature')
        data.show = true
        success = true
        error_msg = nil

    elseif chk_company == true and body.mode == "check" then

        credit_ekyc = rc:get(name_company .. '.credit_ekyc')
        count_use_ekyc = get_count_use_ekyc()
        if tonumber(count_use_ekyc) < tonumber(credit_ekyc) then
            success = true
            error_msg = nil
        else
            success = false
            error_msg = "จำนวนเครดิตครบจำนวนการใช้งาน"
            data = nil
        end

    else
        success = true
        data.show = false
        -- data.credit_ekyc = "0"
        -- data.count_use_ekyc = "0"
        -- data.credit_signature = "0"
        -- data.count_use_signature = "0"
    end
    
    ret = { success = success, data = data, error_msg = error_msg }
    print(cjson.encode(ret))
end





