#!/usr/bin/env lua
local common = require 'common'
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)


local body = common.init_post {pin = "1234"}
local ctx  = common.mongo_ctx()
local ret  = {success = false, error_msg = "N/A"}
	
	local myusername = session.data.username or "N/A" 
	
	local col = ctx.col['dopa.esig_user']

    local u, err = col:find_one({adminEmail = myusername})
    
    if(u.last_cert_info)then
       ret.success =  (body.pin == tostring(u.last_cert_info.pin))
    end
    print(cjson.encode(ret))
