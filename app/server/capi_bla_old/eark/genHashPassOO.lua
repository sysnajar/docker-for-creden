#!/usr/bin/env lua
local scrypt = require "resty.scrypt"
local cjson = require 'cjson'

if(ngx)then
	     ngx.req.read_body()
    body = ngx.req.get_body_data()
    print = ngx.say
    body = cjson.decode(body)
else
   body  = {pass = arg[1]} 
end

 a = scrypt.crypt(tostring(body.pass))
 chk = scrypt.check(body.pass, a)
 print(a)


