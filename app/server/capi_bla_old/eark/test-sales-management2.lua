-- activestatus:0, //สถานะ: 0=NotVerify 1=Activate Verify 2=NotActive
-- username:null,  //email
-- fname:null,     //ชื่อ
-- sname:null,     //สกุล
-- fullname:null,      //ชื่อเต็ม
-- level:"STAFF",     //ตำแหน่ง
-- saleteam:"(NONE)",   //ทีมขาย
-- password:null,     //รหัส
-- createtime:null,    //วันที่-เวลาสร้าง
-- createby:null,      //ผู้สร้าง
-- modifiedtime:null,  //วันที่-เวลาแก้ไขล่าสุด
-- modifiedby:null,    //ผู้แก้ไขล่าสุด
-- revision:0          //ครั้งที่แก้ไข

txt = [[
{
  "data" : [
    { 
      "username":"auddy@sale.co",
      "fname":"Hattaporn",
      "sname":"Chaicumnerd",   
      "fullname":"Hattaporn Chaicumnerd",
      "level":"ADMIN",
      "saleteam":"A",
      "createtime":"",
      "createby":"symphony",
      "modifiedtime":"",
      "modifiedby":"",
      "revision":0
    },
    {
      "username":"test3@sale.co", 
      "fname":"กุ๊กไก่",
      "sname":"ฟหกด",            
      "fullname":"กุ๊กไก่ ฟหกด",
      "level":"STAFF",
      "saleteam":"B",
      "createtime":"",
      "createby":"symphony",
      "modifiedtime":"",
      "modifiedby":"",
      "revision":0      

    },     
    {
      "username":"test1@sale.co",
      "fname":"ทดสอบ",
      "sname":"สกุล1",             
      "fullname":"ทดสอบ สกุล1",
      "level":"SUPERVISOR",
      "saleteam":"A",
      "createtime":"",
      "createby":"symphony",
      "modifiedtime":"",
      "modifiedby":"",
      "revision":0
    }, 
    {
      "username":"test2@sale.co", 
      "fname":"ทดสอบ",
      "sname":"สกุล2",            
      "fullname":"ทดสอบ สกุล2",
      "level":"SUPERVISOR",
      "saleteam":"A",
      "createtime":"",
      "createby":"symphony",
      "modifiedtime":"",
      "modifiedby":"",
      "revision":0
    },           
    {
      "username":"zeah@Sale2.co", 
      "fname":"Zeah",
      "sname":"Rider",             
      "fullname":"Zeah Rider",
      "level":"STAFF",
      "saleteam":"A",
      "createtime":"",
      "createby":"symphony",
      "modifiedtime":"",
      "modifiedby":"",
      "revision":0
    }   
  ]
}
]]
 
 

ngx.header["Content-Type"] = 'application/json';
ngx.say(txt);
