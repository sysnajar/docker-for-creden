#!/usr/bin/env lua
local cjson = require 'cjson'
local bson = require "resty-mongol.bson"
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    args  =  ngx.req.get_post_args()
else
    args  = {fb_id  = arg[1], email = arg[2]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
ret = {success = false, error_msg = "N/A"}
if not ok then
	ret = {success = false, error_msg = err}
else
	db = conn:new_db_handle("dopa")
	col = db:get_col("user_email")
    s, err = col:find_one({ key = tostring(args.fb_id) , source = 'facebook' })  
	if s then --case I. found email address
		s._id = nil
		ret = {success = true , email = s.email , reset_key = (not s.email) and s.reset_key or nil}
		ret.error_msg = err
    else
		if(not err) then 
		local doc = {key = args.fb_id, source = 'facebook'} 
		-- todo doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 

	    if(args.email and #args.email>0) then 
			doc.email = args.email  --case  II.  not found email address but fecebook sends one.
		else
			--todo use stronger random
			
            doc.reset_key  = tostring(os.time()):sub(1,5) -- case III. not found email abd facebook DID NOT sends one.
            --doc.reset_key2 = tostring(152778 + os.time() + os.time()) -- case III. not found email abd facebook DID NOT sends one.
		end

		-- insert data and use doc as JSON response
        local i, err = col:insert({doc}, nil, true)
		ret = doc
		ret.reset_key = nil
		--ret.reset_key2 = nil
		ret.success = true
		end
	end
end

print(cjson.encode(ret))
