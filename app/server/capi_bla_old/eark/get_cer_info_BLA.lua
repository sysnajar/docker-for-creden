#!/usr/bin/env lua
local common = require 'common'
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)


local body = common.init_post {pin = "1234"}
local ctx  = common.mongo_ctx()
local ret  = {success = true, have_cert = false, error_msg = "N/A"}
	
	local myusername = session.data.username or "N/A" 
	
	local col = ctx.col['dopa.esig_user']

	function serial(str)
           local i1 = string.find(str, 'Serial')
           local i2 = string.find(str, 'Signature')
	   local ret = string.sub(str, i1 , i2)
	   ret = string.gsub(ret, "Serial Number:", "")
	   ret = string.sub(ret,1,#ret-2)
	   return ret
	end

	function date_format(ret)

		local yyyy = string.sub(ret, #ret-3 , #ret)
	   local mm = string.sub(ret, 2 , 4) 
	   local dd1, dd2 = string.find(ret , '%d+')
	   local dd = string.sub(ret, dd1, dd2)

	   local m = {Jan = "01", Feb = "02", Mar = "03" , Apr = "04", May ="05" , Jun ="06" , Jul ="07", Aug = "08" , Sep = "09", Oct = "10" , Nov = "11",  Dec = "12"}

	   ret =   dd .. '/' ..m[mm] .. '/' .. yyyy
	   return ret
    end

	function issue_date(str)
           local i1 = string.find(str, 'Not Before')
           local i2 = string.find(str, 'GMT' , i1) 
	   local ret = string.sub(str, i1 , i2)
	   local i3  = string.find(ret, ':' )+1
	   ret = string.sub(ret, i3 , #ret-2)

	  

	   return date_format(ret)
	end

	function expire_date(str)
           local i1 = string.find(str, 'Not After')
           local i2 = string.find(str, 'GMT' , i1) 
	   local ret = string.sub(str, i1 , i2)
	   local i3  = string.find(ret, ':' )+1
	  ret = string.sub(ret, i3, #ret-2)
	   return date_format(ret)
	end


--	local info  = { pub =  rc:get("pub." .. myusername) ,
--	                 tx =  rc:get("txt." .. myusername) }

    --local update_cmd = {["$set"] = {have_cert = true , last_cert_info = info}}
    local u, err = col:find_one({adminEmail = myusername})
    
    if(u.last_cert_info)then
       ret.have_cert = true
	--    public_key=u.last_cert_info.pub 

	   if(myusername == "bla04@creden.co") then

       ret.cert = {serial_number = serial(u.last_cert_info.tx) , issue_date = "20/05/2022" , expire_date="25/06/2522"}
	   else
		ret.cert = {serial_number = serial(u.last_cert_info.tx) , issue_date = issue_date(u.last_cert_info.tx) , expire_date=expire_date(u.last_cert_info.tx)}

	   end
    end
    print(cjson.encode(ret))
