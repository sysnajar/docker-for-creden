#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local ret = {success = false}
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_comment")
    
    if session.data.username then 
        local comment, err_commment = col:find({doc_id = body.doc_id})
        local get_table = {}
        if comment then
            for k,v in comment:pairs() do 
                -- v._id = tostring(v._id)
                local new_data = {}
                for n,u in ipairs(v.user) do 
                    if u.email == session.data.username then
                        u.read = true
                    end
                    table.insert(new_data,u) 
                end

                local update = {["$set"] = {user=new_data}}
                u, err = col:update({_id = v._id}, update)
            end
            ret.success = true
        else
            ret.success = false
            ret.msg = 'not found'
    
        end
    else
        ret = {success = false, error_msg = err, msg = "no session" }
    end

end

print(cjson.encode(ret))