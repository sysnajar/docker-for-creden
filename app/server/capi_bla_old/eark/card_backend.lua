#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local HOST_NAME = os.getenv("HOST_NAME")
local prog2 = exec.new('/tmp/exec.facebox.sock')
prog2.timeout_fatal = false

local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

function trim5(s)
	  return s:match'^%s*(.*%S)' or ''
end

function dob_line(txt)
local dob_line  = nil
local i = 0
local c = {}
for line in string.gmatch(txt, '[^\r\n]+') do
    --print(i,line)
    i = i + 1
    
    if(dob_line == nil and (string.find(line, 'Date')~=nil or string.find(line, 'Birth')~= nil)) then
       dob_line = i
    end

    if(dob_line == nil and string.find(line, 'ศาสนา')~=nil) then
		    dob_line = i -1
    end
    table.insert(c, line)
end


return c, dob_line
end

function nameTh(txt)
  want = false
  full_name = ''
  find_first = true
  for line in string.gmatch(txt, '[^\r\n]+') do 
    --print(line)
    if find_first and ( string.find(line, 'ด.ช.') or string.find(line, 'นาย') or string.find(line, 'น.ส.') or string.find(line, 'นางสาว') or string.find(line, 'นาง')) then
      want = true
      find_first = false
    end
    if want then
      if string.find(line,"[a-z]") then
        want = false
      end
      if want then
        full_name = full_name .. ' '..line
      end
    end
  end
  line = full_name
  words = {}
  for word in string.gmatch(line,"%S+") do table.insert(words, word) end
    size_words = table.getn(words)
    t.name = words[size_words-1] or ''
    if string.find(t.name, 'นาย') then
      t.name = string.gsub(t.name,'นาย','')
    elseif string.find(t.name, 'นางสาว') then
      t.name = string.gsub(t.name,'นางสาว','')
    elseif string.find(t.name, 'น.ส.') then
      t.name = string.gsub(t.name,'น.ส.','')
    elseif string.find(t.name, 'นาง') then
      t.name = string.gsub(t.name,'นาง','')
    end
    t.lastname = words[size_words] or ''   
end

rc:set('tmp_card2', body.img)
--body.id = 'dob'
--base64 to image
--name = "1518685937" -- "1518661406"
--name =  "1518750065"
name = body.img
--rc:set('tmp_card2', body.img)
local path = "/home/creden/work/app/client/face/images/card/"..name..".png";

local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh', HOST_NAME..'/face/images/card/' .. name ..'.png')
--local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh','http://dev.hjkl.ninja:8124/face/images/card/' .. name ..'.png')
if not err then
    ocr = cjson.decode(res.stdout)
    if ocr.responses[1].fullTextAnnotation then

        long_text = ocr.responses[1].fullTextAnnotation.text
        nameTh(long_text);
        long_text = string.gsub(long_text, "%s+", "")
        -- print(long_text)
        a = string.match(long_text, '%d%d%d%d%d%d%d%d%d%d%d%d%d')
    if a == nil then
      t.err = "รูปไม่ถูกต้อง"
      print(cjson.encode(t))
      return 0
    end   
		local line_dob, _i = dob_line(ocr.responses[1].fullTextAnnotation.text)
	    local dob_en = line_dob[_i]
		--print('dob_en', dob_en)
        dob_en = string.sub(dob_en, string.find(dob_en, '%d'), #dob_en) 
		--print('dob_en', dob_en)

		local dd,mm,yy = '','',''
		local cnt = 0
        for ch in  string.gmatch(dob_en,"%S+") do
          cnt = cnt+1
    			if(cnt==1)then dd = ch end
    			if(cnt==2)then mm = ch end
    			if(cnt==3)then yy = ch end
    		end

        yy = tonumber(yy)

		t.dob = {
        day   = ((string.len(dd) == 1) and '0'..dd or dd),
		mon  = (string.gsub(mm,'%.','')) or '',
	    year = (yy) and tostring(yy + 543) or ''
		}	

        t.card_id = a
        t.success = true
  end
else
    print(err)
end

print(cjson.encode(t))
rc:lpush('q_move_card',body.img)
