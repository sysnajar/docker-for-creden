local server = require ("resty.websocket.server")

local clients = {}
local cjson = require 'cjson'
local redis = require 'resty.redis'
local red = redis:new()
red:set_timeout(1000)
red:connect('127.0.0.1', 6379)
local sleep_timeout = 10

--[[ngx.thread.spawn(function() 
  red2 = redis:new()
  red2:set_timeout(1000)
  red2:connect('127.0.0.1', 6379)
  red2:subscribe('jobs')
  ngx.log(ngx.INFO, "subscribed to jobs pubsub")

  while true do
      local res, err = red2:read_reply()
	  if(res)then 
          ngx.log(ngx.INFO, "pubsub msg " .. cjson.encode(res))
		  if(res[2] == 'jobs')then
            ngx.log(ngx.INFO, 'Sending notify *******************************************')  
            clients[res[3 ] ]:send_text('jobs is done'..res[3])
		  end
	  end
  	  ngx.sleep(sleep_timeout)
  end

end)
]]
-- end pubsub

local wb, err = server:new{
  timeout = 5000,
  max_payload_len = 65535
}
if not wb then
  ngx.log(ngx.ERR, "failed to new websocket: ", err)
  return ngx.exit(444)
end


local key = nil
while true do	
  	
  local data, typ, err = wb:recv_frame()

  if(key ~= nil)then
      local val = red:get(key)
      --ngx.log(ngx.INFO, 'loop ' .. tostring(key) .. ' = ' .. tostring(val) )
	  if(type(val)=='string') then 
		  wb:send_text(val)
		  break 
	  end
  end

  if wb.fatal then
	ngx.log(ngx.ERR, "failed to receive frame: ", err)
	return ngx.exit(444)
  end
  if not data then
	local bytes, err = wb:send_ping()
	if not bytes then
	  ngx.log(ngx.ERR, "failed to send ping: ", err)
	  return ngx.exit(444)
	end
  elseif typ == "close" then break
  elseif typ == "ping" then
	local bytes, err = wb:send_pong()
	if not bytes then
	  ngx.log(ngx.ERR, "failed to send pong: ", err)
	  return ngx.exit(444)
	end
  elseif typ == "pong" then
  elseif typ == "text" then
    key = data	
	--clients[key] = wb
	--ngx.log(ngx.INFO, "registered ws client#" .. data)
  end
  ngx.sleep(sleep_timeout)
end

ngx.log(ngx.INFO, 'closing websocket for client' .. tostring(key))
wb:send_close()
