#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local line_esign = require "line_esign_noti"
prog.timeout = 1000 * 60
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body   = {doc_id='1234567'} 
end

conn = mongo() 
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if err then
    ret = {success = false, error_msg = err}
else
    local myusername  = session.data.username
    
    if(not myusername) then
        ret = {success = false, error_msg = 'not user session'}
    end
    
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    doc, err = col:find_one({id=body.doc_id})

    if doc then
        local update_cmd = {["$set"] = {
            screener_approve = true,
        }}
        u, err = col:update({id=body.doc_id}, update_cmd, 0, 0, true)
        if u then
            ret = { success = true, error_msg = 'update success', error_code = "P01"}
         else
            ret = { success = false, error_msg = 'update unsuccess', error_code = "P02"}
        end
    else
        ret = { success = false, error_msg = 'No document', error_code = "P00"}
    end
end

print(cjson.encode(ret))