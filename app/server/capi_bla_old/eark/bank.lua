#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local redis = require 'redis'

local rc = redis.connect('127.0.0.1', 6379)
local prog = exec.new('/tmp/exec.sock')
local body = nil


if(ngx)then  
   ngx.req.read_body()
   print = ngx.say
   --body = ngx.req.get_post_args()
   body = ngx.req.get_body_data()
   body = cjson.decode(body)
else
   --body  = {bank='kbank',username=2,password=3,key=arg[1]}
   body  = {key=arg[1], bank=arg[2], username=arg[3], password=arg[4] }
end

t = {success = false, data = body.bank, key = body.key}

if( not (body.bank=='scb' or body.bank=='kbank')
	or not body.username or not body.password) then	
   t.error_msg = 'invalid parameter'
   t.body = body
else
	local cmd = 'xvfb-run -a node /home/creden/' .. body.bank  .. '.js false 15000'
	local params = body.key .. ' ' ..  body.username .. ' ' .. body.password .. ' &'
	cmd = cmd .. ' ' .. params
	os.execute(cmd)
    -- t.cmd = cmd  -- >>>>> for debug only
    rc:lpush('active_jobs', body.key)
	t.success = true
end

-- phantom & save in redis by key--
--local cmd = 'lua /home/creden/work/app/server/capi/test_delay.lua '
print(cjson.encode(t))
