local upload = require "resty.upload"
local cjson = require "cjson"
local chunk_size = 4096 -- should be set to 4096 or 8192
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

rand  = function(len)
  local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
  local t = {}
  local cnt = 0
            
  local i = 1
  repeat
    local f = assert (io.popen (cmd, 'r'))
    local line = f:read('*all'):match("%d+")
    f:close()
    cnt = cnt + #line
    table.insert(t, line)
  until cnt>len                               
  local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
  return ret
end

local name = rand(20) .. tostring(os.time())

ngx.say(name)
