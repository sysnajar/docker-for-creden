#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {}

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--todo cust_code may not == username in the future
function chk_pwd(usr, pwd)
    local map = {credendemo = 'creden@demo'}
    
    if usr ~= nil and pwd ~= nil and map[usr] == pwd then
        return true
    end
        local chk = nil
        local conn = mongo()
        conn:set_timeout(5000)
        local ok, err = conn:connect('127.0.0.1',27017)
        if not err then
            local db = conn:new_db_handle("dopa")
            local col = db:get_col("company")
            local s = col:find_one({adminEmail = usr, encryptPass=md5.sumhexa(pwd)})   
            if s then chk = true end
        end
        return chk
end

--todo cust_code may not == username in the future
function chk_apiKey(apiKey)
    local chk = nil
    local conn = mongo()
    conn:set_timeout(5000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
         chk = true 
        
        end
    end
    return chk
end

local comp_id = {
  ["0105558080778"]="0105558080778",
  ["0105558183739"]="0105558183739",
  ["0105561087818"]="0105561087818",
  ["0105556195641"] = "0105556195641",
  ["0115558006004"] = "0115558006004",
  ["0105552046187"] = "0105552046187",
  ["0105557093787"] = "0105557093787",
  ["0105561205365"] = "0105561205365",
  ["0105560045011"] = "0105560045011",
  ["0105546114532"] = "0105546114532",
  ["0107535000303"] = "0107535000303",
  ["0205558018435"] = "0205558018435",
  ["0115552007564"] = "0115552007564",
  ["0105558183739"] = "0105558183739",
  ["0105563115215"] = "0105563115215"
}

if(ngx)then
    ngx.save_log({desc = "api_chk_kyc"}) 
     
    ngx.req.read_body()
    print = ngx.say
    body  = ngx.req.get_body_data()
    header = ngx.req.get_headers() or {}
    local chk2 = chk_apiKey(header['apikey'] or 'n/a')
    if(chk2 == nil) then
         ngx.exit(401)

    end

    -- print(body)
    body = cjson.decode(body)
else
  
   body  = {id="0105544009391"} 
end

-- local key_cache = 'api.check.comp.'..tostring(body.id)
-- local get_cache = rc:get(key_cache)
-- if(get_cache)then
--   -- print(get_cache)
--   -- ngx.exit(200)
-- end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
else

  local db = conn:new_db_handle("newdbd")
  local col = db:get_col("company_wh")
  local col_partner = db:get_col("partner")

  if(comp_id[body.id] == nil)then
    ngx.exit(401)
  end
	ngx.log(ngx.NOTICE,body.id)
  local get_detail = col:find_one({_id = comp_id[body.id] or 'n/a'})
  
  local data = {}
  data.JP_TNAME = get_detail.JP_TNAME
  data.STATUS_TNAME = get_detail.STATUS_TNAME
  data.JP_NO = get_detail.JP_NO
  data.partner = {}
  
  local get_partner,err = col_partner:find({JP_NO=data.JP_NO},{OBJ_TNAME=0,JP_TYPE_CODE=0,STATUS_TNAME=0})

  for _,v in get_partner:pairs() do
    v._id = nil
    table.insert(data.partner,v)
  end

  if(#data.partner == 0)then
    data.partner = nil
  end

  ret.success = true 
  ret.data = data

	ngx.log(ngx.NOTICE,cjson.encode(ret))
  -- local set_cache = rc:set(key,cjson.encode(ret))
  -- local set_expire = rc:expire(key,60 * 60 * 24)

end


print(cjson.encode(ret))
   
