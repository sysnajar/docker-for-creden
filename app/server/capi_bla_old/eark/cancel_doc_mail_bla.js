var mailer = require("nodemailer");
var redis = require('redis')
var rc = redis.createClient('6379', '127.0.0.1');
rc.get('is_ldd', function (err, reply) {
  send_mail(reply)
})

function send_mail(is_company) {

  var logo = process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"

  // var style_bg = "background-image: url('" + process.env.HOST_NAME + "/signature/img/bg-01.png');width:100%;"
  // var logo = process.env.HOST_NAME + "/signature/img/Creden_logos_Logo_C.png"
  // var style_logo = "width:300px;height:100;display: block; border: 0px solid #ffffff;"
  // var reply_mail_company = "info@creden.co"

  var email = process.argv[2]
  var name_doc = process.argv[3]
  var id_doc = process.argv[4]
  var owner_doc = process.argv[5]
  var note = process.argv[6]
  var owner_name = process.argv[7]
  var subject = " Canceled: " + name_doc
  var link_forgot = process.env.HOST_NAME + "/bla/#/document?id=" + id_doc
  var old_msg = process.argv[8]

  var secret_level = process.argv[9] || 1
  var secret_text = ""
  if (secret_level == 4) { secret_text = "ความลับที่สุด (Top Secret)" }
  else if (secret_level == 3) { secret_text = "ความลับมาก (Secret)" }
  else if (secret_level == 2) { secret_text = "ความลับ (Confidential)" }

  var html_secret = ``
  if (secret_level > 1) {
    html_secret = `<div style="font-size:10px;">
          <b>Document Classification/ระดับความลับของเอกสาร :</b>
          "`+ secret_text + `"
        </div>
          <br>`
  }



  var html = `<!doctype html>
  <html lang="th-TH" dir="ltr">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
    </head>
    <body style="background: #E0E0E0;padding-bottom: 30px;"><br>
  

      <table border="0" cellspacing="0" cellpadding="0"  align="center"
      style="background: #ffffff;padding: 5px 30px 50px 30px;width:80%;border-radius: 5px;">
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="${logo}" alt="logo website" style="width: 300px;margin-top: 30px;">
            </td>
        </tr>
        <tr>
          <td style="width: 100%;color: #000;padding: 0px 30px 30px 30px;margin-top: 30px;">
            <div style="margin-top:30px;font-size: 16px;">
              <label style="font-weight: bold;">Document ID/รหัสเอกสาร :</label>  ${id_doc}
            </div>
            <div style="margin-top:15px;font-size: 16px;">
              <label style="font-weight: bold;">From/จาก :</label> ${owner_name} ( ${owner_doc} )
            </div>
            <div style="color:#EB5757">
              <b>Cancled/ถูกยกเลิกเพราะ:</b> ${note}
            </div>
            ${html_secret}
            </td>
        </tr>
        <tr>
          <td style="display: flex;justify-content: center;">
              <div style="background: #0058A9;width: 100%;display: block;padding-bottom: 100px;">
                  <div style="text-align: center;margin-top: 60px">
                      <img src="`+ process.env.HOST_NAME + `/bla/images/mail/mail_cancle.png" style="margin-top: 30px;">
                  </div>
                  <div style="text-align: center;font-size: 16px;color: #FFF;margin-top: 15px">
                  ${owner_name} - Canceled File : ${name_doc}.pdf 
                  </div>
                  <div style="text-align: center;font-size: 16px;color: #FFF;margin-top: 15px">
                  ${owner_name} - ได้ทำการยกเลิกเอกสาร : ${name_doc}.pdf 
                  </div>
                  </div>
              </div>
          </td>
        </tr>
        </table>
        </body>
        </html>`


  var smtp = {
    host: 'smtp.bla.co.th', //set to your host name or ip
    // host: 'smtp.mailgun.org', //set to your host name or ip
    port: 25, //25, 465, 587 depend on your 
    auth: {
        user: 'electronic_sign@bangkoklife.com', //user account
       // pass: 'dnkrakkaxnmnakpr'
      //   pass: 'creden2018' //user password
      // user: 'postmaster@mg.creden.co', //user account
      // pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };


  var smtpTransport = mailer.createTransport(smtp);

  var mail = {
    from: 'Bangkok Life - eSinature Cancel', //from email (option)
    to: email, //to email (require)
    cc: '',
    subject: subject, //subject
    html: html//head + body + end
  }


  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}

