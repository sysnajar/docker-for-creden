#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}


function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function count(cur)
	local ret = 0
	for i,v in cur:pairs() do
	    ret = ret + 1
	end
	return ret
end

function match_myorder(signers) 
    local order = 0
    for i, v in ipairs(signers) do
      if v.is_group == true then
        for i2, v2 in ipairs(v.user_group) do
          if v2.email == body.email then
            order = v.order_set
            break
          end
        end
      else
        if v.email == body.email then
          order = v.order_set
          break
        end
      end
    end
    return tostring(order)
end

function sort_signing_order(arr) 
    local tmp = {}
    for i, v in ipairs(arr) do
        local myorder = 0
        if v.use_signing_order == true and v.status == 0 then
            myorder = match_myorder(v.signers)
            if myorder == "" then 
              myorder = "0" 
            end

            if tonumber(v.current_order) >= tonumber(myorder) then
                table.insert(tmp, v)
              end

        else
            table.insert(tmp, v)
        end
    end
    return tmp
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {owner_folder = arg[1]} 
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    inbox, err = col:find({ status =  0, signers={['$elemMatch']={email = body.email, is_viewed = false }} })
    inbox2, err = col:find({ status = 0, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email, is_viewed = false }}}} })

    cancel, err = col:find({ status = 3, signers={['$elemMatch']={email = body.email, is_viewed = false }} })
    cancel2, err = col:find({ status = 3, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email, is_viewed = false }}}} })

    local list_inbox = {}
    local list_inbox2 = {}
    for i, v in inbox:pairs() do 
        v._id = nil
        table.insert(list_inbox, v)
    end

    for i, v in inbox2:pairs() do 
        v._id = nil
        table.insert(list_inbox, v)
    end
    list_inbox2 = sort_signing_order(list_inbox)


    local list_cancel = {}
    for i, v in cancel:pairs() do 
        v._id = nil
        table.insert(list_cancel, v)
        table.insert(list_inbox, v)
    end

    for i, v in cancel:pairs() do 
        v._id = nil
        table.insert(list_cancel, v)
        table.insert(list_inbox, v)
    end

    ret = {success = true, inbox = #list_inbox2, cancel = #list_cancel, error_msg = "get is viewed document", error_code = ""}
    -- ret = {success = true, inbox = list_inbox, cancel = list_cancel, error_msg = "get is viewed document", error_code = ""}
end
print(cjson.encode(ret))
