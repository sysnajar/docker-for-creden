#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
common = require "common"
local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})
function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    check_session()
   
    local myusername  = session.data.username
    
    if myusername then
        db = conn:new_db_handle("dopa")
        col = db:get_col("package_ss")
        p, err = col:find({ company_id = session.data.company_id})

        if err then
            ret = {success = false, error_msg = "find plan unsuccess", error_code = "" }
        else
            local data = {}

            for i, v in p:pairs() do
                v._id = nil
                table.insert(data, v)
            end
            
            

            ret = {success = true, data = data, error_msg = "find plan success", error_code = ""}
        end




    else 
        ret = {success = false, error_msg = "not session", error_code = "" }
    end

end

print(cjson.encode(ret))
