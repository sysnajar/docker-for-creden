#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
prog.timeout_fatal = false

local load_h = prog('lua', '/home/creden/work/app/server/capi/h_name.lua')
local HOST_NAME = load_h.stdout
HOST_NAME = HOST_NAME:gsub("%\n", "")

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
-- rc2 = redis.connect('172.31.7.255', 6379)
rc2 = redis.connect('54.254.182.56', 6379)

rc2:auth('noRedisPass1010')
body = {}

os.execute2 = function(cmd)
  print(cmd)
 os.execute(cmd)
end

function trim5(s)
    return s:match'^%s*(.*%S)' or ''
end
  
function getVal(key)
    local ret = body[key]
    ret = ret and trim5(ret) or nil
    if(ret=='')then ret = nil end
    return ret
end
  
function get_cust_code(usr)
    return usr
end
function chk_DBD()
    -- body
    local tmp = {}
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
       
        -- body.text = 'ภาวุธ พงษ์วิทยภานุ'
         db = conn:new_db_handle("newdbd")
    --    col = db:get_col("company")
        col2 = db:get_col("partner")
        con3 = {PARTNER = {['$regex']= body.fnameTH..' '..body.lnameTH}}

        s3 = col2:find(con3,{JP_TNAME=1,PARTNER=1,JP_NO=1,JP_ENAME=1})
        

        for k, v in s3:pairs() do
            v._id = tostring(v.JP_NO)
           

            table.insert(tmp,v)
        end 
        -- return cjson.encode(tmp)
        return tmp
    end
end
function chk_kyc()
    local result = nil
    local params   = 'national_id=' .. (body.cardNo or '') ..'&first_name=' .. (body.fnameTH or '') .. '&last_name=' .. (body.lnameTH or '') .. '&date_of_birth=' ..  (body.dob or '')  .. '&cvv_code=' ..(body.laserCode or '')   
    --local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')
    local dob = body.dob:sub(7,10)..body.dob:sub(4,5)..body.dob:sub(1,2)
    print(body.laserCode)
    -- if(body.laserCode:find('-','')) then
        -- body.laserCode = body.laserCode
    -- else
        body.laserCode = body.laserCode
    -- end


    print("xxxx"..dob)
    local res, err = prog('sh', '/home/creden/work/app/server/capi/check_cardbylaser_proxy1.sh', body.cardNo, checkstring(body.fnameTH), checkstring(body.lnameTH), dob, body.laserCode)

    if(not err)then
    print('dddddko['..res.stdout..']')    
        local tmp = xml_to_table(res.stdout)
            if(tmp) then
                if tostring(tmp.status) == 'true' then
                    result = tmp.status
                    body.status = 'pass'
                    body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                else
                    body.status = 'fail'
                    body.remarks = tmp.remarks
                end             
            isValidDopa = tmp.status
            end 
    end
   
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
    return result
end
-- function chk_kyc()
--     local result = nil
--     local params   = 'national_id=' .. (body.cardNo or '') ..'&first_name=' .. (body.fnameTH or '') .. '&last_name=' .. (body.lnameTH or '') .. '&date_of_birth=' ..  (body.dob or '')  .. '&cvv_code=' ..(body.laserCode or '')   
--     local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

--     if(not err)then
--         local tmp = cjson.decode(res.stdout)
--             if(tmp) then
--                 if tostring(tmp.status) == 'true' then
--                     result = tmp.status
--                     body.status = 'pass'
--                     body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
--                 else
--                     body.status = 'fail'
--                     body.remarks = tmp.message:gsub('java.lang.RuntimeException: %[4%]','')   
--                 end             
--             isValidDopa = tmp.status
--             end 
--     end
   
--     body.isValidDopa = isValidDopa
--     if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
--     return result
-- end

function xml_to_table(xml)
local status  = false
local remarks = ''

rc:set('last_xml', xml)

if( string.find(xml, "<IsError>false") ) then status = true  end

local i1 =  string.find(xml, "<Desc>") 
local i2 =  string.find(xml, "</Desc>")

if(i1==nil or i2==nil) then
    remarks = 'Dopa Error:' .. tostring(xml)
else
    remarks = string.sub(xml , i1+6 , i2)
end
    


local ret = { status = status, remarks = remarks}
return ret
end
function checkstring(value)
 if(not value) then
      return ""
 else
      value = string.gsub(value," ","%20")
    return value
 end
end
function chk_amlo()
    local isValidAML = false
    local ret_amlo = {}
    local card_no = body.cardNo
    local res, err = prog('sh', '/home/creden/work/app/server/capi/testAMO.sh', card_no,body.fnameTH,body.lnameTH)
    print(res.stdout)
    print(res.stdout)
    if not err then
      tmp = cjson.decode(res.stdout)

      ret_amlo.isValidAML = true
      ret_amlo.amlo_desc = tmp.Desc
      ret_amlo.amlo_code = tmp.Code
      print(cjson.encode(tmp))
    else
    ret_amlo.isValidAML = false 
    ret_amlo.amlo_desc = false
    ret_amlo.amlo_code = false
    end 
    -- body.isValidAML = isValidAML
    return cjson.encode(ret_amlo)
end

function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical)
    if body.vdo and string.match((body.vdo.url or ''), "http") then
        body.isValid = false
        body.isValidLiveness = 'processing'
        body.process_status = 'processing'
    else
        if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidAML) == 'true' then
            body.isValid = true
        else
            body.isValid = false
        end 
        body.process_status = 'finished'
    end
end

function chk_face()
  t = {}
  local faceId1 = ''
  local faceId2 = ''
  if getVal('cardImg1') and getVal('cardImg3') then
    faceId1 = detect_face(body.cardImg1:gsub('.png',''))
    faceId2 = detect_face(body.cardImg3:gsub('.png',''))
    t = verify_face(faceId1, faceId2)
    body.faceConfidence = (t.faceMatchPercent or 0)
    body.isFaceIdentical = (t.isIdentical or false)
  end
end

function detect_face(img)
    print('detect_face')
    print(img)
    local faceId = ''
    local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
    if not err then
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end

function verify_face(faceId1,faceId2)
    t = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
    if not err then
        print('verify_face')
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
    end
    return t
end

function download_vdo()
    local dir = '/home/creden/work/app/client/vdo/ex/'
    local res, err = prog('wget', body.vdo.url, '-O', dir .. body.txid .. '.webm')
end
function dbd_load()
    -- body

end
function chk_vdo()
    
    local vdo_dir = {}
    vdo_dir['https://creden.co'] = 'prod'
    vdo_dir['https://test.hjkl.ninja'] = 'test'
    vdo_dir['https://hjkl.ninja'] = 'dev'

    if body.vdo and body.vdo.url and string.match(body.vdo.url, "http") then
        download_vdo()
        body.vdo.url =   'https://creden.co/vdo/'.. vdo_dir[HOST_NAME] .. '/' ..body.txid..'.webm'
        --TODO1 check audio
        local vdo_req = {vdo=body.vdo, txid=body.txid, compCode=body.compCode, urlPrefix=HOST_NAME}
        -- rc2:publish('api.vdo.req', cjson.encode(vdo_req))
        rc2:rpush('vdo.queue', cjson.encode(vdo_req))
        --TODO2  check audio
        --local audio_cmd = "curl -X POST http://159.65.2.13/capi/submit_vdo2 -d '"..cjson.encode(vdo_req).."' &"
        local audio_cmd = "lua /home/creden/work/app/server/capi/submit_vdo.lua '"..cjson.encode(vdo_req).."' &"
        os.execute(audio_cmd)
    end
end

function downloadImgs()
    redis = require 'redis'
    rc = redis.connect('127.0.0.1', 6379)
    local cnt = tostring(rc:incr('kyc.counter'))
    local dir = '/home/creden/work/app/client/face/images/card/'
    for i, k in ipairs({'cardImg1', 'cardImg2' , 'cardImg3'}) do
        local v = getVal(k)
        if(v)then
            local filename = cnt..k 
            chk = string.sub(v, 1, 4)
            if string.match(chk, "http") then
                local cmd = 'wget '.. v ..' -O ' .. dir .. filename .. '.png'
                os.execute(cmd)
            else
            local tmp = 'tmp.kyc.img' .. filename   
            rc:set(tmp , v)
            local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
            os.execute(cmd)
            rc:del(tmp) 
        end     
  
        body[k] = filename .. '.png'
        rc:lpush('q_move_card',filename)
      end
    end
    body.cardImg = body.cardImg1
end


    while true do
        os.execute("sleep 3")
        len = rc:llen('api.kyc.active')
        for i=1, len do
            local data = rc:rpoplpush('api.kyc.active', 'api.kyc.complete')
            if data then
                body = cjson.decode(data)
                conn = mongo()
                conn:set_timeout(1000)
                ok, err = conn:connect('127.0.0.1',27017)
                if err then
                    ret = {success = false, error_msg = err}
                else
                    db = conn:new_db_handle("dopa")
                    col = db:get_col("ekyc")
                    body.channel = 'api'
                    body.createDate = os.date("%x")
                    body.createTime = os.date("%X")
                    body.createdDtm = bson.get_utc_date(os.time() * 1000)
                    body.compCode = get_cust_code(body.username)
                    -- if chk_kyc() then
                        chk_kyc()
                        downloadImgs()
                        chk_face()
                       local chk_amlo = chk_amlo()
                       chk_amlo = cjson.decode(chk_amlo)
                       body.amlo_code = chk_amlo.amlo_code
                       body.amlo_desc = chk_amlo.amlo_desc
                       body.isValidAML = chk_amlo.isValidAML
                       print('chk_amlo==='..cjson.encode(chk_amlo))
                       local chk_DBD_01 = chk_DBD()
                       -- chk_DBD_01 = cjson.decode(chk_DBD_01)
                       body.dbd_results = chk_DBD_01

                       
                        chk_vdo()
                    -- end
                    chk_is_valid(body.isValidDopa, body.isValidAML, body.isFaceIdentical)

                    -- Convert Photo to base64  if Orisma
                    if(body.username == "orisma@creden.co") then  
                        
                        print("Convert base64 cardImg1")
                        local img_dir = "/home/creden/work/app/client/face/images/card/"
                        local vdo_dir = "/home/creden/work/app/client/vdo/ex/"

                        os.execute2("sh /home/creden/work/app/server/capi/save_b64.sh " .. body.txid .. " " .. img_dir .. body.cardImg1 .. " b64.img1.".. body.txid .." &")
                        os.execute2("sh /home/creden/work/app/server/capi/save_b64.sh " .. body.txid .. " " .. img_dir .. body.cardImg2 .. " b64.img2.".. body.txid .." &")
                        os.execute2("sh /home/creden/work/app/server/capi/save_b64.sh " .. body.txid .. " " .. img_dir .. body.cardImg3 .. " b64.img3.".. body.txid .." &")
                        os.execute2("sh /home/creden/work/app/server/capi/save_b64.sh " .. body.txid .. " " .. vdo_dir .. body.txid .. ".webm" .. " b64.vdo.".. body.txid .." &")
                                        
                       
                    end  
                    -- end Orisma

                    body.username = nil
                    body.password = nil
                    selector = {txid=body.txid, compCode = body.username}
                    update = {["$set"] = {isValid = body.isValid,amlo_code=body.amlo_code,amlo_desc=body.amlo_desc,isValidAML=body.isValidAML,dbd_results=body.dbd_results, isValidDopa = body.isValidDopa, isValidAML = body.isValidAML, isFaceIdentical = body.isFaceIdentical, faceConfidence = body.faceConfidence, remarks = body.remarks, process_status = body.process_status}}
                    n, err = col:update(selector, update, 0, 0, true)
                    if not n then
                        ret = {success = false, error_msg = err}
                    else
                        ret = {success = true, error_msg = err, txid = body.txid , usr = body.compCode}
                        print(cjson.encode(ret))
                    end
                end
            end
        end
        print('no queue')
    end
