#!/usr/bin/env lua
redis = require 'redis'
cjson = require 'cjson'

local t    = {pages={}}
local page = nil -- page object
local text = nil -- set this to {} if we want to keep original pdf_dump in redis

for line in io.stdin:lines() do



  if(line:find('NumberOfPages:')==1) then t.NumberOfPages = line:match('%d+') end
  

  if(line:find('PageMediaNumber:')==1) then
	  p = {page = tonumber(line:match('%d+'))}
	  table.insert(t.pages, p) 
  end

  if(line:find('PageMediaDimensions:')==1) then
	  for num in line:gmatch('%d+%.*%d*') do
          if(not p.width) then 
			  p.width = tonumber(num) 
		  else
             if(not p.height)then p.height = tonumber(num) end
		  end
	  end
  end

   if(text)then table.insert(text, line) end --store original pdf_dump if needed
end

if(text)then t.original_text = table.concat(text, '\n') end

print(cjson.encode(t))
