#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

local body = nil

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function gen_num( )
    local server_id = '000'
    local inc = tostring(rc:incr('genref_eks'))
    while(#inc<7) do
         inc = '0' .. inc
    end
 
    return server_id .. inc
 end

function get_package(num) 
	local package = "N/A"
	if num == "1" then
		package = "basic"
	elseif num == "2" then
		package = "advanced"
	elseif num == "3" then
		package = "premium"
	end	
	return package
end

function get_user_package(num) 
	local package = 5
	if num == "1" then
		package = 5
	elseif num == "2" then
		package = 10
	elseif num == "3" then
		package = 20
	end	
	return package
end

 function insert_package(com)
	local name_package = get_package(body.package)
	if name_package == "N/A" then
		print(cjson.encode({success = false, error_msg = "no package", error_code = ""}))
		ngx.exit(ngx.HTTP_OK)
	end

	local current_date = bson.get_utc_date(os.time() * 1000)
	local exp_date = bson.get_utc_date((os.time()*1000) + (1000*60*60*24*body.long_term)) 

	local data_package = {
		user_ekyc = 0,
		plan = "CREDIT",
		discount = 0,
		email = com.create_by,
		total = "0000",
		package = get_package(body.package),
		ekyc = {},
		promo_code_use = false,
		status = true,
		ref_id = gen_num(),
		promo_code = "",
		user_add_on = 0,
		active_package = true,
		advance_func = false,
		storage = {},
		type_of_business = "",
		form = {},
		company_id = tostring(com._id),
		date_buy = current_date,
		price_package = "0000",
		company_name = "",
		lastname = "",
		promo_code_value = "",
		firstname = "",
		need_invoice = false,
		users = get_user_package(body.package),
		active_expire_date = exp_date,
		active_start_date = current_date,
		date_pay = current_date,
		-- end_active_package = ISODate("2021-04-07T11:54:55Z")
	}

	i, err = col_pack:insert({data_package})
	if err then 
		ret = { success = false, error_msg = "Add package not found", error_code = "" }
    else
		ret = { success = true, error_msg = "Add package success", error_code = "" }
    end
	print(cjson.encode(ret))
 end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
	body = ngx.req.get_post_args()
else
    body = { password = arg[1], email = arg[2], package = arg[3], long_term = arg[4] }
end

if body.password ~= "135790" then
	print(cjson.encode({success = false, error_msg = "password incorrect", error_code = ""}))
	ngx.exit(ngx.HTTP_OK)
end

local conn = mongo()
conn:set_timeout(5000)
local ok, err = conn:connect('127.0.0.1',27017)

if not ok then
	ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
	
    local db = conn:new_db_handle("dopa")
	col_pack = db:get_col("package_ss")
	col_com = db:get_col("user_company")

	local c, err = col_com:find_one({ create_by = body.email })
	if not c then
		ret = { success = false, error_msg = "not company", error_code = "" }
	else
		insert_package(c)
	end


end
