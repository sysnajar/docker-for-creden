#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1')
fun = require 'fun'


local Save = false
local Tmp = {}

local total_scores  = {DEMO = 600, SSO=300, FIN = 600, CRIM = 100, SOC = 100, BUS = 200, INT = 900}
local all_keys = fun.foldl(function(acc, x) table.insert(acc, x) return acc end, {}, total_scores)

local fn = {CRIM = require 'COMP_CRIM',
INT = require 'COMP_INT'
}

function set_score(k, v, id)
local key = id .. ':SCORE.' .. k 

if(Save)then
   rc:set(key, v)
else
   Tmp[key] =  v
end

--print('saved', key, v)

end

function get_score(k, id)
local key = id .. ':SCORE.' .. k 
local ret = nil

if(Save) then
   ret = tonumber(rc:get(key) or 0)
else
   ret = Tmp[key] or 0
end

return ret
end

function compute(id, keys)
keys = keys or {}
print('\ncomputing score', id)

-- 1. compute chosen scores 
for _, v in ipairs(keys) do  	
   local score = fn[v](id)
   local old = get_score(v, id)
   print(v , old  , '>>>' , score )   
   set_score(v, score, id)
end


print('')   

-- 2. combined all scores
local total = 0
for _, v in ipairs(all_keys) do
	local score = get_score(v, id)	
    total = total + score

    print(v , '=' , score )   
end

print('total', total)

end


--------------
compute('sysnajar@gmail.com', {'CRIM', 'INT'})
