#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {date='12/02/2021',mode="walkin"}
end
     data = { morning = {}, afternoon = {} }
     data.morning = {
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "09:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "09:30"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "10:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "10:30"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "11:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "11:30"
            }
    
        }
    }

     data.afternoon = {
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "13:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "13:30"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "14:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "14:30"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "15:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "15:30"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "16:00"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "16:30"
            }
    
        },
        {   
            date = body.date,
            slot = {
                available_min = 0,
                available_max = 10,
                time = "17:00"
            }
    
        }
    }

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("slot_certificates")
    find_slot = col:find_one({date_slot = body.date,mode=body.mode})
    if find_slot then
        find_slot._id = tostring(find_slot._id)
        ret = { success = true, data = find_slot , found = true }
    else
        local new_slot= {date_slot = body.date, morning = data.morning ,afternoon = data.afternoon,mode=body.mode}
        u,err = col:insert({new_slot})

        if u then 
            ret = { success = true, data = new_slot , found = false}
        else
            ret = { error_msg = "Add slot not found" }
        end
    
    end
    -- local new_slot= {date_slot = body.date, morning = data.morning ,afternoon = data.afternoon}
    -- u,err = col:insert({new_slot})

    -- if u then 
    --     ret = { error_msg = "Add slot success" }
    -- else
    --     ret = { error_msg = "Add slot not found" }
    -- end
    -- ret = { success = true, data = data }

end

print(cjson.encode(ret))
