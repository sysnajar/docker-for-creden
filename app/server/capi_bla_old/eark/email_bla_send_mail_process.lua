#!/usr/bin/env lua
local cjson = require 'cjson'
local body = nil
local mongo = require "resty-mongol"
local redis = require 'redis'
local ret = {succes = true}
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {id = arg[1] , to = arg[2],type = arg[3],error_code = arg[4] } 
end
if body.type == "process" then
    cmd = 'node /home/creden/work/app/server/capi/ndid_ver_process.js "'..body.id..'" "'..body.to..'"'
elseif body.type == "success" then
    cmd = 'node /home/creden/work/app/server/capi/ndid_ver_succes.js "'..body.id..'" "'..body.to..'"'
elseif body.type == "reject" then
    cmd = 'node /home/creden/work/app/server/capi/ndid_ver_reject.js "'..body.id..'" "'..body.to..'" "'..body.error_code..'" '
end
os.execute(cmd)

print(cjson.endcode(ret))