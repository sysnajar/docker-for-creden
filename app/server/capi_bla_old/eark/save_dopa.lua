#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {fnameTH='', 
            lnameTH='',
            fnameEN='', 
            lnameEN='', 
            cardNo='', 
            laserCode='', 
            dob='03/24/18', 
            createDate='03/24/18',
            cardImg='1100701382071.png',
            status='wait',
            compCode='rads'} 
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")
        doc = {fnameTH=body.fnameTH, 
               lnameTH=body.lnameTH,
               fnameEN=body.fnameEN, 
               lnameEN=body.lnameEN, 
               cardNo = body.cardNo, 
               laserCode=body.laserCode, 
               dob=body.dob, 
               createDate=body.createDate,
               cardImg=body.cardImg,
               status=body.status,
               compCode=body.compCode}

        i, err = col:insert({doc}, nil, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            ret = {success = true, error_msg = err}
        end
    end

print(cjson.encode(ret))


