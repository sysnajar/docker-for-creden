#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {compCode = 'rads'} 
end

--skip counting
if(false)then
   print(cjson.encode({success = false, error_msg = "N/A"}))
   ngx.exit(ngx.HTTP_OK)
end
--skip counting

if(body.compCode == 'all') then
  body.compCode = nil
end

function compare(a, b) 
    c = string.gsub(a.date, "%/", "")
    d = string.gsub(b.date, "%/", "")
    return tonumber(c) > tonumber(d)
end

local tx_mon_fn = function(txs)
    local mon = {}
	local sort = {}
    local data = {}
    for _j, t in ipairs(txs) do
            local date = t.createDate or 'eark'
            mon[date] = mon[date] or {date = date, total_txs = 0, price = 0}
            mon[date].total_txs = mon[date].total_txs + 1
    end
   
    for k, v in pairs(mon) do
    	table.insert(sort, v)
    end
    table.sort(sort, compare)
    
	local count = 0
	for k, v in pairs(sort) do
		count = count + 1
		if count <= 7 then table.insert(data, v) end
	end

	return data
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
    s = col:find({compCode=body.compCode},{cardImg1=0,cardImg2=0,cardImg3=0})
    data = {}
    for k, v in s:pairs() do
	    -- ngx.log(ngx.INFO, 'IDx >>> ' .. tostring(v._id))
        v._id = tostring(v._id)
        table.insert(data, k, v)
    end

    res = {success = true, error_msg = "complete", data = tx_mon_fn(data)}
end
print(cjson.encode(res))
