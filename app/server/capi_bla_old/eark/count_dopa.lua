#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
--local mongo = require 'mongo'
--local client = mongo.Client 'mongodb://127.0.0.1'
--local col = client:getCollection('dopa', 'ekyc')

if(ngx)then  
    print = ngx.say
	 body = ngx.req.get_uri_args()
else
     body  = {compCode = 'all'} 
end

conn = mongo()
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    if(body.compCode == 'all')then body.compCode =  nil end  
	local month = body.month or '04'

    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
  	local mdy = {createDate={['$regex']=month.."/\\d\\d/19"}}
 	t  = col:find(mdy)
 	count=0
    
	
	list = {}
	for i=1,31 do
		local key = tostring(i)
		local val = {total = 0, wait = 0, pass = 0, fail = 0}
		list[key] = val
	end

 	for i,v in t:pairs() do
		local date  = tonumber(v.createDate:sub(4,5))
	    if(date)then
		   local key = tostring(date)
           local val = list[key]

		   val.wait  = val.wait + ((v.status=='wait') and 1 or 0)	
		   val.pass  = val.pass + ((v.status=='pass') and 1 or 0)	
		   val.fail  = val.fail + ((v.status=='fail') and 1 or 0)	
		   val.total = val.total+1 
		end

 	end --for

end


ret = {success = true, error_msg = "complete", data = list }
print(cjson.encode(ret))
