#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {} 
end



-- sign fn
function getX(x, width)
    local ret = 394.8
    local xp = (x*100)/width
    
    local x2= (600*xp)/100

	-- caribrate x2
	--x2 = x2 - (340/10)
	x2 = x2 - 20

    return x2
    end
    
    function getY(y, height)
    local ret = 174 
    y = height - y
    local yp = (y*100)/height
    y2 = (800*yp)/100
	
	-- caribrate x2
	--y2 = y2 - (120/5) : AKA 24
	y2 = y2 + 10 --(-45)
    return y2
    end
    
    
    function sign(s, f)
    local file2 = string.gsub(s.pdfFile , '%.pdf', '_stamped.pdf')
    local sig  = '/home/creden/work/app/client/edoc/app-assets/images/signature/'..f.email..'_'..f.type..'.png' 
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. s.pdfFile
    local pdfFile2  = dir .. file2
    local page = tostring(f.page+1)
    
    local x2 = tostring(getX(f.x,s.width))
    local y2 = tostring(getY(f.y,s.height))
    
    local cmd  = 'java -jar /home/creden/scripts/pdfstamp.jar -i '.. sig.. ' -l '.. x2 ..','.. y2 ..' -d 200 -o '.. dir ..' -p '.. page .. ' ' .. pdfFile
    
    local cmd2 = 'cp ' ..pdfFile2 .. ' '.. pdfFile
    ngx.log(ngx.NOTICE,'--------------------------'..cmd)
    ngx.log(ngx.NOTICE,'--------------------------'..cmd2)
	os.execute('cp ' .. pdfFile ..' /home/creden/tmp.pdf')
    os.execute(cmd)
    os.execute(cmd2)
    rc:set('last_sign', cmd)
    end
    -- end sign fn

            --update mongo
            --data = '{"pdfFile":"1525826939.pdf","fields":[{"time":0,"x":329,"id":"1525827051045","y":553,"page":0,"status":1,"email":"a"},{"time":0,"x":322,"id":"1525827059551","y":405,"page":1,"status":1,"email":"a"},{"time":0,"x":331,"id":"1525827061957","y":461,"page":1,"status":1,"email":"a"},{"time":0,"x":333,"id":"1525827065006","y":514,"page":1,"status":1,"email":"s"}],"signers":[{"email":"a"},{"email":"a"},{"email":"s"}],"id":"1525827067223","status":0,"imageFiles":[{"fileName":"pdf1525826939-0.png"},{"fileName":"pdf1525826939-1.png"},{"fileName":"pdf1525826939-2.png"}]}'
            --body = cjson.decode(data)
            conn = mongo()
            conn:set_timeout(1000)
            ok, err = conn:connect('127.0.0.1',27017)
            if err then
                ret = {success = false, error_msg = err}
            else
                db = conn:new_db_handle("edoc")
                col = db:get_col("edoc_list")
                s, err = col:find_one({id=body.id})
                s._id = nil
                for i,v in ipairs(s.fields) do
                    ngx.log(ngx.NOTICE,i , ') ' , v.id)
                    for i2,v2 in ipairs(body.fields) do
                        ngx.log(ngx.NOTICE,'    ' , i2 , ') ' , v2.id..' VS '..v.id)
                        if (v.id == v2.id) then 
							v.status = 1 v.time = os.date("%x %X")
							v.type = v2.type 
							--v.sign_now = true
                            ngx.log(ngx.NOTICE,' set signing type for ' .. v.id .. ' >> ' , v.type)
						end
                    end
                end

                count_sign = 0
                for i3,v3 in ipairs(s.fields) do
                    if v3.status == 1 then count_sign = count_sign + 1 end
                end

                if table.getn(s.fields) == count_sign then 
                    s.status = 1 

                    for i,v in ipairs(s.fields) do	
						   ngx.log(ngx.NOTICE,' start signing for ' .. v.id .. ' ' , v.type)
                           sign(s, v) 
                    end
                    --cmd = 'cpdf -add-text "APPORVE BY CREDEN" -topleft 30 -font-size 20 multipage.pdf -o out.pdf'
                    local res0, err0 = prog('cpdf', '-add-text', "DOCUMENT ID: "..s.id, '-topleft', 30, '-font-size', 15, '/home/creden/work/app/client/face/images/card/'..s.pdfFile , '-o', '/home/creden/work/app/client/face/images/card/'..s.id..'-stamp.pdf')
                    local res, err = prog('lua', '/home/creden/work/app/server/capi/tc_edoc.lua', cjson.encode(s))
                end

				--ETDA Timestamp
				local ts_cmd = 'sh /home/creden/scripts/ts.sh ' .. s.id .. '-stamp.pdf'
				os.execute(ts_cmd)
				--'_timestamped'
				--END ETDA Timestamp


                update = {["$set"] = {fields=s.fields, status=s.status}}
    
                u, err = col:update({id=body.id}, update, 0, 0, true)
                
                if err then
                    ret = {success = false, error_msg = err}
                else
                    ret = {success = true, error_msg = err}
                end
            end

print(cjson.encode(ret))


