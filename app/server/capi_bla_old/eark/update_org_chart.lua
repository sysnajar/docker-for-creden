local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_uri_args() 
else
   body  = ''--{token = args[1],otp = args[2]} 
end


function rand(len)
local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
local t = {}
local cnt = 0
 
 local i = 1
 repeat
     local f = assert (io.popen (cmd, 'r'))
	 local line = f:read('*all'):match("%d+")
	 f:close()
	 cnt = cnt + #line
	 table.insert(t, line)
 until cnt>len

		
 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
 return ret
end


--u = esig_user
--p = new position
function add_org_node(company_id, u , new_pos)	
	local new_node = {
	  id  = company_id .. rand(6),
	  title_id  = new_pos.position_id,
	  title     = new_pos.position_name,
	  person_id = tostring(u._id),
	  name = u.adminFisrtname,
	  img = "xxx",
	  email = u.adminEmail
	 }

	local key = "orgchart." ..company_id
	local json = rc:get(key)
	local ret = nil

	if(json)then
	  ret = cjson.decode(json)
	else
	  ret = {is_empty = false, company_id = company_id, nodes = {}}
	end

	table.insert(ret.nodes, new_node)
	ret.is_empty = false
	ret.success  = true
	ngx.log(ngx.NOTICE, "update org_chart ... " .. cjson.encode(ret))
	rc:set(key, cjson.encode(ret))
end
