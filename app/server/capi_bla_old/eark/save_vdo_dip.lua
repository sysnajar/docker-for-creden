#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local mongo = require "resty-mongol"
local body = nil
local HOST_NAME = os.getenv('HOST_NAME')
prog.timeout_fatal = false
rc2 = redis.connect('172.31.7.255', 6379)
rc2:auth('noRedisPass1010')

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()

    ngx.log(ngx.NOTICE,body)
	
    body = cjson.decode(body)
    ngx.log(ngx.NOTICE,'txid:' .. (body.txid) .. ',usr:' .. tostring(body.usr))

else
 	body = {}
end

local ret = {success = false,  confidence = 0, error_msg = ""}

--save vdo
if(body.vdo_data)then
   rc:set(body.usr..'_'..body.txid,body.vdo_data)
end

ngx.log(ngx.NOTICE, 'CMD = sh /home/creden/work/app/server/capi/convert_vdo.sh '..body.usr..'_'..body.txid..' '..body.txid)
os.execute('sh /home/creden/work/app/server/capi/convert_vdo.sh '..body.usr..'_'..body.txid..' '..body.txid)

local need_rotate =  true
if body.is_pc and tostring(body.is_pc)=='true' then need_rotate = false end


if need_rotate then
	local res, ex_err = prog('ffprobe', '-v' ,'error', '-select_streams', 'v:0', '-show_entries', 'stream=width,height', '-of',
	'csv=s=x:p=0', '/home/creden/work/app/client/vdo/ex/' .. body.txid ..'.webm')

	if(res.stdout)then
	   ngx.log(ngx.NOTICE,"ROTATE vdo dimension W,H = " .. res.stdout)
	  local w =  res.stdout:match("%d+") 
	  local h =  res.stdout:match("x%d+")
	  w = tonumber(w)
	  h = tonumber(h:sub(2))
	  if(w>h)then      
		   ngx.log(ngx.NOTICE,"ROTATE VDO")
		   local res2, ex_err2 = prog('sh', '/home/creden/work/app/server/capi/rotate_vdo.sh', body.txid)
		   if(ex_err2)then
			  ngx.log(ngx.NOTICE,"ERROR ROTATE = " .. ex_err2)
		   else	  
			  ngx.log(ngx.NOTICE,"ROTATE STDOUT = " .. res2.stdout)
			end  
		   
	  else
		   ngx.log(ngx.NOTICE,"DONT ROTATE VDO")
	  end	   
	end
else
    ngx.log(ngx.NOTICE,"is_pc is true , skip vdo rotation")	
end




body.vdo = {}
body.vdo.missions = {}
body.vdo.url = HOST_NAME..'/vdo/ex/'..body.txid..'.webm'

function get_time(t)
    local time_str = ''
    if (tonumber(t) or 0) < 10 then
        time_str = '00:0'..t
    else
        time_str = '00:'..t
    end
    return time_str
end

--make json vdo
for k, v in ipairs(body.arr_missions) do
    local mission = {}
    mission.no = k
    mission.word = v.show
    mission.startTime = get_time(body.arr_times[k])
    mission.endTime = get_time((tonumber(body.arr_times[k]) or 0) + 3)
    if v.type == 'face' then mission.type = 'blink' else mission.type = 'voice' end
    table.insert(body.vdo.missions, mission)
end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    ok,  err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end

if body.vdo.url and string.match(body.vdo.url, "http") then

    local vdo_dir = {}
    vdo_dir['https://creden.co'] = 'prod'
    vdo_dir['https://test.hjkl.ninja'] = 'test'
    vdo_dir['https://hjkl.ninja'] = 'dev'

    body.vdo.url =   'https://creden.co/vdo/'.. vdo_dir[HOST_NAME] .. '/' ..body.txid..'.webm'
    
    --TODO1 check video
    local vdo_req = {vdo=body.vdo, txid=body.txid, compCode=body.usr, urlPrefix=HOST_NAME}

    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("ekyc")
        local found_kyc = col:find_one({txid = body.txid, compCode = body.usr}) 
        
        if(found_kyc)then
            ngx.log(ngx.NOTICE, 'Case A found_kyc >>>>  added to vdo.queue')
            rc2:rpush('vdo.queue', cjson.encode(vdo_req))
        else
            local key = 'vdo.queue.' .. body.txid
            ngx.log(ngx.NOTICE, 'Case B save vdo_req for later use for key >>>>>  [' .. key ..']')

            rc2:set(key,cjson.encode(vdo_req))
            rc2:expire(key,60*30) --30 minutes

			if(body.ghb_id and body.ghb_id~='')then
				local link_key = 'ghb_id' .. body.ghb_id
                rc2:set(link_key, body.txid)
                rc2:expire(link_key,60*30) --30 minutes
                ngx.log(ngx.NOTICE, 'Found ghb_id  >>>>>  [ghb_id' .. body.ghb_id ..'] linking done')
                ngx.log(ngx.NOTICE, link_key .. ' < LINK > ' .. body.txid)

				--do face verification here		
			    
                ngx.log(ngx.NOTICE, body.img1 ..',' .. body.txid)
				os.execute('sleep 1')
				local res, err = prog('lua','/home/creden/work/app/server/capi/face_match_ghb.lua', body.img1, body.txid, body.ghb_id, body.usr )
                                 -- {idcard_file = arg[1], old_txid = arg[2], txid = arg[3], usr = arg[4]}
				if not err then
				local vdo_facematch_result = res.stdout
                ngx.log(ngx.NOTICE, vdo_facematch_result)
				
				if(vdo_facematch_result)then
				   local x = cjson.decode(vdo_facematch_result)
				   ret.confidence = x.confidence
                   ret.selfie_file = body.img1 -- "68494043874499116042"
                   ret.faceid_file = x.enrolled_file --"selfie_Glosec1562575998588" --todo1 sends selfie photo

				   local boo = ret.confidence >= 0.5
                   local page3 = {
					               face={isIdentical = boo , confidence  = x.confidence }
			                     } 
				   --todo2 remove fkey usage				 
				   local fkey = 'face.ghb'..body.ghb_id				 
				   rc:set( fkey  , cjson.encode(page3) )
                   ngx.log(ngx.NOTICE, 'set fkey ' .. fkey)

				  --run python liveness test if isFaceIdentical 
				  if(boo) then
					  local py_cmd = '/home/creden/work/app/server/capi/liveness_tensorflow.lua ' .. '/home/creden/work/app/client/vdo/ex/' .. body.txid .. '.webm ghb' .. body.ghb_id ..''

                      ngx.log(ngx.NOTICE, 'run tenserflow cmd: (saved to '.. body.ghb_id  ..') ' .. py_cmd)
                      ngx.log(ngx.NOTICE, '*****************************************************************************')

					  local exit_code = os.execute(py_cmd)
					  local key = 'ghb' .. body.ghb_id .. '.tensorflow.result'
                      ngx.log(ngx.NOTICE, 'tensorflow key = '..key)
					  local tensorflow_json = rc:get(key)
                      ngx.log(ngx.NOTICE, 'tensorflow_json = '..tostring(tensorflow_json))

					  if(exit_code == 0)then
						  local tensorflow_res = cjson.decode(tensorflow_json)
						  ret.is_fake = not tensorflow_res.is_fake
						  ret.tensorflow_confidence = tensorflow_res.confidence
					  else
						  ret.is_fake = false
						  ret.tensorflow_confidence = 1
						  ret.tensor_error = true
					  end


					  local key2 = 'ghb' .. body.ghb_id .. '.latlon'
					  rc:set(key2, cjson.encode({lat=body.lat , lon=body.lon}))

				  end
					

				end --if vdo_facematch_result

				end
				--


		    end

        end
    end

    --TODO2  check audio
    local audio_cmd = "lua /home/creden/work/app/server/capi/submit_vdo.lua '"..cjson.encode(vdo_req).."' &"
    --os.execute(audio_cmd)
    ret.success = true
    ret.data = body.vdo
end

print(cjson.encode(ret))
