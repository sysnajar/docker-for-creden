#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local HOST_NAME = os.getenv("HOST_NAME")
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

-- print(HOST_NAME)
local t = {success = false, face = nil, faceId = nil, face_count = 0}

local face, err = prog('node', '/home/creden/work/app/server/capi/test_curl.js', body.img)

if face.stdout then
    t.face = cjson.decode(face.stdout)
	if(t.face and t.face.error == nil and t.face[1]) then
		t.faceId = t.face[1].faceId
		t.face_count = #t.face
		t.success = true
	end
end
print(cjson.encode(t))
rc:lpush('q_move_card',body.img)
