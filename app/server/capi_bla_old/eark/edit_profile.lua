#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false
local HOST_NAME = os.getenv("HOST_NAME")

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {adminUsername = arg[1]} 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end


        --update mongo
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if err then
            ret = {success = false, error_msg = err}
        else
            db = conn:new_db_handle("dopa")
            col = db:get_col("company")

            --get existing record
            old_rec = col:find_one({adminEmail=body.adminEmail}, {comStatus = 1})


            body.activeStatus = 4
            body._id = nil    
            u, err = col:update({adminEmail=body.adminEmail}, {["$set"] = body}, 0, 0, true)
            
            if err then
                ret = {success = false, error_msg = err}
            else
                ret = {success = true, error_msg = err, data = body}
                --send email if needed
				if(old_rec and old_rec.comStatus == 'unApprove') then
                    local msg = {
                     key  = 'email' .. tostring(ngx.now()), to   = 'info@creden.co', subject = '[CREDEN] Please activate my account', 
                     html = '<a href="'..HOST_NAME..'/ekyc/admin/com_detail.html?comp_id='.. tostring(old_rec._id) ..'">Please activate my account</a><br/>email = ' .. body.adminEmail .. "<br/>company name = " .. body.comNameTH:sub(1,20)
                     }

                     rc:set(msg.key, cjson.encode(msg))
                     cmd = 'node /home/creden/work/app/server/capi/send_mail.js ' .. msg.key
                     os.execute(cmd)
                end
				--end send email
            end

        end
ret = {success = true, error_msg = err, data = body, old_comStatus = tostring(old_rec)}

print(cjson.encode(ret))
