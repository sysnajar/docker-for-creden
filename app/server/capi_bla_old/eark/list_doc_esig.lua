#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],status = arg[2]}
end
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	-- data={}
	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list")
  query = {}
  

	--prevent email param
	if(not session.data.username ) then
		print(cjson.encode({success = false, error_msg = "session expired"}))
		ngx.exit(ngx.HTTP_OK)
	else
		if(body.email~=session.data.username) then
			ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
			body.email = session.data.username
		end
	end	
	--prevent email param

  if body.status==3 then
    query = {signers={['$elemMatch']={email=body.email}}}
  else
    query = {signers={['$elemMatch']={email=body.email}},status=body.status}
  end


  if body.originalFile then
    query = {originalFile = {['$regex'] = body.originalFile},signers={['$elemMatch']={email=body.email}}}
    --query = {originalFile = {['$regex'] = body.originalFile}}
    --query = {signers={['$elemMatch']={email=body.email}}}

  end
	s = col:find(query)
	tmp = {}
	for k, v in s:pairs() do
       v._id = tostring(v._id)
       if v.createdDtm then
       		v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000)
	   end
       table.insert(tmp,v)
    end
    --[[
    data = {}
    size = table.getn(tmp)
    for i,v in ipairs(tmp) do
    	if i > size -5 then
    		v.time = os.date('%d-%m-%Y %H:%M:%S', v.createdDtm)
	    	table.insert(data,v)
	    end
    end
    ]]--
    ret = {success = true, error_msg = "complete", data = tmp}
    print(cjson.encode(ret))
end
