#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
common = require "common"

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
  
   body  = {adminEmail = arg[1],subject = arg[2]} 
end

conn = mongo()
conn:set_timeout(10000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    -- oja = {subject = {['$regex']=body.subject}}
    -- s = col:find({owner=body.adminEmail},{oja})    
      -- query = {['$or']={{owner='p'},{subject ={['$regex']='p'}}}}
      -- query = {['$or']={owner={['$regex']='p'}}}
    -- query = {['$or']={{subject = "s"}, {owner = "c"}}}

    function chk_deleted_ids(ids,doc)
        local flat = true

        for i,r in ipairs(ids) do 
             if r == doc.id then
                 flat = false
                 break
             end
        end

        return flat
     end

    local deleted_docs = db:get_col('deleted_doc'):find({email = body.email})
    local deleted_ids = {0}
    for k, v in deleted_docs:pairs() do
        if (tostring(v.id) ~= "nil") then
            table.insert(deleted_ids, tostring(v.id))
        end
    end
    local arr = {}
    local map = {}
    if(#deleted_ids > 0 ) then
    for i,v in ipairs(deleted_ids)do
        if(not map[v])then
            map[v] =  "123"
            table.insert(arr,v)
            count_trash = #arr
        end
    end
    end
    -- print(cjson.encode(deleted_ids))
    local query = {}
    query["$or"]={}
    query["$or"][0]={subject={ ['$regex']=body.subject, ['$options'] = 'i' }}
    query["$or"][1]={owner={ ['$regex']=body.subject, ['$options'] = 'i' }}
    query["$or"][2]={signers={['$elemMatch']={ email={['$regex']=body.subject, ['$options'] = 'i' }}}}
    query["$or"][3]={signers={['$elemMatch']={name={ ['$regex']=body.subject, ['$options'] = 'i' }}}}
    -- query["$or"][4]={originalFile={ ['$regex']=body.subject, ['$options'] = 'i' }}
    query["$or"][4]={msg={ ['$regex']=body.subject, ['$options'] = 'i' }}
    -- signers:{$elemMatch:{name:{$regex:"pat"}}
    local my_email = body.adminEmail --todo change to session
        --  query = {subject="testt",subject="TestFOrm"}
     s = col:find(query,{owner=1,subject=1,_id=1,pdfFile=1,id=1,createdDtm=1,time=1,msg=1,signers=1,height=1,originalFile=1,width=1,_company=1,ip_stamp=1,fields=1,imageMerge=1,imageFiles=1,status=1,complete_dtm=1})  
    if s then 
        data = {}
      local  data2 = {}
        for k, doc in s:pairs() do
            
            doc._id = nil
            local signers =common.edoc_get_real_signers(doc)
            local can_sign = false

            for _, person in ipairs(signers) do
                if(person.email==my_email)then
                    can_sign = true
                end
            end
            
            if(can_sign or doc.owner==my_email) then
                table.insert(data2,doc)
            end
        end

        for m,l in ipairs(data2) do
            if chk_deleted_ids(deleted_ids,l) then
                table.insert(data,l.id .. '.' ..l.subject.. '.' ..l.msg.. '.' ..l.owner)
            end
        end

        ret = {success = true, error_msg = "complete", data = data}

    else
        ret = {success = false, error_msg = "err"}
    end
end

   
print(cjson.encode(ret))
