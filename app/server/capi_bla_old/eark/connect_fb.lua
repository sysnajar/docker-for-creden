#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local https=require 'ssl.https'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {accessToken = 'EAACEdEose0cBACJEZCF2TrA5QPpY5gqhDlerjzjZCZBLY3wuexcZBwnDpG2MMPPxPD1hglEiC0V9shbGqrzAEM6JTjgrLJivltT6vwIZCpia4b0N2LRdN3BZCECwuPZB0BQhgBAh0lS6ZC1YZCHg2k8RvieYZAeiZAvCAVmOpkbd8MZA8zmuwyQSrFDOjJU17IOgtzcZD'} 
end

ret = {success = false, error_msg = '', data = {}}
url = 'https://graph.facebook.com/me?fields=id,birthday,education,email,about,gender,first_name,last_name,work,likes,friends&access_token='..body.accessToken
fb,c,l,h = https.request(url)
body = cjson.decode(fb)

ret = {success = true, error_msg = "complete", data = body}

profilePicture = 'https://graph.facebook.com/v2.10/'..body.id..'/picture?type=large&redirect=true'

if not body.error then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret.error_msg = err
    else
        db = conn:new_db_handle("test")
        fb = db:get_col("facebook")
        fb:update({fb_Id=body.id}, body, 1, 0, true)
    end
else
    ret.error_msg = "Login Fail!!"
end

print(cjson.encode(ret))
