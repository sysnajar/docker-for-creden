#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
HOST_NAME = os.getenv("HOST_NAME")

function gen_qrcode(s,host) 
    local link = host .. "/company/#/document?id=" .. s.id
    res, err = prog('sh','/home/creden/work/app/server/capi/generate_qrcode.sh', 'qrcode_' .. s.id, link)
end
-- end qrcode fn

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body  = {} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    s, err = col:find_one({ id = '1581485866' })
    s._id = nil

    local qrcode = "/home/creden/work/app/client/face/images/qrcode/qrcode_" .. s.id .. ".png"
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local page = "0"
    local pdfFile = dir .. s.pdfFile .. '.pdf'
    local cmd  = 'java -jar /home/creden/scripts/pdfstamp.jar -i '.. qrcode .. ' -l '.. 40 ..','.. 40 ..' -d 200 -o '.. dir ..' -p '.. page .. ' ' .. pdfFile
    print(cmd)
    -- if s then
    --     gen_qrcode(s,HOST_NAME)
    -- else
    --     print("not find document")
    -- end
end
-- print(cjson.encode(ret))

