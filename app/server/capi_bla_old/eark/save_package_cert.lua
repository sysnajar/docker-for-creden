#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil
bson = require "resty-mongol.bson"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})

function gen_num( )
    local server_id = '500'
    local inc = tostring(rc:incr('genref_eks'))
    while(#inc<7) do
         inc = '0' .. inc
    end
 
    return server_id .. inc
 end

 function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.email = session.data.username
        body.company_id = session.data.company_id
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    check_session()



    db = conn:new_db_handle("edoc")
    col = db:get_col("booked_slot")
    find_booking,err = col:find_one({ booking_id = body.booking_id, booking_owner= body.email})

    if find_booking then
        find_booking._id = tostring(find_booking._id)
        ret = { success = true, data = find_booking }
    else
        ret = { success = false, msg = "no booking data" }
        print(cjson.encode(ret))
        ngx.exit(ngx.HTTP_OK)
    end

 

    db = conn:new_db_handle("dopa")
    col = db:get_col("package_ss")

    local price_map = {['1'] = 300, ['12'] = 1000}
    
    local price = tostring(price_map[find_booking.booking_exp_month])

    body.package = "dipchip"
    body.exp_month = find_booking.booking_exp_month 
    body.price_package = price
    body.total = price
    body.qr_refno = "GG" .. os.time()

	local key = "mobile.scan." .. body.qr_refno
	rc:set(key, session.data.username)

    body.date_buy = bson.get_utc_date(os.time() * 1000)
    body.status = false

    body.ref_id = gen_num()
    u, err = col:insert({body})

    if u then 
        ret = { success = true, error_msg = "Add package success", error_code = "", data = body}
    else
        ret = { success = false, error_msg = "Add package not found", error_code = "" }
    end


end

print(cjson.encode(ret))
