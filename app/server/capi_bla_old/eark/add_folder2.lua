#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { company_id = '123', folder = 'than'}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else    
    
    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders2")
    local date = bson.get_utc_date(ngx.now() * 1000)

    local new_folder = {
        owner_folder = body.email,
        name_folders = body.name_folder,
        created_Dtm = date,
        cat = os.time(),
        status_folder = "1",
        list_doc = {},
    }
    u, err = col:insert({new_folder})

    if err then
        ret = { success = false, error_msg = "Add folder unsuccess", error_code = "2202" }
    else 
        ret = { success = true, error_msg = "Add folder success", error_code = "2201" }
    end

end

print(cjson.encode(ret))
