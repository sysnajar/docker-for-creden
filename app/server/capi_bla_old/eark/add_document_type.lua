#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { name_document_type = arg[1] }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("document_type")
    local date = os.date('%d/%m/%Y %H:%M:%S', os.time() + (7*60*60))

    local data = { 
        name_doc_type = body.name_doc_type, 
        signers = body.signers,
        use_signing_order = body.use_signing_order, 
        owner_doc_type = body.owner ,
        company_id = body.company_id ,
        createDtm = date
    }
    u,err = col:insert({data})

    if u then 
        ret = { success = true, error_msg = "Add document type success" }
    else
        ret = { success = false, error_msg = "Add document type unsuccess" }
    end
end

print(cjson.encode(ret))
