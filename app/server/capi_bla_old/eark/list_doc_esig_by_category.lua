#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	-- data={}
	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list")

  if(not body.email or body.email=='')then  body.email = 'XXXXXhjhgjhghghjghjs' end	

--prevent email param
if(not session.data.username ) then
    print(cjson.encode({success = false, error_msg = "session expired"}))
	ngx.exit(ngx.HTTP_OK)
else
	if(body.email~=session.data.username) then
        ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
	    body.email = session.data.username
	end
end	
--prevent email param

  --find trash list
  local deleted_docs = db:get_col('deleted_doc'):find({email = body.email})
  local deleted_ids = {0}
	for k, v in deleted_docs:pairs() do
       table.insert(deleted_ids, tostring(v.id))
    end
 --end


  s = {}  
  query = {}

  -- if body.cat=="all" then
  --   query = {signers={['$elemMatch']={email=body.email} }}
  -- end 

  if body.cat=="signer" then
    query = {signers={['$elemMatch']={email=body.email}}}
  end  

  if body.cat=="owner" then
    query = {owner=body.email}
  end  
  
  if(body.cat == "trash") then
     query = {id = {['$in']  = deleted_ids}}
  else
     query.id = {['$nin'] = deleted_ids}
  end


  if body.cat=="contact" then
    query = {owner="xxxx"}
  end  



  if(body.status==0 or body.status==1) then
    query.status = body.status
  end


    local s2 = nil
    if body.cat=="all" then
        query_signer = {owner={['$ne']=body.email}, signers={['$elemMatch']={email=body.email}}}
        query_owner = {owner=body.email}
        s_signer = col:find(query_signer)
        s_owner = col:find(query_owner)
        --add owner to s table
        for k, v in s_owner:pairs() do
            table.insert(s,v)
        end
        --add signer to s table
        for k1, v1 in s_signer:pairs() do
            table.insert(s,v1)
        end
	    s2 = s
    else
    	s  = col:find(query)
		s2 = {} 
	    for k, v in s:pairs() do table.insert(s2, v) end
    end

	tmp = {}
	for k, v in ipairs(s2) do
       v._id = tostring(v._id)
       v._company = tostring(v._company)
       if v.createdDtm then
       		v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
	   end
       table.insert(tmp,v)
    end
    ret = {success = true, error_msg = "complete", data = tmp, deleted_ids = deleted_ids,cat=body.cat}
	--print("XXX")
    print(cjson.encode(ret))
end
