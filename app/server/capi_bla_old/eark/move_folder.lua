#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
-- bson = require "resty-mongol.bson"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    -- body = { id_doc = "5d14a2278a1a2eb6bd2c95e5", id_folder = "5d5bd38e8a1a2eb6bd2cc92f"}
    body = { id_doc = arg[1], id_folder = arg[2]}
    -- body = {id_doc="1566192890",id_folder="5d5cc49a8a1a2eb6bd2cca5d"},{id_doc="1564987658",id_folder="5d5cc49a8a1a2eb6bd2cca5d"},{id_doc="1564651474",id_folder="5d5cc49a8a1a2eb6bd2cca5d"},{id_doc="1564037365",id_folder="5d5cc49a8a1a2eb6bd2cca5d"},{id_doc="1564036918",id_folder="5d5cc49a8a1a2eb6bd2cca5d"},{id_doc="1563861960",id_folder="5d5cc49a8a1a2eb6bd2cca5d"},{id_doc="1562922764",id_folder="5d5cc49a8a1a2eb6bd2cca5d"}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else    

    db = conn:new_db_handle("edoc")
    col = db:get_col("folders")

    for i,r in ipairs(body) do 

        folder = { _id = object_id.new(convertHexStringToNormal(r.id_folder))}
        f,err = col:find_one(folder)
        local tmp = {}

        if f then  
            -- doc = object_id.new(convertHexStringToNormal(body.id_doc))
            doc = r.id_doc

            if not f.list_doc or  #f.list_doc == 0 then 

                list = {id_doc = doc , updateDtm = os.date('%d/%m/%Y %H:%M:%S') }
                table.insert(tmp,list)
                update = {["$set"] = {list_doc = tmp}}
                folders, err = col:update(f, update, 0, 0, true)
                ret = { success = true, error_msg = "Move Document Success 1", error_code = ""}

            elseif #f.list_doc > 0 then

                tmp = {}
                for k, v in ipairs(f.list_doc) do
                    if v.id_doc ~= doc then
                        table.insert(tmp,v)
                    end
                end
                list = {id_doc = doc , updateDtm = os.date('%d/%m/%Y %H:%M:%S') }
                table.insert(tmp,list)
                update = {["$set"] = {list_doc = tmp}}
                id = {_id = object_id.new(convertHexStringToNormal(r.id_folder))}
                folders, err = col:update(id, update, 0, 0, true)
                if folders then
                    ret = { success = true, error_msg = "Move Document Success 2", error_code = "" }
                else
                    ret= { success = false, error_msg = "error update", error_code = "" }
                end

            end
        else
            ret = { success = false, error_msg = "No Folder", error_code = ""}
        end
    end
end
print(cjson.encode(ret))
