#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { image = '123', company_id='123'}
end
conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else  
    db = conn:new_db_handle("dopa")
    col = db:get_col("user_company")
    if session.data.username then 
        local query = { _id = object_id.new(convertHexStringToNormal(session.data.company_id))}
        
        e, err = col:find_one(query)
        if err then
            ret = { success = false, error_msg = "find not stamp", error_code = "" }
        else
            local update_cmd = {
                company_stamp = body.image,
            }
            local u, err = col:update(query, {["$set"] = update_cmd }, 0, 0, true)
            if err then
                ret = { success = false, error_msg = "Update stamp unsuccess", error_code = "" }
            else
                ret = { success = true, error_msg = "Update stamp success", error_code = "" }
            end
        end

    else
        ret = {success = false, error_msg = err, msg = "no session" }
    end

end
print(cjson.encode(ret))