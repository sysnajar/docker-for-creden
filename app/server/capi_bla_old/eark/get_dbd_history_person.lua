#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if ngx then  
    ngx.req.read_body()
    print = ngx.say
    --body = ngx.req.get_body_data()
    body = ngx.req.get_uri_args()
    --body = cjson.decode(body)
else
   body  = {name = arg[1]}
end
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	db = conn:new_db_handle("newdbd")
	col = db:get_col("history")

	s = col:find({name = body.name})
	data = {}
	for k, v in s:pairs() do
	    v._id = nil
	    table.insert(data, v)
	end
	print(cjson.encode({success = true, data = data}))
end
