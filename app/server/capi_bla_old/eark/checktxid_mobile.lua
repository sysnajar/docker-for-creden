#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {}
prog.timeout = 1000 * 300
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)

else
    body = {txid='GG1234567'}
end

if(body.txid ~=nil) then
   if(body.txid == "GG1234567") then
        ret.txid = body.txid
        ret.success = true
   else
        ret.txid = body.txid
        ret.success = false
   end
else
    ret.txid = body.txid
    ret.success = false
    ret.msg = 'not found'
end
print(cjson.encode(ret))
