#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

if not body.txid then
    ret.error_msg = 'Invalid Data'
    print(cjson.encode(ret))
    return 0
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
    local col_ekyc = db_dopa:get_col("ekyc")
    local col_sdk_code = db_dopa:get_col("sdk_code")
    local col_esig_user = db_dopa:get_col("esig_user")
    local approve_dmt = bson.get_utc_date(ngx.now() * 1000)
    local approve_by = body.approvedBy or 'N/A'
    local approve_manual = true
    update, err_u = col_ekyc:update({txid=body.txid}, {["$set"] = {isValid = body.approve, approve_by = approve_by, approve_dmt = approve_dmt, approve_manual = approve_manual}}, 0, 0, true)
    if not err_u then
        if body.channel == 'sdk' then
            find_email, err_f = col_sdk_code:find_one({txid=body.txid})
            if find_email then
                if find_email.ref_no then
                    update, err_u = col_esig_user:update({adminEmail=find_email.ref_no}, {["$set"] = {verified_ekyc = body.approve, ekyc_approve_by = body.approve_by, ekyc_approve_dmt = approve_dmt, ekyc_approve_manual = approve_manual}}, 0, 0, true)
                    --todo send noti
                    -- NOTI
                        local sess_col = db_dopa:get_col('sdk_code')
                        local kyc_session  = sess_col:find_one({txid = body.txid, username = 'creden'})
                        if(kyc_session and kyc_session.ref_no) then 
                            local notifications = {
                                    {id     = ngx.now()*1000,
                                        dtm = ngx.now()*1000,
                                        status = 0,
                                        description = 11,
                                        txid = body.txid,
                                        email = kyc_session.ref_no,
                                        code = kyc_session.code,
                                        isValid = body.approve
                                        } 
                                    }
                
                            local db2 = conn:new_db_handle("edoc")
                            local ncol       = db2:get_col("notification")
                            local i2, err2   = ncol:insert(notifications, nil, true)                            
                        end
                    -- END NOTI

					--send mail invite
					cmd = 'node /home/creden/work/app/server/capi/approve_ekyc.js "'.. find_email.ref_no ..'" ' .. tostring(body.approve) ..  ' &'
					os.execute(cmd)
					--send mail invite
                end
            end
        end
        ret.success = true
    else 
        ret.error_msg = err_u
    end
end

print(cjson.encode(ret))
   
