#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local scrypt = require "resty.scrypt"
common = require "common"
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body   = {user="codernajar@gmail.com",list_phone_number=''} 
end

	body.user = body.user:lower()
    conn = mongo() 
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

	if err then
        ret = {success = false, error_msg = err}
    else
		db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        s, err = col:find_one({ adminEmail = body.user or "N/A" }, nil, true)
		
		if s then
			math.randomseed(os.clock()*100000000000)
            otp = ''
            for i=1,3 do
            	a = math.random(10, 99)       
                otp = otp .. a
            end

			local update_cmd = {["$set"] = {
				list_phone_number  = body.list_phone_number
			}}
			u, err = col:update({adminEmail=body.user}, update_cmd, 0, 0, true)
			
			token = ''
			for i=1,10 do
				a = math.random(10, 99)       
				token = token .. a
			end
			key = 'otp_'..token..'_'..otp
			rc:set(key,1)
			rc:expire(key,300)

			local m, err = prog('node', '/home/creden/work/app/server/capi/reset_mobile_number_otp.js', body.user, otp) 
			ret = { success = true, error_msg = 'Email otp', error_code = "" ,token = token }
		else
			ret = { success = false, error_msg = 'Email already exists.', error_code = "P00" }
		end
	end

	print(cjson.encode(ret))