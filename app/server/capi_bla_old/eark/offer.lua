#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = 'kjunchean@gmail.com', password = '1'} 
end

t = {success = true,
     data = {
            {
                title="River Star Princess for Silkspan customer",
                desc="ลด 40% จากปกติ 1500 เหลือ 900",
                pic="https://www.silkspan.com/Voucher/Popup_privilege/Images/hotel/a_homey_condi.jpg"
            },{
                title="Irene Clinic for Silkspan customer",
                desc="Botox ลดกรามไม่จำกัด unit ราคาเพียง 3999",
                pic="https://www.silkspan.com/Voucher/Popup_privilege/Images/spa/cc/cc340_irene-botox.jpg"
            }
        }
    }

print(cjson.encode(t))
