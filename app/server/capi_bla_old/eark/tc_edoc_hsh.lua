#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false
local HOST_NAME = os.getenv("HOST_NAME") or 'HSH'

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
conn = mongo()
conn:set_timeout(10000)
ok, err = conn:connect('127.0.0.1',27017)
db = conn:new_db_handle("dopa")
col = db:get_col("esig_user")

function get_timezone_thai (time)
	return os.date()
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
else
    body  = {} 
end
--data = '{"pdfFile":"1534752427","ip_stamp":"96.30.94.34","width":722,"id":"1534752427","createdDtm":1534752457092,"owner":"kitchinvuttinunt@gmail.com","subject":"Please sign หนังสือรับรองรายได้ผู้ปกครอง-ทกศ.032 (1).pdf","imageMerge":"pdf1534752424.png","fields":[{"x":0,"toolName":"Signature","id":1534752457055,"page":0,"xp":30,"status":1,"email":"kitchinvuttinunt@gmail.com","time":"08\/20\/18 08:07:52","scale":100,"yp":12,"ip_field":"96.30.94.34","toolData":"Signature","n":1,"y":0,"type":"draw"}],"signers":[{"needToSign":true,"eKyc":false,"usePassword":false,"name":"kit","password":"","email":"kitchinvuttinunt@gmail.com"}],"height":1021,"originalFile":"หนังสือรับรองรายได้ผู้ปกครอง-ทกศ.032 (1).pdf","msg":"","status":1,"imageFiles":[{"fileName":"pdf1534752424.png","aspectRatio":0.70665083135392}]}'
--body = cjson.decode(data)
--body = cjson.decode(arg[1])

--json = rc:get('tc_edoc_2')
--body = cjson.decode(json)
body = cjson.decode(arg[1])


---Reduce fields to 1 field----------------------------
local arr = {}
for i,f in ipairs(body.fields) do
   if(f.toolName=='Signature')then
	table.insert(arr,f)
	break
   end
end
body.fields = arr
---------------------------------


t = {}
table.sort(body.fields,function (f1, f2) 
	return tostring(f1.time) < tostring(f2.time)
    --return f1.time<f2.time
end)


local fields2 = {}
local tt = {}

for k,v in pairs(body.fields) do
    if(not tt[v.email])then
      tt[v.email] = true
      table.insert(fields2, v)
    end
end

s = col:find_one({adminEmail =body.owner })

-- fix API signing produce no timestamp
 s.FNAME = s.FNAME or body.owner 
 s.LNAME = s.LNAME or ''

--s.LNAME = ''
html_head = [[<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><style>@font-face {font-family: 'THSarabunNew';src: url('./sarabun-webfont-master/fonts/thsarabunnew-webfont.woff');font-weight: normal;font-style: normal;} body {font-family: 'THSarabunNew';} table, th, td {border: 1px solid black;text-align: center;border-collapse: collapse;}</style>
    <title></title>
</head>
<style>table{
    width:100%;
}</style>
<body>
    <img style='display: block; margin-left: auto; margin-right: auto;' src=']]..HOST_NAME..[[/esig/app-assets/images/logo/robust-logo-dark.png' width='100'>
    <table>
        <tr>
            <td  colspan='3' style='background-color:powderblue;'>Certificate Of Completion/ใบรับรองการสำเร็จ
</td>
        </tr>
        <tr>
            <td>Envelope Id/หมายเลขเอกสาร: ]]..body.id..[[</td>
            <td colspan='2'>Status: Completed/สถานะ:สมบูรณ์</td>
        </tr>
        <tr>
            <td colspan='3'>Subject: Please DocuSign/เรื่องเอกสาร: ]]..body.originalFile..[[</td>
        </tr>
        <tr>
            <td>Document Pages/จำนวนหน้า: ]]..table.getn(body.imageFiles)..[[</td>
            <td colspan='2'>Signatures/ลายเซ็น: ]]..table.getn(body.signers)..[[</td>
        </tr>
        <tr>
            <td>Certificate Pages/จำนวนหน้าใบรับรอง:
 1 </td>
            <td colspan='2'>]]..(s.FNAME or '')..' '..s.LNAME..[[</td>
        </tr>
        <tr>
            <td></td>
            <td colspan='2'>]]..body.owner..[[</td>
        </tr>
        <tr>
            <td>EnvelopeId Stamping: Enabled/เวลาเปิดใช้งาน</td>
            <td colspan='2'>IP Address: ]]..body.ip_stamp..[[</td>
        </tr>
        <tr>
            <td colspan='3'>Time Zone: (GMT+07:00) Bangkok</td>
        </tr>
    </table>
    <br>
    <table>
        <tr>
            <td colspan='3'style='background-color:powderblue;'>Record Tracking/การติดตามบันทึก</td>
        </tr>
        <tr>
            <td>Status/สถานะ: ]]..os.date('%d/%m/%Y %H:%M:%S',body.createdDtm/1000 + (7 * 60 * 60))..[[</td>
            <td>Holder/เจ้าของ: ]]..(s.FNAME or '')..' '..(s.LNAME or '')..' '..body.owner..[[</td>
            <td>Location/สถานะ: Creden.co</td>
        </tr>
    </table>
    <br>
    <table>
        <tr style='background-color:powderblue;'>
            <td>Signer Events/ลำดับการเซ็น</td>
            <td>Timestamp/เวลา</td>
        </tr>]]
        head2 = ''
        for i2,v2 in ipairs(body.fields) do
            s2 = col:find_one({adminEmail =v2.email })
			
			if (not s2) then 
				s2 = {} -- API user may not have creden account
            end

            kyc_text = ''
            if s2.verified_ekyc == true then
                kyc_text = ',EKYC'
            end
            if FNAME then
                em = ' ('..v2.email..')'
            else
                em = v2.email
            end
            head2 = head2 .. [[
                        <tr>
                            <td>]]..i2..') '..(s2.FNAME or '')..' '..(s2.LNAME or '')..em..[[</td>
                            <td>Sent/ส่งเมื่อ: ]].. os.date('%d/%m/%Y %H:%M:%S',(body.createdDtm/1000) + (7 * 60 * 60)) ..[[</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Signed/เซ็นครบเมื่อ: ]].. get_timezone_thai(v2.time) ..[[</td>
                        </tr>
                        <tr>
                            <td>Security Level/ระดับความปลอดภัย
: Email/อีเมล์,OTP/รหัสยืนยัน]].. kyc_text..[[</td>
                            <td>Using IP Address/ใช้หมายเลข IP Address: ]]..tostring(v2.ip_field)..[[</td>
                        </tr>
                    ]]
        end
        
 head3 = [[
    </table>
    <br>
    <table>
        <tr style='background-color:powderblue;'>
            <td>Envelope Summary Events/สรุปกิจกรรมการเซ็น</td>
            <td>Timestamp/เวลา</td>
        </tr>
        <tr>
            <td>Envelope sent/ส่งเมื่อ</td>
            <td>]]..os.date('%d/%m/%Y %H:%M:%S',body.createdDtm/1000 + (7 * 60 * 60))..[[</td>
        </tr>
        <tr>
            <td>Signing complete/เซ็นครบเมื่อตอนไหน</td>
            <td>]].. get_timezone_thai() ..[[</td>
        </tr>
        <tr>           
            <td>completed/เซ็นสมบูรณ์</td>
            <td>]]..os.date('%d/%m/%Y %H:%M:%S', os.time() + (7 * 60 * 60))..[[</td>
        </tr>
    ]]



html_end = [[</table></body></html>]]


html = html_head..head2..head3..html_end
--print(html)
os.execute('sh /home/creden/work/app/server/capi/convert_htmltopdf.sh "'..html..'" '..body.id..'ts')



