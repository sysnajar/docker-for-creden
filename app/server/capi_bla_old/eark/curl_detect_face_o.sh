# 1).create group
# curl -v -X PUT "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb005_03" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii '{"name": "group1","userData": "user-provided data attached to the person group.","recognitionModel": "recognition_01"}' 

# 2).เพิ่มคนเข้า group จะได้หมายเลข personId
# curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb005_a/persons" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii '{"name": "'$1'","userData": "'$2'"}' 
#personId 98bab721-ad46-4a9e-adbd-ebca3ee1f887 
# curl -v -X GET "https://westus.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb003/persons/{personId}" \
# -H "Ocp-Apim-Subscription-Key: {subscription key}" \
# --data-ascii "{body}" 
# curl -v -X GET "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb005_a/persons?start=0&top=1000" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii "{body}" 

# 3).แอดรูปของคนที่อยู่ในกลุ่มนั้นจากหัวข้อที่สอง สิ่งที่ต้องใช้ ชื่อกลุ่ม และหมายเลข personId ตัวอย่างนี้จะใช้รูป ป๊อปปองกูลเป็นหลัก
# curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb005_a/persons/4e343f39-8468-4d51-9333-baa3dcdd4439/persistedFaces?userData=Person1&detectionModel=detection_01" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-binary '{"url":"https://praew.com/app/uploads/2019/03/fsdset.jpg"}'
# persistedFaceId 67c5f3b3-318e-4fba-902d-1d4c9ba43e9d

# 4).เมื่อทำ หัวข้อที่ สามเสร็จ แล้วให้ทำการ traing 
# Start Traing
#********************************************
# curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb005_a/train" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii "{body}" 
# Check status Traing
# curl -v -X GET "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/persongroups/ghb005_a/training" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii "{body}" 
#********************************************
# end Traing and check Traing

# 5).เมื่อต้องการเทียบรูปบุคคลอื่นให้มาทำที่อันนี้ เพื่อเอา faceId 
# 5.1) ถ้าทำการเพิ่มรูปที่ต้องการเปรียบเทียบแล้วให้ กลับไปทำ หัวข้อที่ 4 ทุกๆครั้ง
curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&recognitionModel=recognition_02&returnRecognitionModel=false&detectionModel=detection_02" \
-H "Content-Type: application/json" \
-H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
--data-ascii '{"url": "https://hjkl.ninja/face/images/card/rec_885-1.png"}'

# curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/identify" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii '{"personGroupId": "ghb005_03","faceIds": ["ec4640aa-5600-44d2-92a2-44d878709b15"],"maxNumOfCandidatesReturned": 1,"confidenceThreshold": 0.5}' 
# "faceId":"650bd8f4-e13a-4cd6-a01a-e507646de5b4" image1
# "faceId":"f7040a0a-5c9a-41ee-b615-d5c63e1fd883" image2

# "faceId":"a21ce4af-93da-4460-a7d3-87e0595dc136" image3 POP
# "faceId":"86cc89d7-6403-4b63-b93f-88aa2317407c" image4 GOLF

# ec4640aa-5600-44d2-92a2-44d878709b15 image face o

# 6.)เป็นการเอาสองรูปมา แยกแยะก่อน
# curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/identify" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii '{"personGroupId": "ghb003","faceIds": ["a21ce4af-93da-4460-a7d3-87e0595dc136","86cc89d7-6403-4b63-b93f-88aa2317407c"],"maxNumOfCandidatesReturned": 1,"confidenceThreshold": 0.5}' 


# 7.)สุดท้าย ให้ไปทำการ verify โดยจะต้องเอา เลข PersonId ที่ได้จาก ตอนเพิ่มคนเข้ากลุ่ม ของหัวข้อที่ 2 มาใช้ 
# faceId คือตัวรูปที่เราจะเอามาเทียบกับหน้า ของคนที่อยู่ในกลุ่มเรา
# curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/verify" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii '{"faceId": "ec4640aa-5600-44d2-92a2-44d878709b15","personId": "98bab721-ad46-4a9e-adbd-ebca3ee1f887","personGroupId": "ghb003"}' 

