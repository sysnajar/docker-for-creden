#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {company_id = '5c2d92cc778fdc6a5f91c0e1'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

-- if not body.company_detail.company_id or body.company_detail.company_id == "" then
--     ret.error_msg = 'Invalid Data'
--     print(cjson.encode(ret))
--     return 0
-- end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else
    com = body.company_detail

    if not com.company_id then
        ret = { success = false, error_msg = "not company id", error_code = "9002" }
    else
        check_session()
        db = conn:new_db_handle("dopa")
        col = db:get_col("user_company")
        local id_com = object_id.new(convertHexStringToNormal(com.company_id))
        update_cmd = {
            companyName = com.companyName or "",
            companyTaxNo = com.companyTaxNo or "",
            companyBranch = com.companyBranch or "",
            companyAddress = com.companyAddress or "",
            companyLogo = com.companyLogo or "",
        }
        c, err = col:update({ _id = id_com }, {["$set"] = update_cmd }, 0, 0, true)

        if err then
            ret = { success = false, error_msg = "update companay detail unsuccess", error_code = "" }
        else
            local redis_key = 'companyLogo.' .. session.data.company_id
            rc:del(redis_key)
            ret = { success = true, error_msg = "update companay detail success", error_code = "" }
        end

    end
end
print(cjson.encode(ret))