#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    --body = cjson.decode(body)
else
   body  = {email = arg[1]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("test")
    col = db:get_col("kyc")
    s = col:find()

    if s then
        data = {}
        for k, v in s:pairs() do
            v._id = v._id:tostring()
            table.insert(data, k, v)
        end
        ret = {success = true, error_msg = "complete", data = data}
    else
        ret = {success = false, error_msg = "N?A"}
    end
end

--[[
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("test")
    col = db:get_col("user")
    s = col:find_one({email=body.email, activated=1})

    if s then
        --kyc
        res, err = prog('/capi/kyc.lua', body.firstname, body.lastname, body.citizenIdF, body.citizenIdB, body.dob)
        if(not err)then
            print(res.stdout)
            kyc = cjson.decode(res.stdout)
        end

        --update
        selector = {email=body.email, activated=1}
        update = {["$set"] = {kyc = 1, citizenIdF = body.citizenIdF, citizenIdB = body.citizenIdB}}
        i, err = col:update(selector, update, 0, 0, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            ret = {success = kyc.success, error_msg = kyc.card_message, death = kyc.death.valid}
        end
    else
        ret = {success = false, error_msg = "not valid email"}
    end
end
]]
print(cjson.encode(ret))


