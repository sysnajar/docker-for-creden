#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local HOST_NAME = os.getenv("HOST_NAME")
local prog2 = exec.new('/tmp/exec.facebox.sock')
prog2.timeout_fatal = false

local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local common = require "common"

local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    apiKey = ngx.req.get_headers().apiKey or body.apiKey
    -- ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

function trim5(s)
	  return s:match'^%s*(.*%S)' or ''
end

function replace_char(pos, str, r)
    return str:sub(1, pos-1) .. r .. str:sub(pos+1)
end


function nameEN(txt)
local want = false
-- local text = rc:get('TestOx')
local full_nameEN = ''
local firstnameEn = ""
local firstnameEn_ = ""
local lastnameEn = ""
local lastnameEn_ = ""



    for i in string.gmatch(txt, "[^\r\n]+") do
    local arr_filter_titlename = {
      "Mr.","Mr","Name Mr,","Name Mr.","Nume Mr.","Num. Mr.","Nom Mr.","Nam Mr.","NemMr.","Nome Mr.","Mr. ","Miss","Miss ","Miss.","Ms",
      "Mrs","Mrs.","Feride Mr. "
    }

    local arr_filter_titlename2 = {
      ["Name Mr,"] = "Mr.",
      ["Mr"] = "Mr.",
      ["Feride Mr. "] = "Mr.",
      ["Name Mr."] = "Mr.",
        ["Nume Mr."] = "Mr.",
        ["Num. Mr."] = "Mr.",
        ["Nom Mr."] = "Mr.",
        ["Nam Mr."] = "Mr.",
        ["NemMr."] = "Mr.",
        ["Nome Mr."] = "Mr.",
        ["Mr. "] = "Mr.",
        ["Miss. "] = "Miss.",
        ["Miss"] = "Miss.",
        ["Miss "] = "Miss.",
        ["Ms. "] = "Ms.",
        ["Mrs. "] = "Mrs.",
        ["Mrs"] = "Mrs.",
        ["Mrs. "] = "Mrs."

    }

    local titlename = ""
    local firstname = ""
    local fao = ""
 
    for _,tn in ipairs(arr_filter_titlename)do
      if(string.find(i,tn) == 1)then
        
        title_first,title_last = string.find(i,tn)
        titlename = string.sub(tn,title_first,title_last)

        firstname = string.sub(i,title_last+1)
        str = string.gsub(firstname, "%s+", "")

        t.firstnameEn = str


      end
    end
  end




    for i in string.gmatch(txt, "[^\r\n]+") do
        local arr_filter_lastnameEn = {
          "Last name ","Last ",

        }

        local arr_filter_lastnameEn2 = {
          ["Last name "] = "Last Name",
          ["Last"] = "Last Name",
          
        }


        local titlenameEN = ""
        -- local firstname = ""
        local fao = ""
        for _,tn in ipairs(arr_filter_lastnameEn)do
          if(string.find(i,tn) == 1)then
            -- LASTNAME EN----
            lastnameEn,lastname_lastEn = string.find(i,tn)
            lastnameEn = string.sub(tn,lastnameEn,lastname_lastEn)
            lastnameEn = arr_filter_lastnameEn2[lastnameEn] or lastnameEn
            latnameEn_ = string.sub(i,lastname_lastEn+1)
              str = string.gsub(latnameEn_, "%s+", "")
              if string.find(str,'name') then
                str = string.gsub(str,'name','')
              end

              t.lastnameEn = str

          end
        end
    end
end
function dob_line(txt)
local dob_line  = nil
local i = 0
local c = {}
for line in string.gmatch(txt, '[^\r\n]+') do
    --print(i,line)
    i = i + 1

    if(dob_line == nil and string.find(line, 'เกิดวันที่')~=nil) then
        dob_line = i + 1
    end
    
    if(dob_line == nil and (string.find(line, 'Date')~=nil or string.find(line, 'Birth')~= nil)) then
       dob_line = i
       break
    end

    if(dob_line == nil and string.find(line, 'ศาสนา')~=nil) then
            dob_line = i -1
        break
    end
    table.insert(c, line)
end


return c, dob_line
end
function province_line(txt)
local dob_line  = nil
local i = 0
local c = {}
for line in string.gmatch(txt, '[^\r\n]+') do
    --print(i,line)
    i = i + 1
    
    if(dob_line == nil and (string.find(line, 'Date')~=nil or string.find(line, 'Birth')~= nil)) then
       dob_line = i+3
    end

    if(dob_line == nil and string.find(line, 'ศาสนา')~=nil) then
        if(trim5(line) == 'ศาสนา')then
			dob_line = i + 3
		else
		    dob_line = i + 2
		end	
    end
    table.insert(c, line)
end
return c, dob_line
end

function nameTh(txt)
    want = false
    full_name = ''
    find_first = true
    for line in string.gmatch(txt, '[^\r\n]+') do 
      --print(line)
      if find_first and ( string.find(line, 'ด.ช.') or string.find(line, 'นาย') or string.find(line, 'นางสาว') or string.find(line, 'นาง') or string.find(line, 'ชื่อตัวและชื่อสกุล') or string.find(line, 'น.ส.')) then
        want = true
        find_first = false
      end
      if want then
        if string.find(line,"[a-z]") then
          want = false
        end
        if want then
          full_name = full_name .. ' '..line
          break
        end
      end
    end
    line = full_name
  
    words = {}
    line = string.gsub(line,'ชื่อตัวและชื่อสกุล','')
    for word in string.gmatch(line,"%S+") do table.insert(words, word) end
      size_words = table.getn(words)
      t.name = words[size_words-1] or ''
      if string.find(t.name, 'นาย') then
        t.name = string.gsub(t.name,'นาย','')
      elseif string.find(t.name, 'นางสาว') then
        t.name = string.gsub(t.name,'นางสาว','')
      elseif string.find(t.name, 'น.ส.') then
          t.name = string.gsub(t.name,'น.ส.','')
      elseif string.find(t.name, 'นาง') then
        t.name = string.gsub(t.name,'นาง','')
      end
      t.lastname = words[size_words] or ''   
    end

-- common.validate_alphanumeric(body.img, 'invalid param')
    


--body.id = 'dob'
--base64 to image
--name =  "1518750065"

if(apiKey=="AcUjaxcXIxbYyD0UWQioM15OePPIG5u3") then
  local tmp = 'tmp.img1b64.' .. body.cardNo   
        rc:set(tmp , body.img1)
  -- rc:set('tmp.img1b64.'..body.cardNo, body.img1)


  ngx.log(ngx.NOTICE,"result_url_2  connect===== dddds" )

  local res, err = prog('lua', '/home/creden/work/app/server_amway/save_file_deBigLoad.lua', body.cardNo)

  
   reslut = cjson.decode(res.stdout)
  -- print(reslut.data)
  name =reslut.data

else
--body.id = 'dob'
--base64 to image
--name =  "1518750065"
rc:set('tmp_card2', body.img)



name = body.img
end
-- name = body.img
local path = "/home/creden/work/app/client/face/images/card/"..name..".png";
local path_emp = "/home/creden/work/app/client/face/images/card/"..name.."_emp.png";
local path_out = "/home/creden/work/app/client/face/images/card/"..name.."_out.png";

--body.is_mobile = 'true'
body.is_mobile = tostring(body.is_mobile)
rc:set('tmp.is_mobile', tostring(body.is_mobile))

if body.is_mobile == 'Mobile' then
  --  os.execute('cp ' .. path .. ' ' .. path ..'.png')
   os.execute('mogrify -rotate 180 ' .. path)
-- else
   --ngx.log(ngx.NOTICE,'not mobile: '..path)
  --  os.execute('sh /home/creden/work/app/server/capi/crop.sh '..path ..' > /dev/null')
end

--reduce file size from fullter
-- os.execute('convert -density 400 -resize 1024 /home/creden/work/app/client/face/images/card/'..name..'.png /home/creden/work/app/client/face/images/card/'..name..'.png')

local face_box = nil
-- ngx.sleep(1)
local face, err = prog('node', '/home/creden/work/app/server/capi/test_curl.js', name)
if face.stdout then
    --print(face.stdout)

	local face_arr = cjson.decode(face.stdout)
   -- ngx.log(ngx.NOTICE,face.stdout)
	if(#face_arr > 0) then
         prog2.stdin = face.stdout
          -- ngx.sleep(1)
         local res2, err2 = prog2('sh', '/home/creden/work/app/server/capi/facebox.sh')
      --   ngx.log(ngx.NOTICE,res2.stdout)
		 -- print('res2.stduot', res2.stdout)
		 face_box  = cjson.decode(res2.stdout)
	end

    t.face = face_arr
end



if(not t.face or #t.face<1)then
   --print(cjson.encode(t))
   t.err = 'ไม่พบใบหน้าในรูป'
   print(cjson.encode(t))

   if(ngx)then
	  --print('ngx.exit')	
	  ngx.exit(200)
    else
	   os.exit(0)	
    end
end

rc:set('tmp.face_count', tostring(#t.face))

--print('box', cjson.encode(face_box))
--  ngx.sleep(1)
local res, err = prog('sh', '/home/creden/work/app/server/capi/img.sh', path, path_emp, path_out, face_box.x1, face_box.y1, face_box.x2, face_box.y2)

t_province = {}
--  ngx.sleep(1)
local res, err = prog('cat', '/home/creden/work/app/server/capi/province.txt')
if not err then
  str = string.gsub(res.stdout, "%s+", "")
  str = '{'..str..'}'
  t_province = cjson.decode(str)
else
  print(err)
end

province = ''
t.sex = ''
local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh', name ..'_out.png', name ..'_out.png')
-- ngx.log(ngx.NOTICE,'sh /home/creden/work/app/server/capi/ocr_google.sh '..HOST_NAME..'/face/images/card/'.. name ..'_out.png '.. name ..'_out.png')
if not err then
    rc:set('tmp.ocr_res', res.stdout)
    rc:set('tmp.ocr_cmd','sh /home/creden/work/app/server/capi/ocr_google.sh '..name ..'_out.png')
    ocr = cjson.decode(res.stdout)
    if ocr.responses[1].fullTextAnnotation then

        long_text = ocr.responses[1].fullTextAnnotation.text
        -- rc:set('TestOx',ocr.responses[1].fullTextAnnotation.text)
        nameTh(long_text);
        nameEN(long_text);
        long_text = string.gsub(long_text, "%s+", "")
        -- print(long_text)
        a = string.match(long_text, '%d%d%d%d%d%d%d%d%d%d%d%d%d')
        
    if a == nil then
      t.err = "รูปไม่ถูกต้อง"
      print(cjson.encode(t))
      return 0
    else
      if string.sub(a, 1, 1) == "U" then
        a = replace_char(1,a,"J")
      elseif string.sub(a, 1, 1) == "V" then
        a = replace_char(1,a,"M")
      end

      if string.sub(a, 2, 2) == "0" then
        a = replace_char(2,a,"C")
      end
    end   
		local line_dob, _i = dob_line(ocr.responses[1].fullTextAnnotation.text)
	    local dob_en = line_dob[_i]
		--print('dob_en', dob_en)
    if dob_en then
      dob_en = string.sub(dob_en, string.find(dob_en, '%d'), #dob_en) 
    end
		--print('dob_en', dob_en)

		local dd,mm,yy = '','',''
		local cnt = 0
    if dob_en then
    for ch in  string.gmatch(dob_en,"%S+") do
        cnt = cnt+1
  			if(cnt==1)then dd = ch end
  			if(cnt==2)then mm = ch end
  			if(cnt==3)then yy = ch end
  		end
    end

        yy = tonumber(yy)

		t.dob = {
        day   = ((string.len(dd) == 1) and '0'..dd or dd),
		mon  = (string.gsub(mm,'%.','')) or '',
	    year = (yy) and tostring(yy + 543) or ''
		}	

        t.card_id = a
        t.success = true

        long_text = ocr.responses[1].fullTextAnnotation.text
       -- print(long_text)
        row_address = 1
        long_text = string.gsub(long_text, "ทีอยู่\n", "ที่อยู่ ")
        long_text = string.gsub(long_text, "ที่อยู\n", "ที่อยู่ ")
        long_text = string.gsub(long_text, "ที่อย่\n", "ที่อยู่ ")
        long_text = string.gsub(long_text, "ที่อยู่\n", "ที่อยู่ ")
        for i in string.gmatch(long_text, "[^\r\n]+") do
            if row_address == 2 then
                t_add = string.gmatch(i,"%S+")
                province = ''
                for j in t_add do
                province = j
                end
                break
            end 

            if string.match(i, "ทีอยู่") or string.match(i, "ที่อยู") or string.match(i, "ที่อย่") or string.match(i, "ที่อยู่") then
                row_address = 2
            end

            if string.match(i, "Mr.") or string.match(i, "Mr") then
                t.sex = 'male'
            elseif string.match(i, "Ms") or string.match(i, "Ms.") or string.match(i, "Mrs") or string.match(i, "Mrs.") or string.match(i, "Miss") then
                t.sex = 'female'
            end 
        end
    end
    if province == '' then
        if ocr.responses[1].fullTextAnnotation then
            local t_line, line = province_line(ocr.responses[1].fullTextAnnotation.text)
            if(line)then
				-- print('line',line)
                t_add = string.gmatch(t_line[line] or '',"%S+")
                province = ''
                for j in t_add do
                    province = j
                end
            end 
        end       
    end 

else
    rc:set('tmp.ocr_error', os.date() .. err)
    print(err)
end

if province ~= '' then
  go_google = true
  for k,v in pairs(t_province) do
    if string.match(k, province) then
        go_google = false
        
        t.province = v
      break
    end
  end

  if go_google then
	  
    rc:set('tmp.province', province)
    local res, err = prog('xvfb-run', '-a', 'node', '/home/creden/google_chk_word.js',province)
    if not err then
      province_google = string.gsub(res.stdout, "%s+", "")
      if t_province[province_google] then
        os.execute('echo \',"'..province..'":"'..t_province[province_google]..'"\' >> /home/creden/work/app/server/capi/province.txt')
        
        t.province = t_province[province_google]
      else
        t.province = ''
      end   
    else
      t.province = ''
    end
  end
else
  t.province = ''
end
-- ngx.sleep(1)
local face, err = prog('node', '/home/creden/work/app/server/capi/test_curl.js', name)
if face.stdout then
    t.face = cjson.decode(face.stdout)

end
print(cjson.encode(t))

