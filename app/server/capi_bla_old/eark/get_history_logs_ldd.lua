#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local bson = require "resty-mongol.bson" 
local session = require "resty.session".open({secret = "4321"})

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

-- u = session.data.username or ""
local myusername  = session.data.username or ''
--prevent email param
if(myusername ~= "ldd1@creden.co") then
	print(cjson.encode({success = false, error_msg = "session expired"}))
	ngx.exit(ngx.HTTP_OK)
end	

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    -- local ddd = 05
    -- local mmm = 12
    -- local yyyy = 2018

    -- local dd = 06
    -- local mm = 12
    -- local yy = 2018  

    local ddd = body.start_dd
    local mmm = body.start_mm
    local yyyy = body.start_yyyy

    local dd = body.end_dd
    local mm = body.end_mm
    local yy = body.end_yyyy

    local db = conn:new_db_handle("edoc")
    local col = db:get_col("activity_history");
        
    local targetDate = (os.time({year=tonumber(yyyy),month=tonumber(mmm),day=tonumber(ddd),hour=0, sec=0})*1000)
    local dtm  = bson.get_utc_date(targetDate)

    local targetDate2 = (os.time({year=tonumber(yy),month=tonumber(mm),day=tonumber(dd),hour=23, sec=59})*1000)
    local dtm2  = bson.get_utc_date(targetDate2)

    local user_list = col:find({createdDtm = {["$gte"] = dtm, ["$lt"] = dtm2}})
    local new_list = {}


    for i, v in user_list:pairs() do 
        v._id = nil
        -- local col = db:get_col("ekyc");
        -- local t, err_ekyc = col:find_one({txid = v.id, compCode = 'globlex'})
        -- if t then
        --     v.ekyc = {remarks = t.remarks, isValid = t.isValid, isValidDopa = t.isValidDopa, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, isValidLiveness = t.isValidLiveness, livenessResult = t.livenessResult,status = t.process_status}
        -- end
        table.insert(new_list, v)
    end

    ret = {success = true, error_msg = "complete", data = new_list }
end

print(cjson.encode(ret))
