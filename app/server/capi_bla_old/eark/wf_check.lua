#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})
local common = require "common"

--init ok"Wq
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    local _json  = ngx.req.get_body_data()
    args   = cjson.decode(_json)
else
    args   = {} 
end


--run
local u = session.data
if (not u or not u.username) then 
	common.err("Session Expired!!")
end

local ret = {success = true , is_customized = false}

--1. Vajira Check
local vkey = 'vajira.users.' .. u.username
if(rc:get(vkey)=='true')then
   ret.is_customized = true
   ret.is_vajira     = true
end

-- Test
-- 1. login as vajira
-- 2. login as non vajira
-- 3. not logged in

print(cjson.encode(ret))
