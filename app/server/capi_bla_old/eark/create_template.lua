#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
bson = require "resty-mongol.bson"
common = require "common"
-- local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local res = {success = false}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

local template = [[
    {
        "pdfFile" : "1549439734",
        "imageMerge" : "pdf579040951549439723-0.png\npdf579040951549439723-1.png",
        "createdDtm" : "",
        "subject" : "Please sign ",
        "msg" : "",
        "signers" : [
            {
                "name" : "",
                "email" : "",
                "eKyc" : false,
                "usePassword" : false,
                "order_set" : "",
                "password" : "",
                "needToSign" : true
            }
        ],
        "originalFile" : "eSignature v2 (company).pdf",
        "imageFiles" : [
            {
                "aspectRatio" : 0.7067137809187279,
                "fileName" : "pdf579040951549439723-0.png"
            },
            {
                "aspectRatio" : 0.7067137809187279,
                "fileName" : "pdf579040951549439723-1.png"
            }
        ],
        "current_order" : 1,
        "is_template" : true,
        "company_id" : "5c580cf6778fdc6a5fd98b2e",
        "use_signing_order" : false,
        "fields" : [
            {
                "toolData" : "Signature",
                "toolName" : "Signature",
                "id" : 1549439753567,
                "page" : 0,
                "xp" : 42.816901408450704,
                "status" : 0,
                "email" : "",
                "time" : 0,
                "scale" : 100,
                "yp" : 37.120214547547704,
                "signOrder" : 1,
                "n" : 1
            }
        ],
        "status" : 0,
        "owner" : "kjunchean@gmail.com",
        "id" : "1549439734"
    }    
]]

template = cjson.decode(template)
template.createdDtm = bson.get_utc_date(os.time() * 1000)
ctx   = ctx or common.mongo_ctx()
col = ctx.col["edoc_list"]
i, err = col:insert({template})

if i then 
    res.success = true
else
    res.error_msg = err
end

print(cjson.encode(res))