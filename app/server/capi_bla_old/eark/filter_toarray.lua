#!/usr/bin/env lua
--[[ combine multiple lines of json strings in to a single Array]]

local cjson = require 'cjson'
local t = {'['}

local i=1
for l in io.lines() do
   if(i>1)then table.insert(t, ',') end
   i = i+1
   table.insert(t, l)
end


table.insert(t, ']')
print(table.concat(t))
