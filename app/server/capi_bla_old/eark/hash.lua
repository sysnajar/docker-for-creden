scrypt = require "resty.scrypt"
local opt = {
secret = "1234",
keysize = 32,
n = 32768,
r = 8,
p = 1,
saltsize = 32
}

local h1 = scrypt.crypt(opt)
local h2 = scrypt.crypt(opt)
local h3 = scrypt.crypt(opt.secret)
print = ngx.say
print(h1)
print("<br/>")
print(h2)
print("<br/>")
print("<b>"..h3.."</b>")
print("<br/>")
print("<b>CHECK1: ".. tostring(scrypt.check(opt.secret, h1))  .."</b><br/>")
print("<b>CHECK2: ".. tostring(scrypt.check(opt.secret, h2))  .."</b><br/>")
print("<b>CHECK3: ".. tostring(scrypt.check(opt.secret, h3))  .."</b><br/>")

--local b1 = scrypt.check("My Secret", scrypt.crypt "My Secret") -- returns true
--local b2 = scrypt.check("My Secret", scrypt.crypt "No Secret") -- returns false
--print(b1)
--print(b2)
