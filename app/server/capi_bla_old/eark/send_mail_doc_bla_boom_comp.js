var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
  var bg = "#1667B2"
  var wTable = "100%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"
  var msg = process.argv[3]
  var private_mail = ''
  var list_cc = process.argv[4]
  var doc_id = process.argv[5]
  var owner_email = process.argv[6]
  var owner_name = process.argv[7]
  var subject = process.argv[8]
  var link = process.env.HOST_NAME + "/bla/#/document?id=" + doc_id + "&signer=" + process.argv[2]
  var secret_level = process.argv[9] || 1
  var secret_text = ""


  var attachments = [{filename:'attachment.pdf', path:'/home/creden/work/app/client/face/images/card/'+doc_id+'_original.pdf'}]
  //var attachments = [{filename:'attachment.pdf', path:'/home/creden/work/app/client/face/images/card/'+doc_id+'-stamp.pdf'}]




  if (secret_level == 4) { secret_text = "ความลับที่สุด (Top Secret)" }
  else if (secret_level == 3) { secret_text = "ความลับมาก (Secret)" }
  else if (secret_level == 2) { secret_text = "ความลับ (Confidential)" }


  var html2 = ``
  if (private_mail != '') {
    html2 = `<div style="color:#2F80ED">
          <b>Private Message/ข้อความส่วนตัว:</b>
          <br>
          "`+ private_mail + `"
        </div>
         <br><br>`
  } else {
    html2 = ``
  }

  var html_secret = ``
  if (secret_level > 1) {
    html2 = `<div style="font-size:10px;">
          <b>Document Classification/ระดับความลับของเอกสาร :</b>
          "`+ secret_text + `"
        </div>
          <br>`
  } else {
    html_secret = ``
  }

  var html = `<!doctype html>

  <html lang="th-TH" dir="ltr">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
      integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
      <link rel="icon" type="image/x-icon" href="images/Creden_logos_ICON_2-Tones.ico">
    </head>
    <body style="background: #E0E0E0;padding-bottom: 30px;"><br>
      <div style="background-color: #0058A9;height: 350px; float: left;"></div>
        <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 30px 50px 30px;width: 35%;border-radius: 30px;">
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="https://positioningmag.com/wp-content/uploads/2018/08/bankok.jpg" alt="logo website" style="width: 300px;margin-top: 30px;">
            </td>
        </tr>
        <tr>
          <td style="width: 100%;color: #000;padding: 0px 30px 30px 30px;margin-top: 30px;">
            <div style="font-size: 16px;">
              <label style="font-weight: bold;">Document ID/รหัสเอกสาร :</label>  ${doc_id}
            </div>
            <div style="margin-top:15px;font-size: 16px;">
              <label style="font-weight: bold;">From/จาก :</label> ${owner_name} ( ${owner_email} )
            </div>
            <div style="margin-top:10px;font-size: 16px;">
              <label style="font-weight: bold;">Message/ข้อความ :</label> ${msg}
            </div>
            ${html2}
            ${html_secret}
            </td>
        </tr>
        <tr>
          <td style="display: flex;justify-content: center;">
              <div style="background: #0058A9;width: 100%;display: block;padding-bottom: 170px;">
                  <div style="text-align: center;margin-top: 100px ;color: white; ">
                  <!-- <img src="https://e7.pngegg.com/pngimages/988/926/png-clipart-computer-icons-business-computer-software-check-mark-check-in-icon-blue-angle.png" style="margin-top: 30px;"> -->
                  <i class="fas fa-check-circle fa-9x"></i>
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    Document has been completed by all participants
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    เอกสารถูกลงนามเสร็จสิ้นแล้ว
                  </div>
                  <!-- <div style="text-align: center;margin-top: 50px">
                    <a href="`+ link + `" style="text-decoration: none;color: #000;background: #FFF;padding: 15px 30px;font-size: 16px;">
                    คลิกเพื่ออ่านเอกสาร
                  </a>
                  </div> -->
              </div>
          </td>
        </tr>
        </table>
        </body>
        </html>`

  var smtp = {
    // host: 'smtp.bla.co.th', //set to your host name or ip
    // port: 25, //25, 465, 587 depend on your 
    // auth: {
    //   user: 'electronic_sign@bangkoklife.com',
    //   // user account
    //   // pass: 'creden2018' 
    //   // user password
    //  // pass: 'dnkrakkaxnmnakpr'
    // }
    host: 'smtp.mailgun.org', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      // user: 'credenmailer@gmail.com', //user account
      // // pass: 'creden2018' //user password
      // pass: 'dnkrakkaxnmnakpr'
      user: 'postmaster@mg.creden.co', //user account
      pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
//   var smtpTransport = mailer.createTransport(smtp);
//   var mail = {
//     from: 'Bangkok Life - eSinature', //from email (option)
//     to: process.argv[2], //to email (require)
//     cc: '',
//     subject: subject, //subject
//     html: html//head + body + end
//   }

var smtpTransport = mailer.createTransport(smtp);
var mail = {
  from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
  to: process.argv[2], //to email (require)
  cc: list_cc,//'chaluemchat@gmail.com,sysnajar@gmail.com'
  subject: 'Please Sign:' + subject, //subject
  html: html,//head + body + end
  attachments: attachments
}
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


