curl -v -X POST https://api.line.me/v2/bot/message/push \
-H 'Content-Type:application/json' \
-H 'Authorization: Bearer indARxinViDCBMjmEtyNOtlCBM1WWotI4MtHM3mC8Xqpaz1C7yO+piU5GlUhEbvtVqFZ6bI90lASeh+XpCVDurLrnCxjkQL0apDdS47qG6zYpiXxa0LOGiSJ7mt9fAb00uLzdGQKZn2vPsIhWcgPmAdB04t89/1O/w1cDnyilFU=' \
-d '{
  "to": "'$1'",
  "messages": [
    {
       "type": "template",
  "altText": "this is a carousel template",
  "template": {
    "type": "carousel",
    "actions": [],
    "columns": [
      {
        "thumbnailImageUrl": "https://creden.co/images/Creden_logos_Logo_C.png",
        "title": "ชื่อ",
        "text": "ข้อความ",
        "actions": [
          {
            "type": "message",
            "label": "การดำเนินการ 1",
            "text": "การดำเนินการ 1"
          },
          {
            "type": "message",
            "label": "การดำเนินการ 2",
            "text": "การดำเนินการ 2"
          }
        ]
      },
      {
        "thumbnailImageUrl": "https://creden.co/images/Creden_logos_Logo_C.png",
        "title": "ชื่อ",
        "text": "ข้อความ",
        "actions": [
          {
            "type": "message",
            "label": "การดำเนินการ 1",
            "text": "การดำเนินการ 1"
          },
          {
            "type": "message",
            "label": "การดำเนินการ 2",
            "text": "การดำเนินการ 2"
          }
        ]
      }
    ]
  }

    }
  ]
}'