#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

local load_h = prog('lua', '/home/creden/work/app/server/capi/h_name.lua')
local HOST_NAME = load_h.stdout
HOST_NAME = HOST_NAME:gsub("%\n", "")

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {filename = arg[1],filenamett = arg[5]} 
end

function trim1(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end
-- os.execute('rm /home/creden/work/app/client/face/images/vdo/' .. body.filename ..'.wav')
print(cjson.encode(body))
local transcript = ''
local res, err = prog('sh', '/home/creden/work/app/server/capi/speech_api.sh', body.filename )

-- ngx.log(ngx.NOTICE, 'speech speech_api '..body.filename )

if not err then
   local speech = cjson.decode(res.stdout)
   for i,v in pairs(speech.results or {}) do
       for j,k in pairs(v.alternatives) do
   		transcript = transcript .. ' ' .. k.transcript
   	end
   end
end

line_concat = ''
point = 0
local res, err = prog('cat', '/home/creden/work/app/client/vdo/ex/'..body.filenamett..'.vtt')

-- ngx.log(ngx.NOTICE, 'speech speech_api cat vtt'..body.filename )

if not err then
   	str = res.stdout
   	lines = {}
	for s in str:gmatch("[^\r\n]+") do
	    table.insert(lines, s)
	end
	for i,v in ipairs(lines) do
		if math.mod(i,2) ~= 0 then 
			if string.find(string.lower(transcript), string.lower(trim1(v))) then
				point = point + 1
			end
			line_concat = line_concat .. v
		end
	end
end

local update_res = {success = true, username = arg[3], txid = arg[2], score = point, maxScore = arg[4], type = "audio"}
local update_cmd = "lua /home/creden/work/app/server/capi/demoupdate_res_ch.lua '"..cjson.encode(update_res).."' &"
print(cjson.encode(update_res))
-- local update_cmd = "lua /home/creden/work/app/server/capi/update_res.lua '"..cjson.encode(update_res).."' &"
os.execute(update_cmd)
