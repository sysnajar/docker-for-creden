#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local ret = {success = false}
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_comment")
    
    db2 = conn:new_db_handle("dopa")
    col2 = db2:get_col("esig_user")
    if session.data.username then 
        local comment, err_commment = col:find({doc_id = body.doc_id})
        local get_table = {}
        if comment then
            for k,v in comment:pairs() do 
                v._id = tostring(v._id)

                user_data = col2:find_one({adminEmail = v.sender})
                if user_data.profile_pic then
                    v.sender_name = user_data.adminFisrtname.." "..user_data.adminLastname
                    if user_data.profile_pic ~= "" then
                        v.img_profile = user_data.profile_pic
                    else
                        v.img_profile = "" 
                    end
                else
                    v.img_profile = "" 
                end

                table.insert(get_table, v) 
            end
            if #get_table > 0 then
                ret.success = true
                ret.data = get_table
            else
                ret.success = false
                ret.msg = 'no comment'
            end
            
        else
            ret.success = false
            ret.msg = 'not found'
    
        end
    else
        ret = {success = false, error_msg = err, msg = "no session" }
    end

end

print(cjson.encode(ret))