#!/usr/bin/env lua
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

txid = arg[2]
key  = txid .. '.tensorflow.result'
cmd  = "/home/creden/work/app/server/capi/liveness_tensorflow.sh " .. arg[1] .. ' ' .. txid
os.execute(cmd)

json = rc:get(key)
print('json from ', key , json)
