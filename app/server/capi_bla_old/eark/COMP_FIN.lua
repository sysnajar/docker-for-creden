#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson.safe'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end
    -- id = kjunchean@gmail.com:kbank
    --{"EBANK_sumcashin6mth":353871.14,"EBANK_finBalance":2785.49,"EBANK_sumcashout6mth":351335.75,"EBANK_iniBalance":51.1}
    local fn =  function(id)
        local key = 'TRAN:FIN:' .. id
        local t = cjson.decode(rc:get(key))
        local score = 0 --total = 600

		if(t==nil) then
           return 0
		end

        if t.EBANK_finBalance > 0 then
            score = score + 200
        end

        if t.EBANK_finBalance > 200000 and t.EBANK_finBalance <= 500000 then 
            score = score + 50
        elseif t.EBANK_finBalance > 500000 and t.EBANK_finBalance <= 1000000 then 
            score = score + 100
        elseif t.EBANK_finBalance > 1000000 then 
            score = score + 200
        end

        if t.capital/t.cashflow >= 3 and t.capital/t.cashflow < 5 then
            score = score + 50
        elseif t.capital/t.cashflow >= 5 and t.capital/t.cashflow < 10 then
            score = score + 100
        elseif t.capital/t.cashflow > 10 then
            score = score + 200
        end

        return score
    end
    
    
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
    
