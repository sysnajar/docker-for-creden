#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local redis = require 'redis'
local common = require "common"
local object_id = require "resty-mongol.object_id"

rc = redis.connect('127.0.0.1', 6379)
local body = nil

---------------------COMMON FUNCTIONS----------------------------------
function parse_csv(str)
t = {}
for line in str:gmatch("[^\r\n]+") do
	local r = {}
	for col in line:gmatch("[^,]+") do
		table.insert(r, col)
    end  
    table.insert(t, r)
end
return t
end

function concat_table(t1, t2)
 for _,v in ipairs(t2)do
	table.insert(t1, v)
 end
 return t1
end

function thai_date()
	return os.date('%d/%m/', (os.time()+(60*60*7))  ).. (tonumber(os.date('%Y'))+543) 
end
---------------------MONGODB FUNCTIONS----------------------------------
--return new mongodb context
function mongo_ctx()
    local ret = {}
	local conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
	--	assert(false)
    else
		ret.dopa = conn:new_db_handle("dopa")
		ret.edoc = conn:new_db_handle("edoc")
		local col = {}
		local mt  = {}

		-- if k = xxx.yyy then col_name = yyy 
		-- if k = xxx     then col_name= xxx
		mt = {__index = function(t, k)  
			local db = (string.find(k, 'dopa.')==1) and ret.dopa or ret.edoc
			local i = string.find(k, '%.')
			local col_name = (i==nil) and k or (k:sub(i+1))
			return db:get_col(col_name)
		    end
		    }
	     
		 setmetatable(col, mt)
		 ret.col = col
		 ret.by_id = function(col_name, _id)	 
		 return by_id( ret.col[col_name], _id ) --call global function  by_id()
		 end
     end

    return ret		 
end

function convertHexStringToNormal( str )
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end

function by_id(col, _id)
  if(type(_id == string)) then
     _id = object_id.new(convertHexStringToNormal(_id))
  end

  local res = col:find_one({_id = _id})
  return res
end



--------------------SIGNATURE FUNCTIONS----------------------------------
function gen_new_doc_id()
  return  tostring(os.time()) 
end

function create_edoc_from_template(ctx, _tid , doc)
local tpl = by_id(ctx.col['dopa.edoc_template'], _tid)

ngx.log(ngx.NOTICE, "Copy create_edoc_from_template : " .. tostring(tpl))

for k,v in pairs(tpl) do
   if k~='_id' and doc[k] == nil then
       doc[k] = tpl[k]  
       ngx.log(ngx.NOTICE, "+++++++++++++ Copy attribute : " .. k)
   end
end


-- todo : copy PDF file from template to new doc
local dir = '/home/creden/work/app/client/face/images/card/'
local f1  = dir .. tpl.id .. '.pdf'
local f2  = dir .. doc.id .. '.pdf'
local cmd = 'cp ' .. f1 .. ' ' .. f2
ngx.log(ngx.NOTICE,"Copy pdf file from document template .. \n" .. cmd) ; os.execute(cmd)

end

--------------------GLOBLEX FUNCTIONS----------------------------------
local globlex_id = "5c063d9f86cff27897d5eeb5"
local globlex_template_id = "5c07b6b29a6817a8bf8a5ea9"

local globlex_position_csv = [[1,1,75,102,9.375,18.05309735,VGAP,7,width,800,height,565
1,2,75,109,9.375,19.2920354,,,width,800,height,565
1,3,75,116,9.375,20.53097345,,,width,800,height,565
1,4,75,123,9.375,21.7699115,,,width,800,height,565
2,1,75,149,9.375,26.37168142,VGAP,7,width,800,height,565
2,2,75,156,9.375,27.61061947,,,width,800,height,565
2,3,75,163,9.375,28.84955752,,,width,800,height,565
2,4,75,170,9.375,30.08849558,,,width,800,height,565
3,1,75,190,9.375,33.62831858,VGAP,7,width,800,height,565
3,2,75,197,9.375,34.86725664,,,width,800,height,565
3,3,75,204,9.375,36.10619469,,,width,800,height,565
3,4,75,211,9.375,36.34513274,,,width,800,height,565
4,1,75,229,9.375,40.53097345,VGAP,7,width,800,height,565
4,2,75,236,9.375,41.7699115,,,width,800,height,565
4,3,75,243,9.375,43.00884956,,,width,800,height,565
4,4,75,250,9.375,44.24778761,,,width,800,height,565
5,1,75,271,9.375,47.96460177,VGAP,7,width,800,height,565
5,2,75,278,9.375,49.20353982,,,width,800,height,565
5,3,75,285,9.375,50.44247788,,,width,800,height,565
5,4,75,292,9.375,51.68141593,,,width,800,height,565
6,1,75,308,9.375,54.51327434,VGAP,7,width,800,height,565
6,2,75,315,9.375,55.75221239,,,width,800,height,565
6,3,75,322,9.375,56.99115044,,,width,800,height,565
6,4,75,329,9.375,58.2300885,,,width,800,height,565
7,1,75,398,9.375,70.44247788,VGAP,7,width,800,height,565
7,2,75,405,9.375,71.68141593,,,width,800,height,565
7,3,75,412,9.375,72.92035398,,,width,800,height,565
7,4,75,419,9.375,73.15929204,,,width,800,height,565
8,1,75,437,9.375,77.34513274,VGAP,7,width,800,height,565
8,2,75,444,9.375,78.5840708,,,width,800,height,565
8,3,75,451,9.375,79.82300885,,,width,800,height,565
8,4,75,458,9.375,80.0619469,,,width,800,height,565
9,1,75,479,9.375,84.77876106,VGAP,7,width,800,height,565
9,2,75,486,9.375,86.01769912,,,width,800,height,565
9,3,75,493,9.375,87.25663717,,,width,800,height,565
9,4,75,500,9.375,87.49557522,,,width,800,height,565
10,1,463,59,57.875,10.44247788,VGAP,7,width,800,height,565
10,2,463,66,57.875,11.68141593,,,width,800,height,565
10,3,463,73,57.875,12.92035398,,,width,800,height,565
10,4,463,80,57.875,14.15929204,,,width,800,height,565
11,1,463,119,57.875,19.0619469,,,width,800,height,565
11,2,506,119,63.25,19.0619469,,,width,800,height,565
12,1,463,149,57.875,25.37168142,,,width,800,height,565
12,2,506,149,63.25,25.37168142,,,width,800,height,565]]


function globlex_suitability_positions(str)
local t = {}
local suit_page = 3

for i,r in ipairs(parse_csv(str)) do
   local key   = r[1] .. '_' .. r[2]
        t[key] = {xp = tonumber(r[5]), yp = tonumber(r[6]), page = suit_page}
end

return t
end


function suit_to_fields(form)
local t = {}
local question_cnt = 12
local pos_map = globlex_suitability_positions(globlex_position_csv) -- {key, xp, yp, page}

local total_scores = 0
for i=1,question_cnt do
   local ans = tonumber(form['c'..i])
   if(ans)then
      local pos = pos_map[i..'_'..ans] --get position
	  assert(pos, "Unnable to find position of Q: " .. i .. ',A: '..ans)
      
	  if(i<11)then total_scores = total_scores + ans end --add up to total_scores

	  table.insert(t, {q = i, toolName = "Check" , toolData = "X" ,
	  status = 1,
	  scale = 100, signOrder = 1, type = "", n = 0 ,
	  page = pos.page, xp = pos.xp, yp = pos.yp
	  })
   end --if ans	    
end
table.insert(t,	{toolData=tostring(total_scores), page = t[1].page, xp = 82.25, yp = 59.884, 
toolName="Text",status=1,scale=100,n=0,signOrder=1
})

return t
end

function globlex_personal_fields(form)
local page, w, h = 1, 840, 593

local address  = string.gsub(form.address , '\n' , ' ')
local fullname = (form.firstName or '') .. ' ' ..  (form.lastName or '')
local t = {
	{toolData=fullname, page = page, x = 85, y = 363, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=form.cardID, page = page, x = 88, y = 438, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=form.phone, page = page, x = 515, y = 166, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=form.email, page = page, x = 515, y = 181, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=address, page = page, x = 485, y = 314, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },



	{toolData=fullname, page = 5, pageN = 9, x = 112, y = 103.9035, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },

	{toolData=fullname, page = 11, pageN = 21, x = 96, y = 130, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },

	{toolData=form.email, page = 11, pageN = 21, x = 96, y = 140, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },

	{toolData=form.phone, page = 11, pageN = 21, x = 280, y = 140, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=thai_date(), page = 11, pageN = 21, x = 285, y = 103.9, 
	 toolName="Date",status=1,scale=100,n=0,signOrder=1
    },

	{toolData=fullname ,page = 12, pageN = 23, x = 143.4, y = 150.075, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=thai_date(), page = 12, pageN = 23, x = 261, y = 125.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=fullname ,page = 12, pageN = 24, x = 537, y = 150.075, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=thai_date(), page = 12, pageN = 24, x = 670, y = 125.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },

	{toolData=fullname ,page = 13, pageN = 25, x = 195.4, y = 155.075, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=thai_date(), page = 13, pageN = 25, x = 280, y = 102.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },


	{toolData=fullname ,page = 14, pageN = 27, x = 130.4, y = 71.075, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=address, page = 14, pageN = 27, x = 115, y = 110.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=form.phone, page = 14, pageN = 27, x = 305, y = 110.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },

	 {toolData=form.txtNumTax_1, page = 4, pageN = 8, x = 680, y = 98.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
	{toolData=form.txtNumTax_2, page = 4, pageN = 8, x = 680, y = 131.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
    {toolData=form.txtNumTax_3, page = 4, pageN = 8, x = 680, y = 180.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
    {toolData=form.txtGreenCard, page = 4, pageN = 8, x = 680, y = 141.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
    {toolData=form.txtAcc, page = 4, pageN = 8, x = 680, y = 221.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
     {toolData=form.txtBank_1, page = 4, pageN = 8, x = 700, y = 233.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
      {toolData=form.txtBank_2, page = 4, pageN = 8, x = 680, y = 260.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
         {toolData=form.txtfullnamex, page = 4, pageN = 8, x = 702, y = 288.45, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1
    },
 



}

local title_map = {
  ['MR.'] = {toolData="X", page = page, x = 72, y = 329, 
	 toolName="Check",status=1,scale=100,n=0,signOrder=1},
  ['MRS.'] = {toolData="X", page = page, x = 124, y = 329, 
	 toolName="Check",status=1,scale=100,n=0,signOrder=1},
  ['MISS.'] = {toolData="X", page = page, x = 175, y = 329, 
	 toolName="Check",status=1,scale=100,n=0,signOrder=1},

  ['OTHERS'] = {toolData="X", page = page, x = 235, y = 329, 
	 toolName="Check",status=1,scale=100,n=0,signOrder=1},
	
  ['OTHERS2'] = {toolData= form.title or '', page = page, x = 270, y = 320, 
	 toolName="Text",status=1,scale=100,n=0,signOrder=1}
 }

 if title_map[form.title] then
    table.insert(t, title_map[form.title])
    --[[table.insert(t, title_map['MRS.'])
    table.insert(t, title_map['MISS.'])
    table.insert(t, title_map['OTHERS'])
    table.insert(t, title_map['OTHERS2'])
	--]]
 else
    table.insert(t, title_map['OTHERS'])
    table.insert(t, title_map['OTHERS2'])
 end




--compute xp, yp
for _ ,r in ipairs(t) do
	r.xp, r.yp = (100*r.x)/w , (100*r.y)/h
	r.x, r.y = nil, nil
end



return t
end

function globlex_signature_fields(form)

--[[1,Signature,4,155,460,8,19.375,75.4159292,width,800,height,565,100,0
1,Text,4,155,468,8,19.375,82.83185841,width,800,height,565,100,1
2,Signature,4,557,472,8,69.625,77.53982301,width,800,height,565,100,0
2,Text,4,557,480,8,69.625,84.95575221,width,800,height,565,100,1]]

local csv = [[
1,Signature,4,155,460,8,19.375,81.4159292,width,800,height,565,50,0
1,Text,4,155,468,8,19.375,82.83185841,width,800,height,565,100,1
2,Signature,4,557,472,8,69.625,83.53982301,width,800,height,565,50,0
2,Text,4,557,480,8,69.625,84.95575221,width,800,height,565,100,1
3,Signature,10,492,318,8,61.5,56.28318584,width,800,height,565,50,0
3,Text,10,557,472,8,61.61,57.53982301,width,800,height,565,50,1
4,Signature,11,568,458,8,71,81.0619469,width,800,height,565,50,0
4,Text,11,568,466,8,71,82.47787611,width,800,height,565,100,1
5,Signature,12,160,315,8,20,55.75221239,width,800,height,565,50,0
5,Text,12,160,323,8,20,57.16814159,width,800,height,565,100,1
6,Signature,12,550,328,8,68.75,58.05309735,width,800,height,565,50,0
6,Text,12,550,336,8,68.75,59.46902655,width,800,height,565,100,1
7,Signature,13,194,266,8,24.25,47.07964602,width,800,height,565,50,0
7,Text,13,194,274,8,24.25,48.49557522,width,800,height,565,100,1
8,Signature,14,219,365,8,27.375,64.60176991,width,800,height,565,50,0
8,Text,14,219,373,8,27.375,66.01769912,width,800,height,565,100,1
9,Signature,15,220,198,8,27.5,35.04424779,width,800,height,565,50,0
10,Signature,15,220,222,8,27.5,39.2920354,width,800,height,565,50,0
11,Signature,15,220,447,8,27.5,79.11504425,width,800,height,565,50,0
12,Signature,15,220,470,8,27.5,83.18584071,width,800,height,565,50,0
9,Text,15,117,91,8,14.625,14.50619469,width,800,height,565,100,1
10,Text,15,88,197,8,11,34.86725664,width,800,height,565,100,1
11,Text,15,115,340,8,14.375,59.17699115,width,800,height,565,100,1
12,Text,15,88,440,8,11,77.87610619,width,800,height,565,100,1
1,Date,15,268,73,8,33.5,10.92035398,width,800,height,565,100,1
2,Date,15,268,323,8,33.5,55.39814159,width,800,height,565,100,1
]]

local t = {}
local dataMap = {Date =  thai_date() , Text = form.fullname } --value of each ToolName
for i,r in ipairs(parse_csv(csv)) do
	    if(r[2]=='Signature') then 
		    r[8]  = r[8]-6
		    r[13] = 100 
		end

        local f = {xp = tonumber(r[7]), yp = tonumber(r[8]), page = tonumber(r[3]),
         toolData = (dataMap[r[2]] or ''),scale = tonumber(r[13]), toolName = r[2], status = tonumber(r[14]), signOrder=1
        }
	
	  table.insert(t,f)	
end

return t
end

function new_globlex_doc(form_id)
local id = gen_new_doc_id()
local signer_email = id .. '.globlex@customer.creden.co'
--local form = ctx.by_id('dopa.securities_form', form_id)
local form = ctx.col['dopa.securities_form']:find_one({id = form_id})
assert(form~=nil, 'form#'.. form_id ..' is nil')
form.signer_email = signer_email
form.fullname = ((form.firstName or '') .. ' ' ..  (form. lastName or ''))

local suit_fields = suit_to_fields(form)
local per_fields = globlex_personal_fields(form)
local all_fields = concat_table(concat_table({},suit_fields), per_fields)
all_fields = concat_table(all_fields, globlex_signature_fields(form))

--assign id, n , time
local tmp_time = ngx.now() * 1000
for i,v in ipairs(all_fields) do 
	v.id = tmp_time + i
	v.n = i  
	v.email = signer_email
	v.time = "09/01/19 00:00:00"
end


local usa_fields = 
{
  y1 = {xp = 76, yp = 15},	
  n1 = {xp = 82, yp = 15},	
  y2 = {xp = 76, yp = 21},	
  n2 = {xp = 82, yp = 21},
  y3 = {xp = 76, yp = 29},	
  n3 = {xp = 82, yp = 29},	
  y4 = {xp = 76, yp = 36},	
  n4 = {xp = 82, yp = 36},	
  y5 = {xp = 76, yp = 42},	
  n5 = {xp = 82, yp = 42},	
  y6 = {xp = 76, yp = 47},	
  n6 = {xp = 82, yp = 47}
}

for i = 1 , 6 do 
   local k = (form.config_1=='1' and 'n' or 'y') .. i
   local tmp = {toolName = "Check", toolData = "X", status = 1, scale = 100, signOrder = 1 , type="", n = #all_fields, page = 4}
   tmp.xp = usa_fields[k].xp 
   tmp.yp = usa_fields[k].yp 

   table.insert(all_fields, tmp)
end

      --[[
	  table.insert(t, {q = i, toolName = "Check" , toolData = "X" ,
	  status = 1,
	  scale = 100, signOrder = 1, type = "", n = 0 ,
	  page = pos.page, xp = pos.xp, yp = pos.yp
	  })]]


local doc = { id = id, pdfFile = id,
is_globlex = true,
form_id = form_id,
createdDtm = bson.get_utc_date(ngx.now() * 1000), 
owner_name = "Globlex securities Co.,Ltd",
owner = "globlex@customer.creden.co",
subject = "Gloglex Application Form ", msg = "Please sign",
originalFile = "Globlex-Book 2561 P1-15_1_RE.pdf",
signers = {
	{needToSign=true,  order_set = "", eKyc = false , usePassword = false, password = "", title ="", name = (form.firstName or 'N/A'),email = signer_email}
    },
	fields = all_fields
} -- doc

create_edoc_from_template(ctx, globlex_template_id , doc)

return doc, form
end

-------------------------------------------------------
-- MOU FN

function create_mou_doc()
	local ret = {}
	for i=1,4 do
		local id = '35781527111'.. tostring(i) 
		local owner = id .. '.mou@customer.creden.co'
		 local t = {id = id, owner = owner , subject  = ('MOU document '..id), pdfFile = id, 
		 imageFiles = {{fileName='pdf'..id..'.png', aspectRatio = 0.70671378091873}},
		 signers    = {{name="signer_name", title="", email= owner, needToSign= true, usePassword=false, eKyc=false}},
		 originalFile = "MOU with Creden.co", msg = "", owner_name = "Creden.co",
		 createdDtm = bson.get_utc_date(ngx.now() * 1000),
		 current_order = 1, use_signing_order = false, status = 0,
		 fields = {{ id = tonumber(id)+1, toolName="Signature" , toolData="",
	       n  = 1,
		   xp = 17.246376811594,
		   yp = 75.280249877083,
		   status = 0,
		   page = 0,
		   email = owner,
		   signOrder = 1,
		   time  = '21/01/19 00:00:00',
		   scale = 100
	       }
	     }
		}
		 if(i==4)then t.fields[1].xp = 59.565217391304 end
		 table.insert(ret, t)
    end
         i, err = ctx.col['edoc_list']:insert(ret, nil, true)
		 --print('inserted '.. tostring(i) ..' documents.'..tostring(err)..'\n')

end

-- END MOU FN
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
end

	ctx = mongo_ctx()	
	local args, err = ngx.req.get_uri_args()
	local mou_docs = {}
	mou_docs['357815271111']=true;
	mou_docs['357815271112']=true;
	mou_docs['357815271113']=true;
	mou_docs['357815271114']=true;
	mou_docs['357815271115']=true;

    local is_mou = mou_docs[args.id] or false
  

	if (is_mou) then
		local mou_id = args.id
        -- print('is_mou'..tostring(is_mou)..', id = '..mou_id)    
	    --create_mou_doc()	
        doc, err = ctx.col.edoc_list:find_one({id=mou_id})
		if(not doc) then 
			doc={};
			err = "document not found!!!" 
		 end
		--send to client
		doc.time = '15/01/19' --todo: replace this
		doc._id = nil
		doc.status = 0; doc.fields[1].status = 0
		print(cjson.encode({success=(err==nil), errMsg=err, data = doc, id=doc.id, is_mou = is_mou}))
   else
		local doc, form = new_globlex_doc(args.id)
		i, err = ctx.col.edoc_list:insert({doc}, nil, true)
		
		--send to client
		doc.time = '09/01/19' --todo: replace this
		doc.owner_name = form.fullname
		doc._id = nil
		doc._company = nil

		print(cjson.encode({success=(err==nil), errMsg=err, data = doc, id=doc.id}))
   end
