#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
local redis = require 'redis'
common = require "common"
local session = require "resty.session".open({secret = "4321"})
rc = redis.connect('127.0.0.1', 6379)
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {} 
end
    -- data = [[
    -- {
    --     "pdfFile":"test.pdf",
    --     "imageFiles":[{"fileName":"page1.png"},{"fileName":"page2.png"}],
    --     "signers":[{"email":"nutp10.1@gmail.com"},{"email":"kjunchean@gmail.com"}],
    --     "id":"1",
    --     "status":"0",
    --     "fields":[
    --         {"id":"1","email":"nutp10.1@gmail.com","x":"10","y":"10","status":"0","page":"1","time":"xx"},
    --         {"id":"2","email":"kjunchean@gmail.com","x":"20","y":"20","status":"0","page":"1","time":"xx"}
    --     ]
    -- }
    -- ]]
    -- print(data)
    -- body = cjson.decode(data)

    function convertHexStringToNormal( str )
        return (str:gsub('..', function (cc)
        return string.char(tonumber(cc, 16))
        end))
    end

    function trim5(s)
        return s:match'^%s*(.*%S)' or ''
    end

    for i,s in ipairs(body.signers) do
        if s.email then
            local signers = trim5(s.email)
            body.signers[i].email = signers
        end
    end

    for i,f in ipairs(body.fields) do
        if f.email then
            local fields = trim5(f.email)
            body.fields[i].email = fields
        end
    end


    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        
        common.activity_history(body.id ,body.owner, 'Created', 'สร้างเอกสาร', 'The document was created '..body.owner, body.owner..' สร้างเอกสาร', 'Created', body.originalFile, body.subject, body.msg, nil, nil)
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        doc = body
        doc.company_id = session.data.company_id 
		doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 
        -- doc.company_id = object_id.new(convertHexStringToNormal(doc.company_id))
        -- doc.document_type_id = object_id.new(convertHexStringToNormal(doc.document_type_id))
        i, err = col:insert({doc}, nil, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            if tostring(doc.is_template) ~= 'true' then
                list_signers={}
                send_mail_list = {}
                for i2,v2 in ipairs(common.edoc_get_real_signers(doc)) do



                    if v2.password~='' then
                        send_mail = {signer = v2.name, to = v2.email , id = doc.id, subject = doc.subject, msg = doc.msg ,password=v2.password} 
                    else
                        send_mail = {signer = v2.name, to = v2.email , id = doc.id, subject = doc.subject, msg = doc.msg }
                    end
                    
                    send_mail.sender_name  = doc.owner_name or 'N/A'
                    send_mail.sender_email = doc.owner or 'N/A'

                    table.insert(send_mail_list,send_mail)
                    local n = {doc_id = doc.id,

                                filename = doc.originalFile, 
                                dtm = ngx.now()*1000,
                                signer = doc.owner,
                                email  = v2.email,
                                id     = ngx.now()*1000,
                                status = 0,
                                description = 1
                                } 
                                table.insert(list_signers, n)
                    common.activity_history(body.id ,body.owner, 'Sent Invitation', 'ส่งเอกสาร', body.owner..' invitation to '..v2.email, body.owner..' ส่งเอกสารให้ '..v2.email, 'Sent', body.originalFile, body.subject, body.msg, nil, nil)
                end

                if(#list_signers>0)then
                    ncol = db:get_col("notification")
                    _i, err = ncol:insert(list_signers, nil, true)
                end

                rc:set('list_send_mail',cjson.encode(send_mail_list))
                os.execute('lua /home/creden/work/app/server/capi/email_invite_sign.lua &')
                os.execute('lua /home/creden/work/app/server/capi/running_number.lua '..body.document_type_id..' '..body.id..' &')
            end
            ret = {success = true, error_msg = err}
        end
    end

print(cjson.encode(ret))
