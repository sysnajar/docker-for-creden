#!/usr/bin/env lua
local cjson = require 'cjson'
local body = nil
local redis = require 'redis'
local mongo = require "resty-mongol"
local HOST_NAME = os.getenv("HOST_NAME")
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
	body = ngx.req.get_post_args()
    print = ngx.say
else
   body  = {email = arg[1]}
end

        --update mongo
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        db = conn:new_db_handle("dopa")

        if body.login_service == 'esig' then
        	col = db:get_col("esig_user")
        	 --get existing record
	        old_rec = col:find_one({adminEmail=body.email}, {comStatus = 1})
			if(old_rec) then
				local rand  = tostring(math.random())
				rand = rand:sub(3,7) ..rand:sub(10,12)
				local key = 'resetpwd:' .. body.email 

				rc:set(key, rand)
				rc:expire(key, 60*30)

				local email = {to = body.email, subject = "Reset creden.co password.", key = 'email.reset' .. rand,
				 html = [[Here's the link to reset your password within 30 minutes:<br/>
				 <a href="]]..HOST_NAME..[[/signature/forgot.html?email=EMAIL&code=CODE">click here to reset password</a>]]
				 }

				 email.html = email.html:gsub("EMAIL", body.email):gsub("CODE", rand)

				rc:set(email.key, cjson.encode(email))
				rc:expire(email.key, 60*30)
				cmd = 'node /home/creden/work/app/server/capi/send_mail.js ' .. email.key
				os.execute(cmd)
			end
        else
        	col = db:get_col("company")
        	--get existing record
	        old_rec = col:find_one({adminEmail=body.email}, {comStatus = 1})
			if(old_rec) then
				local rand  = tostring(math.random())
				rand = rand:sub(3,7) ..rand:sub(10,12)
				local key = 'resetpwd:' .. body.email 

				rc:set(key, rand)
				rc:expire(key, 60*30)

				local email = {to = body.email, subject = "Reset creden.co password.", key = 'email.reset' .. rand,
				 html = [[Here's the link to reset your password within 30 minutes :<br/>
				 <a href="]]..HOST_NAME..[[/business/forgot.html?email=EMAIL&code=CODE">click here to reset password</a>]]
				 }

				 email.html = email.html:gsub("EMAIL", body.email):gsub("CODE", rand)

				rc:set(email.key, cjson.encode(email))
				rc:expire(email.key, 60*30)
				cmd = 'node /home/creden/work/app/server/capi/send_mail.js ' .. email.key
				os.execute(cmd)
			end
        end   
local ret = {success= true}
print(cjson.encode(ret))

