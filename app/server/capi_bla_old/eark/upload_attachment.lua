local upload = require "resty.upload"
local cjson = require "cjson"
local chunk_size = 4096 -- should be set to 4096 or 8192
local file

local path = '/home/creden/work/app/client/face/images/card/'
local _fileName = ''
local fileName = tostring(os.time())  
local pdfStr = ''
local imgStr = ''


local form, err = upload:new(chunk_size)
if not form then
  ngx.say "no form"
end

form:set_timeout(1000) -- 1 sec

local len = 0
local count = 0
while true do
    local typ, res, err = form:read()
    if not typ then
        ngx.say("failed to read: ", err)
        return
    end

    if(typ=='header')then
        local file_header = res[2]
        local i, j = string.find(file_header,"filename=")
        if j then
            local file_name = string.sub(file_header,j+2,string.len(file_header)-1):lower()
            if(file_name:find('.png') or file_name:find('.jpg') or file_name:find('.jpeg') ) then
                _fileName = 'img'..fileName..'_'..tostring(count)..'.png'
                imgStr = imgStr..path.._fileName..' '
            else
                _fileName = 'pdf'..fileName..'_'..tostring(count)..'.pdf'
                pdfStr = pdfStr..path.._fileName..' '
            end
            file = io.open(path.._fileName, "w+")
            count = count + 1
        end
    end

    if(typ=='body')then
    len = len + #res
    file:write(res)
    end
    --ngx.say("read1: ", cjson.encode({typ, res}))

    if typ == "eof" then
        file:close()
        break
    end
end

--convert img to pdf || merge pdf
if imgStr ~= '' and pdfStr ~= '' then
    img_cmd = 'convert '..path..'img'..fileName..'_*.png '..path..fileName..'.pdf'
    merge_cmd = 'cpdf -merge '..pdfStr..path..fileName..'.pdf -o '..path..fileName..'.pdf'
    os.execute(img_cmd)
    os.execute(merge_cmd)
elseif imgStr ~= '' then
    img_cmd = 'convert '..path..'img'..fileName..'_*.png '..path..fileName..'.pdf'
    os.execute(img_cmd)
else
    merge_cmd = 'cpdf -merge '..pdfStr..' -o '..path..fileName..'.pdf'
    os.execute(merge_cmd)
end

ngx.say (cjson.encode({success = (len>0), filename = fileName}))

