#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    body = ngx.req.get_uri_args()
else
   body  = {form_id = "Glo1546988279565",stamp = true} 
end

    file_name = nil
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = "connection error"}
    else
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        list = col:find({form_id = body.form_id})
		s = nil
		
		for _k, v in list:pairs() do
			s = v
		end


		if(s)then
                    file_name = s.id
		end
	end	

if (file_name and body.stamp and s.status==1) then
    file_name =  file_name..'-stamp'
end

if file_name then
        if(not ngx)then
   	  print('found ', 'file = ' .. file_name .. 'status = ' .. s.status )
	else
	  ngx.exec('/face/images/card/'..file_name..'.pdf')
	 --ngx.say("x")
        end 
else
    ngx.say(cjson.encode({error = "file not found"}))
end
