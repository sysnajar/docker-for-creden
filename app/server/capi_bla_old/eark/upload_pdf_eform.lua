local cjson = require "cjson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local prog1 = exec.new('/tmp/exec.sock')
local prog1b = exec.new('/tmp/exec.sock')
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson" 
local session = require "resty.session".open({secret = "4321"})
local body = nil

prog.timeout_fatal = false
prog1.timeout = 1000 * 60 * 30
prog1.timeout_fatal = false
prog1b.timeout_fatal = false

--return number of PDF pages or 1 if the file is image file (png,jpg)
function get_page_count(file)
local res, err = prog1('pdftk', file ,  'dump_data'  )
prog1.stdin = res.stdout
res,err = prog1('grep', 'NumberOfPages')
prog1.stdin = nil

if(not err)then
   return tonumber(res.stdout:match('%d+'))
else
   return 0	
end

end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { name_pdf = arg[1] }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
local eform = { success = false, error_msg = "N/A" }

if not ok then
    ret = { success = false , error_msg = "not connect db" }
else

    local myusername  = session.data.username
    if myusername == body.owner then
        local db = conn:new_db_handle("edoc")
        local col = db:get_col("eform");
        local e, err = col:find_one({ eform = body.name_pdf or "N/A", owner = body.owner or "N/A"})

        if e then
            e._id  = nil
            eform = e
            eform.success = true

            local file
            -- local name = rand(8) .. tostring(os.time()) 
            name = body.name_pdf
            local file_name  = '/home/creden/work/app/client/face/images/card/' .. name .. ".pdf"
            local file_name2 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. ".png"
            local file_name3 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. '*.png'

            redis = require 'redis'
            rc = redis.connect('127.0.0.1', 6379)

            file = io.open(file_name, "w+")



            ngx.log(ngx.NOTICE,"FINISHED CMD UPLOADING")


            local cmd1,size1,err1
            local page_count = get_page_count(file_name)
            local last_page_index = page_count-1
            ngx.log(ngx.NOTICE,"PDF PAGE_COUNT = " .. page_count)
            local page_convert_size = 10
            local start_page,end_page = 0,(page_count<page_convert_size) and last_page_index or page_convert_size-1
            local stop_now = false
            while(not stop_now)do
            
            cmd1 = 'convert -density 300 ' .. file_name .. '['..start_page..'-'..end_page..'] ' .. file_name2 ; ngx.log(ngx.NOTICE,"CMD1 = " .. cmd1)
            size2, err1 = prog1('convert','-density', '300' , file_name..'['.. start_page .. '-' .. end_page ..']'  , file_name2)
            ngx.log(ngx.NOTICE,"FINISHED CMD1")


            --exit loop if we did last page
            if(end_page == last_page_index) then 
                ngx.log(ngx.NOTICE, 'stopped conversion at ' .. end_page )
                stop_now = true
                break 
            else
                ngx.log(ngx.NOTICE, end_page..'~='..last_page_index)
            end

            -- start_page,end_page for next loop
            start_page = end_page+1
            end_page = start_page+page_convert_size-1
            if(end_page> last_page_index)then end_page = last_page_index end
            end

            --eark todo 
            if(false)then
            ngx.log(ngx.NOTICE,"START CMD1B")
            local size1b, err1b= prog1b('mogrify','-resize','800x',file_name3)
            ngx.log(ngx.NOTICE,"FINISHED CMD1B")
            else
            ngx.log(ngx.NOTICE,"SKIP CMD1B")
            --local size1b, err1b= prog1b('mogrify','-resize','800x',file_name3)
            ngx.log(ngx.NOTICE,"SKIPPEDED CMD1B (resizing)")
            end


            local cmd2 = 'find '.. file_name3  ..' -printf "%f\n" | sort -V | /home/creden/scripts/rset.lua pdf'
            os.execute(cmd2)
            ngx.log(ngx.NOTICE,"FINISHED CMD2")
            pdf_str = rc:get('pdf')

            local all_img = ''
            local image_merge = ''
            local page_num = 0
            --merge image
            for i in string.gmatch(pdf_str, "[^\n]+") do
                page_num = page_num + 1
                all_img = all_img..'/home/creden/work/app/client/face/images/card/'..i..' '
            end

            ngx.log(ngx.NOTICE,"START CMD3")
            if (false and page_num > 1) then 
                image_merge = 'pdf'..name..'.png'
                local cmd_merge = 'convert '..all_img..' -append /home/creden/work/app/client/face/images/card/'..image_merge 
                ngx.log(ngx.NOTICE,cmd_merge)
                os.execute(cmd_merge) 
            else
                image_merge = pdf_str
            end
            ngx.log(ngx.NOTICE,"FINISHED CMD4")
            ret = {success = true, filename = name .. '.pdf' , pdf_str = pdf_str, image_merge = image_merge, page_num = page_num, eform = eform}



        else
           ret =  { success = false ,eform = { success = false, error_msg = "1" } }
        end
    else
        ret =  { success = false ,eform = { success = false, error_msg = "2" } }
    end
    ngx.say(cjson.encode(ret))
end


