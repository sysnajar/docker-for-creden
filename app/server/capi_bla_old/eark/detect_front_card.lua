#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

local load_h = prog('lua', '/home/creden/work/app/server/capi/h_name.lua')
local HOST_NAME = load_h.stdout
HOST_NAME = HOST_NAME:gsub("%\n", "")
local t = {success = false}
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], txid = arg[2]} 
end

function trim5(s)
	  return s:match'^%s*(.*%S)' or ''
end

function replace_char(pos, str, r)
    return str:sub(1, pos-1) .. r .. str:sub(pos+1)
end

function dob_line(txt)
    local dob_line  = nil
    local i = 0
    local c = {}
    for line in string.gmatch(txt, '[^\r\n]+') do
        i = i + 1

        if(dob_line == nil and string.find(line, 'เกิดวันที่')~=nil) then
            dob_line = i + 1
        end
        
        if(dob_line == nil and (string.find(line, 'Date')~=nil or string.find(line, 'Birth')~= nil)) then
            dob_line = i
            break
        end

        if(dob_line == nil and string.find(line, 'ศาสนา')~=nil) then
            dob_line = i -1
            break
        end
        table.insert(c, line)
    end

    return c, dob_line
end

function province_line(txt)
    local dob_line  = nil
    local i = 0
    local c = {}
    for line in string.gmatch(txt, '[^\r\n]+') do
        i = i + 1
        
        if(dob_line == nil and (string.find(line, 'Date')~=nil or string.find(line, 'Birth')~= nil)) then
            dob_line = i+3
        end

        if(dob_line == nil and string.find(line, 'ศาสนา')~=nil) then
            if(trim5(line) == 'ศาสนา')then
                dob_line = i + 3
            else
                dob_line = i + 2
            end	
        end
        table.insert(c, line)
    end

    return c, dob_line
end

function nameTh(txt)
    want = false
    full_name = ''
    find_first = true
    for line in string.gmatch(txt, '[^\r\n]+') do 
        if find_first and ( string.find(line, 'ด.ช.') or string.find(line, 'นาย') or string.find(line, 'นางสาว') or string.find(line, 'นาง') or string.find(line, 'ชื่อตัวและชื่อสกุล') or string.find(line, 'น.ส.')) then
            want = true
            find_first = false
        end

        if want then
            if string.find(line,"[a-z]") then
                want = false
            end
            if want then
                full_name = full_name .. ' '..line
                break
            end
        end
    end
    line = full_name
  
    words = {}
    line = string.gsub(line,'ชื่อตัวและชื่อสกุล','')
    for word in string.gmatch(line,"%S+") do table.insert(words, word) end
        size_words = table.getn(words)
        t.name = words[size_words-1] or ''
        if string.find(t.name, 'นาย') then
            t.name = string.gsub(t.name,'นาย','')
        elseif string.find(t.name, 'นางสาว') then
            t.name = string.gsub(t.name,'นางสาว','')
        elseif string.find(t.name, 'น.ส.') then
            t.name = string.gsub(t.name,'น.ส.','')
        elseif string.find(t.name, 'นาง') then
            t.name = string.gsub(t.name,'นาง','')
        end
        t.lastname = words[size_words] or ''   
end

function downloadImgs()
    local cnt = tostring(rc:incr('detect.card.name')) .. 'front_card'
    local dir = '/home/creden/work/app/client/face/images/card/'
    local filename = cnt
    chk = string.sub(body.img, 1, 4)
    if string.match(chk, "http") then
        local cmd = 'wget '.. body.img ..' -O ' .. dir .. filename .. '.png'
        os.execute(cmd)
    else
        local tmp = 'tmp.kyc.img' .. filename   
        rc:set(tmp , body.img)
        local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
        os.execute(cmd)
        rc:del(tmp) 
    end
    return filename
end


name = downloadImgs()
local path = "/home/creden/work/app/client/face/images/card/"..name..".png";
local path_emp = "/home/creden/work/app/client/face/images/card/"..name.."_emp.png";
local path_out = "/home/creden/work/app/client/face/images/card/"..name.."_out.png";

body.is_mobile = tostring(body.is_mobile)

if body.is_mobile == 'true' then
   os.execute('cp ' .. path .. ' ' .. path ..'.png')
   os.execute('mogrify -rotate -90 ' .. path)
end

t_province = {}
local res, err = prog('cat', '/home/creden/work/app/server/capi/province.txt')
if not err then
  str = string.gsub(res.stdout, "%s+", "")
  str = '{'..str..'}'
  t_province = cjson.decode(str)
else
  print(err)
end

province = ''
t.sex = ''
local params = {img_url = HOST_NAME..'/face/images/card/' .. name ..'.png', img_name = name ..'.png'}
local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/ocr', '-d', cjson.encode(params))
if not err then
    ocr = cjson.decode(res.stdout)
    if ocr.responses[1].fullTextAnnotation then
        long_text = ocr.responses[1].fullTextAnnotation.text
        nameTh(long_text);
        long_text = string.gsub(long_text, "%s+", "")
        a = string.match(long_text, '%d%d%d%d%d%d%d%d%d%d%d%d%d')
    if a == nil then
        t.error_msg = "รูปไม่ถูกต้อง"
        print(cjson.encode(t))
        os.exit(0)
    else
        if string.sub(a, 1, 1) == "U" then
            a = replace_char(1,a,"J")
        elseif string.sub(a, 1, 1) == "V" then
            a = replace_char(1,a,"M")
        end

        if string.sub(a, 2, 2) == "0" then
            a = replace_char(2,a,"C")
        end
    end   

    local line_dob, _i = dob_line(ocr.responses[1].fullTextAnnotation.text)
    local dob_en = line_dob[_i]
    if dob_en then
      dob_en = string.sub(dob_en, string.find(dob_en, '%d'), #dob_en) 
    end

    local dd,mm,yy = '','',''
    local cnt = 0
    if dob_en then
        for ch in  string.gmatch(dob_en,"%S+") do
            cnt = cnt+1
  			if(cnt==1)then dd = ch end
  			if(cnt==2)then mm = ch end
  			if(cnt==3)then yy = ch end
  		end
    end

    yy = tonumber(yy)
    t.dob = {
        day   = ((string.len(dd) == 1) and '0'..dd or dd),
        mon  = (string.gsub(mm,'%.','')) or '',
        year = (yy) and tostring(yy + 543) or ''
    }	

    t.card_id = a
    t.success = true

    long_text = ocr.responses[1].fullTextAnnotation.text

    row_address = 1
    long_text = string.gsub(long_text, "ทีอยู่\n", "ที่อยู่ ")
    long_text = string.gsub(long_text, "ที่อยู\n", "ที่อยู่ ")
    long_text = string.gsub(long_text, "ที่อย่\n", "ที่อยู่ ")
    long_text = string.gsub(long_text, "ที่อยู่\n", "ที่อยู่ ")

    for i in string.gmatch(long_text, "[^\r\n]+") do
        if row_address == 2 then
            t_add = string.gmatch(i,"%S+")
            province = ''
            for j in t_add do
                province = j
            end
            break
        end 

        if string.match(i, "ทีอยู่") or string.match(i, "ที่อยู") or string.match(i, "ที่อย่") or string.match(i, "ที่อยู่") then
            row_address = 2
        end

        if string.match(i, "Mr.") or string.match(i, "Mr") then
            t.sex = 'male'
        elseif string.match(i, "Ms") or string.match(i, "Ms.") or string.match(i, "Mrs") or string.match(i, "Mrs.") or string.match(i, "Miss") then
            t.sex = 'female'
        end 
    end

    if province == '' then
        if ocr.responses[1].fullTextAnnotation then
            local t_line, line = province_line(ocr.responses[1].fullTextAnnotation.text)
            if(line)then
                t_add = string.gmatch(t_line[line] or '',"%S+")
                province = ''
                for j in t_add do
                    province = j
                end
            end 
        end       
    end 
end
else
    rc:set('tmp.ocr_error', os.date() .. err)
    print(err)
end

if province ~= '' then
  for k,v in pairs(t_province) do
    if string.match(k, province) then
        go_google = false
        
        t.province = v
      break
    end
  end
else
  t.province = ''
end

print(cjson.encode(t))

