var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo =  process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
  var bg = "#1667B2"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"
  var owner_name = process.argv[2]

  var html = `<!doctype html>

  <html lang="th-TH" dir="ltr">
  
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
  </head>
  
  <body style="background: #E0E0E0;padding-bottom: 30px;"><br>
  
    <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 30px 50px 30px;width:80%;border-radius: 5px;">
      <tr>
        <td style="width: 100%;text-align: center;">
          <img src="${logo}" alt="logo website" style="width: 300px;margin-top: 30px;">
        </td>
      </tr>
      <tr>
        <td style="width: 100%;padding: 0px 30px 50px 30px;">
          <div style="font-size: 16px;">
            <b>เรียน : </b> ${owner_name}
          </div>
          <div style="margin-top:10px;font-size: 14px;">
            เพื่อยืนยันการสร้างบัญชีของคุณ กรุณากรอกรหัสนี้ในระบบ
          </div>
          <div
            style="margin-top:15px;padding:5px;color:#FFF;background-color:#0058A9;font-size: 18px;font-weight: bold;letter-spacing: 5px;text-align:center;">
            <div>
              <img src="`+ process.env.HOST_NAME +`/bla/images/mail/mail_active.png" style="margin-top: 30px;">
            </div>
            <div style="margin-top: 10px;">
              ${process.argv[3]}
            </div
          </div>
          <div style="margin-top:14px">
            หากต้องการสอบถามข้อมูลเพิ่มเติมโปรดติดต่อฝ่ายบริการลูกค้า support_eks@creden.co
          </div>
          <div style="margin-top:14px">
            <b>หมายเหตุ</b> : โปรดอย่าตอบกลับอีเมลนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
          </div>
        </td>
      </tr>
    </table>
    <hr>
    <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 30px 50px 30px;width:80%;border-radius: 5px;">
      <tr>
        <td style="width: 100%;text-align: center;">
          <img src="${logo}" alt="logo website" style="width: 300px;margin-top: 30px;">
        </td>
      </tr>
      <tr>
        <td style="width: 100%;padding: 0px 30px 50px 30px;">
          <div style="font-size: 16px;">
            <b>Dear. : </b> ${owner_name}
          </div>
          <div style="margin-top:10px;font-size: 14px;">
            You have successfully set up an account. To complete your registration, please verify your email address by entering the verification code below.
          </div>
          <div
            style="margin-top:15px;padding:5px;color:#FFF;background-color:#0058A9;font-size: 18px;font-weight: bold;letter-spacing: 5px;text-align:center;">
            <div>
            <img src="`+ process.env.HOST_NAME +`/bla/images/mail/mail_active.png" style="margin-top: 30px;">
            </div>
            <div style="margin-top: 10px;">
              ${process.argv[3]}
            </div
          </div>
          <div style="margin-top:14px">
            Please send any feedback or bug reports to support_eks@creden.co
          </div>
          <div style="margin-top:14px">
            <b> Note </b>  : Please do not reply to this automated email.
          </div>
        </td>
      </tr>
    </table>
  </body>
  
  </html>`

  var smtp = {
    // host: 'smtp.mailgun.org', //set to your host name or ip
    host: 'smtp.bla.co.th', //set to your host name or ip
    port: 25, //25, 465, 587 depend on your 
    auth: {
      user: 'electronic_sign@bangkoklife.com', //user account
      // // pass: 'creden2018' //user password
 //  pass: 'dnkrakkaxnmnakpr'
      // user: 'postmaster@mg.creden.co', //user account
      // pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'Bangkok Life', //from email (option)
    to: process.argv[2], //to email (require)
    cc: '',
    subject: '[Bangkok Life] - Code Authen Register', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


