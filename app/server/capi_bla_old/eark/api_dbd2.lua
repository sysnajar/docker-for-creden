#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local mongo = require "resty-mongol"



function split(str, pat)
   local t = {}  -- NOTE: use {n = 0} in Lua-5.0
   local fpat = "(.-)" .. pat
   local last_end = 1
   local s, e, cap = str:find(fpat, 1)
   while s do
      if s ~= 1 or cap ~= "" then
         table.insert(t,cap)
      end
      last_end = e+1
      s, e, cap = str:find(fpat, last_end)
   end
   if last_end <= #str then
      cap = str:sub(last_end)
      table.insert(t, cap)
   end
   return t
end


if(ngx)then  
   ngx.req.read_body()
   print = ngx.say
   args  =  ngx.req.get_post_args()
else
   args   = require 'pl.lapp' [[-f, --firstname (default '') Person's firsname
   -l, --lastname (default '') Person's lastname
   -t, --companyname (default '') CompanyName (Thai)
   -e, --companyname (default '') CompanyName (Eng)
   -j, --JP_NO (default '')
   --fin include financial info (default to FALSE)
   ]]
end

conn = mongo()
--conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	
     obj = {}
	 --args.firstname = 'ทรงยศ' -- 'ภาวุธ'
   --args.lastname  = 'เฮงสุนทร'
     --args.fin = true
     result1 = {}
	 if(args.firstname ~= '' or args.lastname ~= '') then
		local fullname =  args.firstname .. (args.lastname=='' and '' or ' '.. args.lastname)
    db = conn:new_db_handle("newdbd")
		col = db:get_col("company")
        s, err = col:find({PARTNER = {['$regex'] = fullname}}) 
        if s then
	        data = {}
	        for k, v in s:pairs() do
	            table.insert(data, v)
	        end
	        result1 = {success = true, error_msg = "complete", data = data, total = table.getn(data)}

	        profit_comp = 0
    		 	non_profit_comp = 0
    		 	for i,v in pairs(result1.data) do
    		 		v.financialInfo["__array"] = nil
    		 		profit = 0
    		 		for j,k in pairs(v.financialInfo) do
    		 			profit = profit + tonumber(k.NET_PROFIT_MARGIN)
    		 		end
    		 		if profit > 0 then
    		 			profit_comp = profit_comp + 1
    		 		else
    		 			non_profit_comp = non_profit_comp + 1
    		 		end
    		 	end
    		 	profit_info = { profit_comp = profit_comp, non_profit_comp = non_profit_comp }
    		 	result1.profit_info = profit_info

	        print(cjson.encode(result1))
	    else	    	
	        ret = {success = false, error_msg = "N?A"}
	        print(cjson.encode(ret))
	    end
     end
end