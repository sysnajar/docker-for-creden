#!/usr/bin/env lua
local cjson = require 'cjson'
redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

local scb = cjson.decode(rc:get('demo@creden.co.scb'))

function compare(a, b) 
    return a.sort < b.sort
end

function convert(date,time)
    t_date = {}
    t_time = {}
    date = string.gsub(date, "/", " ")
    for i in string.gmatch(date, "%S+") do
        table.insert(t_date,i)
    end

    time = string.gsub(time, ":", " ")
    for j in string.gmatch(time, "%S+") do
        table.insert(t_time,j)
    end

    return tonumber(t_date[3]..t_date[2]..t_date[1]..t_time[1]..t_time[2])
end

for i,acc in ipairs(scb.accounts) do

    for j,st in ipairs(acc.data) do
        st.sort = convert(st.date,st.time)
    end


    table.sort(acc.data, compare)
end

print(cjson.encode(scb))