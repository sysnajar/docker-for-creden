#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("user_email") 
    selector = {key = body.fb_id, reset_key = body.reset_key, source = 'facebook'}
    s = col:find_one(selector)  
    if s then 
        update = {["$set"] = {email = body.email, reset_key = ""}}
        u, err = col:update(selector, update, 0, 0, true)
        if not u then
            ret = {success = false, error_msg = err}
        else
            ret = {success = true, error_msg = "Complete"}
        end
    else
        ret = {success = false, error_msg = "Invalid activate code"}
    end
end

print(cjson.encode(ret))


