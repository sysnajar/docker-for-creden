#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson.safe'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end

    local comp_fn =  function(t1, t2)
        for _k, _v in ipairs(t1) do
            for k, v in ipairs(_v.data) do
                v.deposit = v.deposit:gsub("+", "")
                v.deposit = v.deposit:gsub(",", "")
                v.withdraw = v.withdraw:gsub("-", "")
                v.withdraw = v.withdraw:gsub(",", "")
                v.balance = v.balance:gsub(",", "")

                if tonumber(v.deposit) then
                    t2.EBANK_sumcashin6mth = t2.EBANK_sumcashin6mth + tonumber(v.deposit)
                end
                if tonumber(v.withdraw) then
                    t2.EBANK_sumcashout6mth = t2.EBANK_sumcashout6mth + tonumber(v.withdraw)
                end
                if k == #_v.data and tonumber(v.balance) then
                    t2.EBANK_finBalance = t2.EBANK_finBalance + tonumber(v.balance)
                end
                if k == 1 and tonumber(v.balance) then
                    t2.EBANK_iniBalance =  t2.EBANK_iniBalance + tonumber(v.balance)
                end
            end
        end
        return t2
    end

    local fn =  function(id)
        local key = 'PREP:FIN:' .. id
        local t1 = cjson.decode(rc:get(key))
        local t2 = {EBANK_sumcashin6mth = 0, EBANK_sumcashout6mth = 0, EBANK_finBalance = 0, EBANK_iniBalance = 0, capital = 0, cashflow = 0}
        
        if t1.scb.accounts then t2 = comp_fn(t1.scb.accounts,t2) end
        if t1.kbank.accounts then t2 = comp_fn(t1.kbank.accounts,t2) end

        t2.capital = ((t1.scb.capital or 0) + (t1.kbank.capital or 0))
        t2.cashflow = ((t1.scb.cashflow or 0) + (t1.kbank.cashflow or 0))
    
        return t2
    end
    
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
    