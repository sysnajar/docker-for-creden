#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { owner_folder = arg[1], status_folder = arg[2] ,name_folder = arg[3] }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    -- db = conn:new_db_handle("edoc")
    -- col = db:get_col("folders")
    local date = os.date('%d/%m/%Y %H:%M:%S')
	local data = {
		fname = "thanaphas",
		lname = "Laohasinnuruk",
		job = "programmer",
		created_dtm = date
	}


        ret = { error_msg = "Add folder success", success = true, data = data }



end

print(cjson.encode(ret))
