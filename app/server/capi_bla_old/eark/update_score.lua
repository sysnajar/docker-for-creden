#!/usr/bin/env lua

-- split a string
function string:split(delimiter)
local result = { }
local from  = 1
local delim_from, delim_to = string.find( self, delimiter, from  )
while delim_from do
  table.insert( result, string.sub( self, from , delim_from-1 ) )
  from  = delim_to + 1
  delim_from, delim_to = string.find( self, delimiter, from  )
end
table.insert( result, string.sub( self, from  ) )

return result
end

function init()
    if(#arg > 0  )then
      cjson = require 'cjson'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
	  score_types = string.split(arg[2], ',')
      return true
    else
      return false	
    end 
end
    local fn =  function(id, score_types)
		-- print('updating score of ', id, score_types)
		for _, v in ipairs(score_types) do
            --print(v)
            local cmd1 = '~/scripts/PREP_'.. v .. '.lua ' .. id .. ' | ~/scripts/rset.lua ' .. 'PREP:'.. v .. ':' .. id
			os.execute(cmd1)

            local cmd2 = '~/scripts/TRAN_'.. v .. '.lua ' .. id .. ' | ~/scripts/rset.lua ' .. 'TRAN:'.. v .. ':' .. id
			os.execute(cmd2)

            local cmd3 = '~/scripts/COMP_'.. v .. '.lua ' .. id .. ' | ~/scripts/rset.lua ' .. 'COMP:'.. v .. ':' .. id
			os.execute(cmd3)

	    end
		return {success = true}
    end
    
    
    if(init())then
      print(cjson.encode(fn(id, score_types)))
    else
      return fn
    end
    
