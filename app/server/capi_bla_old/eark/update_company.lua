#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {adminUsername = arg[1]} 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end
--        local counter_key = 'counter.'..body.adminUsername..'.tx'
--        local max_counter = 'counter.'..body.adminUsername..'.txMax'
--        if body.comStatus == 'tial' then

       local counter_key = 'counter.'..body.adminEmail..'.tx'
       local max_counter = 'counter.'..body.adminEmail..'.txMax'
	   local send_mail = false

       if body.comStatus == 'tial' then
		   send_mail = true
            r = rc:get(counter_key)
            if not r then
                rc:set(counter_key,0)
                rc:set(max_counter,20)
            else
                rc:set(max_counter,20)                
            end
        else
            rc:set(max_counter,0)            
        end

        --update mongo
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if err then
            ret = {success = false, error_msg = err}
        else
           db = conn:new_db_handle("dopa")
            col = db:get_col("company")
            body.activeStatus = 4
            update = body
            id = body._id
            body._id = nil
            u, err = col:update({_id=object_id.new(convertHexStringToNormal(id))}, update, 0, 0, true)
            
            if err then
                ret = {success = false, error_msg = err}
            else
                ret = {success = true, error_msg = err, data = body}

                --send email if needed
				if(send_mail) then
                    local msg = {
                     key  = 'email' .. tostring(ngx.now()), to   = body.adminEmail, subject = 'Your creden.co account has been activated', 
                     html = [[Your creden.co account has been activated.<br/>Please <a href="]]..HOST_NAME..[[/business">re-login</a>
					 and start using our service.<br/>Thanks! ]]
                     }

                     rc:set(msg.key, cjson.encode(msg))
                     cmd = 'node /home/creden/work/app/server/capi/send_mail.js ' .. msg.key
                     os.execute(cmd)
                end
				--end send email


            end

        end
ret = {success = true, error_msg = err, data = body, send_mail = send_mail}

print(cjson.encode(ret))


