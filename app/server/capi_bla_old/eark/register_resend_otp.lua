#!/usr/bin/env lua
local bson = require "resty-mongol.bson"
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
prog.timeout_fatal = false


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body  = { adminEmail = arg[1] } 
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1', 27017)
if err then
    ret = { success = false, error_msg = "connect mongo fail", error_code = "" }
else

    db = conn:new_db_handle("dopa")
    col_pre = db:get_col("pre_register")
    col_esig = db:get_col("esig_user")
    p, err = col_pre:find({ adminEmail = body.adminEmail or "N/A" })
    s, err = col_esig:find_one({ adminEmail = body.adminEmail or "N/A", activeStatus = { ["$ne"] = 4 } })
   
    local list_mail = {}
    for k, v in p:pairs() do 
        v._id = nil
        table.insert(list_mail, v)
    end
        

    if #list_mail > 0 then

        local user = list_mail[#list_mail]    
        math.randomseed(os.clock()*100000000000)
        otp = ''
        token = ''
        for i=1,3 do
            a = math.random(10, 99)       
            otp = otp .. a
        end
        for i=1,10 do
            a = math.random(10, 99)       
            token = token .. a
        end
    
        key = 'otp_' .. user.adminEmail .. '_' .. token .. '_' .. otp
        data = {}
        data.token = token
        rc:set(key, user.activeCode)
        rc:expire(key, 600)
        local m, err = prog('node', '/home/creden/work/app/server/capi/register_mail.js', body.adminEmail, otp) -- Send email register
        ret = { success = true, error_msg = "send mail otp success", error_code = "", data = data }

    elseif s then
        s._id = nil
        math.randomseed(os.clock()*100000000000)
        otp = ''
        token = ''
        for i=1,3 do
            a = math.random(10, 99)       
            otp = otp .. a
        end
        for i=1,10 do
            a = math.random(10, 99)       
            token = token .. a
        end
    
        key = 'otp_' .. s.adminEmail .. '_' .. token .. '_' .. otp
        data = {}
        data.token = token
        rc:set(key, s.activeCode)
        rc:expire(key, 600)
        local m, err = prog('node', '/home/creden/work/app/server/capi/register_mail.js', s.adminEmail, otp) -- Send email register
        
        ret = { success = true, error_msg = "send mail otp success esig user not active", error_code = "", data = data }

    else

        ret = { success = false, error_msg = "not list email", error_code = "" }

    end
        
end

print(cjson.encode(ret))


