#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function get_user_fields(fields, email)
    local is_sign = false
    for i, v in ipairs(fields) do
        if v.email == email then
            is_sign = true
            break;
        end
    end
    return is_sign
end

function update_status_restore(doc_id) 

    doc, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(doc_id)) })

    if doc and doc.status == 1 or doc.status == 3 then
        local sign = {}
        local owner_is_remove = nil
        local owner_time_remove = nil

        for i, v in ipairs(doc.signers) do

            if v.is_group == true then
                local count_sign_group = 0
                for i2, v2 in ipairs(v.user_group) do 

                    local my_sign = get_user_fields(doc.fields, v2.email)
                    if my_sign == true and v2.email == body.email then
                        v2.is_remove = ""
                        v2.time_remove = ""
                    
                    elseif my_sign == false and v2.email == body.email then
                        v2.is_remove = ""
                        v2.time_remove = ""
                    end

                    if v2.is_remove == true and my_sign == true then
                        count_sign_group = count_sign_group + 1
                    end

                end

                if count_sign_group ~= v.amount_least_sign then
                    v.is_remove = ""
                    v.time_remove = ""
                end

            else

                if v.email == body.email then
                    v.is_remove = ""
                    v.time_remove = ""
                end

            end


            table.insert(sign, v)
        end

        if doc.owner == body.email then
            owner_is_remove = ""
            owner_time_remove = ""
        end

        local key = { _id = object_id.new(convertHexStringToNormal(doc_id)) }
        local update_cmd = { signers = sign, owner_is_remove = owner_is_remove, owner_time_remove = owner_time_remove }
        local update_s, err_s = col:update(key, {["$set"] = update_cmd}, 0, 0, true)
    end

end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { company_id = "", email = "",  }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    local list_id = body.list_id
    for i, v in ipairs(list_id) do
        update_status_restore(v)
    end

    ret = { success = true, error_msg = "accept restore success", error_code = "2102" }


print(cjson.encode(ret))

end
