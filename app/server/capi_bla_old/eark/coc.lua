#!/usr/bin/env lua
local session = require "resty.session".open({secret = "4321"})
--new
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
object_id = require "resty-mongol.object_id"
common = require "common"
--new

--ngx.say("<html><body>Session was started by <strong>", session.data.username or "Anonymous", "from", session.data.custCode or "Unknown Company")

local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    body = ngx.req.get_uri_args()
else
   body  = {adminEmail = arg[1],file = "1530097604161",stamp = false} 
end


have_file = false 
err_msg = ''
response = {error = "NO PERMISSION"}

username = session.data.username or 'N/A'
ngx.log(ngx.NOTICE, "username = " .. username)

    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = "connection error"}
    else
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        doc = col:find_one({id = body.file})

        --check 3.1
		if(doc.owner == username or username == "paysolution1@gmail.com" or username == "demotaladinvvoice@gmail.com" or username == "ktbst@creden.co" or doc.owner == "alltest@creden.co")then
	--	if(doc.owner == username or doc.compcodex == 'demoquick01@creden.co')then
           have_file = true
		end

        --check 3.2
	    for i, person in ipairs(doc.signers) do
            if(person.email == username)then 
				have_file = true 
			end
		end
		
        --check 3.3
		if(body.key and body.key~="" and doc.password and body.key==doc.password )then
           have_file = true
		end


        --check 3.4
        local position_list = common.edoc_get_positions_id_by_email(username) 

	    for _, pos in ipairs(position_list) do
            for __, f in ipairs(doc.fields) do
                  if(f.position_id and tostring(f.position_id)==pos) then
                      have_file = true
				  end
			end
			
		end


		-- group signing
		for _,f in ipairs(doc.fields) do
			if(f.person and f.person.is_group  and 
				common.is_in_group(f.person.user_group, username)) 
			then
		            have_file = true
					break
			end
		end
		-- end group signing

		if(doc.share_public==true)then
		 have_file = true
		end

		if  have_file then
		   file_name = body.file
			if body.stamp then
				file_name =  file_name .. 'ts'
			end
			ngx.exec('/face/images/card/'..file_name..'.pdf')
		else
			-- ngx.say(cjson.encode(response))
			ngx.redirect('/permission.html')
		end

end
