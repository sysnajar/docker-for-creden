#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1], code = arg[2]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("creden")
    col = db:get_col("users")
    if body.type == "google" then
        selector = {gl_id=body.gl_id}
    elseif body.type == "facebook" then
        selector = {fb_id=body.fb_id}
    end
    
    update = {["$set"] = {tel = body.tel}}
    i, err = col:update(selector, update, 0, 0, true)
    if not i then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true, error_msg = "complete"}
    end
end

print(cjson.encode(ret))


