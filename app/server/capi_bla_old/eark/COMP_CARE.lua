#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson.safe'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end
    -- id = kjunchean@gmail.com:kbank
    --{"EBANK_sumcashin6mth":353871.14,"EBANK_finBalance":2785.49,"EBANK_sumcashout6mth":351335.75,"EBANK_iniBalance":51.1}
    local fn =  function(id)
        local key = 'TRAN:CARE:' .. id
        local t = cjson.decode(rc:get(key))
        --cjson = require 'cjson.safe'
        --local t = '{"IN_occu":"Self-employed","IN_salgrade":30000,"IN_workyrs":"1-3"}'
        --t = cjson.decode(t)
        local score = 0 --total = 190

    		if(t==nil) then
               return 0
    		end

        if t.IN_occu == "Unemployed" then score = 0        
        elseif t.IN_occu == "Self-employed" then score = 20
        elseif t.IN_occu == "Government Officials" then score = 30
        elseif t.IN_occu == "Employee" then score = 40
        elseif t.IN_occu == "Business Owner" then score = 60
        end
        t.IN_salgrade = tonumber(t.IN_salgrade)
        if t.IN_salgrade == 0 then score = score + 0
        elseif t.IN_salgrade > 0 and t.IN_salgrade <= 8000 then score = score + 10 
        elseif t.IN_salgrade > 8000 and t.IN_salgrade <= 12000 then score = score + 15
        elseif t.IN_salgrade > 12000 and t.IN_salgrade <= 20000 then score = score + 20
        elseif t.IN_salgrade > 20000 and t.IN_salgrade <= 35000 then score = score + 30
        elseif t.IN_salgrade > 35000 and t.IN_salgrade <= 50000 then score = score + 40
        elseif t.IN_salgrade > 50000 and t.IN_salgrade <= 70000 then score = score + 50
        elseif t.IN_salgrade > 70000 and t.IN_salgrade <= 100000 then score = score + 60
        elseif t.IN_salgrade > 100000 and t.IN_salgrade <= 150000 then score = score + 70
        elseif t.IN_salgrade > 150000 and t.IN_salgrade <= 200000 then score = score + 80
        elseif t.IN_salgrade > 200000 then  score = score + 100 
        end

        if t.IN_workyrs == '0' then score = score + 0
        elseif t.IN_workyrs == '<1' then score = score + 5
        elseif t.IN_workyrs == '1-3' then score = score + 8
        elseif t.IN_workyrs == '3-5' then score = score + 10
        elseif t.IN_workyrs == '5-8' then score = score + 15
        elseif t.IN_workyrs == '8-12' then score = score + 20
        elseif t.IN_workyrs == '12-15' then score = score + 25
        elseif t.IN_workyrs == '>15' then score = score + 30
        end

        return score
    end
    
    --print(fn(1))
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
