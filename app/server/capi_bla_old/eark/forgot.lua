#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local redis = require 'redis'
local scrypt = require "resty.scrypt"
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    json = ngx.req.get_body_data()
    body = cjson.decode(json)
else
    body  = {adminEmail = arg[1] , adminPassword = arg[2] ,adminCode = arg[3]} 
end

    ret = {}
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
        ret = { success = false, error_msg = "connect mongo fail", error_code = "" }
    else 
    
        local key = 'resetpwd:' .. body.adminEmail 
        key = rc:get(key)

        if key == body.code then

            hashedPassword  = scrypt.crypt(tostring(body.adminPassword))
            local update_cmd = {["$set"] = { hashedPassword = hashedPassword }}
            db = conn:new_db_handle("dopa")
            col = db:get_col("esig_user")
            u, err = col:update({ adminEmail = body.adminEmail }, update_cmd, 0, 0, true)
            s, err2 = col:find_one({ adminEmail = body.adminEmail })

            if err then
                ret = { success = false, error_msg = "update reset password unsuccess", error_code = "" }
            else

                s._id = nil
                s.adminPassword = nil;
                s.adminPasswordC = nil;
                s.encryptPass = nil
                s.hashedPassword = nil
                s.cvv_code = nil
                s.txtPid = nil
                s.date_of_birth = nil

                math.randomseed(os.clock()*100000000000)
                extend_key = ''
                for i=1,2 do
                    a = math.random(10, 99)       
                    extend_key = extend_key .. a
                end
                rc:set('extend_key' .. ':' .. s.adminEmail .. ':' .. extend_key, "true")
                rc:expire('extend_key' .. ':' .. s.adminEmail .. ':' .. extend_key, 60*30)
                local session = require "resty.session".start{ secret = "4321" }
                session.data.username = s.adminEmail
                session.data.custCode = s.adminEmail
                session:save()

                ret = { success = true, error_msg = "update reset password success", error_code = "", data = s, username = s.adminEmail, custCode = s.adminEmail, extend_key = extend_key }
            
            end
            
        else 
            ret = { success = false, error_msg = "code invalid", error_code = "" }
        end


    end
    
print(cjson.encode(ret))
