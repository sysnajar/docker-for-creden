#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    db = conn:new_db_handle("dopa")
    col = db:get_col("group_sign")
    body.company_id = session.data.company_id
    g, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(body.id)), company_id = body.company_id })

    if g then

        del, err_del = col:delete({ _id = object_id.new(convertHexStringToNormal(body.id)), company_id = body.company_id })
        if del then
            ret = { success = true, error_msg = "remove group success", error_code = "2023" }
        else
            ret = { success = false, error_msg = "remove group unsuccess", error_code = "2024" }
        end
    
    else
        ret = { success = false, error_msg = "find not ground", error_code = "2022" }
    end
end
print(cjson.encode(ret))
