#!/usr/bin/env lua

local ev = require'ev'
local ws_client = require('websocket.client').ev()

ws_client:on_open(function()
    print('connected')
  end)

ws_client:connect('ws://creden.co:8134/creden','creden')

ws_client:on_message(function(ws, msg)
    print('received',msg)
  end)


-----
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ch = 'jobs'

for msg, abort in rc:pubsub({subscribe = {ch}}) do
   if(msg.kind=='message' and msg.channel==ch)then
	 ws_client:send('jobdone:' .. msg.payload )
   end
end

