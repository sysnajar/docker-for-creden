var mailer = require("nodemailer");

process.env.HOST_NAME = "https://esign.ldd.go.th"
// process.env.HOST_NAME = "https://dev2.hjkl.ninja"
var logo = "https://esign.ldd.go.th/company/img/LDD/LDD_Logo.png"
var password = process.argv[6]
var html_pass = `<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;*This document is password protected. To unlock the document<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;please use the pin : `+password+`</p>`
var email = process.argv[2]
var name = process.argv[7]
var sender_email = process.argv[8]
var sender_name = process.argv[9]
var html = `
<html>
 <head>
  <link href="https://fonts.googleapis.com/css?family=Prompt" rel="stylesheet">
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>Confirmation</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
  <style>
  a {
      color:#15c !important;
  }
  lable {
    color: #000000 !important;
  }
  .redius{
    border-radius:  30px 30px 0 0;;
  }
  .redius2{
    border-radius:  0 0 30px 30px;;
  }
  .redius3{
    border-radius:  30px;
  }
  .ii a[href]{
    color: #ffffff !important;
  }
  </style>
</head>
<body style="margin: 0; padding: 0;font-family: 'Prompt', sans-serif;" bgcolor="#F5F5F7">
 <table border="0"  cellpadding="0" cellspacing="0" width="100%" background="`+process.env.HOST_NAME+`/signature/img/ldd-bg-01.png"  style="background-repeat: repeat-x;">
  <tr>
   <td >
     <table class="redius3" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;box-shadow: 5px 5px 5px 1px rgba(50,50,50,.1);margin-top: 50px;margin-bottom: 70px;">
       <tr>
         <td class="redius"  align="center" bgcolor="#ffffff" style="padding: 40px 0 0px 0;">
          <img src="`+ logo +`" style="width:130px" />
         </td>
       </tr>
       <tr>
         <td bgcolor="#ffffff" style="padding: 0px 30px 40px 30px;" class="redius2">
          <div><br>
          
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>From/จาก:</b> `+ sender_name +` (`+ sender_email +`)</p>
          <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Message/ข้อความ:</b> `+process.argv[3]+`</p>`;


          if(password != ''){
            html = html + html_pass
          }

          html = html+`<br><a href="`+process.env.HOST_NAME+`/company/#/document?id=`+process.argv[5]+`"><div style="display: table; clear: both; background-color: #7e983f !important; margin-left: auto; margin-right: auto; width: 500px; height: 500px;"><br><br><img src="`+process.env.HOST_NAME+`/signature/app-assets/images/document_Email2.png" class="center" style="display: block; margin-left: auto; margin-right: auto;"><br><br><center><h2 style="color: white;">You have document waiting for you to sign</h2></center><center><h2 style="color: white;">คุณมีเอกสารที่ต้องเซ็นรออยู่</h2></center><br><br><center><h3><a href="`+process.env.HOST_NAME+`/company/#/document?id=`+process.argv[5]+`" style="color: #ffffff !important">Click here to sign</a></h3></center><center><h3><a href="`+process.env.HOST_NAME+`/company/#/document?id=`+process.argv[5]+`" style="color: #ffffff !important;">คลิกตอบรับเพื่อเซ็นหรืออ่านเอกสาร</a></h3></center></div><div style="margin-left:315px">
            </div></div></a>
         </td>
       </tr>
       <tr>
 
       </tr>
     </table>
   </td>
  </tr>
 </table>
</body>
</html>
`

// var smtp = {
//     // host: 'smtp.gmail.com', //set to your host name or ip
//     host: 'smtp.mailgun.org', //set to your host name or ip
//     port: 465, //25, 465, 587 depend on your 
//     auth: {
//     //   user: 'credenmailer@gmail.com', //user account
//     //   pass: 'creden2018' //user password
//       user: 'postmaster@mg.creden.co', //user account
//       pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
//     }
//   };
var smtp = {
    host: 'mail.ldd.go.th', //set to your host name or ip
    port: 25, //25, 465, 587 depend on your test 465 
    auth: {
      user: 'esignature@ldd.go.th', //user account
      pass: 'lddesign2562' //user password
    }
  };  
  var smtpTransport = mailer.createTransport(smtp);

  var mail = {
    from: 'LDD <noreply@ldd.go.th>', //from email (option)
    // from: 'LDD eSignature <noreply@ldd.go.th>', //from email (option)
    to: process.argv[2], //to email (require)
    cc: '',
    subject: process.argv[4], //subject
    html: html//head + body + end
 }

 smtpTransport.sendMail(mail, function(error, response){
    smtpTransport.close();
    if(error){
       //error handler
       var res = {"success":false, "message":error}
       console.log(res);
    }else{
       //success handler 
       var res = {"success":true}
       console.log(res);
    }
    process.exit();
 });


      
