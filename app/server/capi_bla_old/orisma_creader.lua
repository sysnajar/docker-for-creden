#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {success=false}
local conn = nil
prog.timeout = 1000 * 300
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    header = ngx.req.get_headers()
else
    body = {Action='CheckReader',SerialNumber='12345789SSSQPA',ReaderName='SCR0001',IPAddress='192.168.1.1'}
end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    local ok,err = conn:connect('127.0.0.1',27017)
    local result = false
    if err then
        result = false
        ret = {success = result, msg = err}        
    else
        result = true
    end
    return result
end

function chk_apiKey(apiKey)
    local chk = false
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("company")
    local s = col:find_one({apiKey = apiKey})   
    if s then
        chk = true  
    end
    return chk
end

function CheckReader(SerialNumber,ReaderName)
    local ret_CheckReader = {}
    local data = {}
    local db = conn:new_db_handle("db_orisma")
    local col = db:get_col("m_cardreader")       
    local t, err = col:find_one({SerialNumber = SerialNumber})
    if not t then
        data.SerialNumber = SerialNumber
        data.ReaderName = ReaderName
        data.ReadCount = 0
        data.InsertTime = bson.get_utc_date(os.time() * 1000)
        local i, err_i = col:insert({data})
        ret_CheckReader.success = true
        ret_CheckReader.msg = 'Insert complete'
    else
        ret_CheckReader.success = false
        ret_CheckReader.msg = 'Serial number is exists'
    end

    return cjson.encode(ret_CheckReader)
end

function UpdateReader(SerialNumber,IPAddress)
    -- connect mongo
    local ret_UpdateReader = {}
    local data = {}
    local conn = mongo()
    conn:set_timeout(10000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if err then
        -- ret = {success = false, msg = err}
        ret_UpdateReader.success = false
        ret_UpdateReader.msg = err
    else
        local db = conn:new_db_handle("db_orisma")
        local col = db:get_col("m_cardreader")
        local t, err = col:find_one({SerialNumber = SerialNumber})
        if not t then
            UpdateOtherReader(SerialNumber,IPAddress)
            ret_UpdateReader.success = false
            ret_UpdateReader.msg = 'Card reader serial number not exists'
        else
            local selector = {SerialNumber=SerialNumber}
            local update = {
            ["$set"] = {
                --InsertTime = bson.get_utc_date(os.time() * 1000),
                ReadCount = t.ReadCount+1,
                ReadTime= bson.get_utc_date(os.time() * 1000),
                IPAddress=IPAddress}
            }
            local n, err = col:update(selector, update, 0, 0, true)
            if not n then
                -- ret = {success = false, msg = err}
                ret_UpdateReader.success = false
                ret_UpdateReader.msg = err
            else
                ret_UpdateReader.success = true
                ret_UpdateReader.msg = 'Update success'
                -- ret = {success = true, msg = err, txid = body.txid , usr = body.compCode}
                -- print(cjson.encode(ret))
            end
        end
    end
    return cjson.encode(ret_UpdateReader)
end

function UpdateOtherReader(SerialNumber,IPAddress)
    local conn = mongo()
    conn:set_timeout(10000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if (not ok) then
        return
    end

    local data = {}
    local db = conn:new_db_handle("db_orisma")
    local col = db:get_col("log_other_cardreader")
    data.SerialNumber = SerialNumber
    data.IPAddress = IPAddress
    data.InsertTime = bson.get_utc_date(os.time() * 1000)
    local i, err_i = col:insert({data})
end

function GetDemoExpDate()
    local conn = mongo()
    conn:set_timeout(10000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false,msg = err}
    else
        local db = conn:new_db_handle("db_orisma")
        local col = db:get_col("sys_config")
        local t, err = col:find_one({})
        if t then
            ret.demo_expdate = t.demo_expdate
        end
    end
end

if (chk_apiKey(tostring(header.apiKey))) then
    if(body.Action ~= nil and body.SerialNumber~= nil) then        
        if(body.Action == "UpdateReader") then
            local result = UpdateReader(body.SerialNumber,body.IPAddress)
            result = cjson.decode(result)
            ret.success = result.success
            ret.msg = result.msg
            ret.ostime = tostring(os.time())

        elseif(body.Action == "CheckReader") then
            local result = CheckReader(body.SerialNumber,body.ReaderName)
            result = cjson.decode(result)
            ret.success = result.success
            ret.msg = result.msg
            ret.ostime = tostring(os.time())
        end

        GetDemoExpDate()
    else
        ret.success = false
        ret.msg = 'not data'
    end
else
    ret.success = false
    ret.msg = 'Authentication Fail'
end
 -- print(findxx3)
print(cjson.encode(ret))
