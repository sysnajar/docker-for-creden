#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    --body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
else
   body  = {img = arg[1], id = arg[2]} 
end


local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh',arg[1])
local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
if not err then
    ocr = cjson.decode(res.stdout)
    long_text = ocr.responses[1].fullTextAnnotation.text
    a = string.gsub(long_text,"%s+", "")
    t = {success = true, capcha = a}
    print(cjson.encode(t))
else
    print(err)
end