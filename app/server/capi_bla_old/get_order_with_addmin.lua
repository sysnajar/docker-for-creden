local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil
bson = require "resty-mongol.bson"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
-- local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("package_ss")
    col_pay = db:get_col("history_payload_payment")
    get_order = col:find({})

    list_oreder = {}

    if get_order then
        for i, v in get_order:pairs() do
            -- print(v.ref_id)
            v._id = nil
            
            
            get_payment_order = col_pay:find_one({refno = v.ref_id})
            if get_payment_order then
                v.cardtype = get_payment_order.cardtype
            else
                v.cardtype = '-'
            end
            table.insert(list_oreder, v) 
        end
        ret.success = true
        ret.data = list_oreder
    else
        ret.error_msg = "no order"
    end
    
end

print(cjson.encode(ret))