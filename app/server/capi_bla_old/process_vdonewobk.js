const puppeteer = require('puppeteer');

//public
var redis = require('redis');
var opt = {password:'noRedisPass1010'}
//var rc = redis.createClient(6379, '172.31.7.255', opt); 
var rc = redis.createClient(6379, '52.77.238.216', opt); 
//var rc = redis.createClient(6379, '127.0.0.1'); 
var pc_id = "dev2"
var url_prefix
//var bot_url = 'https://hjkl.ninja/brfv4/ex'; 
var bot_url = 'https://hjkl.ninja/brfv4/ex'; 

//
var HOST_NAME = "https://hjkl.ninja"
//var vdo = {"url":"https://hjkl.ninja/vdo/prod/1543236299603.webm","missions":[{"no":"1","type":"voice","startTime":"00:05","endTime":"00:08","word": "มือถือ"}, {"no":"2","type":"blink", "word": "", "startTime":"00:11", "endTime":"01:16"}]}


/*
setTimeout(function(){
console.log("+++++++++++++++++++++++ vdo queue inserted +++++++++++++++++")	
var vdo_req = {vdo:vdo, txid:"eark001", compCode:"creden", urlPrefix:HOST_NAME} 
rc.lpush('vdo.queue', JSON.stringify(vdo_req))
},30000);
*/

var mainFn = async function () {
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();
  page.on('console', function(msg){ var txt = msg.text();
    if(txt.indexOf('Beyond Reality Face SDK') !=0 && txt.indexOf('Please visit') !=0 && txt.indexOf('c++ (32bit)') !=0)
      console.log('PAGE LOG:', txt)
  });

  await page.goto(bot_url); 

  var args = process.argv.slice(2);
  pc_id = args[0] || "DEV2"
  console.log('pc_id = '+ pc_id)

//check queue
var show_waiting=true

function check_queue()
{
   //console.log('check_queue')
   rc.lpop('vdo.queue', function(err, reply){
    if(reply==null)
      { 
	//console.log("reply is null")
        if(show_waiting){console.log('wait to check_queue ');show_waiting=false}
        setTimeout(check_queue, 1000)
      }
     else
      {
        show_waiting=true
        process_vdo(reply)
      } 
   })
}

//------------------
function post_json(json)
{
    console.log("post_json is called ... json = "+ json)  
    //rc.lpush('vdo.queue.complete', json)

	// var url = url_prefix  +"/capi/update_res"
       var url = url_prefix  +"/hshapi/demoupdate_res"

	var update_cmd = "curl -X POST "+  url  +" -d '" +  json +  "'"
	update_cmd = update_cmd.replace( /"/g  , '\\"')
	update_cmd = update_cmd.replace( /'/g  , '"')
	console.log("update_cmd  = "+ update_cmd)  
	
	var exec = require('child_process').execSync;
	exec(update_cmd, function(error, stdout, stderr) {
	console.log("done update_cmd "+ stdout)
	});


}
//------------------
async function waitForSDK()
{
//    var ret = await page.evaluate('(brfv4Example!==undefined && typeof(brfv4Example.init)=="function")')
    const element = await page.$("#is_ready");
    const blink = await page.evaluate(element => element.textContent, element);
    console.log('is_ready: '+blink)

   if(blink=='start_vdo')

      check_queue()
    else
      setTimeout(waitForSDK, 1000)  
}

async function getVdoResults()
{
    //console.log('getVdoResults')
   var ret = await page.evaluate('getVdoResults()')

   if(ret!=null) {
       post_json(ret)
       check_queue()
     }
    else
       setTimeout(getVdoResults, 1000)  
}

async function process_vdo(json)
{
    console.log('processed vdo jsonA = '+json)
    //json = json.replace(/creden\.co/g,"hjkl.ninja")
    var req = JSON.parse(json)
    req.pc_id = pc_id
    url_prefix = req.urlPrefix
    req.vdo.url = req.vdo.url.replace("creden.co", "hjkl.ninja")

    json = JSON.stringify(req)
    processing_vdo = json
	
    console.log('processed vdo jsonB = '+json)
    console.log('URL:'+req.vdo.url)
    
    //got vdo
    await page.evaluate("setJob('"+ json  +"')")    
    getVdoResults()
}
     
  console.log("Waiting For SDK")
  waitForSDK()

}// main function
mainFn()
