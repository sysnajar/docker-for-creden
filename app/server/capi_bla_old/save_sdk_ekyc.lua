#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
res = {success = false}

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body = {code = 'esig'} 
end

username = body.username
body.username = nil

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    ok,  err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end

function save()
    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        update = {["$set"] = {conf = body}}
        u, err_u = col:update({adminEmail=username},update, 0, 0, true)
        if not u then
            res.error_msg = err_u
        else
            res.success = true
         end
    end 
end

save()

print(cjson.encode(res))
   
