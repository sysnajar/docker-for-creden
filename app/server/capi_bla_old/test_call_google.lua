#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
else
   body  = {img = arg[1], id = arg[2]} 
end


local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh',HOST_NAME..'/face/images/card/1516177912_out.png')
local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
if not err then
    ocr = cjson.decode(res.stdout)
    long_text = ocr.responses[1].fullTextAnnotation.text
    print(long_text)
else
    print(err)
end

print(cjson.encode(t))