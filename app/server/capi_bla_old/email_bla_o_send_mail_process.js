var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = 'http://brandage.com/images/content/5C8F7CB-0D894AC-46E32FF.png'
  var bg = "#1667B2"
  var name = process.argv[2]
  var wTable = "95%"
  // var link_forgot = process.env.HOST_NAME + "/ekyc_ndid_new/request_ndid_email.html?request_id" + process.argv[2] 
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/esign/web/policy.html"
  var error_code = process.argv[4]
  var err_c = {
    "30300": { "th": "ไม่ได้เป็นลูกค้าของ IdP ที่เลือก", "en": "ID not found" },
    }
  var html = `
  <!DOCTYPE html>
<html lang="th-TH" dir="ltr">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
</head>

<body>
  <div style="width: 100%;display: flex;justify-content: center;">
    <div style="width: 100%;border: 1px solid #E0E0E0;border-radius: 4px;background: #FFFFFF;padding: 15px 30px 30px 30px;">
        <div style="display: flex;justify-content: center;">   
          <div style="max-width: 160px;">
             <img src="`+logo+`" alt="logo website" style="width: 100%;">
          </div>
        </div>
        <div style="margin-top: 15px;font-style: normal;font-size: 14px;font-weight: 300;">
          เรียน <span style="font-weight: 800;">`+name+`</span>
        </div>
        <div style="margin-top: 20px;font-style: normal;font-size: 14px;font-weight: 300;">
         ใบรับรองอิเล็กทรอนิกส์ของคุณ จะหมดอายุในอีก 15 วัน หากต้องการต่ออายุใบรับรองอิเล๊กทรอนิกส์ สามารถทำได้ด้วยตัวเอง ดังนี้
        </div>
        <div style="margin-top: 5px;font-style: normal;font-size: 14px;font-weight: 300;">
        <p> 1. ล๊อคอินเข้าสู่ sign.bangkoklife.com <p>
        <p> 2. ไปที่เมนู ข้อมูลส่วนตัว <p>
        <p> 3. คลิกที่ปุ่ม อัปเดตการยืนยันตัวตน <p>
        <p> หากต้องการสอบถามข้อมูล เพิ่มเติมโปรดติดต่อผ่านบริการลูกค้า support@creden.co <p>
      
        </div>
        
       
        <div style="margin-top: 15px;font-style: normal;font-size: 14px;font-weight: 300;">
          หากต้องการสอบถามข้อมูลเพิ่มเติมโปรดติดต่อผ่ายบริการลูกค้า <a href="mailto:support@creden.co" style="color: #1667B2;text-decoration:none;">support@creden.co</a>
        </div>
        <div style="margin-top: 15px;font-style: normal;font-size: 14px;font-weight: 300;">
         <span style="font-weight: 800;">หมายเหตุ</span>  : โปรดอย่าตอบกลับอีเมลนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
        </div>
        <hr style="width: 100%;border: 0.5px solid #E0E0E0;margin-top: 30px;">
        <div style="margin-top: 30px;font-style: normal;font-size: 14px;font-weight: 300;">
          Dear <span style="font-weight: 800;">`+name+`</span>
        </div>
        <div style="margin-top: 20px;font-style: normal;font-size: 14px;font-weight: 300;">
         Your digital certificate will be expire in 15 days, please renew your digital certificate. Can be done by yourself as follows
        </div>
        <div style="margin-top: 5px;font-style: normal;font-size: 14px;font-weight: 300;">
        <p> 1. Log in to sign.bangkoklife.com <p>
        <p> 2. Go to the Profile menu <p>
        <p> 3. Click on the update eKYC button. <p>
        <p> หากต้องการสอบถามข้อมูล เพิ่มเติมโปรดติดต่อผ่านบริการลูกค้า support@creden.co <p>
      
        </div>
       
        <div style="margin-top: 15px;font-style: normal;font-size: 14px;font-weight: 300;">
          Please send any feedback or bug reports to <a href="mailto:support@creden.co" style="color: #1667B2;text-decoration:none;">support@creden.co</a>
        </div>
        <div style="margin-top: 15px;font-style: normal;font-size: 14px;font-weight: 300;">
         <span style="font-weight: 800;">Note</span>  : Please do not reply to this automated email.
        </div>
    </div>
  </div>
</body>
</html>
  `

  var smtp = {
    host: 'smtp.bla.co.th', //set to your host name or ip
    port: 25, //25, 465, 587 depend on your 
    auth: {
      user: 'electronic_sign@bangkoklife.com', //user account
      // pass: 'creden2018' //user password
     // pass: 'dnkrakkaxnmnakpr'
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'BLA <electronic_sign@bangkoklife.com>', //from email (option)
    to: process.argv[3], //to email (require)
    cc: '',
    subject: '[BLA]  Verification Certificate', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


