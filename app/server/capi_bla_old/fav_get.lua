#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local mongo = require "resty-mongol"
local body = nil
prog.timeout_fatal = false
local ret = {success = false, error_msg = ""}
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    -- =json
else
     body = {}
end


conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    -- ต่อdbได้

    local db_dopa = conn:new_db_handle("dopa")

    local col_history = db_dopa:get_col("fav_logs")
    -- เข้าหรือสร้าง

    --หาข้อมูลชื่อที่ตรงกัน
    local history =  col_history:find({owner=body.owner})
    
    
    -- เซ็คว่าข้อมูลที่ insert ที่ส่งไปจะไม่ซ้ำ


    local get_table = {}
    if history then
        for k, v in history:pairs() do 
            v._id = tostring(v._id)
            if(v.fav== true) then
                table.insert(get_table,v)
            end
        end
        ret.success = true
        ret.data = get_table
    else
        ret.success = false
        ret.msg = 'not found'

    end
    ngx.log(ngx.NOTICE,' ref====='..tostring(body.owner))
    ngx.log(ngx.NOTICE,' ref====='..tostring(history.fav))
end
print(cjson.encode(ret))