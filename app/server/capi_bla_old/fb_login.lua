#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local https=require 'ssl.https'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {accessToken = 'EAACEdEose0cBACJEZCF2TrA5QPpY5gqhDlerjzjZCZBLY3wuexcZBwnDpG2MMPPxPD1hglEiC0V9shbGqrzAEM6JTjgrLJivltT6vwIZCpia4b0N2LRdN3BZCECwuPZB0BQhgBAh0lS6ZC1YZCHg2k8RvieYZAeiZAvCAVmOpkbd8MZA8zmuwyQSrFDOjJU17IOgtzcZD'} 
end

ret = {success = false, error_msg = '', data = {}}
url = 'https://graph.facebook.com/me?fields=id,birthday,education,email,about,gender,first_name,last_name,work,likes,friends&access_token='..body.accessToken
fb,c,l,h = https.request(url)
body = cjson.decode(fb)

ret = {success = true, error_msg = "complete", data = body}

profilePicture = 'https://graph.facebook.com/v2.10/'..body.id..'/picture?type=large&redirect=true'

if not body.error then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret.error_msg = err
    else
        db = conn:new_db_handle("test")
        user = db:get_col("user")
        s = user:find_one({fb_Id=body.id})
        if s then
            --old user
            s._id = s._id:tostring()

            ret = string.match(s.profilePicture ,"https:")
            if not ret then
                local res, err_base64 = prog('sh', '/home/creden/work/app/server/capi/img2base64.sh', s.profilePicture)
                if not err_base64 then
                    s.profilePicture = rc:get('img_tmp')
                    ret = {success = true, error_msg = "complete", data = s}
                else
                    ret = {success = false, error_msg = "err_base64", data = s}
                end
            else
                ret = {success = true, error_msg = "complete", data = s}
            end

        else
            --new user
            body.email = body.id..'@fb.creden.co'
            doc = {firstname=body.first_name, lastname=body.last_name, email=body.email, activated=1, fb_Id=body.id, profilePicture = profilePicture}
            i, err = user:insert({doc}, nil, true)
            if err then
                ret = {success = false, error_msg = err}
            else
                ret = {success = true, error_msg = "complete", data = doc}
            end
        end

        fb = db:get_col("facebook")
        fb:update({fb_Id=body.id}, body, 1, 0, true)
    end
else
    ret.error_msg = "Login Fail!!"
end

print(cjson.encode(ret))
