#!/usr/bin/env lua

function init()
if(#arg > 0  )then
  cjson = require 'cjson.safe'
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379) 
  id = arg[1]
  return true
else
  return false	
end 
end

function compare(a, b) 
    return a.sort < b.sort
end

function convert(date,time)
    t_date = {}
    t_time = {}
    date = string.gsub(date, "/", " ")
    for i in string.gmatch(date, "%S+") do
        table.insert(t_date,i)
    end

    time = string.gsub(time, ":", " ")
    for j in string.gmatch(time, "%S+") do
        table.insert(t_time,j)
    end

    return tonumber(t_date[3]..t_date[2]..t_date[1]..t_time[1]..t_time[2])
end

local fn =  function(email)
    local scb = cjson.decode(rc:get(email..'.scb:comp'))
    local kbank = cjson.decode(rc:get(email..'.kbank:comp'))

    if scb then
        for i,acc in ipairs(scb.accounts) do
            for j,st in ipairs(acc.data) do
                st.sort = convert(st.date,st.time)
            end
            table.sort(acc.data, compare)
        end
    end

--[[    if kbank then
        for i,acc in ipairs(kbank.accounts) do
            for j,st in ipairs(acc.data) do
                st.sort = convert(st.date,st.time)
            end
            table.sort(acc.data, compare)
        end
    end]]

    local t = {scb = scb or {}, kbank = kbank or {}}
return t
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
