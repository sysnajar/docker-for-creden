#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local object_id = require "resty-mongol.object_id"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {id_folder = arg[1]} 
    -- body  = {id_folder = "5d5bb3f28a1a2eb6bd2cc8a0"} 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders")
    -- list = {company_id = "5c57c868778fdc6a5fd98acf"}
    list = { _id = object_id.new(convertHexStringToNormal(body.id_folder))}
    l,err = col:find_one(list)

    local list_folder = {}

    if not l.list_doc then
        local ret = {data = {} }
        print(cjson.encode(ret))
        return false
    end

    for i,v in ipairs(l.list_doc) do
        -- print(i,v.id_doc)
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        list_doc = { _id = object_id.new(convertHexStringToNormal(v.id_doc))}
        d,err = col:find_one(list_doc)

        if d then
            d._id = tostring(d._id)
            if d.createdDtm then
                d.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(d.createdDtm)/1000 + (7 * 60 * 60))
            end 
            table.insert(list_folder,d)
        end
    --     -- print(cjson.encode(v))
    -- --     if v.list_doc.id_doc == body.id_folder then
    -- --         v._id = tostring(v._id)
    -- --         if v.createdDtm then
    -- --             v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
    -- --        end
    -- --         table.insert(list_folder,v)
    -- --     end
    end
    
    local ret = {data = list_folder }
    print(cjson.encode(ret))
end



