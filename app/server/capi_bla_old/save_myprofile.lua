#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { name_document_type = arg[1] }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    
    local myusername = session.data.username
    if body.username == myusername then
        db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        e, err = col:find_one({ adminEmail = body.username or "N/A" })

        if e then
            
            local update_cmd = {["$set"] = { 
                adminFisrtname = body.adminFirstname
            }}
            n,err = col:update({ adminEmail = body.username }, update_cmd, 0, 0, true)

            if n then 
                ret = { success = true, error_msg = "save profile success", name = body.adminFirstname }
            else
                ret = { success = false, error_msg = "save profile unsuccess", err = "1" }
            end

        end
    else 
        ret = { success = false, error_msg = "session user expire", err = "2" }
    end

end

print(cjson.encode(ret))
