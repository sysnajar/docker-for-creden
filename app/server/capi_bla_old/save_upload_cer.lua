local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {cer_owner='amorrut@creden.co',cer_name="name.pfx",file_name='12345678'}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("certificate")
    if body.file_name then
        math.randomseed(os.clock()*100000000000)
        id_cer= ''
        for i=1,4 do
            a = math.random(10, 99)       
            id_cer = id_cer .. a
        end
        local data ={
            cer_id = id_cer,
            cer_owner = body.cer_owner,
            cer_name = body.cer_name,
            cer_file_name = body.file_name,
            cer_exp = body.cer_exp,
            cer_date = body.cer_date,
        }
        u,err = col:insert({data})
        if u then
            ret = { success = true, msg = "success"}
        else
            ret = { success = false, msg = "not success"}
        end
    end
    

end 
    print(cjson.encode(ret))