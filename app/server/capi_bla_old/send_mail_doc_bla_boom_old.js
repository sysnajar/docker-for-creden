var mailer = require("nodemailer");
send_mail()

//process.env.HOST_NAME = 'https://docker-for-creden-credenesignature.apps.nprod.bla.co.th'

function send_mail() {
  var logo = process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
  var bg = "#1667B2"
  var wTable = "100%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"
  var msg = process.argv[3]
  var private_mail = process.argv[4]
  var doc_id = process.argv[5]
  var owner_email = process.argv[6]
  var owner_name = process.argv[7]
  var subject = process.argv[8]
  var link = process.env.HOST_NAME + "/bla/#/document?id=" + doc_id + "&signer=" + process.argv[2]
  var secret_level = process.argv[9] || 1
  var secret_text = ""
  if (secret_level == 4) { secret_text = "ความลับที่สุด (Top Secret)" }
  else if (secret_level == 3) { secret_text = "ความลับมาก (Secret)" }
  else if (secret_level == 2) { secret_text = "ความลับ (Confidential)" }


  var html2 = ``
  if (private_mail != '') {
    html2 = `<div style="color:#2F80ED">
          <b>Private Message/ข้อความส่วนตัว:</b>
          <br>
          "`+ private_mail + `"
        </div>
         <br><br>`
  } else {
    html2 = ``
  }

  var html_secret = ``
  if (secret_level > 1) {
    html2 = `<div style="font-size:10px;">
          <b>Document Classification/ระดับความลับของเอกสาร :</b>
          "`+ secret_text + `"
        </div>
          <br>`
  } else {
    html_secret = ``
  }

  var html = `<!doctype html>

  <html lang="th-TH" dir="ltr">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
    </head>
    <body style="background: #E0E0E0;padding-bottom: 30px;"><br>
  
      <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 30px 50px 30px;width:60%;border-radius: 5px;">
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="${logo}" alt="logo website" style="width: 300px;margin-top: 30px;">
            </td>
        </tr>
        <tr>
          <td style="width: 100%;color: #000;padding: 0px 30px 30px 30px;margin-top: 30px;">
            <div style="margin-top:30px;font-size: 16px;">
              <label style="font-weight: bold;">Document ID/รหัสเอกสาร :</label>  ${doc_id}
            </div>
            <div style="margin-top:15px;font-size: 16px;">
              <label style="font-weight: bold;">From/จาก :</label> ${owner_name} ( ${owner_email} )
            </div>
            <div style="margin-top:10px;font-size: 16px;">
              <label style="font-weight: bold;">Message/ข้อความ :</label> ${msg}
            </div>
            ${html2}
            ${html_secret}
            </td>
        </tr>
        <tr>
          <td style="display: flex;justify-content: center;">
              <div style="background: #0058A9;width: 100%;display: block;padding-bottom: 100px;">
                  <div style="text-align: center;margin-top: 30px">
                  <img src="`+ process.env.HOST_NAME +`/bla/images/mail/mail_sign.png" style="margin-top: 30px;">
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    Your have document wating for sign
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    คุณมีเอกสารที่ต้องเซ็นรออยู่
                  </div>
                  <div style="text-align: center;margin-top: 50px">
                    <a href="`+ link + `" style="text-decoration: none;color: #000;background: #FFF;padding: 15px 30px;font-size: 16px;">
                    คลิกเพื่อเซ็นหรืออ่านเอกสาร
                  </a>
                  </div>
              </div>
          </td>
        </tr>
        </table>
        </body>
        </html>`


 var smtp = {
    host: 'smtp.gmail.com', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      user: 'credenmailer@gmail.com', //user account
      // pass: 'creden2018' //user password
      pass: 'dnkrakkaxnmnakpr'
    }
  };
//   var smtpTransport = mailer.createTransport(smtp);
//   var mail = {
//     from: 'Bangkok Life - eSinature', //from email (option)
//     to: process.argv[2], //to email (require)
//     cc: '',
//     subject: subject, //subject
//     html: html//head + body + end
//   }

var smtpTransport = mailer.createTransport(smtp);
var mail = {
  from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
  to: process.argv[2], //to email (require)
  cc: process.argv[10],//'chaluemchat@gmail.com,sysnajar@gmail.com'
  subject: 'Please Sign:' + subject, //subject
  html: html//head + body + end
}
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


