#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
local common = require "common"
local bson = require "resty-mongol.bson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { email = arg[1], content = arg[2] ,content_txt = arg[3] }
end

function rand(len)
    local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
    local t = {}
    local cnt = 0
     
    local i = 1
    repeat
        local f = assert (io.popen (cmd, 'r'))
        local line = f:read('*all'):match("%d+")
        f:close()
        cnt = cnt + #line
        table.insert(t, line)
    until cnt>len
    
            
    local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
    return ret
end

function get_timezone_thai (time)
    local format="(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)"
    local day,mon,year,hour,min,sec = tostring(time):match(format)
    local dt = {year=tonumber(year), month=tonumber(mon), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=tonumber(sec)}
    local time2 = os.time(dt)
    local time3 = os.date('%d/%m/%Y %H:%M:%S', tonumber(time2)  + (7 * 60 * 60) )
    return time3
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else

    db = conn:new_db_handle("edoc")
    col = db:get_col("eform")

    html = [[<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
    <style>
        @font-face {
            font-family: ]] .. body.f_family .. [[;
            src: url('./sarabun-webfont-master/fonts/]] .. body.f_family .. [[.ttf');
            font-weight: normal;
            font-style: normal;
        }
        html,body,div {
            font-family: ]] .. body.f_family .. [[;
            font-size: ]] .. body.f_size .. [[px;
        }
        li,p {
            font-size: 1rem;
        }
    </style>
</head>

<body style='max-width:100%;max-height: 100%;'>
    <div style='margin-top:30px'>
    
        <ul style='list-style-type: none;width:100%'>
            <li style='float:left;width:20%;'>
                <img src='https://hjkl.ninja/company_sig/img/garuda.jpg' style='width:100px;margin-left:0px;'>
            </li>
            <li style='float:left;padding-left:170px;padding-top:60px;width:60%'>
                <label style='font-size: 20px;font-weight: 800'>บันทึกข้อความ</label>
            </li>
            <li style='float:left;'>&nbsp;</li>
        </ul>
        <p>&nbsp;</p>
        <p style='margin-top: -20px;padding-left: 60px;'>
            <label style='font-size: 20px;font-weight: 800;'>ส่วนราชการ</label>&nbsp;&nbsp;]] .. body.doc_department .. [[ 
        </p>
    
        <ul style='list-style-type: none;'>
            <li style='width:40%;float:left;margin-top:-10px;padding-left:20px'> <label style='font-size: 20px;font-weight: 800;'>ที่</label>&nbsp;&nbsp;]] .. body.doc_id .. [[</li>
            <li style='width:50%;float:left;margin-top:-10px;padding-left:20px'> <label style='font-size: 20px;font-weight: 800;'>วันที่</label>&nbsp;&nbsp;]] .. body.doc_date .. [[</li>
        </ul>

        <p style='margin-top:40px;padding-left: 60px;'>
            <label style='font-size: 20px;font-weight: 800;'>เรื่อง</label>&nbsp;&nbsp;]] .. body.doc_subject .. [[ 
        </p>

        <p style='margin-top:-10px;padding-left: 60px;'>เรียน&nbsp;&nbsp;]] .. body.doc_to .. [[</p>

        <div style="margin-top:30px;width:100%">
            <p style='padding-left:60px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]] .. body.detail1   .. [[</p>
        </div>
        <div>
            <p style='margin-top:30px;padding-left:60px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]] .. body.detail2   .. [[</p>
        </div>
        <div>
            <p style='margin-top:30px;padding-left:60px;'>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;]] .. body.detail3   .. [[</p>
        </div>
      
        <p style='margin-left:500px;margin-top:100px'>(&nbsp;]] .. body.doc_sign .. [[&nbsp;)</p>
        <p style='margin-left:480px;margin-top:10px'>]] .. body.doc_position .. [[</p>
    </div>
</body>
</html>]]


    name = "eform" .. rand(8) .. os.time()
    -- name = "eform2" 
    local c,err = prog('sh','/home/creden/work/app/server_thaicom/convert_eformhtmltopdf.sh', html, name)
    local date = os.date('%d/%m/%Y %H:%M:%S')

    -- base64 = prog('sh','/home/creden/work/app/server_thaicom/convert_pdftobase64.sh', name)
    -- data_base64 = prog('cat','/home/creden/work/app/client/face/images/card/eform.base64')
    -- data_base64 = data_base64.stdout
    -- content_no_style = replace_br_tags(body.content_no_style)
    -- tax = replace_img_tags(content_no_style)

    xml = [[
        <?xml version="1.0" encoding="UTF-8"?>
            <rsm:TaxInvoice_CrossIndustryInvoice xmlns:rsm="urn:etda:uncefact:data:standard:TaxInvoice_CrossIndustryInvoice:2" xmlns:ram="urn:etda:uncefact:data:standard:TaxInvoice_ReusableAggregateBusinessInformationEntity:2" xmlns:ns3="http://www.w3.org/2000/09/xmldsig#">
            <rsm:ExchangedDocumentContext>
                <ram:GuidelineSpecifiedDocumentContextParameter>
                    <ram:ID schemeAgencyID="ETDA" schemeVersionID="v2.0">ER3-2560</ram:ID>
                </ram:GuidelineSpecifiedDocumentContextParameter>
            </rsm:ExchangedDocumentContext>
            <rsm:ExchangedDocument>
                <OWNER>]] .. body.email .. [[</OWNER>
                <DATE>]] .. get_timezone_thai(date) .. [[</DATE>
                <FONT_FAMILY>]] .. body.f_family .. [[</FONT_FAMILY>
                <FONT_SIZE>]] .. body.f_size .. [[</FONT_SIZE>
                <TYPE_EFORM>]] ..body.type_eform .. [[</TYPE_EFORM>
                <DOCUMENT_DEPARTMENT>]] .. body.doc_department .. [[</DOCUMENT_DEPARTMENT>
                <DOCUMENT_ID>]] .. body.doc_id .. [[</DOCUMENT_ID>
                <DOCUMENT_DATE>]] .. body.doc_date .. [[</DOCUMENT_DATE>
                <DOCUMENT_SUBJECT>]] .. body.doc_subject .. [[</DOCUMENT_SUBJECT>
                <DOCUMENT_TO>]] .. body.doc_to .. [[</DOCUMENT_TO>
                <DOCUMENT_DETAIL1>]] .. body.doc_detail1 .. [[</DOCUMENT_DETAIL1>
                <DOCUMENT_DETAIL2>]] .. body.doc_detail2 .. [[</DOCUMENT_DETAIL2>
                <DOCUMENT_DETAIL3>]] .. body.doc_detail3 .. [[</DOCUMENT_DETAIL3>
                <DOCUMENT_SIGN>]] .. body.doc_sign .. [[</DOCUMENT_SIGN>
                <DOCUMENT_POSITION>]] .. body.doc_position .. [[</DOCUMENT_POSITION>
            </rsm:ExchangedDocument>
            </rsm:TaxInvoice_CrossIndustryInvoice>
    ]]
    
    local x,err = prog('sh','/home/creden/work/app/server/capi/convert_pdftoxml.sh', xml, name)
    -- ngx.sleep(5)

    -- XML block chain
    local ts_file = '/home/creden/work/app/client/face/images/card/' .. name .. '.xml'
    local bc_cmd = 'lua /home/creden/scripts/put_tx.lua '.. name .. ' "' .. ts_file  ..'"' ..' | node /home/creden/scripts/stellar_tx_pub2.js | /home/creden/scripts/rset.lua bc:' .. name .. ' &'
    rc:set('last_bc',bc_cmd)
    os.execute(bc_cmd)
    -- End XML block chain

    ngx.sleep(10)

    local hash =  rc:get('bc:' .. name)
    local new_eform = { 
        filename_pdf = name .. ".pdf", 
        filename_xml = name .. ".xml", 
        owner = body.email,
        createDtm = date,
        hash_xml = hash,
        eform = name,
        font_family = body.f_family,
        font_size = body.f_size,
        type_eform = body.type_eform,
        doc_department = body.doc_department,
        doc_id = body.doc_id,
        doc_date = body.doc_date,
        doc_subject = body.doc_subject,
        doc_to = body.doc_to,
        doc_detail1 = body.doc_detail1,
        doc_detail2 = body.doc_detail2,
        doc_detail3 = body.doc_detail3,
        doc_sign = body.doc_sign,
        doc_position = body.doc_position,
    }
    u,err = col:insert({ new_eform })
    
    -- end



    if c then 
        ret = { success = true, error_msg = "Add eform success", name_pdf = name }
    else
        ret = { success = false, error_msg = "Add eform not found" }
    end


end

print(cjson.encode(ret))
