#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {compCode = 'rads'} 
end

if(body.compCode=='all') then body.compCode = nil end 

--get credit
credit = 0
if(body.compCode) then
    counter_key = 'counter.'..body.compCode..'.tx'
    max_counter = 'counter.'..body.compCode..'.txMax'
    c = rc:get(counter_key)
    m = rc:get(max_counter)
    credit = (tonumber(m) or 0) - (tonumber(c) or 0)
end
function checkNaN(Q1)
    local  x = 0

    if(tostring(Q1) == "nan" or tostring(Q1) == "inf") then
        return 0

    else
        return Q1
    end

  
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    --local month = body.month or 10
    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
    s = col:find({},{cardImg1=0,cardImg2=0,cardImg3=0})


local month = tonumber(body.month)
local oldmonth = tonumber(body.month)-1
local total_sdk = 0
local total_web = 0
local total_api = 0
local total_fail_sdk = 0
local total_fail_web = 0
local total_fail_api = 0
local total_fail_last_sdk = 0
local total_fail_last_web = 0
local total_fail_last_api = 0
local total_fail_year_sdk = 0
local total_fail_year_web = 0
local total_fail_year_api = 0
local total_fail_last_year_sdk = 0
local total_fail_last_year_web = 0
local total_fail_last_year_api = 0
local total_fail_mom_api = 0
local total_fail_mom_web = 0
local total_fail_mom_sdk = 0
local totalyoy_fail_web = 0
local totalyoy_fail_api = 0
local totalyoy_fail_sdk = 0
local total_last_sdk = 0
local total_last_web = 0
local total_last_api = 0
local totalmom_api = 0
local totalmom_web = 0
local totalmom_sdk = 0
local totalyoy_api = 0
local totalyoy_web = 0
local totalyoy_sdk = 0
local wait = 0
local pass = 0
local fail = 0
local last_m_sdk = 0
local last_m_web = 0
local last_m_api = 0
local last_m_wait = 0
local last_m_pass = 0
local last_m_fail = 0
local last_fail = 0
local offyear_wait = 0
local offyear_pass = 0
local offyear_fail = 0
local year_sdk = 0
local year_web = 0
local year_api = 0
local year_wait = 0
local year_pass = 0
local year_fail = 0
local year = os.date("%Y")
local counter = 0
local total_year_sdk = 0
local total_year_web = 0
local total_year_api = 0
local total_last_year_sdk = 0
local total_last_year_web = 0
local total_last_year_api = 0
    for k, v in s:pairs() do
   -- local  mm = "0"   

        if tonumber(v.createdDtm) ~= nil and v.isValidDopa==true then
            v._id = tostring(v._id)
            counter = counter+1
            --ngx.log(ngx.INFO, 'v.createdDtm = ' .. v.createdDtm)
            local dd = os.date("%d",tonumber(v.createdDtm)/1000)
            local mm = os.date("%m",tonumber(v.createdDtm)/1000)
            local yy = os.date("%Y",tonumber(v.createdDtm)/1000)


            if (tonumber(mm) == month and tonumber(yy)==tonumber(year)) then
                -- print(v._id)
                if(v.channel=="sdk") then
                    total_sdk = total_sdk + 1
                end

                if(v.channel=="web") then
                    total_web = total_web + 1
                end

                if(v.channel=="api") then
                    total_api = total_api + 1
                end
            end
            -- MOM - 1 --
            if (oldmonth == tonumber(mm) and tonumber(yy) == tonumber(year)) then
                if(v.channel=="sdk") then
                    total_last_sdk = total_last_sdk + 1
                    -- print(v._id)
                end

                if(v.channel=="web") then
                    total_last_web = total_last_web + 1
                end

                if(v.channel=="api") then
                    total_last_api = total_last_api + 1
                end
            end
            -- end MOM --
            -- yoy - Current --
            if (tonumber(yy) == tonumber(year)) then
                if(v.channel=="sdk") then
                    total_year_sdk = total_year_sdk + 1
                end

                if(v.channel=="web") then
                    total_year_web = total_year_web + 1
                    -- print('web='..v._id..mm..yy)
                end

                if(v.channel=="api") then
                    total_year_api = total_year_api + 1
                end
            end
            -- end yoy --
           -- yoy - last --
            if (tonumber(yy) == tonumber(year)-1) then
                if(v.channel=="sdk") then
                    total_last_year_sdk = total_last_year_sdk + 1
                end

                if(v.channel=="web") then
                    total_last_year_web = total_last_year_web + 1

                end

                if(v.channel=="api") then
                    total_last_year_api = total_last_year_api + 1
                end
            end
            -- end yoy --
        

        elseif tonumber(v.createdDtm) ~= nil and v.isValidDopa == false then
            local dd = os.date("%d",tonumber(v.createdDtm)/1000)
            local mm = os.date("%m",tonumber(v.createdDtm)/1000)
            local yy = os.date("%Y",tonumber(v.createdDtm)/1000)
            -- print(tonumber(mm)..month..yy..year)
            
            if (tonumber(mm) == month and tonumber(yy)==tonumber(year)) then

                if(v.channel=="sdk") then
                    total_fail_sdk = total_fail_sdk + 1
                end

                if(v.channel=="web") then
                   total_fail_web = total_fail_web + 1
                end

                if(v.channel=="api") then
                   total_fail_api = total_fail_api + 1
                end
            end

             -- MOM - 1 --
            if (oldmonth == tonumber(mm) and tonumber(yy) == tonumber(year)) then
                if(v.channel=="sdk") then
                    total_fail_last_sdk = total_fail_last_sdk + 1
                end

                if(v.channel=="web") then
                    total_fail_last_web = total_fail_last_web + 1
                end

                if(v.channel=="api") then
                    total_fail_last_api = total_fail_last_api + 1
                end
            end
            -- end MOM --
            -- yoy - Current --
            if (tonumber(yy) == tonumber(year)) then
                if(v.channel=="sdk") then
                    total_fail_year_sdk = total_fail_year_sdk + 1
                end

                if(v.channel=="web") then
                    total_fail_year_web = total_fail_year_web + 1
                    -- print('web='..v._id..mm..yy)
                end

                if(v.channel=="api") then
                    total_fail_year_api = total_fail_year_api + 1
                end
            end
            -- end yoy --
           -- yoy - last --
            if (tonumber(yy) == tonumber(year)-1) then
                if(v.channel=="sdk") then
                    total_fail_last_year_sdk = total_fail_last_year_sdk + 1
                end

                if(v.channel=="web") then
                    total_fail_last_year_web = total_fail_last_year_web + 1

                end

                if(v.channel=="api") then
                    total_fail_last_year_api = total_fail_last_year_api + 1
                end
            end
            
        end
    end

            if (totalmom_api~=nil and totalmom_web ~= nil and totalmom_sdk ~= nil and total_last_year_sdk ~= nil and total_last_year_api ~= nil and total_last_year_web ~= nil and total_last_year_sdk ~= nil and total_last_year_web ~= nil and total_last_year_api ~= nil) then
                 totalmom_api=((total_api * 100) / total_last_api)-100
                 totalmom_api = checkNaN(totalmom_api)
                 if(tonumber(totalmom_api)~=0) then
                    totalmom_api = math.floor(totalmom_api*100)/100
                 end
                 totalmom_web=((total_web * 100) / total_last_web)-100
                 totalmom_web = checkNaN(totalmom_web)
                 if(tonumber(totalmom_web)~=0) then
                    totalmom_web = math.floor(totalmom_web*100)/100
                 end
                 totalmom_sdk=((total_sdk * 100) / total_last_sdk)-100
                 totalmom_sdk = checkNaN(totalmom_sdk)
                 if(tonumber(totalmom_sdk)~=0) then
                    totalmom_sdk = math.floor(totalmom_sdk*100)/100
                 end
                 -- yoy
                 totalyoy_api=((total_year_api * 100) / total_last_year_api ) * (total_year_api<total_last_year_api and -1 or 1)
                 totalyoy_api = checkNaN(totalyoy_api)
                 if(tonumber(totalyoy_api)~=0) then
                    totalyoy_api = math.floor(totalyoy_api*100)/100
                 end

                 totalyoy_sdk=((total_year_sdk * 100) / total_last_year_sdk ) * (total_year_sdk<total_last_year_sdk and -1 or 1)
                 totalyoy_sdk = checkNaN(totalyoy_sdk)
                 if(tonumber(totalyoy_sdk)~=0) then
                    totalyoy_sdk = math.floor(totalyoy_sdk*100)/100
                 end

                 totalyoy_web=((total_year_web * 100) / total_last_year_web ) * (total_year_web<total_last_year_web and -1 or 1)
                 totalyoy_web = checkNaN(totalyoy_web)
                 if(tonumber(totalyoy_web)~=0) then
                    totalyoy_web = math.floor(totalyoy_web*100)/100
                 end
                 -- end YOY

                 --fail mom
                 total_fail_mom_api=((total_fail_api * 100) / total_fail_last_api)-100
                 total_fail_mom_api = checkNaN(total_fail_mom_api)
                 if(tonumber(total_fail_mom_api)~=0) then
                    total_fail_mom_api = math.floor(total_fail_mom_api*100)/100
                 end
                 total_fail_mom_web=((total_fail_web * 100) / total_fail_last_web)-100
                 total_fail_mom_web = checkNaN(total_fail_mom_web)
                 if(tonumber(total_fail_mom_web)~=0) then
                    total_fail_mom_web = math.floor(total_fail_mom_web*100)/100
                 end
                 total_fail_mom_sdk=((total_fail_sdk * 100) / total_fail_last_sdk)-100
                 total_fail_mom_sdk = checkNaN(total_fail_mom_sdk)
                 if(tonumber(total_fail_mom_sdk)~=0) then
                    total_fail_mom_sdk = math.floor(total_fail_mom_sdk*100)/100
                 end
                 -- print("total_fail_mom_sdk="..total_fail_api.."&total_fail_last_api="..total_fail_last_api)
                 --end mom
                 
                 -- fail yoy
                 totalyoy_fail_api=((total_fail_year_api * 100) / total_fail_last_year_api ) * (total_fail_year_api<total_fail_last_year_api and -1 or 1)
                 totalyoy_fail_api = checkNaN(totalyoy_fail_api)
                 if(tonumber(totalyoy_fail_api)~=0) then
                    totalyoy_fail_api = math.floor(totalyoy_fail_api*100)/100
                 end

                 totalyoy_fail_sdk=((total_fail_year_sdk * 100) / total_fail_last_year_sdk ) * (total_fail_year_sdk<total_fail_last_year_sdk and -1 or 1)
                 totalyoy_fail_sdk = checkNaN(totalyoy_fail_sdk)
                 if(tonumber(totalyoy_fail_sdk)~=0) then
                    totalyoy_fail_sdk = math.floor(totalyoy_fail_sdk*100)/100
                 end

                 totalyoy_fail_web=((total_fail_year_web * 100) / total_fail_last_year_sdk ) * (total_fail_year_web<total_fail_last_year_sdk and -1 or 1)
                 totalyoy_fail_web = checkNaN(totalyoy_fail_web)
                 -- print(totalyoy_fail_web)
                 if(tonumber(totalyoy_fail_web)~=0) then
                    totalyoy_fail_web = math.floor(totalyoy_fail_web*100)/100
                 end
                 -- end YOY
            end
            
            -- print("&totalyoy_fail_api="..totalyoy_fail_api.."&totalyoy_fail_web="..total_fail_year_web.."&totalyoy_fail_sdk="..totalyoy_fail_sdk)
            -- print("&total_fail_mom_api="..total_fail_mom_api.."&total_fail_mom_web="..total_fail_mom_web.."&total_fail_mom_sdk="..total_fail_mom_sdk)
            
            -- print("&total_fail_year_api="..total_fail_year_api.."&total_fail_year_web="..total_fail_year_web.."&total_fail_year_sdk="..total_fail_year_sdk)
            -- print("&total_fail_last_api="..total_fail_last_api.."&total_fail_last_web="..total_fail_last_year_web.."&total_fail_last_sdk="..total_fail_last_sdk)
            -- print("&total_fail_last_year_api="..total_fail_last_year_api.."&total_fail_last_year_web="..total_fail_last_year_web.."&total_fail_last_year_sdk="..total_fail_last_year_sdk)
            -- print("&totalyoy_api="..totalyoy_api.."&totalyoy_sdk="..totalyoy_sdk.."&totalyoy_web="..totalyoy_web)
            -- print("&total_year_sdk="..total_year_sdk.."&total_year_api="..total_year_api.."&total_year_web="..total_year_web)
            -- print("&totalmom_api="..totalmom_api.."&totalmom_web="..totalmom_web.."&totalmom_sdk="..totalmom_sdk)
            -- print("&sdk="..total_sdk.."&web="..total_web.."&api="..total_api)
            -- print("&failsdk="..total_fail_sdk.."&failweb="..total_fail_web.."&failapi="..total_fail_api)
            -- print("&total_last_api="..total_last_api.."&total_last_web="..total_last_web.."&total_last_sdk="..total_last_sdk)
    ret = {success = true,wait=wait, error_msg = "complete", data = {api=total_api,sdk=total_sdk,web=total_web,total_last_sdk=total_last_sdk,total_last_web=total_last_web,total_last_api=total_last_api,total_year_sdk=total_year_sdk,total_year_api=total_year_api,total_year_web=total_year_web,total_fail_sdk=total_fail_sdk,total_fail_web=total_fail_web,total_fail_api=total_fail_api,total_fail_mom_api=total_fail_mom_api,total_fail_mom_web=total_fail_mom_web,total_fail_mom_sdk=total_fail_mom_sdk,totalyoy_fail_api=totalyoy_fail_api,totalyoy_fail_web=totalyoy_fail_web,totalyoy_fail_sdk=totalyoy_fail_sdk} }

end
print(cjson.encode(ret))
