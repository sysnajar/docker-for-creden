#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')

prog.timeout = 1000 * 300
prog.timeout_fatal = false
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   -- body  = {card_no = arg[1],firstname = arg[2],lastname = arg[3]} 

end
-- local i = 0
local propetry = {}
local cal_x1 = 0
local cal_x2 = 0
local rex = nil

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

function ParseCSVLine (line,sep) 
  local res = {}
  local pos = 1
  sep = sep or ','
  while true do 
    local c = string.sub(line,pos,pos)
    if (c == "") then break end
    if (c == '"') then
      -- quoted value (ignore separator within)
      local txt = ""
      repeat
        local startp,endp = string.find(line,'^%b""',pos)
        txt = txt..string.sub(line,startp+1,endp-1)
        pos = endp + 1
        c = string.sub(line,pos,pos) 
        if (c == '"') then txt = txt..'"' end 
      until (c ~= '"')
      table.insert(res,txt)
      assert(c == sep or c == "")
      pos = pos + 1
    else  
      -- no quotes used, just look for the first separator
      local startp,endp = string.find(line,sep,pos)
      if (startp) then 
        table.insert(res,string.sub(line,pos,startp-1))
        pos = endp + 1
      else
        -- no separator found -> use rest of string and terminate
        table.insert(res,string.sub(line,pos))
        break
      end 
    end
  end
  return res
end

function fiter_num(num)
  num = tonumber(num)
  if(num == nil)then
    num = 0
  end

  return num
end

local count_colum1 = 0
local count_colum2 = 0
local count_colum3 = 0
local count_colum4 = 0
local count_row = 0
local count = 1
local ret = {}
local count_colum = {}

for line in io.stdin:lines() do

  count = count + 1
  
  line = trim5(line)
  line = ParseCSVLine(line)
  
  for u,x in ipairs(line)do
    -- print(x)
    
    count_colum[u] =  fiter_num(count_colum[u]) + fiter_num(x)

  end

  -- count_colum1 = count_colum1 + fiter_num(line[1])
  -- count_colum2 = count_colum2 + fiter_num(line[2])
  -- count_colum3 = count_colum3 + fiter_num(line[3])
  -- count_colum4 = count_colum4 + fiter_num(line[4])
   for i,v in ipairs(line)do
      count_row = count_row + fiter_num(v)
   end
print(cjson.encode(line),'count_row',count_row)

count_row = 0

end
print(cjson.encode(count_colum))

