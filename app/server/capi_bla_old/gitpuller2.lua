#! /usr/bin/env lua
local redis = require 'redis'
local client = redis.connect()
local rc2 = redis.connect()

--client:select(9)
local channels = { 'git' }


for msg, abort in client:pubsub({ subscribe = channels }) do
    if msg.kind == 'subscribe' then
        print('Subscribed to channel '..msg.channel)
    elseif msg.kind == 'message' then
        if msg.channel == 'git' then
            if msg.payload == 'pull' then
                print('got pull command')
				--os.execute("/home/creden/work/app/server/capi/gitpull.sh")
				local file = assert(io.popen('/home/creden/work/app/server/capi/gitpull.sh 2>&1', 'r'))
				local output = file:read('*all')
				file:close()
                print('pull done output = ' .. output)

				rc2:set('git.result', output)
            else
                print('Received an unrecognized command: '..msg.payload)
            end
        else
            print('Received the following message from '..msg.channel.."\n  "..msg.payload.."\n")
        end
    end
end

