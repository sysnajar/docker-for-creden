#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

local bson = require "resty-mongol.bson" 
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
--------
function fromCSV (s)
  s = s .. ','        -- ending comma
  local t = {}        -- table to collect fields
  local fieldstart = 1
  repeat
    -- next field is quoted? (start with `"'?)
    if string.find(s, '^"', fieldstart) then
      local a, c
      local i  = fieldstart
      repeat
        -- find closing quote
        a, i, c = string.find(s, '"("?)', i+1)
      until c ~= '"'    -- quote not followed by quote?
      if not i then error('unmatched "') end
      local f = string.sub(s, fieldstart+1, i-1)
      table.insert(t, (string.gsub(f, '""', '"')))
      fieldstart = string.find(s, ',', i) + 1
    else                -- unquoted; find next comma
      local nexti = string.find(s, ',', fieldstart)
      table.insert(t, string.sub(s, fieldstart, nexti-1))
      fieldstart = nexti + 1
    end
  until fieldstart > string.len(s)
  return t
end
function escapeCSV (s)
  if string.find(s, '[,"]') then
    s = '"' .. string.gsub(s, '"', '""') .. '"'
  end
  return s
end
function toCSV (tt)
  local s = ""
-- ChM 23.02.2014: changed pairs to ipairs 
-- assumption is that fromCSV and toCSV maintain data as ordered array
  for _,p in ipairs(tt) do  
    s = s .. "," .. escapeCSV(p)
  end
  return string.sub(s, 2)      -- remove first comma
end

-------

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    -- body = cjson.decode(ngx.req.get_body_data())
    body = ngx.req.get_uri_args()
else
   body   = {}
end


conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)


if not ok then
    ret = {success = false, error_msg = err}
else

local d1_month = body.fake_d1_month 
local d1_day = body.fake_d1_day 
local d1_year = body.fake_d1_year 
local d2_month = body.fake_d2_month 
local d2_day = body.fake_d2_day 
local d2_year = body.fake_d2_year  
local usrname = body.getname 
db = conn:new_db_handle("dopa")
col2 = db:get_col("ekyc");

 local user_list = col2:find({txid=body.txid,username=body.getname})
 local new_list = {}
 ngx.header["Content-Type"] = 'application/octet-stream';
 ngx.header["Content-Disposition"] =  'form-data; name="myFile"; filename="report.csv"';
 print(', , , , ')
 print('Transaction ID,cardNo,name,lastnam,Create On, Status')
    for i, v in user_list:pairs() do 
        v._id = nil
     
      
        if (v.createdDtm ~= nil and v.createdDtm ~= nil v.status~=nil) then
        local create = os.date("%d/%m/%Y", tonumber(v.createdDtm)/1000)
        local complete2 = os.date("%d/%m/%Y",tonumber(v.createdDtm)/1000) 
          local v2 = {v.txid or v.id,v.cardNo,v.fnameTH,v.lnameTH,create,v.status}
          
          local csv = toCSV(v2)
        print(csv)
      end
    end


end




