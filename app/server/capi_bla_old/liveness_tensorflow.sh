#!/bin/sh
cd /home/creden/work/app/liveness-detection-ml
python3 liveness_demo.py --model liveness.model --le le.pickle --detector face_detector --input $1 --txid $2
