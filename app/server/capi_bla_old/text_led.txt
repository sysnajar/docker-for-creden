
<tbody><tr><td>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr>
</tr></tbody></table>
</td><td></td></tr><tr><td>
<table class="tableLayout" border="1" cellpadding="0" cellspacing="0" width="100%">
<tbody><tr class="tableLayoutRow"><td class="tableLayoutCell" width="100%">
<div style="" class="tableContainer" id="tableContainer_lkPccBankruptTable">
<table class="table" border="0" cellpadding="0" cellspacing="1" width="100%">
 <thead class="fixedHeader" id="fixedHeader_lkPccBankruptTable">
  <tr class="tableHeaderRow">
    <th class="tableHeaderLabel" rowspan="2">รายที่</th>

<th class="tableHeaderCell" rowspan="2" id="lkPccBankruptTableColumnfullName"><span class="tableHeaderLabel">ชื่อที่ตรวจสอบพบ</span></th>

<th class="tableHeaderCell" rowspan="2" id="lkPccBankruptTableColumnfullRecvCase"><span class="tableHeaderLabel">เรื่องที่</span></th>

<th class="tableHeaderCell" rowspan="2" id="lkPccBankruptTableColumncourtName"><span class="tableHeaderLabel">ศาล</span></th>
    <td class="tableHeaderCell" colspan="2"><label>คดีหมายเลข</label></td>
  </tr>
  <tr class="tableHeaderRow">

<th class="tableHeaderCell" id="lkPccBankruptTableColumnblackCaseNo"><span class="tableHeaderLabel">ดำที่</span></th>

<th class="tableHeaderCell" id="lkPccBankruptTableColumnredCaseNo"><span class="tableHeaderLabel">แดงที่</span></th>
  </tr>
 </thead>
<input type="hidden" name="lkPccBankruptTable_ROWCOUNT" value="1">
 <tbody class="scrollContent" id="scrollContent_lkPccBankruptTable">
  <tr class="tableRowOdd">
    <td class="tableIndexCell" align="center" style="cursor: default; "><span>1</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_1">ทะเบียนนิติบุคคลเลขที่ </span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_2">ศาลล้มละลายกลางได้มีคําพิพากษา</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_3">รัชดา</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_4">1</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_5">2</span></td>
</tr>
<tr class="tableEmptyCell">
 <td class="tableIndexCell" align="center" style="cursor: default; "><span>2</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_1">ทะเบียนนิติบุคคลเลขที่ </span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_2">ศาลล้มละลายกลางได้มีคําพิพากษา ครั้งที่สอง</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_3">รัชดา</span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_4"></span></td>
    <td class="tableCell" style="cursor: default; "><span id="lkPccBankruptTable_data_1_5">2</span></td>
</tr>
<tr class="tableEmptyCell"><td class="tableCell">&nbsp;</td><td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
</tr>
<tr class="tableEmptyCell"><td class="tableCell">&nbsp;</td><td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
</tr>
<tr class="tableEmptyCell"><td class="tableCell">&nbsp;</td><td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
<td class="tableCell">&nbsp;</td>
</tr>
 </tbody></table>
</div></td></tr></tbody></table>
</td><td valign="top">
</td>
</tr>
</tbody>
