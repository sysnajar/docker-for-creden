#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'

function init()
if(#arg > 0  )then
  mongo = require "resty-mongol"
  prog = exec.new('/tmp/exec_bus.sock')
  prog.timeout_fatal = false
  conn = mongo()
  conn:set_timeout(1000)
  ok, err = conn:connect('127.0.0.1',27017)
  db = conn:new_db_handle("test")
  users = db:get_col("user") 
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(email)
	local t  = users:find_one({email = email, activated = 1})
	local ret = {} 
	
	local res, err = prog('curl', '-X', 'POST', '-d' , 'fin=false&firstname=' .. t.firstname ..'&lastname=' .. t.lastname , 'http://sm.hjkl.ninja:8124/xapi/api_dbd')

	if not err then
		local tmp = cjson.decode(res.stdout) --try decoding
		ret = (type(tmp.success) == 'boolean') and tmp or {success = false, error_msg = 'Invalid data from api_dbd:\n' .. res.stdout }
	else
		ret = {success = false, error_msg = err}
    end

return ret
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
