var mailer = require("nodemailer");
send_mail()


function send_mail() {
  process.env.HOST_NAME = "https://sign.bangkoklife.com/bla/"
  var logo = process.env.HOST_NAME + "images/bla_logo_th.png"
  var signed_icon = process.env.HOST_NAME + "images/mail/signed-icon.png"
  var btn_bla = process.env.HOST_NAME + "images/btn_bla.png"	
  //var logo = "https://www.bangkoklife.com/assets/images/bla_logo_2021_en.svg"
  var bg = "#1667B2"
  var wTable = "100%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "web/policy.html"
  var msg = process.argv[3]
  var private_mail = process.argv[4]
  var doc_id = process.argv[5]
  var owner_email = process.argv[6]
  var owner_name = process.argv[7]
  var subject = process.argv[8]
  var link = process.env.HOST_NAME + "#/document?id=" + doc_id + "&signer=" + process.argv[2]
  var secret_level = process.argv[9] || 1
  var secret_text = ""
  if (secret_level == 4) { secret_text = "ความลับที่สุด (Top Secret)" }
  else if (secret_level == 3) { secret_text = "ความลับมาก (Secret)" }
  else if (secret_level == 2) { secret_text = "ความลับ (Confidential)" }
  
 var html2 = ``
  if (private_mail != '') {
    html2 = `
    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
      <strong>Private Message/ข้อความส่วนตัว : </strong>
      `+ private_mail + `
    </p>
    `
  } else {
    html2 = ``
  }

  var html_secret = ``
  if (secret_level > 1) {
    html_secret = `
    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;">
      <strong>Document Classification/ระดับความลับของเอกสาร : </strong>
      `+ secret_text + `
    </p>
    `
  } else {
    html_secret = ``
  }	
  var html = `
  <!doctype html>
<html>
  <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Your have document waiting for sign</title>
    <style>
@media only screen and (max-width: 620px) {
  table.body h1 {
    font-size: 28px !important;
    margin-bottom: 10px !important;
  }

  table.body p,
table.body ul,
table.body ol,
table.body td,
table.body span,
table.body a {
    font-size: 16px !important;
  }

  table.body .wrapper,
table.body .article {
    padding: 10px !important;
  }

  table.body .content {
    padding: 0 !important;
  }

  table.body .container {
    padding: 0 !important;
    width: 100% !important;
  }

  table.body .main {
    border-left-width: 0 !important;
    border-radius: 0 !important;
    border-right-width: 0 !important;
  }

  table.body .btn a {
    width: 100% !important;
  }

  table.body .img-responsive {
    height: auto !important;
    max-width: 100% !important;
    width: auto !important;
  }
}
@media all {
  .ExternalClass {
    width: 100%;
  }

  .ExternalClass,
.ExternalClass p,
.ExternalClass span,
.ExternalClass font,
.ExternalClass td,
.ExternalClass div {
    line-height: 100%;
  }

  .apple-link a {
    color: inherit !important;
    font-family: inherit !important;
    font-size: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
    text-decoration: none !important;
  }

  #MessageViewBody a {
    color: inherit;
    text-decoration: none;
    font-size: inherit;
    font-family: inherit;
    font-weight: inherit;
    line-height: inherit;
  }

  .wrapper-btn table td:hover {
    background-color: #4fadea !important;
  }

  .wrapper-btn a:hover {
    background-color: #4fadea !important;
    border-color: #4fadea !important;
    color: #ffffff !important;
  }
}
</style>
  </head>
  <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
    <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">Your have document waiting for sign / คุณมีเอกสารที่รอการลงนาม</span>
    <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: rgb(36,86,164); background: linear-gradient(180deg, rgba(36,86,164,1) 0%, rgba(36,86,164,1) 30%, rgba(224,224,224,1) 30%, rgba(224,224,224,1) 100%); width: 100%;" width="100%">
      <tr>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
        <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; max-width: 580px; padding: 10px; width: 580px; margin: 0 auto;" width="580" valign="top">
          <div class="content" style="box-sizing: border-box; display: block; margin: 0 auto; max-width: 580px; padding: 10px;">

            <!-- START CENTERED WHITE CONTAINER -->
            <table role="presentation" class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; background: #ffffff; border-radius: 15px; width: 100%;" width="100%">

              <!-- START MAIN CONTENT AREA -->
              <tr>
                <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px 50px 50px;" valign="top">
                  <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;" width="100%">
                    <tr>
                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">
                        <p class="logo" style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px; text-align: center;"><img src="`+ logo +`" alt="Bla Logo" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%; width: 300px; margin-top: 30px;" width="300"></p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>ID/รหัสเอกสาร : </strong> ${doc_id}</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>From/จาก :</strong> ${owner_name} ( ${owner_email} )</p>
                        <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong>Message/ข้อความ :</strong> ${msg}</p>
                        ${html2}
                        ${html_secret}
                        <table role="presentation" border="0" cellpadding="0" cellspacing="0" class="btn wrapper-btn" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; box-sizing: border-box; width: 100%; background-color: #0058A9; color: #ffffff; padding: 50px 20px;" width="100%" bgcolor="#0058A9">
                          <tbody>
                            <tr class="space" align="center">
                              <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top"> 
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><img src="`+ signed_icon +`" alt="Bla Logo" style="border: none; -ms-interpolation-mode: bicubic; max-width: 100%; width: 200px;" width="200"></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong style="font-size: 18px;">Your have document waiting for sign</strong></p>
                                <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; margin-bottom: 15px;"><strong style="font-size: 18px;">คุณมีเอกสารที่รอการลงนาม</strong></p>
                              </td>
                            </tr>
                            <tr>
                              <td align="center" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;" valign="top">
                                <table role="presentation" border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto; margin-bottom: 15px;">
                                  <tbody>
                                    <tr>
                                      <td style="font-family: sans-serif; font-size: 14px; vertical-align: top; border-radius: 5px; text-align: center; background-color: #ffffff;" valign="top" align="center" bgcolor="#ffffff"> <a href="`+ link +`" target="_blank" style="border: solid 1px #3498db; border-radius: 5px; box-sizing: border-box; cursor: pointer; display: inline-block; font-size: 14px; font-weight: bold; margin: 0; padding: 12px 25px; text-decoration: none; text-transform: capitalize; background-color: #ffffff; border-color: #ffffff; color: #000000;">คลิกเพื่อเซ็นหรืออ่านเอกสาร</a> </td>
                                    </tr>
                                  </tbody>
                                </table>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
              </tr>

            <!-- END MAIN CONTENT AREA -->
            </table>
            <!-- END CENTERED WHITE CONTAINER -->
          </div>
        </td>
        <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;" valign="top">&nbsp;</td>
      </tr>
    </table>
  </body>
</html>`		


   var smtp = {
      host: 'smtp.bla.co.th',
      port: 25,
      auth: {
          user: 'sign@bangkoklife.com',
          pass: 'P@ssw0rd'
            },
      tls:  {
            rejectUnauthorized: false
            }
  };	
	
	

  // var smtpTransport = mailer.createTransport(smtp);
  // console.log(process.argv[2]);
  // var mail = {
  //   from: 'Bangkok Life Smart Sign <sign@bangkoklife.com>',  
  //   to: process.argv[2], //to email (require)
  //   cc: process.argv[10],
  //   subject:'Please Sign:'+ subject, //subject
  //   html: html//head + body + end
  // }
  // smtpTransport.sendMail(mail, function (error, response) {
  //   smtpTransport.close();
  //   if (error) {
  //     //error handler
  //     var res = { "success": false, "message": error }
  //     console.log(res);
  //   } else {
  //     //success handler 
  //     var res = { "success": true }
  //     console.log(res);
  //   }
  //   process.exit();
  // });

  // var smtp = {
  //   host: 'smtp.gmail.com', //set to your host name or ip
  //   port: 465, //25, 465, 587 depend on your 
  //   auth: {
  //     user: 'credenmailer@gmail.com', //user account
  //     // pass: 'creden2018' //user password
  //     pass: 'dnkrakkaxnmnakpr'
  //   }
  // };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'Bangkok Life Smart Sign <sign@bangkoklife.com>', //from email (option)
    to: process.argv[2], //to email (require)
    cc: process.argv[10],
    subject:'Please Sign:'+ subject, //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


