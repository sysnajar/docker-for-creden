#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local template = require "resty.template"      -- OR
local template = require "resty.template.safe" -- return nil, err on errors
local object_id = require "resty-mongol.object_id"

local session = require "resty.session".open({secret = "4321"})
local output
local output2

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

if session.data.username then
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    local db_dopa = conn:new_db_handle("dopa")

    local user_company = db_dopa:get_col("company_members")

    local u, err_u = user_company:find_one({email = session.data.username})
   
    if u then
        local col_company = db_dopa:get_col("user_company")
        usr_com,err = col_company:find_one({_id=object_id.new(convertHexStringToNormal(tostring(u.company_id)))})
        if usr_com.template ~= nil then
            
        output = template.process ("ekyc/css/index_template.css", { CI_HEADER = usr_com.template.CI_BG,CI_COLOR = usr_com.template.CI_HEADER,CI_CIRCLE_ACTIVE = usr_com.template.CI_BG,CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER=usr_com.template.CI_HEADER,CI_BUTTON = usr_com.template.CI_BG,CI_TEXT=usr_com.template.CI_HEADER })
        output2 = template.process ("ekyc/css/index2_template.css", {  CI_HEADER = usr_com.template.CI_BG,CI_COLOR = usr_com.template.CI_HEADER,CI_CIRCLE_ACTIVE =usr_com.template.CI_BG,CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER=usr_com.template.CI_HEADER,CI_BUTTON = usr_com.template.CI_BG,CI_TEXT=usr_com.template.CI_HEADER })
        end
    else
        output = template.process ("ekyc/css/index_template.css", { CI_HEADER = "#222222",CI_COLOR = "#F05522",CI_CIRCLE_ACTIVE = "#222222",CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER="#F05522",CI_BUTTON = "#222222",CI_TEXT="#F05522" })
        output2 = template.process ("ekyc/css/index2_template.css", {  CI_HEADER = "#222222",CI_COLOR = "#F05522",CI_CIRCLE_ACTIVE = "#222222",CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER="#F05522",CI_BUTTON = "#222222",CI_TEXT="#F05522" })
    end
    
else
    output = template.process ("ekyc/css/index_template.css", { CI_HEADER = "#222222",CI_COLOR = "#F05522",CI_CIRCLE_ACTIVE = "#222222",CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER="#F05522",CI_BUTTON = "#222222",CI_TEXT="#F05522" })
    output2 = template.process ("ekyc/css/index2_template.css", {  CI_HEADER = "#222222",CI_COLOR = "#F05522",CI_CIRCLE_ACTIVE = "#222222",CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER="#F05522",CI_BUTTON = "#222222",CI_TEXT="#F05522" })
end


-- local output = template.process ("ekyc/css/index_template.css", { CI_HEADER = "#222222",CI_COLOR = "#F05522",CI_CIRCLE_ACTIVE = "#222222",CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER="#F05522",CI_BUTTON = "#222222",CI_TEXT="#F05522" })
-- local output2 = template.process ("ekyc/css/index2_template.css", {  CI_HEADER = "#222222",CI_COLOR = "#F05522",CI_CIRCLE_ACTIVE = "#222222",CI_IN_CIRCLE_ACTIVE = "#ffffff",CI_NUMBER="#F05522",CI_BUTTON = "#222222",CI_TEXT="#F05522" })
ngx.say(output,output2)