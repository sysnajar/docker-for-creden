#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local body = nil

function convertHexStringToNormal( str )
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body  = ngx.req.get_post_args()
    --body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	-- data={}
	db = conn:new_db_handle("dopa")
	local col_name = 'company_members'
	col = db:get_col(col_name)
  
    -- CHECK SESSION
    if not session.data.company_id then
        ret.error_msg = 'Invalid Data'
        print(cjson.encode(ret))
        return 0
    else
        body.company_id = session.data.company_id
    end

  if(not body.company_id) then
        ret = {success = false, error_msg = "Invalid company_id"}
  else
        company_id = object_id.new(convertHexStringToNormal(body.company_id))
    	s  = col:find({company_id = company_id})
		id_list = {} 

	    for k, v in s:pairs() do 
		    --table.insert(id_list,v._id)
		    table.insert(id_list,v.esig_user_id)
		--	print(tostring(v._id))
		end

        query = {_id = {['$in']  = id_list}}
        --query = {_id = id_list[1]}
	    col = db:get_col('esig_user')
    	s2  = col:find(query)
		list = {} 

	    for k, v in s2:pairs() do 
		    table.insert(list, {value = tostring(v._id) , 
			label = tostring(v.adminFisrtname) ..' (' .. tostring(v.adminEmail) ..')' ,
			name  = tostring(v.adminFisrtname) ,
			email = v.adminEmail,
			img = v.profile_pic and ('../face/images/card/'..v.profile_pic..'.png') or 'https://hjkl.ninja/company/img/Poster_creden-01.jpg'
		   })
		end
       ret = {success = true, data = list}
         
  end	
    print(cjson.encode(ret))
end
