#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    print = ngx.say
	 body = ngx.req.get_uri_args()
else
     body  = {compCode = 'all'} 
end

conn = mongo()
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    if(body.compCode == 'all')then body.compCode =  nil end  
	local month = body.month or '04'

    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
  	local mdy = {compCode = body.compCode, createDate={['$regex']=month.."/\\d\\d/19"}}
 	t  = col:find(mdy)
 	count=0
	wait = 0
	total = 0
    
	
	list = {}
	for i=1,31 do
		local key = tostring(i)
		local val = {total = 0, wait = 0, pass = 0, fail = 0,web = 0}
        list[key] =  val
	end

 	for i,v in t:pairs() do
		local date  = tonumber(v.createDate:sub(4,5))
	    if(date)then
		   local key = tostring(date)
           local val = list[key]
           if(v.isValid==true) then
		   val.pass  = val.pass + 1
		   
           elseif(v.isValid==false) then
		   val.fail  = val.fail + 1	
           else
		   val.pass  = val.pass + ((v.status=='pass') and 1 or 0)	
		   val.fail  = val.fail + ((v.status=='fail') and 1 or 0)	
		   end
		   val.wait  = val.wait + ((v.status=='wait') and 1 or 0)	
		   val.total = val.total+1 


		   if(v.status=='wait')then wait = wait +1 end
		   total = total + 1


		end

 	end --for

end


ret = {success = true, error_msg = "complete", data = list }
print(cjson.encode(ret))

