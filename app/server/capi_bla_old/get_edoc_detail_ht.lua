#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
local bson = require "resty-mongol.bson"
common = require "common"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
redis = require 'redis'

function do_blockchain(u)
	local bc_tx = rc:get('bc:' .. tostring(u.id))	
	if(bc_tx and bc_tx~='')then
	  u.bc_url = 'https://steexp.com/tx/' .. bc_tx
	  u.bc_tx = bc_tx
	else
	  u.bc_url = false
	end
end


rc = redis.connect('127.0.0.1', 6379)
print = ngx.say
body = ngx.req.get_uri_args()
local can_read = false
local myusername  = session.data.username
--prevent email param
--[[
if(not session.data.username ) then
	print(cjson.encode({success = false, error_msg = "session expired"}))
	ngx.exit(ngx.HTTP_OK)
end	
]]
--prevent email param


ctx   = ctx or common.mongo_ctx()
col = ctx.col["edoc_list"]
u, err = col:find_one({id=body.id})


if(not u)then
ngx.log(ngx.NOTICE,"CREATE NEW DO +++++C ")
--  create document
--prefix = "owat"
prefix = "hatari"
owner = prefix.."1@creden.co"
id =  body.id -- tostring(os.time())
local template = {txid = "hatari"..id, default_field_values = {}, send_email = true,
imageFiles = {
{aspectRatio = 0.7066704675028507, fileName = "pdf294603911565168079-0.png" } ,
{aspectRatio = 0.7066704675028507, fileName = "pdf294603911565168079-0.png" } 
}
}

--db = conn:new_db_handle("edoc")
--col = db:get_col("edoc_list")


template._id = nil
template.is_template = nil
template.is_hatari = true
template.state = 1
--template.company_id = company_id
template.originalFile = "excel"
template.fields     = {}
template.signers    = {}

--gen new document id
template.id = id
--create document
template.createdDtm = bson.get_utc_date(os.time() * 1000)
template.pdfFile = template.id
template.owner = owner
 owner = prefix.."1@creden.co"

local field_positions = { 
{xp = 27.25, yp = 15.30},
{xp = 63.25, yp = 15.33},
{xp = 39.75, yp = 30.5},
{xp = 20.58, yp = 30.5},
{xp = 63.7, yp = 51.78},
{xp = 22.56, yp = 82.83},
{xp = 49.12, yp = 82.83},
{xp = 77.23, yp = 82.83} 
}

-- add fields info, if required
for i=1,4  do
   local v = {}
   v.signOrder = i
   v.id = i
   v.toolName = 'Signature'
   v.toolData = 'Signature'
   v.field_no = 'field'..i
   v.status = 0
   
   if i > 2 then 
     v.page = 1
   else
     v.page = 0
	end

   --    v.page = 0
	   v.xp = 1
	   v.yp = 1
	   v.scale = 100 
	   v.type = 'draw'
	   v.n = tostring(i)
	   v.email = prefix .. i .. '@creden.co'
	   v.xp = field_positions[i].xp
	   v.yp = field_positions[i].yp
	   table.insert(template.fields , v)
	   local s = {no = i, email = v.email , name = "Hatari" .. i }
	   table.insert(template.signers , s)
	end

    local create, err_create = col:insert({template})
--end create
u, err = col:find_one({id=body.id})
end



			if(u.owner == myusername) then can_read = true end
            if body.owner then
                common.activity_history(body.id ,body.owner, 'View', 'ดูเอกสาร', body.owner..' view document '.. u.originalFile, body.owner..' ดูเอกสาร '..u.originalFile, 'View', u.originalFile, nil, nil, nil, nil)
            end
            -- check use_sign_order flag
            if(u.use_signing_order == nil) then
              u.use_signing_order = false
              u.current_order = 1
            end  
            --


                u._id = nil
                u._company = nil
				u.time = (u.createdDtm) and (os.date('%d/%m/%Y %H:%M:%S', tonumber(u.createdDtm)/1000  + (7 * 60 * 60) )) or 'N/A'
				do_blockchain(u) 

                local col2 = ctx.col["dopa.esig_user"]             
                local user_data = col2:find_one({adminEmail = u.owner})   
                u.owner_name = user_data.adminFisrtname or ''

				--owner check
				if(body.owner) then	
                      local deleted_docs = ctx.col['deleted_doc']:find_one({email = "sysnajar@gmail.com", id = tonumber(body.id)})
					  if(deleted_docs) then
                          u.is_deleted = true
						  u.deleted_count = #deleted_docs
					  else
						  u.is_deleted = false
					  end
					  
				    end
				--owner check
				--footer
				
			   local map_pos_signers = {}

				--read permission check
				for k, signer in ipairs(u.signers) do
                    if(signer.email and signer.email ~='') then 
						-- specified email address
						if(signer.email==myusername) then can_read = true end 
					else
						-- specified only position_id
						if(signer.position_id and signer.position_id~='') then
					        signer.people_in_position = common.edoc_get_signers_by_position(signer.position_id)
							map_pos_signers[signer.position_id] = signer.people_in_position
						end
					end
				end


                for k, v in ipairs(u.fields) do 
                    if v.time and v.time ~= '' and v.time ~= 0 then
                        local format="(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)"
                        local mon,day,year,hour,min,sec = tostring(v.time):match(format)
                        local dt = {year=tonumber('20'..year), month=tonumber(mon), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=tonumber(sec)}
                        v.time2 = os.time(dt)
                        v.time3 = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.time2)  + (7 * 60 * 60) )
                    end

					v.people_in_position = map_pos_signers[v.position_id] 
					if((not can_read) and v.people_in_position)then
                       for _,p2 in ipairs(v.people_in_position) do if(p2.email==myusername) then can_read = true end end
					end
					
                    -- if(v.email==myusername) then can_read = true end
                end --for

				can_read = true
				if(can_read==true)then
                   ret = {success = true, error_msg = err, data = u, id = body.id}
			    else
			       ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to access docid#  ' .. tostring(body.id) )
                end
				--footer
            
print(cjson.encode(ret))
