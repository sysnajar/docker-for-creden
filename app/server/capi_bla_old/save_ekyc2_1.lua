#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
res = {success = false}
HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    apiKey = ngx.req.get_headers().apiKey
else
   body = {code = 'esig'} 
end
ngx.log(ngx.NOTICE, '====================='..body.code)
ngx.log(ngx.NOTICE, '====================='..ngx.req.get_body_data())

conn = mongo()
conn:set_timeout(1000)
local ok_conn,  err_conn = conn:connect('127.0.0.1',27017)
if err_conn then
    local ret = {success = false, error_msg = err_conn}
    print(cjson.encode(ret))
    ngx.exit(ngx.HTTP_OK)
end

function chk_is_valid(isValidDopa, isFaceIdentical, isValidLiveness)
    -- if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidAML) == 'true' and tostring(isValidLiveness) == 'true' then
    if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidLiveness) == 'true' then
        return true
    else
        return false
    end 
end
function chk_tx(usr)
        local ret = {success = false, error_msg = 'Your credit is not enough'}
        local counter_key = 'counter.'..usr..'.tx'
        local max_counter = 'counter.'..usr..'.txMax'
        c = rc:get(counter_key)  
        m = rc:get(max_counter)
        if not c and not m then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            if tonumber(c) >= tonumber(m) then
                print(cjson.encode(ret))    
                ngx.exit(ngx.HTTP_OK)
            else
                rc:incr(counter_key)      
            end
        end
end

os.execute2 = function(cmd)
  ngx.log(ngx.NOTICE, cmd)
 os.execute(cmd)
end

local db = conn:new_db_handle("dopa")
if body.username ~= 'creden' then chk_tx(body.username) end
body.createDate = os.date("%x")
body.createTime = os.date("%X")
body.createdDtm = bson.get_utc_date(ngx.now() * 1000)
body.compCode = body.username

ngx.log(ngx.NOTICE, 'kyc results:' .. tostring(body.ekyc_level) ..', process_status :' .. tostring(body.process_status))
if body.ekyc_level and tostring(body.ekyc_level) == '2.2' then
    body.process_status = 'finished'
elseif body.ekyc_level and tostring(body.ekyc_level) == '2.3' then
    body.isValid = body.isValidDopa
    body.process_status = 'finished'
end
local col = db:get_col("ekyc")
body.process_status = "finished"

body.txid = body.txid..'card'
n, err_n = col:insert({body})
if not n then
    res.error_msg = err_n
else
    res.success = true
    res.txid = body.txid
    res.usr = body.username

    --orisma 
    ngx.log(ngx.NOTICE, "Convert base64 cardImg1")
    local img_dir = "/home/creden/work/app/client/face/images/card/"
    local vdo_dir = "/home/creden/work/app/client/vdo/ex/"

    os.execute2("sh /home/creden/work/app/server/capi/save_b64.sh " .. body.txid .. " " .. img_dir .. body.cardImg3 .. " b64.imgdipchip.".. body.txid .." &")
                    
    --orisma
    
end

   
    ngx.log(ngx.NOTICE, 'kyc valid = false')
    ngx.log(ngx.NOTICE, 'txid = ' .. body.txid)
    local sdk_tx  = db:get_col("sdk_code"):find_one({txid = body.txid})
    local ekyc_tx = db:get_col("ekyc"):find_one({txid = body.txid})
    if (sdk_tx) then
        os.execute('/home/creden/work/app/server/capi/linekyc.sh "susess liveness ekyc attempt:' .. sdk_tx.ref_no ..'" ' .. tostring(ekyc_tx._id) .. ' ' .. HOST_NAME ..' &' )
    end


print(cjson.encode(res))
   
