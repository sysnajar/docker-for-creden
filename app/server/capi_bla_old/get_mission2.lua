#!/usr/bin/env lua
local cjson = require 'cjson'
redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = {}--ngx.req.get_body_data()
    --ngx.log(ngx.NOTICE,body)
    --body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

function shuffle(tbl)
  size = #tbl
  for i = size, 1, -1 do
    local rand = math.random(size)
    tbl[i], tbl[rand] = tbl[rand], tbl[i]
  end
  return tbl
end
local n_mission = 3
local t = {success = false}
local face_mission = { 'หันซ้าย', 'หันขวา', 'กระพริบตา' }
math.randomseed(os.time())
arr_mission = {}
table.insert(arr_mission, {type='face', show = face_mission[3] }) 
--table.insert(arr_mission, {type='face', show = face_mission[ math.random( #face_mission)] }) 
--table.insert(arr_mission, {type='face', show = face_mission[ math.random( #face_mission)] })


local res, err = prog('shuf', '-n', n_mission - 1, '/home/creden/work/app/server/capi/silkspan_text.txt')
if not err then
	t.success = true
	t.arr_time = {}
	n = 1
	for i in string.gmatch(res.stdout, "[^\r\n]+") do
		table.insert(arr_mission, {type='speech', show = i})
	end

	t.arr_mission = shuffle(arr_mission)
	
	if n_mission >= 1 then t.arr_time[1] = math.random( 1,6 ) end
	if n_mission >= 2 then t.arr_time[2] = math.random( 9,14 ) end
	if n_mission >= 3 then t.arr_time[3] = math.random( 17,22 ) end
	if n_mission >= 4 then t.arr_time[4] = math.random( 25,30 ) end

else
	print(err)
end
print(cjson.encode(t))
