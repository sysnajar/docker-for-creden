local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {cer_owner='amorrut@creden.co',cer_id="1234"}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("certificate")
    f,err = col:find_one({cer_id=body.cer_id,cer_owner=body.cer_owner})
    if f then 
        i, err = col:delete({cer_id=body.cer_id},1,0)
        if i then
            ret = {error_msg = true, data = "Remove  success"}
        else
            ret = {error_msg = false , data = "Not remove "}
        end
    else
        ret = { success = false, msg = "no data"}
    end
end
print(cjson.encode(ret))