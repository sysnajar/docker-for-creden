#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {}
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
	header = ngx.req.get_headers() or {}
	local check_api = header['X-CREDEN-KEY'] or {}
	if(check_api ~= 'credennotsodmama')then
		ngx.exit(401)
	end

    body = ngx.req.get_body_data()
else
    body = {cardNo='1103700530312',fnameTH='พัทธพงศ์',lnameTH='ประเทืองโอฬาร',dob='26/07/2534',cvv_code='JT0070481499'}
end

local key = 'count_node_ubuntu'
local key_incr_node = 'check_ince_node_count'

local get_key_node_count = rc:get(key)
if(get_key_node_count) then
    local del_redis = rc:del(key)
    local del_ince = rc:del(key_incr_node)
    t = {}
    t.targets ='dopa'
    t.status= '0'
    ret = t
else
    local check_count_post = rc:incr(key_incr_node)
    if(tonumber(check_count_post) >= 2)then
        t = {}
        t.targets = 'dopa'
        t.status = '1'
        ret = t
    else
        t = {}
        t.targets ='dopa'
        t.status= '0'
        ret = t
    end

end
     
print(ret.status)
