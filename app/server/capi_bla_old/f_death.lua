#!/usr/bin/env lua
cjson = require 'cjson'

local t = {}
for line in io.stdin:lines() do
  table.insert(t, line)
end
local s = table.concat(t)

-- produce output
local ret = {stCode = '-1' , stDesc = 'N/A' , valid = false}

local i  = s:find("<isError>false</isError>")

local i2 = s:find("<stDesc>")
local i3 = s:find("</stDesc>")

local i4 = s:find("<stCode>")
local i5 = s:find("</stCode>")


if(i)then ret.valid = true end

if(i2 and i3)then
	ret.stDesc = s:sub(i2+8, i3-1)
end

if(i4 and i5)then
	ret.stCode = s:sub(i4+8, i5-1)
end


print(cjson.encode(ret))


