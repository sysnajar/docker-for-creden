cjson = require('cjson')
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false
redis = require('redis')
rc = redis.connect('127.0.0.1')
ngx.log(ngx.NOTICE,"************** ndid_get_status got fired **************")
ngx.req.read_body()
print = ngx.say
local key = 'ndid_cb_list'
local body = ngx.req.get_body_data()
ngx.log(ngx.NOTICE, body)

body = cjson.decode(body)
request_id = body.request_id
assert(request_id , "no request_id")

local list2 = {}
local list  = rc:lrange(key,0,-1)
for i,v in ipairs(list) do
   local t = cjson.decode(v)
   --print(t.request_id .. ' ' .. request_id)
   if(t.request_id == request_id)then
     table.insert(list2, t)		
   end
end



function get_error(t)
local ret  = 'N/A1'
local code = '000' 
--local err = { ['30300'] = 'Error30300' , ['XX'] = 'XX' , ['YY'] = 'YY'  }

local err = { ['30300'] = 'Error30300' ,
 ['30000'] = '“ผใู้หบ้รกิารยืนยนัตวัตนท่ที่านเลือกไม่สามารถใหบ้รกิารไดใ้นขณะนี้ กรุณาทารายการใหม่หรือเลือกผใู้หบ้รกิารยืนยนัตวัตนรายอื่น' ,
 ['30200'] = 'ไม่สามารถทารายการไดใ้นขณะนี้กรุณาตรวจสอบเลขบตัร ประชาชนของท่านและทารายการใหม่หรือเลือกผใู้หบ้รกิารยืนยนั ตวั ตนรายอื่น' ,
 ['30300'] = 'ไม่พบขอ้มลูของทา่นในฐานขอ้มลูผใู้ชบ้รกิารของผใู้หบ้รกิารยืนยนั ตวัตนท่ทีา่นเลือกกรุณาเลือกผใู้หบ้รกิารยืนยนัตวัตนรายอื่นท่ีท่าน เคยลงทะเบียนและมีโมบายแอปพลิเคชนั' ,
 ['30400'] = 'ขออภยั ขอ้มลูของทา่นไม่เป็นปัจจบุนั กรุณาติดต่อผใู้หบ้รกิาร ยืนยนัตวัตนท่ที่านเลือกเพ่อือปัเดทขอ้มลูหรือเลือกผใู้หบ้รกิารยนืยนั ตวั ตนรายอื่น' ,
 ['30500'] = 'การยืนยนัตวัตนไม่สาเรจ็เน่อืงจากท่านระบุPINผิด,ภาพถ่ายไม่ ชดัเจน กรุณาทารายการใหม่ หรือเลือกผใู้ หบ้ รกิ ารยืนยนั ตวั ตนรายอื่น' ,
 ['30510'] = 'การยืนยนัตวัตนไม่สาเรจ็เน่อืงจากท่านระบุPIN/Passwordไม่ ถกูตอ้งกรุณาทารายการใหม่ หรือเลือกผใู้ หบ้ รกิ ารยืนยนั ตวั ตนรายอื่น',
 ['30520'] = 'การยืนยนัตวัตนไม่สาเรจ็เน่อืงจากท่านยืนยนัตวัตนดว้ยระบบจดจา ใบหนา้(FaceRecognition)ไมผ่่านตามเง่อืนไขท่กีาหนด กรุณาทารายการใหม่หรือเลือกผใู้หบ้รกิารยืนยนัตวัตนรายอื่น' ,
 ['30530'] = 'การยืนยนัตวัตนไม่สาเรจ็เน่อืงจากท่านระบุรหสัOTPไม่ถกูตอ้ง กรุณาทารายการใหม่ หรือเลือกผใู้ หบ้ รกิ ารยืนยนั ตวั ตนรายอื่น' ,
 ['30600'] = 'การยืนยนัตวัตนไม่สาเรจ็ เน่อืงจากไดท้า่นยกเลิกรายการยืนยนั ตวัตนท่โีมบายแอปพลิเคชนัของผใู้หบ้รกิารยืนยนัตวัตนกรุณาเลือก ผใู้หบ้รกิารยืนยนัตวัตนรายอื่น' ,
 ['30610'] = 'การยืนยนัตวัตนไม่สาเรจ็ เน่อืงจากทา่นไม่ไดย้อมรบัเง่อืนไขการ ใหบ้รกิารยืนยนัตวัตนของผใู้หบ้รกิารยืนยนัตวัตนท่ที่านเลือกกรุณา ทารายการใหม่ หรือเลือกผใู้ หบ้ รกิ ารยืนยนั ตวั ตนรายอื่น' ,
 ['30700'] = 'การยืนยนัตวัตนไม่สาเรจ็ เน่อืงจากผใู้หบ้รกิารยืนยนัตวัตนท่ที่าน เลือกไม่สามารถใหบ้รกิารไดใ้นขณะนี้กรุณาทารายการใหม่หรือ เลือกผใู้หบ้รกิารยืนยนัตวัตนรายอื่น' ,
 ['30800'] = 'ท่านยงัไม่ไดล้งทะเบยีนและยอมรบัเง่อืนไขการใชบ้รกิารNDIDท่โีม บายแอปพลิเคชนั ของผใู้ หบ้ รกิ ารยืนยนั ตวั ตน กรุณาดาเนินการก่อน ทารายการ',
 ['30900'] = 'ไม่สามารถทารายการไดใ้นขณะนี้เน่อืงจากอย่นูอกเวลาการ ใหบ้รกิารของ[IdP]กรุณาเลือกผใู้หบ้รกิารยืนยนัตวัตนรายอื่',
 ['40000'] = 'ขออภยั ท่านไม่สามารถทารายการไดใ้นขณะนี้กรุณาทารายการ ใหม่',
 ['40200'] = 'ขออภยั ไม่สามารถดาเนินการตอ่ไดเ้น่อืงจากทา่นใส่เลขบตัร ประชาชนไมถ่กูตอ้งกรุณาทารายการใหม่อีกครงั้',
 ['40300'] = 'ขออภยั ไม่สามารถดาเนินการตอ่ ได้ กรุณาติดต่อ',
 ['40400'] = 'ขออภยั ไม่สามารถดาเนินการตอ่ไดเ้น่อืงจากขอ้มลูไม่ถกูตอ้งกรุณา ทารายการใหม่อีกครงั้ ',
 ['40500'] = 'ขออภยั ไม่สามารถดาเนินการตอ่ ได้ '
}

for i,res in ipairs(t.response_list) do
    if(res.error_code)then
		ret = err[tostring(res.error_code)] or 'N/A'
		code = tostring(res.error_code)
	end
end

return ret, code
end

last_status = ''
last_error  = ''
last_item   = nil
timed_out   = false
local ret = {success = true , status = "000" , is_error = false ,
      request_id = request_id, status_text = "N/A", details = false, error_code = ""}

--print('found ' .. tostring(#list2))
for i,v in ipairs(list2) do
 --  print(cjson.encode(v))
 --  print(v.status)
   last_item = v

   if(v.status)then
       last_status = v.status

	if(v.status == "errored")then
	    last_error,ret.error_code = get_error(v)
	end

	if(v.status == "rejected" or v.status == "reject")then
	    
	    ret.status = "999"  --new
	    ret.is_error = true --new
            ret.status_text = "Rejected" --new
	    
	    last_error = "rejected"
	    ret.error_code =  "rejected"
	end

	if(v.status == "completed")then
        last_status = v.status
	    ret.status = "999"
	    last_error = ""
	    ret.error_code = ""

           local keyGet = 'ndid.details.' .. ret.request_id
           ret.details = cjson.decode(rc:get(keyGet))

	    -- if(ret.details)then
        --        local json = cjson.encode(ret.details)
		-- 	   local keySet = 'ndid.details.' .. ret.request_id
	    --    rc:set(keySet,json)
		--    ngx.log(ngx.NOTICE, 'store ndid details#' .. ret.request_id,'Key Set ====>', keySet,'Json ===>',json)
	    -- end

	    break
	end

   end

   if(v.timed_out)then
      ret.status = "999"
      ret.is_error = true
      ret.status_text = "Timeout"
      break;
   end

end


if(last_status == "errored")then
   ret.status = "999"
   ret.is_error = true
  -- ret.status_text = "Rejected"
end


--print('last_status ' .. last_status .. ','.. #list2)
print(cjson.encode(ret))
