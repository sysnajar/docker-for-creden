#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local scrypt = require "resty.scrypt"
common = require "common"
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)



if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body   = {user="codernajar@gmail.com",number_phone="",otp='',mode="",token=''} 
end

token = body.token
otp = body.otp
key = 'otp_'..token..'_'..otp
check = rc:get(key)

    body.user = body.user:lower()
    conn = mongo() 
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        s, err = col:find_one({ adminEmail = body.user or "N/A" }, nil, true) 

        if s then
            
            if body.mode == 'email' then
                if check then
                    ret = {success = true, error_msg = "complete", error_code='P005'}
                    rc:del(key)
                end
            elseif  body.mode == 'mb' then
                if check then
                    rc:del(key)
                    local update_cmd = {["$set"] = {
                        comTel  = body.number_phone
                    }}
                    u, err = col:update({adminEmail=body.user}, update_cmd, 0, 0, true)
                    ret = {success = true, error_msg = "complete",error_code='P006'}
                end

            end
           
            
            
        else
            ret = { success = false, error_msg = 'Email already exists.', error_code = "P00" }
        end
    end

    print(cjson.encode(ret))