#!/usr/bin/env lua 
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local object_id = require "resty-mongol.object_id"
rc = redis.connect('127.0.0.1', 6379)
print = ngx.say


function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function get_my_company(username)
	ngx.log(ngx.INFO, "get_my_company >>> [" .. tostring(username).."]")
    assert(username, "Username can not be nil!!!")

    local redis_key = 'companyof.' .. username
    local ret = rc:get(redis_key) 
    if (ret) then 
        return ret 
    end

    local mongo = (mongo) and mongo or require "resty-mongol"
    local conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if not ok then
            return false,nil
        else
            db = conn:new_db_handle('dopa')
            col = db:get_col('esig_user')
            local res = col:find_one({adminEmail = username})
            conn:close()
            local ret = assert(res.company_id,'company can not be nil')
            rc:set(redis_key, res.company_id) 
            return ret	
        end			    
    return nil
end

function get_logo_company(company_id)
    assert(company_id, "company_id can not be nil!!!")

    local redis_key = 'companyLogo.' .. company_id
    local ret = rc:get(redis_key) 
    if (ret) then 
        return ret 
    end

    local mongo = (mongo) and mongo or require "resty-mongol"
    local conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if not ok then
            return false,nil
        else
            db = conn:new_db_handle('dopa')
            col = db:get_col('user_company')
            local res = col:find_one({ _id = object_id.new(convertHexStringToNormal(company_id)) })
            conn:close()
            local ret = "images/Creden_logo.png"
            if res and res.companyLogo and res.companyLogo ~= "" then
                ret = "/face/images/card/" .. res.companyLogo .. ".png"
            end
            rc:set(redis_key, ret) 
            return ret	
        end			    
    return nil
end


session.data.last_update = os.date()
ngx.log(ngx.INFO, 'username', session.data.username)
company_id = get_my_company(session.data.username or 'bla@creden.co')
company_logo = get_logo_company(company_id)
session.data.company_id = company_id
session:save(false)
u = session.data.username or ""
c = session.data.company_id

--Load notifications
-- conn = mongo()
-- conn:set_timeout(5000)
-- ok, err = conn:connect('127.0.0.1',27017)
-- if err then
--     ret = {success = false, error_msg = err}
-- else
--     db = conn:new_db_handle("edoc")
--     col = db:get_col("notification")
--     s, err = col:find({email = (u=="" and "N/A" or u), status = 0})
--     n_list = {} 

--     for k, v in s:pairs() do
--         v._id = nil
--         table.insert(n_list , v)
--     end

--     if(#n_list<1)then n_list = nil end

-- end
local key = "invite_company_" .. u
local invite_company = rc:get(key) or nil

local t = {
    success = true, 
    user_only = u,
    user = u ..'@'.. (session.data.last_update or ''),
    rd = tostring(tonumber(ngx.req.get_uri_args()['random'])),
    new_code = true,
-- notifications = n_list,
    invite_company = invite_company,
    company_id = c,
    company_logo = company_logo,
}

print( cjson.encode(t) )





