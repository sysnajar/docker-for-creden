#!/usr/bin/env lua
local cjson = require 'cjson'
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local common = require "common"

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
  --body  = ngx.req.get_post_args()
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {} 
end


common.validate_alphanumeric(body.username , "Invalid Param username")
common.validate_sign_type(body.sign_type , "Invalid Param sign_type")
common.validate_alphanumeric(tostring(body.field_id), "Invalid Param field_id")

if (body.sign_type == 'draw_initial' and body.img ~= "") then
    rc:set('tmp_sign',body.img)
    os.execute('lua /home/creden/work/app/server/capi/redisget.lua tmp_sign | base64 -d > /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.username..'_draw_initial_onlydraw.png')
end

image_name = body.username..body.field_id..'_'..body.sign_type

-- if rc:get('is_ldd') and rc:get('is_ldd')=='true' then
--   code = "' '"
-- else
--   code = ngx.md5(body.code):sub(1,10)
-- end
code = ngx.md5(body.code):sub(1,10)
local user_initial = body.username .. '_draw_initial_onlydraw'

local name = body.name
local lname = body.lname
local day = tostring(os.date("%d/%m/%Y"))
local hour = tostring(os.date("%H:%M:%S"))
if (name=="") then
  name="-"
end
if (lname=="") then
  lname="-"
end

os.execute('rm /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.username..'_draw_initial.png')

if (body.selectpic==2) then
  os.execute('sh /home/creden/work/app/server/capi/create_sign3.sh '..user_initial..' '.. name ..' '.. lname ..' '.. day ..' '.. hour ..' '.. image_name)
else
  os.execute('sh /home/creden/work/app/server/capi/create_sign4.sh '..user_initial..' '.. code ..' '.. image_name)
end


os.execute('cp /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.username..body.field_id..'_draw_initial.png /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.username..'_draw_initial.png')
t = {success = true, image_name = image_name}
print(cjson.encode(t))
