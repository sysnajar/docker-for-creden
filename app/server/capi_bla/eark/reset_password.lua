#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local scrypt = require "resty.scrypt"
common = require "common"
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)



if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body   = {user="codernajar@gmail.com", password="1234",befor_pass="1234",mode=""} 
end

    body.user = body.user:lower()
    conn = mongo() 
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        s, err = col:find_one({ adminEmail = body.user or "N/A" }, nil, true) 

        if s then
            if body.mode == 'otp' then
                math.randomseed(os.clock()*100000000000)
                otp = ''
                for i=1,3 do
                    a = math.random(10, 99)       
                    otp = otp .. a
                end
                token = ''
                for i=1,10 do
                    a = math.random(10, 99)       
                    token = token .. a
                end
                key = 'otp_'..token..'_'..otp
	            rc:set(key,1)
                rc:expire(key,300)
                local m, err = prog('node', '/home/creden/work/app/server/capi/resetpass_mail_otp.js', body.user, otp) -- Send email otp
                ret = { success = true, error_msg = 'otp', error_code = "P04",token = token }

                -- local update_cmd = {["$set"] = {
                --     hashedPassword  = scrypt.crypt(tostring(otp))
                -- }}
                -- u, err = col:update({adminEmail=body.user}, update_cmd, 0, 0, true)
                
                -- if u then
                --     local m, err = prog('node', '/home/creden/work/app/server/capi/resetpass_mail_otp.js', body.user, otp) -- Send email otp
                --     ret = { success = true, error_msg = 'otp', error_code = "P04" }
                -- else
                --     ret = { success = false, error_msg = 'otp', error_code = "P04" }
                -- end
            end

            if body.mode == 'edit' then
                chk = scrypt.check(body.befor_pass, s.hashedPassword)
                if chk then
                    local update_cmd = {["$set"] = {
                        hashedPassword  = scrypt.crypt(tostring(body.password))
                    }}
                    u, err = col:update({adminEmail=body.user}, update_cmd, 0, 0, true)
                    if u then
                        ret = { success = true, error_msg = 'update success', error_code = "P01"}
                    else
                        ret = { success = false, error_msg = 'update unsuccess', error_code = "P02"}
                    end
                else
                    ret = { success = false, error_msg = 'Old password not match', error_code = "P03"}
                end
            end

            if body.mode == 'sc' then
                -- chk = scrypt.check(body.befor_pass, s.hashedPassword)
                -- if chk then
                    local update_cmd = {["$set"] = {
                        hashedPassword  = scrypt.crypt(tostring(body.password))
                    }}
                    u, err = col:update({adminEmail=body.user}, update_cmd, 0, 0, true)
                    if u then
                        ret = { success = true, error_msg = 'update success', error_code = "P01"}
                    else
                        ret = { success = false, error_msg = 'update unsuccess', error_code = "P02"}
                    end
                -- else
                --     ret = { success = false, error_msg = 'Old password not match', error_code = "P03"}
                -- end
            end
            
        else
            ret = { success = false, error_msg = 'Email already exists.', error_code = "P00" }
        end
    end

    print(cjson.encode(ret))