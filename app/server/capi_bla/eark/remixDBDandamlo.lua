#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {}
prog.timeout = 1000 * 300
prog.timeout_fatal = false
local data = {}
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)

else
    body = {fnameTH='พัทธพงศ์',lnameTH='ประเทืองโอฬาร',cardNo='1103700530312'}
end
function chk_DBD(firstname,lastname)
    -- body
    local tmp = {}
    local conn = mongo()
    conn:set_timeout(10000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
       
        -- body.text = 'ภาวุธ พงษ์วิทยภานุ'
         db = conn:new_db_handle("newdbd")
    --    col = db:get_col("company")
        col2 = db:get_col("partner")
        con3 = {PARTNER = {['$regex']= firstname..' '..lastname}}
            ngx.log(ngx.NOTICE, 'txid = ' .. firstname,lastname)


        s3 = col2:find(con3,{JP_TNAME=1,PARTNER=1,JP_NO=1,JP_ENAME=1})
        

        for k, v in s3:pairs() do
            v._id = tostring(v.JP_NO)
           

            table.insert(tmp,v)
        end 
        -- return cjson.encode(tmp)
        return tmp
    end
end
function chk_amlo(cardNo,firstname,lastname)
    local isValidAML = false
    local ret_amlo = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/chk_amlo2.lua', cardNo,firstname,lastname)
    if not err then
      tmp = cjson.decode(res.stdout)
      ret_amlo.isValidAML = tmp.isValidAmlo
      ret_amlo.amlo_desc = tmp.amlo_desc
      ret_amlo.amlo_code = tmp.amlo_code
    else
    ret_amlo.isValidAML = false 
    ret_amlo.amlo_desc = false
    ret_amlo.amlo_code = false
    end 
    -- body.isValidAML = isValidAML
    return cjson.encode(ret_amlo)
end
if( body.fnameTH ~= nil and body.lnameTH~= nil and body.cardNo~= nil) then
   
    local chk_amlo = chk_amlo(body.cardNo,body.fnameTH,body.lnameTH)
    chk_amlo = cjson.decode(chk_amlo)
    ret.amlo_code = chk_amlo.amlo_code
    ret.amlo_desc = chk_amlo.amlo_desc
    ret.isValidAML = chk_amlo.isValidAML
    local chk_DBD_01 = chk_DBD(body.fnameTH,body.lnameTH)
    ret.dbd_results = chk_DBD_01
    ret.success = true
else
ret.success = false
ret.dbd_results = nil
ret.isValidAML = nil
ret.amlo_desc = nil
ret.amlo_code = nil 
ret.msg = 'fnameTH and lnameTH dob not send'   
end
 -- print(findxx3)
print(cjson.encode(ret))
