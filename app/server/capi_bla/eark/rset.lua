#!/usr/bin/env lua
redis = require 'redis'

if(arg[1]==nil)then os.exit(1) end

local rc = redis.connect('127.0.0.1', 6379)
local t = {}
for line in io.stdin:lines() do
  table.insert(t,line)
end

rc:set(arg[1],table.concat(t, '\n'))
