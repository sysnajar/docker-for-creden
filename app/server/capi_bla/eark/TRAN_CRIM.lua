#!/usr/bin/env lua

function init()
if(#arg > 0  )then
  cjson = require 'cjson'
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379)
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(id)
	local key = 'PREP:CRIM:' .. id
	local t1 = cjson.decode(rc:get(key))
	local t2 = {
		CRIM_blackamt = (type(t1.black)=='table') and t1.black.total or 0,
		CRIM_redamt = (type(t1.red)=='table') and t1.red.total or 0,
		CRIM_bankrupt = (type(t1.bankrupt)=='table') and t1.bankrupt.total or 0
	}

	return t2
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
