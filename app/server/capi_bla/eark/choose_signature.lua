#!/usr/bin/env lua
local cjson = require 'cjson'
local session = require "resty.session".open({secret = "4321"})
local common = require "common"

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
  --body  = ngx.req.get_post_args()
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {} 
end

local myusername  = assert(session.data.username)
body.code = myusername
body.field_id = tostring(tonumber(body.field_id)) 


common.validate_alphanumeric(body.code, 'invalid param')
common.validate_number(body.field_id, 'invalid param')

if body.sign_type == 'draw' then
    os.execute('cp /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..'_draw.png /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..body.field_id..'_draw.png')
end

if body.sign_type == 'gen' then
    os.execute('cp /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..'_gen.png /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..body.field_id..'_gen.png')
end

if body.sign_type == 'draw_initial' then
    os.execute('cp /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..'_draw_initial.png /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..body.field_id..'_draw_initial.png')
end

if body.sign_type == 'gen_initial' then
    os.execute('cp /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..'_gen_initial.png /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..body.field_id..'_gen_initial.png')
end

t = {success = true}
print(cjson.encode(t))
