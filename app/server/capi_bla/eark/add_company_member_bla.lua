#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
prog.timeout_fatal = false
local ret = {success = false}
local data = {}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function round(num) 
    if num >= 0 then return math.floor(num+.5) 
    else return math.ceil(num-.5) end
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

function count_date(exp)
    local current_day = bson.get_utc_date(os.time() * 1000).v
    local sum_date = (exp - current_day) / (24*60*60*1000)
    return round(sum_date)
end

function check_belong_company(email)
    local col_esig = db:get_col("esig_user")
    local e, err = col_esig:find_one({ adminEmail = email })

    if e and e.company_id then
        local col_mem = db:get_col("company_members")
        local m, err = col_mem:find({ company_id = object_id.new(convertHexStringToNormal(e.company_id)) })
        local list = {}
        for k, v in m:pairs()do
            table.insert(list, v)
        end

        if #list > 1 then
            u = { success = false, error_msg = "not add because user one more than", error_code = "2015" }
            print(cjson.encode(u))
            ngx.exit(ngx.HTTP_OK)
        end

    end

    local v, err = col:find_one({ create_by = invite_email })

    if v then
        local col_mem = db:get_col("company_members")
        local m, err = col_mem:find({ company_id = v._id })
        local list = {}
        for k, v in m:pairs()do
            table.insert(list, v)
        end
    
        if #list > 1 then
            u = { success = false, error_msg = "not add because user one more than", error_code = "2015" }
            print(cjson.encode(u))
            ngx.exit(ngx.HTTP_OK)
        end
    end

end
function check_packages(t)
	local ret = 0

	-- for k, v in t:pairs() do
	 
    --     if(v.package ~= 'addon' and v.package ~= 'dipchip' )then
    --         ret = ret + v.users 
	--     else
	-- 		ret = ret + ((v.user_add_on) and v.user_add_on or 0) --active main package
    --     end
        
    -- end

    -- if ret == 0 then
    --     ret = 3
    -- end
 
	return 9999
end

function check_package_company(user, company_id) 
    local limit = 0
    local col_package = db:get_col("package_ss")
    -- p, err = col_package:find({ email =  user.create_by })
    
	--p, err = col_package:find({ company_id =  tostring(user._id) })
    packages = col_package:find({company_id = company_id , active_package = true , package = {['$ne']='dipchip'}})
    limit = check_packages(packages) 

	--[[
    p, err = col_package:find({ company_id =  company_id })
    local list_p = {}
    for i, v in p:pairs() do 
        v._id = nil 
        table.insert(list_p, v)
    end
	]]--


   -- print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002", data = list_package, com_id = tostring(user._id) , limit = limit }))
   -- ngx.exit(ngx.HTTP_OK)
   --[[
    if #list_p > 0 then
        local list_package = {}
        for i, v in p:pairs() do
            v._id = nil
            table.insert(list_package, v)
        end
        -- local package = p
        local package = list_package[0]
        local sum_date = count_date(package.active_expire_date)

        if sum_date > 0 then
            limit = package.users
        else
            limit = 0
        end

    else
        limit = 3
    end
	]]--

    -- if user.create_by == "natcha@creden.co" or user.create_by == "thanaphas@creden.co" then
    --     limit = 1000
    -- end
    
    return limit

end

function check_member(user) 
    local col_mem = db:get_col("company_members")
    m, err = col_mem:find({ company_id =  user._id })
    local list_mem = {}
    for i, v in m:pairs() do
        v._id = nil
        table.insert(list_mem, v)
    end

    return #list_mem
end

function add_member(email, company_id, create, root) 
    local col_mem = db:get_col("company_members")
    m, err = col_mem:find_one({ email = email, company_id = object_id.new(convertHexStringToNormal(company_id)) })
    local u = {}
    if m then
        u = { success = false, error_msg = "User already exist a company", error_code = "2004" }
        print(cjson.encode(u))
        ngx.exit(ngx.HTTP_OK)
    else
    
        local col_esig = db:get_col("esig_user")
        e, err = col_esig:find_one({ adminEmail = email })
        admin, err_admin = col_esig:find_one({ adminEmail = create })
        admin._id = nil
  
        if e then
            mem = {
                esig_user_id = e._id,
                created_by = create,
                email = email,
                createdDtm = bson.get_utc_date(ngx.now() * 1000),
                status = "WAITING",
                role = "USER",
                company_id = object_id.new(convertHexStringToNormal(company_id)),
                positions = {{ position_name = "N/A", position_id = "9999" .. email .. os.time() }}
            }
            local i, err_i = col_mem:insert({mem})


            u = { success = true, error_msg = "add user company success", error_code = "2005" }

        else
            mem = {
                esig_user_id = "",
                created_by = create,
                email = email,
                createdDtm = bson.get_utc_date(ngx.now() * 1000),
                status = "WAITING",
                role = "USER",
                company_id = object_id.new(convertHexStringToNormal(company_id)),
                positions = {{ position_name = "N/A", position_id = "9999" .. email .. os.time() }}
            }
            local i, err_i = col_mem:insert({mem})

            u = { success = true, error_msg = "add user company success and no user in esig user", error_code = "2006" }
        end

        local send = { 
            admin = admin, 
            email = email, 
            company = root.companyName or admin.adminEmail .. "_Company" 
        }
        send = cjson.encode(send)
        local m, err = prog('node', '/home/creden/work/app/server/capi/invite_company_mail_bla.js', send)
        local invite_com = { 
            invite_company = root.companyName or admin.adminEmail .. "_Company", 
            company_id = company_id 
        }
        invite_com = cjson.encode(invite_com)
        local key = "invite_company_" .. email
        rc:set(key, invite_com)
        rc:expire(key, 2592000)

        print(cjson.encode(u))
        ngx.exit(ngx.HTTP_OK)
    end
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { company_id = "", email = "",  }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    db = conn:new_db_handle("dopa")
    col = db:get_col("user_company")
    i, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(body.company_id)) })
    local limit = check_package_company(i, body.company_id)
    local my_member = check_member(i)

    if my_member < limit then
        check_belong_company(body.email)
        add_member(body.email, body.company_id, body.create_by, i)
    else
        ret = { success = false, error_msg = "company limit not add user", error_code = "2003", limit = limit, my_member = my_member, com_id = body.company_id }
    end


end
print(cjson.encode(ret))
