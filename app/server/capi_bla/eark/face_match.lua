#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
end

if not body.selfie_file or body.selfie_file == "" then body.selfie_file = 'selfie_'..body.txid end

function chk_face()
    local t = {}
    local faceId1 = detect_face(body.filename)
    local faceId2 = detect_face(body.selfie_file)
    t = verify_face(faceId1, faceId2)
    rc:set('face.recognize.'..body.txid..'.'..body.num, (t.faceMatchPercent or 0) * 100)
end
  
function detect_face(img)
    local faceId = ''
    local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
    if not err then
        local tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end
  
function verify_face(faceId1,faceId2)
    local t = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
    if not err then
        local tmp = cjson.decode(res.stdout)
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
    end
    return t
end

chk_face()
--rename img
os.execute('mv /home/creden/work/app/client/face/images/card/'..body.filename..'.png /home/creden/work/app/client/face/images/card/rec_'..body.txid..'-'..body.num..'.png')

local pass = 3
local percent = 70
local count = 0
local isFaceVerified = false
for i=1,body.num do
    local key = 'face.recognize.'..body.txid..'.'..i
    local res = (rc:get(key) or 0)
    if tonumber(res) > percent then count = count + 1 end
end
if count >= pass then 
    isFaceVerified = true 
end
rc:set('liveness.'..body.usr..'.'..body.txid..'.face', isFaceVerified)

local ret = {success = true, isFaceVerified = isFaceVerified}
print(cjson.encode(ret))


