local json = [[
{
    "IsSuccess": true,
    "ErrorMessage": null,
    "ErrorCode": 0,
    "Data": [
      {
        "EMAIL": "PHAKHAKUL.PHO@BLA.CO.TH",
        "EmpName": "ภคกุล",
        "EmpID": "003514",
        "EmpSurname": "ผ่องบุพกิจ",
        "OrgDescription": "ส่วนวิเคราะห์เครดิต"
      },
      {
        "EMAIL": "Test1@BLA.CO.TH",
        "EmpName": "fTest1",
        "EmpID": "000001",
        "EmpSurname": "lTest1",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Test2@BLA.CO.TH",
        "EmpName": "fTest2",
        "EmpID": "000002",
        "EmpSurname": "lTest2",
        "OrgDescription": "แผนก HR"
      },
      {
        "EMAIL": "Test3@BLA.CO.TH",
        "EmpName": "fTest3",
        "EmpID": "000003",
        "EmpSurname": "lTest3",
        "OrgDescription": "แผนก Development"
      },
      {
        "EMAIL": "Test4@BLA.CO.TH",
        "EmpName": "fTest4",
        "EmpID": "000004",
        "EmpSurname": "lTest4",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom1@BLA.CO.TH",
        "EmpName": "Boom",
        "EmpID": "000005",
        "EmpSurname": "C",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom2@BLA.CO.TH",
        "EmpName": "BoomBoomBoom",
        "EmpID": "000006",
        "EmpSurname": "CCC",
        "OrgDescription": "แผนก HR"
      },
      {
        "EMAIL": "Boom3@BLA.CO.TH",
        "EmpName": "Chaluemchat",
        "EmpID": "000007",
        "EmpSurname": "Chaluemchat",
        "OrgDescription": "แผนก Development"
      },
      {
        "EMAIL": "Boom4@BLA.CO.TH",
        "EmpName": "Chaluemchat boom",
        "EmpID": "000008",
        "EmpSurname": "CCC Chaluemchat",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom5@BLA.CO.TH",
        "EmpName": "Chaluemchat boom boom",
        "EmpID": "000009",
        "EmpSurname": "CCC Chaluemchat",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom6@BLA.CO.TH",
        "EmpName": "Chaluemchat boom",
        "EmpID": "0000010",
        "EmpSurname": "C Chaluemchat",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Test11@BLA.CO.TH",
        "EmpName": "mazachii",
        "EmpID": "000011",
        "EmpSurname": "Cat",
        "OrgDescription": "แผนก HR"
      }
      
    ]
  }
]]

ngx.say(json)
