#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
prog.timeout_fatal = false

local load_h = prog('lua', '/home/creden/work/app/server/capi/h_name.lua')
local HOST_NAME = 'https://hjkl.ninja'
HOST_NAME = HOST_NAME:gsub("%\n", "")

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
rc2 = redis.connect('54.254.182.56', 6379)

rc2:auth('noRedisPass1010')
body = {}

function trim5(s)
    return s:match'^%s*(.*%S)' or ''
end
  
function getVal(key)
    local ret = body[key]
    ret = ret and trim5(ret) or nil
    if(ret=='')then ret = nil end
    return ret
end
  
function get_cust_code(usr)
    return usr
end

function chk_civil(fullname)
   -- civil court
    local redcase_count   = 0
    local blackcase_count = 0

    local db_red = conn:new_db_handle("redcase")
    local col = db_red:get_col("case")
     -- fullname  = "ณัฐพล หมาดทอง"
        --fullname = "xx"
    local s = col:find({txtAccuName2 = {['$regex'] = fullname}}) 
        for k,v in s:pairs() do
            redcase_count = redcase_count + 1  
        end
        

    local db_black = conn:new_db_handle("blackcase")
    local col = db_black:get_col("case")
    local s = col:find({txtAccuName2 = {['$regex'] = fullname}}) 
        for k,v in s:pairs() do
            blackcase_count = blackcase_count + 1  
        end
    local t = {redcase_count = redcase_count,blackcase_count = blackcase_count}
    -- local t = {civil_results = {redcase_count = redcase_count , blackcase_count = blackcase_count}}
        -- print(cjson.encode(t))
        return t
        -- civil court
end
function chk_amlo()
    local isValidAML = false
    local ret_amlo = {}
    local card_no = body.idcard
    local fnameTH = body.firstname
    local lnameTH = body.lastname
    local res, err = prog('lua', '/home/creden/work/app/server/capi/chk_amlo2.lua', card_no,fnameTH,lnameTH)
    if not err then
      tmp = cjson.decode(res.stdout)
      ret_amlo.isValidAML = tmp.isValidAmlo
      ret_amlo.amlo_desc = tmp.amlo_desc
      ret_amlo.amlo_code = tmp.amlo_code
    else
    ret_amlo.isValidAML = false 
    ret_amlo.amlo_desc = false
    ret_amlo.amlo_code = false
    end 
    -- body.isValidAML = isValidAML
    return cjson.encode(ret_amlo)
end
function chk_kyc()
    local result = nil
    local params   = 'national_id=' .. (body.cardNo or '') ..'&first_name=' .. (body.fnameTH or '') .. '&last_name=' .. (body.lnameTH or '') .. '&date_of_birth=' ..  (body.dob or '')  .. '&cvv_code=' ..(body.laserCode or '')   
    --local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')
    local dob = body.dob:sub(7,10)..body.dob:sub(4,5)..body.dob:sub(1,2)


    print("xxxx"..dob)
    local res, err = prog('sh', '/home/creden/work/app/server/capi/check_cardbylaser_proxy1.sh', body.idcard, checkstring(body.firstname), checkstring(body.lastname), dob, body.lasercode)

    if(not err)then
    print('dddddko['..res.stdout..']')    
        local tmp = xml_to_table(res.stdout)
        print('xml_to_table='..cjson.encode(tmp))
            if(tmp) then

                if tostring(tmp.status) == 'true' and tostring(tmp.code) == "0" then
                    result = tmp.status
                    body.status = 'pass'
                    body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                else
                    if(tostring(tmp.code) ~= "0") then
                    body.status = 'fail'
                    body.remarks = 'ไม่พบข้อมูล เลขหน้าบัตร หลังบัตร และ ชื่อและนามสกุล และวันเดือนปีเกิดกรุณาตรวจสอบ' 
                    end
                   
                end             
            isValidDopa = tmp.status
            end 
    end
   
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
    return result
end

function xml_to_table(xml)
local status  = false
local remarks = ''
local code = ''
rc:set('last_xml', xml)

if( string.find(xml, "<IsError>false") ) then status = true  end

local i1 =  string.find(xml, "<Desc>") 
local i2 =  string.find(xml, "</Desc>")
local i3 =  string.find(xml, "<Code>") 
local i4 =  string.find(xml, "</Code>")

if(i1==nil or i2==nil) then
    code = '5'
    remarks = 'Dopa Error:' .. tostring(xml)
else
    code = string.sub(xml,i3+6,i4-1)
    remarks = string.sub(xml , i1+6 , i2)
end
    


local ret = { status = status, remarks = remarks,code = code}
return ret
end
function checkstring(value)
 if(not value) then
      return ""
 else
      value = string.gsub(value," ","%20")
    return value
 end
end

function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical)
    if body.vdo and string.match((body.vdo.url or ''), "http") then
        body.isValid = false
        body.isValidLiveness = 'processing'
        body.process_status = 'processing'
    else
        if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' then
            body.isValid = true
        else
            body.isValid = false
        end 
        body.process_status = 'finished'
    end
end

function chk_face()
  t = {}
  local faceId1 = ''
  local faceId2 = ''
  if getVal('photoidcard') and getVal('photoselfie') then
    faceId1 = detect_face(body.photoidcard:gsub('.png',''))
    faceId2 = detect_face(body.photoselfie:gsub('.png',''))
    t = verify_face(faceId1, faceId2)
    body.faceConfidence = (t.faceMatchPercent or 0)
    body.isFaceIdentical = (t.isIdentical or false)
  end
end

function detect_face(img)
    print('detect_face')
    print(img)
    local faceId = ''
    local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
    if not err then
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end

function verify_face(faceId1,faceId2)
    t = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
    if not err then
        print('verify_face')
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        if(tmp.data.confidence >= 0.75) then
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
        else
        t.faceMatchPercent = tmp.data.confidence
        t.isIdentical = false
        
        end

    end
    return t
end

function downloadImgs()
    redis = require 'redis'
    rc = redis.connect('127.0.0.1', 6379)
    local cnt = tostring(rc:incr('kyc.counter'))
    local dir = '/home/creden/work/app/client/face/images/card/'
    for i, k in ipairs({'photoidcard', 'photoselfie'}) do
        local v = getVal(k)
        if(v)then
            local filename = cnt..k 
            chk = string.sub(v, 1, 4)
            if string.match(chk, "http") then
                local cmd = 'wget '.. v ..' -O ' .. dir .. filename .. '.png'
                os.execute(cmd)
            else
            local tmp = 'tmp.kyc.img' .. filename   
            rc:set(tmp , v)
            local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
            os.execute(cmd)
            rc:del(tmp) 
        end     
  
        body[k] = filename .. '.png'
        rc:lpush('q_move_card',filename)
      end
    end
    body.cardImg = body.cardImg1
end


    while true do
        os.execute("sleep 3")
        len = rc:llen('api.kyc.activ3')
        for i=1, len do
            local data = rc:rpoplpush('api.kyc.active3', 'api.kyc.complete')
            if data then
                body = cjson.decode(data)
                conn = mongo()
                conn:set_timeout(1000)
                ok, err = conn:connect('127.0.0.1',27017)
                if err then
                    ret = {success = false, error_msg = err}
                else
                    db = conn:new_db_handle("dopa")
                    col = db:get_col("ekyc_zipmex")
                    body.channel = 'api'
                    body.createDate = os.date("%x")
                    body.createTime = os.date("%X")
                    body.createdDtm = bson.get_utc_date(os.time() * 1000)
                    body.compCode = get_cust_code(body.username)
                    -- if chk_kyc() then

                        chk_kyc()
                        downloadImgs()
                        chk_face()
                        local chk_amlo = chk_amlo()
                       chk_amlo = cjson.decode(chk_amlo)
                       body.amlo_code = chk_amlo.amlo_code
                       body.amlo_desc = chk_amlo.amlo_desc
                       body.isValidAML = chk_amlo.isValidAML
                       local fullname = body.firstname..' '..body.lastname
                       body.civil_results = chk_civil(fullname)

                      
                    -- end
                    chk_is_valid(body.isValidDopa, body.isValidAML, body.isFaceIdentical)
                    body.username = nil
                    body.password = nil
                    selector = {txid=body.txid, compCode = body.username}
                    update = {["$set"] = {isValid = body.isValid, isValidDopa = body.isValidDopa, isValidAML = body.isValidAML,amlo_code=body.amlo_code,amlo_desc=body.amlo_desc,isValidAML=body.isValidAML,dbd_results=body.dbd_results,civil_results=body.civil_results, isFaceIdentical = body.isFaceIdentical, faceConfidence = body.faceConfidence,remarks_, remarks = body.remarks, process_status = body.process_status}}
                    n, err = col:update(selector, update, 0, 0, true)
                    if not n then
                        ret = {success = false, error_msg = err}
                    else
                        ret = {success = true, error_msg = err, txid = body.txid , usr = body.compCode}
                        print(cjson.encode(ret))
                    end
                end
            end
        end
        print('no queue')
    end
