#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {adminUsername = arg[1], activeCode = arg[2]} 
end

function create_key()
    if not rc:get('counter.'..body.adminEmail..'.tx') or not rc:get('counter.'..body.adminEmail..'.txMax') then
        rc:set('counter.'..body.adminEmail..'.txMax', 0)
        rc:set('counter.'..body.adminEmail..'.tx', 0)
    end
end

body.creditDateTime = bson.get_utc_date(tonumber(body.creditDate2))

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    create_key()
    local key_credits = 'counter.'..body.adminEmail..'.txMax'
    total_credits = rc:get(key_credits)
    credits = (tonumber(total_credits) or 0) + (tonumber(body.credits) or 0)

    db = conn:new_db_handle("dopa")
    col = db:get_col("company")
    col1 = db:get_col("credit_history")

    selector = {adminEmail=body.adminEmail}
    update = {["$set"] = {credits = credits, creditPrice = body.creditPrice, creditDate = body.creditDate, creditDateTime = body.creditDateTime}}
    u, err = col:update(selector, update, 0, 0, true)
    if not u then
        ret = {success = false, error_msg = err}
    else
        rc:set(key_credits,credits) 
        i, err = col1:insert({body}, nil, true)          
        ret = {success = true, error_msg = "complete"}
    end
end

print(cjson.encode(ret))


