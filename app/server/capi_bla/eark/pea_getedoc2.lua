#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require 'resty-mongol'
local bson = require 'resty-mongol.bson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout = 1000 * 300
prog.timeout_fatal = false
local srcpath = '/home/creden/work/app/client/face/images/card/'
local check_apiKey = 'TdDVFZPbo8uZW3Qb2IZ0tUPSGdJe5BiV'
local apiKey = nil
local ret = nil
local body = nil

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    local ok,err = conn:connect('127.0.0.1',27017)
    local result = false
    if err then
        result = false
    else
        result = true
    end
    return result
end

function chk_apiKey()
    if not apiKey or apiKey ~= check_apiKey then
        ret = {success = false, msg = 'Authentication Fail'} 
        ngx.exit(ngx.HTTP_BAD_REQUEST)
        return false
    end
    return true
end

function file_exists(file)
    local result = false
    local f = io.open(tostring(file),"r")
    if f then
        result = true
        f:close() 
    end
    return result
end

function chk_file()
    local result = false
    local srcfile = nil
    if (body.id and tostring(body.id) ~= '') then
        srcfile = srcpath .. tostring(body.id) .. '_stamped.pdf'
        if (file_exists(srcfile)) then
            result = true
        end
    end
    return result
end

function get_edoc()
    printFile = nil
    local db = conn:new_db_handle("edoc")
    local col = db:get_col("edoc_list")
    local rows,err = col:find_one({id=body.id})
    if (rows) then
        if (chk_file()) then
            printFile = '/face/images/card/' .. tostring(body.id) ..'_stamped.pdf'
        end
    end
end

if(ngx)then
    apiKey = ngx.req.get_headers().apiKey
    print = ngx.say
else
    body  = {id='1610678556'} 
end

chk_apiKey()

if( ngx.var.request_method =='GET') then
    body = ngx.req.get_uri_args()
    if connect_mongo() then
        get_edoc()
    end
end

--ngx.exec('/face/images/card/'..file_name..'.pdf')
ngx.exec(printFile)