#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

local load_h = prog('lua', '/home/creden/work/app/server/capi/h_name.lua')
local HOST_NAME = load_h.stdout
HOST_NAME = HOST_NAME:gsub("%\n", "")
local t = {success = false, laser_code = {lc1 = '', lc2 = '', lc3 = ''}}
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {img = arg[1]} 
end

function downloadImgs()
    local cnt = tostring(rc:incr('detect.card.name')) .. 'back_card'
    local dir = '/home/creden/work/app/client/face/images/card/'
    local filename = cnt
    chk = string.sub(body.img, 1, 4)
    if string.match(chk, "http") then
        local cmd = 'wget '.. body.img ..' -O ' .. dir .. filename .. '.png'
        os.execute(cmd)
    else
        local tmp = 'tmp.kyc.img' .. filename   
        rc:set(tmp , body.img)
        local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
        os.execute(cmd)
        rc:del(tmp) 
    end
    return filename
end


name = downloadImgs()

local params = {img_url = HOST_NAME..'/face/images/card/' .. name ..'.png', img_name = name  .. '.png'}
local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/ocr', '-d', cjson.encode(params))
if not err then
    ocr = cjson.decode(res.stdout)

    if ocr.responses then
        if ocr.responses[1].fullTextAnnotation then
            long_text = ocr.responses[1].fullTextAnnotation.text
            rc:set('long_text',long_text)
        end
    end

	local laser_code_line = ''
	local laser_code_line_raw = nil
    if long_text then 
        str = string.gsub(long_text, "o", "0")
        str = string.gsub(str, "O", "0")
        str = string.gsub(str, "i", "1")
        str = string.gsub(str, "I", "1")
        str = string.gsub(str, "l", "1")
        str = string.gsub(str, "B", "8")
        str = string.gsub(str, "s", "5")
        str = string.gsub(str, "S", "5")
		
        for line in string.gmatch(str, '[^\r\n]+') do
             
             local clean_line = string.gsub(line, "%-", "")
             clean_line = string.gsub(clean_line, " ", "")
             local index = string.find(clean_line, '%d%d%d%d+')

			 if(index ~= nil)then
                 laser_code_line = clean_line
				 laser_code_line_raw = line
			 end
		end
	end

	if(#laser_code_line == 12) then
		t.laser_code.lc3 = laser_code_line:sub(-2)
		t.laser_code.lc2 = laser_code_line:sub(4,10)
        t.laser_code.lc1 = laser_code_line:sub(1,3)
        ch1 = laser_code_line:sub(0,1)
        if ch1 == "U" then
			t.laser_code.lc1 = string.gsub(t.laser_code.lc1, "U", "J")
        end

		ch2 = laser_code_line:sub(2,2)
		if ch2 == "1" then
			t.laser_code.lc1 = string.gsub(t.laser_code.lc1, "1", "T")
        end

        ch3 = laser_code_line:sub(3,3)
		if ch3 == "D" then
			t.laser_code.lc1 = string.gsub(t.laser_code.lc1, "D", "0")
        end

		t.err = ""
		t.success = true
	else
		if(laser_code_line_raw) then
            t.raw_laser_code = laser_code_line_raw 
            local line = string.gsub(laser_code_line_raw, "%-", " ")
            line = string.gsub(line, "  ", " ")

            local cnt = 0
            local ft = {}
            for ch in  string.gmatch(line,"%S+") do
            cnt = cnt+1
            table.insert(ft,ch)
            end

            if(#ft>2)then
                if(#ft[2]>3)then 
                t.laser_code.lc1 = ft[1]
                t.laser_code.lc2 = ft[2]
                t.laser_code.lc3 = ft[3]
                t.err = ""
                t.success = true
                end
            end

            if(#ft==2)then
                local i1 = 1
                if(#ft[1]>3)then 
                    i1 = 2
                end

                t.laser_code['lc' .. tostring(i1)] = ft[1]
                t.laser_code['lc' .. tostring(i1+1)] = ft[2]
                t.err = ""
                t.success = true
            end

            if(#ft==1)then
                if(#ft[1]>3)then 
                    t.laser_code['lc2'] = ft[1]
                    t.err = ""
                    t.success = true
                end

            end
		end
	end
end

print(cjson.encode(t))
