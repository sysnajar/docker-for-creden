local cjson  = require 'cjson'
local mongo = require "resty-mongol"
local redis = require 'redis'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = { adminEmail = "N/A" } 
end
    
    social = rc:get("social_" .. body.adminEmail .. "_" .. body.id_social) or nil
    
    if social then
        ret = { success = true, error_msg = "email valid", data = cjson.decode(social) }
    else
        ret = { success = false, error_msg = "email invalid" }
    end

print(cjson.encode(ret))