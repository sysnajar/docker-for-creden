local upload = require "resty.upload"
local cjson = require "cjson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local chunk_size = 4096 -- should be set to 4096 or 8192

local file
local name =  tostring(os.time()) 
local file_name  = '/home/creden/work/app/client/face/images/card/' .. name .. ".pdf"
local file_name2 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. ".png"
local file_name3 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. '*.png'
local UPLOAD_PHOTO = false 




 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)


--ngx.say "ok"

local form, err = upload:new(chunk_size)
if not form then
  ngx.say "no form"
  --ngx.log(ngx.ERR, "failed to new upload: ", err)
  --ngx.exit(500)
end

-- ngx.say "ok2"
form:set_timeout(1000) -- 1 sec
file = io.open(file_name, "w+")



local len = 0
while true do
local typ, res, err = form:read()
if not typ then
	ngx.say("failed to read: ", err)
	return
end

if(typ=='header')then
	local file_header = res[2]
	local i, j = string.find(file_header,"filename=")
	if j then
	    local file_name = string.sub(file_header,j+2,string.len(file_header)-1):lower()
		if(file_name:find('.png') or file_name:find('.jpg') or file_name:find('.jpeg') ) then
			UPLOAD_PHOTO = true
	    end
    end

end

if(typ=='body')then
  len = len + #res
  file:write(res)
end
--ngx.say("read1: ", cjson.encode({typ, res}))

if typ == "eof" then
	file:close()
	break
end
end

local typ, res, err = form:read()
--ngx.say("read2: ", cjson.encode({typ, res}))

--ngx.say ("total" .. tostring(len))
-- set img to pdf

--local png = '/home/creden/work/app/client/face/images/card/' .. name .. ".png"
--local jpg = '/home/creden/work/app/client/face/images/card/' .. name .. ".jpg"
--local jpeg = '/home/creden/work/app/client/face/images/card/' .. name .. ".jpeg"
local cmd1 = 'convert -density 300 ' .. file_name .. ' ' .. file_name2.. ' && mogrify -resize 800x '..file_name3
local cmd2 = 'find '.. file_name3  ..' -printf "%f\n" | sort | /home/creden/scripts/rset.lua pdf'

rc:del('pdf')
if(UPLOAD_PHOTO )then
   local size, err = prog('sh', '/home/creden/work/app/server/capi/check_size_pic.sh', file_name)
   if tonumber(size.stdout) < 600 then
      ngx.log(ngx.NOTICE,'1111111111111111')
      ngx.say (cjson.encode({success = false, msg = 'img row size'}))
      return
   end
   --cmd_img_pdf = 'convert '..file_name..' -background white -gravity center -extent 595x842 '..file_name
   --os.execute(cmd_img_pdf)
   cmd_img_pdf = 'convert '..file_name..' '..file_name
   os.execute(cmd_img_pdf)
end

os.execute(cmd1)
os.execute(cmd2)
pdf_str = rc:get('pdf')

local all_img = ''
local image_merge = ''
local page_num = 0
--merge image
for i in string.gmatch(pdf_str, "[^\n]+") do
    page_num = page_num + 1
    all_img = all_img..'/home/creden/work/app/client/face/images/card/'..i..' '
end

if page_num > 1 then 
    image_merge = 'pdf'..name..'.png'
    local cmd_merge = 'convert '..all_img..' -append /home/creden/work/app/client/face/images/card/'..image_merge 
    ngx.log(ngx.NOTICE,cmd_merge)
    os.execute(cmd_merge) 
else
    image_merge = pdf_str
end

ngx.say (cjson.encode({success = (len>0), filename = name .. '.pdf' , pdf_str = pdf_str, image_merge = image_merge, page_num= page_num}))

