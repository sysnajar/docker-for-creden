#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

local bson = require "resty-mongol.bson" 
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
--------
function fromCSV (s)
  s = s .. ','        -- ending comma
  local t = {}        -- table to collect fields
  local fieldstart = 1
  repeat
    -- next field is quoted? (start with `"'?)
    if string.find(s, '^"', fieldstart) then
      local a, c
      local i  = fieldstart
      repeat
        -- find closing quote
        a, i, c = string.find(s, '"("?)', i+1)
      until c ~= '"'    -- quote not followed by quote?
      if not i then error('unmatched "') end
      local f = string.sub(s, fieldstart+1, i-1)
      table.insert(t, (string.gsub(f, '""', '"')))
      fieldstart = string.find(s, ',', i) + 1
    else                -- unquoted; find next comma
      local nexti = string.find(s, ',', fieldstart)
      table.insert(t, string.sub(s, fieldstart, nexti-1))
      fieldstart = nexti + 1
    end
  until fieldstart > string.len(s)
  return t
end
function escapeCSV (s)
  if string.find(s, '[,"]') then
    s = '"' .. string.gsub(s, '"', '""') .. '"'
  end
  return s
end
function toCSV (tt)
  local s = ""
-- ChM 23.02.2014: changed pairs to ipairs 
-- assumption is that fromCSV and toCSV maintain data as ordered array
  for _,p in ipairs(tt) do  
    s = s .. "," .. escapeCSV(p)
  end
  return string.sub(s, 2)      -- remove first comma
end

-------

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    -- body = cjson.decode(ngx.req.get_body_data())
    body = ngx.req.get_uri_args()
else
   body   = {}
end


conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)


if not ok then
    ret = {success = false, error_msg = err}
else

local d1_month = body.fake_d1_month 
local d1_day = body.fake_d1_day 
local d1_year = body.fake_d1_year 
local d2_month = body.fake_d2_month 
local d2_day = body.fake_d2_day 
local d2_year = body.fake_d2_year  
local usrname = body.getname 
db = conn:new_db_handle("edoc")
col2 = db:get_col("edoc_list");

 local targetDate = (os.time({year=tonumber(d1_year),month=tonumber(d1_month),day=tonumber(d1_day),hour=0, sec=0})*1000)
 local dtm  = bson.get_utc_date(targetDate)

 local targetDate2 = (os.time({year=tonumber(d2_year),month=tonumber(d2_month),day=tonumber(d2_day),hour=23, sec=59})*1000)
 local dtm2  = bson.get_utc_date(targetDate2)
    

function get_incomplete_list(r)
local ret = ''
local t = {}

for _, signer in ipairs(r.signers) do
       local complete = true

       for __, field in ipairs(r.fields) do
	   if(field.status==0 and field.email==r.email)	then
	       complete = false
	       break
	   end
       end

       if(not conmplete) then
           table.insert(t,signer.email)
       end
end



for i, email in ipairs(t) do
  ret = ret .. email ..'	'
end

return ret
end


 local user_list = col2:find({createdDtm = {["$gte"] = dtm, ["$lt"] = dtm2},owner=body.getname})
 local new_list = {}
 ngx.header["Content-Type"] = 'application/octet-stream';
 ngx.header["Content-Disposition"] =  'form-data; name="myFile"; filename="report.csv"';
print('Report on, "'..d1_day.."/"..d1_month.."/"..d1_year..'", , , ')
print(', , , , ')
 print('Transaction ID, Create On, Status, Incomplete Signer, Transaction Complete on')
    for i, v in user_list:pairs() do 
        v._id = nil
       local status_re = ''
        if(v.status == 1) then
          status_re = "Complete"
        else
          status_re = "Incomplete"
        end
      
        if (v.createdDtm ~= nil and v.complete_dtm ~= nil) then
        local create = os.date("%d/%m/%Y", tonumber(v.createdDtm)/1000)
        local complete2 = os.date("%d/%m/%Y",tonumber(v.complete_dtm)/1000) 
          local v2 = {v.txid or v.id, create,status_re, get_incomplete_list(v) ,complete2}
          -- print(toCSV(v2))
          local csv = toCSV(v2)
          -- rc:set('csv_report',csv)
          -- local cmd = 'redis-cli get csv_report > /home/creden/work/app/client/face/images/card/report.csv'
          -- os.execute(cmd)
          -- ngx.header["Content-Type"] = 'application/octet-stream';
          -- ngx.exec('/face/images/card/report.csv')
        print(csv)
      end
    end

  -- x = toCSV(new_list)
   -- print(x)
   -- print(cjson.encode(new_list))

    -- ret = {success = true, error_msg = "complete", data = new_list }
end

-- print(cjson.encode(ret))



