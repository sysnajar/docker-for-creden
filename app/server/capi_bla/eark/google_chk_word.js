Nightmare = require ('nightmare');
const nightmare = Nightmare({ show: false, waitTimeout :7000 });
setTimeout(function(){ process.exit(0); }, 15000);

nightmare
  .goto('https://www.google.co.th/')
  .type('#lst-ib', process.argv[2])
  .click('input[type="submit"]')
  .wait(1000)
  .evaluate(() => {
   var ret = document.querySelector('a#fprsl').text;
   if(ret && ret.indexOf('จ.')==0)
      {ret = ret.substring(2)} 
    return ret;
  })
  .end()
  .then(console.log)
  .catch((error) => {
    console.error('Search failed:', error);
  });
