#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    --body = ngx.req.get_body_data()
    --body = cjson.decode(body)
else
   body  = {email = 'creden', password = '135790', compCode='creden'} 
end

function utc_month(month, _yyyy)
local year = _yyyy or tonumber(os.date("%Y"))
local tmp  = os.time {year=year, month=month, day=1 , hour=0, min=0, sec=0} * 1000 
return bson.get_utc_date(tmp)
end

   body  = {email = 'creden', password = '135790', compCode='creden'} 

   conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
    s = col:find({compCode = body.compCode , 
	createdDtm = {['$gte'] = utc_month(1, 2018), ['$lt'] = utc_month(1, 2019)} 
    }, {createDate = 1}) 

    col2 = db:get_col("credit_history")
    s2 = col2:find({adminEmail = body.compCode 
	, creditDateTime = {['$gte'] = utc_month(1, 2018), ['$lt'] = utc_month(1, 2019)
    } 
    }, {creditDate = 1, credits = 1, creditPrice = 1}) 

    local txKyc   = {0,0,0,0,0,0,0,0,0,0,0,0}
    local txDoc   = {0,0,0,0,0,0,0,0,0,0,0,0}
    local credits = {0,0,0,0,0,0,0,0,0,0,0,0}

    if s and s2 then 
        -- group tx (kyc & edoc) by month
		for k, v in s:pairs() do
              v._id = nil
			  local month = tonumber(v.createDate:sub(1,2))
			  txKyc[month] = txKyc[month]+1
		end

		-- group credits by month
        for k, v in s2:pairs() do
			  local month = tonumber(v.creditDate:sub(6,7))
			  credits[month] = credits[month] + v.credits
		end

        ret = {success = true, error_msg = "complete", data = {txKyc=txKyc, txDoc=txDoc, credits=credits}}
    else
        ret = {success = false, error_msg = "err"}
    end
end

--print(#ret.data)
print(cjson.encode(ret))
