#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local object_id = require "resty-mongol.object_id"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end

function check_queue_document(arr, myusername)
  local flag = true

  for i, v in ipairs(arr.signers) do

    if v.is_group == true and v.user_group then

      for i2, v2 in ipairs(v.user_group) do
          if myusername == v2.email and v.needToSign == false and arr.status == 0 then
            flag = false
          end
      end

    else

      if myusername == v.email and v.needToSign == false and arr.status == 0 then
        flag = false
      end

    end

    if (flag == false) then
      break
    end
  
    if v.is_inspector == true then

      for i3, v3 in ipairs(arr.fields) do
        if v.email == v3.email and v3.toolName == "Approve" and v3.status == 0 then
          flag = false
          break
        end
      end

      if (flag == false) then
        break
      end

    end

  end
  
  return flag
end

function preload_users()	
  all_users = col2:find({ })
  local c = col_comment:find({ })
  for i, v in c:pairs() do
    v._id = nil
    table.insert(all_comments, v)
  end
end

function filter_comment(arr, key)
  local tbl = 0
  for i, v in ipairs(arr) do
    if v.doc_id == key then
      tbl = tbl + 1
    end
  end
  return tbl
end

function check_read_msg(arr, key)

  local tbl = 0
  for i, v in ipairs(arr) do
    local flag = false
    if v.doc_id == key and #v.user > 0 and #v.tag > 0 then

      for i3, v3 in ipairs(v.tag) do
        if v3.email_tag == session.data.username then
          flag = true
        end
      end

      for i2, v2 in ipairs(v.user) do
        if v2.read == false and v2.email == session.data.username then
          flag = true
          break
        else
          flag = false
        end
      end

      if flag == true then
        tbl = tbl + 1
      end
    end
  end
  return tbl
end


function count(cur)
	local ret = 0
	for i,v in cur:pairs() do
	    ret = ret + 1
	end
	return ret
end

function convertHexStringToNormal( str )
  return (str:gsub('..', function (cc)
  return string.char(tonumber(cc, 16))
  end))
end

function match_pos(doc, pos_id) 
  for i,signer in ipairs(doc.signers or {}) do
      if(signer.position_id and signed.position_id==pos_id)then
        return true
      end
  end
  return false
end


function match_remove(signers, owner, rm)
  local status = false

  if signers then
    for i, v in ipairs(signers) do
      if v.is_group == true then
        
        for i2, v2 in ipairs(v.user_group) do
          if v2.is_remove == true and v2.email == body.email then
            status = true
            break
          end
        end

      else

        if v.is_remove == true and v.email == body.email then
          status = true
          break
        end
      end

    end
  end

  if owner == body.email and rm == true then
    status = true
  end

  return status
end

function chk_signer(signers)
  local user = false
  if signers then
    for i, v in ipairs(signers) do
      if v.is_group == true then
        for i2, v2 in ipairs(v.user_group) do
          if v2.email == body.email then
            user = v2
          end
        end
      else
        if v.email == body.email then
          user = v
        end
      end
    end
  end
  return user
end

function timeout_show_trash(arr)
  local current_time = bson.get_utc_date(ngx.now() * 1000).v
  local exp_time = ""
  local user = chk_signer(arr.signers)
  local show = true

  if user and user.is_remove == true then
    exp_time = user.time_remove + (24*60*60*1000)
    if current_time > exp_time then
      show = false
    end

  elseif arr.owner_is_remove == true then
    exp_time = arr.owner_time_remove + (24*60*60*1000)
    if current_time > exp_time then
      show = false
    end
  end
  return show
end

function match_myorder(signers) 
  local order = 0
  for i, v in ipairs(signers) do
    if v.is_group == true then
      for i2, v2 in ipairs(v.user_group) do
        if v2.email == body.email then
          order = v.order_set
          break
        end
      end
    else
      if v.email == body.email then
        order = v.order_set
        break
      end
    end
  end
  return tostring(order)
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else

	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list")
  col_comment = db:get_col("edoc_comment")

  db2 = conn:new_db_handle("dopa")
  col2 = db2:get_col("esig_user")

  if(not body.email or body.email=='' or type(body.email) == 'userdata')then  
       print(cjson.encode({success = false, error_msg = "session expired"}))
	    ngx.exit(ngx.HTTP_OK)
  end	

	if(not session.data.username) then
		print(cjson.encode({success = false, error_msg = "session expired"}))
	  ngx.exit(ngx.HTTP_OK)
	else
		if(body.email~=session.data.username) then
			ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
			body.email = session.data.username
		end
	end	

  all_users = {}
  all_comments = {}
  preload_users()

  local col_folder2 = db:get_col('folders2')
  local doc = ""
  if body.cat ~= "folder" then
    doc = col_folder2:find({ owner_folder = body.email })
  else
    doc = col_folder2:find({ owner_folder = body.email, _id = object_id.new(convertHexStringToNormal(body.id_folder)) })
  end
  local doc_ids = {}

  if doc then
    for k, v in doc:pairs() do
      v._id = tostring(v._id)
      for k2, v2 in ipairs(v.list_doc) do
        id_doc_folder = col:find_one({ _id = object_id.new(convertHexStringToNormal(v2)) })
        if id_doc_folder.id then
          table.insert(doc_ids, tostring(id_doc_folder.id))
        end
      end
    end

    if #doc_ids == 0 then
      doc_ids = {"999999999"}
    end

  end


  s = {}  
  query, query2, query3 = {}, nil

  if body.cat == "owner" then
    query = { owner = body.email, is_template={['$ne']=true}, id = {['$nin']= doc_ids} }

  elseif body.cat == "self" then
    query = { owner = body.email, signers={['$elemMatch'] = {email = body.email}}, is_template={['$ne']=true}, id = {['$nin']= doc_ids} }
  

  elseif body.cat=="template" then
    query = {owner=body.email, is_template=true}

  
  elseif body.cat=="cancel" then
    query = { owner=body.email, status = 3, id = {['$nin']= doc_ids} }
    query2 = { owner ={['$ne']=body.email}, status = 3, signers={['$elemMatch']={email = body.email}}, id = {['$nin']= doc_ids} }
    query3 = { owner ={['$ne']=body.email}, status = 3, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, id = {['$nin']= doc_ids}}
    -- query2 = { id = {['$in']= select_folder} }

  elseif body.cat=="folder" then
    query = { owner = body.email, is_template={['$ne']=true}, id = {['$in']= doc_ids} }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={email = body.email}}, id = {['$in']= doc_ids} }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, id = {['$in']= doc_ids}}

  elseif(body.cat == "public") then
    query = { owner = body.email, is_template={['$ne']=true}, setting_pdf={['$elemMatch']={ tag_folder = "Public" }}  }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email }}, setting_pdf={['$elemMatch']={ tag_folder = "Public" }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, setting_pdf={['$elemMatch']={ tag_folder = "Public" }} }

  elseif(body.cat == "private") then
    query = { owner = body.email, is_template={['$ne']=true}, setting_pdf={['$elemMatch']={ tag_folder = "Private" }} }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email }}, setting_pdf={['$elemMatch']={ tag_folder = "Private" }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, setting_pdf={['$elemMatch']={ tag_folder = "Private" }} }

  elseif(body.cat == "confidential") then
    query = { owner = body.email, is_template={['$ne']=true}, setting_pdf={['$elemMatch']={ tag_folder = "Confidential" }} }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email }}, setting_pdf={['$elemMatch']={ tag_folder = "Confidential" }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, setting_pdf={['$elemMatch']={ tag_folder = "Confidential" }} }

  elseif(body.cat == "trash") then
    -- query = {id = {['$in']  = deleted_ids}}
    query = { owner = body.email, is_template={['$ne']=true}, owner_is_remove = true }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email, is_remove = true }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email, is_remove = true }}}} }
  end  


  if body.cat == "all" or body.cat == "signer" then
    query = { owner = body.email, is_template={['$ne']=true}, id = {['$nin']= doc_ids} }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={email = body.email}}, id = {['$nin']= doc_ids} }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, id = {['$nin']= doc_ids}}
  end

  s  = col:find(query)
	s2 = {} 

	for k, v in s:pairs() do 
    -- local count_comment = count(col_comment:find({doc_id = v.id}))
    local count_comment = filter_comment(all_comments, v.id)
    v.comment = count_comment

    -- local comment =count(col_comment:find({doc_id = v.id , user = {['$elemMatch'] = {read = false,email = session.data.username}} , tag = {['$elemMatch'] = {email_tag = session.data.username}} }))
    local comment = check_read_msg(all_comments, v.id)
    if comment > 0 then
      v.read_msg = false
    else
      v.read_msg = true
    end
    
    user_data = all_users[v.owner] 
    
    if user_data and user_data.adminFisrtname and user_data.adminLastname then
      v.owner_name = user_data.adminFisrtname .." ".. user_data.adminLastname or ''
    else
      v.owner_name = v.owner
    end

    if body.cat == "self" then
      if #v.signers == 1 and v.signers[1].email == body.email and not match_remove(v.signers, v.owner, v.is_remove) then
        table.insert(s2, v) 
      end

    elseif body.cat == "trash" then
      table.insert(s2, v) 
    else
      if not match_remove(v.signers, v.owner, v.is_remove) then
        table.insert(s2, v) 
      end
    end

    -- table.insert(s2, v) 
  end





  if (true and query2) then
    q2 = col:find(query2)
    for k2, v2 in q2:pairs() do 

      -- local count_comment = count(col_comment:find({doc_id = v2.id}))
      local count_comment = filter_comment(all_comments, v2.id)
      v2.comment = count_comment

      -- local comment = count(col_comment:find({doc_id = v2.id , user = {['$elemMatch'] = {read = false,email = session.data.username}} , tag = {['$elemMatch'] = {email_tag = session.data.username}}}))
      local comment = check_read_msg(all_comments, v2.id)
      if comment > 0 then
        v2.read_msg = false
      else
        v2.read_msg = true
      end
      
      user_data = all_users[v2.owner] 

        if user_data and user_data.adminFisrtname and user_data.adminLastname  then
          v2.owner_name = user_data.adminFisrtname .." ".. user_data.adminLastname or ''
        else
          v2.owner_name = v2.owner
        end
      
      if body.cat == "trash" then
        table.insert(s2, v2) 
      else
        if not match_remove(v2.signers, v2.owner, v2.is_remove) then
         table.insert(s2, v2) 
        end
      end
      -- table.insert(s2, v2) 
    end
  end

  if (true and query3) then
    q3 = col:find(query3)
    for k3, v3 in q3:pairs() do 
      local count_comment = filter_comment(all_comments, v3.id)
      v3.comment = count_comment

      -- local comment = count( col_comment:find({doc_id = v3.id , user = {['$elemMatch'] = {read = false,email = session.data.username}} , tag = {['$elemMatch'] = {email_tag = session.data.username}} }))
      local comment = check_read_msg(all_comments, v3.id)
      if comment > 0 then
        v3.read_msg = false
      else
        v3.read_msg = true
      end

        user_data = all_users[v3.owner] 
        if user_data and user_data.adminFisrtname and user_data.adminLastname  then
          v3.owner_name = user_data.adminFisrtname .." ".. user_data.adminLastname or ''
        else
          v3.owner_name = v3.owner
        end
      if body.cat == "trash" then
        table.insert(s2, v3) 
      else
        if not match_remove(v3.signers, v3.owner, v3.is_remove) then
          table.insert(s2, v3) 
        end
      end
      -- table.insert(s2, v3) 
    end
  end

	tmp = {}

	for k, v in ipairs(s2) do
    v._id = tostring(v._id)
    local flag = check_queue_document(v, session.data.username)


    if session.data.username == v.owner or flag then

      if v.createdDtm then
        v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
      end
  
      for _, p in ipairs(v.signers) do
        p.password = ""
      end 
        -- v.imageFiles = nil
        v.imageMerge = nil
        table.insert(tmp,v)
    end



  end

  local tmp2 = {}
  local myorder = 0
  for i, v in ipairs(tmp) do

    local exp = timeout_show_trash(v)
    if exp == true then
      if v.use_signing_order == true and v.status == 0 then

        myorder = match_myorder(v.signers)
        if myorder == "" then 
          myorder = "0" 
        end
        
        if tonumber(v.current_order) >= tonumber(myorder) then
          table.insert(tmp2, v)
        end
      else
        table.insert(tmp2, v)
      end
    end
  end
  

  ret = { success = true, error_msg = "complete", data = tmp2, cat = body.cat, query = query }
  -- ret = { success = true, error_msg = "complete", data = {}, cat = body.cat, query = query, xx = doc_ids }
  print(cjson.encode(ret))
end
