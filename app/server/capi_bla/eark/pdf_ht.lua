#!/usr/bin/env lua
local session = require "resty.session".open({secret = "4321"})

--ngx.say("<html><body>Session was started by <strong>", session.data.username or "Anonymous", "from", session.data.custCode or "Unknown Company")

local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    body = ngx.req.get_uri_args()
else
   body  = {adminEmail = arg[1],file = "1530097604161",stamp = false} 
end


have_file = false 
err_msg = ''
response = {error = "NO PERMISSION"}

username = session.data.username or 'N/A'
ngx.log(ngx.NOTICE, "username = " .. username)

    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = "connection error"}
    else
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        doc = col:find_one({id = body.file})

        --check 3.1
		if(doc.owner == username)then
           have_file = true
		end

        --check 3.2
	    for i, person in ipairs(doc.signers) do
            if(person.email == username)then 
				have_file = true 
			end
		end
		
        --check 3.3
		if(body.key and body.key~="" and doc.password and body.key==doc.password )then
           have_file = true
		end

		have_file = true 




		if have_file then
		   file_name = body.file
			if body.stamp then
				file_name =  file_name..'-stamp'
			end
			ngx.exec('/face/images/card/'..file_name..'.pdf')
		else
			ngx.say(cjson.encode(response))
		end

end
