#!/usr/bin/env lua

function init()
if(#arg > 0  )then
  cjson = require 'cjson'
  mongo = require "resty-mongol"
  conn = mongo()
  conn:set_timeout(1000)
  ok, err = conn:connect('127.0.0.1',27017)
  db = conn:new_db_handle("test")
  users = db:get_col("user") 
  fb = db:get_col("facebook") 
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(email)
	local t  = users:find_one({email = email , activated = 1})
	if(t) then t._id = nil;  t.password = nil ; t.profilePicture = nil end

	local t2  = fb:find_one({email = email})
	if(t2) then t2._id = nil  end

return {user = t , kyc = t2 and t2.kyc or  nil, death = t2 and t2.kyc.death or nil}
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
