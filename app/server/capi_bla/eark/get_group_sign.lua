#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end


conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("group_sign")
    local col_esig = db:get_col("esig_user")
    local c, err = col:find({ company_id = body.company_id })
    local list = {}
   
    if c then
        for k, v in c:pairs() do 
            v._id = tostring(v._id)
            v.company_id = tostring(v.company_id)

            for k2, v2 in ipairs(v.group_sign) do
                local e, err = col_esig:find_one({ adminEmail = v2.email })
                e.adminFisrtname = e.adminFisrtname or ""
                e.adminLastname = e.adminLastname or ""
                v2.name = e.adminFisrtname .. " " .. e.adminLastname
            end
            v.fav = false
            table.insert(list, v) 
        end

    col = db:get_col("address_book")
    add = col:find({imported = true})
    --table.insert(list, {fa
	--v=false,_id=os.time()..'',created_dtm=tostring(os.time()), group_name="eark group", company_id="001", group_sign = {{email="mail1@gmail.com",name="name1"}}})
   
   all_groups = {}
  for i, g in add:pairs() do
     local group_name = g.OrgDescription
	 local group = all_groups[group_name] or {group_name = group_name, fav=false, group_sign={}, _id=tostring(os.time()), created_dtm=tostring(os.time()), company_id="999" }
	 all_groups[group_name] = group
	 ngx.log(ngx.INFO, 'Add group'..group_name)

	 table.insert(group.group_sign, {email=g.email,name=g.EmpName})
  end
  


  -- insert each group to list
   ngx.log(ngx.INFO, 'Loop' .. #all_groups)
  for i,g in pairs(all_groups) do
     ngx.log(ngx.INFO, i, g.group_name)
	table.insert(list, g)
  end


        ret = { success = true, data = list, error_msg = "get group success", error_code = "2016" }
    else
        ret = { success = false, error_msg = "get group unsuccess", error_code = "2017" }
    end
end
print(cjson.encode(ret))
