#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {selected_date='12/02/2021',mode="walkin",selected_time="13:00",period="afternoon",email="amorrut@creden.co",exp_month="1"}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("slot_certificates")
    
    body.mode = (body.mode=='walkin' or body.mode=='messenger') and
	body.mode or assert(false,'Invalid mode')

    query = { date_slot = body.selected_date,mode=body.mode}
    find_slot = col:find_one(query)
    ret = {}
    if find_slot then
        find_slot._id = tostring(find_slot._id)
        local time_slot = {}
        local a = find_slot.afternoon
        if body.period == "afternoon" then
            for i,v in ipairs(a) do 
                if tostring(v.slot.time) == tostring(body.selected_time) then
                    v.slot.available_min = tonumber(v.slot.available_min) + 1
                end
                
                table.insert(time_slot,v)
            end
            
         local update_cmd = {["$set"] = { afternoon = time_slot}}
         n,err = col:update(query, update_cmd, 0, 0, true)
         if n then
            ret.slot = "update"

         else
            ret.slot = "not update"
         end
        else
            for i,v in ipairs(find_slot.morning) do 
                if tostring(v.slot.time) == tostring(body.selected_time)  then
                    v.slot.available_min = tonumber(v.slot.available_min) + 1
                end
               
                table.insert(time_slot,v)
            end

            local update_cmd = {["$set"] = { morning = time_slot}}
            n,err = col:update(query, update_cmd, 0, 0, true)
            -- if n then
            if n then
                ret.slot = "update"
    
             else
                ret.slot = "not update"
             end

        end

        
        math.randomseed(os.clock()*100000000000)
        id_booking = ''
        for i=1,4 do
            a = math.random(10, 99)       
            id_booking = id_booking .. a
        end
        local data = {
            booking_id = id_booking,
            booking_owner = body.email,
            booking = true,
            booking_date = body.selected_date,
            booking_time = body.selected_time,
            booking_mode = body.mode,
            booking_status = 0,
            booking_exp_month = body.exp_month,
        }
        col2 = db:get_col("booked_slot")
        u,err = col2:insert({data})

        if u then
            ret = { success = true, msg = "success", booking_id= id_booking}
        else
            ret = { success = false, msg = "not success"}
        end

    else
        ret = { success = false, msg = "no data" }
    end

   
end

print(cjson.encode(ret))
