#!/usr/bin/env lua
local cjson = require 'cjson'
redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local body = nil
local HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {key = arg[1]} 
end
key = body.key..'cookie'
header = rc:get(key)
header = string.gsub(header,"; Path=/ledweb/; HttpOnly", "")
os.execute('curl --header "Cookie: '..header..'" -X POST http://ledwebsite.led.go.th/ledweb/WEB3Q010_1ReportServlet > /home/creden/work/app/client/face/images/pdf/'..key..'.pdf')
t = {success = true, data = { link = HOST_NAME..'/face/images/pdf/'..key..'.pdf' }}
print(cjson.encode(t))
