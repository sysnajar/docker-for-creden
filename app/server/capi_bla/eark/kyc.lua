#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

local args = nil
local ret = {success = false, error_msg = "Unspecify error"
  ,death = {stCode = '-1', stDesc = 'N/A', valid = false} 
}

local card_ok,death_ok = false,false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    args = cjson.decode(ngx.req.get_body_data())
else
    args  = {firstname = 'คุณากร' , lastname = 'จันทร์ชื่น' , citizenIdB = 'ME0117526468' , citizenIdF = '1100701382071' , dob = '1991-08-11' } 
end

--convert 1979-10-02 to 02/10/2522
function dob1(dob)
local ret = dob:gsub("-","")

return ret:sub(7,8) .. '/' .. ret:sub(5,6) ..  '/' .. tostring(tonumber(ret:sub(1,4)) + 543)
end

--convert 1979-10-02 to 25221002
function dob2(dob)
local ret = dob:gsub("-","")

return tostring(tonumber(ret:sub(1,4)) + 543) .. ret:sub(5,6) .. ret:sub(7,8)
end

--dob = dob1(args.dob)

-- service1 ID card check
local params   = 'national_id=' .. args.citizenIdF ..'&first_name=' .. args.firstname .. '&last_name=' .. args.lastname .. '&date_of_birth=' ..  dob1(args.dob)  .. '&cvv_code=' ..args.citizenIdB
local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

if(not err)then
	--print(res.stdout)
	local tmp = cjson.decode(res.stdout)
	if(tmp) then
	  card_ok = tmp.status	
      ret.success   = tmp.status
	  ret.error_msg = tmp.message
    end  
end

-- service2 Death status
local tmp_stdin = prog.stdin

if(card_ok)then
  res, err = prog('/home/creden/work/app/server/capi/death.lua', args.citizenIdF, dob2(args.dob))
  if(not err)then
	 prog.stdin = res.stdout 
     res, err = prog('/home/creden/work/app/server/capi/f_death.lua')
	 ret.death = cjson.decode(res.stdout)
  end
end


-- service3 Tax
if(ret.death.valid)then
	prog.stdin = tmp_stdin 
	res, err = prog('curl', '-s', '--data', 'searchType=null&effDate=null&nid=' .. args.citizenIdF ..'&taxYear=2559&fName='.. args.firstname ..'&lName='.. args.lastname, 'http://refundedcheque.rd.go.th/itp_x_tw/SearchTaxpayerServlet')
	ret.tax1 = cjson.decode(res.stdout)

	res, err = prog('curl', '-s', '--data', 'searchType=null&effDate=null&nid=' .. args.citizenIdF ..'&taxYear=2558&fName='.. args.firstname ..'&lName='.. args.lastname, 'http://refundedcheque.rd.go.th/itp_x_tw/SearchTaxpayerServlet')
	ret.tax2 = cjson.decode(res.stdout)
end

if(card_ok) then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = err}
    else
        ret.id_card = {firstname = args.firstname, lastname = args.lastname, citizenIdF = args.citizenIdF, citizenIdB = args.citizenIdB, sex = args.sex}

        if (ret.tax1) then
            ret.tax1["formSendDocDetail"] = ((type(ret.tax1.formSendDocDetail) == 'userdata') and '' or ret.tax1.formSendDocDetail)
        end
        if (ret.tax2) then
            ret.tax2["formSendDocDetail"] = ((type(ret.tax2.formSendDocDetail) == 'userdata') and '' or ret.tax2.formSendDocDetail)
        end

        isVerified = false
        if (ret.death.valid) then
          isVerified = true
        end

        db = conn:new_db_handle("test")
        col = db:get_col("kyc")
        col_vdo = db:get_col("vdo")
        s = col_vdo:find_one({id=args.id})

        today = os.date('*t',os.time())
        mm = tostring(today.month)
        dd = tostring(today.day)
        dir = today.year .. ((#mm==1) and '0'..mm or mm) .. ((#dd==1) and '0'..dd or dd)
        json = {}
        if s then
            json = {filename = s.filename, comp_id = s.comp_id, id = s.id, word = s.word, time = s.time}
            json.kyc = {faceMatched = tonumber(args.face_pct), isVerified = isVerified}
            --vdo json
            local res, err = prog('sh', '/home/creden/work/app/server/capi/save_json.sh', s.comp_id, dir , s.filename, cjson.encode(json))
        end 

        update = {["$set"] = {id = args.id, isVerified = isVerified , kyc = ret}}
        u, err = col:update({email=args.id}, update, 1, 0, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            ret = {success = ret.success, error_msg = ret.error_msg, death = ret.death, tax = {tax1 = ret.tax1, tax2 = ret.tax2}}
        end
    end
end

print(cjson.encode(ret))


