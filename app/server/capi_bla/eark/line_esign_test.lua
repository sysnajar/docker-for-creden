#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local common = require "common"
local line_esign = require "line_esign_noti"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
end


-- local res, user_id = line_esign.get_line_user_id('nutp10.1@gmail.com')

local time_stamp = os.time() + (7 * 3600)
local date = os.date('%d/%m/%Y@%H:%M', time_stamp)

-- local message = "คุณมีเอกสารที่ต้องเซ็นรออยู่\n\nคลิ๊กเพื่อเซ็นเอกสารหรืออ่านเอกสาร\nwww.google.com"

-- if res then
--     line_esign.push(user_id, message)
-- end

print(date)