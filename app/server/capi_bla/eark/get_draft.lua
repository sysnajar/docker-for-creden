#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_uri_args()
    ngx.log(ngx.NOTICE, cjson.encode(body.id))
else
   body  = {email = arg[1],cat = arg[2]}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else

	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list_draft")

  s = {}  
  query = { id = body.id }
  s  = col:find(query)

	tmp = {}

  for k, v in s:pairs() do 
    v._id = tostring(v._id)
    tmp = v
  end

  ret = { success = true, error_msg = "complete", data = tmp }
  print(cjson.encode(ret))
end
