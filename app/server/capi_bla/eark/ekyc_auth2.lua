#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout_fatal = false

if(ngx)then
    ngx.req.read_body()
    body = ngx.req.get_post_args()
    apiKey =  body.apiKey or ngx.req.get_headers().apiKey
    print = ngx.say
else
   body  = {} 
end

res = {success = false}
--alert line
--os.execute('sh /home/creden/work/app/server/capi/linedbd.sh "'.. body.ref_no ..' start ekyc" &')

function connect_mongo()
    conn = mongo()
    conn:set_timeout(10000)
    ok,  err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end

function chk_usr()
    local chk = nil
    if connected then
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey or '@#$%?'})   
        if s then
            chk = true 
            body.username = s.adminEmail
        else
            ret.error_msg = 'Authentication Fail'
            print(cjson.encode(ret))
            ngx.exit(ngx.HTTP_OK)
        end
    end 
    return chk
end

function chk_txid()
    local chk = nil
    if connected then
        local col = db:get_col("ekyc")
        local s = col:find_one({txid = body.txid, compCode = body.username}) 
        if not s then
            chk = true 
        else
            ret.error_msg = 'Existing txid'
            print(cjson.encode(ret))
            ngx.exit(ngx.HTTP_OK)
        end
    end 
    return chk
end

function gen_code()
    body.code = ngx.md5(body.txid)
    return body.code
end

conn = nil
connected = connect_mongo().success
db = (connected==true) and conn:new_db_handle("dopa") or nil
if chk_usr() and chk_txid() then
    --if connected then
        gen_code()
        body.status = 'NORMAL'
        local col = db:get_col("sdk_code")
        i, err_i = col:update({txid=body.txid},body, 1, 0, true)
        if not i then
            ret.error_msg = err_i
         else

			--if(body.numberx ~= nil) then
		 
           ngx.log(ngx.NOTICE,"ekyc_auth2= " .. tostring(body.username))
          

            if(body.card_reader_id == nil or body.card_reader_id == "" or body.new_parameter == nil or body.new_parameter == "") then

			  if(body.numberx~=nil and body.psid~=nil)then
				
              ngx.redirect('/ekyc/app.html#/sdk?code='..body.code..'&u='..body.numberx..'&psid='..body.psid)
			  else
		        if(body.username == "orisma@creden.co") then		
                 ngx.redirect('/ekyc_orima/app.html#/sdk?code='..body.code..'&new_parameter=test')
                else
			     ngx.redirect('/ekyc/app.html#/sdk?code='..body.code..'&new_parameter=test')
                end
              end
		  elseif(body.numberx ~= "" and body.card_reader_id == nil) then
			  
			 ngx.redirect('/ekyc/app.html#/sdk?code='..body.code..'&u='..body.numberx)
		  else
                  ngx.redirect('/ekyc/app.html#/sdk?code='..body.code..'&card_reader_id='..body.card_reader_id..'&new_parameter='..body.new_parameter)
            end
		
		--end
            -- ngx.redirect('/ekyc/app.html#/sdk?code='..body.code..'&card_reader_id='..body.card_reader_id)
            --ret.success = true
            --ret.ekyc_url = HOST_NAME..'/ekyc/app.html#/sdk?code='..body.code
         end
         print(cjson.encode(ret))
    --end
end
