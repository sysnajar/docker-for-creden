local upload = require "resty.upload"
local cjson = require "cjson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local prog1 = exec.new('/tmp/exec.sock')
local prog1b = exec.new('/tmp/exec.sock')

prog.timeout_fatal = false
prog1.timeout = 1000 * 60 * 30
prog1.timeout_fatal = false
prog1b.timeout_fatal = false
local chunk_size = 4096 -- should be set to 4096 or 8192

local config = {dpi = 300}

--
function str_split(str, sep)
  if sep == nil then
    sep = '%s'
  end 

  local res = {}
  local func = function(w)
    table.insert(res, w)
  end 

  string.gsub(str, '[^'..sep..']+', func)
  return res 
end

function change_order(c)
  local t = str_split(c,'\n')
  local t2 = {}
  for i,v in ipairs(t) do
    if(i>1)then	
      table.insert(t2,v)
    end	 
  end
  table.insert(t2,t[1])
  t = t2
  t[#t] = string.gsub(t[#t],".png", "-".. (#t-1) .. ".png")

  -- for i,v in ipairs(t) do
  --   ngx.say(i..' '..v)
  -- end
  -- ngx.say("--print string---")
  local res = table.concat(t,"\n")
  return res
end

function rand(len)
local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
local t = {}
local cnt = 0
 
 local i = 1
 repeat
     local f = assert (io.popen (cmd, 'r'))
	 local line = f:read('*all'):match("%d+")
	 f:close()
	 cnt = cnt + #line
	 table.insert(t, line)
 until cnt>len

		
 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
 return ret
end
--



local file
local name = rand(8) .. tostring(os.time()) 
local file_name  = '/home/creden/work/app/client/face/images/card/' .. name .. ".pdf"
local file_name2 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. ".png"
local file_name3 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. '*.png'
local UPLOAD_PHOTO = false 

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

local form, err = upload:new(chunk_size)
if not form then
  ngx.say "no form"
  --ngx.log(ngx.ERR, "failed to new upload: ", err)
  --ngx.exit(500)
end

--return number of PDF pages or 1 if the file is image file (png,jpg)
function get_page_count(file)

if(UPLOAD_PHOTO)then return 1 end


local res, err = prog1('pdftk', file ,  'dump_data'  )
prog1.stdin = res.stdout
res,err = prog1('grep', 'NumberOfPages')
prog1.stdin = nil

if(not err)then
   return tonumber(res.stdout:match('%d+'))
else
   return 0	
end

end

form:set_timeout(1000) -- 1 sec
file = io.open(file_name, "w+")



local len = 0
while true do
local typ, res, err = form:read()
if not typ then
	ngx.say("failed to read: ", err)
	return
end

if(typ=='header')then
	local file_header = res[2]
	local i, j = string.find(file_header,"filename=")
	if j then
	    local file_name = string.sub(file_header,j+2,string.len(file_header)-1):lower()
		if(file_name:find('.png') or file_name:find('.jpg') or file_name:find('.jpeg') ) then
			UPLOAD_PHOTO = true
	    end
    end

end

if(typ=='body')then
len = len + #res
file:write(res)
end
--ngx.say("read1: ", cjson.encode({typ, res}))

if typ == "eof" then
file:close()
break
end
end

local typ, res, err = form:read()


ngx.log(ngx.NOTICE,"FINISHED CMD UPLOADING")


--ngx.say("read2: ", cjson.encode({typ, res}))

--ngx.say ("total" .. tostring(len))
-- set img to pdf

--local png = '/home/creden/work/app/client/face/images/card/' .. name .. ".png"
--local jpg = '/home/creden/work/app/client/face/images/card/' .. name .. ".jpg"
--local jpeg = '/home/creden/work/app/client/face/images/card/' .. name .. ".jpeg"
local cmd1,size1,err1


local page_count = get_page_count(file_name)
local last_page_index = page_count-1
--if(page_count>10) then page_count = 10 end
ngx.log(ngx.NOTICE,"PDF PAGE_COUNT = " .. page_count)
--local page_convert_size = 50
local page_convert_size = 10
local start_page,end_page = 0,(page_count<page_convert_size) and last_page_index or page_convert_size-1
local stop_now = false
local fraction = false
while(not stop_now)do
  
   cmd1 = 'convert -resize 1920 -density '.. config.dpi ..' ' .. file_name .. '['..start_page..'-'..end_page..'] ' .. file_name2 ; ngx.log(ngx.NOTICE,"CMD1 = " .. cmd1)
   size2, err1 = prog1('convert','-resize', '1920' ,'-density', config.dpi , file_name..'['.. start_page .. '-' .. end_page ..']'  , file_name2)
   ngx.log(ngx.NOTICE,"FINISHED CMD1")

   if page_count > 1 and start_page == end_page then
    fraction = true
  end

   --exit loop if we did last page
   if(end_page == last_page_index) then 
       ngx.log(ngx.NOTICE, 'stopped conversion at ' .. end_page )
       stop_now = true
	   break 
   else
       ngx.log(ngx.NOTICE, end_page..'~='..last_page_index)
   end

   -- start_page,end_page for next loop
   start_page = end_page+1
   end_page = start_page+page_convert_size-1
   if(end_page> last_page_index)then end_page = last_page_index end
end

--eark todo 
if(false)then
  ngx.log(ngx.NOTICE,"START CMD1B")
  local size1b, err1b= prog1b('mogrify','-resize','800x',file_name3)
  ngx.log(ngx.NOTICE,"FINISHED CMD1B")
else
  ngx.log(ngx.NOTICE,"SKIP CMD1B")
  --local size1b, err1b= prog1b('mogrify','-resize','800x',file_name3)
  ngx.log(ngx.NOTICE,"SKIPPEDED CMD1B (resizing)")
end


local cmd2 = 'find '.. file_name3  ..' -printf "%f\n" | sort -V | /home/creden/scripts/rset.lua pdf'

rc:del('pdf')
if(UPLOAD_PHOTO )then
   local size, err = prog('sh', '/home/creden/work/app/server/capi/check_size_pic.sh', file_name)
   if tonumber(size.stdout) < 600 then
      ngx.log(ngx.NOTICE,'1111111111111111')
      ngx.say (cjson.encode({success = false, msg = 'img row size'}))
      return
   end
   --cmd_img_pdf = 'convert '..file_name..' -background white -gravity center -extent 595x842 '..file_name
   --os.execute(cmd_img_pdf)
   cmd_img_pdf = 'convert '..file_name..' '..file_name
   os.execute(cmd_img_pdf)
end

--local size2, err2 = prog2('find '.. file_name3  ..' -printf "%f\n" | sort | /home/creden/scripts/rset.lua pdf')
--os.execute(cmd1)
os.execute(cmd2)
ngx.log(ngx.NOTICE,"FINISHED CMD2")
pdf_str = rc:get('pdf')

if fraction == true then
  local list_page = str_split(pdf_str, "\n")
  local first_page = list_page[1]
  local end_page =  "pdf" .. name .. "-" .. #list_page - 1 .. ".png"
  local cmd_change_page,err = prog('sh', '/home/creden/work/app/server/capi/change_page.sh', first_page, end_page)
  pdf_str = change_order(pdf_str)
end

local all_img = ''
local image_merge = ''
local page_num = 0
--merge image
for i in string.gmatch(pdf_str, "[^\n]+") do
    page_num = page_num + 1
    all_img = all_img..'/home/creden/work/app/client/face/images/card/'..i..' '
end

ngx.log(ngx.NOTICE,"START CMD3")
if (false and page_num > 1) then 
    image_merge = 'pdf'..name..'.png'
    local cmd_merge = 'convert '..all_img..' -append /home/creden/work/app/client/face/images/card/'..image_merge 
    ngx.log(ngx.NOTICE,cmd_merge)
    os.execute(cmd_merge) 
else
    image_merge = pdf_str
end
ngx.log(ngx.NOTICE,"FINISHED CMD4")
ngx.say (cjson.encode({success = (len>0), filename = name .. '.pdf' , pdf_str = pdf_str, image_merge = image_merge, page_num= page_num}))

