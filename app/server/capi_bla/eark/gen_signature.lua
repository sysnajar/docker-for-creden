#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local common = require "common"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
	body = cjson.decode(body)
else
    body  = {} 
end

if body.font_name and body.font_name == '' then 
	font_name = 'Arthit.ttf' 
else
	font_name = body.font_name 
end

if rc:get('is_ldd') and rc:get('is_ldd')=='true' then
    code = "' '"
else
    code = ngx.md5(body.code):sub(1,10)
end

ngx.log(ngx.NOTICE,"gen signatuer ===== " .. string.match(body.name,'[%a%d]*'))

common.validate_alphanumeric(body.code , "Invalid Param")
if string.match(body.name,'[%a%d]*') ~= '' then
    common.validate_name(body.name , "Invalid Param")
end
common.validate_alphanumeric(body.font_name, "Invalid Param")
common.validate_alphanumeric(tostring(body.field_id), "Invalid Param")

black = 'sh /home/creden/work/app/server/capi/gen_sign.sh "'..body.name..'" '..body.code..body.field_id..' '..font_name..' '.. code
white = 'sh /home/creden/work/app/server/capi/gen_sign_white.sh "'..body.name..'" '..body.code..' '..font_name..' '.. code
os.execute(black)
os.execute(white)
os.execute('cp /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..body.field_id..'_gen.png /home/creden/work/app/client/edoc/app-assets/images/signature/'..body.code..'_gen.png')
res = {success = true, data = body}
print(cjson.encode(res))


