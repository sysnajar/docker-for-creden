#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
common = require "common"
local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})
function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    check_session()
   
    local myusername  = session.data.username
    
    if myusername then
        db = conn:new_db_handle("dopa")
        col = db:get_col("package_ss")
        p, err = col:find({ company_id = session.data.company_id, active_package = true, package = {['$ne']='dipchip'}})

        if err then
            ret = {success = false, error_msg = "find plan unsuccess", error_code = "" }
        else
            local data = {}

            for i, v in p:pairs() do
                v._id = nil
                table.insert(data, v)
            end
            -- data = data[#data] or {} -- last pack

			--Storage usage
			local storage_used = 0
			ctx       = ctx or common.mongo_ctx()
			local _u  = ctx.dopa:get_col('esig_user'):find_one({adminEmail = session.data.username}) 
			docs, err = ctx.col['edoc_list']:find({company_id=_u.company_id})

            local doc_used = 0     
			for i, v in docs:pairs() do
                doc_used = doc_used + 1
				storage_used = storage_used + (v.filesize or 999)
			end
			
			--End Storage

            -- Member
            local _c  = ctx.dopa:get_col('company_members'):find_one({email = session.data.username}) 
            local _c2  = ctx.dopa:get_col('company_members'):find({ company_id = _c.company_id }) 
            local member_used = 0
            for i2, v2 in _c2:pairs() do
                member_used = member_used + 1
			end
            
            -- End Member

            -- ekyc use
            

            ret = {success = true, data = data, error_msg = "find plan success", error_code = "" , pack_member_used = member_used,pack_storage_used = storage_used,pack_doc_used = doc_used  }
        end




    else 
        ret = {success = false, error_msg = "not session", error_code = "" }
    end






    -- if u then 
    --     ret = { success = true, error_msg = "Add package success", error_code = "", data = body}
    -- else
    --     ret = { success = false, error_msg = "Add package not found", error_code = "" }
    -- end
end

print(cjson.encode(ret))
