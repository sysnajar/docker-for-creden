#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local redis = require 'redis'
local common = require "common"

rc = redis.connect('127.0.0.1', 6379)
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body  =  ngx.req.get_post_args()
else
   body  = {} 
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("edoc")
        col = db:get_col("deleted_doc")
        col1 = db:get_col("edoc_list")

		doc = { id = tonumber(body.id) , email  = body.email }
		doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 

        i, err = col:insert({doc}, nil, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            u, err = col1:find_one({id=body.id})
            common.activity_history(body.id ,body.email, 'Delete', 'ลบเอกสาร', body.email..' delete document '.. u.originalFile, body.email..' ลบเอกสาร '.. u.originalFile, 'Delete', u.originalFile, nil, nil, nil, nil)
            ret = {success = true, error_msg = err}
        end
    end

print(cjson.encode(ret))