#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local res = {success = false}
local data = {}
local UPLOAD_PHOTO = false 
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    apiKey = ngx.req.get_headers().apiKey
else
   body   = {}
end

function chk_apiKey(apiKey)
    local chk = nil
    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
            s._id = nil
            chk = true 
            body.username = s.adminEmail
        end
    end
    return chk
end

local data_json = [[
    {
        "doc_type_id":"1",
        "company_id":"",
        "pdfFile" : "1543463066",
        "current_order" : 1,
        "owner" : "kataya@creden.co",
        "id" : "1543463066",
        "imageMerge" : "pdf1543463063.png",
        "createdDtm" : ISODate("2018-11-29T03:45:10.030Z"),
        "status" : 1,
        "subject" : "Please sign ffff",
        "use_signing_order" : false,
        "fields" : [
            null,
            {
                "toolData" : "Signature",
                "toolName" : "Signature",
                "id" : 1543463086833,
                "page" : 0,
                "xp" : 33.24637592702672,
                "status" : 1,
                "email" : "kataya@creden.co",
                "time" : "11/29/18 03:47:01",
                "scale" : 100,
                "yp" : 39.46426368839209,
                "ip_field" : "96.30.94.34",
                "n" : 1,
                "signOrder" : 1,
                "type" : "draw"
            },
            {
                "toolData" : "Signature",
                "toolName" : "Signature",
                "id" : 1543463089591,
                "page" : 0,
                "xp" : 40.00221064617474,
                "status" : 1,
                "email" : "kataya@creden.co",
                "time" : "11/29/18 03:47:01",
                "scale" : 67,
                "yp" : 86.13893115686514,
                "ip_field" : "96.30.94.34",
                "n" : 2,
                "signOrder" : 1,
                "type" : "draw"
            }
        ],
        "signers" : [
            null,
            {
                "order_set" : "",
                "needToSign" : true,
                "eKyc" : false,
                "usePassword" : false,
                "title" : "",
                "name" : "yumi",
                "password" : "",
                "email" : "kataya@creden.co"
            }
        ],
        "height" : 0,
        "originalFile" : "1400496563-Screenshot-o.jpg",
        "msg" : "ggggg",
        "imageFiles" : [
            null,
            {
                "aspectRatio" : 0.7067137809187279,
                "fileName" : "pdf1543463063.png"
            }
        ],
        "width" : 0,
        "ip_stamp" : "96.30.94.34",
        "complete_dtm" : ISODate("2018-11-29T03:47:01.692Z"),
        "hash" : "b420f5146a8ee9b556fd8f6c2628f3db"
    }
]]

--imageFiles fn

--get page count
function get_page_count(file)

    if(UPLOAD_PHOTO)then return 1 end
    
    local res, err = prog1('pdftk', file ,  'dump_data'  )
    prog1.stdin = res.stdout
    res,err = prog1('grep', 'NumberOfPages')
    prog1.stdin = nil
    
    if(not err)then
       return tonumber(res.stdout:match('%d+'))
    else
       return 0	
    end
    
end

function downloadPdf()
    local name =  tostring(os.time()) 
    local dir = '/home/creden/work/app/client/face/images/card/'..name
    if string.match(body.fileUrl, "http") then
        if(body.fileUrl:find('.png') or body.fileUrl:find('.jpg') or body.fileUrl:find('.jpeg') ) then
            dir..'.png'
            UPLOAD_PHOTO = true
        else
            dir..'.pdf'
        end
        local cmd = 'wget '.. body.fileUrl ..' -O ' .. dir
        os.execute(cmd)
        data.pdfFile = name
    end  
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db = conn:new_db_handle("edoc")
    local col = db:get_col("edoc_list")
    data.owner = body.owner
    data.id = body.id
    data.current_order = 1
    data.createdDtm = bson.get_utc_date(ngx.now() * 1000)
    data.status = 0
    data.subject = body.subject
    data.msg = body.msg
    data.use_signing_order = body.use_signing_order
    data.fields = body.fields
    data.signers = body.signers
    --local i, err_i = col:insert({body})
    if i then
        ret.success = true
    else
        ret.error_msg = err_i
    end
end

print(cjson.encode(ret))