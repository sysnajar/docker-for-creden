#!/usr/bin/env lua
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false
local i=0

for line in io.stdin:lines() do
    i = i+1
    fname = string.sub(line, 1, string.find(line," ")-1) 
    lname = string.sub(line, string.find(line, " ")+1)

    res,err = prog('sh', 'testAMO.sh', '', fname, lname)
    
    --print(i, '[' .. fname ..']', '['.. lname ..']')
    print(i, '[' .. fname ..']', '['.. lname ..']', res.stdout)

    --break;

end

