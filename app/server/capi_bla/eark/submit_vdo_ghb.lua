#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()

    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
 	body = cjson.decode(arg[1])
end

--vdo_data = [[{"username":"creden","txid":"630","url":"http://159.65.2.13/face/images/vdo/myvdo.webm","missions":[{"no":"1","type":"blink","startTime":"00:05","endTime":"00:08"}, {"no":"2","type":"voice","word":"ลิงลม","startTime":"00:12", "endTime":"00:14"}]}]]
--body.vdo_data = cjson.decode(vdo_data)
local ret = {success = false, error_msg = "N/A"}

local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

body.new_txid = body.txid
body.txid = body.ghb_id
name = body.compCode ..'_'..body.txid

t = {}
for k, v in ipairs(body.vdo.missions) do
    if v.type == 'voice' then
        local voice = {word = v.word, startTime = v.startTime, endTime = v.endTime} 
        table.insert(t, voice)
    end
end

os.execute("/home/creden/work/app/server/capi/gensub.lua '".. cjson.encode(t) .. "' > /home/creden/work/app/client/vdo/ex/"..name..".vtt")

--remove wav
os.remove('/home/creden/work/app/client/vdo/ex/'..name..'.wav')

os.execute('sh /home/creden/work/app/server/capi/upload_vdo.sh '..name..' '..body.txid)

local res, err = prog('lua', '/home/creden/work/app/server/capi/speech_api.lua', name, body.new_txid, body.compCode, table.getn(body.vdo.missions) )
--local res, err = prog('lua', '/home/creden/work/app/server/capi/speech_api.lua', name, body.txid, body.compCode, table.getn(body.vdo.missions) )
-- if(not err) then
--    ret.success = true
--    ret.data = cjson.decode(res.stdout)
-- else
--     print(err)
-- end
