curl -X POST \
-H "Authorization: Bearer "$(gcloud auth application-default print-access-token) \
-H "Content-Type: application/json; charset=utf-8" \
-d '{
    "requests": [
      {
        "image": {
          "source": {
            "imageUri": "https://hjkl.ninja/face/images/card/'$1'.png"
          }
         },
         "features": [
           {
             "type": "TEXT_DETECTION"
           }
         ]
      }
    ]
}' \
https://vision.googleapis.com/v1/images:annotate