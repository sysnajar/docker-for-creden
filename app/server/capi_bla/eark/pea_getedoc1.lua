#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require 'resty-mongol'
local bson = require 'resty-mongol.bson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local HOST_NAME = os.getenv("HOST_NAME")
prog.timeout = 1000 * 300
prog.timeout_fatal = false
local rowsObj = {}
local docObj = {}
local srcpath = '/home/creden/work/app/client/face/images/card/'
local check_apiKey = 'TdDVFZPbo8uZW3Qb2IZ0tUPSGdJe5BiV'
local apiKey = nil
local ret = {success = false}
local body = nil

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    local ok,err = conn:connect('127.0.0.1',27017)
    local result = false
    if err then
        result = false
        ret = {success = result, msg = err}        
    else
        result = true
    end
    return result
end

function chk_apiKey()
    if not apiKey or apiKey ~= check_apiKey then
        ret = {success = false, msg = 'Authentication Fail'} 
        ngx.exit(ngx.HTTP_BAD_REQUEST)
        return false
    end
end

function file_exists(file)
    local result = false
    local f = io.open(tostring(file),"r")
    if f then
        result = true
        f:close() 
    end
    return result
  end

-- function get_readfile(b64file)
--     local result = true
--     --ngx.sleep(1)
--     local res, err = prog('cat',b64file)
--     if (not err) then
--         local buff = res.stdout
--         docObj.base64 = buff
--         ngx.sleep(1)
--         result = true
--     else
--         result = false
--     end
--     return result
-- end

-- function get_file(pdffile)
--     local srcfile = nil
--     local b64file = nil
--     if (pdffile and pdffile ~= '') then
--         srcfile = srcpath .. pdffile .. '_stamped.pdf'
--         if (not file_exists(srcfile)) then
--             srcfile = srcpath .. pdffile .. '-stamp_timestamped.pdf'
--             if (not file_exists(srcfile)) then
--                 srcfile = srcpath .. pdffile .. '-stamp.pdf'
--             end
--         end

--         if (not file_exists(srcfile)) then
--             return
--         end

--         local n = string.len(tostring(fiPdf))
--         b64file = string.sub(tostring(fiPdf),1,n-3) .. 'b64'

--         if not file_exists(b64file) then
--             local res0 = os.execute('sh /home/creden/work/app/server/capi/pea_decode_b64.sh '.. srcfile .. ' '.. b64file)
--         end

--         if file_exists(b64file) then
--             get_readfile(b64file)
--         end
--     end
-- end

function get_OwnerName(ownerEmail)
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("esig_user")
    local r = col:find_one({adminEmail=ownerEmail})
    if (r) then
        --print(r.adminFisrtname)
        docObj.owner_name = r.adminFisrtname
    end
end

function get_edoc()
    rowsObj = {}    
    local db = conn:new_db_handle("edoc")
    local col = db:get_col("edoc_list")
    local qry = {status=1,api_chk={['$ne']=1}}
    --local qry = {status=1,api_chk={['$ne']=nil}}    
    --local qry = {status=1,api_chk=nil}    

    local rows,err = col:find(qry)
    if (rows) then
        local c = 0;
        for i,v in rows:pairs() do
            docObj = {}              

            local signers = {}
            for n, s in ipairs(v.signers) do
                local signer = {}
                signer.email = s.email
                signer.name = s.name
                table.insert(signers, signer)
            end

            local signs = {} -- Signature
            for n, s in ipairs(v.fields) do
                local sign = {}
                sign.email = s.email
                sign.toolName = s.toolName
                sign.page = tonumber(s.page)+1
                sign.time = s.time
                table.insert(signs, sign)
            end                

            local time3 = os.date('%d-%m-%Y %H:%M:%S', tonumber(v.createdDtm)/1000)
            --print(time3)

            docObj = {
                id = v.id,
                owner = v.owner,
                created_time = time3,
                subject = v.subject,
                signers = signers,
                signs = signs,
                hashkey = rc:get('bc:' .. v.id)
            }
            get_OwnerName(v.owner)

            -- if (v.pdfFile) then
            --     get_file(v.pdfFile)
            -- end

            table.insert(rowsObj,docObj)
            local upd_doc = {['$set'] = {api_chk=1,api_dtm=bson.get_utc_date(ngx.now() * 1000)}}
            u, err = col:update({id=v.id}, upd_doc, 1, 0, true)

            --insert_temp(v.id)

            c=c+1
            if (tonumber(c) >= tonumber(body.limit)) then
                break
            end
            -- if (tonumber(c) >= 2) then
            --     break
            -- end            
        end

        --ret = {success = true, count = c, rows=jsonObj}

        if (c > 0) then
            ret = {success = true, count = c, rows=rowsObj}
        else
            ret = {success = false, msg = "Zero rows"}
        end

    else
        ret = {success = false, msg = err}
    end
end

if(ngx)then
    apiKey = ngx.req.get_headers().apiKey
    print = ngx.say
else
    body  = {limit=5} 
end

chk_apiKey()

if( ngx.var.request_method =='GET') then
    body = ngx.req.get_uri_args()
    if connect_mongo() then
        get_edoc()
    end
end

print(cjson.encode(ret))