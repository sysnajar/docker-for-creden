#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
else
    body  = {} 
end

--data = '{"pdfFile":"1525826939.pdf","fields":[{"time":0,"x":329,"id":"1525827051045","y":553,"page":0,"status":0,"email":"a"},{"time":0,"x":322,"id":"1525827059551","y":405,"page":1,"status":0,"email":"a"},{"time":0,"x":331,"id":"1525827061957","y":461,"page":1,"status":0,"email":"a"},{"time":0,"x":333,"id":"1525827065006","y":514,"page":1,"status":0,"email":"s"}],"signers":[{"email":"a"},{"email":"a"},{"email":"s"}],"id":"1525827067223","status":0,"imageFiles":[{"fileName":"pdf1525826939-0.png"},{"fileName":"pdf1525826939-1.png"},{"fileName":"pdf1525826939-2.png"}]}'
--rc:set('tmp_tc_edoc2',arg[1])
body = cjson.decode(arg[1])

t = {}
table.sort(body.fields,function (f1, f2) 
    return f1.time>f2.time
end)


local fields2 = {}
local tt = {}

for k,v in pairs(body.fields) do
    if(not tt[v.email])then
      tt[v.email] = true
	  table.insert(fields2, v)
	end
end

html_head = [[<!DOCTYPE html><html><head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><style>@font-face {font-family: 'THSarabunNew';src: url('./sarabun-webfont-master/fonts/thsarabunnew-webfont.woff');font-weight: normal;font-style: normal;} body {font-family: 'THSarabunNew';} table, th, td {border: 1px solid black;text-align: center;border-collapse: collapse;}</style></head><body><table style="width:100%"><tr><th>ID</th><th>File name</th>     <th>Signer</th> <th>Sign Date</th></tr>]]

html_end = [[</table></body></html>]]

html_body = ''
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    for i,v in ipairs(fields2) do
        html_body = html_body..'<tr><td>'..v.id..'</td><td>'..body.originalFile..'</td><td>'..v.email..'</td><td>'..v.time..'</td></tr>'
    end
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    for i,v in ipairs(fields2) do
        s = col:find_one({adminEmail =v.email })
        
        if s and s.LNAME and s.FNAME then
            html_body = html_body..'<tr><td>'..v.id..'</td><td>'..body.originalFile..'</td><td>'..v.email..'('..s.FNAME..' '..s.LNAME..')'..'</td><td>'..v.time..'</td></tr>'                      
        else
            html_body = html_body..'<tr><td>'..v.id..'</td><td>'..body.originalFile..'</td><td>'..v.email..'</td><td>'..v.time..'</td></tr>'
        end       
    end
end


html = html_head..html_body..html_end

os.execute('sh /home/creden/work/app/server/capi/convert_htmltopdf.sh "'..html..'" '..body.id)



