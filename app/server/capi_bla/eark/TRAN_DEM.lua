#!/usr/bin/env lua

function init()
if(#arg > 0  )then
  cjson = require 'cjson'
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379)
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(id)
	local key = 'PREP:DEM:' .. id
	local t1 = cjson.decode(rc:get(key))
	local t2 = {
	ID_sex = (t1.user.sex ~= '') and t1.user.sex or nil,
	ID_age = (t1.user.dob ~= '') and t1.user.dob or nil,
	ID_name    = (t1.user.firstname ~= '') and t1.user.firstname or nil,
	ID_lname   = (t1.user.lastname ~= '') and t1.user.lastname or nil,
	ID_dob     = (t1.user.dob ~= '') and t1.user.dob or nil,
	ID_citino  = t1.kyc and t1.kyc.id_card.citizenIdF or (t1.user.card_no and t1.user.card_no ~= '') and t1.user.card_no or nil,
    DOPA_death = t1.kyc and t1.kyc.death.valid or (t1.user.DOPA_death and t1.user.DOPA_death ~= '') and t1.user.DOPA_death or nil,
    DOPA_name = (t1.user.DOPA_name and t1.user.DOPA_name ~= '') and t1.user.DOPA_name or nil,
    DOPA_lname = (t1.user.DOPA_lname and t1.user.DOPA_lname ~= '') and t1.user.DOPA_lname or nil,
    DOPA_dob = (t1.user.DOPA_dob and t1.user.DOPA_dob ~= '') and t1.user.DOPA_dob or nil,
    DOPA_sex = (t1.user.DOPA_sex and t1.user.DOPA_sex ~= '') and t1.user.DOPA_sex or nil
	}

	return t2
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
