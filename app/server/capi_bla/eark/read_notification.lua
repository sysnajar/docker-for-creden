#!/usr/bin/env lua 
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"

   ngx.req.read_body()
   print = ngx.say
   body  =  ngx.req.get_post_args()


conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

db = conn:new_db_handle("edoc") --db name
col = db:get_col("notification") -- collection

local update = {["$set"] = {status=1}}
u, err = col:update({email=body.email, status= 0}, update, 0, 1, true)


ret = {success = true, email = body.email  }
print( cjson.encode(ret) )
