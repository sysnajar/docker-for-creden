#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local debug = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body = {idcard_file = arg[1], old_txid = arg[2], txid = arg[3], usr = arg[4]}
end

debug = arg[3]=='1'
if(debug)then
   print("DEBUG MODE ON")
end

function get_time_series(duration , decrease_by,  n)
local sec = string.match(duration,"%d+")
local ms  = string.match(duration,"%.%d+"):sub(2)
local ret = {}

for i=1,n do
   local n_sec = tonumber(sec)
   local n_ms  = tonumber(ms)
   
    local time = sec .. '.' .. ms
	table.insert(ret,time)

   if(n_ms>0)then
   	   n_ms = n_ms - 10
	   if(n_ms<0)then n_ms = 0 end
   else
	   n_ms   = 90 
	   n_sec  = n_sec-1 
   end

   ms  = tostring(n_ms)
   sec = tostring(n_sec)
   if(#sec==1)then sec = '0' .. sec end
   if(#ms==1) then ms  = '0' .. ms  end
   print("")

end
return ret
end

function tprint(x,y)
if(debug)then print(x,y) end
end

function chk_face(rec_face_file)
    local t = {}
	tprint('rec_face_file', rec_face_file)
    local faceId1 = detect_face(rec_face_file)
    local faceId2 = detect_face(body.idcard_file)
	tprint('faceId vdo', faceId1)
	tprint('faceId idcard', faceId2)

    t = verify_face(faceId1, faceId2)
    return t.faceMatchPercent or 0
	--rc:set('face.recognize.'..body.txid..'.'..i, (t.faceMatchPercent or 0) * 100)
end
  
function detect_face(img)
    local faceId = ''
    local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
    if not err then
        local tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end
  
function verify_face(faceId1,faceId2)
    local t = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
    if not err then
        local tmp = cjson.decode(res.stdout)
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
    end
    return t
end


local vdo_file = '/home/creden/work/app/client/vdo/ex/'..body.old_txid..'.webm'
local confidence  = 0
local enrolled_file = ''
local all_confidences = {}

local time  = nil
local time2 = nil
local times = {}
tprint('vdo_file',vdo_file)

--1 Get vdo duration
local res, err = prog('/home/creden/work/app/server/capi/get_vdo_duration.sh', vdo_file)
if not err then
local raw_duration = res.stdout or ''
tprint('raw_duration', raw_duration)

duration = string.match(raw_duration, "%d+%.%d%d")
times = get_time_series(duration, 10, 10)
for i,v in ipairs(times) do
   tprint('time'..i, v) 
end

end

os.execute('sleep 1')
--2 Last frame photo

for i,v in ipairs(times) do
local selfie_filename = 'rec_'.. body.txid .. '-' .. i
local selfie_file  = '/home/creden/work/app/client/face/images/card/' .. selfie_filename .. '.png'


tprint('selfie_file', selfie_file)
local res, err = prog('ffmpeg', '-i', vdo_file, '-ss', v, '-vframes', '1', selfie_file)
if not err then
	os.execute('sleep 1')
	enrolled_file = selfie_filename
	local tmp = chk_face(selfie_filename)
	table.insert(all_confidences, tmp)
	if(tmp>0.5)then confidence = tmp ; break ; end
end
end


local ret = {success = true, confidence = confidence, all_confidences = all_confidences, enrolled_file = enrolled_file}
print(cjson.encode(ret))
