#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
bson = require "resty-mongol.bson"
common = require "common"
local HOST_NAME = os.getenv("HOST_NAME")
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local res = {success = false}
local company_id = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    if body == nil then 
        res.error_msg = 'Invalid Data'
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    else
        if not body.txid then
            res.error_msg = 'Invalid Data'
            print(cjson.encode(res))
            ngx.exit(ngx.HTTP_OK)
        end
    end
    apiKey = ngx.req.get_headers().apiKey
else
   body = {}
end
------------------------------------------------------------------------------------------------------------------
--init mongo
ctx = ctx or common.mongo_ctx()
col_edoc_list = ctx.col["edoc_list"]
col_user_company = ctx.col["dopa.user_company"]
-----------------------------------------------------FN-----------------------------------------------------------
--check apiKey
function chk_apiKey()
    local find_apiKey, err_find_apiKey = col_user_company:find_one({apiKey = (apiKey or 'no key')})
    if not find_apiKey then 
        res.error_msg = 'Authentication fail'
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    else
        company_id = tostring(find_apiKey._id)
    end
end
----------------------------------------------------------------------------------------------------------------

chk_apiKey()
local params = body
local document, err_document = col_edoc_list:find_one({txid = params.txid, company_id = company_id})

if document then 
    document._id = nil
    local signers = {}
    for k, v in ipairs(document.signers) do
        local signer = {}
        signer.no = v.no
        signer.email = v.email
        signer.name = v.name
        signer.signer_session = v.signer_session
        signer.signer_link = v.signer_link
        signer.need_to_sign = v.needToSign
        signer.use_password = v.usePassword
        signer.password = v.password
        for k_field, v_field in ipairs(document.fields) do
            if v_field.status == 1 then
                signer.status = true
            else
                signer.status = false
                break
            end
        end
        table.insert(signers, signer)
    end
    if document.status == 1 then
        res.document_status = true
    else
        res.document_status = false
    end
    res.signers = signers
    
else
    res.error_msg = 'Invalid txid'
end

print(cjson.encode(res))