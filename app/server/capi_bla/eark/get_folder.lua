#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
session = require "resty.session".open({secret = "4321"})

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        -- body.email = session.data.username
        body.owner_folder = session.data.username
    end
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {owner_folder = arg[1]} 
end
check_session()
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders")
    list = {owner_folder = body.owner_folder or ""}
    l,err = col:find(list)

    if l then
            local list_folder = {}
            for i,v in l:pairs() do 
            v._id = tostring(v._id)
            table.insert(list_folder,v)
            -- print(i,v.name_folders .. "   " .. v.owner_folder  )
            end
        ret = {data = list_folder or {}}

    else
        ret = { data = {} }
    end
print(cjson.encode(ret))
end





