#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--todo cust_code may not == username in the future
function get_cust_code(usr)
return usr
end

--todo cust_code may not == username in the future
function chk_pwd(usr, pwd)
    local chk = nil
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({adminUsername = usr, adminPassword = pwd})   
        if s then 
          chk = true
           s._id = tostring(s._id) 
           body = s
        end
    end
    return chk
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
	body  = ngx.req.get_post_args()
    --body = cjson.decode(ngx.req.get_body_data())
else
   body   = {} 
end

if(chk_pwd(body.username, body.password)) then
		body.custCode = get_cust_code(body.username)
    ret = {success = true, error_msg = err, custCode = body.custCode , username = body.username, data = body}
else
		ret = {success = false, error_msg = 'Invalid Login'}
end
print(cjson.encode(ret))
   
