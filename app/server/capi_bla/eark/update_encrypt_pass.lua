#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local md5 = require 'md5'

local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {mode = 'all', status = '1', compCode = 'creden', filter = ''} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("company")
    s = col:find({})  
    
    if s then 
        data = {}
		
        for k, v in s:pairs() do
            v._id = tostring(v._id)
            v.adminPassword = v.adminPassword or ''
            if v.adminPassword ~= '' then
                print(v.adminEmail, v.adminPassword, md5.sumhexa(v.adminPassword))
                local update_cmd = {["$set"] = {adminPassword = v.adminPassword,encryptPass=md5.sumhexa(v.adminPassword)} }
                u, err = col:update({adminEmail=v.adminEmail}, update_cmd, 0, 0, true)
            end
            
            --[[
            package_name = {"Package 1","Package 2","Package 3","Package 4","Package 5","Package 6"}
            status_name = {"เริ่มทำรายการ","แจ้งชำระเงินแล้ว","ยืนยันชำระเงินแล้ว"}
            v.package_name = package_name[tonumber(v.packageId)] 
            v.status_name = status_name[tonumber(v.status)] 
			if(filter ~= '')then
				if( 
					string.find(tostring(v.package_name), filter)	
					or string.find(tostring(v.packageDesc), filter)	
				    or string.find(tostring(v.status_name), filter) 
				  )then
                   table.insert(data, v)
				end

			else
               table.insert(data, v)
			end
			]]--
        end
        ret = {success = true, error_msg = "complete", data = data}
    else
        ret = {success = false, error_msg = "err"}
    end
end
print(cjson.encode(ret))
