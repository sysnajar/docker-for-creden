 curl -X POST  https://openapi-sandbox.kasikornbank.com/v1/qrpayment/request \
 -H 'Authorization: Bearer a2FzaWtvcm5iYW5rdG9rZW4=' \
 -H 'cache-control: no-cache' \
 -H 'Content-Type: application/json' \
 -d '{
  "partnerTxnUid": "PTR202003051007",
  "partnerId": "PTR0000001",
  "partnerSecret": "JGCm8fVXDpQimnVNQJBxCqvq259dx77o",
  "requestDt": "2020-01-08T13:00:00+07:00",
  "merchantId": "KB000001588975",
  "terminalId": "09000107",
  "qrType": "3",
  "txnAmount": 100.5,
  "txnCurrencyCode": "THB",
  "reference1": "INV001",
  "reference2": null,
  "reference3": null,
  "reference4": null,
  "metadata": ""
}'