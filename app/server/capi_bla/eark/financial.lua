#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
else
   body  = {key = 'tmpq7ku4:scb'} --tmp4jqhv:kbank
end

t = {EBANK_sumcashin6mth = 0, EBANK_sumcashout6mth = 0, EBANK_finBalance = 0, EBANK_iniBalance = 0}

local data = rc:get('tmp4jqhv:kbank')
data = cjson.decode(data)

for k, v in pairs(data.data) do

    v.deposit = v.deposit:gsub("+", "")
    v.deposit = v.deposit:gsub(",", "")
    v.withdraw = v.withdraw:gsub("-", "")
    v.withdraw = v.withdraw:gsub(",", "")
    v.balance = v.balance:gsub("-", "")
    v.balance = v.balance:gsub("+", "")
    v.balance = v.balance:gsub(",", "")

    if tonumber(v.deposit) then
        t.EBANK_sumcashin6mth = t.EBANK_sumcashin6mth + tonumber(v.deposit)
    end
    if tonumber(v.withdraw) then
        t.EBANK_sumcashout6mth = t.EBANK_sumcashout6mth + tonumber(v.withdraw)
    end
    if k == #data.data and tonumber(v.balance) then
        t.EBANK_finBalance = tonumber(v.balance)
    end
    if k == 1 and tonumber(v.balance) then
        t.EBANK_iniBalance = tonumber(v.balance)
    end
end

print(cjson.encode(t))
