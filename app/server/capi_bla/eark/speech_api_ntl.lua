#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {filename = arg[1]} 
end

print('------------speech api-------------------')

function trim1(s)
  return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local transcript = ''
local params   = {filename = (body.filename or '')}
local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/speech', '-d', cjson.encode(params))
if not err then
    print(res.stdout)
    local speech = cjson.decode(res.stdout)
    print(cjson.encode(speech.results))
    for i,v in pairs(speech.results or {}) do
        for j,k in pairs(v.alternatives) do
            transcript = transcript .. ' ' .. k.transcript
        end
    end
end



line_concat = ''
point = 0
local res, err = prog('cat', '/home/creden/work/app/client/vdo/ex/'..body.filename..'.vtt')
if not err then
   	str = res.stdout
   	lines = {}
	for s in str:gmatch("[^\r\n]+") do
	    table.insert(lines, s)
	end
	for i,v in ipairs(lines) do
		if math.mod(i,2) ~= 0 then 
			if string.find(string.lower(transcript), string.lower(trim1(v))) then
				point = point + 1
			end
			line_concat = line_concat .. v
		end
	end
end


local update_res = {success = true, username = arg[3], txid = arg[2], score = point, maxScore = arg[4], type = "audio"}
print('--------json update_res----------')
print(cjson.encode(update_res))
local update_cmd = "lua /home/creden/work/app/server/capi/update_res_ntl.lua '"..cjson.encode(update_res).."' &"
os.execute(update_cmd)
os.exit(0)