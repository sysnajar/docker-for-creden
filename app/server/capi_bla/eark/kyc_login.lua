#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local scrypt = require "resty.scrypt"
common = require "common"
prog.timeout_fatal = false

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--todo cust_code may not == username in the future
function get_cust_code(usr)
local ret = usr
local atI = string.find(usr,'@')
if(atI)then 
  ret = string.sub(usr, atI+1) 

  if(ret~='creden')then ret = usr end

end


return ret
end

--todo cust_code may not == username in the future
function chk_pwd(usr, pwd, login_service)
--local map = {rads = '1234', six = 'cink45', nut1shot = '1234'}
--return usr ~= nil and pwd ~= nil and map[usr] == pwd
    local chk = nil
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = {}
        if login_service == "esig" then
          col = db:get_col("esig_user")              
        else
          col = db:get_col("company")
        end
        -- col = db:get_col("esig_user")   
        user_data = col:find_one({adminEmail = usr})   
        if (user_data and user_data.hashedPassword) then 
            chk = scrypt.check(pwd, user_data.hashedPassword)
            user_data._id = tostring(user_data._id); 
			user_data.adminPassword=nil;
			user_data.adminPasswordC=nil;
			user_data.encryptPass = nil
			user_data.hashedPassword = nil
			user_data.cvv_code = nil
			user_data.txtPid = nil
            user_data.date_of_birth = nil
        end  
    end
    return chk
end

function chk_tx(usr)
  ret = {success = false, error_msg = 'Your account has been locked, please contact our support team'}
  local max_counter = 'counter.'..usr..'.txMax'
  m = rc:get(max_counter)
  if not m or tostring(m) == '0' then
    print(cjson.encode(ret))    
    ngx.exit(ngx.HTTP_OK)
  end
end

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
	body  = ngx.req.get_post_args()
    --body = cjson.decode(ngx.req.get_body_data())
else
   body   = {username="codernajar@gmail.com", password="1234", login_service="esig"} 
end
    body.username = body.username:lower()
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")
        ngx.log(ngx.NOTICE,"======check_login_addmin" .. tostring(chk_pwd(body.username, body.password,body.login_service)))
		if(chk_pwd(body.username, body.password,body.login_service)) then
       --chk_tx(body.username)
    		   body.custCode = get_cust_code(body.username)
    		   local session = require "resty.session".start{ secret = "4321" }
    		   
			   local h = ngx.req.get_headers()
			   --Persistence login
			   if(true)then
				 common.create_persistence_token(body.username, h['x-real-ip'] or '2.1.1.1')
			   end

          math.randomseed(os.clock()*100000000000)
          local extend_key=''
          for i=1,2 do
              a = math.random(10, 99)       
              extend_key = extend_key .. a
          end
          
          if user_data.activeStatus == 4 then
          rc:set('extend_key'..':'..body.username..':'..extend_key, "true")
          rc:expire('extend_key'..':'..body.username..':'..extend_key, 60*30)

                ngx.log(ngx.NOTICE,"A SET SESSION TO " .. tostring(body.username))
    			session.data.username = body.username
                session.data.custCode = body.custCode
                if session.data.username then
                    local col_member = db:get_col("company_members")
                    local member_detail, err_member_detail = col_member:find_one({email = session.data.username})
                    if member_detail then
                        session.data.company_id = tostring(member_detail.company_id)
                    end
                end
            session:save()
          end
          t = {desc = "kyc_login"}
          createDate = os.date("%x") 
          createTime = os.date("%X")
          createdDtm = bson.get_utc_date(ngx.now() * 1000) 
          log_save = { description = t.desc, t = cjson.encode(t), username = session.data.username or "N/A", createDate = createDate, createTime = createTime, createdDtm = createdDtm }
          rc:lpush("log_a",cjson.encode(log_save))

           ret = {success = true, error_msg = err, custCode = body.custCode , username = body.username , sess = session.data.name, data = user_data, extend_key=extend_key}

	   else
       ngx.save_log({desc = "kyc_login",success = false}) 
		   ret = {success = false, error_msg = 'Invalid Login'}
		end

    end

   print(cjson.encode(ret))
   
