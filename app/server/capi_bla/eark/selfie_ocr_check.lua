#!/usr/bin/env lua
local cjson = require 'cjson'

json = io.read()
t = cjson.decode(json)
ret = {success = true, text_count = t.text_count, texts = t.texts}
--ret.texts = nil

function check(w)
local ret = true
local n = tonumber(w)

if(n and n>100 and n<=190)then ret = false end

if(string.find(w,'Thai')) then return false end
if(string.find(w,'National')) then return  false end
if(string.find(w,'ID')) then return false end
if(string.find(w,'Card')) then return false end
if(string.find(w,'Expiry')) then return false end
if(string.find(w,'%d%d+%-%d+%d%d+')) then return false end

return ret
end

if(t.text_count>0)then
  for i,v in ipairs(t.texts) do
	 if not check(v) then
		ret.success = false
		ret.err = "invalid word found [" .. v .."]"
		break 
	 end
  end
end


print(cjson.encode(ret))

