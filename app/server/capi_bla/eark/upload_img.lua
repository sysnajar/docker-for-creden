local upload = require "resty.upload"
local cjson = require "cjson"
local chunk_size = 4096 -- should be set to 4096 or 8192

local file
local name =  tostring(os.time()) 
local file_name  = '/home/creden/work/app/client/face/images/card/' .. name .. ".pdf"
local file_name2 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. ".png"
local file_name3 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. '*.png'

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)


--ngx.say "ok"

local form, err = upload:new(chunk_size)
if not form then
  ngx.say "no form"
  --ngx.log(ngx.ERR, "failed to new upload: ", err)
  --ngx.exit(500)
end

-- ngx.say "ok2"
form:set_timeout(1000) -- 1 sec
file = io.open(file_name, "w+")



local len = 0
while true do
local typ, res, err = form:read()
if not typ then
	ngx.say("failed to read: ", err)
	return
end

if(typ=='body')then
  len = len + #res
  file:write(res)
end
--ngx.say("read1: ", cjson.encode({typ, res}))

if typ == "eof" then
	file:close()
	break
end
end

local typ, res, err = form:read()
--ngx.say("read2: ", cjson.encode({typ, res}))

--ngx.say ("total" .. tostring(len))

local cmd1 = 'convert -background transparent ' .. file_name .. ' ' .. file_name2
local cmd2 = 'find '.. file_name3  ..' -printf "%f\n" | sort | /home/creden/scripts/rset.lua pdf'

rc:del('pdf')
os.execute(cmd1)
os.execute(cmd2)
pdf_str = rc:get('pdf')
ngx.say (cjson.encode({success = (len>0), filename = name .. '.pdf' , pdf_str = pdf_str}))

