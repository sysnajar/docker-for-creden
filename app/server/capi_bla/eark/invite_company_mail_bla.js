var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"
  var json = JSON.parse(process.argv[2])
  var r = json.admin
  var to_member = json.email
  var company = json.company || "บริษัท / company"
  if (!r.adminLastname) r.adminLastname = ""
  var fullname = r.adminFisrtname + " " + r.adminLastname
  var link_invite = process.env.HOST_NAME + "/bla/web/invite.html?email="+ to_member +"&rd=" + new Date().getTime()
  var html = `
  <!doctype html>

  <html lang="th-TH" dir="ltr">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
    </head>
    <body style="background: #E0E0E0;padding-bottom: 30px;"><br>
  
      <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 30px 50px 30px;width:80%;border-radius: 5px;">
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="${logo}" alt="logo website" style="width: 300px;margin-top: 30px;">
            </td>
        </tr>
        <tr>
          <td style="width: 100%;color: #000;padding: 0px 30px 30px 30px;margin-top: 30px;">
            <div style="margin-top:30px;font-size: 16px;">
              <label style="font-weight: bold;">คุณ `+ fullname +`(`+ r.adminEmail  +`) ได้เชิญให้คุณเข้าร่วม “`+ company +`”</label>
            </div>
            <div style="margin-top:15px;font-size: 16px;">
              <label style="font-weight: bold;">From/จาก :</label>  <b>Dear, &nbsp;`+ to_member +`</b>   
            </div>
            <div style="margin-top:10px;font-size: 16px;">
              <label style="font-weight: bold;">You have been invited by `+ fullname +`(`+ r.adminEmail +`) to join the company account “`+ company +`”. To accept this invitaion click the above button.</label>
            </div>

            </td>
        </tr>
        <tr>
          <td style="display: flex;justify-content: center;">
              <div style="background: #0058A9;width: 100%;display: block;padding-bottom: 100px;">
                  <div style="text-align: center;margin-top: 30px">
                    <img src="https://hjkl.ninja/bla/images/mail/mail_invite.png" style="margin-top: 30px;">
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    Click to accept an invitation
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    คลิกที่นี่เพื่อตอบรับคำเชิญเข้าบริษัท
                  </div>
                  <div style="text-align: center;margin-top: 50px">
                    <a href="`+ link_invite + `" style="text-decoration: none;">
                      <div style="color: #4F4F4F;font-size: 16px;font-weight: 500;background: #F2C94C;padding: 5px 30px;width: 120px;margin: 0px auto;">
                      Accept
                      <br>
                      ตอบรับเข้าร่วม
                  </div>
                    </a>
                  </div>
              </div>
          </td>
        </tr>
        </table>
        </body>
        </html>
  `

  var smtp = {
    host: 'smtp.bla.co.th', //set to your host name or ip
    // host: 'smtp.mailgun.org', //set to your host name or ip
    port: 25, //25, 465, 587 depend on your 
    auth: {
      user: 'electronic_sign@bangkoklife.com', //user account
      // // pass: 'creden2018' //user password
      //pass: 'dnkrakkaxnmnakpr'
      // user: 'postmaster@mg.creden.co', //user account
      // pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'Bangkok Life - eSinature', //from email (option)
    to: to_member, //to email (require)
    cc: '',
    subject: '[Bangkok Life] Company Account Invitation', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


