#!/usr/bin/env lua
local cjson = require 'cjson'


body  = cjson.decode(arg[1]) 

res = {status = false, message = 'สถานะไม่ปกติ => ข้อมูลไม่ตรง'}

--data = {fnameTH = 'คุณากร' , lnameTH = 'จันทร์ชื่น' , laserCode = 'ME0117526468' , cardNo = '1111111111111' , dob = '11/08/2534' } 

data = {{cardNo = '1111111111111' , status = true, message = ''}, {cardNo = '2222222222222' , status = false, message = 'สถานะไม่ปกติ => ข้อมูลไม่ตรง'}, {cardNo = '3333333333333' , status = false, message = 'สถานะไม่ปกติ => ไม่พบเลข Laser จาก PID นี้'}, {cardNo = '4444444444444' , status = false, message = 'CitizenID is not valid'}} 


for k, v in ipairs(data) do
    if body.cardNo == v.cardNo then
        res.status = v.status
        res.message = v.message
        break
    end
end

print(cjson.encode(res))

