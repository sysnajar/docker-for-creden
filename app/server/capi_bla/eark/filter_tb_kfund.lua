#!/usr/bin/env lua

redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

k = arg[1]
s = rc:get(k)
h_tb = [[ <table width="100%" class="funds" border="0" cellspacing="0" cellpadding="0" id="tblFunds">        
                <tbody><tr>
                  <th width="145" valign="top"><div align="left">สถาบันการเงิน</div></th>
                  <th width="90" valign="top"><div align="left">กองทุน</div></th>
                  <th width="97" valign="top"><div align="right">หน่วยคงเหลือ</div></th>
                  <th width="85" valign="top"><div align="right">NAV</div></th>
                  <th width="125" valign="top"><div style="text-align:center; float:right;">มูลค่าเงินลงทุน<br><font size="3">(บาท)</font></div></th>
                  <th width="78" valign="top"><div style="float:right;">ต้นทุนเฉลี่ย<br><font size="3">(บาท/หน่วย)</font></div></th>
                  <th width="91" valign="top"><div style="float:right;">กำไร/ขาดทุน<br><font size="3">(บาท)</font></div></th>                
                  <th width="72" valign="top">อัตราส่วน</th>
                  <th width="69" valign="top">แก้ไข/ลบ</th>
                </tr>
                ]]
f_tb = [[</tbody></table>]]        

local t = {}                   
local i = 0
html = ''
account = 0
for word in string.gmatch(s, "<tr><td>บลจ.กสิกรไทย จำกัด.+</td></tr>") do
	str = string.gsub(word,",", "KIT")
	str = string.gsub(str,"</tr>", ",")
	for x in string.gmatch(str, '([^,]+)') do
		if string.find(x,'บลจ.กสิกรไทย จำกัด') then
			res = string.gsub(x,"KIT", ",")
			html = html .. res..'</tr>'
			account = account + 1
		end	    
	end
end
html = 'total : '..account..' กองทุน <br> '..h_tb..html..f_tb
rc:set(k,html)