#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
bson = require "resty-mongol.bson"
common = require "common"
local HOST_NAME = os.getenv("HOST_NAME")
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local res = {success = false}
local company_id = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    if body == nil then 
        res.error_msg = 'Invalid Data'
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    else
        if not body.txid or not body.signers or not body.template_id then
            res.error_msg = 'Invalid Data'
            print(cjson.encode(res))
            ngx.exit(ngx.HTTP_OK)
        end
    end
    apiKey = ngx.req.get_headers().apiKey
else
   body = {}
end
------------------------------------------------------------------------------------------------------------------
--init mongo
ctx = ctx or common.mongo_ctx()
col_edoc_list = ctx.col["edoc_list"]
col_user_company = ctx.col["dopa.user_company"]
-----------------------------------------------------FN-----------------------------------------------------------
--check apiKey
function chk_apiKey()
    local find_apiKey, err_find_apiKey = col_user_company:find_one({apiKey = (apiKey or 'no key')})
    if not find_apiKey then 
        res.error_msg = 'Authentication fail'
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    else
        company_id = tostring(find_apiKey._id)
    end
end

--send email
function send_email_fn(doc, flg)
    if tostring(flg) == 'true' then
        -- local list_signers={}
        -- local send_mail_list = {}
        -- local send_mail = {}
        -- for i2,v2 in ipairs(doc.signers) do
        --     if v2.password~='' then
        --         send_mail = {signer = v2.name, to = v2.email , id = doc.id, subject = doc.subject, msg = doc.msg ,password=v2.password} 
        --     else
        --         send_mail = {signer = v2.name, to = v2.email , id = doc.id, subject = doc.subject, msg = doc.msg }
        --     end
        --     send_mail.sender_name  = doc.owner_name or 'N/A'
        --     send_mail.sender_email = doc.owner or 'N/A'
        --     table.insert(send_mail_list,send_mail)
        -- end
        -- rc:set('list_send_mail',cjson.encode(send_mail_list))
        -- os.execute('lua /home/creden/work/app/server/capi/email_invite_sign.lua &')
    end
end

--gen signer key & sign link
function signer_session_fn(template)
    local signer_key = ''
    local signer_link = ''
    math.randomseed(os.clock()*100000000000)
    for i=1,5 do
        local r = math.random(10, 99)	      
        signer_key = signer_key .. r
    end
    signer_key = template.company_id..os.time()..signer_key
    signer_link = HOST_NAME..'/company/viewer.html?id='..template.id..'&rd='..os.time()..'&lang=th&ch=api&tk='..signer_key
    return signer_key, signer_link
end

--check txid
function chk_txid_fn(params)
    local find_txid, err_txid = col_edoc_list:find_one({txid = (params.txid or 'no txid')})
    if find_txid then 
        res.error_msg = 'Existing txid'
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    end
end
----------------------------------------------------------------------------------------------------------------
--local params = {txid = "", template_id = "1549887367", signers = {{no = 1, email = 'nutp10.1@gmail.com', name = 'Kunakorn'}}, default_field_values = {field2 = "Kunakorn", field3 = "0957513388"}, send_email = true, redirect_url = HOST_NAME}
local params = body
chk_apiKey()
chk_txid_fn(params)
local template, err_template = col_edoc_list:find_one({id = params.template_id, company_id = company_id})

if template and tostring(template.is_template) == 'true' then 
    template._id = nil
    template.is_template = nil
    template.txid = params.txid
    -- todo : copy PDF file from template to new doc
    local dir = '/home/creden/work/app/client/face/images/card/'
    local f1  = dir .. template.id .. '.pdf'
    --gen new document id
    template.id = template.company_id .. os.time()
    template.pdfFile = template.id
    local f2  = dir .. template.id .. '.pdf'
    local cmd = 'cp ' .. f1 .. ' ' .. f2 ..' &'
    os.execute(cmd)    

    template.redirect_url = params.redirect_url or 'https://creden.co'
    if table.getn(params.signers) == table.getn(template.signers) then
        --replace email to fields
        for k, v in ipairs(template.fields) do
            --replace value to fields
            if params.default_field_values then
                v.toolData = params.default_field_values[v.field_no]
            end
            for k1, v1 in ipairs(params.signers) do
                if v.signOrder == v1.no then
                    v.email = v1.email
                    v.name = v1.name
                end
            end

             for k2, v2 in pairs(params.prefill_field_values or {}) do
                if v.field_no == k2 then
                    v.toolData = v2
                    v.time = os.date("%x %X")
                    v.status = 1
                    v.ip_field = "-"
                end
            end

            
        end

        --replace email to signers
        for key, val in ipairs(template.signers) do
            for key1, val1 in ipairs(params.signers) do
                if val.no == val1.no then
                    val.email = val1.email
                    val.name = val1.name
                    val.signer_session, val.signer_link = signer_session_fn(template)
                end
            end
        end

        --create document
        template.createdDtm = bson.get_utc_date(os.time() * 1000)



        local create, err_create = col_edoc_list:insert({template})
        if create then
            send_email_fn(template, params.send_email)
            res.document_id = template.id
            res.signers = template.signers
            res.success = true
        else
            res.error_msg = err_create
        end
    else
        res.error_msg = 'Number of signers invalid'
    end
else
    res.error_msg = 'Template is invalid'
end

print(cjson.encode(res))
