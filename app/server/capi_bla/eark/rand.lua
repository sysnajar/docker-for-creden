#!/usr/bin/env lua
local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
local t = {}
local len = arg[1] and tonumber(arg[1]) or 32
local cnt = 0
 
 local i = 1
 repeat
     local f = assert (io.popen (cmd, 'r'))
	 local line = f:read('*all'):match("%d+")
	 f:close()
	 cnt = cnt + #line
	 table.insert(t, line)
 until cnt>len

		
 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
 print(ret)

