#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end
    local fn =  function(id)
        local key = 'TRAN:BUS:' .. id
        local t = cjson.decode(rc:get(key))
        local max   = 200
        local score = 0

		
		if(t.DBD_com == 1) then 
		  score = (max/2)
		end

		if(t.DBD_com > 1) then 
		  score = (max)
		end


        return score
    end
    
    
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
    
