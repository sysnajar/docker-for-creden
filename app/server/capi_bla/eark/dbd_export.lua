#!/usr/bin/env lua
local redis = require 'redis'
local cjson = require 'cjson'

local key  = 'qupdate_'..arg[1]
local host = 'ec2-3-0-61-43.ap-southeast-1.compute.amazonaws.com'
local pwd  = 'sss123YummI'

if(not key)then print("please specify key"); os.exit(1); end
local rc = redis.connect(host, 6379)
rc:auth(pwd)

local t = rc:lrange(key, 0, -1)

local p1 = 'general'
local p2 = 'financial'

local good_list = {}
local bad_list  = {}

-- set up mongodb
db = 'newdbd'
col = 'company'
col2 = 'history'
col3 = 'address'
mongorover = require "mongorover"
client = mongorover.MongoClient.new("mongodb://localhost:27017/")
database = client:getDatabase(db)
col = database:getCollection(col)
col2 = database:getCollection(col2)
col3 = database:getCollection(col3)
obj = rc:get('get_diff_person')
obj = cjson.decode(obj)
for i,v in ipairs(obj) do
  print(cjson.encode(v))
  col2:update_one({name = v.name,jpNo = v.jpNo}, {["$set"] =  v }, true)
end

obj = rc:get('get_diff_address')
obj = cjson.decode(obj)
for i,v in ipairs(obj) do
  print(cjson.encode(v))
  col3:update_one(v, {["$set"] =  v }, true)
end
--
insert_count,update_count = 0, 0
print('Working on ', #t ,'items')
for i,v in ipairs(t) do
  --print(i,v)
   local k1 = p1 .. v
   local k2 = p2 .. v
   
--   if(i>10)then break end

   --if(i>5)then break end
   
   --print(i, k2, v)
   local command1 = 'cat /home/creden/dbdiff/'..k1..'.json'
   local command2 = 'cat /home/creden/dbdiff/'..k2..'.json'

   local handle = io.popen(command1)
   local v1 = handle:read("*a")
   handle:close()

   local handle = io.popen(command2)
   local v2 = handle:read("*a")
   handle:close()
   --local v1 = tostring(rc:get(k1))
   --local v2 = tostring(rc:get(k2))
       
   v1 = string.gsub(v1,':null',':""') 
   v2 = string.gsub(v2,':null',':""')
   v1 = string.gsub(v1,'"null"','""') 
   v2 = string.gsub(v2,'"null"','""')   
   v1 = string.gsub(v1,'null','""')
   v2 = string.gsub(v2,'null','""')
   --print(v1)
   --print(v2)
   local o = nil
   local validJSON = pcall(function()
       o = cjson.decode(v1)
       o.financialInfo = cjson.decode(v2)
       o['_id'] = v
   end)
   if(#v1>10 and validJSON and #v2>10)then
       --for k0,v0 in pairs(o.financialInfo) do if(type(v0)=='userdata') then print('financialInfo', k0) end end
    old = col:find_one({_id = v},{financialInfo = 1})

    tbf_old = {}
    if old.financialInfo then
      for i2,f in ipairs(old.financialInfo) do
        tbf_old[f.FISCAL_YEAR] = f
      end
    end
    
    for i2,f in ipairs(o.financialInfo) do
      tbf_old[f.FISCAL_YEAR] = f
    end

    tbf_new = {}
    for k,v in pairs(tbf_old) do
      table.insert(tbf_new,v)
    end
    o.financialInfo = tbf_new

    insert_res = col:update_one({['_id'] = v}, {["$set"] =  o }, true)
--    print(cjson.encode(insert_res))
    if(insert_res.matched_count==0)then
        insert_count = insert_count+1
      else    
        update_count = update_count+1
    end

     --save mongo 
     table.insert(good_list, v) 
  else
     print('bad key', v)  
     rc:lpush('bad_key',v)
     table.insert(bad_list, v) 
   end

end

print(insert_count, update_count, #bad_list)
rc:del(key)
