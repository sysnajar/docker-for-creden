#!/usr/bin/env lua
local cjson = require 'cjson'

 t = cjson.decode(arg[1])

 txt2 = "WEBVTT\n" 

 for k, v in ipairs(t) do
    txt2 = txt2..'00:'..v.startTime..'.000 --> 00:'..v.endTime..'.000\n'..v.word..'\n'
 end

 print(txt2)