local upload = require "resty.upload"
local cjson = require "cjson"
local chunk_size = 4096 -- should be set to 4096 or 8192

local file
local name =  tostring(os.time()) 
local file_name = '/home/creden/work/app/client/face/images/card/' .. name .. ".png"

local form, err = upload:new(chunk_size)
if not form then
  ngx.say "no form"
end

form:set_timeout(1000) -- 1 sec
file = io.open(file_name, "w+")

local len = 0
while true do
    local typ, res, err = form:read()
    if not typ then
        ngx.say("failed to read: ", err)
        return
    end

    if(typ=='body')then
    len = len + #res
    file:write(res)
    end

    if typ == "eof" then
        file:close()
        break
    end
end

local typ, res, err = form:read()
ngx.say (cjson.encode({success = (len>0), filename = name}))

