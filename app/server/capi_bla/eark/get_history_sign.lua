#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {owner = 'amorrut@creden.co'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end


conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
  
    local col_history = db_dopa:get_col("history_signer")
   
    local history, err_history = col_history:find({owner = body.owner})
   
    --user esig
    local get_table = {}
    if history then
        for k, v in history:pairs() do 
            v._id = tostring(v._id)
           table.insert(get_table, v) 
        end
        ret.success = true
        ret.data = get_table
    else
        ret.success = false
        ret.msg = 'not found'

    end
     
                
         
end

print(cjson.encode(ret))
