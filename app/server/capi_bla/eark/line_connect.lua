#!/usr/bin/env lua
local cjson = require 'cjson'
local common = require "common"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {}
local body = nil

prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
end

local function generate_token(len)
    local upperCase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
    local lowerCase = "abcdefghijklmnopqrstuvwxyz"
    local numbers = "0123456789"
    local characterSet = upperCase .. lowerCase .. numbers
    local output = ""

    math.randomseed(os.time())

    for	i = 1, len do
        local rand = math.random(#characterSet)
        output = output .. string.sub(characterSet, rand, rand)
    end

    return output
end

local function count_file(path)
    local out = '0'
    local res, err = prog('sh', '/home/creden/work/app/server/capi/shell_count_file.sh', path)
    if err then
        ngx.log(ngx.NOTICE,cjson.encode(err))
    end

    if res and res.stdout ~= '' then
        out = res.stdout:gsub("\n","")
        ngx.log(ngx.NOTICE,out)
    end

    return out
end

local function gen_qr(path, token)
    ngx.log(ngx.NOTICE, 'gen_qr')
    local res, err = prog('sh', '/home/creden/work/app/server/capi/line_qrcode.sh', path, token)
    if err then
        ngx.log(ngx.NOTICE, cjson.encode(err))
    end
end

if body then
    local response = {success = true, error_msg = '', qr = ''}
    local redis_data = {email = body.email, token = ''}
    local qr_path = '/home/creden/work/app/client/face/images/card/'
    local token_key = 'esign_line.'..'connect.'..body.email..'.'..'*'
    local token_key_list = rc:keys(token_key)
    
    if table.getn(token_key_list) > 0 then
        ngx.log(ngx.NOTICE, "line connnect case1")
        for k, v in ipairs(token_key_list) do
            rc:del(v)
        end
    end



        ngx.log(ngx.NOTICE, "line connnect generating new token")
        redis_data.token = generate_token(64)
        token_key = 'esign_line.'..'connect.'..body.email..'.'..redis_data.token
        qr_path = qr_path .. 'qr_' .. redis_data.token .. '.png'
        rc:set(token_key, cjson.encode(redis_data))
        gen_qr(qr_path, redis_data.token)
    

    response.qr = '/face/images/card/' .. 'qr_' .. redis_data.token .. '.png'
    return print(cjson.encode(response))
end
