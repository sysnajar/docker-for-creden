#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {id = '5ab81ecc6d75d2683f8a74b5'} 
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

        local session = require "resty.session".open({secret = "4321"})
        --update mongo
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if err then
            ret = {success = false, error_msg = err}
        else
            db = conn:new_db_handle("dopa")
            col = db:get_col("ekyc")
			local compCode = session.data.custCode or ''
            ngx.log(ngx.NOTICE,'get_item_detail#' .. tostring(body.id) ..', compCode = [' .. compCode..']')
            u, err = col:find_one({_id=object_id.new(convertHexStringToNormal(body.id)), compCode = compCode})
            if err then
                ret = {success = false, error_msg = err}
            else
                u._id = tostring(u._id)
				u.s = session.data.compCode
				
				if(u.approve_dmt)then
       		       u.approve_on = os.date('%d/%m/%Y %H:%M:%S', tonumber(u.approve_dmt)/1000  + (7 * 60 * 60) )
			    end

                ret = {success = true, error_msg = err, data = u}
            end
        end
    
print(cjson.encode(ret))


