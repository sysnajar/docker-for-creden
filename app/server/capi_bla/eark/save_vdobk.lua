#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local mongo = require "resty-mongol"
local body = nil
local HOST_NAME = 'https://dev2.hjkl.ninja'
prog.timeout_fatal = false
rc2 = redis.connect('52.77.238.216', 6379)
rc2:auth('noRedisPass1010')

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()

    ngx.log(ngx.NOTICE,body)
	
    body = cjson.decode(body)
    ngx.log(ngx.NOTICE,'txid:' .. (body.txid) .. ',usr:' .. tostring(body.usr))

else
 	body = {}
end

local ret = {success = false,  confidence = 0, error_msg = ""}

--save vdo
if(body.vdo_data)then
   rc:set(body.usr..'_'..body.txid,body.vdo_data)
end

ngx.log(ngx.NOTICE, 'CMD = sh /home/creden/work/app/server/capi/convert_vdo.sh '..body.usr..'_'..body.txid..' '..body.txid)
os.execute('sh /home/creden/work/app/server/capi/convert_vdo.sh '..body.usr..'_'..body.txid..' '..body.txid)

local need_rotate =  true
if body.is_pc and tostring(body.is_pc)=='true' then need_rotate = false end


if need_rotate then
	local res, ex_err = prog('ffprobe', '-v' ,'error', '-select_streams', 'v:0', '-show_entries', 'stream=width,height', '-of',
	'csv=s=x:p=0', '/home/creden/work/app/client/vdo/ex/' .. body.txid ..'.webm')

	if(res.stdout)then
	   ngx.log(ngx.NOTICE,"ROTATE vdo dimension W,H = " .. res.stdout)
	  local w =  res.stdout:match("%d+") 
	  local h =  res.stdout:match("x%d+")
	  w = tonumber(w)
	  h = tonumber(h:sub(2))
	  if(w>h)then      
		   ngx.log(ngx.NOTICE,"ROTATE VDO")
		   local res2, ex_err2 = prog('sh', '/home/creden/work/app/server/capi/rotate_vdo.sh', body.txid)
		   if(ex_err2)then
			  ngx.log(ngx.NOTICE,"ERROR ROTATE = " .. ex_err2)
		   else	  
			  ngx.log(ngx.NOTICE,"ROTATE STDOUT = " .. res2.stdout)
			end  
		   
	  else
		   ngx.log(ngx.NOTICE,"DONT ROTATE VDO")
	  end	   
	end
else
    ngx.log(ngx.NOTICE,"is_pc is true , skip vdo rotation")	
end




body.vdo = {}
body.vdo.missions = {}
body.vdo.url = HOST_NAME..'/vdo/ex/'..body.txid..'.webm'

function get_time(t)
    local time_str = ''
    if (tonumber(t) or 0) < 10 then
        time_str = '00:0'..t
    else
        time_str = '00:'..t
    end
    return time_str
end

--make json vdo
for k, v in ipairs(body.arr_missions) do
    local mission = {}
    mission.no = k
    mission.word = v.show
    mission.startTime = get_time(body.arr_times[k])
    mission.endTime = get_time((tonumber(body.arr_times[k]) or 0) + 3)
    if v.type == 'face' then mission.type = 'blink' else mission.type = 'voice' end
    table.insert(body.vdo.missions, mission)
end


if body.vdo.url and string.match(body.vdo.url, "http") then
    --TODO1 check video
    local vdo_req = {vdo=body.vdo, txid=body.txid, compCode=body.usr, urlPrefix=HOST_NAME}

            local key = 'vdo.queue.' .. body.txid
            local json = cjson.encode(vdo_req)
            ngx.log(ngx.NOTICE, 'Save vdo_req for later use for key >>>>>  [' .. key ..']')
            rc2:rpush('vdo.queue',json)
            rc2:set(key,json)
            rc2:expire(key,60*30) --30 minutes

		    --todo face verification here		

    ret.success = true
    ret.data = body.vdo
end

print(cjson.encode(ret))
