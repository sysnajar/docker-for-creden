#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {adminEmail = arg[1], activeCode = arg[2]} 
end

    local key_credits_used = 'counter.'..body.adminEmail..'.tx'
    local key_total_credits = 'counter.'..body.adminEmail..'.txMax'
    total_credits = rc:get(key_total_credits)
    credits_used = rc:get(key_credits_used)
    credits_balance = (tonumber(total_credits) or 0) - (tonumber(credits_used) or 0)

    ret = {success = false, error_msg = '', total_credits = total_credits, credits_used = credits_used, credits_balance = credits_balance}

print(cjson.encode(ret))


