#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false
local HOST_NAME = os.getenv("HOST_NAME")
-- local HOST_NAME = 'https://hjkl.ninja/'
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
conn = mongo()
conn:set_timeout(10000)
ok, err = conn:connect('127.0.0.1',27017)
db = conn:new_db_handle("dopa")
col = db:get_col("esig_user")
local ekyc_db = db:get_col("ekyc")

function get_timezone_thai (time)
	-- return os.date()
	
    local format="(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)"
    local mon,day,year,hour,min,sec = tostring(time):match(format)
    local dt = {year=tonumber('20'..year), month=tonumber(mon), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=tonumber(sec)}
    local time2 = os.time(dt)
    local time3 = os.date('%d/%m/%Y %H:%M:%S', tonumber(time2)  + (7 * 60 * 60) )
    return time3
end
function get_key_cert (user)
    local key = ""
    u , err = col:find_one({adminEmail=user})
    if u then
        if (u.last_cert_info) then    
            local tx = u.last_cert_info.tx
            key = serial(tx)
        end
    end
    return key
end

function serial(str)
    local i1 = string.find(str, 'Serial')
    local i2 = string.find(str, 'Signature')
    local ret = string.sub(str, i1 , i2)
    ret = string.gsub(ret, "Serial Number:", "")
    ret = string.sub(ret,1,#ret-2)
    return ret
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
else
    body  = {} 
end
-- data = '{"pdfFile":"1534752427","ip_stamp":"96.30.94.34","width":722,"id":"1534752427","createdDtm":1534752457092,"owner":"kitchinvuttinunt@gmail.com","subject":"Please sign หนังสือรับรองรายได้ผู้ปกครอง-ทกศ.032 (1).pdf","imageMerge":"pdf1534752424.png","fields":[{"x":0,"toolName":"Signature","id":1534752457055,"page":0,"xp":30,"status":1,"email":"kitchinvuttinunt@gmail.com","time":"08\/20\/18 08:07:52","scale":100,"yp":12,"ip_field":"96.30.94.34","toolData":"Signature","n":1,"y":0,"type":"draw"}],"signers":[{"needToSign":true,"eKyc":false,"usePassword":false,"name":"kit","password":"","email":"kitchinvuttinunt@gmail.com"}],"height":1021,"originalFile":"หนังสือรับรองรายได้ผู้ปกครอง-ทกศ.032 (1).pdf","msg":"","status":1,"imageFiles":[{"fileName":"pdf1534752424.png","aspectRatio":0.70665083135392}]}'
-- data = [[
-- {"pdfFile":"1618221711","draft_id":"","id":"1618221711","createdDtm":1618221717793,"owner":"pathapong3@creden.co","subject":"Please sign ","msg":"","signers":[{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"thanaphas@gmail.com","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false}],"height":0,"originalFile":"ใบเบิกค่าใช้จ่าย.pdf","imageFiles":[{"fileName":"pdf256613421618221705.png","aspectRatio":0.70718232044199}],"ip_stamp":"49.49.244.64","current_order":1,"attachments":{},"imageMerge":"pdf256613421618221705.png","status":1,"name":"ooookk oooaoaao","width":0,"company_id":"607419a70e3d65f7da4e3b22","use_signing_order":false,"fields":[{"toolName":"Signature","id":1618221714034,"page":0,"xp":44.933033559084,"status":1,"email":"pathapong3@creden.co","time":"04\/12\/21 10:01:57","ip_field":"49.49.244.64","yp":45.820622064758,"n":1,"signOrder":1,"isNew":true,"person":{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"pathapong3@creden.co","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false},"type":"draw"}],"filesize":"70487","is_template":false,"document_type_id":"","sign_only":true,"concensus_list":[{"at_least":1,"total":0}],"exp_date":"2021-08-10"}
-- ]]
data = [[
    {"pdfFile":"1618221711","draft_id":"","id":"1618221711","createdDtm":1618221717793,"owner":"pathapong3@creden.co","subject":"Please sign ","msg":"","signers":[{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"thanaphas@gmail.com","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false},{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"pathapong3@creden.co","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false},{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"pathapong3@creden.co","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false}],"height":0,"originalFile":"ใบเบิกค่าใช้จ่าย.pdf","imageFiles":[{"fileName":"pdf256613421618221705.png","aspectRatio":0.70718232044199}],"ip_stamp":"49.49.244.64","current_order":1,"attachments":{},"imageMerge":"pdf256613421618221705.png","status":1,"name":"ooookk oooaoaao","width":0,"company_id":"607419a70e3d65f7da4e3b22","use_signing_order":false,"fields":[{"toolName":"Signature","id":1618221714034,"page":0,"xp":44.933033559084,"status":1,"email":"pathapong3@creden.co","time":"04\/12\/21 10:01:57","ip_field":"49.49.244.64","yp":45.820622064758,"n":1,"signOrder":1,"isNew":true,"person":{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"pathapong3@creden.co","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false},"type":"draw"},{"toolName":"Signature","id":1618221714034,"page":0,"xp":44.933033559084,"status":1,"email":"thanaphas@gmail.com","time":"04\/12\/21 10:01:57","ip_field":"49.49.244.64","yp":45.820622064758,"n":1,"signOrder":1,"isNew":true,"person":{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"pathapong3@creden.co","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false},"type":"draw"},{"toolName":"Signature","id":1618221714034,"page":0,"xp":44.933033559084,"status":1,"email":"thanaphas@gmail.com","time":"04\/12\/21 10:01:57","ip_field":"49.49.244.64","yp":45.820622064758,"n":1,"signOrder":1,"isNew":true,"person":{"selectedPersonIndex":0,"needToSign":true,"least_sign":true,"position_id":"","name":"ooookk oooaoaao","order_set":"","email":"pathapong3@creden.co","is_group":false,"amount_least_sign":1,"eKyc":false,"usePassword":false,"title":"","user_group":{},"password":"","substitute_sign":false},"type":"draw"}],"filesize":"70487","is_template":false,"document_type_id":"","sign_only":true,"concensus_list":[{"at_least":1,"total":0}],"exp_date":"2021-08-10"}
]]
body = cjson.decode(data)
--body = cjson.decode(arg[1])
cert_page = (arg[2]) and arg[2] or '1'

-- edoc_db = conn:new_db_handle("edoc")
-- edoc_col = edoc_db:get_col("edoc_list")
-- edoc = edoc_col:find_one({id="1615130624"})
-- body = edoc


t = {}
table.sort(body.fields,function (f1, f2) 
	return tostring(f1.time) < tostring(f2.time)
    --return f1.time<f2.time
end)


local fields2 = {}
local tt = {}

for k,v in pairs(body.fields) do
    if(not tt[v.email])then
      tt[v.email] = true
      table.insert(fields2, v)
    end
end

s = col:find_one({adminEmail =body.owner })

-- fix API signing produce no timestamp
if  s.FNAME ~= nil  and   s.LNAME ~= nil then
    FNAME =  s.FNAME .. ' ' .. s.LNAME
else
    FNAME=  s.adminFisrtname .. ' ' ..s.adminLastname or body.owner


end
--  FNAME= s.FNAME ..' '.. s.LNAME or body.owner 
--  s.LNAME = s.LNAME or ''
-- s.FNAME = 'ppp'
-- s.LNAME = 'aaaa'
html_head_pat = 
[[
    <!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
            <style>
            @font-face {
                font-family: 'THSarabunNew';
                src: url('./sarabun-webfont-master/fonts/thsarabunnew-webfont.woff');
                font-weight: normal;
                font-style: normal;
            } 
           
            body {
                font-family: 'THSarabunNew';
                color: #000;
            } 
            table{
                width:100%;
                border: 0.5px solid #4F4F4F;
                border-collapse: collapse;
            }
            tr, td{
                border: 0.5px solid #4F4F4F;
            }
            .bg-dark-blue{
                background: #1667B2;
            }
            .f-w{
                color: #fff;
            }
            .bg-blue{
                background: #96D6EC;
            }
            .bg-yellow{
                background: #F8E2A0;
            }
            .bg-gray{
                background: #F2F2F2;
            }
            .f-10{
                font-size: 16px;
            }
            .f-8{
                font-size: 14px;
            }
            .p-5-10{
                padding: 2px 10px;
            }
            .text-end{
                text-align: start;
            }
            .text-center{
                text-align: center;
            }
            .w-200{
                -- width: 200px;
            }
            b{
                font-weight: bold;
            }
            p{
                margin : 0;
                line-height: 21px;
            }
            </style>
           
        </head>
        <body class="page">
]]

table_cer =
[[
    <table style='margin-bottom: 20px;'>
            <thead>
                <tr class='bg-dark-blue' >
                    <td colspan='4' class='f-10 p-5-10 f-w' >
                    <b>
                        <p>Certificate Of Completion</p>
                        <p>ใบรับรองการลงลายมือชื่ออิเล็กทรอนิกส์</p>
                    </b>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Subject:</p>
                        <p>ชื่อเอกสาร:</p>
                    </td>
                    <td colspan='3' class='f-8 p-5-10'>
                    ]]..body.originalFile..[[
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Document ID:</p>
                        <p>หมายเลขเอกสาร:</p>
                    </td>
                    <td class='f-8 p-5-10'>
                    ]]..body.id..[[
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'>
                        <p>Document Pages:</p>
                        <p>จำนวนหน้าเอกสาร:</p> 
                    </td>
                    <td class='f-8 p-5-10'>
                    ]]..table.getn(body.imageFiles)..[[
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Signatures:</p>
                        <p>ลายเซ็น:</p> 
                    </td>
                    <td class='f-8 p-5-10'>
                    ]]..table.getn(body.signers)..[[
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Sent for Signatures:</p>
                        <p>ส่งเมื่อ:</p> 
                    </td>
                    <td class='f-8 p-5-10'>
                    ]]..os.date('%d/%m/%Y %H:%M:%S',body.createdDtm/1000 + (7 * 60 * 60))..[[
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'>
                        <p>Certificate Pages:</p>
                        <p>จำนวนหน้าใบรับรอง:</p>  
                    </td>
                    <td class='f-8 p-5-10'>
                        ]] .. cert_page ..[[
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Timestamp:</p>
                        <p>การประทับเวลา:</p>  
                    </td>
                    <td class='f-8 p-5-10'>
                        <p>Enabled</p>
                        <p>เปิดใช้งาน</p>  
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                    Time Zone:
                    </td>
                    <td colspan='3' class='f-8 p-5-10'>
                     (GMT+07:00) Bangkok
                    </td>
                </tr>
            </tbody>
        </table>
]]

table_tracking = 
[[
    <table style='margin-bottom: 20px;'>
            <thead>
                <tr class='bg-dark-blue' >
                    <td colspan='6' class='f-10 p-5-10 f-w' >
                    <b>
                        <p>Document Record Tracking</p>
                        <p>ติดตามการลงรายการในเอกสาร</p>  
                    </b>
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Document:</p>
                        <p>เอกสาร:</p> 
                    </td>
                    <td class='f-8 p-5-10'>
                        <p>Original</p>
                        <p>ต้นฉบับ</p> 
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'>
                        <p>Document Holder:</p>
                        <p>เจ้าของ/ผู้สร้างเอกสาร:</p>  
                    </td>
                    <td  class='f-8 p-5-10'>
                        <p>]]..FNAME..[[</p>
                        <p>]]..body.owner..[[</p> 
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Location:</p>
                        <p>สถานที่สร้างเอกสาร:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                        Creden e-Signature Platform
                    </td>
                </tr>
            </tbody>
        </table>
]]

table_signer_even_head1 =
[[
    <table>
        <thead>
            <tr class='bg-blue'>
                <td colspan='5' class='f-10 p-5-10 text-center'>
                    <p>Signer Events</p>
                    <p>ลำดับรายการผู้ลงลายมือชื่อ</p> 
                </td>
            </tr>
        </thead>
    </table>
]]

table_signer_even_head =
[[
    <table style='margin-bottom: 20px;'>
            <tbody>
]]

local signer_event = {}

for num_sign,l_signer in ipairs(body.signers) do
    
    new_sign = { name = l_signer.name, email = l_signer.email }
    -- new_sign = {email = l_signer.email }
    -- get ekyc level
   
    -- print(l_signer.email)
    get_cer = rc:get(body.id..'.'..l_signer.email) 
    print(l_signer.email) 
    if get_cer then
        new_sign.enabled_en = "Enabled"
        new_sign.enabled_th = "เปิดใช้งาน"
        new_sign.key =  get_key_cert(l_signer.email)
        -- new_sign.key =  "0000000"
    else
        new_sign.enabled_en = "-"
        new_sign.enabled_th = ""
        new_sign.key = "-"
    end
    print(new_sign.key) 
    find_ekyc = ekyc_db:find_one({adminEmail = l_signer.email })
        
        if find_ekyc then
            print("=====317====EKYC=") 
            if not find_ekyc.ekyc_level then
                print("=====319====EKYC=") 
                new_sign.ekyc_level = '-'
                print("=====321====EKYC=") 
            else
                if tonumber(find_ekyc.ekyc_level) == 0 then
                    print("=====324====EKYC=") 
                    new_sign.ekyc_level = '-'
                    print("=====326====EKYC=") 
                else
                    -- if(find_ekyc.ekyc_level == 2.2 or find_ekyc.ekyc_level == '2.2') then
                    --     find_ekyc.ekyc_level = 2.3
                    -- end
                    print("=====331====EKYC=") 
                    new_sign.ekyc_level = 'IAL ' .. find_ekyc.ekyc_level
                    print("=====333====EKYC=") 
                end
                
            end

             new_sign.name = find_ekyc.fnameTH .. ' ' .. find_ekyc.lnameTH
            print("ekyc find")
        else
            print("=====341====EKYC=") 
            new_sign.ekyc_level = '-'
            new_sign.name  = l_signer.name
            print("=====344====="..l_signer.name)

        end

        for i2,v2 in ipairs(body.fields) do
            print(l_signer.email) 
            print(v2.email) 
            if l_signer.email == v2.email then
                new_sign.ip = v2.ip_field
            else
                new_sign.ip = "-"
            end
        end
    
    --loop end get ekyc level

    -- for f_index,l_fields in ipairs(body.fields) do
    --     if (l_signer.is_group) then
    --         for g,group in ipairs(l_signer.user_group) do
    --             if(group.email == l_fields.email) then
    --                 new_sign.time3 = get_timezone_thai(l_fields.time) or '-'
    --                 new_sign.email = l_fields.email
    --             end
    --         end
    --     else
    --         if(l_signer.email == l_fields.email) then
    --             new_sign.time3 = get_timezone_thai(l_fields.time) or '-'
    --         end
    --     end
        
    -- end

    if(l_signer.needToSign == true) then
        table.insert(signer_event,new_sign)
    end

end

table_signer_even_detail = ''
for n,new_s in ipairs(signer_event) do
    
    table_signer_even_detail = table_signer_even_detail .. 
    [[
                <tr style='height: 10px;' >
                    <td colspan='5' class='bg-yellow'></td>
                </tr>
                <tr>
                    <td rowspan='4' class='p-5-10 bg-blue w-200 text-center' style='font-size: 20px;' > 
                       (]]..n..[[)
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'>
                        <p>Signer:</p>
                        <p>ผู้ลงนาม:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                        <p>]]..new_s.name..[[</p>
                        <p>(]]..new_s.email..[[)</p> 
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Signed:</p>
                        <p>ลงลายมือชื่อเมื่อ:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                    11/05/2021 11:48:27
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Securtity Level:</p>
                        <p>ระดับความปลอดภัย:</p> 
                    </td>
                    <td class='f-8 p-5-10'>
                    <p>Email/OTP</p>
                    <p>อีเมล/ชุดรหัสผ่านแบบใช้ครั้งเดียว</p> 
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Digital Certificate:</p>
                        <p>การใช้ใบรับรอง:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                    <p>]]..new_s.enabled_en..[[</p>
                    <p>]]..new_s.enabled_th..[[</p> 
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>e-KYC:</p>
                        <p>การยืนยันตัวตนทางอิเล็กทรอนิกส์:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                        ]]..new_s.ekyc_level..[[
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Serial Number:</p>
                        <p>หมายเลขซีเรียล:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                        ]]..new_s.key..[[
                    </td>
                </tr>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                    <p>Using IP Address</p>
                    <p>ใช้หมายเลข IP Address</p> 
                    
                    </td>
                    <td colspan='3' class='f-8 p-5-10'>
                    ]]..new_s.ip..[[
                    </td>
                </tr>
                
]]
end

-- table_signer_even_detail = ''
-- for i2,v2 in ipairs(body.fields) do
--     s2 = col:find_one({adminEmail =v2.email })
    
--     if (not s2) then 
--         s2 = {} -- API user may not have creden account
--     end
    
--     if not s2.ekyc_level then
--         s2.ekyc_level = '-'
--     else
--         s2.ekyc_level = 'IAL ' .. s2.ekyc_level
--     end

--     kyc_text = ''
--     if s2.verified_ekyc == true then
--         kyc_text = ',EKYC'
--     end
--     -- v2.name = v2.person.name
--     -- if FNAME then
--     --     em = ' ('..v2.email..')'
--     -- else
--     --     em = v2.email
--     -- end

--     if (not v2.time3) then 
--         v2.time3 =  '-'
--     end

--     table_signer_even_detail = table_signer_even_detail .. 
--     [[
--     <tr style='height: 21px;' >
--                     <td colspan='5'></td>
--                 </tr>
--                 <tr>
--                     <td rowspan='3' class='p-5-10 bg-blue w-200 text-center' style='font-size: 20px;' > 
--                        (]]..i2..[[)
--                     </td>
--                     <td class='f-8 p-5-10 bg-gray text-end w-200'> 
--                         Signer:
--                         <br>
--                         ผู้ลงนาม:
--                     </td>
--                     <td  class='f-8 p-5-10'>
--                     ]]..v2.person.name..[[
--                         <br>
--                         (]]..v2.email..[[)
--                     </td>
--                     <td class='f-8 p-5-10 bg-gray text-end w-200'> 
--                         Signed:
--                         <br>
--                         ลงลายมือชื่อเมื่อ:
--                     </td>
--                     <td  class='f-8 p-5-10'>
--                     ]]..v2.time3..[[
--                     </td>
--                 </tr>
--                 <tr>
--                     <td class='f-8 p-5-10 bg-gray text-end w-200'> 
--                         Securtity Level:
--                         <br>
--                         ระดับความปลอดภัย:
--                     </td>
--                     <td colspan='3' class='f-8 p-5-10'>
--                     Email/OTP
--                     <br>
--                     อีเมล/ชุดรหัสผ่านแบบใช้ครั้งเดียว
--                     </td>
--                 </tr>
--                 <tr>
--                     <td class='f-8 p-5-10 bg-gray text-end w-200'> 
--                         e-KYC:
--                         <br>
--                         การยืนยันตัวตนทางอิเล็กทรอนิกส์:
--                     </td>
--                     <td  class='f-8 p-5-10'>
--                         ]]..s2.ekyc_level..[[
--                     </td>
--                     <td class='f-8 p-5-10 bg-gray text-end w-200'> 
--                         Digital Certificate:
--                         <br>
--                         ผู้ลงนาม:
--                     </td>
--                     <td  class='f-8 p-5-10'>
--                         Enabled
--                         <br>
--                         เปิดใช้งาน
--                     </td>
--                 </tr>
-- ]]
-- end







table_signer_even_end = [[</tbody></table>]]

table_eummary = 
[[
    <table style='margin-bottom: 40px;'>
            <thead>
                <tr class='bg-blue'>
                    <td colspan='6' class='f-10 p-5-10 text-center' >
                    <p>Summary Document Events</p>
                    <p>สรุปรายการเอกสาร</p> 
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Sending Complete:</p>
                        <p>ส่งเสร็จสมบูรณ์เมื่อ:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                    ]]..os.date('%d/%m/%Y %H:%M:%S',body.createdDtm/1000 + (7 * 60 * 60))..[[
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Signing Complete:</p>
                        <p>ลงลายมือชื่อครบถ้วนเมื่อ:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                    ]].. get_timezone_thai(body.fields[table.getn(body.fields)].time) ..[[
                    </td>
                    <td class='f-8 p-5-10 bg-gray text-end w-200'> 
                        <p>Document Complete:</p>
                        <p>เอกสารเสร็จสมบูรณ์:</p> 
                    </td>
                    <td  class='f-8 p-5-10'>
                    ]]..os.date('%d/%m/%Y %H:%M:%S', os.time()+ (7 * 60 * 60))..[[
                    </td>
                </tr>
            </tbody>
        </table>
]]

heml_end_pat = [[  </body></html>]]
n = 'test_pat16'
html_pat = html_head_pat..table_cer..table_tracking..table_signer_even_head1..table_signer_even_head..table_signer_even_detail..table_signer_even_end..table_eummary..heml_end_pat

os.execute('sh /home/creden/work/app/server/capi/convert_htmltopdf.sh "'..html_pat..'" '..body.id..'ts')
print("A SET show else" .. tostring(body.id))

-- if #signer_event == 2 then
--     print("A SET Ok TO signer_event")
--     -- cut = 'sh /home/creden/work/app/server/capi/cut_coc_page.sh'..body.id..'ts'
--     -- os.execute(pdftk /home/creden/work/app/client/face/images/card/"$1".pdf cat 1 output /home/creden/work/app/client/face/images/card/"$1".pdf)
--     html_pat = html_head_pat..table_cer..table_tracking..table_signer_even_head1..table_signer_even_head..table_signer_even_detail..table_signer_even_end..table_eummary..heml_end_pat

--     os.execute('sh /home/creden/work/app/server/capi/convert_htmltopdf.sh "'..html_pat..'" '..body.id..'t')
--     local res, err = prog('pdftk' , '/home/creden/work/app/client/face/images/card/'..body.id..'t.pdf', 'cat', '1' , 'output', '/home/creden/work/app/client/face/images/card/'..body.id..'ts.pdf')
--     if not err then
--     print("A SET Ok TO pdfk" .. tostring(body.id))
--     else
--         print("A SET errro TO pdffk" .. tostring(body.id))
--     end
-- else
--     html_pat = html_head_pat..table_cer..table_tracking..table_signer_even_head1..table_signer_even_head..table_signer_even_detail..table_signer_even_end..table_eummary..heml_end_pat

--     os.execute('sh /home/creden/work/app/server/capi/convert_htmltopdf.sh "'..html_pat..'" '..body.id..'ts')
--     print("A SET show else" .. tostring(body.id))
-- end
