#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
local common = require "common"
local bson = require "resty-mongol.bson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { email = arg[1], content = arg[2] ,content_txt = arg[3] }
end

function rand(len)
    local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
    local t = {}
    local cnt = 0
     
    local i = 1
    repeat
        local f = assert (io.popen (cmd, 'r'))
        local line = f:read('*all'):match("%d+")
        f:close()
        cnt = cnt + #line
        table.insert(t, line)
    until cnt>len
    
            
    local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
    return ret
end

function get_timezone_thai (time)
    local format="(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)"
    local day,mon,year,hour,min,sec = tostring(time):match(format)
    local dt = {year=tonumber(year), month=tonumber(mon), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=tonumber(sec)}
    local time2 = os.time(dt)
    local time3 = os.date('%d/%m/%Y %H:%M:%S', tonumber(time2)  + (7 * 60 * 60) )
    return time3
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else

    db = conn:new_db_handle("edoc")
    col = db:get_col("eform")

    html = [[<!DOCTYPE html>
<html>
<head>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
</head>

<body style='max-width:100%;max-height: 100%;'>
   <div>]] .. body.content ..[[</div>
</body>
</html>]]


    name = "eform" .. rand(8) .. os.time()
    -- name = "eform2" 
    local c,err = prog('sh','/home/creden/work/app/server_thaicom/convert_eformhtmltopdf.sh', html, name)
    local date = os.date('%d/%m/%Y %H:%M:%S')

    -- base64 = prog('sh','/home/creden/work/app/server_thaicom/convert_pdftobase64.sh', name)
    -- data_base64 = prog('cat','/home/creden/work/app/client/face/images/card/eform.base64')
    -- data_base64 = data_base64.stdout
    -- content_no_style = replace_br_tags(body.content_no_style)
    -- tax = replace_img_tags(content_no_style)

    xml = [[
        <?xml version="1.0" encoding="UTF-8"?>
            <rsm:TaxInvoice_CrossIndustryInvoice xmlns:rsm="urn:etda:uncefact:data:standard:TaxInvoice_CrossIndustryInvoice:2" xmlns:ram="urn:etda:uncefact:data:standard:TaxInvoice_ReusableAggregateBusinessInformationEntity:2" xmlns:ns3="http://www.w3.org/2000/09/xmldsig#">
            <rsm:ExchangedDocumentContext>
                <ram:GuidelineSpecifiedDocumentContextParameter>
                    <ram:ID schemeAgencyID="ETDA" schemeVersionID="v2.0">ER3-2560</ram:ID>
                </ram:GuidelineSpecifiedDocumentContextParameter>
            </rsm:ExchangedDocumentContext>
            <rsm:ExchangedDocument>
                <OWNER>]] .. body.email .. [[</OWNER>
                <DATE>]] .. get_timezone_thai(date) .. [[</DATE>
                <TYPE_EFORM>]] ..body.type_eform .. [[</TYPE_EFORM>
                <DOCUMENT_DETAIL>]] .. body.content_txt .. [[</DOCUMENT_DETAIL>
            </rsm:ExchangedDocument>
            </rsm:TaxInvoice_CrossIndustryInvoice>
    ]]
    
    local x,err = prog('sh','/home/creden/work/app/server/capi/convert_pdftoxml.sh', xml, name)
    -- ngx.sleep(5)

    -- XML block chain
    local ts_file = '/home/creden/work/app/client/face/images/card/' .. name .. '.xml'
    local bc_cmd = 'lua /home/creden/scripts/put_tx.lua '.. name .. ' "' .. ts_file  ..'"' ..' | node /home/creden/scripts/stellar_tx_pub2.js | /home/creden/scripts/rset.lua bc:' .. name .. ' &'
    rc:set('last_bc',bc_cmd)
    os.execute(bc_cmd)
    -- End XML block chain

    ngx.sleep(10)

    local hash =  rc:get('bc:' .. name)
    local new_eform = { 
        filename_pdf = name .. ".pdf", 
        filename_xml = name .. ".xml", 
        owner = body.email,
        createDtm = date,
        hash_xml = hash,
        eform = name,
        type_eform = body.type_eform,
        urgently = body.urgently,
        secretly = body.secretly,
        content = body.content,
        content_no_style = body.content_no_style,
        content_txt = body.content_txt
    }
    u,err = col:insert({ new_eform })
    
    -- end



    if c then 
        ret = { success = true, error_msg = "Add eform success", name_pdf = name }
    else
        ret = { success = false, error_msg = "Add eform not found" }
    end


end

print(cjson.encode(ret))
