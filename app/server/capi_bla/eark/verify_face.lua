#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_post_args()
else
   body  = {faceId1 = arg[1], faceId2 = arg[2]} 
end


local t = {success = false}
require "socket"
local t1 = socket.gettime()
local ret, err = prog('sh', '/home/creden/work/app/server/capi/curl_verify.sh', body.faceId1 , body.faceId2)
if ret.stdout then
	t.data = cjson.decode(ret.stdout)
	t.success = true
	t.processingTime = tostring((socket.gettime()*1000) - (t1*1000)):match('%d+') 
end
print(cjson.encode(t))

