#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil


function trim(s)
    return (s:gsub("^%s*(.-)%s*$", "%1"))
end

local function fromCSV(s)
   s = s .. ','        -- ending comma
   local t = {}        -- table to collect fields
   local fieldstart = 1
   repeat
     -- next field is quoted? (start with `"'?)
     if string.find(s, '^"', fieldstart) then
       local a, c
       local i  = fieldstart
       repeat
         -- find closing quote
         a, i, c = string.find(s, '"("?)', i+1)
       until c ~= '"'    -- quote not followed by quote?
       if not i then error('unmatched "') end
       local f = string.sub(s, fieldstart+1, i-1)
       table.insert(t, (string.gsub(f, '""', '"')))
       fieldstart = string.find(s, ',', i) + 1
     else                -- unquoted; find next comma
       local nexti = string.find(s, ',', fieldstart)
       table.insert(t, string.sub(s, fieldstart, nexti-1))
       fieldstart = nexti + 1
     end
   until fieldstart > string.len(s)
   return t
end

local field = {"ZipNO","RefNo","Name","Lname","IDNO","LaserCode","img1","img2","img3"}
local data = {}

local conn = mongo()
conn:set_timeout(10000)
local ok, err = conn:connect('127.0.0.1',27017)
local db = conn:new_db_handle("dopa")
local col = db:get_col("ekyc_zipmex_col")


-- local chk_ref = col:find({})
-- for i,v in chk_ref:pairs() do
--   print(v.RefNo)
-- end
 for line in io.stdin:lines() do
  line = trim(line)
  --print(line)
  local csv = fromCSV(line)

  local data = {}
  
  data.cardNo = csv[2]
  data.fnameTH = csv[3]
  data.lnameTH = csv[4]
  data.dob = csv[5]
  data.laserCode = csv[6]
  data.verified_ekyc = csv[7]
  data.remark = csv[8]
  local chk_ref = col:find_one({ref_no=csv[1]})
  if(chk_ref) then
      local query = {ref_no=csv[1]}
      local upd = { ["$set"]=data }
      local update,err = col:update(query,upd,0,1,true)
   
  else
    print(csv[1])
    os.exit()
  end


  end

-- end

--     line = trim(line)
--     --local refno = line
--     --print(refno)
--     local text = line
--     -- print(cjson.encode(text))
--     -- if(#text > 2) then
  
--     -- end
--     local chk_ref = col:find_one({RefNo=text})
--     if(chk_ref)then
--      -- print('success')
--       local query = {RefNo=text}
--       local upd = { ["$set"]={Status="success"} }
--       local update,err = col:update(query,upd,0,1,true)
    
--     else
--       -- print('not see')
--       -- print(cjson.encode(text))
--       -- os.exit()


