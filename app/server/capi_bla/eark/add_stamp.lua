#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { company_id = '123', folder = 'than'}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else    
    
    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("stamp")
    local date = bson.get_utc_date(os.time() * 1000)

    if body._id then
        e, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(body._id)), company_id = body.company_id })

        if err then
            ret = { success = false, error_msg = "find not stamp", error_code = "" }
        else

            local key = { _id = e._id, company_id = body.company_id }
            local update_cmd = {
                stamp_name = body.stamp_name,
                updated_dtm = date
            }
            local u, err = col:update(key, {["$set"] = update_cmd }, 0, 0, true)

            if err then
                ret = { success = false, error_msg = "Update stamp unsuccess", error_code = "" }
            else
                ret = { success = true, error_msg = "Update stamp success", error_code = "" }
            end

        end

    else
        body.created_dtm = date
        u, err = col:insert({body})
        if err then
            ret = { success = false, error_msg = "Add stamp unsuccess", error_code = "" }
        else 
            ret = { success = true, error_msg = "Add stamp success", error_code = "" }
        end
    end
end

print(cjson.encode(ret))
