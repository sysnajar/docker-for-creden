if [ $# -ge 2 ]
then
    modelnum="$2"
else
    modelnum="02"
fi
#echo "model num = "$modelnum
curl -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&detectionModel=detection_$modelnum&recognitionModel=recognition_$modelnum&returnRecognitionModel=true" -H "Content-Type: application/octet-stream" -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" --data-binary "@$1"
