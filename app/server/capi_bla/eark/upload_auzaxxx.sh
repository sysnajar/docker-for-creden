# curl -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&recognitionModel=recognition_02&returnRecognitionModel=false&detectionModel=detection_02" \
# -H "Content-Type: application/json" \
# -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" \
# --data-ascii '{"url": "$1"}'
# curl -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&detectionModel=detection_02&recognitionModel=recognition_02" -H "Content-Type: application/octet-stream" -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" --data-binary "@$1"
curl -v -X POST "https://southeastasia.api.cognitive.microsoft.com/face/v1.0/detect?returnFaceId=true&returnFaceLandmarks=false&detectionModel=detection_$1&recognitionModel=recognition_$2&returnRecognitionModel=true" -H "Content-Type: application/octet-stream" -H "Ocp-Apim-Subscription-Key: 4fcf5b2f10d448b89f66ba94d2593444" --data-binary "@$3"
