#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function remove_unit_arr (arr)
    local hash = {}
    local res = {}
    for _,v in ipairs(arr) do
        if (not hash[v]) then
            res[#res+1] = v -- you could print here instead of saving to result table if you wanted
            hash[v] = true
        end
     end
     return res
end

function removekey(table, key)
    local element = table[key]
    table[key] = nil
    return element
end

function remove_doc_before_move_folder(before, doc)
    if before ~= "N/A" then

        local folder = { owner_folder = body.email, _id = object_id.new(convertHexStringToNormal(body.before)) }
        local f, err = col:find_one(folder)

        local list = {}

        for i, v in ipairs(f.list_doc) do
            for i2, v2 in ipairs(doc) do 
                if v == v2 then
                    v = nil
                end
            end

            if v then
                table.insert(list, v)
            end
        end

        list = remove_unit_arr(list)

        local update_cmd ={["$set"] = { 
            list_doc = list, 
            updated_Dtm = bson.get_utc_date(ngx.now() * 1000)
        }}
        local u, err = col:update( folder, update_cmd, 1, 0, true)
        if err then
            ret = { success = false, error_msg = "restore document unsuccess", error_code = "2107" } 
            print(cjson.encode(ret))
            ngx.exit(ngx.HTTP_OK)
        else
            ret = { success = true, error_msg = "restore document success", error_code = "2108" } 
            print(cjson.encode(ret))
            ngx.exit(ngx.HTTP_OK)
        end
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { id_doc = arg[1], id_folder = arg[2] }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1', 27017)
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else    

    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders2")

    folder = { owner_folder = body.email, cat = body.cat }
    f, err = col:find_one(folder)

    if err then
        ret = { success = false, error_msg = "folder not find", error_code = "2205"}
    else
        remove_doc_before_move_folder(body.before, body.list_doc)
    end
end
print(cjson.encode(ret))
