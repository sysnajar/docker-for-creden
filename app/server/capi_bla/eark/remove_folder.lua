#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
-- bson = require "resty-mongol.bson"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { id_folder = arg[1]}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else    
    db = conn:new_db_handle("edoc")
    col = db:get_col("folders")

    folder = { _id = object_id.new(convertHexStringToNormal(body.id_folder))}
    f,err = col:find_one(folder)

    if f then
        i, err = col:delete({_id = object_id.new(convertHexStringToNormal(body.id_folder))},1,0)
        if i then
            ret = {error_msg = true, data = "Remove folder success"}
        else
            ret = {error_msg = false , data = "Not remove folder"}
        end
    else
        ret = {error_msg = "No Folder"}
    end
end
print(cjson.encode(ret))
