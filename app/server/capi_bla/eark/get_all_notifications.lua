#!/usr/bin/env lua 

local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local session = require "resty.session".open({secret = "4321"})
print = ngx.say

session.data.last_update = os.date()
session:save(false)
u = session.data.username or ""



--Load notifications
conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
ret = {success = false, error_msg = err}
else
db = conn:new_db_handle("edoc") --db name
col = db:get_col("notification") -- collection
--s, err = col:find({email = u})
s, err = col:find({email = (u=="" and "N/A" or u)})

n_list = {} 

for k, v in s:pairs() do
v._id = nil
table.insert(n_list , v)
end

if(#n_list<1)then n_list = nil end

end
---------------------------------------------

local t = {success = true ,
notifications = n_list
}


print( cjson.encode(t) )
