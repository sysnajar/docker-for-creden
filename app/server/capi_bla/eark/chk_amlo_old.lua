#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {card_no = arg[1]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    isValid = true
    db = conn:new_db_handle("amlo")
    col = db:get_col("amlo_list")
    s = col:find({id = body.card_no})

    if not body.card_no or body.card_no == '' then isValid = nil end

    if s then
        data = {}
        for k, v in s:pairs() do
            isValid = false
            v._id = nil
            table.insert(data, k, v)
        end
        ret = {success = true, error_msg = "complete", data = data, isValid = isValid}
    else
        ret = {success = false, error_msg = "err"}
    end
end
print(cjson.encode(ret))
