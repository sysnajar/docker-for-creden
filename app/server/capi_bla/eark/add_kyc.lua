#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false


 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--get non-empty value
function getVal(key)
local ret = body[key]
ret = ret and trim5(ret) or nil
if(ret=='')then ret = nil end
return ret
end

--save cardImg1,2 and 3 to disk
function uploadImgs()
 for _, k in ipairs({'cardImg1', 'cardImg2' , 'cardImg3'}) do

    local v = getVal(k)
 
    if(v) then
        v = v .. '.png'
    end

    body[k] = v
 end
end

--todo cust_code may not == username in the future
function get_cust_code(usr)
return usr
end

--todo cust_code may not == username in the future
function chk_pwd(usr, session_username)
    local map = {credendemo = 'creden@demo'}
    
    --return usr ~= nil and pwd ~= nil and map[usr] == pwd
    
    if usr ~= nil and pwd ~= nil and map[usr] == pwd then
        return true
    end

    local chk = nil
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({adminEmail = usr})   
        if (s and usr == session_username ) then chk = true end
    end
    return chk

end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
	--body  = ngx.req.get_post_args()
    body = cjson.decode(ngx.req.get_body_data())
    ngx.save_log({desc = "add_kyc"})
else
   body   = {} 
end

function chk_kyc()
   --kyc
   local isValidDopa = false
   if body.username == 'credendemo' then
        local res, err = prog('lua', '/home/creden/work/app/server/capi/kyc_demo.lua', cjson.encode(body))

        if(not err)then
            local tmp = cjson.decode(res.stdout)
                if(tmp) then
                    if tostring(tmp.status) == 'true' then
                        body.status = 'pass'
                        body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                    else
                        body.status = 'fail'
                        body.remarks = tmp.message   
                    end             
                isValidDopa = tmp.status
                end 
        end 
   else 
        local params   = 'national_id=' .. body.cardNo ..'&first_name=' .. body.fnameTH .. '&last_name=' .. body.lnameTH .. '&date_of_birth=' ..  body.dob  .. '&cvv_code=' ..body.laserCode   
        local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

        if(not err)then
            local tmp = cjson.decode(res.stdout)
                if(tmp) then
                    if tostring(tmp.status) == 'true' then
                        body.status = 'pass'
                        body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                    else
                        body.status = 'fail'
                        body.remarks = tmp.message:gsub('java.lang.RuntimeException: %[4%]','')   
                    end             
                isValidDopa = tmp.status
                end 
        end
    end
  
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
end

function chk_tx(usr)
    if body.username ~= 'credendemo' then
        ret = {success = false, error_msg = 'Your account has been locked, please contact our support team'}
        local counter_key = 'counter.'..usr..'.tx'
        local max_counter = 'counter.'..usr..'.txMax'
        c = rc:get(counter_key)  
        m = rc:get(max_counter)
        if not r and not m then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            if tonumber(c) >= tonumber(m) then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
            else
            rc:incr(counter_key)      
            end
        end
    end
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")

        local session = require "resty.session".open({secret = "4321"})
		if(chk_pwd(body.username, session.data.username)) then
       chk_tx(body.username)
       chk_kyc()
		   body.compCode = get_cust_code(body.username)
		   body.username = nil
		   body.password = nil
		   body.channel = 'web'
		   body.createDate = os.date("%x") -- http://www.lua.org/pil/22.1.html
		   body.createTime = os.date("%X")
		   body.createdDtm = bson.get_utc_date(ngx.now() * 1000) -- http://www.lua.org/pil/22.1.html
		  
		   --body.createDtm  = os.date("%x")
           body.txid = tostring(rc:incr('kyc.tx.counter'))
		   uploadImgs()
           n, err = col:insert({body})

		   if not n then
              ret = {success = false, error_msg = err}
           else
              ret = {success = true, error_msg = 'data has been saved', txid = body.txid , status = body.status, remarks = body.remarks, compCode = body.compCode}
           end
	   else
		   ret = {success = false, error_msg = 'Invalid Login, please re-login and try again'}
		end

	   	

    end

   print(cjson.encode(ret))
   
