const Nightmare = require('nightmare')
const nightmare = Nightmare({ show: false})
setTimeout(function(){process.exit(0);}, 10000) 
nightmare
  .goto('https://duckduckgo.com/lite/?q='+process.argv[2])
  .wait(2000)
  .evaluate(() => {
    var table = document.getElementsByTagName("table")[2];
    var data = []; // first row needs to be headers
    // go through cells 
    for (var i=0; i<table.rows.length; i++) { 
      var tableRow = table.rows[i]; 
      str = tableRow.textContent.replace(/(\r\n\t|\n|\r\t)/gm,"").trim()
      if(str!=""){
        data.push(str);
      }
      
    } 
    return data;
  })
  //.end()
  .then(function(tb){
    items = []    
    for (var i = 0; i < tb.length; i=i+3) {
      title = tb[i].substring(4,tb[i].length).trim();
      desc = tb[i+1];
      link = tb[i+2]; 
      item = {"title":title,"desc":desc,"link":link}
      items.push(item) 
    }
    console.log(JSON.stringify(items))
    process.exit(1);
  })
  .catch((error) => {
    console.error('Search failed:', error);
  });
