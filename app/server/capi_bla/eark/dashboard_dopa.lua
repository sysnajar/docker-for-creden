#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil


--skip counting
if(false)then
   print(cjson.encode({success = false, error_msg = "N/A"}))
   ngx.exit(ngx.HTTP_OK)
end
--skip counting


 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {compCode = 'rads'} 
end

if(body.compCode=='all') then body.compCode = nil end 


--get credit
credit = 0
if(body.compCode) then
    counter_key = 'counter.'..body.compCode..'.tx'
    max_counter = 'counter.'..body.compCode..'.txMax'
    c = rc:get(counter_key)
    m = rc:get(max_counter)
    credit = (tonumber(m) or 0) - (tonumber(c) or 0)
end

conn = mongo()
conn:set_timeout(3000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
    s = col:find({compCode=body.compCode},{cardImg2=0,cardImg3=0,cardImg1=0})
    wait = 0
    pass = 0
    fail = 0
    total = 0
    for k, v in s:pairs() do
        v._id = tostring(v._id)
        if v.status == 'wait' then wait = wait + 1 end
        if v.status == 'pass' then pass = pass + 1 end
        if v.status == 'fail' then fail = fail + 1 end
    end
    total = wait + pass + fail
    ret = {success = true, error_msg = "complete", data = {wait=wait,pass=pass,fail=fail,total=total}, credit = credit}
end
print(cjson.encode(ret))
