#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end
    local fn =  function(id)
        local key = 'TRAN:DEM:' .. id
        local t = cjson.decode(rc:get(key))
        local score = 0 --total = 600

        local fields = {'ID_sex', 'ID_age', 'ID_name', 'ID_lname',  'ID_dob', 'ID_citino', 'DOPA_death', 'DOPA_name', 'DOPA_lname', 'DOPA_dob'}
        local cnt = 0
        for i, v in ipairs(fields) do
           if(t[v]) then
              cnt = cnt+1 
		   end
		end

        score = (cnt * 100) / #fields

        return score
    end
    
    
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
    
