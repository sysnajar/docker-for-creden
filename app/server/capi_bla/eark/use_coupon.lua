#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
session = require "resty.session".open({secret = "4321"})
prog.timeout_fatal = false
local ret = {success = false}


function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function count(cur)
	local ret = 0
	for i,v in cur:pairs() do
	    ret = ret + 1
	end
	return ret
end

function gen_num( )
    local server_id = '000'
    local inc = tostring(rc:incr('genref_eks'))
    while(#inc<7) do
         inc = '0' .. inc
    end
 
    return server_id .. inc
 end

 function insert_coupon(id, time)

    local data_coupon = {
        createdDtm = time,
        email = body.email,
        company_id = body.company_id,
        ref_id = id,
        code_coupon = "ESIG_COUPON_" .. body.coupon
    }
    c, err = col_coupon:insert({data_coupon})

 end

function insert_package(coupon)

    local current_date = bson.get_utc_date(os.time() * 1000)
	local exp_date = bson.get_utc_date((os.time()*1000) + (1000*60*60*24*7)) 
	local user_com = 0

    if coupon == "FREE_TRIAL" then
        user_com = 3
    elseif coupon == "PROJECT" then
        user_com = 999
    elseif coupon == "PROJECT14" then
        user_com = 10
        exp_date = bson.get_utc_date((os.time()*1000) + (1000*60*60*24*14)) 
    end

	local data_package = {
		user_ekyc = 0,
		plan = "COUPON",
		discount = 0,
		email = body.email,
		total = "0000",
		package = coupon,
		ekyc = {},
		promo_code_use = false,
		status = true,
		ref_id = gen_num(),
		promo_code = "",
		user_add_on = 0,
		active_package = true,
		advance_func = false,
		storage = {},
		type_of_business = "",
		form = {},
		company_id = body.company_id,
		date_buy = current_date,
		price_package = "0000",
		company_name = "",
		lastname = "",
		promo_code_value = "",
		firstname = "",
		need_invoice = false,
		users = user_com ,
		active_expire_date = exp_date,
        -- active_expire_date = current_date,
		active_start_date = current_date,
		date_pay = current_date,
		-- end_active_package = ISODate("2021-04-07T11:54:55Z")
	}

    if coupon == "PROJECT14" then
        data_package.old_coupon = "PROJECT14"
        data_package.package = "PROJECT"
    end

	i, err = col_package:insert({data_package})
    insert_coupon(data_package.ref_id, current_date)

	if err then 
		ret = { success = false, error_msg = "Add package not found", error_code = "" }
    else
        text = "Coupon\nstatus: Sucess payment\nRefno: " .. data_package.ref_id .. "\ncustomeremail: " .. body.email .. "\nCoupon: " .. data_package.package
		prog('sh' ,'/home/creden/work/app/server/capi/line_notion_eks.sh', text )
		ret = { success = true, error_msg = "Add package success", error_code = "2301" }
    end
	print(cjson.encode(ret))
 end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {owner_folder = arg[1]} 
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else
    check_session()
    local coupon = rc:get("ESIG_COUPON_" .. body.coupon)

    if not coupon then
        print(cjson.encode({ success = false, error_msg = "not have coupon", error_code = "2303", coupon = coupon or "N/A" }))
        ngx.exit(ngx.HTTP_OK)
    end


    db = conn:new_db_handle("dopa")
    col_package = db:get_col("package_ss")
    col_coupon = db:get_col("coupon")

    local p, err = col_package:find({ company_id = body.company_id, active_package = true })
    local c, err = col_coupon:find({ company_id = body.company_id })

    local pack = count(p)
    local cou = count(c)
    
    if pack > 0 or cou > 0 then
        print(cjson.encode({ success = false, error_msg = "user have package or you used coupon", error_code = "2302" }))
        ngx.exit(ngx.HTTP_OK)

    else
        insert_package(coupon)
        -- ret = { success = false, error_msg = "use coupon success", error_code = "", count_pack = pack, count_coupon = cou }
        -- print(cjson.encode(ret))
    end
    -- ret = { success = false, error_msg = "use coupon success", error_code = "", count_pack = pack, count_coupon = cou }
    -- print(cjson.encode(ret))
end