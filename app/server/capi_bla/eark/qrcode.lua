#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local bson = require 'resty-mongol.bson'
local exec = require'resty.exec'
local md5 = require 'md5'
local body = nil;
local conn = mongo()
local ret = {success=false}

--connect MongoDB Server
function get_ConnectMongo()
    conn:set_timeout(5000)
    local ok,err = conn:connect('127.0.0.1',27017)
    if (not ok) then
        ret = {success = false, error_msg = 'เกิดข้อผิดพลาด:' .. err}
    end
    return ok;
end

function get_Findtxid()
    ret = {success = false}
    local db = conn:new_db_handle('dopa')
    local col = db:get_col('ekyc')
    local rows = col:find_one({txid=body.txid})
    if rows then
        --ret = {success = true}
        local c = rows.cardNo
        local col2 = db:get_col('cardreader_ghb')
        local rows2 = col2:find({Number_card=rows.cardNo}, {Photo = 0})

        result = rows2:sort({createdDtm=-1})
        result:limit(1)

		
        for index,row in result:pairs() do 
			row._id = tostring(row._id) 
			--row.Photo = nil
            local update_cmd = {['$set'] = {id2=row.id}}
            u, err = col:update({txid=body.txid}, update_cmd, 1, 0, true)

			--insert new ekyc tx
			--[[rows.txid = rows.txid .. 'card'
			rows.cardImg1 = "imgcardghb".. row.id ..".png" 
            rows._id = nil
            rows.vdo = nil
			col:insert({rows}, nil , true)
            ]]	
			--end


            if not err then
                ret = {success = true}
            else
                ret = {success = false, error_msg = err}
            end 
		     -- print(cjson.encode(row))
			break
		end

        --print(tostring(result))

        -- if rows2 then
        --     local c2 = rows2.Number_card
        -- end

        -- if c==c2 then
        --     local update_cmd = {['$set'] = {id2=rows2.id}}
        --     u, err = col:update({txid=body.txid}, update_cmd, 1, 0, true)
        --     if not err then
        --         ret = {success = true}
        --     else
        --         ret = {success = false, error_msg = err}
        --     end 
        -- end

    else
        ret = {success = false, error_msg = 'txid not found'}
    end
    --return ret
end

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {txid ='Glosec1605186726362'}
end

if get_ConnectMongo() then
    get_Findtxid()
end

print(cjson.encode(ret))
