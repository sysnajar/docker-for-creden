#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
-- bson = require "resty-mongol.bson"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { id_doc = arg[1], id_folder = arg[2] }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else    

    db = conn:new_db_handle("edoc")
    col = db:get_col("folders")
    folder = { _id = object_id.new(convertHexStringToNormal(body.id_folder)), owner_folder = body.email }
    f, err = col:find_one(folder)

    if err then
        ret = { success = false, error_msg = "folder not find", error_code = ""}
    else

        update = {["$set"] = { name_folders = body.name_folders, updateDtm = os.date('%d/%m/%Y %H:%M:%S') }}
        local folders, err = col:update({ _id = object_id.new(convertHexStringToNormal(body.id_folder))}, update, 1, 0, true)

        if err then
            ret = { success = false, error_msg = "update folder unsuccess", error_code = ""}
        else
            ret = { success = true, error_msg = "update folder success", error_code = "" }
        end

    end
end
print(cjson.encode(ret))
