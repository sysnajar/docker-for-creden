#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil


if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {bookink_id='123456',email="amorrut@creden.co"}
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("booked_slot")
    find_booking,err = col:find_one({ booking_id = body.booking_id, booking_owner= body.email})

    if find_booking then
        find_booking._id = tostring(find_booking._id)
        ret = { success = true, data = find_booking }
    else
        ret = { success = false, msg = "no data" }
    end
end

print(cjson.encode(ret))