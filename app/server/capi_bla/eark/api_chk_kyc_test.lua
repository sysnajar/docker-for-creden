#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then
    ngx.save_log({desc = "api_chk_kyc"})  
    ngx.req.read_body()
    print = ngx.say
	body  = ngx.req.get_post_args()
    header = ngx.req.get_headers()
else
   body   = {username='creden', password='135790', txid=arg[1]} 
end

body.username = 'credendemo'

if header.apiKey ~= 'QGWikbTLBz1OnVrlOzJCtcLuKJXjTXcz' then
    local ret = {success = false, error_msg = 'Authentication Fail'}
    print(cjson.encode(ret))    
    ngx.exit(ngx.HTTP_OK)
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("ekyc")
    t, err = col:find_one({txid = body.txid, compCode = body.username})
	if not t then
        ret = {success = false, error_msg = err or 'txid not found' , txid = body.txid}
    else 
        ret = {success = true, error_msg = err, txid = body.txid , remarks = t.remarks or '', isValid = t.isValid, isValidDopa = t.isValidDopa, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, status = t.process_status}
    end
end

print(cjson.encode(ret))
   
