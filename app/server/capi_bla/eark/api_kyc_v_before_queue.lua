#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
local HOST_NAME = os.getenv("HOST_NAME")

prog.timeout_fatal = false

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)
 rc2 = redis.connect('172.31.7.255', 6379)
 rc2:auth('noRedisPass1010')

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--get non-empty value
function getVal(key)
local ret = body[key]
ret = ret and trim5(ret) or nil
if(ret=='')then ret = nil end
return ret
end

--save cardImg1,2 and 3 to disk
function uploadImgs()
 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)
 local cnt = tostring(rc:incr('kyc.counter'))
 local dir = '/home/creden/work/app/client/face/images/card/'

 for _, k in ipairs({'cardImg1', 'cardImg2' , 'cardImg3'}) do

 local v = getVal(k)
 
 if(v)then
   local filename = cnt .. k	 
   local tmp = 'tmp.kyc.img' .. filename	 
   rc:set(tmp , v)   
   local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
   os.execute(cmd)

   rc:del(tmp) 
   body[k] = filename .. '.png'
   rc:lpush('q_move_card',filename)
 end

 end
 body.cardImg = body.cardImg1
  
end

function downloadImgs()
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379)
  local cnt = tostring(rc:incr('kyc.counter'))
  local dir = '/home/creden/work/app/client/face/images/card/'
  for i, k in ipairs({'cardImg1', 'cardImg2' , 'cardImg3'}) do
    
    local v = getVal(k)
    if(v)then
      local filename = cnt..k 
      chk = string.sub(v, 1, 4)
      if string.match(chk, "http") then
        local cmd = 'wget '.. v ..' -O ' .. dir .. filename .. '.png'
        os.execute(cmd)
      else
        local tmp = 'tmp.kyc.img' .. filename   
        rc:set(tmp , v)
        local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
        os.execute(cmd)
        rc:del(tmp) 
      end     

      body[k] = filename .. '.png'
      rc:lpush('q_move_card',filename)
    end

  end
  body.cardImg = body.cardImg1
end

--todo cust_code may not == username in the future
function get_cust_code(usr)
return usr
end

--todo cust_code may not == username in the future
function chk_pwd(usr, pwd)
local map = {credendemo = 'creden@demo'}
    
--return usr ~= nil and pwd ~= nil and map[usr] == pwd

    if usr ~= nil and pwd ~= nil and map[usr] == pwd then
        return true
    end

    local chk = nil
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({adminEmail = usr, encryptPass=md5.sumhexa(pwd)})   
        if s then chk = true end
    end
    return chk
end

--todo cust_code may not == username in the future
function chk_apiKey(apiKey)

    local chk = nil
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
         chk = true 
         body.username = s.adminEmail
        end
    end
    return chk
end


if(ngx)then  
    ngx.save_log({desc = "api_kyc"})
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    header = ngx.req.get_headers()
else
   body   = {}
end

function chk_kyc()
   --kyc
   local isValidDopa = false
   if body.username == 'credendemo' then
        local res, err = prog('lua', '/home/creden/work/app/server/capi/kyc_demo.lua', cjson.encode(body))

        if(not err)then
            local tmp = cjson.decode(res.stdout)
                if(tmp) then
                    if tostring(tmp.status) == 'true' then
                        body.status = 'pass'
                        body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                    else
                        body.status = 'fail'
                        body.remarks = tmp.message   
                    end             
                isValidDopa = tmp.status
                end 
        end 
   else 
        local params   = 'national_id=' .. body.cardNo ..'&first_name=' .. body.fnameTH .. '&last_name=' .. body.lnameTH .. '&date_of_birth=' ..  body.dob  .. '&cvv_code=' ..body.laserCode   
        local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

        if(not err)then
            local tmp = cjson.decode(res.stdout)
                if(tmp) then
                    if tostring(tmp.status) == 'true' then
                        body.status = 'pass'
                        body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                    else
                        body.status = 'fail'
                        body.remarks = tmp.message:gsub('java.lang.RuntimeException: %[4%]','')   
                    end             
                isValidDopa = tmp.status
                end 
        end
    end
  
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
end

function chk_amlo()
    local isValidAML = false
    local res, err = prog('lua', '/home/creden/work/app/server/capi/chk_amlo.lua', body.cardNo)
    if not err then
      tmp = cjson.decode(res.stdout)
      isValidAML = tmp.isValid
    end 
    body.isValidAML = isValidAML
end

function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical)
    if body.vdo and body.vdo ~= "" then
        body.isValid = false
        body.isValidLiveness = 'processing'
        body.status = 'processing'
    else
        if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidAML) == 'true' then
            body.isValid = true
        else
            body.isValid = false
        end 
    end
end

function chk_face()
  t = {}
  local faceId1 = ''
  local faceId2 = ''
  if getVal('cardImg1') and getVal('cardImg3') then
    faceId1 = detect_face(body.cardImg1:gsub('.png',''))
    faceId2 = detect_face(body.cardImg3:gsub('.png',''))
    t = verify_face(faceId1, faceId2)
    body.faceConfidence = (t.faceMatchPercent or 0)
    body.isFaceIdentical = (t.isIdentical or false)
  end
end

function detect_face(img)
     local faceId = ''
     local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
     if not err then
        tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
     end
     return faceId
end

function verify_face(faceId1,faceId2)
     t = {}
     local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
     if not err then
        tmp = cjson.decode(res.stdout)
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
     end
     return t
end

function chk_tx(usr)
    if body.username ~= 'credendemo' then
        ret = {success = false, error_msg = 'Your account has been locked, please contact our support team'}
        local counter_key = 'counter.'..usr..'.tx'
        local max_counter = 'counter.'..usr..'.txMax'
        c = rc:get(counter_key)  
        m = rc:get(max_counter)
        if not r and not m then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            if tonumber(c) >= tonumber(m) then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
            else
            rc:incr(counter_key)      
            end
        end
    end
end

function download_vdo()
    local dir = '/home/creden/work/app/client/vdo/ex/'
    local res, err = prog('wget', body.vdo.url, '-O', dir .. body.txid .. '.webm')
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")

		if(chk_apiKey(header.apiKey)) then
       chk_tx(body.username)
		   body.channel = 'api'
		   body.createDate = os.date("%x")
		   body.createTime = os.date("%X")
		   body.createdDtm = bson.get_utc_date(ngx.now() * 1000)
		   body.compCode = get_cust_code(body.username)
           body.txid = tostring(rc:incr('kyc.tx.counter'))
           downloadImgs()

       if get_cust_code(body.username) ~= 'rads' then 
        chk_kyc()
        chk_amlo()
        chk_face()
        chk_is_valid(body.isValidDopa, body.isValidAML, body.isFaceIdentical)
       else 
        body.status = 'wait' 
       end
       body.username = nil
       body.password = nil
       n, err = col:insert({body})

		   if not n then
              ret = {success = false, error_msg = err}
           else
              ret = {success = true, error_msg = err, txid = body.txid , usr = body.username}
           end
	   else
		   ret = {success = false, error_msg = 'Invalid Login'}
        end
        
        --[[
            "vdo":{
                "url":"https://hjkl.ninja/xxx.mp4"
                "missions":[{"no":"1","type":"blink","startTime":"05:30", "endTime":"05:35"}, {"no":"2","type":"voice","startTime":15, "endTime":20}]
            }

            type
                - voice
                - blink
                - FaceLeft
                - FaceRight
        ]]



            if body.vdo.url and string.match(body.vdo.url, "http") then
                download_vdo()
                body.vdo.url = HOST_NAME..'/vdo/ex/'..body.txid..'.webm'
                --TODO1 check audio
                local vdo_req = {vdo=body.vdo, txid=body.txid, compCode=body.compCode}
                -- rc2:publish('api.vdo.req', cjson.encode(vdo_req))
                rc2:rpush('vdo.queue', cjson.encode(vdo_req))
                --TODO2  check audio
                local audio_cmd = "curl -X POST http://159.65.2.13/capi/submit_vdo2 -d '"..cjson.encode(vdo_req).."' &"
                os.execute(audio_cmd)
            end
    end

   print(cjson.encode(ret))
   

