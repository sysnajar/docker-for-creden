#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()    
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("company_members")
    local col_user_com = db:get_col("user_company")
    local col_esig = db:get_col("esig_user")
    local col_group = db:get_col("group_sign")

    i, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(body.id)) })
    u, err_u = col_user_com:find_one({ _id = i.company_id })
    e, err_e = col_esig:find_one({ adminEmail = i.email })

    if i.email == u.create_by then
        print(cjson.encode({success = false, error_msg = "can not remove root company", error_code = "2010"}))
        ngx.exit(ngx.HTTP_OK)
    end
-- ret = { success = true, company_id = e.company_id, com = tostring(i.company_id) }
    if i then

        d, err = col:delete({ _id = object_id.new(convertHexStringToNormal(body.id))})

        if err then
            ret = { success = false, error_msg = "remove user unsuccess", error_code = "2011" }
        else

            if e then
                local key = "invite_company_" .. e.adminEmail
                rc:del(key)
    
                local key2 = "companyof." .. e.adminEmail
                rc:del(key2)
    
                if e.company_id == tostring(i.company_id) then
                    u_com, err_u_com = col_user_com:find_one({ create_by = e.adminEmail })
                    key = { adminEmail = i.email }
                    local update_cmd = { company_id = tostring(u_com._id) }
                    local u, err = col_esig:update(key, {["$set"] = update_cmd}, 0, 0, true)
                end
    
                local g, err_g = col_group:find({ company_id = body.company_id, group_sign = {['$elemMatch']={email = e.adminEmail}} })
                for k, v in g:pairs() do  
                    local group = {}
                    for k2, v2 in ipairs(v.group_sign) do
                        if v2.email ~= e.adminEmail then
                            table.insert(group, v2)
                        end
    
                        g_key = { _id = v._id }
                        g_update_cmd = { group_sign = group }
                        local u_group, err_group = col_group:update(g_key, {["$set"] = g_update_cmd}, 0, 0, true)
                    end
                end
            end



            ret = { success = true, error_msg = "remove user success", error_code = "2012" }
        end

    else
        ret = { success = false, error_msg = "not find company member", error_code = "2007" }
    end

end
print(cjson.encode(ret))