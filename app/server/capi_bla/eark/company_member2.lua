#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {member_id = '1', company_id = '1', position_id = '1', email = 'nutp10.1@gmail.com', created_by = 'nutp10.1@gmail.com', role = 'ADMIN', mode = 'add'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end
-- EARK FN
function rand(len)
local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
local t = {}
local cnt = 0
 
 local i = 1
 repeat
     local f = assert (io.popen (cmd, 'r'))
	 local line = f:read('*all'):match("%d+")
	 f:close()
	 cnt = cnt + #line
	 table.insert(t, line)
 until cnt>len

		
 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
 return ret
end


--u = esig_user
--p = new position
function add_org_node(company_id, u, new_pos)	
	local new_node = {
	  id  = new_pos.node_id,
	  title_id  = new_pos.position_id,
	  title     = new_pos.position_name,
	  person_id = tostring(u._id),
	  name = u.adminFisrtname,
	  img = "xxx",
	  email = u.adminEmail
	 }

	local key = "orgchart." ..company_id
	local json = rc:get(key)
	local ret = nil

	if(json)then
	  ret = cjson.decode(json)
	else
	  ret = {is_empty = false, company_id = company_id, nodes = {}}
	end

	table.insert(ret.nodes, new_node)
	ret.is_empty = false
	ret.success  = true
	ngx.log(ngx.NOTICE, "update org_chart ... " .. cjson.encode(ret))
	rc:set(key, cjson.encode(ret))
end
-- END EARK FN

function random()
    local value = 0
    math.randomseed(os.clock()*100000000000)
    for i=1,5 do
        local r = math.random(10, 99)	      
        value = value .. r
    end
    return value..os.time()
end

function set_key_status(email,company_id,admin_name,company_name)
    local key = 'member.'..email..'.'..company_id
    local value = {status = 'WAITING', admin_name = admin_name, company_name = company_name, key = random()}
    rc:set(key, cjson.encode(value))
end

function chk_exist_member(col_member_list, email)
    local exist_member = col_member_list:find_one({email = email})
        if exist_member then
            print(cjson.encode({success = false, error_msg = "User already exist a company", error_code = '002'}))
            ngx.exit(ngx.HTTP_OK)
        end
end

-- CHECK SESSION
-- if not session.data.company_id then
    -- print(cjson.encode({success = false, error_msg = "no company", error_code = '001'}))
    -- ngx.exit(ngx.HTTP_OK)
-- else
    -- body.company_id = session.data.company_id
    -- body.company_id = "5c80af657695fdd90b9fe28c"
-- end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db_dopa = conn:new_db_handle("dopa")
    local col_esig_user = db_dopa:get_col("esig_user")
    local col_member_list = db_dopa:get_col("company_members")
    local col_user_company = db_dopa:get_col("user_company")
    local s_eisg_user, err_s = col_esig_user:find_one({adminEmail = body.email})
    data.company_id = object_id.new(convertHexStringToNormal(body.company_id))
    local company_detail, err_company_detail = col_user_company:find_one({_id = data.company_id})
    if (body.mode == 'add' or body.mode == 'edit') and body.positions then
        data.positions = body.positions
        for p, q in ipairs(data.positions) do 
            q.position_id = object_id.new(convertHexStringToNormal(q.position_id))
        end
    end
    --user esig
    if s_eisg_user then
        local i=nil
        local err_i=nil
        data.esig_user_id = s_eisg_user._id
        if body.mode == 'add' then
            chk_exist_member(col_member_list, body.email)
            data.created_by = body.created_by
            data.createdDtm = bson.get_utc_date(ngx.now() * 1000)
            data.email = body.email
            data.status = 'WAITING'
            data.role = body.role
            i, err_i = col_member_list:insert({data})
            local add_company, add_company_err = col_esig_user:update({adminEmail=body.email}, {["$set"] = {company_id = body.company_id}}, 0, 0, true)
            -- if(body.new_position) then
            --     add_org_node(body.company_id, s_eisg_user , body.new_position) 
            -- end
            --send mail invite
            cmd = 'node /home/creden/work/app/server/capi/invite_member.js "'..data.email..'" "'..body.created_name..'" "'..company_detail.companyName..'" &'
            os.execute(cmd)
            --send mail invite
            set_key_status(data.email, body.company_id, body.created_name, company_detail.companyName)
        elseif body.mode == 'edit' then
            if data.positions then
                i, err_i = col_member_list:update({_id=object_id.new(convertHexStringToNormal(body.member_id))}, {["$set"] = {positions = data.positions}}, 0, 0, true)
				if(body.new_position) then
			       add_org_node(body.company_id, s_eisg_user , body.new_position) 
				end
            else
                data.role = body.role
                i, err_i = col_member_list:update({_id=object_id.new(convertHexStringToNormal(body.member_id))}, {["$set"] = {role = data.role}}, 0, 0, true)
            end
        elseif body.mode == 'delete' then
            i, err_i = col_member_list:remove({_id=object_id.new(convertHexStringToNormal(body.member_id))})
        elseif body.mode == 'get' then
            i, err_i = col_member_list:find({company_id=object_id.new(convertHexStringToNormal(body.company_id))})
        else
            ret.error_msg = 'Invalid mode'
            print(cjson.encode(ret))
            return 0
        end

        if not err_i then
            ret.success = true
            --get list members
            if body.mode == 'get' then
                local get_table = {}
                --[[
		for k, v in i:pairs() do
		    v._id = tostring(v._id)
                    ngx.log(ngx.NOTICE, "HELLOA#" .. v._id)
                    -- ngx.log(ngx.NOTICE, "update org_chart ... "..cjson.encode(v)..")")

                end
		]]--

                for k, v in i:pairs() do 
                    local pos_t = {}
                    v._id = tostring(v._id)
                    v.company_id = tostring(v.company_id)
                    -- ngx.log(ngx.NOTICE, "HELLOB#" .. v._id)
                    if v.esig_user_id then
                        local get_user, get_user_err = col_esig_user:find_one({_id=object_id.new(convertHexStringToNormal(tostring(v.esig_user_id)))})
                        if get_user then
                            v.email = get_user.adminEmail
                            if get_user.FNAME and get_user.FNAME ~= '' then
                                v.fName = get_user.FNAME
                            else
                                v.fName = get_user.adminFisrtname
                            end
                            v.lName = get_user.LNAME
                            if v.lName then v.name = v.fName..' '..v.lName else v.name = v.fName end
                        end
                        v.esig_user_id = tostring(v.esig_user_id)
                    end
                    for k1, v1 in ipairs(v.positions or {}) do 
                            -- ngx.log(ngx.NOTICE, "update org_chart ... "..tostring(v1.position_id)..")")

                        v1.position_id = tostring(v1.position_id)
                        v1.text = v1.position_name
                        table.insert(pos_t, v1.position_name) 
                    end
                    v.positions_name = pos_t
                    table.insert(get_table, v) 
                end
                ret.data = get_table
            end
            --get list members
        else
            ret.error_msg = err_i
        end
    else
        --no user esig
        if body.mode == 'add' then
            data.created_by = body.created_by
            data.createdDtm = bson.get_utc_date(ngx.now() * 1000)
            data.email = body.email
            data.status = 'UNAPPROVE'
            data.role = body.role
            local add_user, add_user_err = col_member_list:insert({data})
            if not add_user_err then
                --send mail invite
                cmd = 'node /home/creden/work/app/server/capi/invite_member.js "'..data.email..'" "'..body.created_name..'" "'..company_detail.companyName..'" &'
                os.execute(cmd)
                --send mail invite
                set_key_status(data.email, body.company_id, body.created_name, company_detail.companyName)
                ret.success = true
                ret.new_user = true
            else
                ret.success = false
                ret.error_msg = add_user_err
            end
        end
        --no user esig
    end
end
--
-- for i,v  in ipairs(ret.data) do
-- ngx.log(ngx.NOTICE, "GOT " .. i .. v.email ..' of ' .. #ret.data .. ' ' .. tostring(v._id))
-- ngx.log(ngx.NOTICE, cjson.encode(v)  )

-- --v.positions = {""}
--  --print(v.positions) 
--  --print(cjson.encode(v))
-- end
--
--print('eark')
print(cjson.encode(ret))
