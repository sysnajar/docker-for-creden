local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {token = args[1],otp = args[2]} 
end

token = body.token
otp = body.otp
key = 'otp_'..token..'_'..otp
check = rc:get(key)

if check then
    rc:del(key)
    ret = {success = true, error_msg = 'success otp'}
else
    ret = {success = false, error_msg = 'none otp'}
end
print(cjson.encode(ret))