#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
exec = require'resty.exec'
redis = require 'redis'
rc = redis.connect('127.0.0.1')
fun = require 'fun'


local Save = true
local Tmp = {}

local total_scores  = {DEM = 600,  FIN = 600, CRIM = 100, BUS = 200} --CARE = 300, SOC = 100  INT = 900
local all_keys = fun.foldl(function(acc, x) table.insert(acc, x) return acc end, {}, total_scores)

local fn = {}

for _i, v in ipairs(all_keys) do
   fn[v] = require ('COMP_'..v)
   print('Loaded COMP',v)
end


function set_score(k, v, id)
local key = id .. ':COMP:' .. k 

if(Save)then
   rc:set(key, v)
else
   Tmp[key] =  v
end

--print('saved', key, v)

end

function get_score(k, id)
local key = id .. ':COMP:' .. k 
local ret = nil

if(Save) then
   ret = tonumber(rc:get(key) or 0)
else
   ret = Tmp[key] or 0
end

return ret
end

function compute(id, keys)
keys = keys or {}
print('\ncomputing score', id)

-- 1. compute chosen scores 
for _, v in ipairs(keys) do  	
	print('score type',v)
   local score = fn[v](id)
   local old = get_score(v, id)
   print(v , old , '(OLD)' , '>>>' , score , '(NEW)' )   
   set_score(v, score, id)
end


print('')   

-- 2. combined all scores
local total = 0
for _, v in ipairs(all_keys) do
	local score = get_score(v, id)	
    total = total + score

    print(v , '=' , score )   
end

print('total', total)

end


--------------
compute('sysnajar@gmail.com', {'CRIM', 'FIN', 'DEM'})
