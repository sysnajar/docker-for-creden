return {

		company = {
			name = "กรมพัฒนาที่ดิน"
		}, 

	    err = function(text)
          print(cjson.encode({success = false, error_msg = text}))
          ngx.exit(ngx.HTTP_OK)
		end,

		rand  = function(len)
		local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
		local t = {}
		local cnt = 0
		 
		 local i = 1
		 repeat
			 local f = assert (io.popen (cmd, 'r'))
			 local line = f:read('*all'):match("%d+")
			 f:close()
			 cnt = cnt + #line
			 table.insert(t, line)
		 until cnt>len

				
		 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
		 return ret
		end,
   
		search_collections = function (db,collection_name,filter)
		local mongo = (mongo) and mongo or require "resty-mongol"
        local conn = mongo()
			conn:set_timeout(1000)
			ok, err = conn:connect('127.0.0.1',27017)
			if not ok then
			    return false,nil
			else
			    db = conn:new_db_handle(db)
			    col = db:get_col(collection_name)
			    
				list = col:find(filter)    
				--list = col:find({})    

			    if list then 
			        data = {}

			        for _i, v in list:pairs() do
			            v._id = tostring(v._id)
			            table.insert(data, v)
			        end

			         return true ,data
			    else
			         return false,nil
			    end
			end  
		end,

		test_init_ngx = function()
			if(ngx)then  
			    ngx.req.read_body()
			    print = ngx.say
				body  =  ngx.req.get_post_args()
			else
			    if(no_ngx)then no_ngx() end    
			end
        end,
        
        activity_history = function(docID ,owner, actionEN, actionTH, activityEN, activityTH, status, fileName, subject, msg, toolName, toolData)
            local mongo = (mongo) and mongo or require "resty-mongol"
            local bson = (bson) and bson or require "resty-mongol.bson"
            local conn = mongo()
            conn:set_timeout(1000)
            local ok, err = conn:connect('127.0.0.1',27017)
            if ok then
                local createDate = os.date("%x") 
                local createTime = os.date("%X")
                local createdDtm = bson.get_utc_date(ngx.now() * 1000) 
                local log = { docID = docID, owner = owner, fileName = fileName, subject = subject, msg = msg, actionEN = action, actionTH = actionTH, activityEN = activityEN, activityTH = activityTH, toolName = toolName, toolData = toolData, status = status, createDate = createDate, createTime = createTime, createdDtm = createdDtm }
			    local db = conn:new_db_handle('edoc')
                local col = db:get_col('activity_history')
                local i, err = col:insert({log}, nil, true)
            end
        end ,
        
		add_all = function (t1 , t2) --add all items in t2 to t1
        for _,v in ipairs(t2) do
           table.insert(t1, v)
		end
		return t1
		end,

map = function (fn , t)
local ret = {}
for _, v in ipairs(t) do
   table.insert(ret, fn(v))
end
return ret
end,

shallow_copy = function (src, _dest)
local ret = _dest or {}
for k, v in pairs(src) do
	ret[k] = v
end
return ret
end,

by_id = function (col, _id)
  if( type(_id)=="string" ) then
     _id = object_id.new(common.convertHexStringToNormal(_id))
  end

  local res = col:find_one({_id = _id})
  return res
end,

mongo_ctx = function()
    local ret = {}
	local conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
	--	assert(false)
    else
		ret.dopa = conn:new_db_handle("dopa")
		ret.edoc = conn:new_db_handle("edoc")
		local col = {}

		-- if k = xxx.yyy then col_name = yyy 
		-- if k = xxx     then col_name= xxx
		local mt = {__index = function(t, k)  
			local db = ret.edoc
			if(string.find(k, 'dopa.')==1) then db = ret.dopa end
			if(string.find(k, 'smartcity.')==1) then db = conn:new_db_handle("smartcity") end

			local i = string.find(k, '%.')
			local col_name = (i==nil) and k or (k:sub(i+1))
			return db:get_col(col_name)
		    end
		    }
	     
		 setmetatable(col, mt)
		 ret.col = col
		 ret.by_id = function(col_name, _id)	 
		 return common.by_id( ret.col[col_name], _id )
		 end
     end

    return ret		 
end,

convertHexStringToNormal = function( str )
ngx.log(ngx.NOTICE,"convertHexStringToNormal#" .. tostring(str) .. 'type#' .. type(str))
	
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end,

object_id = function( _id )
	if( type(_id)=="string" ) then
		_id = object_id.new(common.convertHexStringToNormal(_id))
	end
	return _id
end,

		edoc_get_signers_by_position = function(_id) 
           ngx.log(ngx.INFO,'ldd1 pos_id = '.. tostring(_id) )

           local  people =  common.edoc_get_persons_by_position(_id)

           ngx.log(ngx.INFO,'ldd1 people len = '.. #people )
		   
		   return common.map(function(u) 
			   local signer = {
			   name  = u.adminFisrtname or '',
			   email = u.adminEmail or '',
			   position_id = tostring(_id)
		      }
			  return signer
		   end, people)
		end,

		edoc_get_positions_id_by_email = function(email) 
		assert(email, 'Email can not be nil!!!')	
		local ret   = {}
		local ctx   = ctx or common.mongo_ctx()
        local members = ctx.col['dopa.company_members']:find({email=email})

	   for _, m in members:pairs() do
		    for _, _pos in ipairs(m.positions or {}) do
                table.insert(ret, tostring(_pos.position_id))     
			end --for pos
       end

		return ret
		end,

		edoc_get_persons_by_position = function(_id) 
		if(not _id or (_id==''))then return {} end	
		local ret   = {}
		local ctx   = ctx or common.mongo_ctx()

		if( type(_id)=="string" ) then
            ngx.log(ngx.NOTICE,"ldd1 edoc_get_persons_by_position#" .. tostring(_id))
			
		    _id = object_id.new(common.convertHexStringToNormal(_id))
		end
        
        local query = {positions={['$elemMatch']={position_id=_id}}}
        --local query = {}
        local members = ctx.col['dopa.company_members']:find(query)

       ngx.log(ngx.NOTICE,"ldd1 listing members")
	   local _cnt=0
	   for _, m in members:pairs() do
		    _cnt = _cnt+1 
            local u = ctx.by_id('dopa.esig_user', m.esig_user_id)
			if(u) then  table.insert(ret, u)  end
       end
       ngx.log(ngx.NOTICE,"ldd1 found ".. _cnt  .." members")

		return ret
		end,

		edoc_get_real_signers = function(doc) --returns actual signers even if some of the signer are specified using position_id
            ngx.log(ngx.NOTICE,"edoc_get_real_signers company_id#" .. tostring(doc.company_id))

			local ret = nil
			if(not doc.company_id) then 
			   return doc.signers
		   else
			   ret = {}
			   for _i, v in ipairs(doc.signers) do
                   if(v.email and v.email~="") then 
					   table.insert(ret, v) 
				   else
					   local all_persons = common.edoc_get_persons_by_position(v.position_id)
            		   ngx.log(ngx.NOTICE,"find ".. #all_persons .." persons for position_id#" .. tostring(v.position_id) .."(" .. tostring(v.title)  ..")")

					   ret = common.add_all(ret, common.map(function(u) 
					   local signer = common.shallow_copy(v) 
					   signer.name  = u.adminFisrtname or ''
					   signer.email = u.adminEmail or ''
            		   ngx.log(ngx.NOTICE," - " .. signer.name ..' (' .. signer.email ..')')
					   return signer
					   end, all_persons))


				   end
			   end --for
			end

			return ret
        end,
}
