#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local common = require "common"

local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
res = {success = false, conf = {}}

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
    apiKey = ngx.req.get_headers().apiKey
else
   body   = {code = 'esig'} 
end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    ok,  err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end

common.validate_alphanumeric(body.code , "Invalid Param")

if connect_mongo().success == true then
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("company")
    local col1 = db:get_col("sdk_code")
    -- local s_normal = col1:find_one({code = body.code, status = 'NORMAL'}) 
    -- local s_edit = col1:find_one({code = body.code, status = 'EDIT'}) 
    -- local s1 = (s_normal) and s_normal or s_edit
    local s1 = col1:find_one({code = body.code})
    if s1 then
        local company = col:find_one({adminEmail = s1.username})  
            if company then 
                if s1.status == 'NORMAL' or s1.status == 'EDIT' then
                    u, err_u = col1:update({code = body.code},{["$set"] = {status = 'DONE'}}, 0, 0, true)
                    res.success = true
                else
                    res.success = false
                end
                if s1.redirect_url and s1.redirect_url ~= '' then company.conf.redirect_url = s1.redirect_url end
                res.conf = company.conf
                res.usr = s1.username
                res.txid = s1.txid
                res.status = s1.status
            else
                res.error_msg = 'Authentication Fail'
            end
    else
        res.error_msg = 'Invalid CODE'
    end
end 

print(cjson.encode(res))
   
