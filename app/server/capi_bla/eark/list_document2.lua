#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end


function match_pos(doc, pos_id) 

  for i,signer in ipairs(doc.signers or {}) do
      if(signer.position_id and signed.position_id==pos_id)then
        return true
      end
  end

  return false
end

function match_remove(signers, owner, rm)
  local status = false

  for i, v in ipairs(signers) do
    if v.is_group == true then
      
      for i2, v2 in ipairs(v.user_group) do
        if v2.is_remove == true and v2.email == body.email then
          status = true
          break
        end
      end

    else

      if v.is_remove == true and v.email == body.email then
        status = true
        break
      end
    end

  end

  if owner == body.email and rm == true then
    status = true
  end

  return status
end

function chk_signer(signers)
  local user = false
  for i, v in ipairs(signers) do
    if v.is_group == true then
      for i2, v2 in ipairs(v.user_group) do
        if v2.email == body.email then
          user = v2
        end
      end
    else
      if v.email == body.email then
        user = v
      end
    end
  end
  return user
end

function timeout_show_trash(arr)
  local current_time = bson.get_utc_date(ngx.now() * 1000).v
  local exp_time = ""
  local user = chk_signer(arr.signers)
  local show = true

  if user and user.is_remove == true then
    exp_time = user.time_remove + (24*60*60*1000)
    if current_time > exp_time then
      show = false
    end

  elseif arr.owner_is_remove == true then
    exp_time = arr.owner_time_remove + (24*60*60*1000)
    if current_time > exp_time then
      show = false
    end
  end
  return show
end

function match_myorder(signers) 
  local order = 0
  for i, v in ipairs(signers) do
    if v.is_group == true then
      for i2, v2 in ipairs(v.user_group) do
        if v2.email == body.email then
          order = v.order_set
          break
        end
      end
    else
      if v.email == body.email then
        order = v.order_set
        break
      end
    end
  end
  return tostring(order)
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else

	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list")

  if(not body.email or body.email=='' or type(body.email) == 'userdata')then  
       print(cjson.encode({success = false, error_msg = "session expired"}))
	    ngx.exit(ngx.HTTP_OK)
  end	

	if(not session.data.username) then
		print(cjson.encode({success = false, error_msg = "session expired"}))
	  ngx.exit(ngx.HTTP_OK)
	else
		if(body.email~=session.data.username) then
			ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
			body.email = session.data.username
		end
	end	

  -- local deleted_docs = db:get_col('deleted_doc'):find({email = body.email})
  -- local deleted_ids = {}
	-- for k, v in deleted_docs:pairs() do
  --   table.insert(deleted_ids, tostring(v.id))
  -- end

  s = {}  
  query, query2, query3 = {}, nil

  if body.cat == "owner" then
    query = { owner = body.email, is_template={['$ne']=true} }

  elseif body.cat == "self" then
    query = { owner = body.email, signers={['$elemMatch'] = {email = body.email}}, is_template={['$ne']=true} }
  

  elseif body.cat=="template" then
    query = {owner=body.email, is_template=true}

  
  elseif body.cat=="cancel" then
    query = {owner=body.email, status = 3 }

  elseif(body.cat == "public") then
    query = { owner = body.email, is_template={['$ne']=true}, setting_pdf={['$elemMatch']={ tag_folder = "Public" }}  }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email }}, setting_pdf={['$elemMatch']={ tag_folder = "Public" }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, setting_pdf={['$elemMatch']={ tag_folder = "Public" }} }

  elseif(body.cat == "private") then
    query = { owner = body.email, is_template={['$ne']=true}, setting_pdf={['$elemMatch']={ tag_folder = "Private" }} }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email }}, setting_pdf={['$elemMatch']={ tag_folder = "Private" }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, setting_pdf={['$elemMatch']={ tag_folder = "Private" }} }

  elseif(body.cat == "confidential") then
    query = { owner = body.email, is_template={['$ne']=true}, setting_pdf={['$elemMatch']={ tag_folder = "Confidential" }} }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email }}, setting_pdf={['$elemMatch']={ tag_folder = "Confidential" }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, setting_pdf={['$elemMatch']={ tag_folder = "Confidential" }} }

  elseif(body.cat == "trash") then
    -- query = {id = {['$in']  = deleted_ids}}
    query = { owner = body.email, is_template={['$ne']=true}, owner_is_remove = true }
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ email = body.email, is_remove = true }}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email, is_remove = true }}}} }
  end  


  if body.cat == "all" or body.cat == "signer" then
    query = { owner = body.email, is_template={['$ne']=true}}
    query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={email = body.email}}  }
    query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}} }
  end

  s  = col:find(query)
	s2 = {} 
	for k, v in s:pairs() do 

    if body.cat == "self" then
      if #v.signers == 1 and v.signers[1].email == body.email and not match_remove(v.signers, v.owner, v.is_remove) then
        table.insert(s2, v) 
      end

    elseif body.cat == "trash" then 
      table.insert(s2, v) 
    else
      if not match_remove(v.signers, v.owner, v.is_remove) then
        table.insert(s2, v) 
      end
    end
    -- table.insert(s2, v) 
  end

  if (true and query2) then
    q2 = col:find(query2)
    for k2, v2 in q2:pairs() do 

      if body.cat == "trash" then
        table.insert(s2, v2) 
      else
        if not match_remove(v2.signers, v2.owner, v2.is_remove) then
          table.insert(s2, v2) 
        end
      end
      -- table.insert(s2, v2) 
    end
  end

  if (true and query3) then
    q3 = col:find(query3)
    for k3, v3 in q3:pairs() do 

      if body.cat == "trash" then
        table.insert(s2, v3) 
      else
        if not match_remove(v3.signers, v3.owner, v3.is_remove) then
          table.insert(s2, v3) 
        end
      end
      -- table.insert(s2, v3) 
    end
  end

	tmp = {}
	for k, v in ipairs(s2) do
    v._id = tostring(v._id)
    -- v._company = tostring(v._company)
    if v.createdDtm then
      v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
    end
      -- v.imageFiles = nil
      v.imageMerge = nil
      table.insert(tmp,v)
  end

  local tmp2 = {}
  local myorder = 0
  for i, v in ipairs(tmp) do

    local exp = timeout_show_trash(v)
    if exp == true then
      if v.use_signing_order == true and v.status == 0 then

        myorder = match_myorder(v.signers)
        if myorder == "" then 
          myorder = "0" 
        end
        
        if tonumber(v.current_order) >= tonumber(myorder) then
          table.insert(tmp2, v)
        end
      else
        table.insert(tmp2, v)
      end
    end
  end

  ret = { success = true, error_msg = "complete", data = tmp2, cat = body.cat, query = query }
  print(cjson.encode(ret))
end
