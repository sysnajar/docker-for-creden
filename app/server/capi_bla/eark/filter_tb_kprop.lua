#!/usr/bin/env lua

redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

k = arg[1]
s = rc:get(k)
h_tb = [[ <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody><tr>
              <td>
        <table width="100%" border="0" cellpadding="0" cellspacing="0" class="output1" id="tblSecurities">
              
          
                  <tbody><tr>
                    <th width="93" valign="top">สถาบันการเงิน</th>
                    <th width="50" valign="top">ชื่อหุ้น<br><font size="3">(ย่อ)</font></th>
                    <th width="83" valign="top"><div style="text-align:center; float:right;">จำนวนทั้งหมด</div></th>
                    <th width="63" valign="top"><div style="text-align:center; float:right;">เงินต้นทุน<br><font size="3">(บาท)</font></div></th>
                    <th width="73" valign="top"><div style="text-align:center; float:right;">ราคาต้นทุน<br><font size="3">(บาท)</font></div></th>
                    <th width="65" valign="top" style="text-align:center;">ราคาล่าสุด<br><font size="3">(บาท)</font></th>
                    <th width="112" valign="top" style="text-align: center">มูลค่าทรัพย์สินตาม<br>ราคาล่าสุด<font size="3">(บาท)</font></th>
                    <th width="170" valign="top" colspan="2" style="text-align: center">กำไร/ขาดทุนตามราคาล่าสุด<br><font size="3">(บาท)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;(%)</font></th>             
                    <th width="56" valign="top">อัตราส่วน</th>
                    <th width="56" valign="top" style="text-align:center;">แก้ไข/ลบ</th>
                  </tr>
                ]]

f_tb = [[</tbody></table>]]        

local t = {}                   
local i = 0
html = ''
account = 0
for word in string.gmatch(s, "<tr><td>บล.กสิกรไทย จำกัด.+</td></tr>") do
  str = string.gsub(word,",", "KIT")
	str = string.gsub(str,"</tr>", ",")
	for x in string.gmatch(str, '([^,]+)') do
		if string.find(x,'บล.กสิกรไทย จำกัด') then
			res = string.gsub(x,"KIT", ",")
			html = html .. res..'</tr>'
			account = account + 1
		end	    
	end
end
html = 'total : '..account..' จำนวนหุ้น <br> '..h_tb..html..f_tb
--print(html)
rc:set(k,html)