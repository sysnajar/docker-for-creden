#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local line_esign = require "line_esign_noti"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local HOST_NAME = os.getenv("HOST_NAME")
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

local function send_line_noti(type,doc_id,email)
	local res_noti, user_id = line_esign.get_line_user_id(email)
	if res_noti then
        local params =  {url= "",id = doc_id}
		local document_link = HOST_NAME.. "/esign/#/document?id=" .. doc_id
		params.url = document_link
		local message = line_esign.message(type, params)
		line_esign.push(user_id, message)
	end
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_comment")

    if session.data.username then 
        local add_comment = body
        add_comment.sent_time = (os.time()*1000)
        u, err = col:insert({add_comment})
        if u then
            ret = { success = true, msg = "success",data = add_comment}
            if #body.tag > 0 then
                for n,u in ipairs(body.tag) do 
                    cmd = 'node /home/creden/work/app/server/capi/mail_chat.js "'..body.doc_id..'" "'..u.email_tag..'"'
	                os.execute(cmd)
                    send_line_noti('chat', body.doc_id,u.email_tag)
                end
            end
        else
            ret = { success = false, msg = "not success",data = add_comment}
        end
    else
        ret = {success = false, error_msg = err, msg = "no session" }
    end

end

print(cjson.encode(ret))