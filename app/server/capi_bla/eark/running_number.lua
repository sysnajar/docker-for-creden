#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
    body   = {document_type_id = arg[1], document_id = arg[2]}
--    body   = {document_type_id = '5c3dd040778fdc6a5f91c682', document_id = '1547608805'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    print(body.document_type_id)
    local db_dopa = conn:new_db_handle("dopa")
    local db_edoc = conn:new_db_handle("edoc")
    local col_edoc_list = db_edoc:get_col("edoc_list")
    local col_doc_type = db_dopa:get_col("document_type")
    local doc_type, err_doc_type = col_doc_type:find_one({_id=object_id.new(convertHexStringToNormal(body.document_type_id))})
    if doc_type then
        doc_type.current_number = (tostring(doc_type.current_number) + 1) or 0
        local i, err_i = col_doc_type:update({_id=object_id.new(convertHexStringToNormal(body.document_type_id))}, {["$set"] = {current_number = doc_type.current_number}}, 0, 0, true)

        local document_number = doc_type.prefix..'.'..doc_type.current_number..'/'..(tonumber(os.date("%Y")) + 543)
        print(document_number)
        local j, err_j = col_edoc_list:update({id=body.document_id}, {["$set"] = {document_number = document_number}}, 0, 0, true)
    end
end