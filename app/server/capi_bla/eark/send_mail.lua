#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local body = nil
HOST_NAME = os.getenv("HOST_NAME")

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {} 
end


conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = {}
    col = db:get_col("user_email") 
    s = col:find_one({key = body.fb_id, source = 'facebook'})  
    if s then 
        local link = HOST_NAME..'/signature/verify_email.html?email='..body.email..'&reset_key='..s.reset_key..'&fb_id='..s.key..'&state=2'
        cmd = 'node /home/creden/work/app/server/capi/register_mail.js "'..body.email..'" "'..body.fname..'" "'..link..'" &'
        os.execute(cmd)
        ret = {success = true}
    else
        ret = {success = false, error_msg = "Invalid ID"}
    end
end

print(cjson.encode(ret))


