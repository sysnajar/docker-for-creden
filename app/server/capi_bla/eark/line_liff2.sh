curl -v -X POST https://api.line.me/v2/bot/message/push \
-H 'Content-Type:application/json' \
-H 'Authorization: Bearer indARxinViDCBMjmEtyNOtlCBM1WWotI4MtHM3mC8Xqpaz1C7yO+piU5GlUhEbvtVqFZ6bI90lASeh+XpCVDurLrnCxjkQL0apDdS47qG6zYpiXxa0LOGiSJ7mt9fAb00uLzdGQKZn2vPsIhWcgPmAdB04t89/1O/w1cDnyilFU=' \
-d '{
 "to": "'$1'",
 "messages": [
   {
        "type": "template",
        "altText": "this is a buttons template",
        "template": {
          "type": "buttons",
          "actions": [
            {
              "type": "uri",
              "label": "เข้าเว็บไซต์",
              "uri": "line://app/'$LIFF_ID'?line_id='$1'"
            }
          ],
          "thumbnailImageUrl": "https://creden.co/images/Creden_logos_Logo_C.png",
          "title": "creden.co",
          "text": "ส่งและเซ็นเอกสารออนไลน์ได้ทันที ใช้งานได้ทุกที่ ทุกเวลา ทุกอุปกรณ์"
        }
      }
 ]
}'
