#!/usr/bin/env lua

local body = nil
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local redis = require 'redis'
local object_id = require "resty-mongol.object_id"

local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local isValidLED = false
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local led_desc

local max_update   = 9000
local update_count = 0

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)

else
    body = {capcha='1103700530312',ref_no='พัทธพงศ์'}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end


function chk_amlo(card_no,fnameTH,lnameTH)
    local isValidAML = false
    local ret_amlo = {}
    local _card_no = card_no
    local _fnameTH = fnameTH
    local _lnameTH = lnameTH
    local res, err = prog('lua', '/home/creden/work/app/server/capi/chk_amlo2.lua', _card_no, _fnameTH, _lnameTH)
    if not err then
      tmp = cjson.decode(res.stdout)
      ret_amlo.isValidAML = tmp.isValidAmlo
      ret_amlo.amlo_desc = tmp.amlo_desc
      ret_amlo.amlo_code = tmp.amlo_code
    else
    ret_amlo.isValidAML = false 
    ret_amlo.amlo_desc = false
    ret_amlo.amlo_code = false
    end 
    -- body.isValidAML = isValidAML
    return cjson.encode(ret_amlo)
end



    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    tmp = {}
    data = {}
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("ekyc_zipmex_col")
        --local s = col:find({ ['$ne'] = { status = 'Complete'}  })
        --local s = col:find( {processed_amlo = { ['$ne'] = true }} )
        local s = col:find( {isValidAML = { ['$exists'] = false }} )
        local count = 0
        local status = false
        local createdDtm2
        -- local isValidLED = false
        local x = 0
        local tmp_csv = {"rererence no","id","isValidDopa","dopaRemarks" ,"isValidFace" , "facePct","isValidLED","ledDesc"}
        for k, v in s:pairs() do 
            count = count + 1
	        
	        if(v.cardNo ~= '1103700530312') then  --check dummy ID card
            
                -- isValidLED,led_desc,data_full_LED = chk_led(tostring(v._id),v.cardNo)


                local chk_amlo = chk_amlo(v.cardNo,v.fnameTH,v.lnameTH)
                chk_amlo = cjson.decode(chk_amlo)
                createdDtm2 = bson.get_utc_date(os.time() * 1000)
                -- check max update
                update_count = update_count+1
                -- print('updated' , update_count .. '/' .. max_update, v._id)
                print(chk_amlo.amlo_code, chk_amlo.amlo_desc,  chk_amlo.isValidAML)

		--update data
                local update  = {["$set"] = {amlo_code = chk_amlo.amlo_code,
		amlo_desc     = chk_amlo.amlo_desc,
		isValidAML    = chk_amlo.isValidAML,
		createdDtmAML = createdDtm2
	       }}

                u, err_u = col:update({_id=object_id.new(convertHexStringToNormal(tostring(v._id)))},update, 0, 0, true)
                if not u then
                    ret.error_msg = err_u
                else
                    
                    ret.msg2 = 'update success'
                    ret.success = true
                    print(cjson.encode(ret))
                end


                -- --local query = { _id = object_id.new(convertHexStringToNormal(session.data.company_id))}
                -- u, err_u = col:update({_id=object_id.new(convertHexStringToNormal(tostring(v._id)))},update, 0, 0, true)
                -- if not u then
                --     ret.error_msg = err_u
                -- else
                    
                --     ret.msg2 = 'update success'
                --     ret.success = true
                --     print(cjson.encode(ret))
                -- end

            if(max_update ~= -1 and update_count >= max_update)then
                print('exit code A')
                break; --eark test
            end
		

	        else
		    print('skipped LED dummy ID card ['.. v.cardNo..']')   

            end -- 1103700530312

          
        end
   
	print('exit code B')
       

            
            -- if(count > 0 and count < 11) then
            -- print(k,tostring(v._id))
            -- end
            --print(count)
            
            -- if(v.ref_no ~= nil and v.ref_no ~= "555555" and v.data_LED ~= nil) then
            --     if(count > 1 and count < 560) then
            --         x = x +1
            --     if(v.remark == "สถานะปกติ (ข้อมูลถูกต้อง)") then
            --         status = true
            --     end
            --     if(v.data_LED ~= nil) then
            --         isValidLED = true
            --     end
            --     local dd = os.date("%d/%m/%Y",tonumber(v.createdDtm_led)/1000)
            --    -- print(v.data_LED)
            --      local s = tostring(x) .. "," .. v.ref_no .. "," .. v.cardNo .. "," ..tostring(status).. "," ..tostring(v.remark)..","..tostring(v.isIdentical)..","..tostring(v.faceMatchPercent)..","..tostring(isValidLED)..","..cjson.encode(v.data_LED[1].title)..","..tostring(dd)
            --     -- v._id = tostring(v._id)
           
          
            --      print(s..'\n')
            --     end
            -- end


    

--end


