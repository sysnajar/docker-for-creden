#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {}
prog.timeout = 1000 * 300
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    header = ngx.req.get_headers()
else
    body = {SerialNumber='12345789SSSQPA',ReaderName='SCR0001',IPAddress='192.168.1.1'}
end

function InsertDipChip()
    local conn = mongo()
    conn:set_timeout(10000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if (not ok) then
        return
    end

    local data = {}
    local db = conn:new_db_handle("db_pea")
    local col = db:get_col("log_dipchip")
    data.SerialNumber = body.SerialNumber
    data.ReaderName = body.ReaderName
    data.IPAddress = body.IPAddress
    data.InsertTime = bson.get_utc_date(os.time() * 1000)
    local i, err_i = col:insert({data})
    if (i) then
        ret = {success = true}
    else
        ret = {success = false, msg = err_i}
    end
end

InsertDipChip()
print(cjson.encode(ret))
