#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
prog.timeout = 1000 * 300
prog.timeout_fatal = false
local ret = {success = false}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say

    body = ngx.req.get_body_data()
    -- ngx.log(ngx.NOTICE,'ret body',body)

    body = cjson.decode(body)
    if body == nil then 
        ret.error_msg = 'Invalid Data'
        print(cjson.encode(ret))
        return 0
    end
    header = ngx.req.get_headers()
else
   --body   = {fname="พัทธพงศ์"}
    body = {fname="พัทธพงศ์",lname="ประเทืองโอฬาร",cardNo="1103700530312",cvv_code="JT0070481499",dob="26/07/2534"}
end

function connect_mongo()
    conn = mongo()
    conn:set_timeout(1000)
    ok,  err = conn:connect(rc:get('amway.db'),27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true} 
    end
    return ret
end
function ocr(cardImg)
    -- body
    local res, err = prog('lua', '/home/creden/work/app/server/capi/ocr_demoOO.lua', cardImg,'12345')
    ngx.log(ngx.NOTICE,"======>ORC==="..cardImg.."====="..res.stdout)
    if(not err) then
    ret.success = true
    ret.data = cjson.decode(res.stdout)
    else
    ret.success = false
    ret.data = cjson.decode(res.stdout)
    end


end
function decode_loadBase64(photo1)
    -- body
    if(photo1 ~= nil)then
   local dir = '/home/creden/work/app/client/face/images/card/'
   local tmp = 'tmp.zipmex.imgcard'..body.id   
   rc:set(tmp , photo1)
   local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. 'imgcard_zip' .. body.id .. '.png'
   os.execute(cmd)
   rc:del(tmp)
   local photo1F='imgcard_zip'..body.id
   ocr(photo1F)
    else
        ret.success = false
        ret.msg = 'data photo1 not found'
        ret.data = {}
    end
   -- rc:del(tmp)
   -- print('imgcard'..body.id)





end
function chk_tx(usr)
        local ret = {success = false, error_msg = 'Your credit is not enough'}
        local counter_key = 'counter.'..usr..'.tx'
        local max_counter = 'counter.'..usr..'.txMax'
        c = rc:get(counter_key)  
        m = rc:get(max_counter)
        if not c and not m then
            print(cjson.encode(ret))    
            ngx.exit(ngx.HTTP_OK)
        else
            if tonumber(c) >= tonumber(m) then
                print(cjson.encode(ret))    
                ngx.exit(ngx.HTTP_OK)
            else
                rc:incr(counter_key)      
            end
        end
end
function chk_apiKey(apiKey)
    local chk = nil
    if connect_mongo().success == true then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
            s._id = nil
            chk = true          
        end
    end
    return chk
end

if(chk_apiKey(header.apiKey)) then
   chk_tx(body.username)
   decode_loadBase64(body.photo1)
else
    ret.success = false 
    ret.error_msg = 'Authentication Fail'
    ret = {success=false,ret.error_msg}

end

print(cjson.encode(ret))