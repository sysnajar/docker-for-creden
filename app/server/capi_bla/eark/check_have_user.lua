#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    u, err = col:find_one({ adminEmail = body.email })

    if u then
        local type_login = "0"
        if u.hashedPassword then
            type_login = "is_creden"
        elseif u.is_fb then
            type_login = "is_fb"
        elseif u.is_gamil then
            type_login = "is_gamil"
        end
        ret = { success = true, error_msg = "have user", error_code = "", type_login = type_login }
    else
        ret = { success = false, error_msg = "not have user", error_code = "" }
    end
end
print(cjson.encode(ret))
