#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {key = arg[1]} 
end

local all_accounts = cjson.decode(rc:get('demo@creden.co.scb'))

local ret = {accounts = {}, capital = body.capital, cashflow = body.cashflow}

local cashflow_fn =  function(accounts)
    local sum_w = 0
    local sum_d = 0
    for i,acc in ipairs(accounts) do
        for _j, t in ipairs(acc.data) do
--[[            t.deposit = t.deposit:gsub("+", "")
            t.deposit = t.deposit:gsub(",", "")
            t.withdraw = t.withdraw:gsub(",", "")
            t.withdraw = t.withdraw:gsub("-", "")]]
            if tonumber(t.deposit) then
                sum_w = sum_w + tonumber(t.deposit)
            end
            if tonumber(t.withdraw) then
                sum_d = sum_d + tonumber(t.withdraw)
            end
        end
    end
    return sum_w - sum_d
end

local sum_mon_fn = function(accounts)
    local mon = {}
    for i,acc in ipairs(accounts) do
        for _j, t in ipairs(acc.data) do
            local period = t.date:sub(4)
            t.deposit = t.deposit:gsub("+", "")
            t.deposit = t.deposit:gsub(",", "")
            t.withdraw = t.withdraw:gsub(",", "")
            t.withdraw = t.withdraw:gsub("-", "")
            t.withdraw = tonumber(t.withdraw) or 0
            t.deposit = tonumber(t.deposit) or 0

            mon[period] = mon[period] or {withdraw = 0, deposit = 0, balance = 0}
            mon[period].withdraw = mon[period].withdraw + t.withdraw
            mon[period].deposit = mon[period].deposit + t.deposit
            mon[period].balance = mon[period].balance + (t.deposit - t.withdraw)
        end
    end 
    return mon
end

local capital_fn = function(accounts)
    local mon = sum_mon_fn(accounts)
    local min = nil
    for k, v in pairs(mon) do
        if not min then min = v.balance end
        if v.balance < min then min = v.balance end
    end 
    return min
end

cashflow = cjson.encode(cashflow_fn(all_accounts.accounts))
capital = capital_fn(all_accounts.accounts)

print('capital = ', capital)
print('cashflow = ', cashflow)
print('ratio = ', capital/cashflow)


