#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local body = nil

function convertHexStringToNormal( str )
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body  = ngx.req.get_post_args() 
    --body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

-- CHECK SESSION
if not session.data.company_id then
    ret.error_msg = 'Invalid Data'
    print(cjson.encode(ret))
    return 0
else
    body.company_id = session.data.company_id
end

if not ok then
    ret = {success = false, error_msg = err}
else
	-- data={}
	db = conn:new_db_handle("dopa")
	local col_name = 'title'
	col = db:get_col(col_name)


  if(not body.company_id) then
        ret = {success = false, error_msg = "Invalid company_id"}
  else
        _id = object_id.new(convertHexStringToNormal(body.company_id))
    	s  = col:find({company = _id})
		list = {} 
	    for k, v in s:pairs() do 
			table.insert(list, {text = v.title,label = v.title, value = tostring(v._id)}) 
		end
       ret = {success = true, data = list}
         
  end	
    print(cjson.encode(ret))
end
