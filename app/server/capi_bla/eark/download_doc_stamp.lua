#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local common = require "common"
local session = require "resty.session".open({secret = "4321"})
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {}
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {} 
end

local updct = 1
local uplog = ''
local uperr = 0
local server_name = rc:get('server.name') or 'N/A'
function do_prog(...)
    local updid = body.id
    local cmd = ''
    local arg = {...}
	for i,v in ipairs(arg)do
       cmd = cmd .. ' ' .. v
	end
	
	uplog = uplog .. 'updid:'..updid..'#'..updct..') '.. cmd ..'\n'
	ngx.log(ngx.NOTICE, 'updid:'..updid.. '#' .. updct .. ') ' .. cmd )
	local res, err = prog(...)
	updct = updct+1
	local err2 = tostring(res.stderr)

	if(err or err2 ~= '')then
		 uperr = uperr + 1
		 local key  = 'err.upd.' .. rc:incr('err.update.cnt')
		 rc:set(key, cjson.encode({cmd = cmd, err = tostring(err), err2 = err2, doc_id = body.id , num = updct}))
		 rc:expire(key,60*60*24*7) -- expire error log after 7 days


	     ngx.log(ngx.NOTICE, 'HAS ERROR ' .. tostring(err) ..', err2 ' .. err2 .. 'ERR_CMD = '  .. cmd)
		 local str = "Document_fail_Document_ID:" .. tostring(body.id) .. ",cmd#" .. updct.. '@' .. server_name .. '[' .. key ..']'

	 else
	     ngx.log(ngx.NOTICE,  'NO ERROR [' .. tostring(res.stdout) ..']' )
	end
end

if body.download_type == 'doc' then
    do_prog('sh', '/home/creden/scripts/ts.sh', body.id .. '-stamp2.pdf')
elseif body.download_type == 'cert' then
	do_prog('sh', '/home/creden/scripts/ts.sh', body.id .. 'ts.pdf')
elseif body.download_type == 'all' then
	do_prog('sh', '/home/creden/scripts/ts.sh', body.id .. 'ts.pdf')
	do_prog('sh', '/home/creden/scripts/ts.sh', body.id .. '-stamp2.pdf')
	local zip = body.id .. '_all' .. '.zip'
	local doc = body.id .. 'ts_timestamped.pdf'
	local cert = body.id .. '-stamp2_timestamped.pdf'
	-- ngx.log(ngx.NOTICE, "sh /home/creden/work/app/server/cap/zip.sh '" .. zip .."' '".. doc .."' '".. cert .."'")
	-- os.execute("sh /home/creden/work/app/server/capi/zip.sh '" .. zip .."' '".. doc .."' '".. cert .."'")
	prog('sh', '/home/creden/work/app/server/capi/zip.sh', zip, doc, cert)
end

ret = {success = true, error_msg = ''}
print(cjson.encode(ret))