#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
bson = require "resty-mongol.bson"
common = require "common"
local HOST_NAME = os.getenv("HOST_NAME")
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local res = {success = false}
local company_id = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    json = ngx.req.get_body_data()
    body = cjson.decode(json)
    if body == nil then 
        res.error_msg = 'Invalid Data body is NIL'
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    else

        if not body.signers or not body.template_id then
            res.error_msg = 'Invalid Data ' .. json
            print(cjson.encode(res))
            ngx.exit(ngx.HTTP_OK)
        end
        
    end

    apiKey = ngx.req.get_headers().apiKey

else
   body = {}
end

-----------------------------------------------------FN-----------------------------------------------------------
function chk_apiKey()
    local find_apiKey, err_find_apiKey = col_user_company:find_one({apiKey = (apiKey or 'no key')})
    if not find_apiKey then 
        ngx.log(ngx.NOTICE,'company_id = ' ..  tostring(apiKey))
        res.error_msg = 'Authentication fail key =[' .. tostring(apiKey) .."]"
        print(cjson.encode(res))
        ngx.exit(ngx.HTTP_OK)
    else
        company_id = tostring(find_apiKey._id)
    end
end

--send email
function send_email_fn(temp)
    
    local pm = {}
    for i, v in pairs(temp.signers) do
        if v.email == "pm_test@creden.co" then
            table.insert(pm, v)
        end
    end

    for i2, v2 in ipairs(pm) do
        local email = v2.email
        local id = temp.doc_number
        local cmd = "node /home/creden/work/app/server_symphony/send_mail_approve.js " .. email .. " " .. id
        os.execute(cmd)
    end
    
end

--gen signer key & sign link
function signer_session_fn(template)
    local signer_key = ''
    local signer_link = ''
    math.randomseed(os.clock()*100000000000)
    for i=1,5 do
        local r = math.random(10, 99)	      
        signer_key = signer_key .. r
    end
    signer_key = template.company_id..os.time()..signer_key
    signer_link = HOST_NAME..'/company/viewer.html?id='..template.id..'&rd='..os.time()..'&lang=th&ch=api&tk='..signer_key
    return signer_key, signer_link
end

function update_deal(doc_number, doc_id) 
    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret_deal = { success = false }
    else
        db = conn:new_db_handle("symphony")
        deal = db:get_col("deal")

        local update_cmd = {["$set"] = { document_id = doc_id }}                
        local u_deal, e_deal = deal:update({ doc_number = doc_number }, update_cmd, 1, 0, true)

        if u_deal then
            ret_deal = { success = true }
        else
            ret_deal = { success = false }
        end
    end
    return ret_deal.success or false
end


ctx = ctx or common.mongo_ctx()
col_edoc_list = ctx.col["edoc_list"]
col_user_company = ctx.col["dopa.user_company"]
doc_number = body.doc_number
body.doc_number = nil
local params = body

chk_apiKey()
ngx.log(ngx.NOTICE,'company_id = ' .. company_id)
local template, err_template = col_edoc_list:find_one({id = params.template_id, company_id = company_id})

ngx.log(ngx.NOTICE,'Found template'.. tostring(template))
 
if template  then 
    template.txid = params.txid or ''
    -- todo : copy PDF file from template to new doc
    local dir = '/home/creden/work/app/client/face/images/card/'
    local f1  = dir .. template.id .. '.pdf'
    --gen new document id
    template.id = template.company_id .. os.time()
    template.pdfFile = template.id
    template.owner = "symphony@creden.co"
    template.owner_name = "SYMPHONY"
    template.owner_position = ""
    local f2  = dir .. template.id .. '.pdf'
    local cmd = 'cp ' .. f1 .. ' ' .. f2 ..' &'
    os.execute(cmd) 
    

    template.redirect_url = params.redirect_url or 'https://creden.co'
    if table.getn(params.signers) == table.getn(template.signers) then
        --replace email to fields
        for k, v in ipairs(template.fields) do
            --replace value to fields
            if params.default_field_values then
                v.toolData = params.default_field_values[v.field_no]
            end
            for k1, v1 in ipairs(params.signers) do
                if v.signOrder == v1.no then
                    v.email = v1.email
                    v.name = v1.name
                end
            end

            for k2, v2 in pairs(params.prefill_field_values or {}) do
                if v.field_no == k2 then
                    v.toolData = v2
                    v.time = os.date("%x %X")
                    v.status = 1
                    v.ip_field = "-"
                end
            end
        end

        --replace email to signers
	
	-- patch auto insert signer.no
	for i,signer in ipairs(template.signers) do
             if(not signer.no) then signer.no = i end
	end

        for key, val in ipairs(template.signers) do
            for key1, val1 in ipairs(params.signers) do
                if val.no == val1.no then
                --if true then
                    val.email = val1.email
                    val.name = val1.name
                    val.signer_session, val.signer_link = signer_session_fn(template)
                end
            end
        end

        --create document
        template._id = nil
        ngx.log(ngx.NOTICE,'CREATED>>>>' .. template.id)
        --template.createdDtm = bson.get_utc_date(os.time() * 1000)
        template.doc_number = doc_number
        local create, err_create = col_edoc_list:insert({template})
        update_deal(doc_number, template.id)

        if create then
            send_email_fn(template)
            res.document_id = template.id
            res.signers = template.signers
            res.success = true
        else
            res.error_msg = err_create
        end
    else
        res.error_msg = 'Number of signers invalid'
    end
else --if template
    res.error_msg = 'Template is invalid'
end
res.xxx = company_id
ngx.log(ngx.NOTICE,'check txid444' .. tostring(cjson.encode(res)))
print(cjson.encode(res))
