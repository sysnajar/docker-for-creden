local server = require ("resty.websocket.server")

local clients = {}
local cjson = require 'cjson'
local redis = require 'resty.redis'
local red = redis:new()
red:set_timeout(1000)
red:connect('127.0.0.1', 6379)
local sleep_timeout = 10

local wb, err = server:new{
  timeout = 5000,
  max_payload_len = 65535
}
if not wb then
  ngx.log(ngx.ERR, "failed to new websocket: ", err)
  return ngx.exit(444)
end


local key = nil
while true do	
  	
  local data, typ, err = wb:recv_frame()

  if(key ~= nil)then
      local val = red:get(key)
      --ngx.log(ngx.INFO, 'loop ' .. tostring(key) .. ' = ' .. tostring(val) )
      if(type(val)=='string') then
          local res = {key = key}
          red:lrem('active_jobs', 0, key)
          red:lpush('complete_jobs', key)          
          bytes, err = wb:send_text(cjson.encode(res))
          if not err then
            red:set('tmp_nut',bytes)
            red:lrem('complete_jobs', 0, key)            
          end
		  break 
	  end
  end

  if wb.fatal then
	ngx.log(ngx.ERR, "failed to receive frame: ", err)
	return ngx.exit(444)
  end
  if not data then
	local bytes, err = wb:send_ping()
	if not bytes then
	  ngx.log(ngx.ERR, "failed to send ping: ", err)
	  return ngx.exit(444)
	end
  elseif typ == "close" then break
  elseif typ == "ping" then
	local bytes, err = wb:send_pong()
	if not bytes then
	  ngx.log(ngx.ERR, "failed to send pong: ", err)
	  return ngx.exit(444)
	end
  elseif typ == "pong" then
  elseif typ == "text" then
    key = data	
	--clients[key] = wb
	--ngx.log(ngx.INFO, "registered ws client#" .. data)
  end
  ngx.sleep(sleep_timeout)
end

ngx.log(ngx.INFO, 'closing websocket for client' .. tostring(key))
wb:send_close()
