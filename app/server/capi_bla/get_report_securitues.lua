#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local bson = require "resty-mongol.bson" 

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    -- local ddd = 05
    -- local mmm = 12
    -- local yyyy = 2018

    -- local dd = 06
    -- local mm = 12
    -- local yy = 2018  

    local ddd = body.start_dd
    local mmm = body.start_mm
    local yyyy = body.start_yyyy

    local dd = body.end_dd
    local mm = body.end_mm
    local yy = body.end_yyyy

    local db = conn:new_db_handle("dopa")
    local col = db:get_col("securities_form");
        
    local targetDate = (os.time({year=tonumber(yyyy),month=tonumber(mmm),day=tonumber(ddd),hour=0, sec=0})*1000)
    local dtm  = bson.get_utc_date(targetDate)

    local targetDate2 = (os.time({year=tonumber(yy),month=tonumber(mm),day=tonumber(dd),hour=23, sec=59})*1000)
    local dtm2  = bson.get_utc_date(targetDate2)

    local user_list = col:find({createdDtm = {["$gte"] = dtm, ["$lt"] = dtm2}})
    local new_list = {}
    -- local field = {"id","title","firstName","lastName","cardID","phone","email","address","createdDtm"}
    -- local csv = 'id,title,firstName,lastName,cardID,phone,email,address,createdDtm\n'

    -- --[[{"country_id":"1234","cardType":"เลขประจำตัวประชาชน(ใน)","lastName":"จันทร์ชื่น","firstName":"คุณากร","c10":"1","c1":"4","c8":"3","c3":"3","c2":"1","cardID":"1100701382071","phone":"0957513388","email":"nutp10.1@gmail.com","id":"1111111","c7":"2","c4":2,"c5":"1","c12":"2","title":"MR.","c9":"3","address":"198\/70","c6":"2","cc4":["1","2"],"createdDtm":1544042731700,"c11":"1"}]]

    -- for i, v in user_list:pairs() do 
    --     v._id = nil
    --     for k, v2 in ipairs(field) do
    --         csv =csv..v[v2]..','
    --     end
    --     csv = csv:sub(1, -2)
    --     csv = csv..'\n'
    -- end

    for i, v in user_list:pairs() do 
        v._id = nil
        local col = db:get_col("ekyc");
        local t, err_ekyc = col:find_one({txid = v.id, compCode = 'globlex'})
        if t then
            v.ekyc = {remarks = t.remarks, isValid = t.isValid, isValidDopa = t.isValidDopa, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, isValidLiveness = t.isValidLiveness, livenessResult = t.livenessResult,status = t.process_status}
        end
        table.insert(new_list, v)
    end

    ret = {success = true, error_msg = "complete", data = new_list }
end

print(cjson.encode(ret))
