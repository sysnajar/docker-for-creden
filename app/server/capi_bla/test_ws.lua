#!/usr/bin/env lua
local copas = require'copas'
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

-- create a copas webserver and start listening
local server = require'websocket'.server.copas.listen
{
  -- listen on port 8080
  port = 8089,
  -- the protocols field holds
  --   key: protocol name
  --   value: callback on new connection
  protocols = {
    -- this callback is called, whenever a new client connects.
    -- ws is a new websocket instance
    creden = function(ws)
      while true do
        local message = ws:receive()
        if message then
          res = rc:get(message)
          if res then
            print(res)
          else
            os.execute('sleep 10')
          end
        else
           ws:close()
           return
        end
      end
    end
  }
}

-- use the copas loop
copas.loop()
