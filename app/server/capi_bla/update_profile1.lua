#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = 'kjunchean@gmail.com', password = '1'} 
end

if body.email ~= '' then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("test")
        col = db:get_col("user")
        body.activated = 1
        update = body
        u, err = col:update({email=body.email}, update, 1, 0, true)
        if not err then
            ret = {success = true, error_msg = "complete", data = update}
        else
            ret = {success = false, error_msg = err}
        end
    end
else
    ret = {success = true, error_msg = "complete", data = body}
end

print(cjson.encode(ret))
