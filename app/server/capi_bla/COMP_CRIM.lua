#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end
    local fn =  function(id)
        local key = 'TRAN:CRIM:' .. id
        local t = cjson.decode(rc:get(key))
        local max   = 500
        local score = max

		
		if(t.CRIM_blackamt > 0) then 
		  score = score - (max/5)
		end

		if(t.CRIM_redamt > 0) then 
		  score = score - (max/5)
		end

		if(t.CRIM_bankrupt > 0) then 
		  score = score - ((max/5)*3)
		end


        return score
    end
    
    
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
    
