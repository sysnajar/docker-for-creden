var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = process.env.HOST_NAME + "/esign/images/Creden_logo.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var html = `
  <!DOCTYPE html>
  <html lang="th-TH" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
  </head>
  <body style="background: #E0E0E0;"><br>
    <table border="0" cellspacing="0" cellpadding="0" style="background: #ffffff;margin: 0px auto 15px auto;padding: 10px 20px;width:`+ wTable +`;">
      <tr>
          <td>
            <div style="max-width: 160px;">
              <img src="`+ logo +`" alt="logo website" style="width: 100%;">
           </div>
          </td>
      </tr>
      <tr>
        <td style="width: 100%;text-align: center;color: #ffffff;padding: 0px 30px 50px 30px;background: `+ bg +`;">
          <div>
            <img src="`+ process.env.HOST_NAME +`/esign/images/mail/mail_active.png" style="margin-top: 30px;">
          </div>
          <div style="margin-top:15px;font-size: 16px;">
          Your verification code is :
          </div>
          <div style="margin-top:10px;font-size: 16px;">
          รหัสยืนยันของคุณคือ:
          </div>
          <div style="margin-top:15px;color: #F2C94C;font-size: 18px;font-weight: bold;letter-spacing: 5px;">`+ process.argv[3] +`</div>
        </td>
      </tr>
      <tr>
        <td style="color: #222222;font-size:12px;">
          <div style="margin-top:15px">
          รหัสยืนยันนี้จะหมดอายุใน 5 นาที หากคุณพบปัญหาโปรดติดต่อฝ่ายสนับสนุนของเรา
          </div>
          <div style="margin-top:10px">
          This verification code will expire in 5 minutes. If you are having trouble with this code please contact our support.
          </div>
          <div style="margin-top:30px">
            ขอบพระคุณที่เลือกใช้ ครีเดน เอเชีย 
          </div>
          <div style="margin-top:10px;margin-bottom: 10px;">
            Thank you for choosing Creden.co
          </div>
        </td>
      </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="0" style="margin:auto;padding: 10px 20px;color: #222222;font-size:12px;width:`+ wTable +`;">
    <tr>
      <td>
        <div>
          <b>หมายเหตุ</b> : โปรดอย่าตอบกลับอีเมลนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
        </div>
        <div style="margin-top:5px">
          หากต้องการสอบถามข้อมูลเพิ่มเติมโปรดติดต่อฝ่ายบริการลูกค้า <a href="mailto:support@creden.co">support@creden.co</a>
        </div>
        <div>
          <hr style="width: 100%;margin-top: 20px;background: #BDBDBD;">
        </div>
        <div style="margin-top:15px">
          <b>Note</b> :  Please do not reply to this automated email.
        </div>
        <div style="margin-top:5px">
          Please send any feedback or bug reports to <a href="mailto:support@creden.co">support@creden.co</a>
        </div>
      </td>
    </tr>
  </table>
  </body>
  </html>
  `

  var smtp = {
    host: 'smtp.mailgun.org', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      // user: 'credenmailer@gmail.com', //user account
      // // pass: 'creden2018' //user password
      // pass: 'dnkrakkaxnmnakpr'
      user: 'postmaster@mg.creden.co', //user account
      pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
    to: process.argv[2], //to email (require)
    cc: '',
    subject: '[CREDEN] code authen', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


