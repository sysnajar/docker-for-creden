#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("ekyc");
    if body.txid and body.txid ~= '' then
        local s, err = col:find_one({txid=body.txid})
        if s then
            s._id = nil
            local data = {fnameTH = s.fnameTH, lnameTH = s.lnameTH, cardNo = s.cardNo, dob = s.dob}
            ret = {success = true, error_msg = "complete", data = data }
        else
            ret = {success = false, error_msg = err}
        end
    else
        ret = {success = false, error_msg = 'Invalid txid'}
    end
end

print(cjson.encode(ret))
