curl -v -X POST https://api.line.me/v2/bot/message/push \
-H 'Content-Type:application/json' \
-H 'Authorization: Bearer indARxinViDCBMjmEtyNOtlCBM1WWotI4MtHM3mC8Xqpaz1C7yO+piU5GlUhEbvtVqFZ6bI90lASeh+XpCVDurLrnCxjkQL0apDdS47qG6zYpiXxa0LOGiSJ7mt9fAb00uLzdGQKZn2vPsIhWcgPmAdB04t89/1O/w1cDnyilFU=' \
-d '{
  "to": "'$1'",
  "messages": [
    {
     "type": "template",
  "altText": "this is a confirm template",
  "template": {
    "type": "confirm",
    "actions": [
      {
        "type": "message",
        "label": "ยืนยัน",
        "text": "ยืนยัน"
      },
      {
        "type": "message",
        "label": "ยกเลิก",
        "text": "ยกเลิก"
      }
    ],
      "text": "'$2'"
    }
    }
  ]
}'