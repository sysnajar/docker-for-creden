local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local is_company = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
end

if not session.data.username and (body.action ~= 'accept' or body.action ~= 'cancel') then 
    print(cjson.encode(ret))
    return 0
end

--u = esig_user
--p = new position
function add_org_node(company_id, u, new_pos)	
    new_pos.position_id = nil
	local new_node = {
	  id  = new_pos.node_id,
	  title_id  = new_pos.position_id,
	  title     = new_pos.position_name,
	  person_id = tostring(u._id),
	  name = u.adminFisrtname,
	  img = "xxx",
	  email = u.adminEmail
	 }

	local key = "orgchart." ..company_id
	local json = rc:get(key)
	local ret = nil

	if(json)then
	  ret = cjson.decode(json)
	else
	  ret = {is_empty = false, company_id = company_id, nodes = {}}
	end

	table.insert(ret.nodes, new_node)
	ret.is_empty = false
	ret.success  = true
	ngx.log(ngx.NOTICE, "update org_chart ... " .. cjson.encode(ret))
	rc:set(key, cjson.encode(ret))
end
-- END EARK FN

-- update member status
conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if not err then
    local db_dopa = conn:new_db_handle("dopa")
    local col_member_list = db_dopa:get_col("company_members")
    local col_esig_user = db_dopa:get_col("esig_user")
    user_member = db_dopa:get_col("user_company")
    local email = session.data.username
    local status = ''

    if body.action == 'accept' then 
        status = 'APPROVE' 
    else 
        status = 'UNAPPROVE' 
    end

    local member, member_err = col_member_list:update({email = email}, {["$set"] = {status = status}})
    local s_eisg_user, err_s = col_esig_user:find_one({adminEmail = session.data.username})

    if s_eisg_user then
        local member_detail, err_member_detail = col_member_list:find_one({email = session.data.username})
        if member_detail then

            admin,err = col_member_list:find({ company_id = member_detail.company_id, role = "ADMIN" })
            for i, v in admin:pairs() do 
                owner,err = user_member:find_one({ _id = v.company_id })
                prog('node', '/home/creden/work/app/server/capi/approve_member.js', v.email, status, s_eisg_user.adminEmail, owner.companyName)
            end

            add_org_node(tostring(member_detail.company_id), s_eisg_user, member_detail.positions[1]) 
            -- delete member key
            local key = 'member.'..session.data.username..'.'..tostring(member_detail.company_id)
            local find_key = rc:keys(key)
            rc:del(find_key[1])
        end
    end
end



ret.success = true
print(cjson.encode(ret))