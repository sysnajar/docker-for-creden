#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil
prog.timeout_fatal = false
--generate code
math.randomseed(os.clock()*100000000000)
code = ''
for i=1,2 do
    code = math.random(10, 99)..code
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)  
else
   body  = {email = arg[1], firstname = 'Kunakorn'} 
end

if body.email ~= '' then
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("creden")
        col = db:get_col("users")
        s = col:find_one({email=body.email, activated=1})

        if not s then
            doc = {firstname=body.firstname, lastname=body.lastname, email=body.email, dob=body.dob, password=body.password, tel=body.tel, activated=0, code=code}
            i, err = col:insert({doc}, nil, true)
            if err then
                ret = {success = false, error_msg = err}
            else
                local res, err = prog('curl', 'https://y1fnlipapa.execute-api.ap-southeast-1.amazonaws.com/Testing/function5?email='..body.email..'&code='..code..'&server=b2c')
                if not err then
                    ret = {success = true, error_msg = "please confirm your email", data = body}
                else
                    ret = {success = false, error_msg = err}
                end
            end
        else
            ret = {success = false, error_msg = "existing email"}
        end
    end
else
    ret = {success = false, error_msg = " Incorrect email"}
end

print(cjson.encode(ret))


