#!/usr/bin/env lua
size = arg[1]/4 .. 'x' .. arg[2]/2
print('convert -size '..size..' xc:black empty.jpg')
