local upload = require "resty.upload"
local cjson = require "cjson"
local chunk_size = 4096 -- should be set to 4096 or 8192

local file

rand  = function(len)
	local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
	local t = {}
	local cnt = 0
				    
	local i = 1
	repeat
		local f = assert (io.popen (cmd, 'r'))
		local line = f:read('*all'):match("%d+")
		f:close()
		cnt = cnt + #line
		table.insert(t, line)
	until cnt>len												        
	local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
	return ret
end

local name = rand(20)

local file_name = '/home/creden/work/app/client/face/images/card/' .. name .. ".png"



--ngx.say "ok"

local form, err = upload:new(chunk_size)
if not form then
  ngx.say "no form"
  --ngx.log(ngx.ERR, "failed to new upload: ", err)
  --ngx.exit(500)
end

-- ngx.say "ok2"
form:set_timeout(1000) -- 1 sec
file = io.open(file_name, "w+")



local len = 0
while true do
local typ, res, err = form:read()
if not typ then
	ngx.say("failed to read: ", err)
	return
end

if(typ=='body')then
  len = len + #res
  file:write(res)
end
--ngx.say("read1: ", cjson.encode({typ, res}))

if typ == "eof" then
	file:close()
	break
end
end

local typ, res, err = form:read()
--ngx.say("read2: ", cjson.encode({typ, res}))

--ngx.say ("total" .. tostring(len))
ngx.say (cjson.encode({success = (len>0), filename = name}))

