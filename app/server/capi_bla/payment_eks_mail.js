var mailer = require("nodemailer");
var redis = require('redis')
var rc = redis.createClient('6379', '127.0.0.1');
var process2  = require('./config.js')
process.env.HOST_NAME = process2.env.HOST_NAME // Was >> "https://hjkl.ninja/"

var ref_id = "package_refid." + process.argv[2]
rc.get(ref_id, function (err, reply) {
  if (reply == null) {
    res = { "success": false, "message": "not ref id" }
    console.log(res)
    process.exit();
  } else {
    send_mail(reply)
  }
});


// send_mail()

function status_pay(s, lang) {
  var txt = ""
  if (s == true) {
    if (lang == "th") txt = `<th style="color: #219653">ชำระแล้ว</th>`
    else txt = `<th style="color: #219653">PAID</th>`
  }
  else {
    if (lang == "th") txt = `<th style="color: #F05522">ยังไม่ได้ชำระ</th>`
    else txt = `<th style="color: #F05522">UNPAID</th>`
  }
  return txt
}

function date_time(d, lang) {
  var date = d + (7 * 60 * 60 * 1000)
  date = new Date(date)
  if (lang == "th") {
    var result = date.toLocaleDateString("th-TH", {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    })
  }
  else {
    var result = date.toLocaleDateString("en-EN", {
      year: "numeric",
      month: "long",
      day: "numeric",
      hour: "numeric",
      minute: "numeric",
    })
  }
  return result
}

function numberWithCommasDotZero(x) {
  x = Number(x).toFixed(2)
  x = x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  return x
}

function detail(list, sub_list, count, price, currency, discount) {
  price = Number(price)
  var txt = `<tr>
  <td style="padding-top: 10px !important;">
    <table>
      <tr>
        <td style="vertical-align: top;">
          <img src="`+ process.env.HOST_NAME + `/esign/images/creden-icon.png" style="width: 30px;">
        </td>
        <td style="padding: 0px 5px;">
          <div style="text-transform: capitalize;">
          <b> `+ list + ` </b>
          </div>`

    if (!sub_list.length) sub_list = []
    var sub = ""
    for (i = 0; i < sub_list.length; i++) {
      sub = sub + "<div> - " + sub_list[i] + "</div>"
    }

  var txt2 = `</td>
      </tr>
    </table>
  </td>
  <td style="text-align:center;padding-top: 10px !important;vertical-align: top;"><b> `+ count + ` </b></td>`
  var txt3 = `<td style="text-align:right;padding-top: 10px !important;vertical-align: top;"><b> `+ numberWithCommasDotZero(price) + " " + currency + ` </b></td></tr>`
  if (discount == true) {
    txt3 =  `<td style="text-align:right;padding-top: 10px !important;vertical-align: top;color: #EB5757;"><b> - `+ numberWithCommasDotZero(price) + " " + currency + ` </b></td></tr>`
  }

  var result = txt + sub + txt2 + txt3
  return result
}

function get_detail_package(obj, currency){
  var result = ""
  var sub_list = []
  var s = ""

  if (obj.package != "addon"){
    sub_list = []
    sub_list.push(obj.users  + " User")
    result = result + detail(obj.package + " Package", sub_list, "1", obj.price_package, currency)
  }
  else {
    result = result + detail("Add-on Package", [], "1", "0", currency)
  }

  if (!obj.storage.length) obj.storage = []
  if (obj.storage.length > 0 ){
    s = obj.storage[0]
    result = result + detail("Additional " + s.display_name, [], "1", s.price, currency)
  }
 
  if (obj.user_ekyc > 0 ){
    s = obj.ekyc[0]
    result = result + detail("e-KYC " + s.display_name, [], obj.user_ekyc, s.price, currency)
  }

  if (obj.advance_func == true){
    sub_list = []
    sub_list = ["Initials Sign", "In-person Sign", "Clone All Signature", "Image Field"]
    result = result + detail("Add-on Features", sub_list, "1", "3000", currency)
  }

  if (!obj.form.length) obj.form = []
  if (obj.form.length > 0) {
    sub_list = []
    var f = obj.form[0]
    for (i=0; i < 7; i++) {
      if (f["form_" + (i+1)] == true) {
        sub_list.push("Form " + (i+1))
      }
    }
    result = result + detail("Additional e-Form", sub_list, "1", f.price, currency)
  }

  if (obj.user_add_on > 0) {
    var sum = 1500 * obj.user_add_on
    result = result + detail("Additional User", [], obj.user_add_on, sum, currency)
  }


  if (Number(obj.discount) > 0) {
    result = result + detail("Remaining Credit", [], "1", obj.discount, currency, true)
  }

  if(obj.promo_code_use == true){
    sub_list = [obj.promo_code]
    result = result + detail("Promotion Code", sub_list, "1", obj.promo_code_value, currency, true)
  }

  return result
}


function send_mail(obj) {
  process.env.HOST_NAME = "https://hjkl.ninja/"
  var logo = process.env.HOST_NAME + "/esign/images/Creden_logo.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/esign/web/policy.html"
  var icon = process.env.HOST_NAME + "/esign/images/creden-icon.png"
  var user = JSON.parse(process.argv[3])
  var to_member = user.adminEmail
  var json = JSON.parse(obj)
  var r = json

  var html = `
  <!DOCTYPE html>
  <html lang="th-TH" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
  </head>
  <body style="background: #E0E0E0;"><br>
    <table border="0" cellspacing="0" cellpadding="0" style="background: #ffffff;margin: 0px auto 15px auto;padding: 10px 20px;width:`+ wTable + `;">
      <tr>
          <td>
            <div style="max-width: 160px;">
              <img src="`+ logo + `" alt="logo website" style="width: 100%;">
            </div>
          </td>
      </tr>
      <tr>
        <td style="width: 100%;text-align: center;color: #ffffff;padding: 0px 30px 50px 30px;background: `+ bg + `;">
          <div>
            <img src="`+ process.env.HOST_NAME + `/esign/images/mail/mail_check.png" style="margin-top: 30px;">
          </div>
          <div style="margin-top:15px;font-size: 16px;">
            Thank You for Your Order!
          </div>
          <div style="margin-top:10px;font-size: 16px;">
            ขอบคุณที่ชำระค่าบริการ
          </div>
        </td>
      </tr>
      <tr>
        <td style="color: #222222;font-size:14px;">
          <div style="margin-top:15px">
            <b>สวัสดี&nbsp;`+ user.adminEmail +`</b>  
          </div>
          <div style="margin-top:5px;margin-bottom:30px;">
            ใบสั่งซื้อของคุณ # `+ r.ref_id + ` ได้รับการดำเนินการอย่างสมบูรณ์
          </div>
        </td>
      </tr>
      <tr>
        <td style="color: #222222;border-top: 2px dashed;width:100%;"></td>
      </tr>
      <tr>
        <td style="padding: 15px 0px;">
          <table style="width:100%">
            <thead>
              <tr style="text-align: left">
                <th>สั่งซื้อเมื่อ:</th>
                <th>ราคาทั้งหมด:</th>
                <th>สถานะ:</th>
                <th>การชำระเงิน:</th>
              </tr>
              <tr style="text-align: left">
                <th>`+ date_time(r.date_buy, "th") + ` น.</th>
                <th>`+ r.total + `.00 บาท</th>
                <th style="color: #219653">ปิด</th>
                `+ status_pay(r.status, "th") + `
              </tr>
            </thead>
          </table>
        </td>
      </tr>
      <tr>
        <td style="color: #222222;border-top: 2px dashed;width:100%;"></td>
      </tr>
      <tr>
        <td style="font-size:18px;padding-top: 20px;"><b>Creden e-Signature - สรุปคำสั่งซื้อ</b></td>
      </tr>
      <tr>
        <td>
          <table style="width:100%;margin-top:15px;">
            <thead>
              <tr style="text-align: left">
                <th>รายการ</th>
                <th style="text-align:center;">จำนวน</th>
                <th style="text-align:right;">ราคา</th>
              </tr>
            </thead>
            <tbody>`+ get_detail_package(r, "บาท") +`</tbody>
          </table>
        </td>
    </tr>
    
    <tr>
      <td style="color: #222222;border-bottom: 2px dashed;width:100%;padding-top:30px;"></td>
    </tr>

    <tr>
      <td style="padding-top:15px;">
        หากคุณต้องการความช่วยเหลือส่วนตัวโปรดติดต่อเราโดยตรงได้ที่: <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
      </td>
    </tr>

    <tr>
      <td style="padding-top:10px;">
        ขอแสดงความนับถือ
      </td>
    </tr>

    <tr>
      <td style="padding-top:10px;">
        ทีมงาน Creden EKS
      </td>
    </tr>

    <tr>
      <td style="color: #222222;border-bottom: 2px dashed;width:100%;padding-top:40px;"></td>
    </tr>
    <tr>
      <td style="color: #222222;border-bottom: 2px dashed;width:100%;padding-top:20px;"></td>
    </tr>

    <tr>
    <td style="color: #222222;font-size:14px;">
      <div style="margin-top:40px">
        <b>Dear&nbsp;`+ user.adminEmail +`</b>  
      </div>
      <div style="margin-top:5px;margin-bottom:30px;">
        Your order # `+ r.ref_id + ` has been fully processed.
      </div>
    </td>
  </tr>
  <tr>
    <td style="color: #222222;border-top: 2px dashed;width:100%;"></td>
  </tr>
  <tr>
    <td style="padding: 15px 0px;">
      <table style="width:100%">
        <thead>
          <tr style="text-align: left">
            <th>Order Placed:</th>
            <th>Total Amount:</th>
            <th>Status:</th>
            <th>Payment:</th>
          </tr>
          <tr style="text-align: left">
            <th>`+ date_time(r.date_buy, "en") + ` </th>
            <th>`+ Number(r.total).toFixed(2) + ` Bath</th>
            <th style="color: #219653">CLOSED</th>
            `+ status_pay(r.status, "en") + `
          </tr>
        </thead>
      </table>
    </td>
  </tr>
  <tr>
    <td style="color: #222222;border-top: 2px dashed;width:100%;"></td>
  </tr>
  <tr>
    <td style="font-size:18px;padding-top: 20px;"><b>Buy Package - Order Summary</b></td>
  </tr>
  <tr>
    <td>
      <table style="width:100%;margin-top:15px;">
        <thead>
          <tr style="text-align: left">
            <th>Item</th>
            <th style="text-align:center;">Qty</th>
            <th style="text-align:right;">Amount</th>
          </tr>
        </thead>
        <tbody>`+ get_detail_package(r, "Baht") +`</tbody>
      </table>
    </td>
</tr>

<tr>
  <td style="color: #222222;border-bottom: 2px dashed;width:100%;padding-top:30px;"></td>
</tr>

<tr>
  <td style="padding-top:15px;">
    If you need personal assistance, please contact us directly by emailing: <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
  </td>
</tr>

<tr>
  <td style="padding-top:10px;">
    Sincerely.
  </td>
</tr>

<tr>
  <td style="padding:10px 0px 30px 0px;">
    Creden EKS team
  </td>
</tr>


  </table>
  <table border="0" cellspacing="0" cellpadding="0" style="margin:auto;padding: 10px 20px;color: #222222;font-size:12px;width:`+ wTable + `;">
    <tr>
      <td>
        <div>
          <b>หมายเหตุ</b> : โปรดอย่าตอบกลับอีเมลฉบับนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
        </div>
        <div style="margin-top:5px">
          หากต้องการสอบถามข้อมูลเพิ่มเติมโปรดติดต่อฝ่ายบริการลูกค้า <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
        </div>
        <div>
          <hr style="width: 100%;margin-top: 20px;background: #BDBDBD;">
        </div>
        <div style="margin-top:15px">
          <b>Note</b> : This is an automated message. Please do not reply directly to this email.
        </div>
        <div style="margin-top:5px;margin-bottom: 5px;">
          For more information, please contact the application's support team: <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
        </div>
      </td>
    </tr>
  </table>

  <table border="0" cellspacing="0" cellpadding="0" style="margin:0px auto auto auto;color: #000000;font-size:12px;width:100%;background: #F0F0F0;">
    <tr style="background: #FFF;"><td style="height: 10px"></td></tr>
    <tr>
      <td style="text-align:center;padding-top:15px;">
        Creden Asia Co., Ltd. 9/22 Ratchapruek Road, Bang Ramat Subdistrict, Taling Chan District 10170, <span style="color: #2F80ED">095-374-8973</span>
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding-top: 10px;">
        <a target="_blank" style="text-decoration: none;" href="`+ home + `">www.creden.co</a> 
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding-top: 10px;">
        <a target="_blank" style="text-decoration: none;" href="`+ policy + `">Privacy Policy</a> 
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding: 10px 0px 15px 0px;">
        © 2021, Creden Asia Co., Ltd. All Rights Reserved 
      </td>
  </tr>
  </table>
  </body>
  </html>
  `

  var smtp = {
    // host: 'smtp.gmail.com', //set to your host name or ip
    host: 'smtp.mailgun.org', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      // user: 'credenmailer@gmail.com', //user account
      // // pass: 'creden2018' //user password
      // pass: 'dnkrakkaxnmnakpr'
      user: 'postmaster@mg.creden.co', //user account
      pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
    to: to_member, //to email (require)
    cc: '',
    subject: '[Creden] Order Number #' + r.ref_id, //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


