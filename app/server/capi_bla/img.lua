#!/usr/bin/env lua
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    body = ngx.req.get_uri_args()
else
   body  = {adminEmail = arg[1],file = "1530097604161"} 
end


---------------------------------------------------
function get_docs_by_image_file(fileName)
    local conn = mongo()
    conn:set_timeout(5000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
		return nil
    else
        local db = conn:new_db_handle("edoc")
        local col = db:get_col("edoc_list")
        local list = col:find({['imageFiles.fileName'] = fileName})
        return list
     end
end


function is_viewable(doc, username, pwd)
	    local ret = false
        --check 3.1
		if(doc.owner == username)then
           ret = true
		end

        --check 3.2
	    for i, person in ipairs(doc.signers) do
            if(person.email == username)then 
			 ret = true 
			end
		end

    return ret

end
---------------------------------------------------








have_file = false 
err_msg = ''

local uri  = ngx.var.request_uri
local uri_i  =  string.match(uri, '^.*()/')
uri   =  string.sub(uri, uri_i+1)

local qi = string.find(uri, '?')
if(qi)then uri = string.sub(uri,1,qi-1)  end

local ext = nil
local di = string.find(uri, '%.')
if(di)then  ext = string.sub(uri, di+1) end

response = {error = "NO PERMISSION", file = uri, ext = ext}
response.ext = nil
response.uri = nil
response.original_uri = ngx.var.request_uri

local myusername  = session.data.username or 'N/A'

if(ext and ext:lower()=='pdf')then
--   ngx.say(cjson.encode(response))
   local header      = ngx.req.get_headers()
   local ip_stamp = header["x-real-ip"] or ''
   ngx.log(ngx.NOTICE, "DETECT DOC ACCESS  " .. myusername .. " (" .. ip_stamp  .. ") try to access " .. response.file)
  ngx.log(ngx.NOTICE, "tomecho"..tostring(response.file).."<<") 
     if(true) then
	 ngx.exec('/face/images/card/' .. response.file)
     else
	ngx.say(cjson.encode(response))
	ngx.exit(ngx.HTTP_OK)
    end
   
   --ngx.exit(ngx.HTTP_OK)
else	
   --non PDF file access 	
   local f3 = response.file:sub(1,3) 
   local show_file = true

   file_counter = 0

   if(f3=='pdf' and ext:lower()=='png')then
       show_file = false    
	   local docs = get_docs_by_image_file(response.file)
       ngx.log(ngx.NOTICE, "DETECT IMG ACCESS3 [" .. myusername ..']')
	   for i, doc in docs:pairs() do
		   file_counter = file_counter+1
		   if(is_viewable(doc, myusername, nil))then
             show_file = true
			 break;
		   end
			
	   end

	   if(not show_file and file_counter==0) then
		   show_file = true
	   end

   end

	
       ngx.log(ngx.NOTICE, "file_counter = " .. file_counter)

    show_file = true	   
   if(show_file==true)then
      ngx.exec('/face/images/card/' .. response.file)	  
   end	  
end

