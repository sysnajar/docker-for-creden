#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
prog.timeout_fatal = false
local HOST_NAME = 'http://103.219.196.104:7519'
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
body = {}

function trim5(s)
    return s:match'^%s*(.*%S)' or ''
end
  
function getVal(key)
    local ret = body[key]
    ret = ret and trim5(ret) or nil
    if(ret=='')then ret = nil end
    return ret
end
  
function get_cust_code(usr)
    return usr
end

function chk_kyc()
    print('--------------check dopa-------------')
    local result = nil
    local params   = {cardNo = (body.cardNo or ''), fnameTH = (body.fnameTH or ''), lnameTH = (body.lnameTH or ''), dob = (body.dob or ''), laserCode = (body.laserCode or '')}
    print('dopa: '.. cjson.encode(params))
    local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/dopa', '-d', cjson.encode(params))
    if(not err)then
        print('response chk_kyc: '..res.stdout)
        local tmp = cjson.decode(res.stdout)
            if(tmp) then
                if tostring(tmp.status) == 'true' then
                    result = tmp.status
                    body.status = 'pass'
                    body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
                else
                    body.status = 'fail'
                    body.remarks = tmp.message:gsub('java.lang.RuntimeException: %[4%]','')   
                end             
            isValidDopa = tmp.status
            end 
    else 
        print('chk_kyc error: '..err)
    end
   
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
    return result
end

function chk_amlo()
    print('chk_amlo')
    local isValidAML = false
    local params   = {card_no = (body.cardNo or '')}
    local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/amlo', '-d', cjson.encode(params))
    if not err then
      tmp = cjson.decode(res.stdout)
      isValidAML = tmp.isValid
    end 
    body.isValidAML = isValidAML
end

function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical)
    if body.vdo and string.match((body.vdo.url or ''), "http") then
        body.isValid = false
        body.isValidLiveness = 'processing'
        body.process_status = 'processing'
    else
        if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' and tostring(isValidAML) == 'true' then
            body.isValid = true
        else
            body.isValid = false
        end 
        body.process_status = 'finished'
    end
end

function chk_face()
  t = {}
  local faceId1 = ''
  local faceId2 = ''
  if getVal('cardImg1') and getVal('cardImg3') then
    faceId1 = detect_face(body.cardImg1)
    faceId2 = detect_face(body.cardImg3)
    t = verify_face(faceId1, faceId2)
    body.faceConfidence = (t.faceMatchPercent or 0)
    body.isFaceIdentical = (t.isIdentical or false)
  end
end

function detect_face(img)
    print('detect_face')
    local img_url = HOST_NAME..'/face/images/card/'..img
    print('img_url: '..img_url)
    local faceId = ''
    local params   = {img_url = img_url}
    local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/detect_face', '-d', cjson.encode(params))
    if not err then
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end

function verify_face(faceId1,faceId2)
    t = {}
    local params   = {faceId1 = faceId1, faceId2 = faceId2}
    local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/services/verify_face', '-d', cjson.encode(params))
    if not err then
        print('verify_face')
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        t.faceMatchPercent = tmp.data.confidence 
        t.isIdentical = tmp.data.isIdentical
    end
    return t
end

function download_vdo()
    print('download_vdo')
    local dir = '/home/creden/work/app/client/vdo/ex/'
    local res, err = prog('wget', body.vdo.url, '-O', dir .. body.txid .. '.webm')
end

function chk_vdo()
    if body.vdo and body.vdo.url and string.match(body.vdo.url, "http") then
        download_vdo()
        body.vdo.origin_url = body.vdo.url
        body.vdo.url =   'https://creden.co/vdo/test/' ..body.txid..'.webm'
        --TODO1 check audio
        local vdo_req = {vdo=body.vdo, txid=body.txid, compCode=body.compCode, urlPrefix='https://test.hjkl.ninja'}
    	local res, err = prog('curl', '-X' , 'POST', 'https://test.hjkl.ninja/capi/add_vdo_queue', '-d', cjson.encode(vdo_req))
	    --TODO2  check audio
        local audio_cmd = "lua /home/creden/work/app/server/capi/submit_vdo_ntl.lua '"..cjson.encode(vdo_req).."' &"
        os.execute(audio_cmd)
    end
end

function downloadImgs()
    local cnt = tostring(rc:incr('kyc.counter'))
    local dir = '/home/creden/work/app/client/face/images/card/'
    for i, k in ipairs({'cardImg1', 'cardImg2' , 'cardImg3'}) do
        local v = getVal(k)
        if(v)then
            local filename = cnt..k 
            chk = string.sub(v, 1, 4)
            if string.match(chk, "http") then
                local cmd = 'wget '.. v ..' -O ' .. dir .. filename .. '.png'
                os.execute(cmd)
            else
            local tmp = 'tmp.kyc.img' .. filename   
            rc:set(tmp , v)
            local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
            os.execute(cmd)
            rc:del(tmp) 
        end     
  
        body[k] = filename .. '.png'
        rc:lpush('q_move_card',filename)
      end
    end
    body.cardImg = body.cardImg1
end


    while true do
        os.execute("sleep 3")
        len = rc:llen('api.kyc.active')
        for i=1, len do
            local data = rc:rpoplpush('api.kyc.active', 'api.kyc.complete')
            if data then
                print(data)
                body = cjson.decode(data)
                conn = mongo()
                conn:set_timeout(1000)
                ok, err = conn:connect('127.0.0.1',27017)
                if err then
                    ret = {success = false, error_msg = err}
                else
                    db = conn:new_db_handle("dopa")
                    col = db:get_col("ekyc")
                    body.channel = 'api'
                    body.createDate = os.date("%x")
                    body.createTime = os.date("%X")
                    body.createdDtm = bson.get_utc_date(os.time() * 1000)
                    body.compCode = get_cust_code(body.username)
                    if chk_kyc() then
                        downloadImgs()
                        chk_face()
                        chk_amlo()
                        chk_vdo()
                    end
                    chk_is_valid(body.isValidDopa, body.isValidAML, body.isFaceIdentical)
                    body.username = nil
                    body.password = nil
                    selector = {txid=body.txid, compCode = body.username}
                    update = {["$set"] = {isValid = body.isValid, isValidDopa = body.isValidDopa, isValidAML = body.isValidAML, isFaceIdentical = body.isFaceIdentical, faceConfidence = body.faceConfidence, remarks = body.remarks, process_status = body.process_status}}
                    n, err = col:update(selector, update, 0, 0, true)
                    if not n then
                        ret = {success = false, error_msg = err}
                    else
                        ret = {success = true, error_msg = err, txid = body.txid , usr = body.compCode}
                        print(cjson.encode(ret))
                    end
                end
            end
        end
        print('no queue')
    end
