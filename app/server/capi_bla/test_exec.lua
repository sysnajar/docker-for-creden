#!/usr/bin/env lua
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

local res, err = prog('pdftk', '/home/creden/work/app/client/face/images/card/1540130577.pdf',  'dump_data'  )

prog.stdin = res.stdout
res,err = prog('grep', 'NumberOfPages')
prog.stdin = nil

print(res.stdout:match('%d+'))
