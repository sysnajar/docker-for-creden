#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = { company_id = arg[1]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
local ret = {}
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("document_type")
    list = { company_id = body.company_id or "N/A"}
    l,err = col:find(list)

    if l then
            local list_doc_type = {}
            for i,v in l:pairs() do 
                v._id = tostring(v._id)
                -- print(i,v.name_doc_type)
                table.insert(list_doc_type,v)
            end
        ret = { data = list_doc_type or {}}

    else
        ret = { data = {} }
    end
print(cjson.encode(ret))
end





