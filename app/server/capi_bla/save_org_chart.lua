local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local exec = require'resty.exec'
local rc = redis.connect( '127.0.0.1', 6379)
---------------------MONGODB FUNCTIONS----------------------------------
--return new mongodb context
function mongo_ctx()
    local ret = {}
	local conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)

    if err then
	--	assert(false)
    else
		ret.dopa = conn:new_db_handle("dopa")
		ret.edoc = conn:new_db_handle("edoc")
		local col = {}
		local mt  = {}

		mt = {__index = function(t, k)  
			local db = (string.find(k, 'dopa.')==1) and ret.dopa or ret.edoc
			local i = string.find(k, '%.')
			local col_name = (i==nil) and k or (k:sub(i+1))
			return db:get_col(col_name)
		    end
		    }
	     
		 setmetatable(col, mt)
		 ret.col = col
		 ret.by_id = function(col_name, _id)	 
		 return by_id( ret.col[col_name], _id ) --call global function  by_id()
		 end
     end

    return ret		 
end

function convertHexStringToNormal( str )
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end

function by_id(col, _id)
  if(type(_id == string)) then
     _id = object_id.new(convertHexStringToNormal(_id))
  end

  local res = col:find_one({_id = _id})
  return res
end

function mid(id)
 assert(id~=nil,"ID can not be nil")
 return object_id.new(convertHexStringToNormal(id))
end

--object_id.new(convertHexStringToNormal(node.title_id))

function remove_node_in_collections(node_id)
  assert(node_id~=nil,"cannot remove node from collection, node_id can not be nil")
  local col = ctx.col['dopa.company_members']
  local cmd = {["$pull"] = {positions={node_id = node_id}}}
  u, err = col:update({}, cmd, 0, 1, true) --upsert,multi,safe
  ngx.log(ngx.INFO, 'removed node#'.. node_id ..' in collections')
end

function insert_node_to_collections(node, company_id)
  ngx.log(ngx.INFO, 'insert_node_to_collections nodd = '.. cjson.encode(node))
  ngx.log(ngx.INFO, 'company_id'.. tostring(company_id))
  local col = ctx.col['dopa.company_members']
  if(node.person_id and node.title_id)then
	  local pos = { node_id = node.id, position_name = node.title,
	  position_id = mid(node.title_id) }

      local cmd = {["$push"] = {positions = pos}}
      u, err = col:update({
	  esig_user_id = mid(node.person_id),
	  company_id   = mid(company_id)
	  }, cmd, 0, 0, true)

      ngx.log(ngx.INFO, 'inserted node '.. tostring(node.id)  ..' in collections')
  end
end

ngx.req.read_body()
print = ngx.say
json = ngx.req.get_body_data()
body = cjson.decode(json)


ret = {success = false}

-- CHECK SESSION
if not session.data.company_id then
    ret.error_msg = 'Invalid Data'
    print(cjson.encode(ret))
    return 0
else
    body.company_id = session.data.company_id
end

if(not body.company_id or not body.nodes or #body.nodes<1)then 
   ret = {success = false, error_msg = "Invalid data"}
else
   if(body.updated_node)then 
	 ctx = mongo_ctx()  
	 remove_node_in_collections(body.updated_node.id)
     insert_node_to_collections(body.updated_node, body.company_id)
   end

   if(body.removed_node_id)then 
	 ctx = mongo_ctx()  
	 remove_node_in_collections(body.removed_node_id)
   end

   local key = "orgchart." .. body.company_id
   rc:set(key, json)
   ret.success = true
   ngx.log(ngx.INFO, 'saved company orgchart ' .. key)
end

print(cjson.encode(ret))
