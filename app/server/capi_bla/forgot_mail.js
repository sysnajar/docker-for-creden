var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = process.env.HOST_NAME + "/esign/images/Creden_logo.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var link_forgot = process.env.HOST_NAME + "/esign/web/forgot.html?email=" + process.argv[2] + "&code=" + process.argv[3]
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/esign/web/policy.html"
  var html = `
  <!DOCTYPE html>
  <html lang="th-TH" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
  </head>
  <body style="background: #E0E0E0;"><br>
    <table border="0" cellspacing="0" cellpadding="0" style="background: #ffffff;margin: 0px auto 15px auto;padding: 10px 20px;width:`+ wTable +`;">
      <tr>
          <td>
            <div style="max-width: 160px;">
              <img src="`+ logo +`" alt="logo website" style="width: 100%;">
           </div>
          </td>
      </tr>
      <tr>
        <td style="width: 100%;text-align: center;color: #ffffff;padding: 0px 30px 50px 30px;background: `+ bg +`;">
          <div>
            <img src="`+ process.env.HOST_NAME +`/esign/images/mail/mail_forgot.png" style="margin-top: 30px;">
          </div>
          <div style="margin-top:15px;font-size: 16px;">
            Click the button to reset your password.
          </div>
          <div style="margin-top:10px;font-size: 16px;">
            กรุณาคลิกปุ่มเพื่อรีเซ็ตรหัสผ่านของคุณ
          </div>
          <div style="margin-top:30px;">
            <a href="`+ link_forgot +`" style="text-decoration: none;color: #4F4F4F;background: #F2C94C;padding: 10px 30px;font-size: 16px;">
              รีเซ็ตรหัสผ่าน
            </a>
          </div>
        </td>
      </tr>
      <tr>
        <td style="color: #222222;font-size:12px;">
          <div style="margin-top:15px">
          โปรดป้อนรหัสการเข้าถึงนี้สำหรับการเข้าถึงบัญชี Creden.co ของคุณ ด้วยเหตุผลด้านความปลอดภัย อย่าแชร์รหัสนี้แก่บุคคลอื่น 
          <br><br>
          ไม่ได้ขอรหัสนี้? เข้าสู่โปรไฟล์ Creden.co ของคุณและอัปเดตรหัสผ่านของคุณ
          </div>
          <div style="margin-top:30px;width:30%;">
            <hr style="width:100%;border: 1px dashed #000000">
          </div>
          <div style="margin-top:30px;margin-bottom: 10px;">
            Please enter this unique access code into Creden.co  for your login. <br>
            For security purposes, do not give out this access code to anyone.
          
            <br><br>
            If you didn't request a password code reset, please log in to your Creden account and change your password immediately.
          </div>
        </td>
      </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="0" style="margin:auto;padding: 10px 20px;color: #222222;font-size:12px;width:`+ wTable +`;">
    <tr>
      <td>
        <div>
          <b>หมายเหตุ</b> : โปรดอย่าตอบกลับอีเมลฉบับนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
        </div>
        <div style="margin-top:5px">
          หากต้องการสอบถามข้อมูลเพิ่มเติมโปรดติดต่อฝ่ายบริการลูกค้า <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
        </div>
        <div>
          <hr style="width: 100%;margin-top: 20px;background: #BDBDBD;">
        </div>
        <div style="margin-top:15px">
          <b>Note</b> : This is an automated message. Please do not reply directly to this email.
        </div>
        <div style="margin-top:5px;margin-bottom: 5px;">
          For more information, please contact the application's support team: <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
        </div>
      </td>
    </tr>
  </table>

  <table border="0" cellspacing="0" cellpadding="0" style="margin:0px auto auto auto;color: #000000;font-size:12px;width:100%;background: #F0F0F0;">
    <tr style="background: #FFF;"><td style="height: 10px"></td></tr>
    <tr>
      <td style="text-align:center;padding-top:15px;">
        Creden Asia Co., Ltd. 9/22 Ratchapruek Road, Bang Ramat Subdistrict, Taling Chan District 10170, <span style="color: #2F80ED">095-374-8973</span>
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding-top: 10px;">
        <a target="_blank" style="text-decoration: none;" href="`+ home +`">www.creden.co</a> 
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding-top: 10px;">
        <a target="_blank" style="text-decoration: none;" href="`+ policy +`">Privacy Policy</a> 
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding: 10px 0px 15px 0px;">
        © 2021, Creden Asia Co., Ltd. All Rights Reserved 
      </td>
  </tr>
  </table>
  </body>
  </html>
  `

  var smtp = {
    // host: 'smtp.gmail.com', //set to your host name or ip
    host: 'smtp.mailgun.org', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      // user: 'credenmailer@gmail.com', //user account
      // // pass: 'creden2018' //user password
      // pass: 'dnkrakkaxnmnakpr'
      user: 'postmaster@mg.creden.co', //user account
      pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
    to: process.argv[2], //to email (require)
    cc: '',
    subject: '[Creden] Reset Password', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


