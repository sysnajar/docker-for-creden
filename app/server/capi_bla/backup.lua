#!/usr/bin/env lua
local cjson = require 'cjson'
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    print = ngx.say
    body = ngx.req.get_uri_args()
else
    body  = {}
end

local mongo = rc:get('backup_mongo_'..body.period)
local redis = rc:get('backup_redis_'..body.period)
local img = rc:get('backup_img_'..body.period)

local ret = {mongo = mongo, redis = redis, img = img}

print(cjson.encode(ret))