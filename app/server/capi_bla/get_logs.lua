#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {adminEmail = arg[1],login_service=arg[2],description=arg[3]}
end
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	db = conn:new_db_handle("dopa")
	col = db:get_col("listlog")
	-- s = col:find({username = body.adminEmail})
	-- data = {}
	-- for k, v in s:pairs() do
	-- 	    v._id = tostring(v._id)
	-- 	    table.insert(data, v)
	-- end

	s = col:find({username = body.adminEmail,login_service=body.login_service,description=body.description})
	data = {}
	if login_service=="esig" then
		for k, v in s:pairs() do
		    v._id = tostring(v._id)
		    table.insert(data, v)
		end
	else
		for k, v in s:pairs() do
		    v._id = tostring(v._id)
		    table.insert(data, v)
		end
	end
	print(cjson.encode({success = ok, data = data}))
end
