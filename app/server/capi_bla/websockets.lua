#!/usr/bin/env lua
local copas = require'copas'
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

-- create a copas webserver and start listening
local server = require'websocket'.server.copas.listen
{
  -- listen on port 8080
  port = 8089,
  -- the protocols field holds
  --   key: protocol name
  --   value: callback on new connection
  protocols = {
    -- this callback is called, whenever a new client connects.
    -- ws is a new websocket instance
    creden = function(ws)
      while true do
        local message = ws:receive()
        if message then
          while true do  
            print('ms\n',message)  
            res = rc:get(message)
            if res then
                print('Found Redis data : ', #res)
                rc:lrem('active_jobs', 0, message)
                rc:lpush('complete_jobs', message) 
                ws:send(res)
                --send complete
                print('del complete_jobs: ', message)
                rc:lrem('complete_jobs', 0, message)
                rc:del(message)
                return
            else
                print('wait\n',message)  
                os.execute('sleep 10')
            end
          end
        else
           print('no key\n',message)
           ws:close()
           return
        end
      end
    end
  }
}

-- use the copas loop
copas.loop()
