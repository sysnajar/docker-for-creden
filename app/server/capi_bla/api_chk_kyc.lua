#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

function trim5(s)
  return s:match'^%s*(.*%S)' or ''
end

--todo cust_code may not == username in the future
function chk_pwd(usr, pwd)
    local map = {credendemo = 'creden@demo'}
    
    if usr ~= nil and pwd ~= nil and map[usr] == pwd then
        return true
    end
        local chk = nil
        local conn = mongo()
        conn:set_timeout(1000)
        local ok, err = conn:connect('127.0.0.1',27017)
        if not err then
            local db = conn:new_db_handle("dopa")
            local col = db:get_col("company")
            local s = col:find_one({adminEmail = usr, encryptPass=md5.sumhexa(pwd)})   
            if s then chk = true end
        end
        return chk
end

--todo cust_code may not == username in the future
function chk_apiKey(apiKey)
    local chk = nil
    local conn = mongo()
    conn:set_timeout(1000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if not err then
        local db = conn:new_db_handle("dopa")
        local col = db:get_col("company")
        local s = col:find_one({apiKey = apiKey})   
        if s then
         chk = true 
         body.username = s.adminEmail
         if body.username == "creden" then
            body.createDate = os.date("%x")
            body.createTime = os.date("%X")
            rc:lpush('hack.kyc.api', cjson.encode(body))
            print("")
            ngx.exit(ngx.HTTP_OK)
         end
        end
    end
    return chk
end



function delete_data(col, selector)
assert(selector, "Selector can not be nil!!!")
ngx.log(ngx.NOTICE, 'delete_data = ' .. cjson.encode(selector))

local t = {}
-- t.cardNo     = "N/A" DO NOT RESET WE NEED THIS
t.lnameTH    = "N/A"
t.fnameTH    = "N/A"
t.dob		 = "N/A"
t.laserCode  = "N/A"
t.address    = "N/A"
t.province   = "N/A"
t.cardImg1   = ""
t.cardImg2   = ""
t.cardImg3   = ""
t.call_id    = ""
t.vdo   = ""
t.is_exported  = true 
t.exported_dtm = os.time() * 1000

update = {["$set"] = t}
local n, err = col:update(selector, update, 0, 0, true)
end


if(ngx)then
    ngx.save_log({desc = "api_chk_kyc"})  
    ngx.req.read_body()
    print = ngx.say
  body  = ngx.req.get_post_args()
    header = ngx.req.get_headers() or {}
else
   body   = {username='creden', password='135790', txid=arg[1]} 
end

    conn = mongo()
    conn:set_timeout(10000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")
      if(chk_apiKey(tostring(header.apiKey)) and body.txid == "3001") then
                ret = {isValid= false, isValidDopa = false,isFaceIdentical= true,faceConfidence= 0.5942,isValidLiveness= false,status= "finished",remarks= "สถานะไม่ปกติ => ไม่พบเลข Laser จาก PID นี้"}
      elseif(chk_apiKey(tostring(header.apiKey)) and body.txid == "3002") then
              ret = {isValid= true, isValidDopa = true,isFaceIdentical= true,faceConfidence= 60,status= "finished",remarks= "สถานะปกติ"}

      else
        if(chk_apiKey(tostring(header.apiKey))) then
			   local selector = {txid = (body.txid or 'x'), compCode = body.username}
               t, err = col:find_one(selector)

           body.username = nil
           body.password = nil
           if not t then
                  ret = {success = false, error_msg = err or 'txid not found' , txid = body.txid}
               else
                  if t.livenessResult then 
                    t.livenessResult = {score = t.livenessResult.score, maxScore = t.livenessResult.maxScore, isFaceVerified = t.livenessResult.isFaceVerified} 
                  end
                  --ret = {success = true, error_msg = err, txid = body.txid , remarks = t.remarks, isValid = t.isValid, isValidDopa = t.isValidDopa, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, isValidLiveness = t.isValidLiveness, livenessResult = t.livenessResult,status = t.process_status}
                  if(header.apiKey == "ic0tyUs8psYbDxs20o24saikx" or header.apiKey == "ic0tyUs8psYbDp59e5xWcapx234s") then

                    ret = {success = true,card={cardNo = t.cardNo,lnameTH=t.lnameTH,fnameTH=t.fnameTH,dob=t.dob},dbd_results=t.dbd_results,amlo_code=t.amlo_code,amlo_desc=t.amlo_desc,isValidAML=t.isValidAML,card_img1=t.cardImg1,card_img2=t.cardImg2,card_img3=t.cardImg3, error_msg = err, txid = body.txid , remarks = t.remarks, isValid = t.isValid, isValidDopa = t.isValidDopa, isValidAML = t.isValidAML, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, isValidLiveness = t.isValidLiveness, livenessResult = t.livenessResult,status = t.process_status, is_exported = t.is_exported, exported_dtm = t.exported_dtm}
                  
		   local del_data   = false
		   --local del_ttl    = 60
		   local del_ttl    = 60*60*12

           if(t.channel=='sdk')then
              if(#t.cardImg1>3)then
              ret.cardImg1    = "https://hjkl.ninja/face/images/card/" .. t.cardImg1
              ret.cardImg1b64 = rc:get('b64.img1.' .. t.txid) 
              end

              if(#t.cardImg2>3)then
              ret.cardImg2 = "https://hjkl.ninja/face/images/card/" .. t.cardImg2
              ret.cardImg2 = string.gsub(ret.cardImg2, '_out', '')

              ret.cardImg2b64 = rc:get('b64.img2.' .. t.txid) 

              end

              if(#t.cardImg3>3)then
               if string.find(body.txid,'card') then
              ret.selfieDipChipImg = "https://hjkl.ninja/face/images/card/" .. t.cardImg3
              ret.selfieDipChipImgb64 = rc:get('b64.imgdipchip.' .. t.txid)
              else
              ret.cardImg3 = "https://hjkl.ninja/face/images/card/" .. t.cardImg3
              ret.cardImg3b64 = rc:get('b64.img3.' .. t.txid) 
              end
              end

              if(t.call_id and #t.call_id>3)then
              local tmp_photo1 = '/home/creden/work/app/client/vdo/ex/' .. t.call_id ..'.webm'
              ret.vdo = "https://hjkl.ninja/vdo/ex/" .. t.call_id .. '.webm'
              ret.vdob64 = rc:get('b64.vdo.' .. t.txid)  
              end  
            end

            if(t.channel=='api')then
             
            end            


			if(ret.cardImg1b64)then

			  if(rc:ttl('b64.img1.'..t.txid)<0)then
			     rc:expire('b64.img1.' .. t.txid, del_ttl)	
			     rc:expire('b64.img2.' .. t.txid, del_ttl)	
			     rc:expire('b64.img3.' .. t.txid, del_ttl)	
			     rc:expire('b64.vdo.'  .. t.txid, del_ttl)	
				 del_data = true
			  end

			end

			if(ret.selfieDipChipImgb64)then

			  if(rc:ttl('b64.imgdipchip.'..t.txid)<0)then
			     rc:expire('b64.imgdipchip.' .. t.txid, del_ttl)	
				 del_data = true
			  end

			end

				-- Discard Data
				if(del_data)then
					delete_data(col, selector)
				end
				-- END


                  else
                    ret = {success = true, error_msg = err, txid = body.txid , remarks = t.remarks, isValid = t.isValid, isValidDopa = t.isValidDopa, isValidAML = t.isValidAML, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, isValidLiveness = t.isValidLiveness, livenessResult = t.livenessResult,status = t.process_status}

                  end
                  -- ret = {success = true, error_msg = err, txid = body.txid , remarks = t.remarks, isValid = t.isValid, isValidDopa = t.isValidDopa, isValidAML = t.isValidAML, isFaceIdentical = t.isFaceIdentical, faceConfidence = t.faceConfidence, isValidLiveness = t.isValidLiveness, livenessResult = t.livenessResult,status = t.process_status}
                  if tostring(body.details) == 'true' then
                    ret.cardNo = t.cardNo
                    ret.lnameTH = t.lnameTH
                    ret.fnameTH = t.fnameTH
                  end
                end
         else
           ret = {success = false, error_msg = 'Invalid Login'}
        end
      end
    end

   print(cjson.encode(ret))
   
