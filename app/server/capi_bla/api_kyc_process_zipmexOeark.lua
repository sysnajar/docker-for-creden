#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local prog_node = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"
prog.timeout_fatal = false
prog_node.timeout = 1000 * 80
prog_node.timeout_fatal = true
local load_h = prog('lua', '/home/creden/work/app/server/capi/h_name.lua')
local HOST_NAME = load_h.stdout
print(load_h.stdout)
HOST_NAME = HOST_NAME:gsub("%\n", "")

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
-- rc2 = redis.connect('54.254.182.56', 6379)
print(cjson.encode(body))
-- rc2:auth('noRedisPass1010')
body = {}

function trim5(s)
    return s:match'^%s*(.*%S)' or ''
end
  
function getVal(key)
    local ret = body[key]
    ret = ret and trim5(ret) or nil
    if(ret=='')then ret = nil end
    return ret
end
  
function get_cust_code(usr)
    return usr
end


function chk_led(body)
	local ret  = false
	local desc = nil
    local end_time_led = 0
--	assert(body.ref_no  , "Invalid ref_no")
--	assert(body.idcard  , "Invalid cardNo")
    print('goto LED')

    print('ooooo')
  --  os.execute('sleep 5')
  local cnt1 = tostring(rc:incr('zpimex_in'))
  local start_time =(os.time())
  

 
  local text1 = "LED#"..cnt1

 -- local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[Start] "..start_time)

	local res1,err1 = prog_node("node", "/home/creden/work/app/server/capi/auto_LED_bot.js", body.ref_no, body.idcard , "true")
    local end_time = (os.time())
    end_time_led = ((end_time - start_time ))
    --os.execute('sleep 1')

   -- local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[end] "..total_time)

    
    print(err1)
    if(tostring(err1) == 'timeout') then
        local a = tostring(rc:incr('kyc.zipmex.fail'))
        
        local res3,err3 = prog("sh", "/home/creden/work/app/server/capi/pkill_chrome_pid_node.sh")
       -- local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[error] "..total_time)
    end

    print('go to update')
    local res2,err2 = prog("lua", "/home/creden/work/app/server/capi/update_LED_text_API.lua", body.ref_no)
    

    if ( (not err1) and (not err2) ) then
	local led_res = tostring(res2.stdout)    
     	print('LED  result = ' .. led_res)
	  local tmp = cjson.decode(led_res)
      
	  desc   = (tmp and tmp.data_LED and tmp.data_LED[1]) and tostring(tmp.data_LED[1].title) or 'N/A'
     
     -- local index    = desc:find('ไม่พบรายการ')
      
	  if(desc ~= 'N/A') then ret = true end
    else
	
	end

    print(desc)
    if(desc == nil) then
        local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[GET LED result =] end_time="..end_time_led..'===='..tostring(ret).." == N/A ===== ")
        print("end_time"..end_time_led)
    else
        local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[GET LED result =] end_time="..end_time_led..'===='..tostring(ret).." == "..desc or 'N/A'.." ===== ")
        print("end_time"..end_time_led)
    end


    local rc_check = rc:ttl('LEDstart.'..body.ref_no)
    if(tonumber(rc_check) > 0) then


    else
        local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[GET LED overtime =] end_time="..end_time_led..'===='..tostring(ret).." == ".. tostring(desc) or 'N/A'.." ===== ")
        
    end

     --local desc2 = desc:sub(1,15)

    
   --  local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[GET LED result =]"..tostring(ret).." == "..desc or 'N/A'.." ===== "..total_time)
	return ret, desc
end


function chk_amlo()
    local isValidAML = false
    local ret_amlo = {}
    local card_no = body.idcard
    local fnameTH = body.firstname
    local lnameTH = body.lastname
    local res, err = prog('lua', '/home/creden/work/app/server/capi/chk_amlo2.lua', card_no,fnameTH,lnameTH)
    if not err then
      tmp = cjson.decode(res.stdout)
      ret_amlo.isValidAML = tmp.isValidAmlo
      ret_amlo.amlo_desc = tmp.amlo_desc
      ret_amlo.amlo_code = tmp.amlo_code
    else
    ret_amlo.isValidAML = false 
    ret_amlo.amlo_desc = false
    ret_amlo.amlo_code = false
    end 
    -- body.isValidAML = isValidAML
    return cjson.encode(ret_amlo)
end
function chk_kyc()
    local result = nil
    local params   = 'national_id=' .. (body.cardNo or '') ..'&first_name=' .. (body.fnameTH or '') .. '&last_name=' .. (body.lnameTH or '') .. '&date_of_birth=' ..  (body.dob or '')  .. '&cvv_code=' ..(body.laserCode or '')   
    --local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')
    local dob = body.dob:sub(7,10)..body.dob:sub(4,5)..body.dob:sub(1,2)


    print("xxxx"..body.firstname,body.lastname, body.dob, body.lasercode)
    local res, err = prog('sh', '/home/creden/work/app/server/capi/check_cardbylaser_proxy1.sh', body.idcard, checkstring(body.firstname), checkstring2(body.lastname), dob, body.lasercode)

    if(not err)then
    print('dddddko['..res.stdout..']')    
        local tmp = xml_to_table(res.stdout)
        print('xml_to_table='..cjson.encode(tmp))
            if(tmp) then

                if tostring(tmp.status) == 'true' and tostring(tmp.code) == "0" then
                    result = tmp.status
                    body.status = 'pass'
                    body.remarks = tmp.remarks 
                else
                    if(tostring(tmp.code) ~= "0") then
                    body.status = 'fail'
                    body.remarks = tmp.remarks 
                    end
                   
                end             
            isValidDopa = tmp.status
            end 
    end
   
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
    return result
end

function xml_to_table(xml)
local status  = false
local remarks = ''
local code = ''
rc:set('last_xml', xml)

if( string.find(xml, "<IsError>false") ) then status = true  end

local i1 =  string.find(xml, "<Desc>") 
local i2 =  string.find(xml, "</Desc>")
local i3 =  string.find(xml, "<Code>") 
local i4 =  string.find(xml, "</Code>")

if(i1==nil or i2==nil) then
    code = '5'
    remarks = 'ข้อมูลที่ใช้ในการตรวจสอบไม่ครบ'
  --  remarks = 'Dopa Error:' .. tostring(xml)
else
    code = string.sub(xml,i3+6,i4-1)
    remarks = string.sub(xml , i1+6 , i2)
end
    


local ret = { status = status, remarks = remarks,code = code}
return ret
end

function checkstring2(value)
    if(not value) then
         return ""
    else
        if(string.find(value,"%s+") == nil) then
            return value
          else
            value = string.gsub(value,"%s+","")
          end
       --  value = string.gsub(value,"%s+","")
    
       return value
    end
end
function checkstring(value)
 if(not value) then
      return ""
 else
    if(string.match(value,"นาย ")) then

        value = string.gsub(value,"นาย ","")
        print('string.match case นาย')
    elseif(string.match(value,"นาย")) then

        value = string.gsub(value,"นาย","")
        print('string.match case นาย')  

    elseif(string.match(value,"น.ส. ")) then

        value = string.gsub(value,"น.ส. ","")
        print('string.match case น.ส. ')
    elseif(string.match(value,"น.ส ")) then

        value = string.gsub(value,"น.ส ","")
        print('string.match case น.ส ')
    elseif(string.match(value,"นส ")) then

        value = string.gsub(value,"นส ","")
        print('string.match case นส ')
    elseif(string.match(value,"ว่าที่ ร.ต. ")) then

        value = string.gsub(value,"ว่าที่ ร.ต. ","")
        print('string.match case ว่าที่ ร.ต. ')
    elseif(string.match(value,"นาง ")) then

        value = string.gsub(value,"นาง ","")
        print('string.match case นาง')
    elseif(string.match(value,"หม่อมหลวง ")) then

        value = string.gsub(value,"หม่อมหลวง ","")
        print('string.match case หม่อมหลวง')
    elseif(string.match(value,"นางสาว ")) then

        value = string.gsub(value,"นางสาว ","")
        print('string.match case นางสาว')
    elseif(string.match(value,"Mr ")) then

        value = string.gsub(value,"Mr ","")
        print('string.match case Mr')
    elseif(string.match(value,"Miss ")) then

        value = string.gsub(value,"Miss ","")
        print('string.match case Miss')

    elseif(string.match(value,"นางสาว ")) then

        value = string.gsub(value,"นางสาว ","")
        print('string.match case นางสาว')

    else
    --   if(string.find(value,"%s+") == nil) then
    --     return value
    --   else
    --   value = string.gsub(value," ","%20")
    --   end
       if(string.find(value,"%%s+") == nil) then
           return value
         else
         value = string.gsub(value," ","%20")
         end
    end
    return value
 end
end
function download_vdo()
    local dir = '/home/creden/work/app/client/vdo/ex/'
    local res, err = prog('wget', body.vdo.url, '-O', dir .. body.txid .. '.webm')
end

function chk_vdo()
    
    local vdo_dir = {}
    vdo_dir['https://creden.co'] = 'prod'
    vdo_dir['https://test.hjkl.ninja'] = 'test'
    vdo_dir['https://hjkl.ninja'] = 'dev'

    if body.vdo and body.vdo.url and string.match(body.vdo.url, "http") then
        download_vdo()
        -- body.vdo.url =   'https://hjkl.ninja/vdo/'.. vdo_dir[HOST_NAME] .. '/' ..body.txid..'.webm'
        -- print(body.vdo.url)
        --TODO1 check audio
        local vdo_req = {vdo=body.vdo, txid=body.txid,output_name=body.compCode, compCode=body.compCode, urlPrefix=HOST_NAME}
        -- rc2:publish('api.vdo.req', cjson.encode(vdo_req))
        rc:rpush('vdo.queue', cjson.encode(vdo_req))
        -- rc:lpush('api.kyc.active2', cjson.encode(body))

        --TODO2  check audio
        --local audio_cmd = "curl -X POST http://159.65.2.13/capi/submit_vdo2 -d '"..cjson.encode(vdo_req).."' &"
        local audio_cmd = "lua /home/creden/work/app/server/capi/submit_vdo.lua '"..cjson.encode(vdo_req).."' &"
        os.execute(audio_cmd)
        
    end
end
function chk_is_valid(isValidDopa, isValidAML, isFaceIdentical)
    if body.vdo and string.match((body.vdo.url or ''), "http") then
        body.isValid = false
        body.isValidLiveness = 'processing'
        body.process_status = 'processing'
    else
        if tostring(isValidDopa) == 'true' and tostring(isValidAML) == 'true' then
            body.isValid = true
        else
            body.isValid = false
        end 
        body.process_status = 'finished'
    end
end

function chk_face()
  t = {}
  local faceId1 = ''
  local faceId2 = ''
  if getVal('photoidcard') and getVal('photoselfie') then
    faceId1 = detect_face(body.photoidcard:gsub('.png',''))
    faceId2 = detect_face(body.photoselfie:gsub('.png',''))
    t = verify_face(faceId1, faceId2)
    body.faceConfidence = (t.faceMatchPercent or 0)
    body.isFaceIdentical = (t.isIdentical or false)
  end
end

function detect_face(img)
    print('detect_face')
    print(img)
    local faceId = ''
    local res, err = prog('lua', '/home/creden/work/app/server/capi/detect_face.lua', img)
    if not err then
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
        faceId = tmp.faceId
    end
    return faceId
end

function verify_face(faceId1,faceId2)
    t = {}
    local res, err = prog('lua', '/home/creden/work/app/server/capi/verify_face.lua', faceId1, faceId2)
    if not err then
        print('verify_face')
        print(cjson.encode(res.stdout))
        tmp = cjson.decode(res.stdout)
	if(tmp.data.confidence and tmp.data.confidence >= 0.70) then
            t.faceMatchPercent = tmp.data.confidence 
            t.isIdentical = tmp.data.isIdentical
            else
            t.faceMatchPercent = tmp.data.confidence
            t.isIdentical = false
        end
    end
    return t
end



function rand(len)
local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
local t = {}
local cnt = 0
 
 local i = 1
 repeat
     local f = assert (io.popen (cmd, 'r'))
	 local line = f:read('*all'):match("%d+")
	 f:close()
	 cnt = cnt + #line
	 table.insert(t, line)
 until cnt>len

		
 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
 return ret
end

function downloadImgs()
    redis = require 'redis'
    rc = redis.connect('127.0.0.1', 6379)
    local cnt = tostring(rc:incr('kyc.counter'))
    local dir = '/home/creden/work/app/client/face/images/card/'
    for i, k in ipairs({'photoidcard', 'photoselfie'}) do
        local v = getVal(k)
		local filename = 'api'..cnt..'_'.. rand(8) .. tostring(os.time()) 
        
		if(v)then
            --local filename = cnt..k

            chk = string.sub(v, 1, 4)
            if string.match(chk, "http") then
                local cmd = "wget '".. v .."' -O " .. dir .. filename .. '.png'
                local wget_res = os.execute(cmd)
				
				print('cmd = [' .. cmd ..']')
				print('wget response = '..tostring(wget_res))


            else
            local tmp = 'tmp.kyc.img' .. filename   
            rc:set(tmp , v)
            local cmd = 'redis-cli get '.. tmp ..' | base64 -d > ' .. dir .. filename .. '.png'
            os.execute(cmd)
            rc:del(tmp) 
        end     
  
        body[k] = filename .. '.png'
        rc:lpush('q_move_card',filename)
      end
    end
    body.cardImg = body.cardImg1
end

   function is_valid_led_time(t1)
   local diff = os.time() - t1
   local max  = 60*4
   return diff<=max
   end

    while true do
        os.execute("sleep 3")
        len = rc:llen('api.kyc.active2')
        for i=1, len do
            local data = rc:rpoplpush('api.kyc.active2', 'api.kyc.complete')
            if data then
                body = cjson.decode(data)
                conn = mongo()
                conn:set_timeout(1000)
                ok, err = conn:connect('127.0.0.1',27017)
                if err then
                    ret = {success = false, error_msg = err}
                else
                    db = conn:new_db_handle("dopa")
                    col = db:get_col("ekyc_zipmex")
                    body.channel = 'api'
                    body.createDate = os.date("%x")
                    body.createTime = os.date("%X")
                    body.createdDtm = bson.get_utc_date(os.time() * 1000)
                    body.compCode = get_cust_code(body.username)
                    -- if chk_kyc() then

                        chk_kyc()
                        --downloadImgs()
                        --chk_face()
                        local chk_amlo = chk_amlo()
                       chk_amlo = cjson.decode(chk_amlo)
                       body.amlo_code = chk_amlo.amlo_code
                       body.amlo_desc = chk_amlo.amlo_desc
                       body.isValidAML = chk_amlo.isValidAML
                       local fullname = body.firstname..' '..body.lastname or ''
                       local key = 'LEDstart.'..body.ref_no
                       rc:set(key, body.ref_no)
                       rc:expire('LEDstart.'..body.ref_no, 60*10)

					   local start_led_time = os.time()
					   body.isValidLED,body.led_desc = chk_led(body)
                    --    check loop
                      retry = 0
                       while(is_valid_led_time(start_led_time) and body.isValidLED == false and body.led_desc == 'N/A' or body.led_desc == nil) do
                        retry = retry +1
                        local cnt2 = tostring(rc:incr('zipmex.recheck.led'))
                        local text1 = "LED#"..cnt2
                        

                        print('kill')

                        local res2,err2 = prog("sh", "/home/creden/work/app/server/capi/pkill_chrome_pid.sh")
                        print('end kill')
                    
                        if(retry >= 3) then
                            local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[Recheck]== retry"..retry)
                        end


                          --  local res1,err1 = prog("sh", "/home/creden/work/app/server/capi/script_autoAPI_alert_zipmex.sh", text1.."ref_no="..body.ref_no, "[Recheck] ")

                                body.isValidLED,body.led_desc = chk_led(body)
                         
                       end

                       

                    -- chk_vdo()

                    -- end
                    chk_is_valid(body.isValidDopa, body.isValidAML, body.isFaceIdentical)
                    body.username = nil
                    body.password = nil
                    selector = {txid=body.txid, compCode = body.username}
                    update = {["$set"] = {isValid = body.isValid, isValidDopa = body.isValidDopa,
					isValidAML = body.isValidAML,amlo_code=body.amlo_code,amlo_desc=body.amlo_desc,
					isValidAML=body.isValidAML,dbd_results=body.dbd_results, 
					isFaceIdentical = body.isFaceIdentical, faceConfidence = body.faceConfidence,
					remarks = body.remarks, process_status = body.process_status, update_LED_endtime = bson.get_utc_date(os.time() * 1000) ,
					led_desc = body.led_desc, isValidLED = body.isValidLED
					}}
                    n, err = col:update(selector, update, 0, 0, true)
                    if not n then
                        ret = {success = false, error_msg = err}
                    else
                        local res3,err3 = prog("sh", "/home/creden/work/app/server/capi/pkill_chrome.sh")
                        ret = {success = true, error_msg = err, txid = body.txid , usr = body.compCode}
                        print(cjson.encode(ret))
                    end
                end
            end
        end
        print('no queue')
    end
