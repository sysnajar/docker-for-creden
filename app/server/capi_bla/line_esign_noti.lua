local cjson = require 'cjson.safe'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local noti = {}



noti.push = function(user_id, message)
    local send_message = {
        to = user_id,
        messages = {
            {
                type = "text",
                text = message
            }
        }
    }

    local res, err = prog('sh', '/home/creden/work/app/server/capi/line_esign_send_push.sh', cjson.encode(send_message))
    if err and ngx then
        ngx.log(ngx.NOTICE,cjson.encode(err))

    end
end

noti.get_line_user_id = function(email)
    local ret = false
    local user_id = nil
    local account_key = 'esign_line.'..'connect.'..email..'.*'
    local account_key_list = rc:keys(account_key)
    if(ngx)then
       ngx.log(ngx.NOTICE,cjson.encode(account_key_list))
    end

    if table.getn(account_key_list) == 1 then
        local account_data = cjson.decode(rc:get(account_key_list[1]))
        user_id = account_data.line_user_id
        ret = true
    end
    return ret, user_id
end

noti.message = function(type, params)
    local time_stamp = os.time() + (7 * 3600)
    local date = os.date('%d/%m/%Y@%H:%M', time_stamp)
    local message = ''
    params.secret = params.secret or ""

    if type == 'signer' then
        if params.secret ~= "" then
            message = date .. '\n' .. 'คุณมีเอกสารที่ต้องเซ็นรออยู่ ℹ️' .. '\n' .. params.subject .. ': ' .. params.originalFile ..'\n'.. 'Document Classification : '.. params.secret .. '\n\n' .. 'คลิกลิงก์เพื่อทำการเซ็นหรือเปิดดู 👀 ✍🏻' .. '\n' .. params.url
        else
            message = date .. '\n' .. 'คุณมีเอกสารที่ต้องเซ็นรออยู่ ℹ️' .. '\n' .. params.subject .. ': ' .. params.originalFile .. '\n\n' .. 'คลิกลิงก์เพื่อทำการเซ็นหรือเปิดดู 👀 ✍🏻' .. '\n' .. params.url
        end
    elseif type == 'viewer' then
        if params.secret ~= "" then
            message = date .. '\n' .. params.sender_name .. ' ได้ส่งเอกสารให้คุณดู 👀\n' .. params.subject .. ': ' .. params.originalFile ..'\n'.. 'Document Classification : '.. params.secret ..  '\n\n' .. 'คลิกลิงก์เพื่อทำการเปิดดู 👀' .. '\n' .. params.url
        else
            message = date .. '\n' .. params.sender_name .. ' ได้ส่งเอกสารให้คุณดู 👀\n' .. params.subject .. ': ' .. params.originalFile .. '\n\n' .. 'คลิกลิงก์เพื่อทำการเปิดดู 👀' .. '\n' .. params.url
        end
    elseif type == 'signed' then
        message = date .. '\n' .. 'เอกสารถูกเซ็นเสร็จแล้ว ✅' .. '\n' .. params.subject .. ': ' .. params.originalFile .. '\n\n' .. 'คลิกลิงก์เพื่อทำการเปิดดู 👀' .. '\n' .. params.url
    elseif type == 'cancel' then
        if params.secret ~= "" then
            message = date .. '\n' .. 'เอกสารถูกยกเลิก 🚫' .. '\n' .. params.subject .. ': ' .. params.originalFile ..'\n'.. 'Document Classification : '.. params.secret ..  '\n\n' .. 'หมายเหตุ: ' .. params.remark .. '\n\n' .. 'คลิกลิงก์เพื่อทำการเปิดดู 👀' .. '\n' .. params.url
        else
            message = date .. '\n' .. 'เอกสารถูกยกเลิก 🚫' .. '\n' .. params.subject .. ': ' .. params.originalFile .. '\n\n' .. 'หมายเหตุ: ' .. params.remark .. '\n\n' .. 'คลิกลิงก์เพื่อทำการเปิดดู 👀' .. '\n' .. params.url
        end
    elseif type == 'alert_package' then
        message = '...'
    elseif type == 'chat' then
        message = date .. '\n'.. 'มีคนส่งข้อความหาคุณ📮✉️' .. '\n' .. 'เลขที่เอกสาร: '.. params.id .. '\n\n' .. 'กรุณากดลิ้งค์ด้านล่างเพื่อตอบกลับ 👇🏻' .. '\n' .. params.url
    else
        message = '...'
    end
    return message
end

return noti