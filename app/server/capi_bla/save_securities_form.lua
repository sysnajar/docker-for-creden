#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local body = nil
ret = {success = false}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("securities_form")
    body.createdDtm = bson.get_utc_date(ngx.now() * 1000)
    local i, err_i = col:insert({body})
    if i then
        ret.success = true
    else
        ret.error_msg = err_i
    end
end

print(cjson.encode(ret))
   

