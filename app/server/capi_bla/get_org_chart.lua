local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local session = require "resty.session".open({secret = "4321"})
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_uri_args() 
else
   body  = ''--{token = args[1],otp = args[2]} 
end



ret = {success = false}

-- CHECK SESSION
if not session.data.company_id then
    ret.error_msg = 'Invalid Data'
    print(cjson.encode(ret))
    return 0
else
    body.company_id = session.data.company_id
end

if(not body.company_id)then 
   ret = {success = false, error_msg = "Invalid data"}
else
   local key = "orgchart." .. body.company_id
   local json = rc:get(key)
   if(json)then
      ret = cjson.decode(json)
      ret.is_empty = false
  else
      ret.is_empty = true
  end
   ret.success = true
end

print(cjson.encode(ret))
