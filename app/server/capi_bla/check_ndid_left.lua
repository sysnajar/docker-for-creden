#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil
bson = require "resty-mongol.bson"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})


 function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.email = session.data.username
        body.company_id = session.data.company_id
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    check_session()


    db = conn:new_db_handle("dopa")

	local col_packcert = db:get_col("package_cert")
        local key = {email = body.email or "N/A", status = true, used = false}
	local cert_rec = col_packcert:find_one(key)

    if cert_rec then 
        ret = { success = true, ndid_left = true}
    else
        ret = { success = true, ndid_left = false}
    end


end

print(cjson.encode(ret))
