#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
redis = require 'redis'

function do_blockchain(u)
	local bc_tx = rc:get('bc:' .. tostring(u.id))	
	if(bc_tx and bc_tx~='')then
	  u.bc_url = 'https://steexp.com/tx/' .. bc_tx
	  u.bc_tx = bc_tx
	else
	  u.bc_url = false
	end
end
function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

rc = redis.connect('127.0.0.1', 6379)
print = ngx.say
body = ngx.req.get_uri_args()
local can_read = false
local myusername  = session.data.username

ngx.log(ngx.NOTICE,'SPEED CDECK ', body.id   ,'START', myusername)

--prevent email param
if(not session.data.username and (not body.password or #body.password<1)) then
	print(cjson.encode({success = false, error_msg = "session expired1",code='D01'}))
	ngx.exit(ngx.HTTP_OK)
end	
--prevent email param


ctx   = ctx or common.mongo_ctx()
col = ctx.col["edoc_list"]
-- local db_company_members = conn:new_db_handle("dopa")
-- local col_company_members = db_company_members:get_col("company_members")
col_company_members = ctx.col["dopa.company_members"]  
u, err = col:find_one({id=body.id})
	if u then
		if(body.owner ~= u.owner ) then ret = {success = false, error_msg = "not math owner",code='D00'} end
		if(u.owner == myusername) then can_read = true end
            if body.owner then
                common.activity_history(body.id ,body.owner, 'View', 'ดูเอกสาร', body.owner..' view document '.. u.originalFile, body.owner..' ดูเอกสาร '..u.originalFile, 'View', u.originalFile, nil, nil, nil, nil)
            end
            -- check use_sign_order flag
            if(u.use_signing_order == nil) then
              u.use_signing_order = false
              u.current_order = 1
            end  
            --


                u._id = tostring(u._id)
                u._company = nil
				u.time = (u.createdDtm) and (os.date('%d/%m/%Y %H:%M:%S', tonumber(u.createdDtm)/1000  + (7 * 60 * 60) )) or 'N/A'
				do_blockchain(u) 

                local col2 = ctx.col["dopa.esig_user"]             
				local user_data = col2:find_one({adminEmail = u.owner})   
				local name_ekyc = nil
				-- if (user_data.FNAME and user_data.LNAME) then
				--  	name_ekyc = user_data.FNAME .. ' ' .. user_data.LNAME
				-- end
                -- u.owner_name = name_ekyc or user_data.adminFisrtname or ''
				u.owner_name = user_data.adminFisrtname .. ' '
				if user_data.adminLastname then
					u.owner_name = u.owner_name .. user_data.adminLastname
				end
 
				--owner check
				if(body.owner) then	
                      local deleted_docs = ctx.col['deleted_doc']:find_one({email = "sysnajar@gmail.com", id = tonumber(body.id)})
					  if(deleted_docs) then
                          u.is_deleted = true
						  u.deleted_count = #deleted_docs
					  else
						  u.is_deleted = false
					  end
					  
				end
				--owner check
				--footer
				
			   local map_pos_signers = {}

				--read permission check
				for k, signer in ipairs(u.signers) do
                    if(signer.email and signer.email ~='') then 
						-- specified email address
						if(signer.email==myusername) then can_read = true end 
					else
						-- specified only position_id
						if(signer.position_id and signer.position_id~='') then
					        signer.people_in_position = common.edoc_get_signers_by_position(signer.position_id)
							map_pos_signers[signer.position_id] = signer.people_in_position
						end
					end
				end


				--public link
				if(u.password and body.password and u.password == body.password) then
					can_read = true
				end
				--public link company only
				if (u.secret_level and u.secret_level == 1 or u.secret_level == 2) then
					if(body.iscompany) then
						i_company_members, err = col:find({ company_id = object_id.new(convertHexStringToNormal(session.data.company_id)), email = myusername })
						if i_company_members then
							can_read = true
						end
					end
				end

				for _,p in ipairs(u.signers) do
					local hide_password = true
				  	if(p.is_group)then
						for _,g in ipairs(p.user_group) do
							if(g.email == myusername)then
								can_read = true
								hide_password = false
								break
							else
								ngx.log(ngx.NOTICE,g.email .. ' != ' ..myusername)
							end
						end --for
					else
					    if(p.email == myusername)then
							hide_password = false
						end

				  	end --is group

					if (u.owner == myusername) then
						hide_password = false
						break
					end

					if(hide_password)then  
				  	   p.password = ""
			     	end

				end --for

                for k, v in ipairs(u.fields) do 
                    if v.time and v.time ~= '' and v.time ~= 0 then
                        local format="(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)"
                        local mon,day,year,hour,min,sec = tostring(v.time):match(format)
                        local dt = {year=tonumber('20'..year), month=tonumber(mon), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=tonumber(sec)}
                        v.time2 = os.time(dt)
                        v.time3 = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.time2)  + (7 * 60 * 60) )
                    end

					v.people_in_position = map_pos_signers[v.position_id] 
					if((not can_read) and v.people_in_position)then
                       for _,p2 in ipairs(v.people_in_position) do if(p2.email==myusername) then can_read = true end end
					end
					
                    -- if(v.email==myusername) then can_read = true end
                end --for


				if(can_read==true)then
                   ret = {success = true, error_msg = err, data = u, id = body.id , dtm = os.date('%d/%m', os.time()+(60*60*7)) .. '/20'..os.date('%y', os.time()+(60*60*7))}

					-------
					--check CERT
                   ret = {success = true, have_cert = false, error_msg = err, data = u, id = body.id, dtm = ret.dtm}
                   local user, err = col2:find_one({adminEmail = myusername})
					
				    if(user and user.last_cert_info)then
				       ret.have_cert = true
				    end
				    -- end check CERT

					------

			    else
			       ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to access docid#  ' .. tostring(body.id) )
                end
				--footer
	else
		ret = {success = false, error_msg = "no data" ,code='D00'}
	end
			
	ret.data.password = nil
            
ngx.log(ngx.NOTICE,'SPEED CDECK ', body.id   ,'END', myusername)
print(cjson.encode(ret))
