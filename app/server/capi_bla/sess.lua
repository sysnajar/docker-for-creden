#!/usr/bin/env lua
cjson = require 'cjson.safe'
local session = require "resty.session".open({secret = "4321"})

--read session data
local myusername = session.data.username or ''
local loggedIn = (myusername~='' and myusername ~= 'value of username') and true or false

-- setup output
local ret = {success = true , loggedIn = loggedIn , username = myusername}
 

ngx.say('var user = ' .. cjson.encode(ret))


