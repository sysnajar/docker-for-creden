#!/usr/bin/env lua
local cjson = require 'cjson'
local body = nil
local redis = require 'redis'
local mongo = require "resty-mongol"
local object_id = require "resty-mongol.object_id"

rc = redis.connect('127.0.0.1', 6379)

HOST_NAME =  os.getenv("HOST_NAME") 
mail_subject = "Reset creden eSignature password."
if rc:get('is_ldd') and rc:get('is_ldd')=='true' then
	HOST_NAME = 'https://esign.ldd.go.th'
	mail_subject = "Reset LDD eSignature password."
end
function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end
function get_user_company(email)
local CI_BG = ''
local CI_HEADER = ''
local LOGO = ''
     db = conn:new_db_handle("dopa")
    col2 = db:get_col("company_members")
    chk_s1, chk_err = col2:find_one({email = email}, nil, true)
    if chk_s1 then
    db2 = conn:new_db_handle("dopa")
    col3 = db2:get_col("user_company")
    usr_com,err = col3:find_one({_id=object_id.new(convertHexStringToNormal(tostring(chk_s1.company_id)))})
        if usr_com.template ~= nil then
         CI_BG = usr_com.template.CI_BG
         CI_HEADER = usr_com.template.CI_HEADER
         LOGO = usr_com.template.LOGO
         Status_com = 'true'

         return CI_HEADER,CI_BG,LOGO,Status_com
        else
         CI_BG = '#1667B2'
         CI_HEADER = '#1667B2'
         LOGO = 'https://hjkl.ninja/signature/img/Creden_logos_Logo_C.png'
         Status_com = 'false'

         return CI_HEADER,CI_BG,LOGO,Status_com
        end
    else
         CI_BG = '#1667B2'
         CI_HEADER = '#1667B2'
         LOGO = 'https://hjkl.ninja/signature/img/Creden_logos_Logo_C.png'
         Status_com = 'false'

         return CI_HEADER,CI_BG,LOGO,Status_com
    end
    return 'N/A'
end
if(ngx)then  
    ngx.req.read_body()
	body = ngx.req.get_post_args()
    print = ngx.say
else
   body  = {email = arg[1]}
end

        --update mongo
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        db = conn:new_db_handle("dopa")

        if body.login_service == 'esig' then
        	col = db:get_col("esig_user")
        	 --get existing record
	        old_rec = col:find_one({adminEmail=body.email}, {comStatus = 1})
			if(old_rec) then
				local rand  = tostring(math.random())
				rand = rand:sub(3,7) ..rand:sub(10,12)
				local key = 'resetpwd:' .. body.email 
				local CI_HEADER_1,CI_BG_1,LOGO_1,Status_com = get_user_company(body.email)
       
				local style_logo = "width:300px;height:100;display: block; border: 0px solid #ffffff;"

				rc:set(key, rand)
				rc:expire(key, 60*30)
				
      
				html = [[
				  <style>
  a {
      color:#1667B2;
  }
  .redius{
    border-radius:  30px 30px 0 0;;
  }
  .redius2{
    border-radius:  0 0 30px 30px;;
  }
  .redius3{
    border-radius:  30px;
  }
  
  </style>

				<div style="margin: 0; padding: 0;font-family: 'Prompt', sans-serif;background-color:]]..CI_BG_1..[[;height:100%;">
				 <table border="0"  cellpadding="0" cellspacing="0" style="width:100%">
  <tr>
   <td >
     <table class="redius3" align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border-collapse: collapse;box-shadow: 5px 5px 5px 1px rgba(50,50,50,.1);margin-top: 50px;margin-bottom: 70px;border-radius:  30px;">
       <tr  >
         <td class="redius"  align="center" bgcolor="#ffffff" style="padding: 40px 0 0px 0;border-radius:  30px 30px 0 0;">
         <img src="]].. LOGO_1 ..[[" alt="Creating Email Magic" style="]]..style_logo ..[[" />    
         </td>
       </tr>
       <tr>
         <td bgcolor="#ffffff" style="padding: 0px 30px 40px 30px;border-radius:  0 0 30px 30px;" class="redius2">
           <table border="0" cellpadding="0" cellspacing="0" width="100%">
            <tr>
             <td>
               
              <b>กรุณากดคลิกลิงค์ ใต้ล่างเพื่อทำการเปลี่ยนรหัสผ่าน </b> 
             
             </td>
            </tr>
            <tr>
             <td style="padding: 20px 0 10px 0;">
             <!-- <p>ขอขอบพระคุณเป็นอย่างสูงที่คุณได้ไว้วางใจใช้บริการ Creden ระบบลายเซ็นออนไลน์ของเรา</p> -->
             </td>
            </tr>
            <tr>
              <td style="padding: 0px 0 10px 0;">
               <a href="]]..HOST_NAME..[[/forgot.html?email=EMAIL&code=CODE">click here to reset password</a>

              </td>
             </tr>                    
           </table>
         </td>
       </tr>
       <tr>
 
       </tr>
     </table>
   </td>
  </tr>
 </table>
				</div>

				]]

				email = {to = body.email, subject = mail_subject, key = 'email.reset' .. rand,
				 html = html
				 }

				 email.html = email.html:gsub("EMAIL", body.email):gsub("CODE", rand)

				rc:set(email.key, cjson.encode(email))
				rc:expire(email.key, 60*30)
				cmd = 'node /home/creden/work/app/server/capi/send_mail.js ' .. email.key
				os.execute(cmd)
			end
        else
        	col = db:get_col("company")
        	--get existing record
	        old_rec = col:find_one({adminEmail=body.email}, {comStatus = 1})
			if(old_rec) then
				local rand  = tostring(math.random())
				rand = rand:sub(3,7) ..rand:sub(10,12)
				local key = 'resetpwd:' .. body.email 

				rc:set(key, rand)
				rc:expire(key, 60*30)

				 email = {to = body.email, subject = mail_subject, key = 'email.reset' .. rand,
				 html = [[Here's the link to reset your password within 30 minutes :<br/>
				 <a href="]]..HOST_NAME..[[/forgot.html?email=EMAIL&code=CODE">click here to reset password</a>]]
				 }

				 email.html = email.html:gsub("EMAIL", body.email):gsub("CODE", rand)

				rc:set(email.key, cjson.encode(email))
				rc:expire(email.key, 60*30)
				cmd = 'node /home/creden/work/app/server/capi/send_mail.js ' .. email.key
				os.execute(cmd)
			end
        end   
local ret = {success= true, email_key = email.key}
print(cjson.encode(ret))

