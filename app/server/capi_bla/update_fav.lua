#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local mongo = require "resty-mongol"
local body = nil
prog.timeout_fatal = false
local ret = {success = false, error_msg = ""}
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    -- =json
else
     body = {}
end


conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    -- connect db

    local db_dopa = conn:new_db_handle("dopa")

    local col_history = db_dopa:get_col("fav_logs")
    

    --หาข้อมูลชื่อที่ตรงกัน
    f,err = col_history:find_one({ name = body.name,email=body.email})
    
    
    -- check for duplicate data

    local get_table = {}
    if f then
        selector = {email=body.email,owner=body.owner}
        update = {["$set"] = {fav = false}}
        i, err = col_history:update(selector,update,0,0,true)
        if i then
            ret = {error_msg = true, data = "Remove  success"}
        else
            ret = {error_msg = false , data = "Not remove "}
        end
    else
        ret = {error_msg = "No History"}
    end
    ngx.log(ngx.NOTICE,' ref====='..tostring(body.name))
    ngx.log(ngx.NOTICE,' ref====='..tostring(history.fav))
end
print(cjson.encode(ret))