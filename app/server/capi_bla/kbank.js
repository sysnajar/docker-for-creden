Nightmare = require ('nightmare');
var ret = {success:false, error:null}
var app = {step : 1, acc_count :0, accNames : [], AI : 0 , pageLinks : []}
var app_data = [];
var accounts = [];
var FN  = {}
var TotalMonth = 2
setTimeout(function(){ process.exit(0); }, 600000);

//-------- test redis ------
 function saveToRedis( k , v)
{
 var redis = require('redis')
 var rc  = redis.createClient();
 rc.set(k, v, function(){
   if(IsPublish) {  
    console.log('publishing events') 
      rc.publish(k, v)  
    //after publishing this'rc' can not send non [pubsub] command
     }
   console.log('saved redis key >>> ['+k+'] , you can see result or errors here')
     process.exit(0);

 })
}


//--------------------------

var show = process.argv.length>2 && process.argv[2]=='true'
var waitTimeout = parseInt(process.argv[3])
if(isNaN(waitTimeout))waitTimeout = 7000;

var Key = process.argv[4];
var Username = process.argv[5];
var Password = process.argv[6];
var IsPublish = process.argv[7] == 'true';

console.log('show = '+show)
console.log('waitTimeout = '+waitTimeout)
console.log('Key = '+Key)
console.log('Username = '+Username)
console.log('Password = '+Password)

function handleError(ex)
{
   console.log("++++++++++++++++++++++++++++++++ ERROR +++++++++++++++++++++++++++++++\n"+ ex)  
   var data = {success:false, job_type : 'kbank', key : Key,  error_msg : ex}
   saveToRedis(Key, JSON.stringify(data))
}

const nightmare = Nightmare({ show: show , waitTimeout : waitTimeout });
nightmare.on('console', (log, msg) => {
        console.log(msg)
   })

nightmare.on('page', (type, msg) => {
        console.log("type = "+type + ", msg = "+msg)
   })

function execute()
{
   var NAMES = ['Login', 'Go to Main Page' ,  'Enter statement Page',  'Choose Account & specify date range', 'Grab Data',  'Step6'];
   var name = NAMES[app.step-1];

   console.log("============= Execute [ "+ app.step +" ] "+ name +' ' +JSON.stringify(app));
   var T = (app.step==2)?3000:50
  
   setTimeout(function(){ FN[app.step+""](); }, T)  
}

FN["1"] = function(){
var url = 'https://online.kasikornbankgroup.com/K-Online/login.jsp?lang=TH'
Promise.resolve()
  .then(() =>
     nightmare
     .goto(url)
     .type('#userName', Username) 
     .type('#password', Password) 
     .click('#loginBtn')
     .wait(4000)
     .goto('https://ebank.kasikornbankgroup.com/retail/accountinfo/AccountStatementInquiry.do')
     .then(() => {
        app.step  = 2
        execute()
     })
   )
}
//--------- End FN1

FN["2"] = function()
{
  var url = 'https://ebank.kasikornbankgroup.com/retail/accountinfo/AccountStatementInquiry.do'
  nightmare
  // test
  /*
  .evaluate( ()=> {  document.getElementById('2').click()  })
  .wait(3000)
  .evaluate( ()=> {  document.getElementById('1').click()  })
  .wait(3000)
  */

  // test 
  .goto(url)
  .wait('select[name="accountNo"]') // Look for AccountNo dropdown 
  .evaluate( ()=> {  
    var _accNames = [];
    var dd_acc = document.querySelector('select[name="accountNo"]');
    for(var i=1; i<dd_acc.length; i++)//index 0 is not account_name
      { 
       _accNames.push(dd_acc[i].text);
    }
    return _accNames; 
  })
  .then( (_accNames) => {  
    for(var i=0; i<_accNames.length; i++)
      {
      app.accNames.push(_accNames[i]);
    }

    app.acc_count = _accNames.length; 
    app.step = 3;
    execute();
  }  )  
  .catch( (e) => { handleError('Unnable to reach account statements (code=2) : '+e)})
   
}


FN["3"] = function()
{
  console.log("Entering statements page")

  var url = 'https://ebank.kasikornbankgroup.com/retail/accountinfo/AccountStatementInquiry.do'
  nightmare
  //.goto(url)
  .evaluate( (_url) => {
    document.location.href = _url
  }, url)
  .wait(5000)
  .then( () => { 
    console.log("you should see statement page")
      app_data = [];
      accounts.push({account_no: app.accNames[app.AI] , data: app_data})
    app.step = 4; execute(); 
  
  }  )  
  .catch( (e) => { handleError('Unnable to enter statements page (code=2) : '+e)})

}


FN["4"] = function()
{
  console.log("start FN 4 A") 
  nightmare
  .evaluate( (ai)=> { 
    console.log("FN 4 C")
    var f = document.StatementForm    
    var _computeFromDate = function(yyyy, mm, dd){
        var y = parseInt(yyyy)
        var m = parseInt(mm)-1

        var tmp = new Date(y, m, 1);
        tmp.setMonth(m+1);
        tmp.setHours(-1);


        var lastDateOfLastMonth = tmp.getDate();
        if(dd <= lastDateOfLastMonth) //eg. 15 <= 31 return 15
           return dd;
        else
          return lastDateOfLastMonth //eg. 31 > 30 returnn 30
    }

    if(f.selPrevMonth.length>1) // If we have an option to select month
      { 
        console.log("FN 4 D")
        f.period[2].checked = true; // use Date-Range (01/06/2017 - 30/11/2017)
          selectEnabled(f, "3");  

      //from date
      var yyyymm = f.selPrevMonth[f.selPrevMonth.length-1].value;
      var yyyy  = yyyymm.substring(0, 4)
      var mm    = yyyymm.substring(4)
       
      //[currentDate] is variable from kbank webpage  >> currentDate = new Date(2017,11,3);
      var fromDate = _computeFromDate(yyyy, mm, currentDate.getDate())
      //f.selDateFrom.value = '01/12/2017';//padZeroPrefix(''+fromDate, 2) + '/' + mm + '/' + yyyy
      f.selDateFrom.value = padZeroPrefix(''+fromDate, 2) + '/' + mm + '/' + yyyy
    }
    else
      {
      //todo : maybe we pick Radio0 (current month) then press OK ?
        console.log("FN 4 E")
    }

    console.log("FN 4 F")
    f.accountNo.selectedIndex = ai + 1;
    JumpOnSelect(f, f.accountNo,  false);
    console.log("FN 4 F2 ... will looking for BillPayment link")
  }, app.AI)
  
  //.wait('a[href="javascript:openPrintWindow()"]') // Look for [Print] Link (Link 3 of 3)  
  .wait('a[href="/retail/cashmanagement/BillPayment.do"]') // Look for [BillPayment] Link (Link 2 of3)    
  .wait(5000) //eark debug
  .evaluate( (ai) => { // get page links
      var ret = [];

    if(ai<1){
      console.log("FN 4 G1")
      var aLinks = document.querySelectorAll('a[href^="/retail/accountinfo/AccountStatementInquiry.do?action=sa_detail"]')

      console.log("FN 4 G2")
      console.log('aLinks ' + typeof(aLinks) + aLinks)
      aLinks.forEach(function( a){ 
       console.log("checking>>> " +a)
       ret.push(a.text); 
       console.log(a.href) 
      })
    }
    
    return ret //return array of URL to then()
  } , app.AI) 
  .then( (links) => { 
    console.log("done step 4 , pageLinks count = "+ links.length)

    app.pageLinks = links 
    app.step = 5 
    execute()
  }  )  
  .catch( (e) => { handleError('Error 4 (Choose Account&Date range) : '+e)})
}

FN["5"] = function()
{
  nightmare
  .evaluate(  () => { 
    console.log("FN5") 
    var to_obj = function(row){
          var ret = {};
        var fields = ["date", "desc", "withdraw", "deposit", "balance", "via", "note"];
                for(var i=0;i<7;i++)
        { var c = row.cells[i];
            ret[ fields[i] ] =  c?c.textContent:"n/a";
          }
        return ret;
    };

   var t = document.querySelectorAll('table [rules=rows]')[0] 
   var arr = [].reduce.call(t.rows, function(ret,row){ ret.push(to_obj(row)); return ret } , []);
   return arr;
  })
  .then( (_arr) => { console.log("DONE")
      var firstRow = true;

        _arr.forEach(function(row){
      if(firstRow)
       firstRow = false 
      else
      app_data.push(row)
    })

    console.log('total rows '+ app_data.length)

      if(app.pageLinks.length>0){
       openNextPage(app.pageLinks.shift())
     }else{ 
     app.AI = app.AI+1
     if(app.AI < app.acc_count){
       console.log("Condition check passed"+ app.AI+" < "+ app.acc_count)
         app.step  = 3 // finished this account, do next account
     }
     else{ 
       app.step  = 6 // finished all account
     }
        execute();
     }


  })  
  .catch( (e) => { handleError('Error 5 (Grab Table Data) : '+e)})

}

function openNextPage(P)
{
  //console.log("openNextPage >>> " + P)  
  nightmare
  .evaluate( (p) => {
    var aLinks = document.querySelectorAll('a[href^="/retail/accountinfo/AccountStatementInquiry.do?action=sa_detail"]')
    
    // remove printBtn
    var printBtn = document.querySelector('a[href="javascript:openPrintWindow()"]')
    printBtn.parentNode.removeChild(printBtn);

     
    aLinks.forEach(function(a){
        if(a.text == p)
        { //console.log("Found Link to page "+p+'\n'+a.href);
        document.location.href = a.href; 
      }
    })

  }, P) 
  .wait('a[href="javascript:openPrintWindow()"]') // Look for [Print] Link  
  .then( (_arr) => { console.log("Done openNextPage") ; execute();   }) 
  .catch( (e) => { handleError('Error openNExtPage (Grab Table Data) : '+e)})

}

FN["6"] = function()
{
   console.log("DONE found "+ app_data.length +" entries") 
   var data = {success:true, job_type : 'kbank', key : Key,  data : accounts}
   saveToRedis(Key, JSON.stringify(data))
}

execute()
