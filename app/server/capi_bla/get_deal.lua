#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {} 
end

function get_deal (id)  
   local db_deal = conn:new_db_handle("symphony")
   local deal = db_deal:get_col("deal")
   local list = deal:find_one({ doc_number = id })  
   local d = {}

    if list then
        list._id = nil
        d = { success = true, data = list }
    else
        d = { success = false, data = {} }
    end
    return d

end

-- End fn

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
else
    local deal = get_deal(body.id)
    ret = { success = true, deal = deal}
    -- local session = require "resty.session".start{ secret = "4321" }
    -- local myusername = session.data.username
   
    -- db = conn:new_db_handle("symphony")

    -- if body.whoami == "symphony" then
    --     user = db:get_col("user_symphony")
    -- else
    --     user = db:get_col("user_creden")
    -- end
    
    -- u = user:find_one({ Username = myusername })  
    
    -- if u and body.Username == myusername then

    --     local deal = get_deal(body.id)
    --     ret = { success = true, deal = deal}

    -- else
    --     ret = { success = false, error_msg = "invalid user" }
    -- end
    
end

print(cjson.encode(ret))
