local t = {
ctx = nil,
col = nil
}
t.__index = t

t.default_map_fn = function(rec) rec._id = tostring(rec._id);return rec end

function t:new(col_name, _ctx)
	self.ctx = _ctx or common.mongo_ctx()
	self.col = self.ctx.col[col_name]

	return self
end

function t:get(q)
	return self.col:find_one(q)
end

function t:del(q, _single_remove)
	self.col:delete(q, _single_remove or 1, true) 
end

function t:save(data)
  local col = self.col

  if(not data._id) then 
	  local ts = ngx.now() 
	  data.created_dtm = bson.get_utc_date(ts * 1000)
	  data._tmp = ts
	  local i,err = col:insert({data}, nil, true)
	  return tostring(self:get({_tmp = ts})._id)
  else
     local updated_fields = common.shallow_copy(data, {updated_dtm = bson.get_utc_date(ngx.now() * 1000)})
     updated_fields._id = nil
	 updated_fields.updated_dtm = ngx.now()*1000
	
     local update_cmd = {["$set"] = updated_fields}
     local u, err = col:update({_id = common.object_id(data._id)}, update_cmd, 0, 0, true)
	 return data._id
  end
end

function t:list(q, returnfields, num_each_query)
	local _cur = self.col:find(q, returnfields, num_each_query)
    local ret = {
		cursor = _cur , 
		pairs  = function() return _cur:pairs() end,
		map_pairs  = function(fn) 
		   local ret = {}
		   for _i, v in _cur:pairs() do
               table.insert(ret, ((fn) and fn(v) or t.default_map_fn(v))) 
		   end
		   return ret
		end
	}

    ret.page = function(_page, _limit) 
	local skip = (_page>1) and (((_page-1)*_limit)) or (false)
             if(skip)  then _cur:skip(skip) end
             if(_limit) then _cur:limit(_limit) end
			return ret
	end
	return ret
end

return t
