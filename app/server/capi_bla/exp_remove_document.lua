#!/usr/bin/env lua
cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function set_dupicate_array(arr) 
    local hash = {}
    local res = {}   
    for _, v in ipairs(arr) do
       if (not hash[v._id]) then
           res[#res+1] = v._id
           hash[v] = true
       end
    end
    local total = {}
    for key, value in pairs(hash) do table.insert(total, key) end
    return total
end

function get_count_signer_remove(signers) 
    local count = 0
    -- local current_time = bson.get_utc_date(ngx.now() * 1000).v
    local current_time = os.time()*1000

    for i, v in ipairs(signers) do
        if v.is_remove and v.is_remove == true and v.time_remove then
            if check_time(v.time_remove) then
                count = count + 1
            end
        end
    end
    return count
end

function check_time(time)
    local status = false
    -- local current_time = bson.get_utc_date(ngx.now() * 1000).v
    local current_time = os.time()*1000
    local exp = time + (24*60*60*1000)
    if current_time > exp then
        status = true
    end
    return status
end

function check_exp_date_document (arr)
    local list = {}

    for i, v in ipairs(arr) do
        local doc, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(v))  })

        if doc then
            doc._id = tostring(doc._id)
            local count_remove = #doc.signers + 1
            local count_signers = get_count_signer_remove(doc.signers)
            
            if doc.owner_is_remove == true and doc.owner_time_remove then
                if check_time(doc.owner_time_remove) then
                    count_signers = count_signers + 1
                end
            end
    
            if count_signers == count_remove then
                table.insert(list, doc._id)
            end
        end
    end

    return list
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body  = ngx.req.get_post_args()
    -- body = cjson.decode(ngx.req.get_body_data())
else
   body   = {}
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")

    query = { owner_is_remove = true }
    query2 = { signers={['$elemMatch']={ is_remove = true }} }

    q = col:find(query)
    q2 = col:find(query2)
    local total_doc = {}

    for i, v in q:pairs() do
        v._id = tostring(v._id)
        table.insert(total_doc, v._id)
    end 

    for i2, v2 in q2:pairs() do
        v2._id = tostring(v2._id)
        table.insert(total_doc, v2._id)
    end 

    total_doc = set_dupicate_array(total_doc)
    total_doc = check_exp_date_document(total_doc)

    if total_doc and #total_doc > 0 then
        for i3, v3 in ipairs(total_doc) do
            i, err = col:delete({ _id = object_id.new(convertHexStringToNormal(v3))})
        end
    end 

    ret = { success = true, error_msg = "list document remove" }

print(cjson.encode(ret))
end
