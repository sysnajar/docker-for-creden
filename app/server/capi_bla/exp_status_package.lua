#!/usr/bin/env lua
cjson = require 'cjson'
mongo = require "resty-mongol"
bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_exp_date_package (arr)

    for i, v in ipairs(arr) do
        local p, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(v))  })

        if p then
            local key = { _id = p._id }
            local update_cmd = {
                active_package = false,
                end_active_package = bson.get_utc_date(os.time() * 1000)
            }
            local update_o ={["$set"] = update_cmd}
            local update_old = col:update(key, update_o)
        end
    end
    return #arr
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_uri_args()
    -- body  = ngx.req.get_post_args()
    -- body = ngx.req.get_body_data()
    -- body = cjson.decode(ngx.req.get_body_data())
    if body.supermankey ~= "supermanvalue" then
        print(cjson.encode({ success = false, error_msg = "no key update status package", error_code = "" }))
        ngx.exit(ngx.HTTP_OK)
    end
-- else
--    body   = {}
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    db = conn:new_db_handle("dopa")
    col = db:get_col("package_ss")

    local time = os.time()*1000
    query = { active_package = true, package={['$ne']="PROJECT"} }

    q = col:find(query)
    local pack_exp = {}

    for i, v in q:pairs() do
        v._id = tostring(v._id)
        if v.active_expire_date and time > v.active_expire_date then
            table.insert(pack_exp, v._id)
        end
    end 


    c = check_exp_date_package(pack_exp)
    ret = { success = true, error_msg = "list package update", data = c }

print(cjson.encode(ret))
end
