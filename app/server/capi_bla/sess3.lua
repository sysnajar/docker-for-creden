local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local is_company = nil

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

-- get member role
conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if not err then
    local db_dopa = conn:new_db_handle("dopa")
    local col_member_list = db_dopa:get_col("company_members")
    if session.data.company_id and session.data.username then 
        local company_id = object_id.new(convertHexStringToNormal(session.data.company_id))
        local email = session.data.username
        local member , member_err = col_member_list:find_one({company_id = company_id, email = email})
        if member then
            -- get member status
            local key = 'member.'..session.data.username..'.'..session.data.company_id
            local find_key = rc:keys(key)
            if table.getn(find_key) ~= 0 then
                data = cjson.decode(rc:get(find_key[1]))
            end
            -- get member status
            member_role = member.role
            if member.status == 'APPROVE' then
                is_company = true
            end
        end
    end
end
-- get member role

local sess = {usr = session.data.username, is_company = is_company, member_role = member_role, invite_detail = data}
ngx.say(cjson.encode(sess))
