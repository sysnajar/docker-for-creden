#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

ref_no = ''
if body.ref_no == nil then
    ref_no = 'ref_tmp'
end

local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
time = os.time()
--filename = body.code..'_'..time
filename = ref_no..'_'..time
rc:set(filename,body.vdo)
rc:expire(filename,10)
--prog.stdin = body.vdo
today = os.date('*t',os.time())
mm = tostring(today.month)
dd = tostring(today.day)
dir = today.year .. ((#mm==1) and '0'..mm or mm) .. ((#dd==1) and '0'..dd or dd)
vtt_format = [[
            WEBVTT \n
            00:00:0]]..body.arr_time[1]..[[.000 --> 00:00:0]]..(body.arr_time[1]+2)..[[.000 \n
            ]]..body.arr_txt[1]..[[ \n
            00:00:0]]..body.arr_time[2]..[[.000 --> 00:00:0]]..(body.arr_time[2]+2)..[[.000 \n
            ]]..body.arr_txt[2]..[[ \n
            00:00:]]..body.arr_time[3]..[[.000 --> 00:00:]]..(body.arr_time[3]+2)..[[.000 \n
            ]]..body.arr_txt[3]
data = {id = body.id ,filename = body.comp_name..'_'..dir..'_'..filename, time = time, comp_id = body.comp_name, word = body.arr_txt, ref_no = ref_no, time = body.arr_time}
local res, err = prog('sh', '/home/creden/work/app/server/capi/convert_vdo.sh', body.comp_name, filename, dir, cjson.encode(data), vtt_format)

if not err then
	--data = {filename = filename, time = time, comp_id = body.comp_name, word = body.code, ref_no = body.ref_no}
    --print(body.vdo)

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("test")
        col = db:get_col("vdo")
        i, err = col:insert({data}, nil, true)
        if err then
            ret = {success = false, error_msg = err}
        else
            ret = {success = true, error_msg = "", data = data}
        end
    end
else
    ret = {success = false, error_msg = err}
end

print(cjson.encode(ret))
