#mogrify -rotate -90 $1
w=$(identify -format '%w' $1)
h=$(identify -format '%h' $1)
w=$((w/4))
h=$((h/2))
convert -size "$w"x"$h" xc:black $2 
convert $1 $2 -gravity southeast -geometry +25+50 -composite $3 && rm $2 > /dev/null
