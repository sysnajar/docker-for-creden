#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil
prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()

    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
 	body = cjson.decode(arg[1])
end

print('------------submit vdo-------------------')
print(arg[1])
local ret = {success = false, error_msg = "N/A"}

local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

name = body.compCode ..'_'..body.txid

t = {}
for k, v in ipairs(body.vdo.missions) do
    if v.type == 'voice' then
        local voice = {word = v.word, startTime = v.startTime, endTime = v.endTime} 
        table.insert(t, voice)
    end
end

os.execute("/home/creden/work/app/server/capi/gensub.lua '".. cjson.encode(t) .. "' > /home/creden/work/app/client/vdo/ex/"..name..".vtt")
print('gen vtt')

--remove wav
os.remove('/home/creden/work/app/client/vdo/ex/'..name..'.wav')
print('remove wav')

os.execute('sh /home/creden/work/app/server/capi/upload_vdo.sh '..name..' '..body.txid)
print('convert webm to wav')

-- local res, err = prog('lua', '/home/creden/work/app/server/capi/speech_api_tmp.lua', name, body.txid, body.compCode, table.getn(body.vdo.missions) )
os.execute('lua /home/creden/work/app/server/capi/speech_api_ntl.lua "'..name..'" "'..body.txid..'" "'..body.compCode..'" "'..table.getn(body.vdo.missions)..'" &')
os.exit(0)