#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379) 

function init()
if(#arg > 0  )then
  redis = require 'redis'
  rc = redis.connect('127.0.0.1', 6379) 
  mongo = require "resty-mongol"
  prog = exec.new('/tmp/exec_crim.sock')
  prog.timeout_fatal = false
  conn = mongo()
  conn:set_timeout(1000)
  ok, err = conn:connect('127.0.0.1',27017)
  db = conn:new_db_handle("test")
  users = db:get_col("user") 
  id = arg[1]
  return true
else
  return false	
end 
end

local fn =  function(email)
	local t  = users:find_one({email = email, activated = 1})
	local ret = {success = true, black = nil, red = nil , bankrupt = nil} 
	
	local res1, err1 = prog('curl', '-X', 'POST', '-d' , 'txtAccuName2=' .. t.firstname ..'%20' .. t.lastname , 'http://sm.hjkl.ninja:8124/xapi/api_civil_black')
	local res2, err2 = prog('curl', '-X', 'POST', '-d' , 'txtAccuName2=' .. t.firstname ..'%20' .. t.lastname , 'http://sm.hjkl.ninja:8124/xapi/api_civil_red')
	
	--local res1, err1 = prog('curl', '-X', 'POST', '-d' , 'txtAccuName2=วราพร%20จุฑากรณ์' , 'http://sm.hjkl.ninja:8124/xapi/api_civil_black')
	--local res2, err2 = prog('curl', '-X', 'POST', '-d' , 'txtAccuName2=วราพร%20จุฑากรณ์' , 'http://sm.hjkl.ninja:8124/xapi/api_civil_red')

	if not err1 then
		local tmp = cjson.decode(res1.stdout) --try decoding
		ret.black = (type(tmp.success) == 'boolean') and tmp or nil
    end

	if not err2 then
		local tmp = cjson.decode(res2.stdout) --try decoding
		ret.red = (type(tmp.success) == 'boolean') and tmp or nil
    end

    --bankrupt
    local bankrupt = rc:get(email..'.led_result:comp')
    if bankrupt ~= '0' then ret.bankrupt = {total = 1} end

return ret
end


if(init())then
  print(cjson.encode(fn(id)))
else
  return fn
end
