local server = require ("resty.websocket.server")

local clients = {}
local cjson = require 'cjson'
local redis = require 'resty.redis'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local red = redis:new()
red:set_timeout(5000)
red:connect('127.0.0.1', 6379)
local sleep_timeout = 10

local wb, err = server:new{
  timeout = 5000,
  max_payload_len = 65535
}
if not wb then
  ngx.log(ngx.ERR, "failed to new websocket: ", err)
  return ngx.exit(444)
end


local scb_key   = 'empty'
local kbank_key = 'empty'

local led_capcha_key = 'empty'
local led_result_key = 'empty'
local led_email = ''

local kfund_key = 'empty'
local kprop_key = 'empty'

while true do	
  	
  local data, typ, err = wb:recv_frame()

      local scb_val   = red:get(scb_key)
      local kbank_val = red:get(kbank_key)
      local bankrupt_val = red:get(led_capcha_key) --red:get('led_capcha')
      local led_result = red:get(led_result_key) --red:get('led_result')
      local kfund_val = red:get(kfund_key) 
      local kprop_val = red:get(kprop_key) 
      --local dbd_val   = red:get('demo.dbd')

	   wb:send_text('demo.ping:{"success": true, "type": "ping"}')
           ngx.log(ngx.NOTICE, 'running')
      

      if(type(scb_val)=='string') then
		   bytes, err = wb:send_text('demo.scb:{"success":true}')
           ngx.log(ngx.INFO, 'sent scb val')
	  end

      if(type(kbank_val)=='string') then
		   bytes, err = wb:send_text('demo.kbank:{"success":true}')
           ngx.log(ngx.INFO, 'sent kbank')
	  end

      if(type(bankrupt_val)=='string') then     	   
		   bytes, err = wb:send_text('bankrupt:{"success":true,"val":"'..bankrupt_val..'"}')
           ngx.log(ngx.INFO, 'sent bankrupt')
           red:del(led_capcha_key)
	  end

	  if(type(led_result)=='string') then
		   bytes, err = wb:send_text('led_result:'..led_result)
           ngx.log(ngx.INFO, 'sent led_result')
	  end

	  if(type(kfund_val)=='string') then  
	  	   os.execute('lua /home/creden/work/app/server/capi/filter_tb_kfund.lua '..kfund_key)   	   
		   kfund_val = red:get(kfund_key)
		   bytes, err = wb:send_text('demo.kfund:'..kfund_val)
           ngx.log(ngx.INFO, 'sent kfund')
           red:del(kfund_key)
	  end

	  if(type(kprop_val)=='string') then  
	  	   os.execute('lua /home/creden/work/app/server/capi/filter_tb_kprop.lua '..kprop_key)   	   
		   kprop_val = red:get(kprop_key)
		   bytes, err = wb:send_text('demo.kprop:'..kprop_val)
           ngx.log(ngx.INFO, 'sent kprop')
           red:del(kprop_key)
	  end


  if wb.fatal then
	ngx.log(ngx.ERR, "failed to receive frame: ", err)
	return ngx.exit(444)
  end
  if not data then
	local bytes, err = wb:send_ping()
	if not bytes then
	  ngx.log(ngx.ERR, "failed to send ping: ", err)
	  return ngx.exit(444)
	end
  elseif typ == "close" then break
  elseif typ == "ping" then
	local bytes, err = wb:send_pong()
	if not bytes then
	  ngx.log(ngx.ERR, "failed to send pong: ", err)
	  return ngx.exit(444)
	end
  elseif typ == "pong" then
  elseif typ == "text" then
	
    body = cjson.decode(data)

	if (body.subject == 'bank') then
	
       if(body.bank == 'scb')   then scb_key   = body.key  end
       if(body.bank == 'kbank') then kbank_key = body.key  end


	   local cmd = 'xvfb-run -a node /home/creden/' .. body.bank  .. '.js false 12000'
	   local params = body.key .. ' ' ..  body.username .. ' ' .. body.password .. ' true &'
	   cmd = cmd .. ' ' .. params
	   os.execute(cmd)
	end

	if (body.subject == 'dbd') then
	   os.execute('sh /home/creden/work/app/server/capi/set_dbd.sh')
	end

	if (body.subject == 'captcha') then
	   --red:del('capcha_ans')
	   --red:del('led_result')
	   --red:del('led_capcha')
	   led_capcha_key = body.key..'captcha'
	   led_result_key = body.key..'result'
	   led_email = body.key..'.let_result:comp'	   
	   os.execute('xvfb-run -a node /home/creden/led.js '..body.txtPid..' '..body.key..' &')
	end

	if (body.subject == 'kfund') then
	   kfund_key = body.key	
	   kprop_key = body.key..'_prop'  
	   os.execute('xvfb-run -a node /home/creden/kbank_funds.js '..body.username..' '..body.password..' '..body.key..' &')
	end
	
  end
  ngx.sleep(sleep_timeout)
end

ngx.log(ngx.INFO, 'closing websocket for client')
wb:send_close()
