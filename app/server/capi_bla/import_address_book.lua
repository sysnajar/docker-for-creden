#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
end


--[[
local json = [[
{
    "IsSuccess": true,
    "ErrorMessage": null,
    "ErrorCode": 0,
    "Data": [
      {
        "EMAIL": "PHAKHAKUL.PHO@BLA.CO.TH",
        "EmpName": "ภคกุล",
        "EmpID": "003514",
        "EmpSurname": "ผ่องบุพกิจ",
        "OrgDescription": "ส่วนวิเคราะห์เครดิต"
      },
      {
        "EMAIL": "Test1@BLA.CO.TH",
        "EmpName": "fTest1",
        "EmpID": "000001",
        "EmpSurname": "lTest1",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Test2@BLA.CO.TH",
        "EmpName": "fTest2",
        "EmpID": "000002",
        "EmpSurname": "lTest2",
        "OrgDescription": "แผนก HR"
      },
      {
        "EMAIL": "Test3@BLA.CO.TH",
        "EmpName": "fTest3",
        "EmpID": "000003",
        "EmpSurname": "lTest3",
        "OrgDescription": "แผนก Development"
      },
      {
        "EMAIL": "Test4@BLA.CO.TH",
        "EmpName": "fTest4",
        "EmpID": "000004",
        "EmpSurname": "lTest4",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom1@BLA.CO.TH",
        "EmpName": "Boom",
        "EmpID": "000005",
        "EmpSurname": "C",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom2@BLA.CO.TH",
        "EmpName": "BoomBoomBoom",
        "EmpID": "000006",
        "EmpSurname": "CCC",
        "OrgDescription": "แผนก HR"
      },
      {
        "EMAIL": "Boom3@BLA.CO.TH",
        "EmpName": "Chaluemchat",
        "EmpID": "000007",
        "EmpSurname": "Chaluemchat",
        "OrgDescription": "แผนก Development"
      },
      {
        "EMAIL": "Boom4@BLA.CO.TH",
        "EmpName": "Chaluemchat boom",
        "EmpID": "000008",
        "EmpSurname": "CCC Chaluemchat",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom5@BLA.CO.TH",
        "EmpName": "Chaluemchat boom boom",
        "EmpID": "000009",
        "EmpSurname": "CCC Chaluemchat",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Boom6@BLA.CO.TH",
        "EmpName": "Chaluemchat boom",
        "EmpID": "0000010",
        "EmpSurname": "C Chaluemchat",
        "OrgDescription": "แผนก บัญชี"
      },
      {
        "EMAIL": "Test11@BLA.CO.TH",
        "EmpName": "mazachii",
        "EmpID": "000011",
        "EmpSurname": "Cat",
        "OrgDescription": "แผนก HR"
      }
      
    ]
  }
]]


local filename = '/home/creden/work/app/server/capi/add.json'
local f = assert(io.open(filename, "r"))
local json = f:read("*all")
f:close()
local t = cjson.decode(json)
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
end
db = conn:new_db_handle("dopa")
col = db:get_col("address_book")
col:delete({imported = true})
for i,v in ipairs(t.Data) do
  local email = tostring(v.EMAIL):lower()
  v.email = email:gsub( "@bla.co.th", "@bangkoklife.com")
  v.EMAIL = nil
  v.imported = true
end
  i, err = col:insert(t.Data, nil, true)
  if err then
    ret = {success = false, error_msg = err}
else
  ret = {success = true}
end

ngx.say(cjson.encode(ret))
