#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { name_document_type = arg[1] }
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("document_type")

    doc = { _id = object_id.new(convertHexStringToNormal(body._id)) or "N/A" }
    d, err = col:find_one(doc)

    if d then
        i, err = col:delete({ _id = d._id or "N/A" },1,0)

        if i then
            ret = {success = true, error_msg = "Remove document type success"}
        else
            ret = {success = false , error_msg = "Remove document type unsuccess"}
        end

    else
        ret = { success = false, error_msg = "Not document type" }
    end
end

print(cjson.encode(ret))
