#!/usr/bin/env lua
local cjson = require 'cjson'

json = [[
 [{
    "id": "5960500028101",
    "nameTH": "นายอำรัน มิง",
    "nameEN": "MR. AMRAN MING",
    "dob": "18 ธันวาคม 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500206046",
    "nameTH": "นายอับดุลเล๊าะฮ์ มะมิง",
    "nameEN": "MR. ABDULLOH MAMING",
    "dob": "1 กรกฎาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960700103038",
    "nameTH": "นายแอ มะแซ",
    "nameEN": "MR. AIR MAESAE",
    "dob": "16 เมษายน 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2960400008139",
    "nameTH": "นายรอวี หะยีดิง",
    "nameEN": "MR. ROWI HAYIDING",
    "dob": "7 พฤษภาคม 2530",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960400260561",
    "nameTH": "นายมะดารี วาหลง",
    "nameEN": "MR. DAREE WALUNG",
    "dob": "22 มิถุนายน 2519",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500431031",
    "nameTH": "นายอับดุลตอเละ บาเย๊าะกาเซ๊ะ",
    "nameEN": "MR. ABDULTOLEH BAYOHKASEH",
    "dob": "20 มกราคม 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2556",
    "passportNo1": "OT0537103(วันหมดอายุ 09/11/2009)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960100084711",
    "nameTH": "นายเตาฟีก โต๊ะเล๊าะ",
    "nameEN": "MR. TAWFIK TOKLOH",
    "dob": "19 ตุลาคม 2532",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950600330318",
    "nameTH": "นายมะซูปียัน ยะกูมอ",
    "nameEN": "MR. MASUPEEYAN YAKUMOR",
    "dob": "18 มิถุนายน 2517",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2556",
    "passportNo1": "OE0249376(วันหมดอายุ 08/02/1998)",
    "passportNo2": "OX034476(วันหมดอายุ 14/12/2004)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2960500035928",
    "nameTH": "นายมะตอเพะ มาลายอ",
    "nameEN": "MR. MATOPHEH MALAYOR",
    "dob": "1 กันยายน 2529",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960500003700",
    "nameTH": "นายอูสนี บาโต",
    "nameEN": "MR. AUSNEE BADO",
    "dob": "20 มกราคม 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500669526",
    "nameTH": "นายบารอวี เจ๊ะแม",
    "nameEN": "MR. BAROWEE CHEMAE",
    "dob": "10 ธันวาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100119014",
    "nameTH": "นายอาบะห์ เจะอาลี",
    "nameEN": "MR. ARBAH CHE ALEE",
    "dob": "15 มิถุนายน 2522",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5950600002058",
    "nameTH": "นายบูคอรี ฮะมะ",
    "nameEN": "MR. BUKOREE HAMA",
    "dob": "3 ธันวาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100117623",
    "nameTH": "นายมะยากี บือราเฮง",
    "nameEN": "MR. MAHYAKEE BARAHANG",
    "dob": "5 กุมภาพันธ์ 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500083971",
    "nameTH": "นายมะนุ๊ มามะ",
    "nameEN": "MR. MANU MAMAH",
    "dob": "12 พฤษภาคม 2514",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2556",
    "passportNo1": "OX0312485 (วันหมดอายุ 08/02/2004)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5960500023401",
    "nameTH": "นายกูลี ลือบา",
    "nameEN": "MR. KULI LUEBA",
    "dob": "20 ธันวาคม 2519",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2556",
    "passportNo1": "OX0319834 (วันหมดอายุ 20/04/2004)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100151449",
    "nameTH": "นายกามารูดิง ขะเดหมะ",
    "nameEN": "MR. KAMARUDING KHADEMAH",
    "dob": "20 มีนาคม 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500042264",
    "nameTH": "นายลุกมัน แปแนะ",
    "nameEN": "MR. LUKMAN PAENAEH",
    "dob": "30 กรกฎาคม 2513",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2556",
    "passportNo1": "ON0110517 (วันหมดอายุ 11/02/1997)",
    "passportNo2": "OJ0308545 (วันหมดอายุ 08/02/2002)",
    "passportNo3": "OT0425800 (วันหมดอายุ 21/01/2007)",
    "passportNo4": "OC0819857 (วันหมดอายุ 11/10/2014)"
  },
  {
    "id": "1961100007482",
    "nameTH": "นายอับดุลบาซิค ยูนุ",
    "nameEN": "MR. ABDULBASIK YUNUH",
    "dob": "4 กันยายน 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2556",
    "passportNo1": "OM0429418 (วันหมดอายุ 07/04/2006)",
    "passportNo2": "OM0633763 (วันหมดอายุ 03/04/2011)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960600011348",
    "nameTH": "นายซัฟวาน สาแล๊ะ",
    "nameEN": "MR. SAFWAN SALAEH",
    "dob": "5 พฤษภาคม 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2556",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5950301047882",
    "nameTH": "นายอับดุลเลาะ ตาเปาะโต๊ะ",
    "nameEN": "MR. ABDULLOH TAPOHTOH",
    "dob": "29 สิงหาคม 2529",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900197851",
    "nameTH": "นายอับดุลฮาดี ดาหาเล็ง",
    "nameEN": "MR. ABDUL HADEE DAHALENG",
    "dob": "4 มกราคม 2521",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1961100075089",
    "nameTH": "นายต่วนยัสลัน นิราแม",
    "nameEN": "",
    "dob": "24 ธันวาคม 2534",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2961100014819",
    "nameTH": "นายราฟี มามะรอยาลี",
    "nameEN": "MR. RAFEE MAMAT ROYALI",
    "dob": "19 กันยายน 2529",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1950100004008",
    "nameTH": "นายยาสะ ดาแม",
    "nameEN": "MR. YASA DAMAE",
    "dob": "22 มีนาคม 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2950100016730",
    "nameTH": "นายรอกิ ดอเลาะ",
    "nameEN": "MR. ROKI DORLOH",
    "dob": "4 เมษายน 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100571502",
    "nameTH": "นายสะกรี ดอเลาะ",
    "nameEN": "",
    "dob": "23 พฤษภาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950600147947",
    "nameTH": "นายกอเซ็ง มิยะ",
    "nameEN": "MR. KOSENG MIYA",
    "dob": "18 เมษายน 2520",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950600175711",
    "nameTH": "นายซัยฟุลลอฮ ซาฟรุ",
    "nameEN": "",
    "dob": "20 มีนาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2950500009706",
    "nameTH": "นายนัชดาน ยะลาแป",
    "nameEN": "MR. NATCHADAN YALAPAE",
    "dob": "8 กุมภาพันธ์ 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1969800006451",
    "nameTH": "นายมูฌาฮีดีน สาตา",
    "nameEN": "MR. MUYAHHIDIN SATA",
    "dob": "30 กรกฎาคม 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100067227",
    "nameTH": "นายซัมบรี ยุนุ",
    "nameEN": "MR. SAMBRI YUNU",
    "dob": "17 มิถุนายน 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1950300017316",
    "nameTH": "นายลุกมาน ปูลาประเปะ",
    "nameEN": "MR. LUKMAN PULAPRAPE",
    "dob": "10 มีนาคม 2529",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 9/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940200358070",
    "nameTH": "นายรูกิปลี สาอิ",
    "nameEN": "",
    "dob": "30 มีนาคม 2521",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 10/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3941100096953",
    "nameTH": "นายสุกรียา เดมะ",
    "nameEN": "MR. SUKRIYA DEMAH",
    "dob": "20 กุมภาพันธ์ 2521",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 11/2557",
    "passportNo1": "T426805 (วันหมดอายุ 12/11/2006)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940500277008",
    "nameTH": "นายอาหามะ จาจ้า",
    "nameEN": "MR. ARHAMA JAJA",
    "dob": "10 ธันวาคม 2513",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 12/2557",
    "passportNo1": "C259743 (วันหมดอายุ 26/09/1996)",
    "passportNo2": "I300991 (วันหมดอายุ 09/09/2001)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5940200003251",
    "nameTH": "นายกามารูดิง มูซอ",
    "nameEN": "MR. KAMARUDING MUSOR",
    "dob": "2 พฤศจิกายน 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 13/2557",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1940500046361",
    "nameTH": "นายอับดุลอาซิ แอเสาะ",
    "nameEN": "MR. OAPDUNARSI AESO",
    "dob": "27 ธันวาคม 2530",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 14/2557",
    "passportNo1": "P721024 (วันหมดอายุ 07/02/2013)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950600328828",
    "nameTH": "นายมาโซ สาและเร๊ะ",
    "nameEN": "",
    "dob": "1 มกราคม 2518",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2558",
    "passportNo1": "I351445 (วันหมดอายุ 11/09/2002)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5950600018035",
    "nameTH": "นายซูลายมัน ยะกูมอ",
    "nameEN": "MR. SULAIMAN YAKUMOR",
    "dob": "16 มีนาคม 2523",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2558",
    "passportNo1": "E483255 (วันหมดอายุ 27/09/2005)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5950699000834",
    "nameTH": "นายอายุ อัตนิ",
    "nameEN": "MR. AYU ATNI",
    "dob": "5 กุมภาพันธ์ 2516",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5950600026241",
    "nameTH": "นายอายุ หะยีบากา",
    "nameEN": "MR. AYU HAYEEBAKA",
    "dob": "15 มกราคม 2520",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2558",
    "passportNo1": "C753907 (วันหมดอายุ 04/04/2012)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100528046",
    "nameTH": "นายกูอาสมัน ดอฆอ",
    "nameEN": "",
    "dob": "15 ธันวาคม2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950600094991",
    "nameTH": "นายสูดิรมาน จารง",
    "nameEN": "MR. SUDIRAMAN CHARONG",
    "dob": "8 พฤษภาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1941000089169",
    "nameTH": "นายนาวาวี ลาโบปี",
    "nameEN": "MR. NAWAWI LABOPI",
    "dob": "28 ตุลาคม2529",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940100174422",
    "nameTH": "นายมะซอเร ดือรามะ",
    "nameEN": "",
    "dob": "7 กรกฎาคม 2522",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940100115434",
    "nameTH": "นายมูฮาหมัดนูรูดิง มะรอแม",
    "nameEN": "MR. MUHAMMADNURUDING MAHRORMAE",
    "dob": "9 กันยายน 2518",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2558",
    "passportNo1": "R322368 (วันหมดอายุ 12/02/2003)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950100175151",
    "nameTH": "นายมะกอเซ็ง เจ๊ะมะ",
    "nameEN": "MR. MAKOSENG JEHMA",
    "dob": "8 มกราคม 2521",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2558",
    "passportNo1": "X336859 (วันหมดอายุ 07/10/2004)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2960600016577",
    "nameTH": "นายมาหามะ แมเร๊าะ",
    "nameEN": "MR. MAHAMAH MAEROH",
    "dob": "1 มกราคม 2530",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940500209142",
    "nameTH": "นายซอบรี บือแน",
    "nameEN": "MR. SOBRI BUENAE",
    "dob": "19 ธันวาคม 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 8/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3900300487696",
    "nameTH": "นายรุสลัน ใบมะ",
    "nameEN": "",
    "dob": "21 กันยายน 2523",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 9/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3900600233754",
    "nameTH": "นายซัมซูดิง บาเหม",
    "nameEN": "",
    "dob": "3 พฤศจิกายน 2521",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 10/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3900600237580",
    "nameTH": "นายอารง สะอิ",
    "nameEN": "MR. ARRONG SAAI",
    "dob": "17 กุมภาพันธ์ 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 10/2558",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1940300012861",
    "nameTH": "นายเมาลานา สาเมาะ",
    "nameEN": "MR. MAOLANA SAMO",
    "dob": "14 กันยายน 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1949900023927",
    "nameTH": "นายเสรี แวมามุ",
    "nameEN": "MR. SERI WAEMAMU",
    "dob": "21 เมษายน 2528",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2559",
    "passportNo1": "V447810 (วันหมดอายุ 27/02/2007)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1940300016379",
    "nameTH": "นายรอมลี รอแน็ง",
    "nameEN": "MR. ROMLEE RONAENG",
    "dob": "20 มกราคม 2528",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2559",
    "passportNo1": "V449148 (วันหมดอายุ 04/03/2007)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900550534",
    "nameTH": "นายมะรีเป็ง มะหะ",
    "nameEN": "MR. MAREEPENG MAHA",
    "dob": "18 พฤษภาคม 2522",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2559",
    "passportNo1": "R379524 (วันหมดอายุ 10/04/2005)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900508813",
    "nameTH": "นายมะยูนิ สาแม",
    "nameEN": "MR. MAUNI SAMAE",
    "dob": "17 กันยายน 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900560262",
    "nameTH": "นายดอเลาะ ดอเลาะ",
    "nameEN": "MR.DORLOH DORLOH",
    "dob": "16 เมษายน 2510",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2559",
    "passportNo1": "K213406 (วันหมดอายุ 17/10/1998)",
    "passportNo2": "J324069 (วันหมดอายุ 19/08/2002)",
    "passportNo3": "S688892 (วันหมดอายุ 08/10/2008)",
    "passportNo4": ""
  },
  {
    "id": "3940900560343",
    "nameTH": "นายอับดุลอายิ ดอเลาะ",
    "nameEN": "MR. ABDUARYI DOLAH",
    "dob": "30 ตุลาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2559",
    "passportNo1": "M536377 (วันหมดอายุ 10/10/2007)",
    "passportNo2": "C799218 (วันหมดอายุ 08/10/2008)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900506268",
    "nameTH": "นายวาครี มะโระ",
    "nameEN": "",
    "dob": "1 มกราคม 2507",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900522565",
    "nameTH": "นายอะมะ ดือเระห์",
    "nameEN": "MR. AHMA DUEREH",
    "dob": "21 มิถุนายน 2521",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2559",
    "passportNo1": "R322226 (วันหมดอายุ 11/02/2003)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960400028880",
    "nameTH": "นายนูรอดิง อับดุลลาตี",
    "nameEN": "MR. NURODING ABDULLATEE",
    "dob": "1 กันยายน 2520",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900564641",
    "nameTH": "นายอาดือนัน เจะอาแซ",
    "nameEN": "MR. ADUENAN JEHASAE",
    "dob": "1 มกราคม 2515",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2559",
    "passportNo1": "N115628 (วันหมดอายุ 10/02/1997)",
    "passportNo2": "X316086 (วันหมดอายุ 08/10/2008)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940900387790",
    "nameTH": "นายมะยากี เจะฮิง",
    "nameEN": "MR. MAHYAKEE CHEHING",
    "dob": "23 เมษายน 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1940900023422",
    "nameTH": "นายอับดุลการิม สาแม",
    "nameEN": "MR. ABDULKARIM SAMAE",
    "dob": "20 กรกฎาคม 2528",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960500237859",
    "nameTH": "นายอัซรัน อาแวบือซา",
    "nameEN": "MR. ASRAN AWAEBUESA",
    "dob": "2 กรกฎาคม 2536",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2559",
    "passportNo1": "P725737 (วันหมดอายุ 21/02/2013)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960500017913",
    "nameTH": "นายดือราพา เจ๊ะอูมา",
    "nameEN": "MR. DUERAPHA CHEAUMA",
    "dob": "15 กันยายน 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5940100018842",
    "nameTH": "นายอิสยาซะห์ หะแย",
    "nameEN": "",
    "dob": "17 พฤษภาคม 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3900600190397",
    "nameTH": "นายซารีซานอัมรี ดือราแม",
    "nameEN": "MR. SARIZANAMRI DUERAMAE",
    "dob": "15 กุมภาพันธ์ 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950300049087",
    "nameTH": "นายนูรุดดิน กาจะลากี",
    "nameEN": "MR. NURUTDIN KACHALAKI",
    "dob": "9 มกราคม 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940200650048",
    "nameTH": "นายสุไลมาน สาเหมาะ",
    "nameEN": "MR. SULAIMAN SAMOH",
    "dob": "5 ตุลาคม 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5941000022903",
    "nameTH": "นายมูฮาหมัดนูร ตูหยง",
    "nameEN": "MR. MUHAMMADNUR TUYONG",
    "dob": "25 พฤษภาคม 2533",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2559",
    "passportNo1": "V791823 (วันหมดอายุ 05/10/2013)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2950500009714",
    "nameTH": "นายฮิลมี ยะลาแป",
    "nameEN": "MR. HINMI YALAPAE",
    "dob": "11 พฤศจิกายน 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3900600227444",
    "nameTH": "นายอับดลเล๊าะ สาเม๊าะ",
    "nameEN": "MR. AMDOLAH SAMAH",
    "dob": "5 กุมภาพันธ์ 2517",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2559",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3961200139662",
    "nameTH": "นายมะรอพี บือราเฮง",
    "nameEN": "MR. MAROPHI BUERAHENG",
    "dob": "13 พฤศจิกายน 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 8/2559",
    "passportNo1": "H550764 (วันหมดอายุ 04/02/2008)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960300333664",
    "nameTH": "นายมะรอมือลี กาแจกาซอ",
    "nameEN": "MR. MAROMUELEE KACHAEKASO",
    "dob": "16 ธันวาคม 2526",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3940300184260",
    "nameTH": "นายมะรุดิง สามะ",
    "nameEN": "MR. MAHRUDING SAMAH",
    "dob": "2 เมษายน 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 2/2560",
    "passportNo1": "W640889 (วันหมดอายุ 25/10/2011)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3100601119863",
    "nameTH": "นายมนูญ ชัยชนะ\nหรืออเนก หรือเอนก\nหรือต๋อย หรือต้อย\nซานฟราน",
    "nameEN": "",
    "dob": "15 ตุลาคม 2498",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 3/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500499299",
    "nameTH": "นายมูฮามัด ขาเดร์",
    "nameEN": "MR. MUHAMMAD KHADE",
    "dob": "5 กรกฎาคม 2520",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2560",
    "passportNo1": "I369615 (วันหมดอายุ 24/12/2002)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960500142050",
    "nameTH": "นายอาแว สามะ",
    "nameEN": "MR. AWAE SAMA",
    "dob": "2 มีนาคม 2532",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500370652",
    "nameTH": "นายมาหะมะซอบือรี กรือสง",
    "nameEN": "MR. MAHAMASOBUEREE KRUESONG",
    "dob": "11 มีนาคม 2523",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2560",
    "passportNo1": "H429909 (วันหมดอายุ 13/07/2005)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960500462832",
    "nameTH": "นายอารง ดือราแม",
    "nameEN": "MR. ARUNG DERAMAE",
    "dob": "1 มกราคม 2511",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3960100366194",
    "nameTH": "นายไรนาอาบีเด็ง สามะ",
    "nameEN": "MR. RAINAABIDENG SAMA",
    "dob": "22 พฤษภาคม 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 4/2560",
    "passportNo1": "M419238 (วันหมดอายุ 18/01/2006)",
    "passportNo2": "J634685 (วันหมดอายุ 15/12/2010)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960300062176",
    "nameTH": "นายอัฎฮาร์ ยูกะ",
    "nameEN": "MR. ADHA YUKA",
    "dob": "27 มิถุนายน 2533",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2560",
    "passportNo1": "N981277 (วันหมดอายุ 06/06/2016)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960300006306",
    "nameTH": "นายดาราแมง อูมา",
    "nameEN": "MR. DARAMAENG AUMA",
    "dob": "12 สิงหาคม 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 5/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2960300011120",
    "nameTH": "นายอับดุลเลาะ อูแล",
    "nameEN": "MR. ABDULLOH OULAE",
    "dob": "11 พฤษภาคม 2528",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1960400032007",
    "nameTH": "นายคอเละ เซ็ง",
    "nameEN": "MR. KHOLEH SENG",
    "dob": "5 มิถุนายน 2529",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 6/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3961100347921",
    "nameTH": "นายมูฮามัดใบฮาดี วาเฮ็ง",
    "nameEN": "MR. MUHAMMADBAIHADEE WAHENG",
    "dob": "10 สิงหาคม 2515",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1940100006302",
    "nameTH": "นายฮากีม ดอเลาะ",
    "nameEN": "MR. HAKIM DOLOH",
    "dob": "17 มีนาคม 2527",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5950300029139",
    "nameTH": "นายมูหาหมัดยากี สาและ",
    "nameEN": "MR. MUHAMATYAKI SAELAEH",
    "dob": "1 กุมภาพันธ์ 2524",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 7/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1949900081749",
    "nameTH": "นายอัสมีน กาเต็มมาดี",
    "nameEN": "MR. AZMEEN KATEMMADEE",
    "dob": "18 กรกฎาคม 2530",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 8/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "1940900113006",
    "nameTH": "นายอัมมัร แวดาราแม",
    "nameEN": "MR. AMMAR WAEDARAMAE",
    "dob": "18 เมษายน 2531",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 8/2560",
    "passportNo1": "C756983 (วันหมดอายุ 17/04/2012)",
    "passportNo2": "AA1774201 (วันหมดอายุ 09/06/2018)",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "5960600017730",
    "nameTH": "นายรอแปะอิง อุเซ็ง",
    "nameEN": "MR. RORPAEHING USENG",
    "dob": "9 กันยายน 2512",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 8/2560",
    "passportNo1": "E241352 (วันหมดอายุ 01/02/1998)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2950800001468",
    "nameTH": "นายอาซูวัน ลุมโมะ",
    "nameEN": "MR. AZUWAN LUMMOH",
    "dob": "20 กุมภาพันธ์ 2533",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 9/2560",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3941000308045",
    "nameTH": "นายอาหะมัด ลือแมซา",
    "nameEN": "MR. ARHAMAD LUEMAESA",
    "dob": "24 มีนาคม 2523",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 9/2560",
    "passportNo1": "X322228 (วันหมดอายุ 17/05/2004)",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950300053459",
    "nameTH": "นายมารซูกี ยูโสะ",
    "nameEN": "MR. MARASUKI YUSOH",
    "dob": "30 ธันวาคม 2514",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2561",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "2950800006222",
    "nameTH": "นายมาหะมะ อาหลีดองอย",
    "nameEN": "MR. MAHAMA ARLIDONGOI",
    "dob": "1 มกราคม 2538",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2561",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  },
  {
    "id": "3950300233279",
    "nameTH": "นายเฟาซี กลูแป",
    "nameEN": "MR. FAWSEE KALOOPAE",
    "dob": "10 ตุลาคม 2525",
    "year": "ประกาศสำนักงาน ปปง. ที่ ปกร. 1/2561",
    "passportNo1": "",
    "passportNo2": "",
    "passportNo3": "",
    "passportNo4": ""
  }]
]]


new_amlo = [[
    [
    {
      "nameTH": "นายอำรัน มิง",
      "id": "5960500028101",
      "dob": "18 ธันวาคม 2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอาหามะ กาเจ",
      "id": "3960300015620",
      "dob": "9 ตุลาคม 2521",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ พ.ปกร.ที่ 1/2559\nลว. 13 ก.ย.\n2559"
    },
    {
      "nameTH": "นายอับดุลเล๊าะฮ์ มะ มิง",
      "id": "3960500206046",
      "dob": "1 กรกฎาคม 2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายแอ มะแซ",
      "id": "3960700103038",
      "dob": "16 เมษายน 2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายรอวี หะยีดิง",
      "id": "2960400008139",
      "dob": "7 พฤษภาคม 2530",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะดารี วาหลง",
      "id": "3960400260561",
      "dob": "22 มิถุนายน 2519",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลตอเละ บาเย๊าะกาเซ๊ะ",
      "id": "3960500431031",
      "dob": "20 มกราคม 2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายเตาฟีก โต๊ะเล๊าะ",
      "id": "1960100084711",
      "dob": "19 ตุลาคม 2532",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะซูปียัน ยะกูมอ",
      "id": "3950600330318",
      "dob": "18 มิถุนายน 2517",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมาหะมะ  มะเด็ง",
      "id": "3960400173341",
      "dob": "27 มีนาคม\n2524",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคลที่ถูก ก าหนดตามประกาศ \nพ.ปกร.ที่ 2/2559 \nลว. 13 ก.ย. \n2559"
    },
    {
      "nameTH": "นายมะตอเพะ มาลายอ",
      "id": "2960500035928",
      "dob": "1 กันยายน 2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอูสนี บาโด",
      "id": "1960500003700",
      "dob": "20 มกราคม 2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายบารอวี เจ๊ะแม",
      "id": "3960500669526",
      "dob": "10 ธันวาคม 2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอาบะห์ เจะอาลี",
      "id": "3950100119014",
      "dob": "15 มิถุนายน 2522",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายบูคอรี ฮะมะ",
      "id": "5950600002058",
      "dob": "3 ธันวาคม 2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะยากี บือราเฮง",
      "id": "3950100117623",
      "dob": "5 กุมภาพันธ์ 2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะนุ๊ มามะ",
      "id": "3960500083971",
      "dob": "12 พฤษภาคม 2514",
      "passportNo1": "OX0312485\n(วันหมดอายุ 08/02/2004)",
      "remark": ""
    },
    {
      "nameTH": "นายกูลี ลือบา",
      "id": "5960500023401",
      "dob": "20 ธันวาคม 2519",
      "passportNo1": "OX0319834\n(วันหมดอายุ 20/04/2004)",
      "remark": ""
    },
    {
      "nameTH": "นายกามารูดิง ขะเดหมะ",
      "id": "3950100151449",
      "dob": "20 มีนาคม 2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายลุกมัน แปแนะ",
      "id": "3960500042264",
      "dob": "30 กรกฎาคม\n2513",
      "passportNo1": "1. ON0110517\n(วันหมดอายุ 11/02/1997)\n2. OJ0308545\n(วันหมดอายุ 08/02/2002)\n3.OT0425800\n(วันหมดอายุ 21/01/2007)\n4.OC0819857\n(วันหมดอายุ 11/10/2014)",
      "remark": ""
    },
    {
      "nameTH": "นายมะยูดิง\n หะยีสะนิ",
      "id": "1960600031624",
      "dob": "8 มีนาคม\n 2529",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อบุคคลที่ถูก \nก าหนดตาม ประกาศ พ.ปกร.ที่ 3/2560 \nลว. 16 พ.ค. \n2560"
    },
    {
      "nameTH": "นายอับดุลบาซิค ยูนุ",
      "id": "1961100007482",
      "dob": "4 กันยายน\n2527",
      "passportNo1": "1. OM0429418\n(วันหมดอายุ 07/04/2006)\nชื่อ – สกุล ตามหนังสือ เดินทางฉบับนี้ คือ ABDULBASIK YUNU 2. OM0633763\n(วันหมดอายุ 03/04/2011)",
      "remark": ""
    },
    {
      "nameTH": "นายซัฟวาน\nสาแล๊ะ",
      "id": "1960600011348",
      "dob": "5 พฤษภาคม\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลเลาะ\nตาเปาะโต๊ะ",
      "id": "5950301047882",
      "dob": "29 สิงหาคม\n2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายปาตะ ลาเต๊ะ",
      "id": "1940400043327",
      "dob": "20 กุมภาพันธ์\n 2531",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ พ.ปกร.ที่ 1/2560\nลว. 5 พ.ค.\n2560"
    },
    {
      "nameTH": "นายอาหาหมัด\n ดือราแม",
      "id": "5940600017845",
      "dob": "1 มกราคม\n 2529",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ พ.ปกร.ที่ 2/2560\nลว. 5 พ.ค.\n2560"
    },
    {
      "nameTH": "นายอับดุลฮาดี\nดาหาเล็ง",
      "id": "3940900197851",
      "dob": "4 มกราคม\n2521",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะรูดิน ตาเฮ",
      "id": "1940400002884",
      "dob": "7 พฤษภาคม\n 2527",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ พ.ปกร.ที่ 5/2560\nลว. 21 ส.ค.\n2560"
    },
    {
      "nameTH": "นายต่วนยัสลัน\nนิราแม",
      "id": "1961100075089",
      "dob": "24 ธันวาคม\n2534",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายราฟี\nมามะรอยาลี",
      "id": "2961100014819",
      "dob": "19 กันยายน\n2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายยาสะ ดาแม",
      "id": "1950100004008",
      "dob": "22 มีนาคม\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายรอกิ ดอเลาะ",
      "id": "2950100016730",
      "dob": "4 เมษายน\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายสะกรี\nดอเลาะ",
      "id": "3950100571502",
      "dob": "23 พฤษภาคม\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายกอเซ็ง มิยะ",
      "id": "3950600147947",
      "dob": "18 เมษายน\n2520",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายซัยฟุลลอฮ ซาฟรุ",
      "id": "3950600175711",
      "dob": "20 มีนาคม\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายนัชดาน ยะลาแป",
      "id": "2950500009706",
      "dob": "8 กุมภาพันธ์\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฌาฮีดีน\nสาตา",
      "id": "1969800006451",
      "dob": "30 กรกฎาคม\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายซัมบรี ยุนุ",
      "id": "3950100067227",
      "dob": "17 มิถุนายน\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะซูการือนอ\n ยะกูมอ",
      "id": "3950600347636",
      "dob": "20 พฤศจิกายน\n 2526",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ พ.ปกร.ที่ 4/2560\nลว. 10 ส.ค.\n2560"
    },
    {
      "nameTH": "นายลุกมาน\nปูลาประเปะ",
      "id": "1950300017316",
      "dob": "10 มีนาคม\n2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายรูกิปลี สาอิ",
      "id": "3940200358070",
      "dob": "30 มีนาคม\n2521",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายสุกรียา เดมะ",
      "id": "3941100096953",
      "dob": "20 กุมภาพันธ์\n2521",
      "passportNo1": "T426805",
      "remark": ""
    },
    {
      "nameTH": "นายอาหามะ\nจาจ้า",
      "id": "3940500277008",
      "dob": "10 ธันวาคม\n2513",
      "passportNo1": "1. C259743\n(วันหมดอายุ 26/09/1996)\n2. I300991\n(วันหมดอายุ",
      "remark": ""
    },
    {
      "nameTH": "นายกามารูดิง\nมูซอ",
      "id": "5940200003251",
      "dob": "2 พฤศจิกายน\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลอาซิ\nแอเสาะ",
      "id": "1940500046361",
      "dob": "27 ธันวาคม\n2530",
      "passportNo1": "P721024\n(วันหมดอายุ 07/02/2013)",
      "remark": ""
    },
    {
      "nameTH": "นายมาโซ\nสาและเร๊ะ",
      "id": "3950600328828",
      "dob": "1 มกราคม\n2518",
      "passportNo1": "I351445\n(วันหมดอายุ 11/09/2002)",
      "remark": ""
    },
    {
      "nameTH": "นายซูลายมัน\nยะกูมอ",
      "id": "5950600018035",
      "dob": "16 มีนาคม\n2523",
      "passportNo1": "E483255\n(วันหมดอายุ 27/09/2005)",
      "remark": ""
    },
    {
      "nameTH": "นายอายุ อัตนิ",
      "id": "5950699000834",
      "dob": "5 กุมภาพันธ์\n2516",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะกะตา\nแวอาแซ",
      "id": "5950600018973",
      "dob": "22 มีนาคม\n2517",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอายุ\nหะยีบากา",
      "id": "5950600026241",
      "dob": "15 มกราคม\n2520",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายกูอาสมัน ดอฆอ",
      "id": "3950100528046",
      "dob": "15 ธันวาคม\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายสูดิรมาน\nจารง",
      "id": "3950600094991",
      "dob": "8 พฤษภาคม\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายนาวาวี ลาโบปี",
      "id": "1941000089169",
      "dob": "28 ตุลาคม\n2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะซอเร\nดือรามะ",
      "id": "3940100174422",
      "dob": "7 กรกฎาคม\n2522",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮาหมัดนูรูดิง มะรอแม",
      "id": "3940100115434",
      "dob": "9 กันยายน\n2518",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮ ามัด\n ซีตีเลาะ",
      "id": "3940100401941",
      "dob": "7 ธันวาคม\n 2523",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ พ.ปกร.ที่ 6/2560\nลว. 6 ก.ย.\n2560"
    },
    {
      "nameTH": "นายฮาพีซี\nยูโซะ",
      "id": "3950100106737",
      "dob": "19 กรกฎาคม\n2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะกอเซ็ง เจ๊ะมะ",
      "id": "3950100175151",
      "dob": "8 มกราคม\n2521",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมาหามะ แมเร๊าะ",
      "id": "2960600016577",
      "dob": "1 มกราคม\n2530",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายซอบรี\n บือแน",
      "id": "3940500209142",
      "dob": "19 ธันวาคม\n 2526",
      "passportNo1": "",
      "remark": "เพิกถอนออก\nจากรายชื่อ บุคคล ที่ถูกก าหนด ตามประกาศ\"พ.ปกร.ที่ \n1/2561 \nลว. 21 พ.ค. \n2561"
    },
    {
      "nameTH": "นายรุสลัน\nใบมะ",
      "id": "3900300487696",
      "dob": "21 กันยายน\n2523",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายซัมซูดิง บาเหม",
      "id": "3900600233754",
      "dob": "3 พฤศจิกายน\n2521",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอารง สะอิ",
      "id": "3900600237580",
      "dob": "17 กุมภาพันธ์\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายเมาลานา\nสาเมาะ",
      "id": "1940300012861",
      "dob": "14 กันยายน\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายเสรี แวมามุ",
      "id": "1949900023927",
      "dob": "21 เมษายน\n2528",
      "passportNo1": "V447810\n(วันหมดอายุ 27/02/2007)",
      "remark": ""
    },
    {
      "nameTH": "นายรอมลี รอแน็ง",
      "id": "1940300016379",
      "dob": "20 มกราคม\n2528",
      "passportNo1": "V449148\n(วันหมดอายุ 04/03/2007)",
      "remark": ""
    },
    {
      "nameTH": "นายจ าเริญ อูมาสะ",
      "id": "3900500097107",
      "dob": "2 ตุลาคม 2516",
      "passportNo1": "I318062\n(วันหมดอายุ 16/12/1998)",
      "remark": ""
    },
    {
      "nameTH": "นายมะรีเป็ง มะหะ",
      "id": "3940900550534",
      "dob": "18 พฤษภาคม\n2522",
      "passportNo1": "R379524\n(วันหมดอายุ 10/04/2005)",
      "remark": ""
    },
    {
      "nameTH": "นายมะยูนิ สาแม",
      "id": "3940900508813",
      "dob": "17 กันยายน\n2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายดอเลาะ ดอเลาะ",
      "id": "3940900560262",
      "dob": "16 เมษายน\n2510",
      "passportNo1": "1. K213406\n(วันหมดอายุ 17/10/1998)\n2. J324069\n(วันหมดอายุ 19/08/2002)\n3. S688892\n(วันหมดอายุ 08/10/2008)",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลอายิ ดอเลาะ",
      "id": "3940900560343",
      "dob": "30 ตุลาคม\n2525",
      "passportNo1": "1. M536377\n(วันหมดอายุ 10/10/2007)\n2. C799218\n(วันหมดอายุ 08/10/2008)",
      "remark": ""
    },
    {
      "nameTH": "นายวาครี มะโระ",
      "id": "3940900506268",
      "dob": "1 มกราคม\n2507",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอะมะ ดือเระห์",
      "id": "3940900522565",
      "dob": "21 มิถุนายน\n2521",
      "passportNo1": "R322226\n(วันหมดอายุ 11/02/2003)",
      "remark": ""
    },
    {
      "nameTH": "นายนูรอดิง อับดุลลาตี",
      "id": "3960400028880",
      "dob": "1 กันยายน\n2520",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอาดือนัน เจะอาแซ",
      "id": "3940900564641",
      "dob": "1 มกราคม\n2515",
      "passportNo1": "",
      "remark": "1. N115628\n(วันหมดอายุ 16/02/1997) 2. x316086(วันหมดอายุ 08/10/2008)"
    },
    {
      "nameTH": "นายมะยากี เจะฮิง",
      "id": "3940900387790",
      "dob": "23 เมษายน\n2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลการิม สาแม",
      "id": "1940900023422",
      "dob": "20 กรกฎาคม\n2528",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอัซรัน\nอาแวบือซา",
      "id": "1960500237859",
      "dob": "2 กรกฎาคม\n2536",
      "passportNo1": "P725737\n(วันหมดอายุ 21/02/2013)",
      "remark": ""
    },
    {
      "nameTH": "นายดือราพา เจ๊ะอูมา",
      "id": "1960500017913",
      "dob": "15 กันยายน\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอิสยาซะห์\nหะแย",
      "id": "5940100018842",
      "dob": "17 พฤษภาคม\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายซารีซานอัมรี\nดือราแม",
      "id": "3900600190397",
      "dob": "15 กุมภาพันธ์\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายนูรุดดิน\nกาจะลากี",
      "id": "3950300049087",
      "dob": "9 มกราคม\n2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายสุไลมาน สาเหมาะ",
      "id": "3940200650048",
      "dob": "5 ตุลาคม 2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮาหมัดนูร ตูหยง",
      "id": "5941000022903",
      "dob": "",
      "passportNo1": "25 พฤษภาคม\n2533",
      "remark": "V791823\n(วันหมดอายุ 05/10/2013)"
    },
    {
      "nameTH": "นายฮิลมี ยะลาแป",
      "id": "2950500009714",
      "dob": "",
      "passportNo1": "11 พฤศจิกายน\n2527",
      "remark": ""
    },
    {
      "nameTH": "นายอับดลเล๊าะ สาเม๊าะ",
      "id": "3900600227444",
      "dob": "",
      "passportNo1": "5 กุมภาพันธ์\n2517",
      "remark": ""
    },
    {
      "nameTH": "นายมะรอพี\nบือราเฮง",
      "id": "3961200139662",
      "dob": "",
      "passportNo1": "13 พฤศจิกายน\n2524",
      "remark": "H550764\n(วันหมดอายุ 04/02/2008)"
    },
    {
      "nameTH": "นายมะรอมือลี\nกาแจกาซอ",
      "id": "3960300333664",
      "dob": "16 ธันวาคม\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะรุดิง สามะ",
      "id": "3940300184260",
      "dob": "2 เมษายน\n2524",
      "passportNo1": "W640889\n(วันหมดอายุ 25/10/2011)",
      "remark": ""
    },
    {
      "nameTH": "นายมนูญ ชัยชนะ\nหรืออเนก หรือเอนก หรือต๋อย หรือต้อย ซานฟราน",
      "id": "3100601119863",
      "dob": "15 ตุลาคม\n2498",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮ ามัด ขาเดร์",
      "id": "3960500499299",
      "dob": "5 กรกฎาคม\n2520",
      "passportNo1": "I369615\n(วันหมดอายุ 24/12/2002)",
      "remark": ""
    },
    {
      "nameTH": "นายอาแว สามะ",
      "id": "1960500142050",
      "dob": "2 มีนาคม 2532",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมาหะมะซอบือรี\nกรือสง",
      "id": "3960500370652",
      "dob": "11 มีนาคม\n2523",
      "passportNo1": "H429909\n(วันหมดอายุ 13/07/2005)",
      "remark": ""
    },
    {
      "nameTH": "นายอารง ดือราแม",
      "id": "3960500462832",
      "dob": "1 มกราคม\n2511",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายไรนาอาบีเด็ง\nสามะ",
      "id": "3960100366194",
      "dob": "22 พฤษภาคม\n2524",
      "passportNo1": "1. M419238\n(วันหมดอายุ 18/01/2006) 2. J634685\n(วันหมดอายุ 15/12/2010)",
      "remark": ""
    },
    {
      "nameTH": "นายอัฎฮาร์ ยูกะ",
      "id": "1960300062176",
      "dob": "27 มิถุนายน\n2533",
      "passportNo1": "N981277\n(วันหมดอายุ 06/06/2016)",
      "remark": ""
    },
    {
      "nameTH": "นายดาราแมง อูมา",
      "id": "1960300006306",
      "dob": "12 สิงหาคม\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลเลาะ อูแล",
      "id": "2960300011120",
      "dob": "11 พฤษภาคม\n2528",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายคอเละ เซ็ง",
      "id": "1960400032007",
      "dob": "5 มิถุนายน\n2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮ ามัดใบฮาดี\nวาเฮ็ง",
      "id": "3961100347921",
      "dob": "10 สิงหาคม\n2515",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายฮากีม ดอเลาะ",
      "id": "1940100006302",
      "dob": "17 มีนาคม\n2527",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูหาหมัดยากี\nสาและ",
      "id": "5950300029139",
      "dob": "1 กุมภาพันธ์\n2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอัสมีน\nกาเต็มมาดี",
      "id": "1949900081749",
      "dob": "18 กรกฎาคม\n2530",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอัมมัร แวดาราแม",
      "id": "1940900113006",
      "dob": "18 เมษายน\n2531",
      "passportNo1": "1. C756983\n(วันหมดอายุ 17/04/2012) 2. AA1774201\n(วันหมดอายุ 09/06/2018)",
      "remark": ""
    },
    {
      "nameTH": "นายรอแปะอิง อุเซ็ง",
      "id": "5960600017730",
      "dob": "9 กันยายน\n2512",
      "passportNo1": "E241352\n(วันหมดอายุ 01/02/1998)",
      "remark": ""
    },
    {
      "nameTH": "นายอาซูวัน ลุมโมะ",
      "id": "2950800001468",
      "dob": "20 กุมภาพันธ์\n2533",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "น า ย อ า ห ะ มั ด\nลือแมซา",
      "id": "3941000308045",
      "dob": "24 มีนาคม\n2523",
      "passportNo1": "X322228\n(วันหมดอายุ 17/05/2004)",
      "remark": ""
    },
    {
      "nameTH": "นายมารซูกี ยูโสะ",
      "id": "3950300053459",
      "dob": "30 ธันวาคม\n2514",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "น า ย ม า ห ะ ม ะ อาหลีดองอย",
      "id": "2950800006222",
      "dob": "1 มกราคม\n2538",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายเฟาซี กลูแป",
      "id": "3950300233279",
      "dob": "10 ตุลาคม\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายสือดี ปูเดะ",
      "id": "1960500112991",
      "dob": "26 กันยายน\n2530",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะเซาฟี ตีโม",
      "id": "1960500096783",
      "dob": "24 ธันวาคม\n2529",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮามะซูลกีฟลี สือแม",
      "id": "3960500342284",
      "dob": "7 ธันวาคม\n2520",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมูฮ าหมัดซากีรีน สาแม",
      "id": "1961200061853",
      "dob": "5 เมษายน\n2535",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "น า ย อั บ ดุ ล ก า รี หะแว",
      "id": "3950600289903",
      "dob": "8 มกราคม\n2516",
      "passportNo1": "1. A247776\n(วันหมดอายุ 16/07/2000) 2. M445149\n(วันหมดอายุ 15/07/2006)",
      "remark": ""
    },
    {
      "nameTH": "นายมาฮาดี มะเก",
      "id": "1969900096088",
      "dob": "14 กันยายน\n2531",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "น า ย อั บ ดุ ล เล า ะ บูละ",
      "id": "2961200018689",
      "dob": "1 มกราคม\n2528",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอาสือรี เจ๊ะตู",
      "id": "2961200006664",
      "dob": "1 มกราคม\n2526",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอับดุลเลาะ ตีงี",
      "id": "3960500651228",
      "dob": "3 ตุลาคม 2518",
      "passportNo1": "T292283\n(วันหมดอายุ 17/07/2001)",
      "remark": ""
    },
    {
      "nameTH": "นายอาลียะห์ เจะเตะ",
      "id": "3960100049881",
      "dob": "27 กันยายน\n2518",
      "passportNo1": "R386583\n(วันหมดอายุ 06/03/2005)",
      "remark": ""
    },
    {
      "nameTH": "นายรอยาลี อาแว",
      "id": "3960500361238",
      "dob": "21 กันยายน\n2523",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอูเซ็ง มามะ",
      "id": "2960500007096",
      "dob": "1 มกราคม\n2524",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายอายิ ยามา",
      "id": "2960500021617",
      "dob": "20 กุมภาพันธ์\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายซอและ มะเซ็ง",
      "id": "3941100060681",
      "dob": "7 มกราคม\n2525",
      "passportNo1": "",
      "remark": ""
    },
    {
      "nameTH": "นายมะรอยาลี สะนิ",
      "id": "3960200435295",
      "dob": "8 กรกฎาคม\n2525",
      "passportNo1": "",
      "remark": ""
    }
  ]
]]

data = cjson.decode(new_amlo)
for k,v in ipairs(data) do
    print(cjson.encode(data[k]))
end
