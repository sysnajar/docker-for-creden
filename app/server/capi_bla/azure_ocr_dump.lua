#!/usr/bin/env lua
local cjson = require 'cjson.safe'

json = io.read()
t = cjson.decode(json)
ret = {success = false, err = "Invalid response from server", texts={}}

if t and t.recognitionResult and t.recognitionResult.lines then 
   	for i, line in ipairs(t.recognitionResult.lines) do
	     for i2, w in ipairs(line.words) do
			 table.insert(ret.texts, w.text)	
		 end
	end
	ret.success = true
	ret.err = nil
end


ret.text_count = #ret.texts
--ret.ocr_json = json
if(ret.text_count<1) then ret.texts = nil end
print(cjson.encode(ret))

