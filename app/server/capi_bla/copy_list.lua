#! /usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(#arg ~= 2) then print('Incorrect parameter size , requires 2') ; os.exit(1); end

-- copy 2 redis lists
local t = rc:lrange(arg[1], 0, -1)
for i,v in ipairs(t) do
    rc:rpush(arg[2], v)
end
