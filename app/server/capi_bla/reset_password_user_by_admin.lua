#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local common = require "common"
local scrypt = require "resty.scrypt"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"}) 

if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body   = {
         admin = { company_id = "5c80af657695fdd90b9fe28c",
                   email = "thanaphas@gmail.com",
                   password = "1"
                },
        user = { email = "got1@creden.co",
                 new_password = "87654321"
            } 
    } 
end

-- check session admin
    function chk_session_admin(admin)
        local chk = false
        local admin_username  = session.data.username
        
        if admin.email == admin_username then
            chk = true
        end
        return chk
    end

-- check role and member of company
    function chk_member_company(admin)
        local chk = false
        db = conn:new_db_handle("dopa")
        col = db:get_col("company_members")
        company,err = col:find_one({ email = admin.email })

        if tostring(company.company_id) == admin.company_id and company.role == 'ADMIN' then
            chk = true
        end

        return chk
    end

-- check password of admin
    function chk_password_admin(admin)
        local chk = false
        db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        user_admin, err = col:find_one({ adminEmail = admin.email })
        hash_password_admin = scrypt.check(admin.password, user_admin.hashedPassword)
        if hash_password_admin then
            chk = true
        end
        return chk
    end

-- check company and role of user
    function chk_role_company_user(user,admin)
        local chk = false
        db = conn:new_db_handle("dopa")
        col = db:get_col("company_members")
        company,err = col:find_one({ email = user.email })

        if company.role == 'USER' and tostring(company.company_id) == admin.company_id then
            chk = true
        end

        return chk
    end


    conn = mongo()
    conn:set_timeout(5000)
    ok, err = conn:connect('127.0.0.1',27017)
    
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {}
        local sess = chk_session_admin(body.admin)
        local member = chk_member_company(body.admin)
        local password = chk_password_admin(body.admin)
        local role_user = chk_role_company_user(body.user,body.admin)

        if sess and member and password and role_user then

            db = conn:new_db_handle("dopa")
            col = db:get_col("esig_user")
            selector = { adminEmail = body.user.email }

            local new_hash = scrypt.crypt(tostring(body.user.new_password))
            update_cmd = {["$set"] = { hashedPassword = new_hash }}
            u, err = col:update(selector, update_cmd, 0, 0, true)

            if u then
                ret = { success = true, error_msg = "reset password complete" }
            else
                ret = { success = false, error_msg = "เปลี่ยนรหัสผ่านไม่สำเร็จ" }
            end

        else
            ret = { success = false, error_msg = "ไม่มีสิทธิ์การเข้าถึงตรงส่วนนี้" }
        
        end
        -- ret = { success = true , sess = sess, member = member, password = password, role_user = role_user }

        print(cjson.encode(ret))
    end




   
