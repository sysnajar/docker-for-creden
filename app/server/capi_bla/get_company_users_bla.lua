#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}
local data = {}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()    
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("esig_user")
    local col_address_book = db:get_col("address_book")
    i, err = col:find({ company_id = body.company_id })

    if err then
        ret = { success = false, error_msg = "find member unsuccess", error_code = "2001" } 
    else

        local list = {}
        local myusername = session.data.username
        local myRole = "USER"

        for k, v in i:pairs() do 
            ngx.log(ngx.INFO, "Got AAAAAAAAAAAAAAAAAAAAA")
            local pos_t = {}
            v._id = tostring(v._id)
            v.company_id = tostring(v.company_id)

            if myusername == v.adminEmail then
                myRole = v.role
            end
            
            local user, err_user = col_address_book:find_one({ email = v.adminEmail, imported = true })
            if user then
                v.fName = user.EmpName or ""
                v.lName = user.EmpSurname or ""
                v.OrgDescription = user.OrgDescription or ""
            else
                v.fName = v.adminFisrtname or ""
                v.lName = v.adminLastname or ""
                v.OrgDescription = ""
            end
            v.email = v.adminEmail or ""
            -- ngx.log(ngx.INFO, "Got GGGGGGGGGGGGG")
            for k1, v1 in ipairs(v.positions or {}) do 
                v1.position_id = tostring(v1.position_id)
                table.insert(pos_t, v1.position_name) 
            end
            -- ngx.log(ngx.INFO, "Got HHHHHHHHHHHHHHHHHHHHHHHHHHH")
            v.positions_name = pos_t
            v.fav = false
            table.insert(list, v) 
        end
        ret = { success = true, error_msg = "find member success", error_code = "2002", data = list, myRole = myRole } 

    end
end
print(cjson.encode(ret))