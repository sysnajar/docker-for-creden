#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
redis = require 'redis'

function do_blockchain(u)
	local bc_tx = rc:get('bc:' .. tostring(u.id))	
	if(bc_tx and bc_tx~='')then
	  u.bc_url = 'https://steexp.com/tx/' .. bc_tx
	  u.bc_tx = bc_tx
	else
	  u.bc_url = false
	end
end


rc = redis.connect('127.0.0.1', 6379)
print = ngx.say
body = ngx.req.get_uri_args()
local can_read = false
--prevent token param
if(not body.token ) then
	print(cjson.encode({success = false, error_msg = "session expired"}))
	ngx.exit(ngx.HTTP_OK)
end	
--prevent token param

ctx   = ctx or common.mongo_ctx()
col = ctx.col["edoc_list"]
ngx.log(ngx.NOTICE, 'token:'.. body.token ..'id:'..body.id)
u, err = col:find_one({id=body.id, signers={['$elemMatch']={signer_session=body.token}}})

-- check use_sign_order flag
if(u.use_signing_order == nil) then
    u.use_signing_order = false
    u.current_order = 1
end  
--

u._id = nil
u._company = nil
u.time = (u.createdDtm) and (os.date('%d/%m/%Y %H:%M:%S', tonumber(u.createdDtm)/1000  + (7 * 60 * 60) )) or 'N/A'
do_blockchain(u) 

local col2 = ctx.col["dopa.esig_user"]             
local user_data = col2:find_one({adminEmail = u.owner})   
u.owner_name = user_data.adminFisrtname or ''

--read permission check
for k, signer in ipairs(u.signers) do
    if(signer.email and signer.email ~='') then 
        -- specified token
        if(signer.signer_session==body.token) then 
            u.session_email = signer.email
            can_read = true 
        end 
    end
end


for k, v in ipairs(u.fields) do 
    if v.time and v.time ~= '' and v.time ~= 0 then
        local format="(%d+)/(%d+)/(%d+) (%d+):(%d+):(%d+)"
        local mon,day,year,hour,min,sec = tostring(v.time):match(format)
        local dt = {year=tonumber('20'..year), month=tonumber(mon), day=tonumber(day), hour=tonumber(hour), min=tonumber(min), sec=tonumber(sec)}
        v.time2 = os.time(dt)
        v.time3 = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.time2)  + (7 * 60 * 60) )
    end
end

if(can_read==true)then
    ret = {success = true, error_msg = err, data = u, id = body.id}
else
    ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(body.token) ..'try to access docid#  ' .. tostring(body.id) )
end
--footer
            
print(cjson.encode(ret))
