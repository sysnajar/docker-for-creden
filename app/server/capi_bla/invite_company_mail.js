var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = process.env.HOST_NAME + "/esign/images/Creden_logo.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/esign/web/policy.html"
  var json = JSON.parse(process.argv[2])
  var r = json.admin
  var to_member = json.email
  var company = json.company || "บริษัท / company"
  if (!r.adminLastname) r.adminLastname = ""
  var fullname = r.adminFisrtname + " " + r.adminLastname
  var link_invite = process.env.HOST_NAME + "/esign/web/invite.html?email="+ to_member +"&rd=" + new Date().getTime()
  var html = `
  <!DOCTYPE html>
  <html lang="th-TH" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
  </head>
  <body style="background: #E0E0E0;"><br>
    <table border="0" cellspacing="0" cellpadding="0" style="background: #ffffff;margin: 0px auto 15px auto;padding: 10px 20px;width:`+ wTable +`;">
      <tr>
          <td>
            <div style="max-width: 160px;">
              <img src="`+ logo +`" alt="logo website" style="width: 100%;">
           </div>
          </td>
      </tr>
      <tr>
        <td style="width: 100%;text-align: center;color: #ffffff;padding: 0px 30px 50px 30px;background: `+ bg +`;">
          <div>
            <img src="`+ process.env.HOST_NAME +`/esign/images/mail/mail_invite.png" style="margin-top: 30px;">
          </div>
          <div style="margin-top:15px;font-size: 16px;">
            Click to accept an invitation
          </div>
          <div style="margin-top:10px;font-size: 16px;">
            คลิกที่นี่เพื่อตอบรับคำเชิญเข้าบริษัท
          </div>
          <div style="margin-top:30px;">        
              <a href="`+ link_invite +`" style="text-decoration: none;">   
                 <div style="color: #4F4F4F;font-size: 16px;font-weight: 500;background: #F2C94C;padding: 5px 30px;width: 120px;margin: 0px auto;">
                    Accept
                    <br>
                    ตอบรับเข้าร่วม
                  </div>
              </a>
          </div>
        </td>
      </tr>
      <tr>
        <td style="color: #222222;font-size:12px;">
          <div style="margin-top:15px">
            <b>เรียน&nbsp;`+ to_member +`</b>  
          <br>
            คุณ `+ fullname +`(`+ r.adminEmail  +`) ได้เชิญให้คุณเข้าร่วม “`+ company +`”
          </div>
          <div style="margin-top:15px;margin-bottom: 30px;">
            <b>Dear, &nbsp;`+ to_member +`</b>   
            <br>
            You have been invited by `+ fullname +`(`+ r.adminEmail +`) to join the company account “`+ company +`”. To accept this invitaion click the above button.
          </div>
        </td>
      </tr>
  </table>
  <table border="0" cellspacing="0" cellpadding="0" style="margin:auto;padding: 10px 20px;color: #222222;font-size:12px;width:`+ wTable +`;">
    <tr>
      <td>
        <div>
          <b>หมายเหตุ</b> : โปรดอย่าตอบกลับอีเมลฉบับนี้ เนื่องจากเป็นอีเมลจากระบบอัตโนมัติ
        </div>
        <div style="margin-top:5px">
          หากต้องการสอบถามข้อมูลเพิ่มเติมโปรดติดต่อฝ่ายบริการลูกค้า <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
        </div>
        <div>
          <hr style="width: 100%;margin-top: 20px;background: #BDBDBD;">
        </div>
        <div style="margin-top:15px">
          <b>Note</b> : This is an automated message. Please do not reply directly to this email.
        </div>
        <div style="margin-top:5px;margin-bottom: 5px;">
          For more information, please contact the application's support team: <a href="mailto:support_eks@creden.co">support_eks@creden.co</a>
        </div>
      </td>
    </tr>
  </table>

  <table border="0" cellspacing="0" cellpadding="0" style="margin:0px auto auto auto;color: #000000;font-size:12px;width:100%;background: #F0F0F0;">
    <tr style="background: #FFF;"><td style="height: 10px"></td></tr>
    <tr>
      <td style="text-align:center;padding-top:15px;">
        Creden Asia Co., Ltd. 9/22 Ratchapruek Road, Bang Ramat Subdistrict, Taling Chan District 10170, <span style="color: #2F80ED">095-374-8973</span>
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding-top: 10px;">
        <a target="_blank" style="text-decoration: none;" href="`+ home +`">www.creden.co</a> 
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding-top: 10px;">
        <a target="_blank" style="text-decoration: none;" href="`+ policy +`">Privacy Policy</a> 
      </td>
    </tr>
    <tr>
      <td style="text-align: center;padding: 10px 0px 15px 0px;">
        © 2021, Creden Asia Co., Ltd. All Rights Reserved 
      </td>
  </tr>
  </table>
  </body>
  </html>
  `

  var smtp = {
    // host: 'smtp.gmail.com', //set to your host name or ip
    host: 'smtp.mailgun.org', //set to your host name or ip
    port: 465, //25, 465, 587 depend on your 
    auth: {
      // user: 'credenmailer@gmail.com', //user account
      // // pass: 'creden2018' //user password
      // pass: 'dnkrakkaxnmnakpr'
      user: 'postmaster@mg.creden.co', //user account
      pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'CREDEN <credenmailer@gmail.com>', //from email (option)
    to: to_member, //to email (require)
    cc: '',
    subject: '[Creden] Company Account Invitation', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


