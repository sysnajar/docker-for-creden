#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local HOST_NAME = os.getenv("HOST_NAME")
--local prog2 = exec.new('/tmp/exec.sock')
--prog2.timeout_fatal = false

local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], id = arg[2]} 
end

function trim5(s)
	  return s:match'^%s*(.*%S)' or ''
end

function dob_line(txt)
local dob_line  = nil
local i = 0
local c = {}
for line in string.gmatch(txt, '[^\r\n]+') do

    i = i + 1
    
    if(string.find(line, 'Date')~=nil or string.find(line, 'Birth')~= nil) then
       dob_line = i+3
    end

    if(dob_line == nil and string.find(line, 'ศาสนา')~=nil) then
        if(trim5(line) == 'ศาสนา')then
		    dob_line = i + 3
		else
		    dob_line = i + 2
		end	
    end
    table.insert(c, line)
end

return c, dob_line
end

local t = {success = false, err = 'N/A', card_id = 'N/A2', face = 'N/A3'}
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
--rc:set('tmp_card2', body.img)
--body.id = 'dob'
--base64 to image
--name = "1518578464"
name = body.img
rc:set('tmp_card2', body.img)
local path = "/home/creden/work/app/client/face/images/card/"..name..".png";
local path_emp = "/home/creden/work/app/client/face/images/card/"..name.."_emp.png";
local path_out = "/home/creden/work/app/client/face/images/card/"..name.."_out.png";
if body.is_mobile == true then
    local res, err = prog('sh', '/home/creden/work/app/server/capi/img.sh', path, path_emp, path_out)
else
    local res, err = prog('sh', '/home/creden/work/app/server/capi/img_pc.sh', path, path_emp, path_out)    
end
t_province = {}
local res, err = prog('cat', '/home/creden/work/app/server/capi/province.txt')
if not err then
  str = string.gsub(res.stdout, "%s+", "")
  str = '{'..str..'}'
  t_province = cjson.decode(str)
else
  print(err)
end

province = ''
t.sex = ''
local res, err = prog('sh', '/home/creden/work/app/server/capi/ocr_google.sh',HOST_NAME..'/face/images/card/' .. name ..'_out.png')
if not err then
    ocr = cjson.decode(res.stdout)
    if ocr.responses[1].fullTextAnnotation then

        long_text = ocr.responses[1].fullTextAnnotation.text
        long_text = string.gsub(long_text, "%s+", "")
        --print(long_text)
        a = string.match(long_text, '%d%d%d%d%d%d%d%d%d%d%d%d%d')
        dob = string.match(long_text, '%d%d%a%a%a%d%d%d%d') or string.match(long_text, '%d%d%a%a%a%.%d%d%d%d') or string.match(long_text, '%d%a%a%a%.%d%d%d%d') or string.match(long_text, '%d%a%a%a%d%d%d%d') or string.match(long_text, '%d%d%a%a%a,%d%d%d%d') or string.match(long_text, '%d%a%a%a,%d%d%d%d')  or 'bad format'
        --print('long_text',long_text)
        --print('dob',dob)
        dob_th = string.gsub(long_text,dob,'')
        dob_th = string.match(long_text, '.25%d%d') or string.match(long_text, '.25%d%d') or string.match(long_text, '.25%d%d') or string.match(long_text, '.25%d%d')  or 'bad format'
        --print('dob_th',dob_th)

        dd = ''
        mm = ''
        yy = ''
        if dob ~= 'bad format' then
            dd_th = string.gsub(dob_th,"%.","")
            dob = string.gsub(''..dob,",","")
            dob = string.gsub(''..dob,"%.","")
            dd = string.gsub(dob,"%.","")
            yy = dd:sub(-4)

            yy_th = ''
            if dd_th and dd_th ~= 'bad format' then
                yy_th = dd_th:sub(-4) - 543
                yy_th = ''..yy_th
            end 
         
            dd = string.gsub(dd,yy,"")
            mm = dd:sub(-3)
            dd = string.gsub(dd,mm,"")
            dd = ((string.len(dd) == 1) and '0'..dd or dd)
            if yy_th ~= yy then
                t.dob = {day = dd, mon = mm, year = ''}
            else
                t.dob = {day = dd, mon = mm, year = yy_th+543}
            end
        else
            t.dob = {day = dd, mon = mm, year = yy}
        end

        t.card_id = a
        t.success = true

        long_text = ocr.responses[1].fullTextAnnotation.text
        --print(long_text)
        row_address = 1
        long_text = string.gsub(long_text, "ทีอยู่\n", "ที่อยู่ ")
        long_text = string.gsub(long_text, "ที่อยู\n", "ที่อยู่ ")
        long_text = string.gsub(long_text, "ที่อย่\n", "ที่อยู่ ")
        long_text = string.gsub(long_text, "ที่อยู่\n", "ที่อยู่ ")
        for i in string.gmatch(long_text, "[^\r\n]+") do
            if row_address == 2 then
                t_add = string.gmatch(i,"%S+")
                province = ''
                for j in t_add do
                province = j
                end
                break
            end 

            if string.match(i, "ทีอยู่") or string.match(i, "ที่อยู") or string.match(i, "ที่อย่") or string.match(i, "ที่อยู่") then
                row_address = 2
            end

            if string.match(i, "Mr.") or string.match(i, "Mr") then
                t.sex = 'male'
            elseif string.match(i, "Ms") or string.match(i, "Ms.") or string.match(i, "Mrs") or string.match(i, "Mrs.") or string.match(i, "Miss") then
                t.sex = 'female'
            end 
        end
    end
    if province == '' then
        if ocr.responses[1].fullTextAnnotation then
            local t_line, line = dob_line(ocr.responses[1].fullTextAnnotation.text)
            if(line)then
                
                t_add = string.gmatch(t_line[line],"%S+")
                province = ''
                for j in t_add do
                    province = j
                end
            end 
        end       
    end 

else
    print(err)
end

if province ~= '' then
  go_google = true
  for k,v in pairs(t_province) do
    if string.match(k, province) then
        go_google = false
        
        t.province = v
      break
    end
  end

  if go_google then
    local res, err = prog('xvfb-run', '-a', 'node', '/home/creden/google_chk_word.js',province)
    if not err then
      province_google = string.gsub(res.stdout, "%s+", "")
      if t_province[province_google] then
        os.execute('echo \',"'..province..'":"'..t_province[province_google]..'"\' >> /home/creden/work/app/server/capi/province.txt')
        
        t.province = t_province[province_google]
      else
        t.province = ''
      end   
    else
      t.province = ''
    end
  end
else
  t.province = ''
end

local face, err = prog('node', '/home/creden/work/app/server/capi/test_curl.js', name)
if face.stdout then
	local face_arr = cjson.decode(face.stdout)
	if(#face_arr > 0)then
         prog.stdin = face.stdout
         local res2, err2 = prog('sh', '~/scripts/facebox.sh')
      
	end

    t.face = face_arr
end
print(cjson.encode(t))

