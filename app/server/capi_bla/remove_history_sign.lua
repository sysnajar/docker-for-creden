#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local object_id = require "resty-mongol.object_id"
-- bson = require "resty-mongol.bson"
local body = nil

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {owner='amorrut@creden.co',email='pat3@creden.co' }
end



conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else    
    db = conn:new_db_handle("dopa")
    col = db:get_col("history_signer")

    
    f,err = col:find_one({ owner = body.owner,email=body.email})

    if f then
        i, err = col:delete({ owner = body.owner,email=body.email},1,0)
        if i then
            ret = {error_msg = true, data = "Remove  success"}
        else
            ret = {error_msg = false , data = "Not remove "}
        end
    else
        ret = {error_msg = "No History"}
    end
end
print(cjson.encode(ret))
