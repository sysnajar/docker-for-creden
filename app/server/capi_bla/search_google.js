const exec = require("child_process").exec
const jsdom = require("jsdom");
const { JSDOM } = jsdom;



exec('curl -A "User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/44.0.2403.89 Safari/537.36" https://www.google.com/search?q='+process.argv[2], (error, stdout, stderr) => {
 //do whatever here
    //console.log(stdout);
  const dom = new JSDOM(stdout);
  var g = dom.window.document.getElementsByClassName("g"); // "Hello world"
  items = []
  for (var i=0, max=g.length; i < max; i++) {
    r = g[i].getElementsByTagName("h3"); // title
    a = g[i].getElementsByTagName("a"); // url
    s = g[i].getElementsByClassName("s"); // Desc
    if(r[0] && s[0] && a[0]){
      if(!a[0].href.includes('smelink')){
        title = r[0].textContent.replace(/(\r\n\t|\n|\r\t)/gm,"").trim()  
        link = a[0].href  
        desc = s[0].textContent.replace(/(\r\n\t|\n|\r\t)/gm,"").trim() 
        item = {"title":title,"desc":desc,"link":link}
        items.push(item) 
      }     
    }
  }
  console.log(JSON.stringify(items))
})