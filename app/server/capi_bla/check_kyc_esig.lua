#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end
if(ngx)then 
    ngx.req.read_body()
    print = ngx.say
	--body  = ngx.req.get_post_args()
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {adminEmail="kitchinvuttinunt@gmail.com"} 
end
    user_data = {}
    user_data2 = {}
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
        print(cjson.encode(user_data))
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("esig_user")
        col_stamp = db:get_col("user_company")

        if(body.adminEmail~=session.data.username) then
            ngx.log(ngx.NOTICE,"DETECT FRUAD user session [" .. tostring(session.data.username) ..'] try to impersonate [' .. tostring(body.adminEmail) ..']' )
            body.adminEmail = session.data.username
			ngx.exit(403)
        end


        user_data = col:find_one({adminEmail = body.adminEmail})
        user_data._id = tostring(user_data._id);
        if user_data.last_kyc_dtm then
            user_data.last_kyc_dtm = os.date('%d/%m/%Y %H:%M:%S', tonumber(user_data.last_kyc_dtm)/1000  + (7 * 60 * 60) )
			user_data.hashedPassword = nil
			if(user_data.txtPid and #user_data.txtPid==13)then user_data.txtPid = user_data.txtPid:sub(1,9) ..'XXXX'  end 
			if(user_data.cvv_code  and #user_data.cvv_code==12)then user_data.cvv_code = user_data.cvv_code:sub(1,8) ..'XXXX'  end 
            
        end
        local db2 = conn:new_db_handle("dopa")
        local col2 = db2:get_col("ekyc")
        local all_data = {}
        --  user_data2 = col2:find_one({adminEmail = body.adminEmail})
        -- user_data.xxxx = user_data2
        -- user_data2._id = tostring(user_data2._id);
        user_data2 = col2:find({adminEmail=body.adminEmail})
        user_data2:limit(1)
        result = user_data2:sort({createdDtm=-1})

        for i,v in result:pairs() do
            -- count = count + 1
            -- -- if(count <= 1000)then
                v._id = nil
            --     print(v.JP_NO)
                user_data.ekyc_level = v.ekyc_level
                user_data.ekyc_level_desc = v.ekyc_level_desc

                -- table.insert( all_data,v )
            -- -- end
        end
        
        
        if user_data.last_cert_info then
            user_data.last_cert_info = {}
        end
        
        -- get company stamp
        local query = { _id = object_id.new(convertHexStringToNormal(session.data.company_id))}
        stamp, stamp_err = col_stamp:find_one(query)
        if stamp and stamp.company_stamp then
            user_data.company_stamp = stamp.company_stamp
        end

        
        -- convert2.ekyc_level = all_data
        ret.success = true
        ret.data = user_data
        -- ret.data2 = all_data
       

        
        user_data.encryptPass = nil
        user_data.hashedPassword = nil
       
        print(cjson.encode(ret))
    end

   
