#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

function serial(str)
    local i1 = string.find(str, 'Serial')
    local i2 = string.find(str, 'Signature')
    local ret = string.sub(str, i1 , i2)
    ret = string.gsub(ret, "Serial Number:", "")
    ret = string.sub(ret,1,#ret-2)
    return ret
end

function get_start_date(str)
    local i1 = string.find(str, 'Not Before:')
    local i2 = string.find(str, 'GMT', i1)
    --local i2 = string.find(str, 'Not After :')
    local ret = string.sub(str, i1 , i2-1)
    ret = string.gsub(ret, "Not Before:", "")
    return ret
end

function get_exp_date(str)
    local i3 = string.find(str, 'Not After :')
    local i4 = string.find(str, 'GMT', i3)
    local ret2 = string.sub(str, i3 , i4-1)
    ret2 = string.gsub(ret2, "Not After :", "")
    --ret2 = string.sub(ret2,1,#ret2-2)
    return ret2
end

function get_key_cert (user)
    local key = ""
    local tx = user.last_cert_info.tx
    key = serial(tx)
    return key
end


if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { company_id = "", email = "",  }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    --user, err = col:find_one({ adminEmail = "thanaphas@gmail.com" })
    user, err = col:find_one({ adminEmail = body.email })

    if err then 
        ret = { success = false, error_msg = "not user", error_code = "" }
    else

        if user.have_cert and user.last_cert_info then
            local data = {}
            data.id_cert = get_key_cert(user)
            data.start_date = get_start_date(user.last_cert_info.tx)
            data.exp_date = get_exp_date(user.last_cert_info.tx)
            ret = { success = true, error_msg = "have certificate", error_code = "", data = data }
        else
            ret = { success = false, error_msg = "not have certificate", error_code = "" }
        end

    end
print(cjson.encode(ret))

end
