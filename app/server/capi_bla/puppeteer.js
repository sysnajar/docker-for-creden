const puppeteer = require('puppeteer');

(async () => {
  const delay = ms => new Promise(resolve => setTimeout(resolve, ms));
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();
  await page.goto('https://hjkl.ninja/brfv4');
//   await delay(10000);
//   await page.screenshot({path: '/home/creden/work/app/client/face/images/card/liveness.png'});

    page.on('console', function(msg){ 
        var txt = msg.text();
        console.log('PAGE LOG:', txt)
    });
    while (true) {
        await delay(1000);
        const element = await page.$("#get_blink");
        const blink = await page.evaluate(element => element.textContent, element);
        console.log(blink)
        await page.screenshot({path: '/home/creden/work/app/client/face/images/card/liveness.png'});
    }
  await browser.close();
})();
