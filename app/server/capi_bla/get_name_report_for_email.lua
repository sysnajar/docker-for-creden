#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local bson = require "resty-mongol.bson"
prog.timeout_fatal = false

local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
    h = ngx.req.get_headers()
    ip_key = 'ips_'..h["x-real-ip"]
    -- print(tostring(h['creden-key'])..'<<<<sesionja')
    -- if(h["creden-key"] ~= "0863765823456")then
    --     print(cjson.encode({success = false, error_msg = "session_expired" ..'_' .. ip_key}))
    --     ngx.exit(ngx.HTTP_BAD_GATEWAY)
    -- end
 
    -- if(h["cusername"] ~= "")then
    --    body.email = h["cusername"]
    -- end
else
    body = {email = 'thanaphas@gmail.com'}
end

ret = {}
conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    
    local check_have_email = col:find_one({adminEmail=body.email_user})
    if(check_have_email == nil)then
        ret = {success = false, error_msg = "ไม่พบอีเมลล์"}
    else
        ret.fuullname = (check_have_email.FNAME or "").." "..(check_have_email.LNAME or "")
        ret.phone = check_have_email.comTel or ""
        ret.success = true
    end
end
print(cjson.encode(ret))
