#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   --body  = {email = 'nutp10.1@gmail.com', password = '1234'} 
   body  = {adminEmail = arg[1]} 
end

conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    s = col:find_one({adminEmail=body.adminEmail}, {profile_pic=1,_id=0})    
    if s then 
        ret = {success = true, error_msg = "complete", data = s}
    else
        ret = {success = false, error_msg = "err"}
    end
end
print(cjson.encode(ret))
