#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
 else
    body  = {key = arg[1]} 
 end

local t = rc:lrange('complete_jobs', 0, -1)

for i,v in ipairs(t) do
    if v == body.key then
        data = rc:get(body.key)
        rc:lrem('complete_jobs', 0, body.key)
        rc:del(body.key)
        break
    end
end

print(data)