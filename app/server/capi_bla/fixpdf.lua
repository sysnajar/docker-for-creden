#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60 * 5000
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

--ngx.req.read_body()
print = ngx.say
body = ngx.req.get_uri_args()

                
ctx   = ctx or common.mongo_ctx()
col = ctx.col["edoc_list"]
doc, err = col:find_one({id=body.id})

ngx.log(ngx.NOTICE,"body " .. tostring(cjson.encode(body)) )


function get_page_count(file,id,owner)
  local files_pang = { }
  local outputFiles = file
  local paramss = {'pdftk'}
  local res, err = prog('pdftk', file, 'dump_data')
  prog.stdin = res.stdout
  
  res, err = prog('grep', 'PageMediaDimensions')
  data = cjson.encode(res.stdout)
  data = string.gsub(tostring(data), "PageMediaDimensions: ", "")
  data = string.gsub(tostring(data), "[\\]", "")
  data = string.gsub(tostring(data), "n", "pt;")
  ngx.log(ngx.NOTICE,'xd : '..file)
    for c in data:gmatch("([^;]*);") do 
        table.insert(files_pang, c)
    end
    for n, w in ipairs(files_pang) do
      datas = string.gsub(tostring(w), '"', "")
      datas = string.gsub(tostring(datas), ",", "")
      datasd = string.gsub(tostring(datas), " ", "pt,")
      move_name = string.gsub(tostring(file), ".pdf", "")
      papersize  = '{'..datasd..'}'
       prog('pdfjam', tostring(file) ,n ,'--papersize' ,papersize ,'-o',move_name..'_convert'..n..'.pdf')
      table.insert(paramss, move_name..'_convert'..n..'.pdf')
    end
   
    table.insert(paramss , 'cat')
    table.insert(paramss , 'output')
    table.insert(paramss , outputFiles)
    local resx , errx  = prog(unpack(paramss))
    prog.stdin = nil  
      if not errx then
            prog('sh','/home/creden/work/app/server/capi/remove_pdf.sh',move_name..'_convert*')
            prog('sh', '/home/creden/work/app/server/capi/run_update_edoc.sh',id,owner,'false')
      end

end

local dir = '/home/creden/work/app/client/face/images/card/'
local outputFile = dir ..body.id..'.pdf'
local params = {'pdftk'}

--pdftk 1.pdf 2.pdf 3.pdf cat output 123.pdf
for i,f in ipairs(doc.imageFiles) do
    local name = f.fileName
    local ok = false

    if(name:find('-'))then
        if(name:find('%-01%.png'))then
	  --print('before '..name)
          name = name:gsub('-01','')
          ok = true
	end
    else
          ok = true
    end

    if(ok)then
      local pdfname = name:match('%d+')
      pdfname = dir .. pdfname .. '.pdf'
      table.insert(params , pdfname)
    end
end
      local resf ={success = true, data = 'N/A' , code = 400}
      table.insert(params , 'cat')
      table.insert(params , 'output')
      table.insert(params , outputFile)
      local resd , errd =  prog(unpack(params))

      if not err then
            get_page_count(outputFile,doc.id,doc.owner)
      end
      
      print(cjson.encode(resf))
