#!/usr/bin/env lua
local cjson = require 'cjson'
mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
object_id = require "resty-mongol.object_id"
local redis = require 'redis'
common = require "common"
local session = require "resty.session".open({secret = "4321"})
rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false

local body = nil
local signers_view = {}
local tmp = {}

    if(ngx)then  
        ngx.req.read_body()
        print = ngx.say
        body = ngx.req.get_body_data()
        ngx.log(ngx.NOTICE,body)
        body = cjson.decode(body)
    else
        body  = {} 
    end

    function convertHexStringToNormal( str )
        return (str:gsub('..', function (cc)
        return string.char(tonumber(cc, 16))
        end))
    end

    function trim5(s)
        return s:match'^%s*(.*%S)' or ''
    end


	-- return the first order_set that belongs to needToSign person
	-- the send_mail_list must already be sorted by order_set
	function get_first_signer_order(send_mail_list)
		 for i, v in ipairs(send_mail_list)do
			 if(tostring(v.needToSign)=='true')then
				return tonumber(v.order_set)
			 end
		 end

		 return -1
	end

    function insert_history(h)
        local db2 = conn:new_db_handle("dopa")
		local history = db2:get_col('history_signer') 
        for i,signer in ipairs(h) do
            check_his, err_i = history:find_one({ owner = body.owner,email=signer.email})
            if not check_his then
                local data = { 
                    owner = body.owner,
                    email = signer.email,
                    name = signer.name
                }
                d,err = history:insert({data})
            end
        end
        
    end   
        
    local function send_line_noti(type, params)
        local res_noti, user_id = line_esign.get_line_user_id(params.to)
        if  params.secret_level and params.secret_level > 1 then
            if params.secret_level == 4 then
                text_secret = "ความลับที่สุด (Top Secret)"
            elseif params.secret_level == 3 then
                text_secret = "ความลับมาก (Secret)"
            elseif params.secret_level == 2 then
                ext_secret = "ความลับ (Confidential)"
            end
            params.secret = text_secret
        end
        if res_noti then
            local document_link = os.getenv('HOST_NAME') .. "/bla/#/document?id=" .. params.id
            params.url = document_link
            local message = line_esign.message(type, params)
            --line_esign.push(user_id, message)
        end
    end
    
   
    local ccid = tostring(os.time())
	for i,f in ipairs(body.fields) do
        if(tostring(f.is_no_sign) == 'true') then
            f.person.concensus_id = f.toolName .. ccid  
		end
	end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        common.activity_history(body.id ,body.owner, 'Created', 'สร้างเอกสาร', 'The document was created '..body.owner, body.owner..' สร้างเอกสาร', 'Created', body.originalFile, body.subject, body.msg, nil, nil)
        db = conn:new_db_handle("edoc")
        col = db:get_col("edoc_list")
        doc = body
        doc.company_id = session.data.company_id 
		doc.createdDtm = bson.get_utc_date(ngx.now() * 1000) 


		-- If use signing order , auto set first signable order 
        if(doc.use_signing_order==true)then
			doc.current_order = 9999
            for i,signer in ipairs(doc.signers) do
			  local n = tonumber(signer.order_set)
              if ( tostring(signer.needToSign)=="true" and n < doc.current_order) then
                  doc.current_order = n
              end
            end --for

		   doc.current_order = (doc.current_order == 9999) and 1 or doc.current_order
           ngx.log(ngx.NOTICE, 'Reset current_order to >>>' .. doc.current_order )

        end


        for k, v in ipairs(doc.fields) do
            if v.toolName == "Qrcode" then
                local name = "qrcode_" .. v.email .. "_" .. doc.id
                local link = v.toolData
                local cmd = "sh /home/creden/work/app/server/capi/generate_qrcode.sh " .. name .. " " .. link
                os.execute(cmd)
            end
        end

        local size_key = 'sizeof:'..doc.id
		doc.filesize = rc:get(size_key)	
		doc.status = 0
		rc:del(size_key)

        local db2 = conn:new_db_handle("dopa")
		local _u  = db2:get_col('esig_user'):find_one({adminEmail = assert(session.data.username)}) 
        local history = db2:get_col('history_signer') 
        
        local history_signer = {}
		doc.company_id = assert(_u.company_id, "+++++++++++++++++++++++++++ NO companyID +++++++++++++++++++++++" .. session.data.username) 
        i, err = col:insert({doc})

        if err then
            ret = {success = false, error_msg = err}
        else
            if tostring(doc.is_template) ~= 'true' then
                send_mail_list = {} --เก็บเมลผู้รับ
                list_signers={}
                for n,user in ipairs(doc.signers) do
                -- for n,user in ipairs(common.edoc_get_real_signers(doc)) do
                    
                
                
                    send_mail = {signer = user.name, to = user.email , id = doc.id, subject = doc.subject, msg = doc.msg }
                    send_mail.sender_name  = doc.name or 'N/A'
                    send_mail.sender_email = doc.owner or 'N/A'
                    send_mail.user_group = user.user_group or {}
                    send_mail.private_msg = user.private_mail or ''
                    send_mail.order_set = user.order_set
                    send_mail.needToSign = user.needToSign
                    send_mail.originalFile = doc.originalFile
                    send_mail.secret_level = doc.secret_level
                    
                    
                    table.insert(send_mail_list,send_mail)

                    local n ={}
                    local s = {}
                    if user.is_group then
                        for g,group in ipairs(user.user_group) do
                            n = {
                                doc_id = doc.id,
                                filename = doc.originalFile, 
                                dtm = ngx.now()*1000,
                                signer = doc.owner,
                                email  = group.email,
                                id     = ngx.now()*1000,
                                status = 0,
                                description = 1
                            }
                            s = {
                                name = group.name,
                                email = group.email
                            }
                            table.insert(list_signers, n)
                            table.insert(history_signer, s)
                            common.activity_history(body.id ,body.owner, 'Sent Invitation', 'ส่งเอกสาร', body.owner..' invitation to '..group.email, body.owner..' ส่งเอกสารให้ '..group.email, 'Sent', body.originalFile, body.subject, body.msg, nil, nil)
                        end
                        
                    else
                        n = {
                            doc_id = doc.id,
                            filename = doc.originalFile, 
                            dtm = ngx.now()*1000,
                            signer = doc.owner,
                            email  = user.email,
                            id     = ngx.now()*1000,
                            status = 0,
                            description = 1
                        }
                        s = {
                            name = user.name,
                            email = user.email
                        }
                        table.insert(list_signers, n)
                        table.insert(history_signer, s)
                        common.activity_history(body.id ,body.owner, 'Sent Invitation', 'ส่งเอกสาร', body.owner..' invitation to '..user.email, body.owner..' ส่งเอกสารให้ '..user.email, 'Sent', body.originalFile, body.subject, body.msg, nil, nil)
                    end
                    
                    
                    

                end

                if(#list_signers>0)then
                    ncol = db:get_col("notification")
                    _i, err = ncol:insert(list_signers, nil, true)
                end

                -- check sign only not sent mail
                if doc.sign_only == false then
                    if doc.use_signing_order == true then
                        if #send_mail_list > 0 then
                            
                            --send_mail_list = 
                            table.sort(send_mail_list, function(a,b) return tonumber(a.order_set) < tonumber(b.order_set) end)
    

							local first_signer_order = get_first_signer_order(send_mail_list)
							for is=1, #send_mail_list do
							  local mail = send_mail_list[is] 	
							   if(tostring(mail.needToSign) == 'true' and tonumber(mail.order_set) == first_signer_order)then
                                  table.insert(tmp, mail)
						       end
							end
                        end
    
                         -- loop signer view only
                         for i,s in ipairs(body.signers) do
                            if ( tostring(s.needToSign)=="false" ) then
                                send_mail_view = {signer = s.name, to = s.email , id = doc.id, subject = doc.subject, msg = doc.msg }
                                send_mail_view.sender_name  = doc.name or 'N/A'
                                send_mail_view.sender_email = doc.owner or 'N/A'
                                send_mail_view.user_group = s.user_group or {}
                                send_mail_view.private_msg = s.private_mail or ''
                                send_mail_view.needToSign  = s.needToSign
								send_mail_view.originalFile = doc.originalFile
								send_mail_view.order_set    = s.order_set 
                                send_mail_view.secret_level = doc.secret_level
                                table.insert(tmp,send_mail_view)
                            end
                        end
                         
    
                        rc:set('list_send_mail',cjson.encode(tmp))
                        os.execute('lua /home/creden/work/app/server/capi/email_invite_sign_bla.lua &')

                        ngx.log(ngx.NOTICE, 'SENDING MAIL A')
                       --[[ local res,err = prog('lua', '/home/creden/work/app/server/capi/email_invite_sign_bla.lua')
			if(err)then 
                             ngx.log(ngx.NOTICE, 'MAIL error' , err)
	                else			
                             ngx.log(ngx.NOTICE, 'MAIL', res.stdout , res.stderr)
			end]]
    
                    else
                        rc:set('list_send_mail',cjson.encode(send_mail_list))
                        os.execute('lua /home/creden/work/app/server/capi/email_invite_sign_bla.lua &')
			--[[
                        ngx.log(ngx.NOTICE, 'SENDING MAIL B')
                        local res,err = prog('lua', '/home/creden/work/app/server/capi/email_invite_sign_bla.lua')
			if(err)then 
                             ngx.log(ngx.NOTICE, 'MAIL error' , err)
	                else			
                             ngx.log(ngx.NOTICE, 'MAIL', res.stdout , res.stderr)
			end
			]]
    
                        --line noti
                        --for k, v in ipairs(send_mail_list) do
                        --    local type = v.needToSign == true and 'signer' or 'viewer'
                            --send_line_noti(type, v)
                        --end
                    end
                else
                    ret = {success = true, error_msg = err ,msg= "sign only"}
                end


              

                -- if doc.draft_id ~= '' then
                --     col_draft = db:get_col("edoc_list_draft")
                --     col_draft:delete({id = doc.draft_id},1,0)
                -- end
                
            
                insert_history(history_signer)
                

                ret = {success = true, error_msg = err ,list= send_mail_list}

            end
        end
    end
    print(cjson.encode(ret))

