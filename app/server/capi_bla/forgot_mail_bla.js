var mailer = require("nodemailer");
send_mail()


function send_mail() {
  var logo = process.env.HOST_NAME + "/bla/images/bla_logo_th.png"
  var bg = "#1667B2"
  var wTable = "95%"
  var link_forgot = process.env.HOST_NAME + "/bla/web/forgot.html?email=" + process.argv[2] + "&code=" + process.argv[3]
  var home = process.env.HOST_NAME
  var policy = process.env.HOST_NAME + "/bla/web/policy.html"
  var html = `
  <!doctype html>

  <html lang="th-TH" dir="ltr">
  <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
    </head>
    <body style="background: #E0E0E0;padding-bottom: 30px;"><br>
  
      <table border="0" cellspacing="0" cellpadding="0" align="center"
      style="background: #ffffff;padding: 10px 30px 50px 30px;width:60%;border-radius: 5px;">
        <tr>
            <td style="width: 100%;text-align: center;">
                <img src="${logo}" alt="logo website" style="width: 300px;margin-top: 30px;">
            </td>
        </tr>
        <tr>
          <td style="display: flex;justify-content: center;">
              <div style="background: #0058A9;width: 100%;display: block;padding-bottom: 100px;">
                  <div style="text-align: center;margin-top: 30px">
                     <img src="`+ process.env.HOST_NAME +`/bla/images/mail/mail_forgot.png" style="margin-top: 30px;">
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    Click the button to reset your password.
                  </div>
                  <div style="text-align: center;font-size: 24px;color: #FFF;margin-top: 15px">
                    กรุณาคลิกปุ่มเพื่อรีเซ็ตรหัสผ่านของคุณ
                  </div>
                  <div style="text-align: center;margin-top: 50px">
                    <a href="`+ link_forgot +`" style="text-decoration: none;color: #4F4F4F;background: #F2C94C;padding: 10px 30px;font-size: 16px;">
                      รีเซ็ตรหัสผ่าน
                    </a>
                  </div>
              </div>
          </td>
        </tr>
        </table>
        </body>
        </html>
  `

  var smtp = {
    host: 'smtp.bla.co.th', //set to your host name or ip
    // host: 'smtp.mailgun.org', //set to your host name or ip
    port: 25, //25, 465, 587 depend on your 
    auth: {
      user: 'electronic_sign@bangkoklife.com', //user account
      // // pass: 'creden2018' //user password
      //pass: 'dnkrakkaxnmnakpr'
      // user: 'postmaster@mg.creden.co', //user account
      // pass: '05b20c4ae05b1eeeee6ba7d230772bac-115fe3a6-372f8998' //user password
    }
  };
  var smtpTransport = mailer.createTransport(smtp);
  var mail = {
    from: 'Bangkok Life - eSinature', //from email (option)
    to: process.argv[2], //to email (require)
    cc: '',
    subject: '[Bangkok Life] Reset Password', //subject
    html: html//head + body + end
  }
  smtpTransport.sendMail(mail, function (error, response) {
    smtpTransport.close();
    if (error) {
      //error handler
      var res = { "success": false, "message": error }
      console.log(res);
    } else {
      //success handler 
      var res = { "success": true }
      console.log(res);
    }
    process.exit();
  });

}


