#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local mongo = require 'mongo'
local client = mongo.Client 'mongodb://127.0.0.1'
local collection = client:getCollection('newdbd', 'company')

local res, err = prog('sh', '/home/creden/work/app/server/capi/down.sh' )
ngx.say(cjson.encode({success = true, output = res.stdout}))
