#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil

if(ngx) then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = {}
end

local conn = mongo()
conn:set_timeout(5000)

local ok, err = conn:connect('127.0.0.1', 27017)

if not ok then
    ngx.say("connect failed: ".. err)
else
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("package_ss")
    local get_com = col:find_one({package="basic"})
    print(cjson.encode(get_com))
end


-- local get_com = col:find({plan="monthly"})
-- for a,n in get_com:pairs() do
    -- print(cjson.encode(get_com))
-- end
