
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local session = require "resty.session".open({secret = "4321"})
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local line_esign = require "line_esign_noti"
images_fields = {}
prog.timeout = 1000 * 60
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

HOST_NAME = os.getenv("HOST_NAME")
if rc:get('is_ldd') and rc:get('is_ldd')=='true' then
	HOST_NAME = 'https://esign.ldd.go.th'
end

rejecter  = nil

-- ex. current_order = 1 , _signing_order = 3 will return signer.signOrder = 2 and 3
function find_people_by_signOrder(signers, current_order, new_signing_order)
    local ret = {}
    for _, v in ipairs(signers) do
        local n = tonumber(v.order_set)
        if( (n > current_order) and (n <= new_signing_order) ) then
           table.insert(ret, v)
        end
    end
    
    return ret
end

 --send email to next signers/viewer  
 function send_mail_to_next_signers(s, send_mail_list)
    for _, v in ipairs(send_mail_list) do 
        if not s.secret_level then
            s.secret_level = 0
        end
        --1. send email 
        local cmd = 'node /home/creden/work/app/server/capi/send_mail_doc.js "' .. v.to .. '" "'..v.msg..'" "'..(v.private_msg or '')..'" "'..v.id..'" "'..(v.sender_email or '')..'" "'..(v.sender_name or '')..'" "'.. s.subject.. '" "'.. s.secret_level.. '" &'
         local line, noti = os.execute(cmd)
         ngx.log(ngx.NOTICE,"send mail to >>> " .. tostring(v.to))    

         --2. send line noti
         local line_noti_params = {subject = s.subject, originalFile = s.originalFile, id = s.id, to = v.to}
         send_line_noti( v.line_msg_type, line_noti_params)


    end
 end
 


local pdf_info = nil -- store pdf_dump infomation eg: see rc:get('pdf_dump:gridA4.pdf')

ngx.req.read_body()
print = ngx.say
body = ngx.req.get_body_data()
ngx.log(ngx.NOTICE,body)
body = cjson.decode(body)

header = ngx.req.get_headers()

-------------------------------
local updct = 1
local uplog = ''
local uperr = 0
local cnt = 0
local server_name = rc:get('server.name') or 'N/A'
function do_prog(...)
    local start_time = ngx.time()*1000
    local updid = body.id
    local cmd = ''
    local arg = {...}
	for i,v in ipairs(arg)do
       cmd = cmd .. ' ' .. v
	end
	
	uplog = uplog .. 'updid:'..updid..'#'..updct..') '.. cmd ..'\n'
	ngx.log(ngx.NOTICE, 'updid:'..updid.. '#' .. updct .. ') ' .. cmd )
	local res, err = prog(...)
	updct = updct+1
	local err2 = tostring(res.stderr)

	if(err or err2 ~= '')then
		 uperr = uperr + 1
		 local key  = 'err.upd.' .. rc:incr('err.update.cnt')
		 rc:set(key, cjson.encode({cmd = cmd, err = tostring(err), err2 = err2, doc_id = body.id , num = updct}))
		 rc:expire(key,60*60*24*7) -- expire error log after 7 days


	     ngx.log(ngx.NOTICE, 'HAS ERROR ' .. tostring(err) ..', err2 ' .. err2 .. 'ERR_CMD = '  .. cmd)
		 local str = "Document_fail_Document_ID:" .. tostring(body.id) .. ",cmd#" .. updct.. '@' .. server_name .. '[' .. key ..']'
		 os.execute('/home/creden/work/app/server/capi/line_esig.sh ' .. str .. ' &')

	 else
	     ngx.log(ngx.NOTICE,  'NO ERROR [' .. tostring(res.stdout) ..']' )
	end
    local end_time = ngx.time() *1000
    local total_time = tonumber((end_time - start_time) / (60*1000))
    cnt = cnt  + 1
    ngx.log(ngx.NOTICE, "count prog " .. cnt )
    ngx.log(ngx.NOTICE, cmd .. 'Gott_command_prog [' .. total_time ..']' )
end

--------------------------------
--group signing function
function is_in_group(t, email)
	for _,person in ipairs(t) do
	   if(person.email==email) then return true end
	end
	return false
end


function send_line_noti(type, params)
    local res_noti, user_id = line_esign.get_line_user_id(params.to)
    if res_noti then
        local document_link = os.getenv('HOST_NAME') .. "/esign/#/document?id=" .. params.id
        params.url = document_link
        local message = line_esign.message(type, params)
        line_esign.push(user_id, message)
    end
end
-- send mail cc
function send_mail_cc(list_cc,msg,id,sender_email,sender_name,subject,secret_level)
    private_msg = ''
    for i,s in ipairs(list_cc) do
        if s.cc and #s.cc > 0 then
            for n,u in ipairs(s.cc) do
                cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_viewonly.js "'..u.email..'" "'..msg..'" "'..private_msg..'" "'..id..'" "'..sender_email .. '" "' .. sender_name .. '" "' .. subject .. '" "' .. secret_level .. '" &'
	            os.execute(cmd)
            end

        end

    end
end

-- sign fn
function getX(xp, width, f)
    local ret = 0
    --local width = 560--595
    local x2= (width*xp)/100

    return x2
end

function getY(yp, height, f)
    local sigHeight =  0

    yp = 100 - yp
    local y2 = ( (height-sigHeight) * yp)/100
    
    if( (not f.toolName) or ( f.toolName=='Signature' or f.toolName == 'Initial') )then
        -- local offset = 20 --was 45
        local offset = 35 --was 45,20,25
        if(f.scale and f.scale~=100)then
          offset = (f.scale*offset)/100
        end

        y2 = y2 - offset
    end

    ngx.log(ngx.NOTICE, 'y2 = ' .. y2)
    return y2
end

function getX_qrcode(xp, width, f)

    -- if width >= 650 then 
    --     xp = 90 
    -- end

    -- local x2 = (width*xp)/100
    -- return x2

    local x2 = (width*xp)/100
    return x2
end
-- end getx qrcode fn

function getY_qrcode(yp, height, f)

    -- if height <= 750 then
    --     yp = 15
    -- end

    -- local sigHeight =  0
    -- yp = 100 - yp
    -- local y2 = ( (height-sigHeight) * yp)/100

    -- return y2

    local sigHeight =  0

    yp = 100 - yp
    local y2 = ( (height-sigHeight) * yp)/100
    
    if( (not f.toolName) or (f.toolName=='Qrcode'))then
        local offset = 90
        if(f.scale and f.scale~=100)then
          offset = (f.scale*offset)/100
        end

        y2 = y2 - offset
    end

    return y2

end
-- end gety qrcode fn

function getX_stamp(xp, width, f)

    if width >= 650 then 
        xp = 90 
    end

    local x2 = (width*xp)/100
    return x2
end

function getY_stamp(yp, height, f)

    local sigHeight =  0

    yp = 100 - yp
    local y2 = ( (height-sigHeight) * yp)/100
    
    if( (not f.toolName) or (f.toolName=='Stamp'))then
        local offset = 60
        if(f.scale and f.scale~=100)then
          offset = (f.scale*offset)/100
        end

        y2 = y2 - offset
    end

    return y2

end
-- end gety stamp fn


function getYText(yp, height, f)
    local sigHeight =  0

    yp = 100 - yp
    local y2 = ( (height-sigHeight) * yp)/100
    
     y2 = y2 - 10

    return y2
end


function do_field(s, f)
        local is_sign  = (not f.toolName) or ( f.toolName=='Signature' or f.toolName=='Initial')
        local is_qrcode = (not f.toolName) or (f.toolName=='Qrcode')
        local is_stamp = (not f.toolName) or (f.toolName=='Stamp')
        local is_text  = (f.toolName) and (f.toolName =='Name' or  f.toolName == 'Email' or f.toolName =='Tel' or f.toolName ==    'Text' or f.toolName =='Date' or f.toolName =='RunningNumber' 
		or f.toolName == 'Checkbox' or f.toolName == 'Dropdown' or f.toolName =='Approve' or f.toolName == 'Radio' or f.toolName == 'Radio2')
        if(is_sign) then sign(s, f) end
        if(is_qrcode) then do_qrcode(s, f) end
        if(is_stamp) then do_stamp(s, f) end
        if(is_text) then do_text(s, f) end  
end


function get_radio_choice(f)
      for i,c in ipairs(f.child) do
            if(c.id == f.toolData)then
			  return c
			end
	  end
	  return nil
end

    obj_txt = {}
function do_text(s, f) 
        
        local pdfFile = s.pdfFile .. '.pdf'
        local dir   = '/home/creden/work/app/client/face/images/card/' 
        local pdfFile   = dir .. pdfFile
        local page = tostring(f.page+1)
        
        local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
        local y2 = tostring(getYText(f.yp ,pdf_info.pages[tonumber(page)].height, f))
        ngx.log(ngx.NOTICE,cjson.encode(f))
        if f.toolName == 'Radio' then
			local choice = get_radio_choice(f)
            x2 = tostring(getX(choice.xp, pdf_info.pages[tonumber(page)].width, f))
            y2 = tostring(getYText(choice.yp ,pdf_info.pages[tonumber(page)].height, f))
            f.toolData = 'X'
        end
        
        --t = {page=page,x=x2,y=y2,text=f.toolData,size=(f.fontSize or 11) ,font="/home/creden/work/app/tmp/" .. (f.styleFont or "Taviraj-Regular") .. ".ttf"}

        if(f.toolName == 'Approve')then f.toolData = f.toolData2 or 'NO TOOL DATA'  end 

        if f.tooData and f.toolData ~= '' then
            t= {page=page,x=x2,y=y2,text=f.toolData,size=(f.fontSize or 16) ,font="/home/creden/work/app/tmp/" .. (f.styleFont or "THSarabunNew") .. ".ttf"}
            table.insert(obj_txt,t) 

            -- clone text fields
            if(s.clone_field_from_page1==true)then
            for i=2,#s.imageFiles do
                local t2 = clone_field(t)
                t2.page = i-1
                table.insert(obj_txt,t2)
            end
            end
            -- end clone text fields

        end
        
end
    -- end text fn

function do_text2(s, f) 
    local pdfFile = s.pdfFile .. '.pdf'
    local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. pdfFile
    local page = tostring(f.page+1)

    local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
    local y2 = tostring(getYText(f.yp ,pdf_info.pages[tonumber(page)].height, f))

    local img = 'text_of_' .. tostring(f.id) .. '.png'
    img = dir .. img
end
    -- end text fn

function do_stamp(s, f)
        local pdfFile = s.pdfFile .. '.pdf'
        local file2 = string.gsub(pdfFile , '%.pdf', '_stamped.pdf')
        local sig  = '/home/creden/work/app/client/face/images/card/'.. f.toolData .. '.png' 
        local dir   = '/home/creden/work/app/client/face/images/card/' 
        local pdfFile   = dir .. pdfFile
        local pdfFile2  = dir .. file2
        local page = tostring(f.page+1)
    
        local x2 = tostring(getX_stamp(f.xp, pdf_info.pages[tonumber(page)].width, f))
        local y2 = tostring(getY_stamp(f.yp ,pdf_info.pages[tonumber(page)].height, f))
        
        if(f.scale and f.scale ~= 100)then
           local sig2  = sig .. '.scale.png'
           do_prog('convert', sig  ,'-resize', tostring(f.scale) ..'%',  sig2)
           sig = sig2
        end 
        
        
        local cmd2 = 'cp ' ..pdfFile2 .. ' '.. pdfFile
        --do_prog('java', '-jar', '/home/creden/scripts/pdfstamp.jar', '-i', sig, '-l',   x2 ..','.. y2 , '-d', '300', '-o', dir, '-p',  tostring(page) ,  pdfFile) 
        --do_prog('cp', pdfFile2, pdfFile)
      
        f = {images = sig,x = x2, y = y2, page = tonumber(page)}
        table.insert(images_fields,f) 
end
    -- end sign Stamp fn

function do_qrcode(s, f)
        local pdfFile = s.pdfFile .. '.pdf'
        local file2 = string.gsub(pdfFile , '%.pdf', '_stamped.pdf')
        local sig  = '/home/creden/work/app/client/face/images/qrcode/qrcode_'.. f.email .. '_' .. s.id ..'.png' 
        local dir   = '/home/creden/work/app/client/face/images/card/' 
        local pdfFile   = dir .. pdfFile
        local pdfFile2  = dir .. file2
        local page = tostring(f.page+1)
    
        local x2 = tostring(getX_qrcode(f.xp, pdf_info.pages[tonumber(page)].width, f))
        local y2 = tostring(getY_qrcode(f.yp ,pdf_info.pages[tonumber(page)].height, f))
        
        if(f.scale and f.scale ~= 100)then
           local sig2  = sig .. '.scale.png'
           do_prog('convert', sig  ,'-resize', tostring(f.scale) ..'%',  sig2)
           sig = sig2
        end 
    
        local cmd2 = 'cp ' ..pdfFile2 .. ' '.. pdfFile
        do_prog('java', '-jar', '/home/creden/scripts/pdfstamp.jar', '-i', sig, '-l',   x2 ..','.. y2 , '-d', '200', '-o', dir, '-p',  tostring(page) ,  pdfFile) 
        do_prog('cp', pdfFile2, pdfFile)

        f = {images = sig,x = x2, y = y2 , page = tonumber(page)}
        table.insert(images_fields,f) 
end
    -- end sign qrcode fn

function gen_qrcode(s,host) 
        --local link = host .. "/company/#/document?id=" .. s.id --qr_link
        local link = host .. "/esign/#/document?id=" .. s.id
        res, err = do_prog('sh','/home/creden/work/app/server/capi/generate_qrcode.sh', 'qrcode_' .. s.id, link)

        local qrcode = "/home/creden/work/app/client/face/images/qrcode/qrcode_" .. s.id .. ".png"
        local output   = '/home/creden/work/app/client/face/images/card/' 
        local page = "1"
        local pdfFile = s.pdfFile .. '.pdf'

        local x2 = tostring(getX_qrcode(85, pdf_info.pages[tonumber(1)].width, nil))
        local y2 = tostring(getY_qrcode(10 ,pdf_info.pages[tonumber(1)].height, nil))
        local xy = "get X : " .. x2 .. "------ get Y : " .. y2
        local wh = "width X : " .. pdf_info.pages[tonumber(1)].width .. "------ height Y : " .. pdf_info.pages[tonumber(1)].height
        local file3 = "/home/creden/work/app/client/face/images/card/" .. pdfFile
        local cmd  = 'java -jar /home/creden/scripts/pdfstamp.jar -i '.. qrcode .. ' -l '.. (x2 ..','.. y2) ..' -d 200 -o '.. output ..' -p '.. page .. ' ' .. file3
        ngx.log(ngx.NOTICE,cmd)
        do_prog('java', '-jar', '/home/creden/scripts/pdfstamp.jar', '-i',  qrcode , '-l'  , (x2 ..',' .. y2) , '-d', '200', '-o', output , '-p' , page ,file3)

        ngx.log(ngx.NOTICE,xy)
        ngx.log(ngx.NOTICE,wh)
        do_prog("mv", "-f", "/home/creden/work/app/client/face/images/card/" .. s.pdfFile .. "_stamped.pdf", "/home/creden/work/app/client/face/images/card/" .. s.pdfFile .. ".pdf")
       

end
    -- end qrcode fn


function clone_field(f)
        local ret = {}
        
        for k,v in pairs(f)do
            ret[k] = v
        end

        return ret
end

function clone_field_from_page1(doc)
        local fields_in_page1 = {}
        --doc.cloned_fields = {}

        for i,f in ipairs(doc.fields) do
            if(f.page==0)then 
                table.insert(fields_in_page1, f)
            end
        end


        for i=2, #doc.imageFiles do
            for j,f in ipairs(fields_in_page1) do
                -----
                local f2 = clone_field(f)
                            f2.page = i-1
                            table.insert(doc.fields, f2) 
                -----
            end
        end

end
    


function set_count_use_company(email_owner,fields)
		local ret = { status = false}

        local col = ctx.col["dopa.company_members"]
        local list = { email =  email_owner }
        local l,err = col:find_one(list)
        local name_company =  "N/A"
        local chk_company = false
    
        if l then 
            l._id = tostring(l._id)
            l.company_id = tostring(l.company_id)
            ngx.log(ngx.INFO,'name_owner =' .. email_owner) 

            local company = {
                { id = "5c597eaee0ca72081a1d4ca9", name = "creden" },
                { id = "5e9d590da1fb1ce3475bf3e1", name = "tvdirect" },
                { id = "5f0d7b3c8a2f8d0d136d8ca1", name = "thaibispa" },
                { id = "5f2a2bed8a2f8d0d136e8c50", name = "pea" },
                { id = "5f32cd6a8a2f8d0d136ee3bd", name = "siamcompressor" },
                { id = "5f59f6f5bb0b32a4320785a2", name = "decathlon" }
            }

            for i, v in ipairs(company) do
                if v.id == l.company_id then
                    name_company = v.name
                    chk_company = true
                end
            end
        end
        
        if chk_company and name_company ~= "N/A" then
            local use = rc:get(name_company .. '.use_signature')
            local credit = rc:get(name_company .. '.credit_signature')

            if tonumber(use) < tonumber(credit) then
				ret  = { status = true, name_company = name_company  }
            else    
                print(cjson.encode({success = false, error_msg = "เครดิตการเซ็นเอกสารไม่พอสำหรับการใช้งาน"}))
                ngx.exit(ngx.HTTP_OK)
            end
        else
            ngx.log(ngx.INFO,'=== No Company ===')   
        end

		return ret
end
    -- end count use signature document company
 
function auto_set_type(f, doc_id)
    local ret = 'draw'
    local file = f.email .. f.id ..'_gen.png'

    if(f.toolName=='Initial')then
        file = f.email .. f.id ..'_gen_initial.png'
    end	    
    	
    if(os.execute('ls /home/creden/work/app/client/edoc/app-assets/images/signature/'..file)==0) then
       ret = 'gen'
    end

    ngx.log(ngx.WARN, 'auto_set_type'  , doc_id ,  f.id , f.n , ret )
    return ret
end

function sign(s, f)
    local pdfFile = s.pdfFile .. '.pdf'
    local file2 = string.gsub(pdfFile , '%.pdf', '_stamped.pdf')


    if(not f.type)then
    	f.type = auto_set_type(f, s.id)
    end


    local sig0  = '/home/creden/work/app/client/edoc/app-assets/images/signature/'..f.email..'_'..f.type..'.png' 

    local sig  = '/home/creden/work/app/client/edoc/app-assets/images/signature/'..f.email .. '_' .. os.time() .. '_'..f.type..'.png' 

    -- increase signature by 20%
	do_prog('convert', sig0  ,'-scale', '120%',  sig)
    
	local dir   = '/home/creden/work/app/client/face/images/card/' 
    local pdfFile   = dir .. pdfFile
    local pdfFile2  = dir .. file2
    local page = tostring(f.page+1)

    ngx.log(ngx.INFO, 'sign fn1', 'field#', f.id, f.email , f.type , sig)
    ngx.log(ngx.INFO, 'sign fn2', 'pdf_info', cjson.encode(pdf_info))
    local x2 = tostring(getX(f.xp, pdf_info.pages[tonumber(page)].width, f))
    local y2 = tostring(getY(f.yp ,pdf_info.pages[tonumber(page)].height, f))
    
    if(f.scale and f.scale ~= 100)then
       local sig2  = sig .. '.scale.png'
       do_prog('convert', sig  ,'-resize', tostring(f.scale) ..'%',  sig2)
       sig = sig2
    end 

   
    
    local cmd2 = 'cp ' ..pdfFile2 .. ' '.. pdfFile
    
    if(s.clone_field_from_page1==true)then
         local t = {'java', '-jar', '/home/creden/scripts/pdfstamp.jar', '-i', sig, '-l',   x2 ..','.. y2 , '-d', '200', '-o', dir}
          for i=1, #s.imageFiles do
             table.insert(t,'-p') 
             table.insert(t,tostring(i)) 
          end
         table.insert(t,pdfFile)
         do_prog(unpack(t))

    else
        -- 1 do_prog('java', '-jar', '/home/creden/scripts/pdfstamp.jar', '-i', sig, '-l',   x2 ..','.. y2 , '-d', '200', '-o', dir, '-p',  tostring(page) ,  pdfFile)
    end

    --2 do_prog('cp', pdfFile2, pdfFile)
    f = {images = sig,x = x2, y = y2, page = tonumber(page)}
    table.insert(images_fields,f) 
    --ngx.log(ngx.NOTICE,'--------------------------PDF INFO IS ' .. cjson.encode(pdf_info))
    --ngx.log(ngx.NOTICE,'--------------------------'..cmd);ngx.log(ngx.NOTICE,'--------------------------'..cmd2)
    --rc:set('last_sign', cmd)
end
    -- end sign fn
	



    ----------- update mongo ----------------------
            ret = {success = false, error_msg = "N/A"}

            if false then
                ret = {success = false, error_msg = err}
            else
				local can_update = false
				local can_update_count = 0
				local myusername  = session.data.username
				if(not myusername) then
					print(cjson.encode({success = false, error_msg = "session expired"}))
                    ngx.log(ngx.INFO,'DETECT FRUAD nil user try to update doc#'.. (body.id) )
					ngx.exit(ngx.HTTP_OK)
                end	
                
				ctx   = ctx or common.mongo_ctx()
                col = ctx.col["edoc_list"]
                s, err = col:find_one({id=body.id})
                s._id = nil



                --CERT_PIN
                if(body.cert_pin and #body.cert_pin>0)then
                   local pin_key = s.id .. '.' .. myusername
                   local pin_val = 'xxxxxx' .. body.cert_pin
                   ngx.log(ngx.INFO,' USE PIN ' .. pin_key ) 
                   rc:set(pin_key, pin_val)
                end


                increment_credit = set_count_use_company(s.owner,s.fields)
				if(s.owner==myusername)then can_update = true end
				
				local notification_list = {}
				local noti_check = {}

				function is_in_fields(id, fields)
                      for _,f in ipairs(fields)do
                             if(f.id == id)then return true end
					  end
					  return false
				end

				-- group signing
                for _,f in ipairs(s.fields) do
					if(f.status ==0 and f.person and f.person.is_group and
					   is_in_group(f.person.user_group, myusername)) then
	                   
					   if(is_in_fields(f.id, body.fields))then
					      f.email = myusername		  
						  ngx.log(ngx.INFO,'Group signing >>>' .. f.person.name ..'@'..f.id)   
					   end
					end
				end
				-- end group signing

                for i,v in ipairs(s.fields) do
                    ngx.log(ngx.INFO,i , ') ' , v.id)
                    for i2,v2 in ipairs(body.fields) do
                        ngx.log(ngx.INFO,'    ' , i2 , ') ' , v2.id..' VS '..v.id)
                        if (v.id == v2.id) then 
					        local pos_signers = (common.edoc_get_signers_by_position(v2.position_id))
                            --ngx.log(ngx.INFO, 'position#'..tostring(v2.position_id)..' has '.. #pos_signers)
							if(not pos_signers)then pos_signers = {} end


							local ps = nil
							for _, p in ipairs(pos_signers) do
								if(p.email == myusername) then 
									ps = p
									v.email = myusername
									break 
								end
							end
							



							if( (v.email == v2.email or ps)
								and v.email ==myusername
							  ) then 
								can_update_count = can_update_count+1 
                                ngx.log(ngx.INFO, 'increment can_update_count field#' .. (v.n) )
							end 


                            v.status = 1 v.time = os.date("%x %X")
                            v.type = v2.type
                            v.toolData  = v2.toolData

                            v.toolData2 = v2.toolData2    -- Approve only
                            v.isApproved = v2.isApproved  -- Approve only

							if(v2.toolName=='Approve' and v2.isApproved==false)then
							  rejecter  = v.email
                            end
                            ngx.log(ngx.INFO,'reject +++' .. tostring(rejecter).."===================")
							v.ip_field = header["x-real-ip"] 
                            --v.sign_now = true
                            ngx.log(ngx.INFO,' set signing type for ' .. v.id .. ' >> ' , v.type)
							local n = {doc_id = s.id,
							filename = s.originalFile, 
							dtm = ngx.now()*1000,
							signer = v.email,
							email  = s.owner,
						    id     = ngx.now()*1000,
							status = 0,
							description = 0
						    } 
							
							if(not noti_check[v.email])then --make sure we sent noti once
							   table.insert(notification_list, n)
							   noti_check[v.email] = true
                            end
                            common.activity_history(s.id ,v.email, 'Sign', 'เซ็นเอกสาร', v.email..' sign document '.. s.originalFile, v.email..' เซ็นเอกสาร '..s.originalFile, 'Sign', s.originalFile, nil, nil)
                        end
                    end
                end

				if(#notification_list>0)then
                   ncol = ctx.col["notification"]
                   _i, err = ncol:insert(notification_list, nil, true)
 				end

                -- update fields information
				ngx.log(ngx.INFO,'REACHED $SET '.. body.id .. type(body.id))
				ngx.log(ngx.INFO,'can_update_count = ' .. (can_update_count) ..'/'.. (#body.fields)  )
				if(can_update_count ~= #body.fields)then
					print(cjson.encode({success = false, error_msg = "session expired..."}))
                    ngx.log(ngx.INFO, 'DETECT FRUAD user '.. myusername  ..' try to [update unauthorized doc]#'.. (body.id) )
					ngx.exit(ngx.HTTP_OK)
				else
                    ngx.log(ngx.INFO, 'User '.. myusername  ..' try to [update doc]#'.. (body.id) ..'>>> OK')
				end





                local update = {["$set"] = {fields=s.fields}}
                u, err = col:update({id=body.id}, update, 0, 0, true)
                -- end


				-- if(rejecter~=nil)then
                --      s.status = 4
				-- 	 s.rejecter = rejecter
                --      ngx.log(ngx.INFO,'==========================' .. rejecter.."===================")
				-- end


                -- update current_order information 
				local new_signing_order = nil
				
				if(s.use_signing_order==true) then
                    local s2, err2 = col:find_one({id=body.id})
					local fields_2 = s2.fields
					local t1 = 0 -- total fields for current signing_order
					local t2 = 0 -- number of signed fields for the current order

					for _i, v in ipairs(s2.fields) do
						 if( tonumber(s.current_order) == tonumber(v.signOrder))then
							t1 = t1+1	
						 
							if(v.status==1)then
							   t2 = t2+1	
						    end
						 else
                             ngx.log(ngx.INFO, s.current_order .. '!=' .. tonumber(v.signOrder) ..'>'..v.email )
							 
						 end

					end

                    ngx.log(ngx.NOTICE, "t1 = " .. tostring(t1) ..", t2 = " .. tostring(t2) )                   
				    if(t1>0 and t1==t2)then -- all fields in current order are signed
					   new_signing_order = s.current_order+1	
                        ngx.log(ngx.NOTICE,"new_signing_order = " .. (new_signing_order))                   

						 -- adjust new signing order 
						 local max_order = #s.signers
							while true do
							    local p = s.signers[new_signing_order]
								if(p and tostring(p.needToSign)=='false')then
									new_signing_order = new_signing_order + 1
								end

								if(not p)then break end

								if(p.needToSign or new_signing_order>100)then 
									break
								end
							

							end
						 --

					end

				end

                if(new_signing_order)then
                    
                    ngx.log(ngx.NOTICE,"UPDATE SIGNING ORDER to " .. tostring(new_signing_order) .. ', current_order = ' .. s.current_order)                   
                    local order_update = {["$set"] = {current_order=new_signing_order}}
                    u3, err3 = col:update({id=body.id}, order_update, 0, 0, true)
 
                    

                     send_mail_list = {}
                     local next_signers_list = find_people_by_signOrder(s.signers, s.current_order , new_signing_order)
                     for _, next_signer in ipairs(next_signers_list) do
                         if(not next_signer.is_group)then
                            local send_mail = {
                                signer = s.name, 
                                to = next_signer.email , 
                                id = s.id, subject = s.subject, 
                                msg = s.msg ,
                                password = next_signer.password,
                                sender_name = s.owner_name or 'N/A',
                                sender_email = s.owner or 'N/A',
                                user_group   = next_signer.user_group or {},
                                private_msg  = next_signer.private_mail or '', 
                                line_msg_type    = (tostring(next_signer.needToSign)=='false') and 'viewer' or 'signer'
                            } 
                            table.insert(send_mail_list, send_mail) 
                         else   
                            for _,g in ipairs(next_signer.user_group) do
                                local send_mail = {
                                    signer = s.name, 
                                    to = g.email , 
                                    id = s.id, subject = s.subject, 
                                    msg = s.msg ,
                                    password = next_signer.password,
                                    sender_name = s.owner_name or 'N/A',
                                    sender_email = s.owner or 'N/A',
                                    user_group   = next_signer.user_group or {},
                                    private_msg  = next_signer.private_mail or '' ,
                                    line_msg_type    = (tostring(next_signer.needToSign)=='false') and 'viewer' or 'signer'
                                } 
                                table.insert(send_mail_list, send_mail) 

                            end --for user_group

                         end
                     
                     end -- for next_signer


                     ngx.log(ngx.NOTICE,"Sending Email >> new_signing_order = " .. tostring(new_signing_order) .. ', current_order = ' .. s.current_order)                   
                     send_mail_to_next_signers(s, send_mail_list) 
 
                 end --new_signing_order
                

                count_sign = 0
                for i3,v3 in ipairs(s.fields) do
                    if v3.status == 1 then count_sign = count_sign + 1 end
                end

                local pdf_file = '/home/creden/work/app/client/face/images/card/' .. s.pdfFile .. '.pdf'
				local pdf_file_original = '/home/creden/work/app/client/face/images/card/' .. s.pdfFile .. '_original.pdf'
                local pdf_dump_key = 'pdf_dump:'..pdf_file

                if table.getn(s.fields) == count_sign then 


                     --if( s.clone_field_from_page1 == true )then
                     --   clone_field_from_page1(s)     
                     --end

                    if(rejecter~=nil)then
                        s.status = 4
                        s.rejecter = rejecter
                        ngx.log(ngx.INFO,'=============not approve ============='..rejecter)
                    else
                        s.status = 1
                        ngx.log(ngx.INFO,'============= success =============')
                    end
                    
                    if not s.secret_level then
                        s.secret_level = 0
                    end
                    
                    if s.status == 1 and s.list_cc then
                        send_mail_cc(s.list_cc,s.msg,s.id,s.owner,s.name,s.subject,s.secret_level)
                    end

                    -- send mail
                    if s.status == 1 then
                        list_mail_signers={}
                        for n,user in ipairs(s.signers) do
                            if user.is_group then
                                for g,group in ipairs(user.user_group) do
                                    if group.email ~= s.owner then
                                        cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_comp.js "'..group.email..'" "'..s.subject..'" "'..s.id..'" "'..s.secret_level..'" &'
                                        os.execute(cmd)
                                    end
                                end
                            else
                                if user.email ~= s.owner then
                                    cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_comp.js "'..user.email..'" "'..s.subject..'" "'..s.id..'" "'..s.secret_level..'" &'
                                    os.execute(cmd)
                                end
                            end
                        end
                        cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_comp.js "'..s.owner..'" "'..s.subject..'" "'..s.id..'" "'..s.secret_level..'" &'
                        os.execute(cmd)

                    end
                    

					if(increment_credit.status)then
                        rc:incr(increment_credit.name_company .. '.use_signature') 
				    end


					--make backup of original PDF
					backup_cmd = 'cp '.. pdf_file .. ' ' .. pdf_file_original .. ' &'
				    ngx.log(ngx.NOTICE,' backup cmd ' .. backup_cmd)
					os.execute(backup_cmd)


					 --pdf upright
					 do_prog('cpdf', '-upright',  pdf_file,  '-o', pdf_file)
					 -- end

                    -- Prepare pdf_dump
					do_prog('lua', '/home/creden/scripts/pdf_dump.lua',  pdf_file)
                    pdf_info = cjson.decode(rc:get(pdf_dump_key))
                    rc:set('last_pdf_dump', cjson.encode(pdf_info))
                    --rc:set('last_pdf_dump_cmd', pdaf_dump_cmd)
                    -- enda

                    for i,v in ipairs(s.fields) do  
                           ngx.log(ngx.NOTICE,' start signing for ' .. v.id .. ' ' , v.type)
                           do_field(s, v) 
                    end

					local redis_key = 'data_fields:' .. s.id

                    rc:set(redis_key, cjson.encode(images_fields))
                    --D1 
					do_prog("java","-cp","/home/creden/work/app/PDFTimestamping/target/*","org.crossref.pdfstamp.Main", pdf_file, redis_key, "350")
                   local file2 = string.gsub(pdf_file , '%.pdf', '_stamped.pdf')
                   --D2 
				   do_prog('cp', file2, pdf_file)


                    --do_prog('java', '-jar', '/home/creden/scripts/pdfstamp.jar')
					--do_prog("java","-cp","/home/creden/work/app/PDFTimestamping/target/*","PdfBoxTool.AddText", a)

                    if table.getn(obj_txt) > 0 then
                        data_add_text = {input_file = pdf_file , output_file = pdf_file, field = obj_txt} 
                        a = cjson.encode(data_add_text)
                        rc:set('test_add_text',a) 
                        cmd = "java -cp '/home/creden/work/app/PDFTimestamping/target/*' PdfBoxTool.AddText '"..a.."'"   
                        ngx.log(ngx.NOTICE,'=========java add text===========') 
                        ngx.log(ngx.NOTICE,cmd)                   
                        do_prog("java","-cp","/home/creden/work/app/PDFTimestamping/target/*","PdfBoxTool.AddText", a)
                    end

                    s.ip_stamp = header["x-real-ip"]
                    update = {["$set"] = {fields=s.fields, status=s.status, ip_stamp = header["x-real-ip"], complete_dtm = bson.get_utc_date(ngx.now() * 1000) }}
                    u, err = col:update({id=body.id}, update, 0, 0, true)

                    -- q,err = gen_qrcode(s,HOST_NAME)

					local doc_id = "DOCUMENT ID:" .. s.id
					
					if( rc:get('is_ldd') and rc:get('is_ldd')=='true'  )then
                       doc_id = ''
					end

                    local res0, err0 = do_prog('cpdf', '-add-text', doc_id, '-topleft', 30, '-font-size', 12, pdf_file , '-o', '/home/creden/work/app/client/face/images/card/'..s.id..'-stamp2.pdf')
                    rc:set('tc_edoc_2',cjson.encode(s))
                    local res, err = do_prog('lua', '/home/creden/work/app/server/capi/tc_edoc_2.lua', cjson.encode(s), '1')
                    local res_page, err_page = prog('sh', '/home/creden/work/app/server/capi/pdf_page.sh', '/home/creden/work/app/client/face/images/card/'..s.id..'ts.pdf')
                    if res_page and res_page.stdout ~= '' then
                        local p = string.match(res_page.stdout, '%d+')
                        rc:set('nut_pdf', p)
                        ngx.log(ngx.NOTICE, p)
                        do_prog('lua', '/home/creden/work/app/server/capi/tc_edoc_2.lua', cjson.encode(s), p and p or '-')
                    else
                        rc:set('nut_pdf_error', cjson.encode(err_page))
                    end

                    if err then
                        ret = {success = false, error_msg = err}
                    else
                        ret = {success = true, error_msg = err}
                    end
    
                    

                    do_prog('cp', '/home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp2.pdf', '/home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp-backup.pdf')

                    do_prog('pdftk', '/home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp-backup.pdf', '/home/creden/work/app/client/face/images/card/' .. s.id .. 'ts.pdf', 'cat', 'output', '/home/creden/work/app/client/face/images/card/'.. s.id ..'-stamp.pdf')

                    -- Add attachments
                    for i,v in ipairs(s.fields) do  
                        ngx.log(ngx.NOTICE,' add attachments for ' .. v.id .. ' ' , v.type)
                        if(v.toolName and v.toolName=='Attachment')then
                           do_prog('cpdf', '-merge', 
						   '/home/creden/work/app/client/face/images/card/'..s.id..'-stamp.pdf',
						   '/home/creden/work/app/client/face/images/card/'..v.toolData, '-o',
						   '/home/creden/work/app/client/face/images/card/' .. s.id .. '-stamp.pdf')
                         end   
                    end
                    -- end add attachments
    
                    --individule sign
                    for i4,_signer in ipairs(s.signers) do 
                       local pin_key = s.id .. '.' .. _signer.email
                       local pin_val = rc:get(pin_key)
                       ngx.log(ngx.NOTICE, "CHK PIN [".. pin_key ..'], [' .. tostring(pin_val)..']')

                       if(pin_val)then
                           ngx.log(ngx.NOTICE, "DO PIN" .. _signer.email .. ', ' .. pin_val)
                           do_prog('sh', '/home/creden/work/app/cert/sign.sh', _signer.email , pin_val ,   s.id .. '-stamp')    
                       else
                           ngx.log(ngx.NOTICE, "SKIP PIN")
                       end
                    end
                    --end individule sign

                    --ETDA Timestamp
					local ts_file = '/home/creden/work/app/client/face/images/card/' .. s.id .. '-stamp.pdf'
                    --D3 do_prog('sh', '/home/creden/scripts/ts.sh', s.id .. '-stamp.pdf')
                    --D4 do_prog('cp', '/home/creden/work/app/client/face/images/card/' .. s.id ..'-stamp_timestamped.pdf',ts_file)
                    do_prog('redis-cli', 'expire',  pdf_dump_key , '16')
                    --END ETDA Timestamp

                    --AddPDF2Redis
                    rc:rpush('pdflist',ts_file)

					--Blockchain
					local bc_cmd = 'lua /home/creden/scripts/put_tx.lua '.. s.id .. ' "' .. ts_file  ..'"' ..' | node /home/creden/scripts/stellar_tx_pub2.js | /home/creden/scripts/rset.lua bc:' .. s.id .. ' &'
                    rc:set('last_bc',bc_cmd)
                    line, noti = os.execute(bc_cmd)
					--end Blockchain

                    local res, err = prog('sh', '/home/creden/work/app/server/capi/create_hash.sh', s.id..'-stamp')
                    
                    if err then
                        ret = {success = false, error_msg = err}
                    else
                        str = res.stdout
                        str = str:gsub('\n', '')
                        update = {["$set"] = {hash=str}}
                        u, err = col:update({id=body.id}, update, 0, 0, true)
                    end
                    

                    -- Send Notification Line Document False
					if uperr==0 then
						local str = "Document_success_Document_ID:" .. tostring(body.id) .. '@' .. server_name
                        ngx.log(ngx.NOTICE, str)
						--os.execute('/home/creden/work/app/server/capi/line_esig.sh ' .. str .. ' &')
                    else

                    end
                    
                    -- Line noti Nut
                    for k, v in ipairs(s.signers) do 
                        local line_noti_params = {subject = s.subject, originalFile = s.originalFile, id = s.id, to = v.email}
                        send_line_noti('signed', line_noti_params)
                    end

                else

                     ret = {success = true, error_msg = "N/A2"}
                end

                
            end
--rc:set('data_fields', cjson.encode(images_fields))
ngx.log(ngx.NOTICE, uplog)
print(cjson.encode(ret))
