#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
prog.timeout_fatal = false
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else

    check_session()    
    local db = conn:new_db_handle("dopa")
    local col = db:get_col("user_company")
    local col_esig = db:get_col("esig_user")
    i, err = col:find_one({ _id = object_id.new(convertHexStringToNormal(body.company_id)) })
    e, err_ = col_esig:find_one({ adminEmail = i.create_by })


    if i and e then
        e._id = nil
        local send = { 
            admin = e, 
            email = body.email, 
            company = i.companyName or e.adminEmail .. "_Company" 
        }
        send = cjson.encode(send)
        local m, err = prog('node', '/home/creden/work/app/server/capi/invite_company_mail.js', send)

        local invite_com = { 
            invite_company = i.companyName or e.adminEmail .. "_Company", 
            company_id = body.company_id 
        }
        invite_com = cjson.encode(invite_com)
        local key = "invite_company_" .. body.email
        rc:set(key, invite_com)
        rc:expire(key, 2592000)

        ret = { success = true, error_msg = "resend invite success", error_code = "2013" }
    else
        ret = { success = false, error_msg = "not have root admin company", error_code = "2014" }
    end
end
print(cjson.encode(ret))