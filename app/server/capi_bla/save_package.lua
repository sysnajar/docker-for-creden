#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil
bson = require "resty-mongol.bson"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})

function gen_num( )
    local server_id = '000'
    local inc = tostring(rc:incr('genref_eks'))
    while(#inc<7) do
         inc = '0' .. inc
    end
 
    return server_id .. inc
 end

 function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.email = session.data.username
        body.company_id = session.data.company_id
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    check_session()
    db = conn:new_db_handle("dopa")
    col = db:get_col("package_ss")

		
	-- re compute total
	-- 1 price_package
    -- local pack_price_list = {free = 0, basic = 1790, advanced = 5990, premium = 15990}
    -- local price_package   = pack_price_list[body.package]
    -- assert(price_package, "Invalid price_package " .. body.package)
	-- body.price_package = tostring(price_package)

	-- --2 price_user
	-- local price_user = (body.user_add_on or  0) * 1500


	-- --3 price_storage
    --  local price_storage = 0
	--  if(body.storage and #body.storage>0) then
    --     local storage_price_list  = { ["10"] = 50 , ["25"] = 125 , ["50"] = 250 , ["100"] = 500 }
	-- 	price_storage = storage_price_list[tostring(body.storage[1].size)] or 0
	--  end

	-- --4 price_acvance_func
	-- local price_fn = (body.advance_func and 3000 or  0)


	-- --5
	-- local total_price = price_package + price_user + price_storage + price_fn

	-- assert(total_price == body.total , "Invalid Price !!!")

    -- body.date_buy = (os.time() + (7*60*60))
    body.date_buy = bson.get_utc_date(os.time() * 1000)
    body.status = false
    body.ref_id = gen_num()
    u, err = col:insert({body})

    if u then 
        ret = { success = true, error_msg = "Add package success", error_code = "", data = body}
    else
        ret = { success = false, error_msg = "Add package not found", error_code = "" }
    end


end

print(cjson.encode(ret))
