#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { id_folder = arg[1]}
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
        body.email = session.data.username
    end
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else    

    check_session()
    db = conn:new_db_handle("edoc")
    col = db:get_col("stamp")

    stamp = { _id = object_id.new(convertHexStringToNormal(body.id)), company_id = body.company_id }
    s, err = col:find_one(stamp)

    if err then
        ret = { success = false, error_msg = "stamp not find", error_code = ""}
    else

        i, err = col:delete({_id = object_id.new(convertHexStringToNormal(body.id))}, 1, 0)
        if err then
            ret = {  success = false, error_msg = "Remove stamp unsuccess", error_code = "" }
        else
            ret = { success = true, error_msg = "Remove stamp success", error_code = "" }
        end

    end

end
print(cjson.encode(ret))
