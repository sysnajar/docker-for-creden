#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local session = require "resty.session".open({secret = "4321"})
local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end
--ngx.log(ngx.INFO,"Execute query2 aaa,  size before = ")

function match_pos(doc, pos_id) 

for i,signer in ipairs(doc.signers or {}) do
    if(signer.position_id and signed.position_id==pos_id)then
		return true
	end
end


return false
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else
	-- data={}
--ngx.log(ngx.INFO,"Execute query2 aaa,  size before = ")
	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list")

    if(not body.email or body.email=='' or type(body.email) == 'userdata')then  
        print(cjson.encode({success = false, error_msg = "session expired"}))
	    ngx.exit(ngx.HTTP_OK)
    end	

	--prevent email param
	if(not session.data.username) then
		print(cjson.encode({success = false, error_msg = "session expired"}))
	    ngx.exit(ngx.HTTP_OK)
	else
		if(body.email~=session.data.username) then
			ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
			body.email = session.data.username
		end
	end	
	--prevent email param

--ngx.log(ngx.INFO,"Execute query2 aaa,  size before = ")
  --find trash list
  local deleted_docs = db:get_col('deleted_doc'):find({email = body.email})
  local deleted_ids = {0}
	for k, v in deleted_docs:pairs() do
       table.insert(deleted_ids, tostring(v.id))
    end
 --end


  s = {}  
  query, query2 = {}, nil
  local position_list = common.edoc_get_positions_id_by_email(body.email) -- {'5c59757b778fdc6a5fd98b82'}
	local my_pos = "POSXXX"
	if(#position_list>0 and position_list[1]~='') then my_pos = position_list[1] end
    ngx.log(ngx.NOTICE,"my_pos" .. my_pos )


  if body.cat=="signer" or body.cat=='all' then
    query = {owner={['$ne']=body.email}, signers={['$elemMatch']={email=body.email}},
    id = {['$nin'] = deleted_ids }, is_template={['$ne']=true} }

    --query2 = nil
	
    query2 = { owner={['$ne']=body.email}, 
	           signers={['$elemMatch']={email='', position_id = {['$in'] = position_list}}}
		     }
    -- id = {['$nin'] = deleted_ids }, is_template={['$ne']=true} }

  end  

  if body.cat=="owner" then
    query = {owner=body.email, is_template={['$ne']=true} }
  end  

  if body.cat=="template" then
    query = {owner=body.email, is_template=true}
  end  
  
  if(body.cat == "trash") then
     query = {id = {['$in']  = deleted_ids}}
  else
     query.id = {['$nin'] = deleted_ids}
     --query = {id = {['$nin']  = deleted_ids}, company_id=body.company_id}
  end


  if body.cat=="need_to_sign" then
    query = {fields={['$elemMatch']={email=body.email, status=0}}}
  end  



  if(body.status==0 or body.status==1) then
    if(true)   then query.status  = body.status end
	
	if(query2) then query2.status = body.status end
  end


    local s2 = nil
    if body.cat=="all" then
        -- old
        -- query_signer = {owner={['$ne']=body.email}, signers={['$elemMatch']={email=body.email}},id = {['$nin'] = deleted_ids }, is_template={['$ne']=true}  }
        -- query_owner = {owner=body.email, id = {['$nin'] = deleted_ids}, is_template={['$ne']=true} }
        -- new
        query_signer = {owner={['$ne']=body.email}, signers={['$elemMatch']={email=body.email}}, status = {['$ne']=3}, id = {['$nin'] = deleted_ids }, is_template={['$ne']=true}}
        query_owner = {owner=body.email,status = {['$ne']=3}, id = {['$nin'] = deleted_ids} , is_template={['$ne']=true}}

        s_signer = col:find(query_signer)
        s_owner = col:find(query_owner)
        --add owner to s table
        for k, v in s_owner:pairs() do
            table.insert(s,v)
        end
        --add signer to s table
        for k1, v1 in s_signer:pairs() do
            table.insert(s,v1)
        end
	    s2 = s



		--position search
		if(true and query2)then
		   	
		   ngx.log(ngx.INFO,"Execute query2 aaa,  size before = " .. #s2)
    	   local sB  = col:find(query2)
	       for k, v in sB:pairs() do 
			   if(match_pos(s2, my_pos))then
			       table.insert(s2, v) 
		       end
		   end
			ngx.log(ngx.NOTICE,"size after = " .. #s2)

	    end
		--position search


    else
    	s  = col:find(query)
		s2 = {} 
	    for k, v in s:pairs() do table.insert(s2, v) end

		--position search
		if(true and query2)then
		   	
		   ngx.log(ngx.INFO,"Execute query2 aaa,  size before = " .. #s2)
    	   local sB  = col:find(query2)
	       
		   for k, v in sB:pairs() do 
			   if(match_pos(s2, my_pos))then
			       table.insert(s2, v) 
		       end
		   end

			ngx.log(ngx.NOTICE,"size after = " .. #s2)

	    end
		--position search
    end

	tmp = {}
	for k, v in ipairs(s2) do
       v._id = tostring(v._id)
       v._company = tostring(v._company)
       if v.createdDtm then
            v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
	   end
       table.insert(tmp,v)
    end
-- ngx.log(ngx.INFO,"Execute query2 aaa,  size before = ")
    ret = {success = true, error_msg = "complete", data = tmp, deleted_ids = deleted_ids,cat=body.cat, query=query}
    print(cjson.encode(ret))
end
