#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"

local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

prog.timeout_fatal = false

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = {fnameTH = 'คุณากร', lnameTH = 'จันทร์ชื่น', dob = '11/08/2534', cvv_code = 'ME0117526468', cardNo = '1100701382071'}
end

function chk_kyc()
    local result = nil
    local params   = 'national_id=' .. body.cardNo ..'&first_name=' .. body.fnameTH .. '&last_name=' .. body.lnameTH .. '&date_of_birth=' ..  body.dob  .. '&cvv_code=' ..body.cvv_code   
    local res, err = prog('curl', '-s' , 
    '-k',
    '-A', "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.103 Safari/537.36", 
    '-H', "Content-Type: application/x-www-form-urlencoded; charset=UTF-8",
    '-H', "Origin: https://empui.doe.go.th",
    '-H', "Host: empui.doe.go.th",
    '-H', "Referer: https://empui.doe.go.th/auth/register_member/2",
    '--data', params, 'https://empui.doe.go.th/auth/checkleser/')
    --local res, err = prog('curl', '-s' , '--data', params, 'https://empui.doe.go.th/auth/checkleser/')

    if(not err)then
	--print('[' .. res.stdout .. ']')
        local tmp = cjson.decode(res.stdout)
            if(tmp) then
                if tostring(tmp.status) == 'true' then
                    result = tmp.status
                    body.success = true
                    body.status = 'pass'
                    body.remarks = 'สถานะปกติ (ข้อมูลถูกต้อง)' 
                    body.remark_code = '001' 
                else
                    body.success = false
                    body.status = 'fail'
                    body.remarks = tmp.message
                    if body.remarks == 'CitizenID is not valid' then
                        body.remark_code =   '002'
                    elseif body.remarks == 'java.lang.RuntimeException: [4] สถานะไม่ปกติ =>ไม่พบเลข Laser จาก PID นี้' or body.remarks == 'java.lang.RuntimeException: [2] บัตรสิ้นสภาพการใช้งาน เนื่องจากกรณี บัตรหาย' then
                        body.remark_code = '003'
                    else
                        body.remark_code = '004'
                    end
                end             
            isValidDopa = tmp.status
            end 
    end
   
    body.isValidDopa = isValidDopa
    if not body.status then body.status = 'wait' body.remarks = 'รอตรวจสอบ' end
    return result
end

chk_kyc()

json = cjson.encode(body)
rc:lpush('kyc_list', json)
print(json)
