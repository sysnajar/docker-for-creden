local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local exec = require'resty.exec'
local rc = redis.connect( '127.0.0.1', 6379)

function convertHexStringToNormal( str )
return (str:gsub('..', function (cc)
return string.char(tonumber(cc, 16))
end))
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {token = args[1],otp = args[2]} 
end

-- CHECK SESSION
if not session.data.company_id then
    ret.error_msg = 'Invalid Data'
    print(cjson.encode(ret))
    return 0
else
    body.company_id = session.data.company_id
end

	conn = mongo()
	conn:set_timeout(1000)
	ok, err = conn:connect('127.0.0.1',27017)
	ret = {success = false}
	
	if not ok then
	    ret = {success = false, error_msg = err}
	else
	    db = conn:new_db_handle("dopa")
	    col = db:get_col("title")
		local created_dtm = ngx.now() * 1000
		local company_id  = object_id.new(convertHexStringToNormal(body.company_id))
        
		local new_title = {title = body.title, created_dtm = ngx.now()*1000,
		      company   = company_id}

        i, err = col:insert({new_title}, nil, true)

        new_rec = col:find_one({created_dtm = created_dtm , company = company_id })
		ret.data = { label = new_rec.title , value = tostring(new_rec._id) }
		ret.success = true

	end

print(cjson.encode(ret))
