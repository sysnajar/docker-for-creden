#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require 'resty-mongol'
local body = nil
bson = require "resty-mongol.bson"
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local session = require "resty.session".open({secret = "4321"})

function gen_num( )
    local server_id = '500'
    local inc = tostring(rc:incr('genref_eks'))
    while(#inc<7) do
         inc = '0' .. inc
    end
 
    return server_id .. inc
 end

 function check_session()
    if not session.data.company_id and not session.data.username then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.email = session.data.username
        body.company_id = session.data.company_id
    end
end

if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
    body = { }
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = "not connect mongo", error_code = "" }
else
    check_session()


    db = conn:new_db_handle("dopa")
    col = db:get_col("package_cert")

    --local price_map = {['m6'] = 450, ['y1'] = 750, ['ndid'] = 205}
    local price_map = {['m6'] = 1, ['y1'] = 1, ['ndid'] = 1}
    local desc_map  = {['m6'] = 'Digital certificate 6 month', ['y1'] = 'Digital certificate 1 year', ['ndid'] = 'NDID verification'}
    
    body.total = tostring(price_map[body.cert_type])
    body.product_detail = tostring(desc_map[body.cert_type])
    body.purchased_date = bson.get_utc_date(os.time() * 1000)
    body.status = false
    body.used   = false
    body.ref_id = gen_num()
    u, err = col:insert({body})

    if u then 
        ret = { success = true, error_msg = "Add package success", error_code = "", data = body}
    else
        ret = { success = false, error_msg = "Add package not found", error_code = "" }
    end


end

print(cjson.encode(ret))
