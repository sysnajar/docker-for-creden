#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
local md5 = require "md5"

prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.save_log({desc = "api_kyc"})
    ngx.req.read_body()
    print = ngx.say
	--body  = ngx.req.get_post_args()
    body = cjson.decode(ngx.req.get_body_data())
    header = ngx.req.get_headers()
else
   body   = {}
end

body.username = 'credendemo'

if header.apiKey ~= 'QGWikbTLBz1OnVrlOzJCtcLuKJXjTXcz' then
    local ret = {success = false, error_msg = 'Authentication Fail'}
    print(cjson.encode(ret))    
    ngx.exit(ngx.HTTP_OK)
end

function chk_kyc()
   --kyc
    local isValidDopa = false
    local res, err = prog('lua', '/home/creden/work/app/server/capi/kyc_demo.lua', cjson.encode(body))
    if(not err)then
        local tmp = cjson.decode(res.stdout)
        if(tmp) then
            if tostring(tmp.status) == 'true' then
                --body.status = 'pass'
                body.remarks = 'สถานะปกติ => พบข้อมูลที่ระบบฐานข้อมูลของกรมการปกครอง'  
            else
                --body.status = 'fail'
                body.remarks = tmp.message   
            end             
            isValidDopa = tmp.status
        end 
    end 
    body.isValidDopa = isValidDopa
end

function chk_is_valid(isValidDopa, isFaceIdentical)
    if tostring(isValidDopa) == 'true' and tostring(isFaceIdentical) == 'true' then
        body.isValid = true
    else
        body.isValid = false
    end 
end

    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if err then
        ret = {success = false, error_msg = err}
    else
        db = conn:new_db_handle("dopa")
        col = db:get_col("ekyc")

        if true then
            chk_kyc()
            body.channel = 'api'
            body.createDate = os.date("%x")
            body.createTime = os.date("%X")
            body.createdDtm = bson.get_utc_date(ngx.now() * 1000)
            body.compCode = body.username
            body.txid = tostring(rc:incr('kyc.tx.counter'))
            body.username = nil
            body.password = nil
            body.faceConfidence = 0.5565 
            body.isFaceIdentical = true
            body.process_status = 'finished'
            chk_is_valid(body.isValidDopa, body.isFaceIdentical)
            n, err = col:insert({body})

            if not n then
                ret = {success = false, error_msg = err}
            else
                ret = {success = true, error_msg = err, txid = body.txid , usr = body.username}
            end
        else
            ret = {success = false, error_msg = 'Invalid Login'}
        end
    end

   print(cjson.encode(ret))
   

