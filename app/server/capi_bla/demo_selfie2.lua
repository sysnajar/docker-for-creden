#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout_fatal = false
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local mongo = require "resty-mongol"
local body = nil
local common = require "common"

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {img = arg[1], faceId1 = arg[2]} 
end



common.validate_selfie_img(body.img, 'invalid param')
common.validate_faceId1(body.faceId1, 'invalid param') -- todo ask O
common.validate_alphanumeric(body.txid, 'invalid param') -- todo ask O

-- common.validate_numbrt(body.faceId1, 'invalid param') -- todo ask O
-- common.validate_alphanumeric(body.txid, 'invalid param') -- todo ask O

os.execute('mv /home/creden/work/app/client/face/images/card/'..body.img..'.png /home/creden/work/app/client/face/images/card/selfie_'..body.txid..'.png')
body.name = 'selfie_'..body.txid

--reduce file size from fullter
-- os.execute('convert -density 400 -resize 1024 /home/creden/work/app/client/face/images/card/'..body.name..'.png /home/creden/work/app/client/face/images/card/'..body.name..'.png')

-- ngx.log(ngx.NOTICE,'000000000000000000000000000000000000000')
ngx.log(ngx.NOTICE,cjson.encode(body))
--base64 to image
local t = {success = false, err = 'N/A', face = 'N/A2'}
local res, err = prog('node', '/home/creden/work/app/server/capi/detect_selfie.js', body.faceId1, body.name)
 ngx.log(ngx.INFO, 'faceId1 = ' .. body.faceId1 )
if res.stdout then
    t.success = true
    t.face = cjson.decode(res.stdout)
    t.filename = body.name
	--face_id_card check
    if(t.face and t.face.isIdentical==true)then
        local res2, err2 = prog('sh', '/home/creden/work/app/server/capi/ocr_azure2_get.sh', body.name ..'.png')
		
		if(res2.stdout) then
           local id_check = cjson.decode(res2.stdout)
		   if(not id_check.success)then	
		      t.face.isIdentical = false
			  t.face.raw_confidence = t.face.confidence
			  t.face.confidence = 0
		      t.face.remarks = "invalid face id check"
			  t.face.text_count = id_check.text_count
			  t.face.text_err = id_check.err or 'N/A'
		   end
		end
	end
	--face_id_card_check
    conn = mongo()
    conn:set_timeout(1000)
    ok, err = conn:connect('127.0.0.1',27017)
    if not ok then
        ret = {success = false, error_msg = err}
    else
        -- db = conn:new_db_handle("test")
        -- col = db:get_col("user")
        -- s = col:find_one({email=body.email})
        -- if s then
        --     selector = {email=body.email}
        --     --update = {email=s["email"], password=s["password"], firstname=s["firstname"], lastname=s["lastname"], dob=s["dob"], activated=1,code=s["code"]}
        --     update = {["$set"] = {profilePicture = body.img..'.png'}}
        --     i, err = col:update(selector, update, 0, 0, true)
        -- end
    end
end

print(cjson.encode(t))
