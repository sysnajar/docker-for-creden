#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
bson = require "resty-mongol.bson"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
ngx.req.read_body()
print = ngx.say
body = {}

local ret = {page = "auto"}
ctx   = ctx or common.mongo_ctx()
local no_company_list   = {}
local have_company_list = {}

function step2(email, id)

	local com = ctx.dopa:get_col("user_company"):find_one({create_by = email})
	if(not email)then return end
	if(com) then return end

	local del_id = '9999' .. email .. os.time()
	ngx.sleep(2)

	local doc = {
		del_id       = del_id,
		companyName  = "",
		-- esig_user_id = id,
		create_by    = email,
		api_key      = '9999'..email,
		--api_key      = 'iFxOyMs1XQxIxVIC4H33IzICpXLOo40P',
	    createdDtm   = bson.get_utc_date(ngx.now() * 1000)

	}

    ctx.dopa:get_col("user_company"):insert({doc}) 
    local company = ctx.dopa:get_col("user_company"):find_one({del_id = del_id})

	step3(email , company._id, del_id, id)

	ctx.dopa:get_col("esig_user"):update({adminEmail = email},{['$set']={ company_id = tostring( company._id) }})

end

function step3(email , company_id, del_id, esig_id)
	local doc = {
		del_id       = del_id,
		email        = email,
		status       = "APPROVE",
		created_by   = email,
		role         = "ADMIN",
	    createdDtm   = bson.get_utc_date(ngx.now() * 1000),
		positions    = {{position_name = "N/A", position_id = del_id}},
		company_id = company_id,
		esig_user_id = esig_id

	}

    ctx.dopa:get_col("company_members"):insert({doc}) 



end



--step1. loop users
col = ctx.dopa:get_col("esig_user")
list, err = col:find({})

local c = 0
local list_user = {}
for i,v in list:pairs() do

	if(not v.company_id) then
		v._id = nil
		table.insert(list_user, v.adminEmail)
	end
end

print(cjson.encode(list_user))
-- ngx.exec(cjson.encode(list_user))

--ret.data = s
-- ret.counter    = c
-- ret.no_count   = #no_company_list
-- ret.have_count = #have_company_list print(cjson.encode(ret)) 
