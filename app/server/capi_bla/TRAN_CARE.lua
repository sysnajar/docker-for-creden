#!/usr/bin/env lua

function init()
    if(#arg > 0  )then
      cjson = require 'cjson.safe'
      redis = require 'redis'
      rc = redis.connect('127.0.0.1', 6379)
      id = arg[1]
      return true
    else
      return false	
    end 
end

    local fn =  function(id)
        local key = 'PREP:CARE:' .. id
        local t1 = cjson.decode(rc:get(key))

        local t2 = {
            IN_occu = t1.user.job,
            IN_salgrade = t1.user.basicSalary,
            IN_workyrs  = t1.user.workMonth
            }
    
        return t2
    end
    
    if(init())then
      print(cjson.encode(fn(id)))
    else
      return fn
    end
    