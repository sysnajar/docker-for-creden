local cjson  = require 'cjson'
local redis = require 'redis'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {tel = arg[1]} 
end

tel = body.tel

if(#tel >= 10)then
	if string.match(tel,'0%d%d%d%d%d%d%d%d%d') then
		tel = string.sub(tel, 2)
	elseif string.match(tel,'+66%d%d%d%d%d%d%d%d%d') then
		tel = string.gsub(tel, "+66", "")
	end

	math.randomseed(os.clock()*100000000000)
	otp = ''
	for i=1,3 do
		a = math.random(10, 99)       
		otp = otp .. a
	end
	token = ''
	for i=1,10 do
		a = math.random(10, 99)       
		token = token .. a
	end
	cmd = 'sh /home/creden/work/app/server/capi/request.sh '..tel..' '..otp..' &'
	os.execute(cmd)
	key = 'otp_'..token..'_'..otp
	rc:set(key,1)
	rc:expire(key,300)
	res = {success = true, token = token}
else
	res = {success = false, token = 'N/A', error = 'Invalid phone number'}
    
end
print(cjson.encode(res))
