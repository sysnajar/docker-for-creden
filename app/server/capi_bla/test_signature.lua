local common = require 'common'
local username = arg[1]
local text     = arg[2]

assert(common.validateEmail(username), "InvalidEmail Address")
assert(common.validateAlphanumeric(text), "Invalid Message")
assert(#text>4 and text<20, "Invalid text length")

os.exit(0)
