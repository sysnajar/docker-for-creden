#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
local ret = {success = false}

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_session()
    if not session.data.company_id then
        print(cjson.encode({success = false, error_msg = "no session company id", error_code = "9002"}))
        ngx.exit(ngx.HTTP_OK)
    else
        body.company_id = session.data.company_id
    end
end

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = cjson.decode(ngx.req.get_body_data())
else
   body   = { }
end

conn = mongo()
conn:set_timeout(5000)
local ok,  err = conn:connect('127.0.0.1',27017)

if err then
    ret = { success = false, error_msg = "not connect mongo", error_code = "9001" }
else


    local db = conn:new_db_handle("edoc")
    local col = db:get_col("edoc_list_draft")
    local list_id = body.list_id
    for i, v in ipairs(list_id) do
        owner_is_remove = false
        owner_time_remove = ""
        key = { _id = object_id.new(convertHexStringToNormal(v))}
        update_cmd = { status_draft_delete = false, owner_is_remove = owner_is_remove, owner_time_remove = owner_time_remove }
        d, err = col:update(key, {["$set"] = update_cmd}, 0, 0, true)
        -- d, err = col:delete({ _id = object_id.new(convertHexStringToNormal(v))})
    end


    if err then
        ret = { success = false, error_msg = "remove draft unsuccess", error_code = "9001" }
    else
        ret = { success = true, error_msg = "remove draft success", error_code = "2102" }
    end


end
print(cjson.encode(ret))