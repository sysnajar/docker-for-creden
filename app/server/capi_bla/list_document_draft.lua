#!/usr/bin/env lua
cjson = require 'cjson.safe'
mongo = require "resty-mongol"
object_id = require "resty-mongol.object_id"
common = require "common"
local bson = require "resty-mongol.bson" 
local session = require "resty.session".open({secret = "4321"})
local body = nil
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1],cat = arg[2]}
end


function match_pos(doc, pos_id) 

  for i,signer in ipairs(doc.signers or {}) do
      if(signer.position_id and signed.position_id==pos_id)then
        return true
      end
  end

  return false
end

function get_late_data()
	local chk_dash = rc:get('have_dash_date' .. body.email)       	
		if(chk_dash and not body.startDate and not body.endDate) then
			local dash_start_date = rc:get('set_date_period_start' .. body.email)
			local dash_end_date = rc:get('set_date_period_end' .. body.email)
			local start_date = os.time({year=tonumber(dash_start_date:sub(5)), month=tonumber(dash_start_date:sub(3,4)), day=tonumber(dash_start_date:sub(1,2)), hour=0, sec=0})
			ngx.log(ngx.ERR, '-------has dash--------' .. cjson.encode(dash_start_date))
			
			local end_date = os.time({year=tonumber(dash_end_date:sub(5)), month=tonumber(dash_end_date:sub(3,4)), day=tonumber(dash_end_date:sub(1,2)), hour=16,min=59, sec=59})
			return { start_date = start_date, end_date = end_date }
		
		elseif(chk_dash and body.startDate and body.endDate) then	
			local tmp_date1 = body.startDate
			local tmp_date2 = body.endDate
			ngx.log(ngx.ERR, '-------has dash and body---------' .. cjson.encode(tmp_date1))
			local start_date = os.time({year=tonumber(tmp_date1:sub(5)), month=tonumber(tmp_date1:sub(3,4)), day=tonumber(tmp_date1:sub(1,2)), hour=0,min=0, sec=0})
						
			local end_date = os.time({year=tonumber(tmp_date2:sub(5)), month=tonumber(tmp_date2:sub(3,4)), day=tonumber(tmp_date2:sub(1,2)), hour=16,min=59, sec=59})
		
			rc:set('set_date_period_start' .. body.email, body.startDate )
			rc:set('set_date_period_end' .. body.email, body.endDate)

			return { start_date = start_date, end_date = end_date }
		elseif(not chk_dash and body.startDate and body.endDate) then
			local tmp_date1 = body.startDate
			local tmp_date2 = body.endDate
			ngx.log(ngx.ERR, '-------has body--------' .. cjson.encode(tmp_date1))
			local start_date = os.time({year=tonumber(tmp_date1:sub(5)), month=tonumber(tmp_date1:sub(3,4)), day=tonumber(tmp_date1:sub(1,2)), hour=0,min=0, sec=0})
			local end_date = os.time({year=tonumber(tmp_date2:sub(5)), month=tonumber(tmp_date2:sub(3,4)), day=tonumber(tmp_date2:sub(1,2)), hour=16,min=59, sec=59})
		
			rc:set('set_date_period_start' .. body.email, body.startDate )
			rc:set('set_date_period_end' .. body.email, body.endDate)
			rc:set('have_dash_date' .. body.email,  body.email)
			ngx.log(ngx.ERR, '-------chk format date--------' .. start_date)
    		
			return { start_date = start_date, end_date = end_date }
		else
			--local start_date = os.time() - (30 * 24 * 60 * 60)
    			--local end_date = os.time()
    		
			local current_time = os.time()
			local current_year = tonumber(os.date("%Y", current_time))
			local current_month = tonumber(os.date("%m", current_time))
			--local current_day = tonumber(os.date("%d", current_time))

			--local start_date = os.time({year=current_year, month=current_month - 1, day=1, hour=0, minute=0, second=0})
			--local end_date = os.time({year=current_year, month=current_month, day=1, hour=0, minute=0, second=0})
			--local end_date = os.time({year = current_year, month = current_month, day = current_day - 1, hour = 23, minute = 59, second = 59})
			local end_date = os.time()
			local days_in_month = os.date("*t", end_date).day
			ngx.log(ngx.ERR, '-------chk date filter--------' .. cjson.encode(days_in_month))
			local start_date = os.time({year=current_year, month=current_month - 1, day=days_in_month, hour=0, minute=0, second=0})
    		
			ngx.log(ngx.ERR, '-------defaut filter--------')
			return { start_date = start_date, end_date = end_date }
		end
end

conn = mongo()
conn:set_timeout(5000)
ok, err = conn:connect('127.0.0.1',27017)

if not ok then
    ret = {success = false, error_msg = err}
else

	db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list_draft")

  if(not body.email or body.email=='' or type(body.email) == 'userdata')then  
       print(cjson.encode({success = false, error_msg = "session expired"}))
	    ngx.exit(ngx.HTTP_OK)
  end	

	if(not session.data.username) then
		print(cjson.encode({success = false, error_msg = "session expired"}))
	  ngx.exit(ngx.HTTP_OK)
	else
		if(body.email~=session.data.username) then
			ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
			body.email = session.data.username
		end
	end	

  local get_late_date = get_late_data()
  local start_range2 =  get_late_date.start_date * 1000
  local end_range2  = get_late_date.end_date * 1000
  local start_range =  bson.get_utc_date(get_late_date.start_date * 1000) 
  local end_range  = bson.get_utc_date(get_late_date.end_date * 1000)
  local date_range = {['$gte'] = start_range, ['$lte'] = end_range}
  s = {}  
  -- query = { owner = body.email }
  query = { owner = body.email, status_draft_delete={ ['$ne'] = true}, createdDtm = date_range }

  s  = col:find(query)
	s2 = {} 
	for k, v in s:pairs() do 
    table.insert(s2, v) 
  end

	tmp = {}
	for k, v in ipairs(s2) do
    v._id = tostring(v._id)
    -- v._company = tostring(v._company)
    if v.createdDtm then
      v.time = os.date('%d/%m/%Y %H:%M:%S', tonumber(v.createdDtm)/1000 + (7 * 60 * 60))
    end
      v.imageFiles = nil
      v.imageMerge = nil
      table.insert(tmp,v)
  end

  ret = { success = true, error_msg = "complete", data = tmp, cat = body.cat, startDate = start_range2, endDate = end_range2  }
  print(cjson.encode(ret))
end
