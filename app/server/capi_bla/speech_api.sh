##ffmpeg -loglevel panic -i /home/creden/work/app/client/face/images/vdo/$1.webm /home/creden/work/app/client/face/images/vdo/$1.wav
     #-H "Authorization: Bearer $(gcloud auth print-access-token)"  \
# -H "Authorization: Bearer $(redis-cli get key.google.translation)"  \
DATA=$( base64 /home/creden/work/app/client/vdo/ex/$1.wav -w 0  )

curl -X POST \
     -H "Authorization: Bearer $(gcloud auth print-access-token)"  \
     -H "Content-Type: application/json; charset=utf-8" \
     -d @- "https://speech.googleapis.com/v1/speech:recognize" <<CURL_DATA
     {
        "config": {
          "encoding": "LINEAR16",
          "languageCode": "th-TH"
        },
        "audio": {
          "content": "$DATA"
        }
     }

CURL_DATA
