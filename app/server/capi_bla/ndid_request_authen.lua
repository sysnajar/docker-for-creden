cjson = require('cjson')
redis = require('redis')
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
prog.timeout = 1000 * 60
prog.timeout_fatal = false
rc = redis.connect('127.0.0.1')
ngx.log(ngx.NOTICE,"************** ndid_request_authen got fired **************")
local session = require "resty.session".open({secret = "4321"})
ngx.req.read_body()
body = ngx.req.get_body_data()
print = ngx.say
ngx.log(ngx.NOTICE, body)

if(true)then
  body = cjson.decode(body)
  myusername  = session.data.username
  assert(body.citizen_id, "Invalid citizen_id")
  assert(body.idp_id, "Invalid idp_id")
  assert(myusername, "Invalid user")
else
  body = {idp_id = "1234", citizen_id = "111"}	
end


as_list = {}
--ready
as_list['C366AC66-B24A-4E3D-9C26-2C454B7DD16D']  = 'A2B98B05-A063-44CB-99F3-302428F38BD2' --BBL
as_list['DC9E1ACB-D450-438F-9558-FE7672F3F3EB']  = 'A5C2FBD3-853C-419E-B5F6-9EDBF568A0E2' --BAY
as_list['6DDF80BD-FCF8-45A6-AB5D-0AA27586501E']  = '8D86D107-80C3-4B2A-B928-FCB9D5833741' --KBANK
as_list['3E118E73-6243-4E23-BE0D-5CF709B760A0']  = '80B2A1EB-92AF-4831-911E-9D49FA57A214' --TMB
as_list['D1F1532B-19AF-4A88-86AC-79693EC158C1']  = '614BC141-6D14-417A-88FD-B314ED149BA5' --SCB
as_list['3687BF5E-B31B-4FDE-8F5A-DFC15CC6FC27']  = 'BB31343A-2E81-490F-B678-65DEC9065058' --KK
as_list['A53750C5-7849-40C3-9673-26E2674A8C71']  = '3FD1EC05-0A4A-486D-B4E9-6DB64C031C7F'  --CIMB

-- not ready
as_list['KTB']                                   = '6EF05B36-E794-4CBB-8B22-66EA5E76A9DA'
as_list['TRUE']                                  = '6E52D525-E86D-4FD7-8C20-ECC809F66460'
as_list['6804AF2A-10F9-448B-91D7-3D53FD4D5C8C'] = 'GSB'
as_list['B9F384A9-E06D-4AAD-8B1A-EFBDB564E161'] = 'GHB'


local json = [[{
    "mode": 1,
    "min_idp": 1,
    "idp_id_list": [
        "XXX"
    ],
    "data_request_list": [
        {
            "min_as": 1,
            "as_id_list": [
                "YYY"
            ],
            "service_id": "001.cust_info_001"
        }
    ],
    "reference_id": "XXX",
    "callback_url": "https://hjkl.ninja/capi/ndid_callback",
    "request_message": "XXX",
    "min_ial": 2.3,
    "min_aal": 2.2,
    "request_timeout": 3600
}]]



local ref_no = 'CDN' .. tostring(os.time())
t = {idp_id = body.idp_id , citizen_id = body.citizen_id  , reference_id = ref_no, request_message = ref_no}
local req            = cjson.decode(json)
req.idp_id_list[1]   = t.idp_id 
req.reference_id     = t.reference_id
req.request_message  = t.request_message
req.data_request_list[1].as_id_list[1] = as_list[t.idp_id]

local ndid_json = cjson.encode(req)
ngx.log(ngx.INFO, ndid_json)

local key = 'payload.'..ref_no
rc:set(key, ndid_json)
rc:expire(key, 60*60)

os.execute('sh /home/creden/work/app/server/capi/ndid_req.sh ' .. key)

	local url   = 'http://127.0.0.1:8443/rp/requests/citizen_id/' .. t.citizen_id
	local file  = '/home/creden/work/app/client/face/images/card/'..key..'.json' 
	
	local res, err = prog('curl',  '-X', 'POST',  '-H', "Content-Type: application/json", '--data', '@'..file  ,url)

	local ret = {success = false, request_id = "N/A" , err = false}
	if(not err)then
		ngx.log(ngx.INFO, 'NDID res', res.stdout)
		local ndid_res = cjson.decode(res.stdout)
		if(ndid_res.request_id)then
		   ret.success = true
		   ret.request_id = ndid_res.request_id
		   ret.ref_no     = ref_no

		   rc:set('ndid.'..ref_no        , ret.request_id)
		   rc:set('ndid.'..ret.request_id, ref_no)
		   rc:set('ndid.user.'..ret.request_id, myusername)

		   --update ndid order
		  --[[ local conn = mongo()
		   conn:set_timeout(5000)
		   local ok, err = conn:connect('127.0.0.1',27017)
		   local db = conn:new_db_handle("dopa")
		   local col_packcert = db:get_col("package_cert")
		   local key = {email = myusername or "N/A", status = true, used = false}
		   local cert_rec = col_packcert:find_one(key)
		   if(cert_rec)then
			local update_cmd = {["$set"] = {used = true}}
			local update_old = col_packcert:update(key, update_cmd)
		   end
		   ]]--
		   --end

	    else
		   ret.err = ndid_res.error
        end
	end



print(cjson.encode(ret))
