#!/usr/bin/env lua
local session = require "resty.session".open({secret = "4321"})
--ngx.say("<html><body>Session was started by <strong>", session.data.username or "Anonymous", "from", session.data.custCode or "Unknown Company")
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {adminEmail = arg[1]} 
end
ngx.log(ngx.NOTICE,'SPEED CDECK ', session.data.username   ,'START', session.data.username)

if(not session.data.username ) then
    print(cjson.encode({success = false, error_msg = "session expired"}))
	ngx.exit(ngx.HTTP_OK)
else

        

        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        if not ok then
            ret = {success = false, error_msg = err}
        else
            db = conn:new_db_handle("dopa")
            col = {}
            -- col = db:get_col("esig_user")
            --if body.login_service == "esig" then
              col = db:get_col("esig_user")            
            --else
            --  col = db:get_col("company")
            --end
            s = col:find_one({adminEmail = session.data.username })  
            if s then 
                s._id = nil
				s.adminPassword = nil
				s.adminPasswordC = nil
				s.encryptPass = nil
				s.hashedPassword = nil
				s.cvv_code = nil
				s.txPid = nil
				--s.last_cert_info.pin = nil
                ret = {success = true, error_msg = "complete", data = s}
            else
                ret = {success = false, error_msg = "err"}
            end
        end
        ngx.log(ngx.NOTICE,'SPEED CDECK ', session.data.username   ,'END', session.data.username)
        print(cjson.encode(ret))
end
