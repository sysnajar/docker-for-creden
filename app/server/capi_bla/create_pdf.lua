#!/usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local common = require "common"

local body = nil

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {} 
end
 
function rand(len)
 local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
 local t = {}
 local cnt = 0
 local i = 1
   repeat
   local f = assert (io.popen (cmd, 'r'))
   local line = f:read('*all'):match("%d+")
   f:close()
   cnt = cnt + #line
   table.insert(t, line)
   until cnt>len
 local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
 return ret
end
 local name_pdf =  tostring(rand(8)) .. tostring(os.time())


function fsize (file_name)
	local file = assert(io.open(file_name, "r"))   -- open file
	local current = file:seek()                    -- get current position
	local size = file:seek("end")                  -- get file size
	file:seek("set", current)                      -- restore position
	assert(file:close())                           -- close file
	return size
end



local path = '/home/creden/work/app/client/face/images/card/'
cmd = 'pdftk'
for i, name in ipairs(body.list_file_pdf) do
  
  common.validate_alphanumeric(name, 'invalid name')

  cmd = cmd..' '..path..name
end
cmd = cmd..' cat output '..path..name_pdf..'.pdf'
ngx.log(ngx.NOTICE,cmd)
os.execute(cmd)

local k = 'sizeof:' .. name_pdf 
local v = fsize(path .. name_pdf..'.pdf')
rc:set(k,tostring(v))
rc:rpush('file_list_size', name_pdf)

ret = {success = true, data = name_pdf}
print(cjson.encode(ret))
