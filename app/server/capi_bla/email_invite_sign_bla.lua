#!/usr/bin/env lua
local cjson = require 'cjson'
local body = nil
local mongo = require "resty-mongol"
local redis = require 'redis'

local object_id = require "resty-mongol.object_id"
local HOST_NAME = os.getenv("HOST_NAME")
--local line_esign = require "line_esign_noti"
rc = redis.connect('127.0.0.1', 6379)
local CI_HEADER_1,CI_BG_1,LOGO_1,ccccc
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
   body  = {to = arg[1] , id = arg[2], subject = "{CREDEN] คุณได้รับคำเชิญเซ็นเอกสาร"} 
end
        -- ngx.log(ngx.NOTICE,'line 10o009',cjson.encode(body))

function send_line_noti(type, params)
	local res_noti, user_id = line_esign.get_line_user_id(params.to)
    local text_secret = ""
    if params.secret_level and params.secret_level > 1 then
        if params.secret_level == 4 then
            text_secret = "ความลับที่สุด (Top Secret)"
        elseif params.secret_level == 3 then
            text_secret = "ความลับมาก (Secret)"
        elseif params.secret_level == 2 then
            ext_secret = "ความลับ (Confidential)"
        end
        params.secret = text_secret
    end

	if res_noti then
		local document_link = HOST_NAME.. "/esign/#/document?id=" .. params.id
		params.url = document_link
		local message = line_esign.message(type, params)
		line_esign.push(user_id, message)
	end
end

function convertHexStringToNormal( str )
    return (str:gsub('..', function (cc)
    return string.char(tonumber(cc, 16))
    end))
end

function check_packages(t)
    local ret = 0
    for k, v in t:pairs() do
        if(v.package ~= 'addon' and v.package ~= 'dipchip' )then
            level_map = {free = 0 ,basic = 1, advanced = 2, premium = 3}
            ret = level_map[v.package] or 0
        end
    end
    return ret
end


--clone table using JSON encoding and decoding
function clone_using_json(t)
local json = cjson.encode(t)
return cjson.decode(json)
end

function get_user_company(email)
local CI_BG = ''
local CI_HEADER = ''
local LOGO = ''
local Status_com = 'false'
  conn = mongo()
  conn:set_timeout(1000)
  ok, err = conn:connect('127.0.0.1',27017)

     db = conn:new_db_handle("dopa")
     
    col2 = db:get_col("company_members")
    chk_s1, chk_err = col2:find_one({email = email}, nil, true)
    if chk_s1 then
    db2 = conn:new_db_handle("dopa")
    col3 = db2:get_col("user_company")
    usr_com,err = col3:find_one({_id=object_id.new(convertHexStringToNormal(tostring(chk_s1.company_id)))})
        if usr_com.template ~= nil then
         CI_BG = usr_com.template.CI_BG
         CI_HEADER = usr_com.template.CI_HEADER
         LOGO = usr_com.template.LOGO
         Status_com = 'true'

         return CI_HEADER,CI_BG,LOGO,Status_com
        else
         CI_BG = '#1667B2'
         CI_HEADER = '#1667B2'
         LOGO = HOST_NAME .. '/signature/img/Creden_logos_Logo_C.png'
         Status_com = 'false'
         return CI_HEADER,CI_BG,LOGO,Status_com
        end
    else
         CI_BG = '#1667B2'
         CI_HEADER = '#1667B2'
         LOGO = HOST_NAME .. '/signature/img/Creden_logos_Logo_C.png'
         Status_com = 'false'
         return CI_HEADER,CI_BG,LOGO,Status_com
    end
    return 'N/A'
end

list_send_mail = rc:get('list_send_mail')
list_send_mail = cjson.decode(list_send_mail)

for i,v in ipairs(list_send_mail) do
	
    if v.password == nil then
        v.password = ''
    end
    -- CI_HEADER_1,CI_BG_1,LOGO_1,ccccc = get_user_company(v.to)
    -- print(v.to)
    if v.user_group and #v.user_group > 0 then
        
        for n,u in ipairs(v.user_group) do
			local v2 = clone_using_json(v)
			v2.to = u.email
            conn = mongo()
            conn:set_timeout(1000)
            ok, err = conn:connect('127.0.0.1',27017)
            db_pack = conn:new_db_handle("dopa")
            col_pack = db_pack:get_col("package_ss")
            user_company = db_pack:get_col("esig_user")
            find_company_id = user_company:find_one({adminEmail=v2.to})
            --packages = col_pack:find({company_id = find_company_id.company_id , active_package = true, package = {['$ne']='dipchip'}})
            --mainPack = check_packages(packages) 
            if tostring(v.needToSign)=="false" then
               -- cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_viewonly_bla.js "'..u.email..'" "'..v.msg..'" "'..v.private_msg..'" "'..v.id..'" "'.. v.sender_email .. '" "' .. v.sender_name .. '" "' .. v.subject .. '" "' .. v.secret_level .. '"'
                cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_bla.js "'..u.email..'" "'..v.msg..'" "'..v.private_msg..'" "'..v.id..'" "'.. v.sender_email .. '" "' .. v.sender_name .. '" "' .. v.subject .. '" "' .. v.secret_level .. '"'
	            os.execute(cmd)
                --if mainPack > 0 then
                --    send_line_noti('viewer', v2)
                --end
            else
                cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_bla.js "'..u.email..'" "'..v.msg..'" "'..v.private_msg..'" "'..v.id..'" "'.. v.sender_email .. '" "' .. v.sender_name .. '" "' .. v.subject .. '" "' .. v.secret_level .. '" "' .. v.secret_level .. '"'
	            os.execute(cmd)
               -- if mainPack > 0 then
               --     send_line_noti('signer', v2)
               -- end
                
            end
            
        end

    else
        v.private_msg = v.private_msg or ''
        conn = mongo()
        conn:set_timeout(1000)
        ok, err = conn:connect('127.0.0.1',27017)
        db_pack = conn:new_db_handle("dopa")
        col_pack = db_pack:get_col("package_ss")
        user_company = db_pack:get_col("esig_user")
        find_company_id = user_company:find_one({adminEmail=v.to})
        --packages = col_pack:find({company_id = find_company_id.company_id , active_package = true, package = {['$ne']='dipchip'}})
        --mainPack = check_packages(packages) 
        if tostring(v.needToSign)=="false" then
            cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_bla.js "'..v.to..'" "'..v.msg..'" "'..v.private_msg..'" "'..v.id..'" "'.. v.sender_email .. '" "' .. v.sender_name .. '" "' .. v.subject .. '" "' .. v.secret_level .. '"'
	        os.execute(cmd)
           -- if mainPack > 0 then
            --    send_line_noti('viewer', v)
            --end
            
        else
            cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_bla.js "'..v.to..'" "'..v.msg..'" "'..v.private_msg..'" "'..v.id..'" "'.. v.sender_email .. '" "' .. v.sender_name .. '" "' .. v.subject .. '" "' .. v.secret_level .. '"'
            os.execute(cmd)
            --if mainPack > 0 then
            --    send_line_noti('signer', v)
            --end
            
        end
    end
	-- cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_bla.js "'..v.to..'" "'..v.msg..'" "'..v.subject..'" "'..v.id..'" "'..v.password..'" "'..v.signer..'" "'.. v.sender_email .. '" "' .. v.sender_name .. '" "'..CI_BG_1..'" "'..LOGO_1..'" "'..ccccc..'"'
	-- os.execute(cmd)
    -- sign_mail.js  เมลเก่า
    -- new mail
    -- cmd = 'node /home/creden/work/app/server/capi/send_mail_doc_bla.js "'..v.to..'" "'..v.id..'" "'..v.private_mail..'" "'..v.signer..'" "'..v.owner_name..'"'
    -- os.execute(cmd)


    CI_HEADER_1=''
    CI_BG_1=''
    LOGO_1=''
    ccccc=''
end

local ret = {success= true}
        -- ngx.log(ngx.NOTICE,'line 109',ret)

print(cjson.encode(ret))


