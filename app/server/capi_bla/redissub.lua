#!/usr/bin/env lua
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local ch = arg[1] or 'jobs'

--websocket client
local ws_client = require('websocket.client').copas()
ws_client:on_open(function() 
print('connected')
end)


for msg, abort in rc:pubsub({subscribe = {ch}}) do
   --print(msg.channel, msg.kind, msg.payload)
   if(msg.kind=='message' and msg.channel==ch)then
	 ws:send('jobdone:' .. msg.payload )
   end
end
