#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local body = nil
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local ret = {}
prog.timeout = 1000 * 300
prog.timeout_fatal = false
local conn = mongo()

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    header = ngx.req.get_headers()
else
    body = {Action='CheckReader',SerialNumber='12345789SSSQPA',ReaderName='SCR0001',IPAddress='192.168.1.1'}
end

function GetConnectMongo()
    conn:set_timeout(10000)
    local ok, err = conn:connect('127.0.0.1',27017)
    if ok then
        return true        
    else
        ret = {success = false, msg = err}
        return false
    end
end

function GetTDate()
    local db = conn:new_db_handle("db_orisma")
    local col = db:get_col("tmp_test")
    local rows,err = col:find_one({})
    if (rows) then
        ret = {success = true,tdate=rows.tdate,ostime=tostring(os.time())}
    else
        ret = {success = false}
    end
end

if (GetConnectMongo()) then
    GetTDate()
end
print(cjson.encode(ret))
