#!/usr/bin/env lua
local cjson = require 'cjson'
local bson = require "resty-mongol.bson"
local mongo = require "resty-mongol"
local body = nil
if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
  --  args  =  ngx.req.get_uri_args()
     json = ngx.req.get_body_data()
     args = cjson.decode(json)
else
    args  = {id  = arg[1]} 
end
 
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
ret = {success = false, error_msg = "N/A"}
if not ok then
	ret = {success = false, error_msg = err}
else
	db = conn:new_db_handle("dopa")
	col2 = db:get_col("ekyc")
    s2, err2 = col2:find_one({ txid = args.card_reader_id_tx})
	col = db:get_col("cardreader_ghb")
    s, err = col:find_one({ id = s2.id2})  
	if s then --case I. found email address
		 t = {}
		 t.images_ghb = s2.id2
		 t.firstname = s.name
		 t.lastname = s.lname
		 t.address = s.address
		 t.CID = s.CID
		 t.bp1no = s.bp1no
		 t.sex = s.sex
		 t.Number_card = s.Number_card
		 t.country = s.country
		 t.britdate = s.britdate
		 t.laser_card_id = s.laser_card_id
		 

		ret = {success = true , data=t}
	end
end

print(cjson.encode(ret))
