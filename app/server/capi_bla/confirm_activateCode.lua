local cjson  = require 'cjson'
local redis = require 'redis'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local rc = redis.connect( '127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {token = args[1],otp = args[2]} 
end

-- token = body.token
-- otp = body.otp
-- actvcode=body.activatecode
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if not ok then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("dopa")
    col = db:get_col("esig_user")
    s = col:find_one({adminEmail=body.adminEmail,activeCode=body.activatecode})
    
	ngx.log(ngx.NOTICE, "confirm_activateCode : adminEmail ".. (body.adminEmail or '') .. ', activeCode = ' .. (body.activatecode or '')   )

    if s then
        selector = {adminEmail=body.adminEmail,activeCode=body.activatecode}
        update = {["$set"] = {activeStatus = 3}}
        i, err = col:update(selector, update, 0, 0, true)
        if not i then
            ret = {success = false, error_msg = err}
        else
            ret = {success = true, error_msg = "complete"}
            -- rc:del(key)
        end
    else
        ret = {success = false, error_msg = "not valid code1"}
    end
end

print(cjson.encode(ret))
