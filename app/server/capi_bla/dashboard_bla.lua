#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local bson = require "resty-mongol.bson"
local object_id = require "resty-mongol.object_id"
local session = require "resty.session".open({secret = "4321"})
redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)
local ret = {success = false}
local time_over = false
local data = {}

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body  = {email = arg[1]}
end

function check_queue_document(arr, myusername)
    local flag = true

    for i, v in ipairs(arr.signers) do

        if v.is_group == true and v.user_group then
            for i2, v2 in ipairs(v.user_group) do
                if myusername == v2.email and v.needToSign == false and arr.status == 0 then
                flag = false
                end
            end
        else
            if myusername == v.email and v.needToSign == false and arr.status == 0 then
            flag = false
            end
        end
        if (flag == false) then
            break
        end
        
        if v.is_inspector == true then
            for i3, v3 in ipairs(arr.fields) do
                if v.email == v3.email and v3.toolName == "Approve" and v3.status == 0 then
                    flag = false
                    break
                end
            end
            if (flag == false) then
                break
            end
        end
    end
    flag = true
    return flag
end

function get_table_length(tbl)
    local getN = 0
    for n in pairs(tbl) do 
        getN = getN + 1 
    end
    return getN
end

function match_remove(signers, owner, rm)
    local status = false

    if signers then
        for i, v in ipairs(signers) do
            if v.is_group == true then
                for i2, v2 in ipairs(v.user_group) do
                    if v2.is_remove == true and v2.email == body.email then
                        status = true
                        break
                    end
                end
            else
                if v.is_remove == true and v.email == body.email then
                    status = true
                    break
                end
            end
    
        end
    end

    if owner == body.email and rm == true then
        status = true
    end

    return status
end

function chk_signer(signers)
    local user = false
    if signers then
      for i, v in ipairs(signers) do
        if v.is_group == true then
          for i2, v2 in ipairs(v.user_group) do
            if v2.email == body.email then
              user = v2
            end
          end
        else
          if v.email == body.email then
            user = v
          end
        end
      end
    end
    return user
end

function timeout_show_trash(arr)
    local current_time = bson.get_utc_date(ngx.now() * 1000).v
    local exp_time = ""
    local user = chk_signer(arr.signers)
    local show = true
    if user and user.is_remove == true then
        exp_time = user.time_remove + (24*60*60*1000)
        if current_time > exp_time then
            show = false
        end
    elseif arr.owner_is_remove == true then
        exp_time = arr.owner_time_remove + (24*60*60*1000)
        if current_time > exp_time then
            show = false
        end
    end
    return show
end

function match_myorder(signers) 
    local order = 0
    for i, v in ipairs(signers) do
      if v.is_group == true then
        for i2, v2 in ipairs(v.user_group) do
          if v2.email == body.email then
            order = v.order_set
            break
          end
        end
      else
        if v.email == body.email then
          order = v.order_set
          break
        end
      end
    end
    return tostring(order)
end

function count(cur)
	local ret = 0
	for i,v in cur:pairs() do
	    ret = ret + 1
	end
	return ret
end

function get_late_data(email, startDate, endDate)
    	
	local chk_dash = rc:get('have_dash_date' .. body.email)       	
		if(chk_dash and not body.startDate and not body.endDate) then
			local dash_start_date = rc:get('set_date_period_start' .. body.email)
			local dash_end_date = rc:get('set_date_period_end' .. body.email)
			local start_date = os.time({year=tonumber(dash_start_date:sub(5)), month=tonumber(dash_start_date:sub(3,4)), day=tonumber(dash_start_date:sub(1,2)), hour=0, sec=0})
			ngx.log(ngx.ERR, '-------has dash--------' .. cjson.encode(dash_start_date))
			
			local end_date = os.time({year=tonumber(dash_end_date:sub(5)), month=tonumber(dash_end_date:sub(3,4)), day=tonumber(dash_end_date:sub(1,2)), hour=16,min=59, sec=59})
			return { start_date = start_date, end_date = end_date }
		
		elseif(chk_dash and body.startDate and body.endDate) then	
			local tmp_date1 = body.startDate
			local tmp_date2 = body.endDate
			ngx.log(ngx.ERR, '-------has dash and body---------' .. cjson.encode(tmp_date1))
			local start_date = os.time({year=tonumber(tmp_date1:sub(5)), month=tonumber(tmp_date1:sub(3,4)), day=tonumber(tmp_date1:sub(1,2)), hour=0,min=0, sec=0})
						
			local end_date = os.time({year=tonumber(tmp_date2:sub(5)), month=tonumber(tmp_date2:sub(3,4)), day=tonumber(tmp_date2:sub(1,2)), hour=16,min=59, sec=59})
		
			rc:set('set_date_period_start' .. body.email, body.startDate )
			rc:set('set_date_period_end' .. body.email, body.endDate)

			return { start_date = start_date, end_date = end_date }
		elseif(not chk_dash and body.startDate and body.endDate) then
			local tmp_date1 = body.startDate
			local tmp_date2 = body.endDate
			ngx.log(ngx.ERR, '-------has body--------' .. cjson.encode(tmp_date1))
			local start_date = os.time({year=tonumber(tmp_date1:sub(5)), month=tonumber(tmp_date1:sub(3,4)), day=tonumber(tmp_date1:sub(1,2)), hour=0,min=0, sec=0})
			local end_date = os.time({year=tonumber(tmp_date2:sub(5)), month=tonumber(tmp_date2:sub(3,4)), day=tonumber(tmp_date2:sub(1,2)), hour=16,min=59, sec=59})
		
			rc:set('set_date_period_start' .. body.email, body.startDate )
			rc:set('set_date_period_end' .. body.email, body.endDate)
			rc:set('have_dash_date' .. body.email,  body.email)
			ngx.log(ngx.ERR, '-------chk format date--------' .. start_date)
    		
			return { start_date = start_date, end_date = end_date }
		else
			--local start_date = os.time() - (30 * 24 * 60 * 60)
    			--local end_date = os.time()
    		
			local current_time = os.time()
			local current_year = tonumber(os.date("%Y", current_time))
			local current_month = tonumber(os.date("%m", current_time))
			--local current_day = tonumber(os.date("%d", current_time))

			--local start_date = os.time({year=current_year, month=current_month - 1, day=1, hour=0, minute=0, second=0})
			--local end_date = os.time({year=current_year, month=current_month, day=1, hour=0, minute=0, second=0})
			--local end_date = os.time({year = current_year, month = current_month, day = current_day - 1, hour = 23, minute = 59, second = 59})
			local end_date = os.time()
			local days_in_month = os.date("*t", end_date).day
			ngx.log(ngx.ERR, '-------chk date filter--------' .. cjson.encode(days_in_month))
			local start_date = os.time({year=current_year, month=current_month - 1, day=days_in_month, hour=0, minute=0, second=0})
    		
			ngx.log(ngx.ERR, '-------defaut filter--------')
			return { start_date = start_date, end_date = end_date }
		end
end

    local get_late_date = get_late_data()
    local start_range =  get_late_date.start_date * 1000
    local end_range  = get_late_date.end_date * 1000

function get_doc_count(type, email, doc_ids)
    
    --local get_late_date = get_late_data()
    local start_range2 =  bson.get_utc_date(get_late_date.start_date * 1000) 
    local end_range2  = bson.get_utc_date(get_late_date.end_date * 1000)
    local time_over = get_late_date.time_over
    local date_range = {['$gte'] = start_range2, ['$lte'] = end_range2}
    
    local query = {}
    local query2 = {}
    local query3 = {}
    if type == "owner" then
        query = { owner = email, is_template={['$ne']=true}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
    end

    if type == "all" then
        query = { owner = body.email, is_template={['$ne']=true}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
        query2 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={email = body.email}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
        query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
    end
    --ngx.log(ngx.NOTICE,'-------chk_data_list_doc------' .. cjson.encode(query2))

    if (type == "signer") then
        query = { owner=body.email, signers={['$elemMatch']={email = body.email}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
        query2 =  { owner ={['$ne']=body.email}, signers={['$elemMatch']={email = body.email}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
        query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range}
    end

    if (type == "not_sign") then
        query = { owner=body.email, signers={['$elemMatch']={email = body.email}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids} , createdDtm =  date_range}
        query2 =  { owner ={['$ne']=body.email}, signers={['$elemMatch']={email = body.email}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids} , createdDtm =  date_range}
        query3 = { owner ={['$ne']=body.email}, signers={['$elemMatch']={ is_group = true, user_group = {['$elemMatch']={ email = body.email }}}}, status = {['$ne']= 3}, id = {['$nin']= doc_ids}, createdDtm =  date_range }
    end

    local s2 = {} 
    --local s  = col:find(query) --tawan
    local s  = col:find(query, {signers=1, owner=1, is_remove=1,fields=1, id=1, use_signing_order=1, current_order=1,status=1})
    for k, v in s:pairs() do 
    --ngx.log(ngx.NOTICE,'-------chk_doc_list------' .. cjson.encode(v))
        if not match_remove(v.signers, v.owner, v.is_remove) then
            table.insert(s2, v) 
        end
    end

    if (true and next(query2)) then
        local q2 = col:find(query2, {signers=1, owner=1, is_remove=1,fields=1, id=1, use_signing_order=1, current_order=1,status=1}  )
        for k2, v2 in q2:pairs() do 
            if not match_remove(v2.signers, v2.owner, v2.is_remove) then
            table.insert(s2, v2) 
            end
        end
    end

    if (true and next(query3)) then
        local q3 = col:find(query3, {signers=1, owner=1, is_remove=1,fields=1, id=1, use_signing_order=1, current_order=1, status=1} )
        for k3, v3 in q3:pairs() do 
            if not match_remove(v3.signers, v3.owner, v3.is_remove) then
            table.insert(s2, v3) 
            end
        end
    end

    tmp = {}

    for k, v in ipairs(s2) do
        v._id = tostring(v._id)
        local flag = check_queue_document(v, session.data.username)
        if session.data.username == v.owner or flag then            
            table.insert(tmp,v)
        end
    end

    local tmp2 = {}
    local myorder = 0

    for i, v in ipairs(tmp) do
        local exp = timeout_show_trash(v)

        if exp == true then
	ngx.log(ngx.INFO, 'CHK_FILTER CONDITION #' .. tostring(v.id) .. ' SIGNING_ORDER ' .. tostring(v.use_signing_order) ..' STATUS ' .. tostring(v.status))
           --if (v.use_signing_order == true and v.status == 0) or (v.use_signing_order == true and v.status == 2) then
	   if v.use_signing_order == true and (v.status == 0 or v.status == 2) then
                myorder = match_myorder(v.signers)
                if myorder == "" then 
                    myorder = "0" 
                end
		ngx.log(ngx.INFO, 'CHK_FILTER 2 ORDER#' .. v.id .. ' order ' .. v.current_order ..' vs ' .. myorder)
                if tonumber(v.current_order) >= tonumber(myorder) then
		    ngx.log(ngx.INFO, 'CHK_FILTER = TRUE ' .. v.id)
                    table.insert(tmp2, v)
                end
            else
		ngx.log(ngx.INFO, 'CHK_FILTER = FALSE ' .. v.id)
                table.insert(tmp2, v)
            end
        end
    end

    --if(type == "not_sign" or type=='all')then
    if(type == "not_sign")then
        local docs2 = {}
        for i, v in ipairs(tmp2) do
            for i2, v2 in ipairs(v.fields) do
                if(type=='not_sign' and (v2.status == 0 and v2.email == email))then
                    table.insert(docs2, v)
		    --ngx.log(ngx.ERR, '----check list_doc condition----A01')
		    
                   break 
                end

                if(type=='all' and v2.status == 0 and v2.email == email)then
                    table.insert(docs2, v)
		    --ngx.log(ngx.ERR, '----check list_doc condition-----A02')
                    break
                end
            end
        end
	for _,n in ipairs(tmp2) do
		ngx.log(ngx.ERR, 'check doc list' .. cjson.encode(n.id))
	end
        tmp2 = docs2
	for _,t in ipairs(tmp2) do
		ngx.log(ngx.ERR, 'check doc list in not_sign' .. cjson.encode(t.id))
	end
    end

    --ngx.log(ngx.ERR, 'check doc list' .. cjson.encode(tmp2))
    return get_table_length(tmp2)
end

--prevent email param
if(not session.data.username ) then
    print(cjson.encode({success = false, error_msg = "session expired"}))
    ngx.exit(ngx.HTTP_OK)
else
    if(body.email~=session.data.username) then
        ngx.log(ngx.NOTICE,"DETECT FRUAD user " .. tostring(session.data.username) ..'try to impersonate ' .. tostring(body.email) )
        body.email = session.data.username
    end
end	
--prevent email param

conn = mongo()
conn:set_timeout(10000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    db = conn:new_db_handle("edoc")
	col = db:get_col("edoc_list")
    local col_folder2 = db:get_col('folders2')
    local doc = ""
    doc = col_folder2:find({ owner_folder = body.email },{signers = 1, owner = 1, is_remove = 1})
    local doc_ids = {}

    if doc then
        for k, v in doc:pairs() do
        v._id = tostring(v._id)
        for k2, v2 in ipairs(v.list_doc) do
            id_doc_folder = col:find_one({ _id = object_id.new(convertHexStringToNormal(v2)) })
            if id_doc_folder.id then
            table.insert(doc_ids, tostring(id_doc_folder.id))
            end
        end
        end

        if #doc_ids == 0 then
        doc_ids = {"999999999"}
        end
    end
    local set = 1
    local key_all = body.email .. ":number_document:all" 
    local num_all = rc:get(key_all)
    local key_not_sign = body.email .. ":number_document:not_sign" 
    local num_not_sign = rc:get(key_not_sign)
    if ((num_all == nil or tonumber(num_all) < 0) and (num_not_sign == nil or tonumber(num_not_sign) < 0)) then
        
	num_all = get_doc_count("all", body.email, doc_ids) or 0
        num_not_sign = get_doc_count("not_sign", body.email, doc_ids) or 0

	--ngx.log(ngx.ERR, '------check data in num_all-------' .. cjson.encode(num_all))
	--ngx.log(ngx.ERR, '------check data in num_not_sign-------' .. cjson.encode(num_not_sign))

        rc:set(key_not_sign , tostring(num_not_sign ))
        rc:set(key_all , tostring(num_all))
        rc:expire(key_all , 1)
        rc:expire(key_not_sign , 1)
    else 
        num_all = tonumber(num_all)
	num_not_sign = tonumber(num_not_sign)
    end

    local key_owner = body.email .. ":number_document:owner" 
    local num_owner = rc:get(key_owner)
    if (num_owner == nil or tonumber(num_owner) < 0) then
        num_owner = get_doc_count("owner", body.email, doc_ids) or 0
        rc:set(key_owner , tostring(num_owner))
        rc:expire(key_owner , 1)
    else 
        num_owner = tonumber(num_owner)
    end

    local key_signer = body.email .. ":number_document:signer" 
    local num_signer = rc:get(key_signer)
    if (num_signer == nil or tonumber(num_signer) < 0) then
        num_signer = get_doc_count("signer", body.email, doc_ids) or 0
        rc:set(key_signer , tostring(num_signer))
        rc:expire(key_signer , 1)
    else 
        num_signer = tonumber(num_signer)
    end

    --local key_not_sign = body.email .. ":number_document:not_sign" 
    --local num_not_sign = rc:get(key_not_sign)
    --if (num_not_sign == nil or tonumber(num_not_sign) < 0) then
    --    num_not_sign = get_doc_count("not_sign", body.email, doc_ids) or 0
    --    rc:set(key_not_sign , tostring(num_not_sign))
    --    rc:expire(key_not_sign , 1)
    --else 
    --    num_not_sign = tonumber(num_not_sign)
    --end

    ret.success = true
    ret.key = key_all
    ret.documents_all = num_all
    ret.documents_owner = num_owner
    ret.documents_signer = num_signer
    ret.documents_not_sign = num_not_sign
    ret.startDate = start_range
    ret.endDate = end_range
end

print(cjson.encode(ret))
