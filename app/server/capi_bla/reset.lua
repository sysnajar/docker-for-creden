#! /usr/bin/env lua
local cjson = require 'cjson'
local redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)

local t = rc:lrange('save_detect_face', 0, -1)
for i,v in ipairs(t) do
    if(i==6)then break end
	local r = cjson.decode(v)
	local ext = (i<5) and '.jpeg' or '.png'
	r.f1 = 'f1_'.. tostring(i) .. ext
	r.f2 = 'f2_'.. tostring(i) .. ext

    rc:lset('save_detect_face', i-1, cjson.encode(r))
end
