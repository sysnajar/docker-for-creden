#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local body = nil


if(ngx)then  
   ngx.req.read_body()
   print = ngx.say
   body = ngx.req.get_body_data()
   --body  =  ngx.req.get_post_args()
   --local tbx = require "pl.tablex"
   --body = tbx.union(ngx.req.get_post_args() or {error = "no data"},ngx.req.get_uri_args())
   --body =  ngx.req.get_uri_args()
   body = cjson.decode(body)
else
   body  = {email=arg[1]}
   --body  = {email='nutp10.1@gmail.com'} 
end

--[[--conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
db = conn:new_db_handle("test")
col = db:get_col("user")
s = col:find_one({email=body.email, activated=1})
]]

default_score = 250 
redis = require 'redis'
local rc = redis.connect('127.0.0.1', 6379)
local raw_score = rc:get('SCORE:' .. body.email)
score = (tonumber(raw_score)) or default_score

ret = {success = true, score = score}
print(cjson.encode(ret))
