#!/usr/bin/env lua
local cjson = require 'cjson'
local mongo = require "resty-mongol"
local body = nil
local common = require 'common'

 redis = require 'redis'
 rc = redis.connect('127.0.0.1', 6379)

if(ngx)then
	ngx.save_log({desc = "kyc_logout"})  
    ngx.req.read_body()
    print = ngx.say
    --body  = ngx.req.get_post_args()
    --body = cjson.decode(ngx.req.get_body_data())
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
else
   body   = {} 
end

   local ret = {success = true, error_msg = "N/A"} 
   local session = require "resty.session".open({secret = "4321"})
   local redis_key = 'companyof.' .. session.data.username
   rc:del(redis_key)
   rc:del('set_date_period_start' .. body.email)
   rc:del('set_date_period_end' .. body.email)
   rc:del('have_dash_date' .. body.email)
   local token = common.get_cookie('login_token')
   	if(token)then
           local ctx   = ctx  or common.mongo_ctx()
           local col = ctx.col['dopa.login_token']
            
       	   col:update({token = token}, {['$set']={valid = false, note = 'logout'}})
     end

   session:destroy()

   print(cjson.encode(ret))
   
