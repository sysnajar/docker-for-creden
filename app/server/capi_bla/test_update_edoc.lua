#!/usr/bin/env lua
local cjson = require 'cjson.safe'
local mongo = require "resty-mongol"
local body = nil
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local object_id = require "resty-mongol.object_id"
prog.timeout_fatal = false

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

if(ngx)then  
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    ngx.log(ngx.NOTICE,body)
    body = cjson.decode(body)
else
    body  = {} 
end

json = [[{"fields":[{"x":90,"y":80,"id":1530098593139,"page":0,"n":1,"status":1,"email":"kitchinvuttinunt@gmail.com","_color":"grey","type":"gen"}],"id":"1530098595408"}]]
body = cjson.decode(json)
conn = mongo()
conn:set_timeout(1000)
ok, err = conn:connect('127.0.0.1',27017)
if err then
    ret = {success = false, error_msg = err}
else
    db = conn:new_db_handle("edoc")
    col = db:get_col("edoc_list")
    s, err = col:find_one({id=body.id})
    s._id = nil
    

    update = {["$set"] = {fields=body.fields, status=body.status}}
    u, err = col:update({id=body.id}, update, 0, 0, true)
    
    if err then
        ret = {success = false, error_msg = err}
    else
        ret = {success = true, error_msg = err}
    end
end
print(cjson.encode(ret))

