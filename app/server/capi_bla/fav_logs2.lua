#!/usr/bin/env lua
local cjson = require 'cjson'
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local mongo = require "resty-mongol"
local body = nil
prog.timeout_fatal = false
local ret = {success = false, error_msg = ""}
if(ngx)then
    ngx.req.read_body()
    print = ngx.say
    body = ngx.req.get_body_data()
    body = cjson.decode(body)
    -- =json
else
     body = {}
end


conn = mongo()
conn:set_timeout(1000)
local ok,  err = conn:connect('127.0.0.1',27017)
if err then
    ret.error_msg = err
else
    -- ต่อdbได้

    local db_dopa = conn:new_db_handle("dopa")

    local col_history = db_dopa:get_col("fav_logs_company")
    -- เข้าหรือสร้าง
    local history =  col_history:find_one({company_id=body.company_id,email=body.email,owner=body.owner,name=body.name,lname=body.lname,groupfav=body.groupfav})
    
    -- เซ็คว่าข้อมูลที่ insert ที่ส่งไปจะไม่ซ้ำ
    if not history then
        --insert
        body.fav=true
        n, err_n = col_history:insert({body})

        if not n then
            ret.error_msg = err_n
        else
            ret.fav=true
            ret.success = true
            ret.txid = body.txid
            ret.usr = body.username

        end
    else
        -- update
        ret.success = true
        local f =true
        ret.fav=true
        if body.fav == true then
            f=false
            ret.fav=false
        end
        
         selector = {email=body.email,company_id=body.company_id}
         update = {["$set"] = {fav = f}}
        n, err = col_history:update(selector, update, 0, 0, true)
        
        ngx.log(ngx.NOTICE,' ref====='..tostring(body.fav))
        
        -- เซ็คว่าข้อมูลที่ insert 
    end
end
print(cjson.encode(ret))