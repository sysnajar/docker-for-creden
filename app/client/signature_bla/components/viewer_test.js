Dropzone.autoDiscover = false;

// field functions --------------------------------------------------------------
var Concensus = {}
function register_concensus(concensus_id, f) {
  //console.log('register_concensus ' + concensus_id + ', field = ' + f.id)

  var arr = Concensus[concensus_id] || []
  Concensus[concensus_id] = arr
  arr.push(f)

  //console.log('register_concensus#' + concensus_id + '\nlen=' + arr.length)
}

function register_sign_concensus(concensus_id, f) {
  var preceed = f.email == getUsername()
  //console.log('register_sign_concensus ' + concensus_id + ', field = ' + f.id + ' email = ' + f.email + ' preceed >> ' + preceed)

  if (!preceed) { return; } //exit

  var arr = Concensus[concensus_id] || []
  arr.forEach(f2 => {
    if (f2.id != f.id) {
      var old = f2.email
      if (!f2.note_of && !f2.person.delegates_sign) {
        f2.email = "" //unset email 
      }


      //console.log('unset email of#' + f2.id + "from old email " + old + " to empty")
    }
  })

}

function chooseSignature_silent(type, field_id){
    
  $.ajax({
    type: "POST",
    url: "../capi/choose_signature",
    data: JSON.stringify({"username":getUsername(),"code":getUsername(),"sign_type":type, "field_id":field_id}),
    dataType: "json",
    success: function(data) { 
      //alert('chooseSignature_silent is called '+ type + ':'+ field_id)
      // alert('111111')
    },
    error: function(request, status, error){
      //alert("|"+ status + ' |' +error);
    }
  });
}



function convert_group_to_mine(f) {
  //alert("check field#"+f.id+', f.is_group = '+ f.is_group)


  if (f.person && f.person.is_group &&
    f.person.user_group.find(p => { return p.email == getUsername() })) {
    f.email = getUsername()
    var concensus_id = (f.person && f.person.concensus_id) ? f.person.concensus_id : false
    if (concensus_id) {
      register_concensus(concensus_id, f)
    }

    //console.log("  convert_group_to_mine >>>> " + f.person.name + " >>> " + f.email)
  }

}

// --------------------------------------------------------------

$(document).ready(function () {
  //setAlert(true)
  setAlert(false)

  app = init_vue()

});

var pages = []
function addImageQueue(i, img) {
  /* todo add this to rendering quque */
  app.initPage(i)
}

function checkDropdownField
  (ele) {
  var val = $(ele).val()
  if (val == '') {
    $(ele).addClass('field-missing')
  }
  else {
    $(ele).removeClass('field-missing')
  }
}

function checkMissingField(ele) {
  if (ele.value == '') {
    $(ele).addClass('field-missing')
  }
  else {
    $(ele).removeClass('field-missing')
  }
}

function saveApprove() {
  app.messageApprove()
}

function clickField(fieldId) {
  var f = app.sign_obj.fields.find(f => f.id == fieldId)
  db('clickField', f)

  f.doc_field = f
  window.oImg = f
  app.signDlg(f, f)
}
function getDataFieldByID(fieldId) {
  var fieldData = app.sign_obj.fields.find(f => f.id == fieldId)
  db('clickField', fieldData)

  var doc_field = fieldData.doc_field
  window.oImg = fieldData
}


function fieldDiv(f, box) {
  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = 250 * zoomLevel
  var h = 80 * zoomLevel

  if (f.toolName == 'Initial') { w = w * 0.66 } // Initial width is 2/3 of signature
  //console.log('f: ', f);

  var onclickText = 'onclick'
  var enabled = (f.status == 0 && app.isMyfield(f) && app.isFieldIncurrentOrder(f))
  var color = enabled ? 'yellow' : 'grey'
  var concensus_id = (f.person && f.person.concensus_id) ? f.person.concensus_id : ''
  var tpl = null

  if (!enabled) { onclickText = '_onclick' }


  var langChoose = localStorage.getItem('lang_choose');
  var default_tpl = `<div id="field${f.id}" style="width:${w}px; height:${h}px;background:${color};opacity:0.7;z-index:9;
                      position:absolute;top:${y}px;left:${x}px" class="text-wrap text-break"
                      ${onclickText}="clickField('${f.id}')" data-concensus-id="${concensus_id}">
                      ${f.toolName}</div>`;


  if (f.toolName == "Approve") {

    var radioButtonApprove = `<div class="form-check form-check-inline">
                        <input class="form-check-input inlineRadioApprove" type="radio" name="inlineRadioApprove" id="inlineRadioApprove${f.id}" value="true" >
                        <label class="form-check-label" for="inlineRadioApprove${f.id}">${app.lang_page.approve[app.lang_choose]}</label>
                      </div>
                      <div class="form-check form-check-inline">
                        <input class="form-check-input inlineRadioApprove" type="radio" name="inlineRadioApprove" id="inlineRadioDeApprove${f.id}" value="false" >
                        <label class="form-check-label" for="inlineRadioDeApprove${f.id}">${app.lang_page.Disapprove[app.lang_choose]}</label>
                      </div>`;
    default_tpl = `<div id="field${f.id}" style="width:${w}px; height:${h}px;background:${color};" class="text-wrap text-break position-relative"
                      ${onclickText}="clickField('${f.id}')" data-concensus-id="${concensus_id}">
                      <p class="text-center pt-4"><i class="far fa-check-circle"></i> ${app.lang_page.confirm_approve[app.lang_choose]}</p>
                      </div>`;

    default_tpl = $('<div>').append(default_tpl).prepend(radioButtonApprove)
    default_tpl.css({
      top: y + 'px',
      left: x + 'px',
      'z-index': 9,
      position: 'absolute'
    })

    default_tpl.find('.inlineRadioApprove').change(function (e) {
      var valueApprove = $(this).val();
      var textChange = valueApprove == 'true'
        ? '<i class="far fa-check-circle"></i> ' + app.lang_page.approve[app.lang_choose]
        : '<i class="far fa-times-circle"></i> ' + app.lang_page.Disapprove[app.lang_choose];
      var $FieldID = default_tpl.find(`#field${f.id}`)
      //console.log('FieldID: ', $FieldID.get(0));
      $FieldID.get(0).innerHTML = `<p class="position-absolute top-50 start-50 translate-middle">${textChange}<p>`;

      getDataFieldByID(f.id)
      app.updateApprove(valueApprove);
    });
  }
  // Text , Dropdown, Checkbox
  if (f.toolName == 'Date' || f.toolName == 'Name' || f.toolName == 'Email') { tpl = singleTextDiv(f, box); }
  if (f.toolName == 'Text') { tpl = textareaDiv(f, box); /*singleTextDiv(f,box)*/; }
  if (f.toolName == 'Checkbox') { tpl = checkboxDiv(f, box); }
  if (f.toolName == 'Dropdown') { tpl = dropdownDiv(f, box); }
  if (f.toolName == 'Radio') { tpl = radioDiv(f, box); }
  if (f.toolName == 'Radio2') { tpl = radioDiv2(f, box); }
  if (f.toolName == 'Qrcode') { tpl = qrDiv(f, box); }
  //if(f.toolName == 'Qrcode') {tpl = approveDiv(f,box);}                   

  if (tpl == null) { tpl = default_tpl }
  return tpl
}

function getField(id) {
  return app.sign_obj.fields.find(f => f.id == id)
}

function textareaDiv(f, box) {
  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = 150 * zoomLevel
  var h = 50 * zoomLevel
  var onclickText = 'onclick'

  var enabled = (f.status == 0 && app.isMyfield(f))

  var color = enabled ? 'yellow' : 'grey'
  var disabledText = (enabled) ? "" : "disabled"

  f.toolData=""   

  var missingClass = (enabled && f.toolData == '') ? "form-control" : ""
  // field-missing
  var div = `<div class="text-tab" id="input-div${f.id}"  data-type="Text"  
       style="left:${x}px; top:${y}px;position:absolute;z-index:9;">
       <textarea id="input${f.id}" ${disabledText} class="${missingClass} form-control"  
       style="width: 200px;  height: 42px;" onchange="checkMissingField(this);getField('${f.id}').toolData=this.value;"
       maxlength="255" oninput='this.style.height = "";this.style.height = this.scrollHeight + "px"'
       >${f.toolData}</textarea>
     </div>`

  return div
}

function radioDiv(f, box) {
  // var x = (f.xp * box.width)/100
  // var y = (f.yp * box.height)/100
  var zoomLevel = app.zoomLevel
  var w = 150 * zoomLevel
  var h = 50 * zoomLevel

  var enabled = (f.status == 0 && app.isMyfield(f))
  var missingClass = (enabled && f.toolData == '') ? "form-control" : ""
  // field-missing

  var color = enabled ? 'yellow' : 'grey'
  var disabledText = (enabled) ? "" : "disabled"

  //console.log('radioDiv>>', f)
  radioHTML = ""
  f.child.forEach(choice => {
    //console.log(choice)
    // optionsHTML += `<option value="${txt}">${txt}</option>`
    var x = (choice.xp * box.width) / 100
    var y = (choice.yp * box.height) / 100
    radioHTML += `<div name="div${f.id}" class = "bg-red" style="left:${x}px; top:${y}px;position:absolute;"><span title="id ${f.id}-${choice.id}"><input type="radio" onchange='getField("${f.id}").toolData = ${choice.id}; radioNotChoose("div${f.id}");' id="${choice.id}" name="${f.id}" value="${choice.id}" style=""></div>`
  })
  //console.log(radioHTML)
  return radioHTML
}

function radioDiv2(f, box) {
  // var x = (f.xp * box.width)/100
  // var y = (f.yp * box.height)/100
  var zoomLevel = app.zoomLevel
  var w = 150 * zoomLevel
  var h = 50 * zoomevel

  var enabled = (f.status == 0 && app.isMyfield(f))
  var missingClass = (enabled && f.toolData == '') ? "form-control" : ""
  // field-missing

  var color = enabled ? 'yellow' : 'grey'
  var disabledText = (enabled) ? "" : "disabled"


  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  radioHTML = ""
  radioHTML += `<div name="div${f.radioGroupName}" class = "bg-red" style="left:${x}px; top:${y}px;position:absolute;"><span title="id ${f.radioGroupName}-${f.id}"><input type="radio" onchange='radioNotChoose("div${f.radioGroupName}");' id="${f.id}" name="${f.radioGroupName}" value="${f.id}" style=""></div>`
  //console.log(radioHTML)
  return radioHTML
}

function radioNotChoose(name) {
  var items = document.getElementsByName(name);
  //console.log(items)
  for (var i = 0; i < items.length; i++)
    items[i].classList.remove("bg-red");
}

function dropdownDiv(f, box) {
  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = 150 * zoomLevel
  var h = 50 * zoomLevel
  var onclickText = 'onclick'


  var enabled = (f.status == 0 && app.isMyfield(f))
  var missingClass = (enabled && f.toolData == '') ? "form-control" : ""
  // field-missing

  var color = enabled ? 'yellow' : 'grey'
  var disabledText = (enabled) ? "" : "disabled"


  //f.toolOptions = 'eark,got,auddy,poae,tom,pat'
  var optionsHTML = '<option value="">----------</option>'
  f.toolOptions.split(",").forEach(txt => {
    optionsHTML += `<option value="${txt}">${txt}</option>`
  })


  var div = `<div class="text-tab" id="input-div${f.id}"  data-type="${f.toolName}"  
       style="left:${x}px; top:${y}px;position:absolute;">
       <select id="input${f.id}" ${disabledText} class="${missingClass} form-control" 
       onchange='checkDropdownField(this); getField("${f.id}").toolData = this[this.selectedIndex].value'>${optionsHTML}
       </select></div>`

  return div
}

function checkboxDiv(f, box) {
  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = 150 * zoomLevel
  var h = 50 * zoomLevel

  var enabled = (f.status == 0 && app.isMyfield(f))
  //enabled = false

  //f.toolData = 'X'
  var color = enabled ? 'yellow' : 'grey'
  var disabledText = (enabled) ? "" : "disabled"
  var checkedText = (f.toolData == 'X') ? "checked" : ""
  var valueText = (f.toolData == 'X') ? 'X' : ''

  var div = `<div class="text-tab" id="input-div${f.id}"  data-type="${f.toolName}"  
       style="left:${x}px; top:${y}px;position:absolute;">
       <input type="checkbox" id="input${f.id}" ${disabledText} value="${valueText}"
       ${checkedText} onchange='(this.value==(this.checked)?"X":"Y");var f = getField("${f.id}");f.toolData="X";getField("${f.id}");'></div>`

  return div
}

function singleTextDiv(f, box) {
  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = 150 * zoomLevel
  var h = 50 * zoomLevel
  var onclickText = 'onclick'

  var enabled = (f.status == 0 && app.isMyfield(f))
  //enabled = false

  if (enabled && f.toolName == 'Date') { f.toolData = app.dtm; enabled = false; } //auto assign date and disable it

  if (enabled && f.toolName == 'Email') { f.toolData = f.email; } //auto assign date and disable it

  if (enabled && f.toolName == 'Name') { f.toolData = app.myFirstname; } //auto assign date and disable it

  var color = enabled ? 'yellow' : 'grey'
  var disabledText = (enabled) ? "" : "disabled"




  var missingClass = (enabled && f.toolData == '') ? "field-missing" : ""

  var div = `<div class="text-tab" id="input-div${f.id}"  data-type="Text"  
       style="left:${x}px; top:${y}px;position:absolute;">
       <input type="text" id="input${f.id}" ${disabledText} 
       value="${f.toolData}" class="${missingClass} form-control" onchange='checkMissingField(this)'
       style="font-family: monospace; letter-spacing: 0.14px;
       color: rgb(0, 0, 0); text-align: left; font-size: 12.16px !important;
       width: ${w}px; height: 14px;">
     </div>`

  return div
}
function signatureDiv(f, box) {
  var pageIndex = f.page
  var img = document.getElementById("img" + pageIndex)
  var box = img.getBoundingClientRect()
  var url = '../edoc/app-assets/images/signature/' + oImg.doc_field.email + oImg.doc_field.id + '_' + oImg.doc_field.type + '.png'

  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = (box.width * 450) / 1920

  if (f.type == 'Initial') { (box.width * 450) / 1920 } //initial is narrower than signature


  w = w * zoomLevel
  //var h = 50  * zoomLevel //auto compute


  var default_tpl = `<img src="${url}" style="width:${w}px; 
                      position:absolute;top:${y}px;left:${x}px">`;


  var tpl = default_tpl
  return tpl
}

function qrDiv(f, box) {
  var pageIndex = f.page
  var img = document.getElementById("img" + pageIndex)
  var box = img.getBoundingClientRect()
  var url = '../face/images/qrcode/qrcode_' + f.email + '_' + app.sign_obj.id + '.png'

  var x = (f.xp * box.width) / 100
  var y = (f.yp * box.height) / 100
  var zoomLevel = app.zoomLevel
  var w = (box.width * 100) / 1920
  w = w * zoomLevel
  //var h = 50  * zoomLevel //auto compute


  var default_tpl = `<img src="${url}" style="width:${w}px; position:absolute;z-index:9;top:${y}px;left:${x}px">`;


  var tpl = default_tpl
  return tpl
}

function click_tab(show) {
  if (show) $(".full-name-sign").show();
  else $(".full-name-sign").hide();
}

moveOnMax = function (field, nextFieldID, previousFieldID) {
  if (event.keyCode == "8" || event.keyCode == "46") {
    if (previousFieldID != "N/A") {
      document.getElementById(previousFieldID).focus();
      return false
    }
  }
  //console.log('chk pin');
  if (field.value.length == 1) {
    if (nextFieldID == "N/A") {
      //console.log('chk done >>>....' + document.getElementById('1').value + " " + document.getElementById('2').value + " " + document.getElementById('3').value + " " + document.getElementById('4').value + " " + document.getElementById('5').value + " " + document.getElementById('6').value);
      app.use_cert2();
      return false
    }
    document.getElementById(nextFieldID).focus();
  }


}
//----------------------------------

function init_vue() {
  return new Vue({
    el: '#app',
    data: {
      x: "got",

      //old fields
      lang_choose: localStorage.getItem('lang_choose') || 'en',
      lang_page: lang_page,
      edit_mode: false,
      edit_items: [],
      mode_sign: "2",
      check_choose_signature: false,
      count_field: 0,

      pin_number: "",
      pin_number1: "",
      id_deal: "",
      status_creden: "",
      status_symphony: "",

      zoomLevel: 1,
      imageFiles: [],
      sign_obj: { id: "", imageFiles: [] },
      cert_pin: "",
      save_list: [],
      dtm: "",
      show_initia_draw: true,
      show_initia_gen: true,
      initia_draw: "../edoc/app-assets/images/signature/" + getUsername() + "_draw_initial.png",
      initia_gen: "../edoc/app-assets/images/signature/" + getUsername() + "_gen_initial.png",
      select_initia_sing: '',
      sign_all_initia: false,
      sign_name_initial: '',
      stk: gup('stampkey'),
      p1: '',
      p2: '',
      p3: '',
      p4: '',
      p5: '',
      p6: '',
      status_pin: false,
      myFirstname: '', // default value used in toolName = 'Name'
      btn_save_show: true,
      check_screener_approve: false,
      pwdCount: 0,
      no_sign_field: [],

      // approve all
      btn_approve: "",
      note_approve: "",
      password_secret_check: true,
      level: "",
      pwdCount: 0,
      count_input_pass: 0,
      password_secret: "",
      chk_pass: "",

    },
    mounted: function () {
      var app = this
      var doc_id = gup("id")

      // $('#profile-tab').click()
      //this.showSignatureTab(1)
      window.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        penColor: 'rgb(0, 0, 0)'
      });
      //initai 
      window.signaturePadInitial = new SignaturePad(document.getElementById('signature-pad-initial'), {
        backgroundColor: 'rgba(255, 255, 255, 0)',
        penColor: 'rgb(0, 0, 0)'
      });



      window.sig_user = getUsername()
      isEng = !(app.lang_choose == 'th')  //!(gup('lang')=='th')
      //console.log('isEng = ' + isEng)
      app.doc_id = doc_id


      //$('.finish-btn').html(app.lang_page.finish[app.lang_choose]) 
      checkSignature()
      var password = gup('stampkey')
      if (password)
        app.getSignObj(true)
      else
        app.getUser()

      app.upload_attachment();





    },
    methods: {
      please_choose: function () {
        var txt = (localStorage.lang_choose == "th") ? "กรุณาเลือกลายเซ็น " : "Please choose signature"
        console.error(txt)
      },
      check_choose: function () {
        var that = this
        if (document.getElementById('type_radio1').checked || document.getElementById('type_radio2').checked) {
          that.check_choose_signature = true
        }
      },

      /* 0 = choose signature , 1 = draw , 2 = generate */
      showSignatureTab: function (i) {
        var tabID = ['home-tab', 'profile-tab', 'contact-tab']
        var aTag = document.querySelector('#' + tabID[i
        ])
        var tab = new bootstrap.Tab(aTag)
        tab.show();
      },

      isMyfield: function (field) {
        return (field.email == getUsername())
      },

      isFieldIncurrentOrder: function (field) {
        var o = this.sign_obj.current_order
        var b1 = !this.sign_obj.use_signing_order
        var b2 = (field.signOrder == o)
        var ret = (b1 || b2)
        //console.log('b1 (not use_signing_order)= ' + b1 + '\nb2 = ' + b2 + '\nret = ' + ret +
          '\nthis.sign_obj.current_order = ' + this.sign_obj.current_order +
          'field.signOrder = ' + field.signOrder + ', field.email ' + field.email)
        return ret
      },

      signDlg: function (field, oImg) {
        var that = this
        var isMyfield = this.isMyfield(field)

        if (isMyfield) {
          window.sign = field

          switch (field.toolName) {

            case 'Signature':
              checkSignature()
              this.showSignatureTab(1)
              this.showSignatureTab(0)
              $("#editSignaturebox").modal("show");

              if (document.getElementById('type_radio1').checked || document.getElementById('type_radio2').checked) {
                that.check_choose_signature = true
              }
              break;

            case 'Initial':
              $("#initialModal").modal("show");
              break;

            case 'Attachment':
              var attachmentName = field.attachmentName || 'Attachment Name'
              //console.log('Please upload document attachment : ' + attachmentName)
              $('#dropzone-attachment').click(); //console.log('showing attachment dialog')
              break;

            case 'Approve':
              $('#approve_reason').val(field.toolData)
              $("#approveModal").modal("show");
              break;

            default:
              break;
          }

        }

      },
      signApprove: function (field) {


      },
      disableField: function (field) {

        if (field.toolName == 'Signature') {
          //console.log('Disable div tool ' + field.toolName)
          $('#field' + field.id).css('background', 'grey')
        }

        if (field.toolName == 'Approve') {
          //console.log('Disable div tool ' + field.toolName)
          $('#field' + field.id).css('background', 'grey')

          var appTxt = field.isApproved == 'true' ? this.lang_page.approve[this.lang_choose] : this.lang_page.Disapprove[this.lang_choose]
          field.toolData2 = appTxt + ' เหตุผลการ ' + appTxt + ' ' + field.toolData

          $('#field' + field.id).html('<span>' + appTxt + ' เหตุผลการ' + appTxt + ' ' + field.toolData + '</span>')
          $('#field' + field.id).css('height', '').addClass('p-3')



        }

      },

      hideField: function (field) {

        if (field.toolName == 'Signature' || field.toolName == 'Initial') {
        //  alert('Hide div tool ' + field.toolName + '<---- Sign dialog should disappeare.')
          $('#field' + field.id).hide()

          if (field.toolName == 'Signature') {
            $("#editSignaturebox").modal("hide"); //hide the signature panel
            //   console.error('hide signature')
          }
          else {
            $("#initialModal").modal("hide"); //hide the initial panel
            // console.error('hide initial')
          }

          if (field.email == getUsername() && field.person && field.person.concensus_id) //ซ่อน concensus field อื่นๆของตัวเองที่เพิ่งเซ็นไป
          {
            //console.log('hide concensus#' + field.person.concensus_id)

            $("div").find(`[data-concensus-id='${field.person.concensus_id}']`).css('background', 'grey')
            $("div").find(`[data-concensus-id='${field.person.concensus_id}']`).each(function () {
              $(this).get(0).onclick = function () { }
            })

            register_sign_concensus(field.person.concensus_id, field)
          }

        } //signature 

        if (field.toolName == 'Attachment') {
          //console.log('Attachment ' + field.toolName + '<---- Sign dialog should disappeare.')
          $('#field' + field.id).css('background', 'grey')

        }



      },

      messageApprove: function () {
        var field = window.oImg
        var txt = $('#approve_reason').val()

        if (txt.length > 0) {

          window.sign.status = 1
          window.sign._color = 'grey'
          app.save_list.push(window.sign)

          field.toolData = txt

          this.disableField(field);
          this.hideField(field)
        }




      },
      updateApprove: function (approveStatus) {

        var field = window.oImg
        field['isApproved'] = approveStatus


      },
      defer_sign: function (originalField, fields) {
        //alert('defer_sign')
        var that = this
        //sign all fields
        for (var i = 0; i < fields.length; i++) {
          // alert(i)
          var f = fields[i]
          f.copy_of = originalField.id
          f.status = 1
          f.type = originalField.type
          f._color = originalField._color
          that.save_list.push(f)
          that.hideField(f)
          $(signatureDiv(f)).appendTo('#page-div' + f.page)
          f.copy_of = false
          chooseSignature_silent(f.type, f.id)
        }

      },
      sign: function () {
        var otherSigs = []
        var that = this
        // if (document.getElementById('sign_all').checked === true) {
        //   console.error('not implemented')
        //   //this.sign_all_signatures(this.sign_type)
        //   //this.auto_sign_text_fields()
        //   return;
        // }

        
        // sign all
        //if (false) {
        if (true) {
          if (window.sign.toolName == 'Signature' && document.getElementById('sign_all').checked) {
            otherSigs = that.sign_obj.fields.filter(function (field) {
              var b1 = (field.status == 0 && field.email == getUsername() && (field.toolName == window.sign.toolName))
              return b1;
            })

            var ele  = document.getElementById('type_radio1') 
            window.sign.type = (ele.checked)?"draw":"gen"

          }

          if (window.sign.toolName == 'Initial' && document.getElementById('sign_all_initial').checked) {
            otherSigs = that.sign_obj.fields.filter(function (field) {
              var b1 = (field.status == 0 && field.email == getUsername() && (field.toolName == window.sign.toolName))
              return b1;
            })

          }
        
          
          //console.log('otherSigs.length = ' + otherSigs.length)
          if (otherSigs.length > 0) {
            setTimeout(function () { that.defer_sign(window.sign, otherSigs) }, 2000)

          }

          return;
        }
        //end sign all
        //console.log('this.sign_type' + this.sign_type)
        window.sign.status = 1
        window.sign.type = this.sign_type
        window.sign._color = 'grey'
        app.save_list.push(window.sign)

        this.disableField(window.oImg);


        if (oImg.doc_field.toolName != 'Signature' && oImg.doc_field.toolName != 'Initial' &&
          oImg.doc_field.toolName != 'Attachment' && oImg.doc_field.toolName != 'Text') {

          /*  
          var defFontSize = 16
          var fontSize = oImg.doc_field.fontSize || defFontSize
          var p1 = window.oImg.top
          var p2 = window.oImg.getScaledHeight()
       
             
            if(oImg.oldText)
               {
                 window.oImg.canvas.remove(oImg.oldText);
                 oImg.oldText = null
               }
               
       
            var text = new fabric.IText(oImg.doc_field.toolData, {objecttype:'text',left: window.oImg.left, top: p1, opacity:0.7, backgroundColor:'yellow', fontSize:fontSize,hasBorders : true,hasControls : false, selectable  : false,  lockMovementX : true, lockMovementY: true , minWidth:50 });
           
            oImg.oldText = text
        
            window.oImg.canvas.add(text);
            window.oImg.opacity = 0.5
            if(oImg.doc_field.toolName != 'Text'){
              window.oImg.canvas.remove(window.oImg);
            }else{
              window.oImg.canvas.bringToFront(window.oImg) 
            }*/

        }

        if (oImg.doc_field.toolName == 'Signature' || oImg.doc_field.toolName == 'Initial') {
          //todo eark review this
          var t = 1000
          setTimeout(function () {
            var html = signatureDiv(oImg.doc_field)
            $(html).appendTo('#page-div' + oImg.doc_field.page)
          }, 1000)

        }


        if (oImg.doc_field.toolName == 'Attachment') {
        }

        this.hideField(oImg.doc_field)

        // this.canFinish = true

        // var _nextField = next_signable_fields(oImg.doc_field)
        // //console.log('_nextField = '+ _nextField)
        // if(_nextField)
        //   {
        //      goto_position(_nextField)
        //   }
        // else
        //   {
        //    on_finishing() 
        //   } 


      },


      initPage: function (pageIndex) {
        this.initFields(pageIndex)

      },

      initFields: function (pageIndex) {
        var img = document.getElementById("img" + pageIndex)
        var box = img.getBoundingClientRect()
        var fields = this.sign_obj.fields.filter(f => {
          return f.page == pageIndex
        })

        if (fields.length > 0) { db('page' + pageIndex + ' has ' + fields.length + " fields.") }

        fields.forEach(f => {
          var html = fieldDiv(f, box)
          db(html)
          $(html).appendTo('#page-div' + pageIndex)
          this.process_finished_fields(f)
        });

        //process status ==1 
        // fields.forEach(f => {
        //      if(f.status ==1)
        //        {

        //        }
        //   }); 
      },

      process_finished_fields: function (f) {
        if (f.status == 1 && (f.toolName == 'Signature' || f.toolName == 'Initial')) {  //alert('process_finished_fields '+ JSON.stringify(f)) 
          this.sign_type = f.type
          f.doc_field = f
          window.sign = f
          window.oImg = f
          this.sign()

        }

        if (f.status == 1 && f.toolName == 'Approve') {
          f.doc_field = f
          window.sign = f
          window.oImg = f
          this.sign()

        }
      },

      upload_attachment: function () {

        if (window.upload_attachment) return;

        window.upload_attachment = true

        //var url = api_host + 'upload_attachment'
        //var url = api_host + 'upload'
        var url = api_host + 'upload_attachment_new'       

        var dropzone = new Dropzone(document.getElementById('dropzone-attachment'), {
          url: url,
          clickable: true,
          //acceptedFiles: ".jpg, .jpeg, .png, .pdf",
          acceptedFiles: ".jpg, .jpeg, .png, .pdf, .xls, .xlsx",
          uploadMultiple: true,
          parallelUploads: 20,
        });

        dropzone.on("addedfile", function (file) { 
           var name  = file.name
           window.lastExt = name.substring(name.lastIndexOf('.'))
           
        });

        dropzone.on("success", function (file, json) {
          //console.log('dropzone success ' + file.name + ', ' + json)
          var res = JSON.parse(json)
          if (res.success) {
            //window.sign.toolData = res.filename + '.pdf';
            window.sign.toolData = res.filename + window.lastExt;
            app.sign();
          }
          else {
            console.error('Upload Error, please try again.')

          }
        });
      },


      saveDoc: function () {
        var that = this;
        that.btn_save_show = false
        var url = window.api_host + 'update_edoc_bla'
        var arr = that.sign_obj.fields.filter(function (field) { return field.status == 0 && field.email == getUsername() && field.toolName == 'Signature' });
        var txt = (localStorage.lang_choose == "th") ? "เอกสารยังไม่สมบูรณ์ " : "Not signed yet."
        if (arr.length > 0) {
          console.error(txt)
          alert(txt)
          that.btn_save_show = true
          //window.tmp_arr = arr
          return
        }


        var arr = that.sign_obj.fields.filter(function (field) { return field.email == getUsername() })
        var update_list = []
        arr.forEach(f1 => {
          f1.doc_field = false
          var f2 = $.extend({ status: 1 }, f1)
          f1.doc_field = f1
          update_list.push(f2)

        })

        var radioArr = that.sign_obj.fields.filter(function (field) { return field.toolName == 'Radio2' && field.email == getUsername() })
        radioArr.forEach(f => {
          var radioEle = document.getElementById(f.id)
          f.toolData = radioEle.checked ? 'X' : ''
        })

        var json = {
          fields: update_list,
          id: that.sign_obj.id,
          cert_pin: that.cert_pin
        }

        json.fields.forEach(f => {
          if (f.toolName == 'Radio2' && f.email == getUsername()) {
            var radioEle = document.getElementById(f.id)
            f.toolData = radioEle.checked ? 'X' : ''
          }
        })

        window.update_list = update_list

        //alert(JSON.stringify(json))
        //console.log(JSON.stringify(json))

        //alert('done1')
        //return ;


        $('#openLoadding').click();
        $.ajax({
          url: url,
          type: "POST",
          data: JSON.stringify(json),
          success: function (result) {

            var res = JSON.parse(result)
            if (res.success) {
              // if(that.id_deal != "" || that.id_deal != undefined || that.id_deal != null){
              //   that.update_status_creden()
              // }
              //alert('done')
              var url = '../bla/#/document?id=' + that.sign_obj.id
              document.location.href = url;
              //$('#closeLoadding').click();
              // that.canFinish = false 
              // on_saved()
            }
            else {
              console.error(res.error_msg)
              
            }


          },
          error: function (error) {
            // hideWaiting() //got
            that.btn_save_show = true
            $('#closeLoadding').click();
            console.error("Error : Something went wrong!");
          }
        })
      },
      close: function () {
        window.close()
      },
      confirmPassword: function () {
        var that = this

        if (that.chk_pass == that.password_secret) {
          $('#passModal').modal('hide')
        } else {
          that.count_input_pass++
          that.password_secret_check = false
          setTimeout(function () {
            that.password_secret_check = true
            that.password_secret = ""
          }, 5000);
        }
      },
      cancelPassword: function () {
        var idDocument = this.sign_obj.id
        document.location.href = '/bla/#/document?id=' + idDocument
      },

      checkPermission: function (verified_ekyc) {
        //console.log("checkPermission " + verified_ekyc)
        var p = this.sign_obj.signers.find(function (p) { return p.email == getUsername() })
        var p2 = this.sign_obj.signers.find(function (p) {
          if (p.people_in_position == undefined || p.people_in_position == null) {
            return null
          } else {
            return p.people_in_position.find(function (p2) { return p2.email == getUsername() })
          }
        })

        this.chk_pass = p.password
        if (!p?.eKyc || (p?.eKyc && verified_ekyc)) {
          
          var passwordOK = true
          var pwdCount = 0
          if(p.usePassword){
            $('#passModal').modal('show')
          }
          
          this.canSign = passwordOK

        }


        this.canView = this.canSign || this.sign_obj.owner == getUsername()
        var ok = this.canView || this.canSign



        if (ok || is_gb || is_api) {
          //console.error('checkPermission '+ok)
          var canvas_init_timeout = 0 * 1000
          //alert('start')
          //setTimeout(function(){ app.initCanvas() }, canvas_init_timeout)
        }
        else {
          console.error('This document requires ekyc verifications.\nต้องทำการยืนยันตัวตนก่อนเปิดเอกสารนี้')
          document.location.href = '/bla/#/ekyc'
        }
      },


      getUser: function () {
        var that = this
        var login_service = "esig"
        $.ajax({
          type: "POST",
          url: api_host + "get_users",
          data: JSON.stringify({ login_service }),
          dataType: "json",
          success: function (data) {
            //console.log('data: ', data);

            if (!data.success) {
              console.error('Session expired ... please login again')
              document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
            }
            ////console.log(JSON.stringify(data))
            that.myFirstname = data.data.adminFisrtname || ''
            that.getSignObj(data.data.verified_ekyc)
          },
          error: function (request, status, error) {
            //console.log('request: ', request);
            //console.log("|" + status + ' |' + error);
            document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
          }
        });

      }, //getUser

      getSignObj: function (vrf_ekyc) {
        var that = this
        //var pass=pass
        var password = gup('stampkey')
        var url = "../capi/get_edoc_detail?id=" + this.doc_id
        if (password) {
          url = url += '&password=' + password
        }


        //if(is_api)
        //url = "../capi/get_edoc_api?id=" + this.doc_id + "&token=" + gup('tk')

        $.ajax({
          type: "GET",
          url: url,
          dataType: "json",
          success: function (data) {

            app.dtm = data.dtm
            var verified_ekyc = vrf_ekyc
            var sign = data.data;
            var fields = sign.fields
            var signers = sign.signers
            that.id_deal = sign.doc_number

            if (that.id_deal) {
              console.error('get_deal')
              that.get_deal()
            }
            if (!fields.length) fields = []

            // group signing  
            fields.forEach(f => {
              if (f.status == 0) { convert_group_to_mine(f) }
            })
            // end group signing

            // sign = a;
            for (i = 0; i < fields.length; i++) {
              if (fields[i].toolName == 'Qrcode' && fields[i].status == 0) {

                fields[i].status = 1
                that.save_list.push(fields[i]);
                that.canFinish = true
                //is_gb = true
              }

            }


            var mail_sign = fields.filter(function (a) {
              return a.email == getUsername()
            }).length

            that.count_field = mail_sign

            that.sign_obj = sign
            // setTimeout(function(){ app.auto_sign_text_fields() }, 3000);
            var mail_screen = that.sign_obj.signers.filter(function (a) {
              return a.email == getUsername()
            })
            if (mail_screen[0].needToSign == true && mail_screen[0].is_inspector && mail_screen[0].is_inspector == true) {
              that.check_screener_approve = true
            }

            // เซนไม่ระบุ
            var no_sign_field = fields.filter(function (f) {
              return f.is_no_sign == true
            })
            that.no_sign_field = no_sign_field


            that.nameProfile = sign.owner_name
            that.canDownload = sign.status > 0
            that.canSave = !that.canDownload

            var user_inspecter = signers.filter(function (s) {
              return (s.email == getUsername() && s.is_inspector == true)
            })

            if (user_inspecter.length == 0) {
              that.use_cert()
            }

            if (user_inspecter[0].usePassword) {
              that.checkPermission(verified_ekyc)
            }

            // that.checkPermission(verified_ekyc)
            that.canshare = true
            //  console.error('+++ /signature/main.html#/'+ document.location.origin)

            that.urlshare = document.location.origin + '/signature/main.html#/manage?id=' + sign.id;

            if (sign.password == undefined || sign.password == '') {
              that.canShare = true
              that.urlsharepublic = "";//document.location.origin + '/sharedoc.html?id='+ sign.id  +'&stampkey='+sign.password;
            } else {
              that.canShare = false
              that.urlsharepublic = document.location.origin + '/sharedoc.html?id=' + sign.id + '&stampkey=' + sign.password;
            }

          },
          error: function (request, status, error) {
            console.error("|" + status + ' |' + error);
          }
        });
      },
      hide_img_sign: function (p) {
        var that = this
        if (p == 1 || p == '1') {
          that.show_initia_draw = false
        } else if (p == 2 || p == '2') {
          that.show_initia_gen = false
        }
      },
      // select_initia_sign:function(s){
      //   var that = this
      //   if(s == 1){
      //     that.select_initia_sing = that.initia_draw
      //   }else if( s == 2){
      //     that.select_initia_sing = that.initia_gen
      //   }
      //  console.error(that.select_initia_sing )
      // },
      clearSignature: function (n) {
        if (n == 1 || n == '1') {
          signaturePad.clear();
        } else if (n == 2 || n == '2') {
          signaturePadInitial.clear()
        }
      },
      createSignature_initial: function () {
        var that = this
        var data = signaturePadInitial.toDataURL('image/png');
        data = data.split(',')
        $.ajax({
          type: "POST",
          url: "../capi/create_signature_initial",
          data: JSON.stringify({ "img": data[1], "username": getUsername(), "code": getUsername(), "sign_type": "draw_initial", "field_id": '' }),
          dataType: "json",
          success: function (data) {
            var path = '../edoc/app-assets/images/signature/' + getUsername() + '_draw_initial.png?rd=' + new Date().getTime()
            that.initia_draw = path
            that.show_initia_draw = true
            console.error('สร้างลายเซ็นสำเร็จ draw')
            that.clearSignature(2)
            set_sign_type(document.getElementById('initial_sign_draw').value)
          },
          error: function (request, status, error) {
            console.error("|" + status + ' |' + error);
          }
        });
      },
      genSignature_initial: function () {
        var that = this
        var url = "../capi/gen_signature_initial"
        data = { "name": that.sign_name_initial, "code": getUsername(), "lang": 'en', "font_name": 'worasait.ttf', "field_id": '' }
        $.ajax({
          type: "POST",
          url: url,
          data: JSON.stringify(data),
          dataType: "json",
          success: function (data) {
            var path = '../edoc/app-assets/images/signature/' + getUsername() + '_gen_initial.png?rd=' + new Date().getTime()
            that.initia_gen = path
            that.show_initia_gen = true
            console.error('สร้างลายเซ็นสำเร็จ gen')
          },
          error: function (request, status, error) {
            console.error("|" + status + ' |' + error);
          }
        });
      },

      use_cert: function () {
        var that = this
        $("#enterpinModal").modal("show");
        var key1 = document.getElementById('1')
        that.p1 = ""
        that.p2 = ""
        that.p3 = ""
        that.p4 = ""
        that.p5 = ""
        that.p6 = ""
        key1.select()
        key1.focus()


      },
      use_cert2: function () {

        var that = this

        var pin = that.p1 + that.p2 + that.p3 + that.p4 + that.p5 + that.p6

        if (pin && pin.length == 6) {
          that.status_pin = true;
          //console.error("use pin success")
          // that.cert_pin = pin
          $.ajax({
            type: "POST",
            url: api_host + "check_pin",
            data: JSON.stringify({ pin: pin }),
            dataType: "json",
            success: function (data) {
              if (data.success) {
                that.cert_pin = pin
                $("#enterpinModal").modal("hide");
                //  $('#closePin').click()
                that.status_pin = false;
                that.checkPermission(true);

              }
              else {
                that.pwdCount = that.pwdCount + 1
                console.error('Invalid PIN')
                if (that.pwdCount < 3) {
                  pin = ""
                  that.cert_pin = ""

                  var key1 = document.getElementById('1')
                  that.p1 = ""
                  that.p2 = ""
                  that.p3 = ""
                  that.p4 = ""
                  that.p5 = ""
                  that.p6 = ""
                  key1.select()
                  key1.focus()
                  that.status_pin = false;
                }
                else {

                  var id = gup("id") || ""
                  if (id) document.location.href = '/bla/#/document?id=' + id
                  else document.location.href = "/bla/#/inbox"

                }

              }


            },
            error: function (request, status, error) {
              console.error("|" + status + ' |' + error);
              that.pin_number = ""

            }
          });

        }


      },
      update_screener: function () {
        var that = this
        var data = {
          doc_id: that.sign_obj.id
        }

        $.ajax({
          type: "POST",
          url: api_host + "approve_screener",
          data: JSON.stringify(data),
          dataType: "json",
          success: function (res) {
            if (res.success) { }
            //console.log("upadte screener")
            // var url = '../bla/#/document?id=' + that.sign_obj.id
            // document.location.href = "/bla/#/document?id=" + that.sign_obj.id
          },
          error: function (request, status, error) {
            console.error("|" + status + ' |' + error);
          }
        });
      },
      approve_document: function (boo) {
        var that = this
        var fields = that.sign_obj.fields

        if (!fields.length) fields = []
        for (var i = 0; i < fields.length; i++) {
          if (fields[i].email == getUsername() && fields[i].toolName == "Approve") {
            fields[i].toolData = that.note_approve
            fields[i].status = 1
            fields[i].isApproved = boo

            $('#field' + fields[i].id).css('background', 'grey')
            var appTxt = fields[i].isApproved ? 'อนุมัติ' : 'ไม่อนุมัติ'
            fields[i].toolData2 = appTxt + ' เหตุผลการ ' + appTxt + ' ' + fields[i].toolData

            $('#field' + fields[i].id).html('<span>' + appTxt + ' เหตุผลการ' + appTxt + ' ' + fields[i].toolData + '</span>')
            $('#field' + fields[i].id).css('height', '')

          }
        }

        if (boo) that.update_screener()
        that.saveDoc()

      },
      select_approve: function (select) {
        var that = this
        that.btn_approve = select
      },
      disable_fields_no_sign: function () {
        var that = this
        // that.no_sign_field
      }



    } //end methods
  })
}

