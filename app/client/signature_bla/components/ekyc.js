var counter
Pages.Ekyc = {
    _template: 'ekyc.html',
    template: '<div>this is ekyc.html</div>',
    data: function () {
        return {
        lang_choose: localStorage.getItem('lang_choose') || 'th',
        lang_txt: lang_ekycactivate,
        adminEmail: getUsername(),
        is_verifly : false,
        lastupdateekyc:'',
        chk_kyc_status:'',
        p1:'',
        p2:'',
        p3:'',
        p4:'',
        p5:'',
        p6:'',
        type:'self_dipchip',
        status_pin:false,
        txid:'',
        tel:'',
        step:0,
        link:'',
        startTime : 0,
        endTime : 0,
        startTime1 : 0,
        endTime1 : 0,
        diff2:0,
        diff:0,
        diff21:0,
        diff1:0,
        count:0,
        minutes:0,
        token:'',
        status_qr:false,
        cardreader_id:'',
        step_verify_otp:false,
        qr_id:gup('qr_id') || ''
        }
    },
    mounted: function () {
        window.myApp = this
        this.setFormProfile()
        if(this.qr_id!=''){
            this.goKyc()
        }
     
    },
    methods: {
        setFormProfile: function () {

            var that = this

            $.ajax({
                type: "POST",
                url: api_host + "check_kyc_esig",
                data: JSON.stringify({ adminEmail: getUsername() }),
                dataType: "json",
                success: function (data) {
                   that.chk_kyc_status = data.data.verified_ekyc

                    if (data.data.verified_ekyc == false) {
                       
                        that.textkyc = "ยืนยันตัวตน"
                     
                    } else {
                        
                        that.textkyc = "อัพเดทการยืนยันตัวตน"
                       
                    }
                    //lastupdate ekyc
                    var lastupdate_ekyc = data.data.last_kyc_dtm
                    that.lastupdateekyc = lastupdate_ekyc
                    that.cardreader_id = data.data.id
                  
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
       
        goKyc:function(){
           // alert(window.screen.width * window.devicePixelRatio )
            if(window.screen.width * window.devicePixelRatio >= 1020){
                var that = this
                // alert('sslpspl')
                that.show()
            }else{
            var that = this
            document.getElementById('ref_no').value = this.adminEmail
            document.getElementById('txid').value = new Date().getTime();
            document.getElementById('adminEmail').value = getUsername()
            document.getElementById('redirect_url').value = document.location.origin + '/bla/#/ekyc';
            document.ekycForm.action = document.location.origin + '/capi/ekyc_auth_self'
            document.ekycForm.submit(); 
            }
        },

        goKyc_dipchip:function(){
            // alert(window.screen.width * window.devicePixelRatio )
           
             var that = this
             document.getElementById('ref_no').value = getUsername()
             document.getElementById('txid').value = new Date().getTime();
             document.getElementById('redirect_url').value = document.location.origin + '/bla/#/ekyc';
             document.getElementById('new_parameter').value = that.type;
             document.getElementById('card_reader_id').value = that.cardreader_id
             document.getElementById('adminEmail').value = getUsername()
            //  document.getElementById('ref_no').value = this.adminEmail
            //  document.getElementById('txid').value = new Date().getTime();
            //  document.getElementById('adminEmail').value = getUsername()
            //  document.getElementById('redirect_url').value = document.location.origin + '/bla/#/ekyc';
             document.ekycForm.action = document.location.origin + '/capi/ekyc_auth_self_dipchip'
             document.ekycForm.submit(); 
             
         },
        show:function(){
            var that = this
            $('#exampleModalLong').show()
            that.create_qr_code()
        },
        create_qr_code:function() {
            var that = this
            
            that.status_qr = false
           var data = { username:getUsername(),modal:'create_qr'}

            $.ajax({
                type: "POST",
                url: document.location.origin + "/capi/create_qr_eks",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    if(res.success){
                        that.link = document.location.origin + '/face/images/qrcode/qrcode_'+res.txid+'.png'
                        that.txid = res.txid
                        that.startcountdown();
                        that.check_qr_code();
                        
                    }
                    else {
                        alert(res.error_msg)
                    }
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
            // data-toggle="modal" data-target="#exampleModalLong"
           
            
        },
        check_qr_code:function() {
            var that = this 
            var data = {qr_id:that.txid,username:getUsername(),modal:'chk_qr'}
            // alert(JSON.stringify(data))
            $.ajax({
                type: "POST",
                url: api_host + "create_qr_eks",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                   if(data.success){
                       if(data.status == "Complete"){
                        // $('#exampleModalLong').hide()
                        that.status_qr = true
                        // clearInterval(counter)
                        counter = 0
                        that.count = 0
                       }else{
                           setTimeout(function(){
                            that.check_qr_code()
                           },1000)
                       }
                   }
                  
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
           

            
        },
        startcountdown:function() {
            
            var that = this
            that.startTime = new Date().getTime()+(1000*60*5);
            var counter = 60;
            var newYearCountdown = setInterval(function(){
              
              
             
              that.count = counter
              counter--
              if (counter === 0) {
                span = document.getElementById("test");
                span.innerHTML = '';
                clearInterval(newYearCountdown);
                that.status_qr = true
              }
              
            }, 1000);

            // setInterval(function() {
            //     if(that.status_qr == true){
            //         span = document.getElementById("test");
            //         span.innerHTML = '';
            //         counter = 0
            //         clearInterval(counter);
            //          that.status_qr = true
            //     }else{
            //   counter--;
            //   if (counter >= 0) {
            //      span = document.getElementById("test");
            //      span.innerHTML = counter;
            //     //that.diff2 = counter
            //   }
            
            //   if (counter === 0) {
            //     // alert('Out of time!');
            //     span = document.getElementById("test");
            //     span.innerHTML = '';
            //     clearInterval(counter);
            //     that.status_qr = true

            //     //that.diff2 = counter
            
            //   }
            // }
            
            // }, 1000);
    
            // setTimeout(that.updateTime(),1000);

            
        },
       
        requestOTP : function(){
                
            this.state = 3;
            var that = this;
            if(that.tel.length==10){
              a = that.tel.substr(0,1);
              // alert(a)
              if(a == 0){ that.tel = that.tel.substr(1)}
            }
            data = {tel: this.tel, email : getUsername(),country:'66'}
             $.ajax({
            type: "POST",
            url: api_host + "request_otp_new",
            data : JSON.stringify(data),
            dataType: "json",
            success: function(data) {
              //console.log(data.emailx);
              that.step = 1
              that.token = data.token;
                
            },
            error: function(request, status, error){
                alert("|"+ status + ' |' +error);
               
            }
            });
        
        },

        check_pin : function(){
            var that = this;
            var pin = that.p1 + that.p2 + that.p3 + that.p4 + that.p5 + that.p6
            
            if(pin.length > 6) {
                that.status_pin = false
            }else{
                that.status_pin = true
            }

        },
        verifyOTP : function(){
            var that = this;
            var otp_confirm = that.p1+that.p2+that.p3+that.p4+that.p5+that.p6 
            data = { otp:otp_confirm, token:that.token, tel:that.tel, adminEmail:getUsername(), login_service:'esig'}
             // alert(JSON.stringify(data))
            $.ajax({
            type: "POST",
            url: api_host + "verify_otp_eks",
            data : JSON.stringify(data),
            dataType: "json",
            success: function(res) {
                if(res.success){
                 
                  that.step = 3
                    that.create_qr_code()
                //    document.location.href = '/con_email.html?email='+app.getemail;
                }else{
                  alert(res.error_msg)
                }
            },
            error: function(request, status, error){
                alert("|"+ status + ' |' +error);
            }
            });
            
        },

    }
}
function countX(){

  
}

routes.push({ path: '/ekyc', component: Pages.Ekyc })
moveOnMax = function (field, nextFieldID, previousFieldID) {
    if (event.keyCode == "8" || event.keyCode == "46") {
      if (previousFieldID != "N/A") {
        document.getElementById(previousFieldID).focus();
        return false
      }
    }
    Pages.Ekyc.methods.check_pin()
    if (field.value.length == 1) {
      if (nextFieldID == "N/A") return false
      document.getElementById(nextFieldID).focus();
    }


  }
var lang_ekycactivate = {
    "profile": { "th": "ข้อมูลส่วนตัว", "en": "Profile" },
    "step_text": { "th": "กรุณาใส่หมายเลขมือถือ", "en": "Please insert phone above in input" },
    "step_text2": { "th": "ข้อมูลส่วนตัว", "en": "Profile" },
    "email": { "th": "อีเมล", "en": "E-mail" },
    "profilepic": { "th": "ภาพโปรไฟล์", "en": "Profile picture" },
    "upload": { "th": "อัพโหลด", "en": "Upload" },
    "ekyc": { "th": "การยืนยันตัวตน", "en": "eKYC" },
    "firstname": { "th": "ชื่อ", "en": "First Name" },
    "lastname": { "th": "นามสกุล", "en": "Last Name" },
    "birthday": { "th": "วันเกิด", "en": "Birthday" },
    "day": { "th": "วัน", "en": "Day" },
    "month": { "th": "เดือน", "en": "Month" },
    "year": { "th": "ปี", "en": "Year" },
    "cardid": { "th": "เลขบัตรประชาชน", "en": "Card ID" },
    "behindcard": { "th": "เลขหลังบัตร", "en": "Laser Code" },
    "province": { "th": "จังหวัด", "en": "Province" },
    "lastdate": { "th": "อัปเดตล่าสุด", "en": "last update eKYC" },
    "save": { "th": "บันทึก", "en": "Save" },

    "Identity": {"th": "ยืนยันตัวตนของคุณ", "en": "Identity Verification"},
    "Verification": {"th": "ระดับการยืนยันตัวตนของคุณ", "en": "Verification Standard"},
    "Verification_Update": {"th": "อัพเดตการยืนยันตัวตน DIPCHIP", "en": "Verification Update"},
    "Level": {"th": "ระดับการยืนยันตัวตนของท่านคือ", "en": "Your level of identify"},
    "Last_update": {"th": "อัปเดตล่าสุด", "en": "Last update"},
}
