var Pages = {},
    router, routes = [],
    app
var api_host = document.location.origin + '/capi/'
var upload_path = '../face/images/card/'

function preload_vue() {
    var doneList = []
    routes.forEach(function(r) {
        var comp = r.component
        $.get(comp._template, function(html) {
            comp.template = html;
            comp._template = null
            doneList.push(comp)
            if (doneList.length == routes.length) {
                init_vue();
                navApp = newApp('navApp');
                extendSession();
            }
        });

    })
}

function init_vue() {
    ////console.log(routes)
    router = new VueRouter({ routes: routes });
    app = new Vue({
        el: '#app',
        router: router,
    })
}


function newApp(id) {
    return new Vue({
        el: '#' + id,
        router: router,
        data: function() {
            return {
                lang_choose: localStorage.getItem('lang_choose') || 'th',
                lang_txt: lang_index,
                myProfile: this.getProfile(),
                myFirstname: getUserField("adminFisrtname") || "",
                myLastname: getUserField("adminLastname") || "",
                myEmail: getUserField("adminEmail") || "",
                myRole: getUserField("role") || "",
                myLogo: "",
                powered_by: "creden",
                name_folder: '',
                list_folder: [],
                choose_folder: [],
                searchHeader: "",
            }
        },
        mounted: function() {
            var init_url = document.location.origin + '/bla/#/'
            var url_name = document.location.href
            if (url_name == init_url) router.push('dashboard');
            //this.get_folder()
        },
        methods: {
            gotoPage: function(page) {
                router.push(page);
                if (window.innerWidth < 992) closeNav()
            },
            getProfile: function() {
                var p = getUserField("profile_pic") || ""
                var f = p.search("http")
                var img = ""

                if (f == 0) img = p
                else if (f == -1 && p != "") img = "../face/images/card/" + getUserField("profile_pic") + ".png"
                else img = "images/profile.png"
                return img
            },

	    getUsername: function(){
	    	const username = localStorage.getItem("username");

		      // Check if username exists in localStorage
		    if (username) {
		        return username;
		    } else {
		    	return "N/A";
		    }
	    },

            logout: function() {
            	
		var data = {"email": getUsername() || "N/A"};
            	//var data = {"email": localStorage.getItem('username') || "N/A"};

                localStorage.removeItem('compCode')
                localStorage.removeItem('username')
                localStorage.removeItem("register_obj")
                    // localStorage.removeItem("listNotification_json")

                $.ajax({
                    type: "POST",
                    url: api_host + "logout",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function(data) {
                        document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
                    },
                    error: function(request, status, error) {
                        //console.log("Logout Error |" + status + ' |' + error);
                        document.location.href = 'index.html'
                    }
                });

            },
            add_folder: function() {
                var that = this

                if (that.name_folder == "") {
                    alert("กรุณาใส่ชื่อแฟ้มเอกสาร")
                    return false
                }

                var data = { owner_folder: that.myEmail, status_folder: "1", name_folder: that.name_folder }
                $.ajax({
                    type: "POST",
                    url: api_host + "add_folder",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function(res) {
                        // //console.log(res);
                        document.location.reload()
                    },
                    error: function(request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });
            },
            get_folder: function() {
                var that = this
                var data = { owner_folder: that.myEmail }
                $.ajax({
                    type: "POST",
                    url: api_host + "get_folder",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function(res) {
                        that.list_folder = res.data
                            // //console.log(that.list_folder);
                    },
                    error: function(request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });

            },
            go_folder: function(name, id) {
                var that = this
                    // document.location.href = '/bla/#/folder_doc?name=' + name + '&id=' + id
                    // document.location.reload();
                router.push({ path: 'folder_doc?name=', query: { name: name, id: id } })
                    // setTimeout(function(){ document.location.reload() }, 500);
                document.location.reload()
            },
            select_folder: function(i) {
                var that = this
                that.choose_folder = that.list_folder[i]
                that.name_folder = that.choose_folder.name_folders
            },
            edit_folder: function() {
                var that = this
                var c = that.choose_folder

                if (c._id) {
                    var data = {
                        email: getUsername() || "N/A",
                        id_folder: c._id,
                        name_folders: that.name_folder,
                    }

                    $.ajax({
                        type: "POST",
                        url: api_host + "edit_folder",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function(resp) {
                            // //console.log(res);
                            if (resp.success) {
                                alert("แก้ไขสำเร็จ")
                                document.location.reload()
                            } else {
                                alert("แก้ไขไม่สำเร็จ")
                            }
                        },
                        error: function(request, status, error) {
                            alert("|" + status + ' |' + error);
                        }
                    });
                }

            },
            deleteFolder: function(id_folder) {
                var that = this;
                var url = api_host + 'remove_folder'
                var langtextalert = ''

                if (this.lang_choose == 'th') {
                    langtextalert = "คุณยืนยันที่จะลบแฟ้มเอกสารนี้ใช่หรือไม่"
                } else {
                    langtextalert = "Do you confirm to delete this folder?"
                }
                var data = { id_folder: id_folder }

                var r = confirm(langtextalert);
                if (r == true) {
                    //console.log(data);

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function(result) {
                            // that.get_folder()
                            document.location.href = document.location.origin + '/bla/#/dashboard'
                            document.location.reload()

                        },
                        error: function(error) {
                            alert("Something went wrong!" + error);
                        }
                    })
                }
            },
            open_pricing: function() {
                var that = this
                window.open("/bla/web/pricing.html")
            },
            confirm_company: function(con, id) {
                var that = this
                var data = {
                    confirm: con,
                    email: getUsername() || "N/A",
                    company_id: id,
                }
                $.ajax({
                    url: api_host + "update_invite_company",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function(resp) {
                        resp = JSON.parse(resp)
                        if (resp.success) {
                            //console.log("update company success")
                            document.location.reload()
                                // var r = JSON.stringify(resp.data)
                                // localStorage.setItem("register_obj", r)
                        } else {
                            alert(resp.error_msg)
                        }

                    },
                    error: function(error) {
                        alert("Something went wrong!" + error);
                    }
                })

            },

        },
        // end Method
    })
}

function extendSession() {
    var x = new Date().getTime()
    document.getElementById('bug_session').src = '../capi/test_bug_session?random=' + x
    $.ajax({
        type: "GET",
        url: api_host + "extend_session?random=" + x,
        data: '',
        dataType: "json",
        success: function(data) {

            if (data.user_only == "" || data.user_only == " " || data.user_only == "null") {
                document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
                extendSession2();
            }

            if (data.invite_company) {
                var r = JSON.parse(data.invite_company)
                Swal.fire({
                    title: "คุณต้องการจะเข้าร่วม " + r.invite_company + " หรือไม่",
                    text: 'ข้อควรระวัง : เอกสารที่คุณทำหรือได้รับหลังจากเข้าร่วมบัญชีประเภทบริษัท เอกสารดังกล่าวจะถือว่าเป็นเอกสารของบริษัท หากออกจากกลุ่มบัญชีประเภทบริษัทไปแล้ว เอกสารจะไม่แสดงให้คุณเห็น',
                    showDenyButton: true,
                    confirmButtonText: "เข้าร่วม",
                    denyButtonText: "ปฏิเสธ",
                }).then((result) => {
                    if (result.isConfirmed) {
                        navApp.confirm_company("YES", r.company_id)
                    } else if (result.isDenied) {
                        navApp.confirm_company("NO", r.company_id)
                    }
                })
            }

            navApp.myLogo = data.company_logo
            var s = data.company_logo.search("/face/images/card")
            if (s == -1) navApp.powered_by = true
            else navApp.powered_by = false

            setTimeout(extendSession, 1000 * 15)
        },
        error: function(request, status, error) {
            //console.log("Extend session error |" + status + ' |' + error);
            document.location.href = "/bla/web/login.html"
        }
    });
}

function extendSession2() {
    //console.log("expire session")
    var x = new Date().getTime()
    $.ajax({
        type: "POST",
        url: api_host + "extend_session2?random=" + x,
        data: 'key=' + localStorage.extend_key + '&username=' + getUsername(),
        dataType: "json",
        success: function(res) {
            // //console.log(res.username + ' ' + res.extend_key)
            localStorage.setItem("extend_key", res.extend_key);
        },
        error: function(request, status, error) {
            //console.log("extend session2|" + status + ' |' + error);
        }
    });
}

function swithLang(lang) {
    localStorage.setItem('lang_choose', lang);
    navApp.lang_choose = lang
    window.myApp.lang_choose = lang
        // reloadVuePage()    
}

var lang_index = {
    "home": { "th": "หน้าหลัก", "en": "Home" },
    "sent": { "th": "เอกสารที่ส่งออก", "en": "Sent" },
    "inbox": { "th": "เอกสารที่ได้รับ", "en": "Inbox" },
    "allinbox": { "th": "เอกสารทั้งหมด", "en": "All document" },
    "support": { "th": "แนะนำการใช้งาน", "en": "Support Center" },
    "get_center": { "th": "เริ่มต้นการใช้งาน", "en": "Getting Started" },
    "help": { "th": "ช่วยเหลือ", "en": "Help" },
    "draft": { "th": "เอกสารร่าง", "en": "Drafts" },
    "notsign": { "th": "เอกสารที่รอการลงนาม", "en": "Incomplete Signing" },
    "self_sign": { "th": "ฉันเซ็นคนเดียว", "en": "Self-Signing Document" },
    "cancel_doc": { "th": "เอกสารยกเลิก", "en": "Cancelled Document" },
    "temp_doc": { "th": "เทมเพลตเอกสาร", "en": "Document Template" },
    "folder": { "th": "ประเภทเอกสาร", "en": "Document Type" },
    "kyc": { "th": "การยืนยันตัวตน", "en": "e-KYC" },
    "setting": { "th": "การตั้งค่า", "en": "Settings" },
    "pricing": { "th": "ราคาและแผนการใช้งาน", "en": "Pricing and Plan" },
    "usage": { "th": "การใช้งานของคุณ", "en": "Usability" },
    "price_and_pack": { "th": "ราคาและแพ็กเกจ", "en": "Pricing and Service Package" },
    "user_set": { "th": "ตั้งค่าผู้ใช้งานในระบบ", "en": "User Settings" },
    "iv": { "th": "ยืนยันตัวตนของคุณ", "en": "Identity Verification" },
    "com_set": { "th": "ตั้งค่าบริษัท", "en": "Company Profile" },
    "transaction": { "th": "ประวัติการทำรายการ", "en": "Transactions History" },
    "upload": { "th": "สร้างเอกสาร", "en": "Create Document" },
    "rename": { "th": "เปลี่ยนชื่อ", "en": "Rename" },
    "delete": { "th": "ลบ", "en": "Delete" },
    "cer": { "th": "ใบรับรองอิเล็กทรอนิกส์", "en": "Bangkok Life Certificate" },
    "close": { "th": "ปิด", "en": "Close" },
    "save": { "th": "บันทึก", "en": "Save" },
    "profile": { "th": "ข้อมูลส่วนตัว", "en": "Profile" },
    "new_folder": { "th": "สร้างแฟ้มเอกสารใหม่", "en": "Create New Folder" },
    "folder_name": { "th": "ตั้งชื่อแฟ้มเอกสาร", "en": "Name a folder" },
    "folder_add": { "th": "เพิ่มแฟ้มเอกสาร", "en": "Add folder" },
    "edit_folder": { "th": "แก้ไขแฟ้มเอกสาร", "en": "Edit Folder" },
    "logout": { "th": "ออกจากระบบ", "en": "Logout" },
    "title": { "th": "จัดการเอกสาร", "en": "Manage Document" },
    "trash": { "th": "ถังขยะ", "en": "Trash" },
    "inbox_title_not_sign": { "th": "เอกสารที่รอการลงนาม", "en": "Incomplete Signing" },
    "des_search": { "th": "ชื่อเอกสาร, ผู้ส่ง, อีเมล", "en": "Subject, Sender, Email" },
}
