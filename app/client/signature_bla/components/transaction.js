Pages.Tran = {
    _template: 'transaction.html',
    template: '<div>this is transaction.html</div>',
    data: function () {
        return {
            display: false,
            list_document: [],
            list_document2: [],
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_transection,
        }
    },
    mounted: function () {
        window.myApp = this
        this.get_list_document()
    },
    filters: {
        date(value) {
            value = value || ''
            var date = new Date(value)
            var day = date.getDate()
            var month = date.getMonth() + 1
            var year = date.getFullYear()
            value = day + "/" + month + "/" + year
            return value || ""
        },
        time(value) {
            value = value || ''
            var date = new Date(value)
            var horus = date.getHours()
            var minutes = date.getMinutes()
            var seconds = date.getSeconds()
            value = horus + ":" + minutes + ":" + seconds
            return value || ""
        }
    },
    methods: {
        get_list_document: function () {
            var that = this
            // var data = { "company_id": "", "email": getUsername() || "N/A", "cat": 'self' };

            // $.ajax({
            //     type: "POST",
            //     url: api_host + "list_document",
            //     data: JSON.stringify(data),
            //     dataType: "json",
            //     success: function (resp) {

            //         if (resp.success) {
            //             if (!resp.data.length) resp.data = []
            //             var list = resp.data

            //             if (list.length > 0) {
            //                 list.sort(function (a, b) {
            //                     return b.createdDtm - a.createdDtm
            //                 })

            //                 for(i=0;i < list.length;i++) {
            //                     list[i].diff_date = that.cal_date(list[i].createdDtm)
            //                 }

            //                 that.list_document = list
            //                 that.list_document2 = list
            //                 that.set_table()
            //             }
            //             else {
            that.set_table()
            //             }
            //         }
            //     },
            //     error: function (request, status, error) {
            //         alert("|" + status + ' |' + error);
            //     }
            // });
        },
        set_table: function () {
            var that = this
            setTimeout(function () {
                table = $('#table').DataTable({
                    paging: true,
                    searching: false,
                    ordering: false,
                    info: false,
                    lengthChange: true,
                    responsive: true,
                    lengthMenu: [[20, 50, 100, -1], [20, 50, 100, "ทั้งหมด"]],
                    dom: '<"top"i>rt<"bottom"flp><"clear">',
                    language: {
                        lengthMenu: "_MENU_",
                    }
                });
            }, 100);
            that.display = true
        },
        show_all_table: function () {
            table.destroy()
        },
    }
}

routes.push({ path: '/transaction', component: Pages.Tran })

var lang_transection = {
    "date": { "th": "วัน-เวลาที่ทำรายการ", "en": "Transaction’s Date & Time" },
    "name": { "th": "ชื่อรายการ", "en": "Transaction’s Name" },
    "amount": { "th": "จำนวนเงิน", "en": "Amount" },
    "id": { "th": "รหัสคำสั่งซื้อ", "en": "Order ID" },
    "status": { "th": "สถานะ", "en": "Status" },
    "paid": { "th": "ชำระเงินแล้ว", "en": "PAID" },
    "voided": { "th": "ยกเลิกรายการ", "en": "VOIDED" },
    "title": { "th": "ประวัติการทำรายการ", "en": "Transection History" },
    "wait": { "th": "กรุณารอสักครู่", "en": "Please wait.." },

}
