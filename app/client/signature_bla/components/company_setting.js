Pages.comSet = {
    _template: 'company_setting.html',
    template: '<div>this is company_setting.html</div>',
    data: function () {
        return {
            company_id: getUserField('company_id') || "N/A",
            company_detail: [],
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_companysetting,
            myRole: "USER",
        }
    },
    mounted: function () {
        window.myApp = this
        this.init_dropzone_logo()
        this.get_users()
        this.get_company_detail()
    },
    methods: {
        init_dropzone_logo: function () {
            var that = this
            var dropzone = new Dropzone(document.getElementById('file-dropzone-upload-logo'), {
                url: api_host + 'upload',
                maxFilesize: 0.025, // 25KB
                clickable: true,
                acceptedFiles: ".jpg, .jpeg, .png",
                parallelUploads: 1
            });

            dropzone.on("success", function (file, json) {
                var res = JSON.parse(json)
                that.company_detail.companyLogo = res.filename;
            });
            dropzone.on("error", function () {
                alert('Upload Error file size must be less than 25KB')
            });
        },
        save_detail_company: function () {
            var that = this

            var data = { company_detail: that.company_detail }
            //console.log(data);
            $.ajax({
                type: "POST",
                url: api_host + "edit_company_detail",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                    if (resp.success) {
                        // alert('บันทึกสำเร็จ')
                        // that.get_company_detail()
                        document.location.reload()
                    }
                    else {
                        alert("บันทึกไม่สำเร็จ")
                    }
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
        get_company_detail: function () {
            var that = this
            var data = { company_id: that.company_id }

            $.ajax({
                type: "POST",
                url: api_host + "get_company_detail",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        var d = resp.data
                        d.companyName = d.companyName || ""
                        d.companyTaxNo = d.companyTaxNo || ""
                        d.companyBranch = d.companyBranch || ""
                        d.companyAddress = d.companyAddress || ""
                        d.companyLogo = d.companyLogo || ""

                        that.company_detail = d
                    }
                    else {
                        alert("ค้นหาบริษัทไม่พบ")
                    }
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
        cancel_edit_company: function () {
            var str = "คุณยืนยันที่จะยกเลิกการแก้ไขหรือไม่"
            if (confirm(str)) {
                document.location.reload()
            }
        },
        remove_logo_company: function() {
            var that = this
            that.company_detail.companyLogo = ""
        },
        get_users: function () {
            var that = this
            var get_company_id = getUserField('company_id')
            var data = {
                company_id: getUserField('company_id') || "N/A",
                mode: 'get'
            }
            $.ajax({
                type: "POST",
                url: api_host + "get_company_member",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        // var r = resp.data

                        // for (i = 0; i < r.length; i++) {
                        //     if (!r[i].group || !r[i].group.length) r[i].group = []
                        // }
                        // that.members = r
                        that.myRole = resp.myRole || "USER"
                    }
                    else {
                        // that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
                        // display_alert("er_alert", "block", "none", "alert-danger")
                    }

                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
    }
}

routes.push({ path: '/companySetting', component: Pages.comSet })

var lang_companysetting = {
    "title": { "th": "ตั้งค่าข้อมูลบริษัท", "en": "Company Settings" },
    "save": { "th": "บันทึก", "en": "Save" },
    "cancel": { "th": "ยกเลิก", "en": "Cancel" },
    "remove": { "th": "ลบ", "en": "Remove" },
    "detail_company": { "th": "รายละเอียดบัญชีองค์กรของคุณ", "en": "Company Details" },
    "com_name": { "th": "ชื่อบริษัท", "en": "Company name" },
    "com_id": { "th": "เลขที่นิติบุคคล", "en": "Juristic ID" },
    "location": { "th": "สำนักงานใหญ่/สาขาเลขที่", "en": "Head office / Branch number" },
    "address": { "th": "ที่อยู่สำนักงาน", "en": "Office address" },
    "com_set": { "th": "ตั้งค่ารูปแบบเว็บไซต์", "en": "Theme Settings" },
    "logo_set": { "th": "ตั้งค่าโลโก้", "en": "Logo Settings" },
    "upload_img": { "th": "อัปโหลดรูปภาพ", "en": "Upload picture" },
    "sup_file": { "th": "รองรับไฟล์ (.png, .jpg) ขนาดไฟล์สูงสุดไม่เกิน 25 KB", "en": "File support (.png, .jpg), file size must be less than 25 KB" },
}
