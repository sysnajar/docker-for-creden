$(document).ready(function () {
    app = init_vue()
});
function swithLang(lang) {
    localStorage.setItem('lang_choose', lang);
    window.myApp.lang_choose = lang
    // reloadVuePage()    
}
function init_vue() {
    return new Vue({
        el: '#app',
        data:{
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            myProfile: '',
            myFirstname: getUserField("adminFisrtname") || "",
            myLastname: getUserField("adminLastname") || "",
            myEmail: getUserField("adminEmail") || "",
            lang_txt: lang_book,
            mode: "walkin",
            t_morning: [],
            t_afternoon: [],
            selected_date: "",
            selected_time: "",
            name_address: "",
            lat: "",
            lng: "",
            phone_number:getUserField("comTel") || "",
            to_day:'',
            exp_month:'1',
            
        },
        mounted: function () {
            window.myApp = this
            this.myProfile = this.getProfile()
            
            $('#datepicker').datepicker({
                dateFormat: "dd/mm/yy",
                minDate: '0D',
                maxDate: '+30D',
            });
            initAutocomplete()
            this.selected_date = $('#datepicker').val()
            this.to_day = $('#datepicker').val()
            this.get_datetime_slot()
            this.get_time_now()
        },
        filters: {
            fomat_time(v){
                var res = v.replace(":", ".");
                return res
            },
        },
        methods:{
            getProfile: function () {
                var p = getUserField("profile_pic") || ""
                var f = p.search("http")
                var img = ""
    
                if (f == 0) img = p
                else if (f == -1 && p != "") img = "../face/images/card/" + getUserField("profile_pic") + ".png"
                else img = "images/profile.png"
                return img
            },
            save_datetime_slot: function() {
                var that = this
                var p =""
                if( parseInt(that.selected_time) > 12 ){
                    p = "afternoon"
                }else{
                    p = "morning"
                }
                var data = {
                    mode: that.mode,
                    selected_date: that.selected_date,
                    selected_time: that.selected_time,
                    lat: that.lat,
                    lng: that.lng,
                    name_address: "",
                    period:p,
                    phone_number:that.phone_number,
                    email:that.myEmail,
                    exp_month:that.exp_month,
                    // name_address: $('#autocomplete').val() || "",
                    
                }


                // if(data.selected_date == "") {
                //     alert("กรุณาเลือกวันที่ต้องการจอง")
                //     return false
                // }

                // if(data.selected_time == "") {
                //     alert("กรุณาเลือกเวลาต้องการจอง")
                //     return false
                // }

                // var str = "คุณยืนยันที่จะทำการจองเพื่อสมัครใบรับรองอิเล็กทรอนิกส์ หรือไม่"
                // if(confirm(str)){
                //     alert("success")
                // }
                
                //console.log(data)
                $.ajax({
                    type: "POST",
                    url: document.location.origin + "/capi/save_datetime_slot",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        if(res.success){
                            location.href = document.location.origin + '/bla/summary.html?id=' + res.booking_id
                        }
                        
                    },
                    error: function (request, status, error) {
                        //console.log("|" + status + ' |' + error);
                    }
                });

            },
            get_datetime_slot: function () {
                var that = this
                
                var data = {
                    date: that.selected_date,
                    // date: "25/02/2021",
                    mode: that.mode,
                }
                var d = new Date(),
                    h = (d.getHours()<10?'0':'') + d.getHours(),
                    m = (d.getMinutes()<10?'0':'') + d.getMinutes();
                var time_now = parseFloat(h + '.' + m)
                $.ajax({
                    type: "POST",
                    url: document.location.origin + "/capi/get_datetime_slot",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {

                        if (res.success) {
                            var dt_slot = res.data
                            if (!dt_slot.morning.length) dt_slot.morning = []
                            if (!dt_slot.afternoon.length) dt_slot.afternoon = []
                            that.t_morning = dt_slot.morning
                            that.t_afternoon = dt_slot.afternoon
                            // that.selected_date = $('#datepicker').val()
                            for(i=0; i<that.t_morning.length; i++){
                                var a = that.$options.filters.fomat_time(that.t_morning[i].slot.time)
                                a = parseFloat(a)
                                if(a<time_now && that.to_day == that.selected_date){
                                    that.t_morning[i].not_select = true
                                }else{
                                    that.t_morning[i].not_select = false
                                }
                                
                            }
                            for(i=0; i<that.t_afternoon.length; i++){
                                var a = that.$options.filters.fomat_time(that.t_afternoon[i].slot.time)
                                a = parseFloat(a)
                                if(a<time_now && that.to_day == that.selected_date){
                                    that.t_afternoon[i].not_select = true
                                }else{
                                    that.t_afternoon[i].not_select = false
                                }
                                
                            }
                            
                        }
                        else {
                            //console.log(res.error_msg);
                        }
                    },
                    error: function (request, status, error) {
                        //console.log("|" + status + ' |' + error);
                    }
                });
            },
            choose_mode: function (mode) {
                var that = this
                that.mode = mode
                that.get_datetime_slot()
            },
            choose_date: function (date) {
                var that = this
                that.selected_date = date
                that.get_datetime_slot()

            },
            get_time_now:function(){
                var d = new Date(),
                    h = (d.getHours()<10?'0':'') + d.getHours(),
                    m = (d.getMinutes()<10?'0':'') + d.getMinutes();
                //console.log(h + '.' + m);

            }
            
        }
    })
    // initMap()
   
    $(".date").datepicker({
        onSelect: function (dateText) {
            //console.log("Selected date: " + dateText + "; input's current value: " + this.value);
        }
    })
}
function initAutocomplete() {
    
    const input = document.getElementById("autocomplete");
    autocomplete = new google.maps.places.Autocomplete(input);
    autocomplete.setFields(["address_component", 'geometry', 'icon', 'name']);
    autocomplete.addListener("place_changed", fillInAddress);

    navigator.geolocation.getCurrentPosition(
        (position) => {
            const pos = {
                lat: position.coords.latitude,
                lng: position.coords.longitude,
            };
            //console.log("lat : " + position.coords.latitude)
            //console.log("lng : " + position.coords.longitude)
            initMap(position.coords.latitude, position.coords.longitude)
        },
        () => {
            handleLocationError(true, infoWindow, map.getCenter());
        }
    );
}
function fillInAddress() {
    const place = autocomplete.getPlace();
    //console.log(place)
    // //console.log(autocomplete.getPlace().geometry)
    var lat = place.geometry.location.lat()
    var lng = place.geometry.location.lng()
    // //console.log("lat : " + lat)
    // //console.log("lng : " + lng)
    initMap(lat, lng)
}
function initMap(lat, lng) {
    app.lat = lat
    app.lng = lng
    const myLatLng = { lat: lat, lng: lng };
    const map = new google.maps.Map(document.getElementById("map"), {
        center: myLatLng,
        zoom: 17,
        draggable: false,
        zoomControl: true,
        scrollwheel: false,
        fullscreenControl: false,
        streetViewControl: false,
        mapTypeControl: false,
    });
    new google.maps.Marker({
        position: myLatLng,
        map,
        title: "Hello!",
    });
}
var lang_book ={
    "Reservations": { "th": "การจองเพื่อสมัครใบรับรองอิเล็กทรอนิกส์", "en": "Reservations to issue digital certificates" },
    "Booking": { "th": "เลือกวันที่ต้องการจอง", "en": "Choose Booking Date" },
    "Morning": { "th": "ช่วงเช้า", "en": "Morning" },
    "Contact": { "th": "เบอร์ติดต่อ", "en": "Contact number" },
    "Location": { "th": "สถานที่ ที่ให้ Messenger ไปดำเนินการ", "en": "Booking Location" },
    "Afternoon": { "th": "ช่วงบ่าย", "en": "Afternoon" },
    "Available": { "th": "ว่าง", "en": "Available" },
    "Summary": { "th": "สรุปข้อมูล", "en": "Summary of Booking" },
    "mont1": { "th": "ใช้งานได้ 1 เดือน", "en": "1-Month Validity" },
    "mont12": { "th": "ใช้งานได้ 12 เดือน", "en": "12-Month Validity" },
    "validity": { "th": "อายุการใช้งานใบรับรองอิเล็กทรอนิกส์", "en": "Electronic certificate validity" },
   
}