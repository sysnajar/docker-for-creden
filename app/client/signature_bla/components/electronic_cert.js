Pages.ElectronicCert = {
    _template: 'electronic_cert.html',
    template: '<div>this is electronic_cert.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_cer,

        }
    },
    mounted: function () {
        window.myApp = this
    },
    methods: {
        
    }
}

routes.push({ path: '/digital_cert', component: Pages.ElectronicCert })

var lang_cer= {
    "digital_cert": { "th": "ใบรับรองอิเล็กทรอนิกส์", "en": "Digital Certificate" },
    "detail1": { "th": "ใบรับรองที่มีข้อมูลบ่งบอกถึงบุคคลหรือหน่วยงานใดๆ ว่าเป็นเจ้าของ Public key หนึ่งๆ ซึ่งข้อมูลดังกล่าวได้รับการรับรองโดยหน่วยงานผู้ออกใบรับรองที่เรียกว่า Certificate authority ข้อมูลที่สำคัญภายในใบรับรองประกอบด้วย ชื่อหน่วยงานผู้ออกใบรับรอง, ชื่อบุคคลหรือหน่วยงานที่ได้รับการรับการรับรอง, วันหมดอายุของใบรับรอง, Public key และ Digital signature ที่เซ็นกำกับใบรับรอง Digital Certificate สามารถนำไปใช้ในการทำธุรกรรมทางอิเล็กทรอนิกส์ระหว่างผู้อื่นอย่างมั่นคง ปลอดภัย ด้วยการใช้ Public key จาก Digital Certificate ในการถอดรหัสลับข้อมูล ผู้ใช้สามารถตรวจสอบข้อมูลว่ามาจากบุคคลที่ได้รับการรับรองจริง",
              "en": "A digital certificate containing information that identifies the owner of a public key, which is certified by a certificate authority known as the certificate authority. The information within the digital certificate includes, The name of the certification authority, the name of the accredited individual, the expiration date of the certificate, the public key, and the digital signature signed Digital Certificate can be used for electronic transactions with others. Secure by using a public key from Digital Certificate to decrypt the information. Users can verify that the information is actually from a certified person." },
    "how_to": { "th": "วิธีสมัคร ใบรับรองอิเล็กทรอนิกส์ ของครีเดน สามารถทำได้ 2 วิธี", "en": "There are two methods to enroll for Creden Electronic Certificate." },
    "booking": { "th": "ลูกค้าจะต้องทำการนัดหมาย ล่วงหน้า 1-2 วัน โดยระบุวันเวลานัดหมายได้ที่ระบบ ของเรา จากนั้นจะมีเจ้าหน้าที่โทรนัดอีกครั้งครั้ง", "en": "Booking must be made at least one or two days before selected time slot. You can select date and time in a booking calendar on Creden website. After complete booking, our admin will call to confirm an operation" },
    "walk_in": { "th": "1. สามารถเข้ามาติดต่อได้ที่ออฟฟิศ ครีเดน เอเชีย (Walk-in)", "en": "Scheduled and Enrolled for Digital Certificate at Creden office" },
    "address": { "th": "ลูกค้าสามารถ Walk in เข้ามาได้ที่ออฟฟิสของเราได้ที เลขที่ 90 อาคารซีดับเบิ้ลยู ทาวเวอร์ อาคารเอ ชั้น 31 ยูนิต เอ3103-เอ3105 ถนนรัชดาภิเษก แขวงห้วยขวาง เขตห้วยขวาง กรุงเทพฯ 10310 เมื่อมาถึงแล้ว แจ้งเคาร์เตอร์ ด้านล่างว่า ชั้น 31 ตลาด ดอท คอม", 
              "en": "Make an appointment to do the transaction at Creden. Office located at 90 CW Tower Building A, 31st Floor, Unit A3103-A 3105. Ratchadaphisek Road, Huai Khwang Subdistrict, Huai Khwang District, Bangkok 10310. When you arrive at CW Tower, please ask the receptionist that you have come to TARAD.com at 31th floor." },
    "checklist": { "th": "สิ่งที่ต้องเตรียมมา", "en": "Prerequisite Checklist" },
    "identity_card": { "th": "- บัตรประชาชน", "en": "National Identity Card" },
    "service50": { "th": "- เงินค่าบริการ", "en": "Service Fee" },
    "on_site": { "th": "2. เรียก Messenger ไปรับเอกสารถึงที่ของลูกค้า  (เร็วๆ นี้)", "en": "2. On-site service (Coming Soon)" },
    "appointment": { "th": "นัดหมาย", "en": "Appointment" },


}