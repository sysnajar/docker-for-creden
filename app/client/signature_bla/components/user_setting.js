Pages.UserSet = {
    _template: 'user_setting.html',
    template: '<div>this is user_setting.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_member,
            lang_err: "",
            members: [],
            select_role: "",
            add_member_email: "",
            chk_btn_add_user: true,
            count_edoc: 0,
            count_ekyc: 0,
            count_edoc_all: 0,
            myEmail: getUserField("adminEmail") || "",
            myRole: "USER",
            myEmail: getUsername() || "N/A",
            init_date: "",
            startdate1: "",
            enddate1: "",
            // Group
            id_group: "",
            name_group: "",
            select_member_group: "",
            push_member_group: [],
            group: [],
            btn_group: "add",

            isShowFristTable: true,
            dataTable: null,
            dataTable2: null,

            userList: [],
            s_date:"",
            n_date:"",
            member_row_count: 0,
            user_list_row_count: 0,
        }
    },
    mounted: function () {
        window.myApp = this
        this.get_users()
        // this.get_group()
        this.get_dashboard_setting()
        this.get_list_user()
        this.set_date()
        // setTimeout(function(){
        //     $("#startdate").datepicker({
        //         todayBtn:  1,
        //         autoclose: true,
        //     }).on('changeDate', function (selected) {
        //         var minDate = new Date(selected.date.valueOf());
        //         $('#enddate').datepicker('setStartDate', minDate);
        //     });

        //     $("#enddate").datepicker()
        //         .on('changeDate', function (selected) {
        //             var minDate = new Date(selected.date.valueOf());
        //             $('#startdate').datepicker('setEndDate', minDate);
        //         });
        // },1000)


    },
    
    methods: {
        get_company_title: function () {
            var that = this
            var data = { company_id: getUserField('company_id') }
            $.ajax({
                type: "POST",
                url: document.location.origin + "/capi/get_company_title",
                data: data,
                dataType: "json",
                success: function (res) {
                    if (res.success) {
                        that.positions = res.data;
                    } else {
                        //console.log(res.error_msg);
                    }
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
        format_date: function (e) {
            value = e.value || ''
            var date = new Date(value)
            var day = date.getDate()
            if (Number(day) <= 9) day = "0" + day
            var month = date.getMonth() + 1
            if (Number(month) <= 9) month = "0" + month
            var year = date.getFullYear()
            value = day + "/" + month + "/" + year
            e.setAttribute( "data-date-format", value)
        },
        set_date: function () {
            const init_date = new Date()
            const s_ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(init_date)
            const s_mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(init_date)
            const s_da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(init_date)

            this.startdate1 = `${s_ye}-${s_mo}-${s_da}`
            this.enddate1 = `${s_ye}-${s_mo}-${s_da}`
            this.init_date = `${s_da}/${s_mo}/${s_ye}`
            // this.enddate1_format = `${s_da}/${s_mo}/${s_ye}`
        },
        filter_date: function () {
            
            var that = this
            var startdate = that.startdate1
            var enddate = that.enddate1
            if (startdate != "" && enddate != "") {
                
                const start_d = new Date(startdate)
                const s_ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(start_d)
                const s_mo = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(start_d)
                const s_da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(start_d)
    
                const end_d = new Date(enddate)
                const n_ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(end_d)
                const n_mo = new Intl.DateTimeFormat('en', { month: 'numeric' }).format(end_d)
                const n_da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(end_d)
    
                that.s_date = `${s_da}-${s_mo}-${s_ye}`
                that.n_date = `${n_da}-${n_mo}-${n_ye}`
                // 2021-07-01
                s_yyyy = startdate.substring(0, 4)
                s_mm = startdate.substring(5, 7)
                s_dd = startdate.substring(8, 10)

                e_yyyy = enddate.substring(0, 4)
                e_mm = enddate.substring(5, 7)
                e_dd = enddate.substring(8, 10)

                full_startdate = s_dd + '-' + s_mm + '-' + s_yyyy
                full_enddate = e_dd + '-' + e_mm + '-' + e_yyyy
                // that.startdate1 = full_startdate
                // that.enddate1 = full_enddate
            
                var data = { startdate: full_startdate, enddate: full_enddate }

                $.ajax({
                    type: "POST",
                    url: document.location.origin + "/capi/dashboard_userSetting_countx",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        // //console.log(res)
                        that.count_edoc = res.count_edoc
                        that.count_edoc_all = res.count_edoc_all
                        that.count_ekyc = res.count_ekyc

                    },
                    error: function (request, status, error) {
                        //console.log("|" + status + ' |' + error);
                    }
                });

            }
            // that.s_date = startdate
            // that.n_date = enddate


        },
        get_dashboard_setting: function () {
            var that = this
            var data = { company_id: getUserField('company_id') }
            $.ajax({
                type: "POST",
                url: document.location.origin + "/capi/dashboard_userSetting_countx",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    //console.log(res)
                    that.count_edoc = res.count_edoc
                    that.count_edoc_all = res.count_edoc_all
                    that.count_ekyc = res.count_ekyc
                    // if (res.success) {
                    //     that.positions = res.data;
                    // } else {
                    //     //console.log(res.error_msg);
                    // }
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
        toggle_role_edit: function (e, r) {
            var selectElem = $('#select-' + r._id);
            var wrapperElem = $(e).closest('.edit-wrapper');
            if (wrapperElem.hasClass('edit-hidden')) {
                $('#select-control-' + r._id).prepend(selectElem);
                wrapperElem.addClass('edit-visible')
                wrapperElem.removeClass('edit-hidden')
            } else {
                $('#role-select-wrapper').prepend(selectElem);
                wrapperElem.addClass('edit-hidden')
                wrapperElem.removeClass('edit-visible')
            }
        },
        // add_user: function () {
        //     var that = this
        //     that.add_member_email = that.add_member_email.trim()

        //     if (that.myRole != "ADMIN") {
        //         that.lang_err = err_c.c_2005[that.lang_choose]
        //         display_alert("er_alert", "block", "none", "alert-danger")
        //         return false
        //     }

        //     if (that.add_member_email == "" || !validateEmail(that.add_member_email)) {
        //         that.lang_err = err_c.c_2001[that.lang_choose]
        //         display_alert("er_alert", "block", "none", "alert-danger")
        //         return false
        //     }
        //     that.chk_btn_add_user = false
        //     var data = {
        //         email: that.add_member_email,
        //         company_id: getUserField('company_id') || "N/A",
        //         create_by: getUsername(),
        //         mode: 'add'
        //     }

        //     $.ajax({
        //         type: "POST",
        //         url: api_host + "add_company_member_bla",
        //         data: JSON.stringify(data),
        //         dataType: "json",
        //         success: function (resp) {

        //             if (resp.success) {
        //                 that.add_member_email = ""
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("er_alert", "block", "none", "alert-success")
        //                 that.get_users()
        //             }
        //             else {
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("er_alert", "block", "none", "alert-danger")
        //             }
        //             that.chk_btn_add_user = true
        //         },
        //         error: function (request, status, error) {
        //             //console.log("|" + status + ' |' + error);
        //         }
        //     });
        // },
        edit_user: function (member, elem) {
            var that = this
            //console.log(member.role)
            //console.log(elem.value)
            if (member.role == elem.value) {
                that.lang_err = err_c.c_2002[that.lang_choose]
                display_alert("er_alert", "block", "none", "alert-danger")
                return false
            }

            var data = {
                id: member._id,
                role: elem.value,
            }

            $.ajax({
                type: "POST",
                url: api_host + "edit_user_role_bla",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
                        display_alert("er_alert", "block", "none", "alert-success")
                        that.get_users()
                        that.toggle_role_edit(elem, member)
                    }
                    else {
                        that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
                        display_alert("er_alert", "block", "none", "alert-danger")
                    }
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
        // delete_user: function (member) {
        //     var that = this

        //     Swal.fire({
        //         title: "คุณยืนยันที่จะลบ " + member.email + " หรือไม่",
        //         showDenyButton: true,
        //         confirmButtonText: "ยืนยัน",
        //         denyButtonText: "ยกเลิก",
        //     }).then((result) => {
        //         if (result.isConfirmed) {

        //             var data = {
        //                 id: member._id,
        //             }

        //             $.ajax({
        //                 type: "POST",
        //                 url: api_host + "remove_company_member",
        //                 data: JSON.stringify(data),
        //                 dataType: "json",
        //                 success: function (resp) {

        //                     if (resp.success) {
        //                         that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                         display_alert("er_alert", "block", "none", "alert-success")
        //                         that.get_users()
        //                         // that.get_group()
        //                     }
        //                     else {
        //                         that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                         display_alert("er_alert", "block", "none", "alert-danger")
        //                     }
        //                 },
        //                 error: function (request, status, error) {
        //                     //console.log("|" + status + ' |' + error);
        //                 }
        //             });
        //         }
        //         else if (result.isDenied) {
        //             //console.log("cancel remove member")
        //         }
        //     })
        // },
        get_list_user: function () {
            var that = this
            var get_company_id = getUserField('company_id')
            var data = {
                company_id: getUserField('company_id') || "N/A",
                mode: 'get'
            }
            $.ajax({
                type: "POST",
                url: api_host + "address_book2",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                    if (resp.IsSuccess) {
                        var r = resp.Data
                        that.userList = r
                    } else {
                        //console.log("error")
                    }
                    that.$nextTick(function () {
                        $('#ListName').DataTable({
                            "oLanguage": {"sSearch": that.lang_txt.search_text[that.lang_choose]},
                            // "drawCallback": function() {
                            //     $('#memberTable_length').remove()
                            // }
                        })
                    })
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });

        },
        get_users: function () {
            var that = this
            var get_company_id = getUserField('company_id')
            var data = {
                company_id: getUserField('company_id') || "N/A",
                mode: 'get'
            }
            $.ajax({
                type: "POST",
                url: api_host + "get_company_users_bla",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        var r = resp.data

                        for (i = 0; i < r.length; i++) {
                            // if (!r[i].group || !r[i].group.length) r[i].group = []
                            r[i].role_selected = r[i].role
                        }
                        that.members = r
                        that.myRole = resp.myRole || "USER"
                    }
                    else {
                        that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
                        display_alert("er_alert", "block", "none", "alert-danger")
                    }
                    that.$nextTick(function () {
                        $('#memberTable').DataTable({
                            "oLanguage": {"sSearch": that.lang_txt.search_text[that.lang_choose]}
                        })
                    })
					// alert( "memberTable >>> " + document.getElementById('memberTable') + ' data = ' + r)
                    // $('#memberTable').DataTable()
                },
                error: function (request, status, error) {
                    //console.log("|" + status + ' |' + error);
                }
            });
        },
        // get_group: function () {
        //     var that = this
        //     var data = {
        //         company_id: getUserField('company_id') || "N/A"
        //     }

        //     $.ajax({
        //         type: "POST",
        //         url: api_host + "get_group_sign",
        //         data: JSON.stringify(data),
        //         dataType: "json",
        //         success: function (resp) {

        //             if (resp.success) {
        //                 var r = resp.data;
        //                 if (!r.length) r = []
        //                 that.group = r
        //             }
        //             else {
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("err_alert_group", "block", "none", "alert-danger")
        //             }

        //         },
        //         error: function (request, status, error) {
        //             //console.log("|" + status + ' |' + error);
        //         }
        //     });
        // },
        // del_group: function (arr) {
        //     var that = this

        //     Swal.fire({
        //         title: "คุณยืนยันที่ต้องการลบกลุ่ม " + arr.group_name + " หรือไม่",
        //         showDenyButton: true,
        //         confirmButtonText: "ยืนยัน",
        //         denyButtonText: "ยกเลิก",
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             var get_company_id = getUserField('company_id')
        //             var data = {
        //                 company_id: getUserField('company_id') || "N/A",
        //                 id: arr._id,
        //                 group_name: arr.group_name
        //             }

        //             $.ajax({
        //                 type: "POST",
        //                 url: api_host + "remove_group_sign",
        //                 data: JSON.stringify(data),
        //                 dataType: "json",
        //                 success: function (resp) {

        //                     if (resp.success) {
        //                         that.get_group()
        //                         that.get_users()
        //                         that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                         display_alert("err_alert_group", "block", "none", "alert-success")
        //                     }
        //                     else {
        //                         that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                         display_alert("err_alert_group", "block", "none", "alert-danger")
        //                     }

        //                 },
        //                 error: function (request, status, error) {
        //                     //console.log("|" + status + ' |' + error);
        //                 }
        //             });
        //         }
        //         else if (result.isDenied) {
        //             //console.log("cancel remove group sign")
        //         }
        //     })
        // },
        // btn_add_group: function () {
        //     var that = this
        //     that.name_group = ""
        //     that.select_member_group = ""
        //     that.push_member_group = []
        //     that.btn_group = "add"
        // },
        // btn_edit_group: function (arr) {
        //     var that = this
        //     that.id_group = arr._id
        //     that.name_group = arr.group_name || ""
        //     that.push_member_group = arr.group_sign || []
        //     that.btn_group = "edit"
        // },
        // remove_list_group: function (index) {
        //     var that = this
        //     that.push_member_group.splice(index, 1);
        // },
        // add_list_group: function (index) {
        //     var that = this
        //     var g = that.members[index]
        //     var add = {
        //         email: g.email,
        //         name: g.fName + " " + g.lName,
        //     }
        //     if (!that.push_member_group.length) that.push_member_group = []
        //     var find_user_group_sign = that.push_member_group.filter(function (a) {
        //         return a.email == g.email
        //     })

        //     if (find_user_group_sign.length > 0) {
        //         that.lang_err = err_c.c_2004[that.lang_choose]
        //         display_alert("err_alert_group_modal", "block", "none", "alert-danger")
        //         return false
        //     }

        //     that.push_member_group.push(add)
        // },
        // save_group: function () {
        //     var that = this
        //     that.name_group = that.name_group.trim()

        //     if (that.name_group == "") {
        //         that.lang_err = err_c.c_2003[that.lang_choose]
        //         display_alert("err_alert_group_modal", "block", "none", "alert-danger")
        //         return false
        //     }

        //     var data = {
        //         company_id: getUserField('company_id'),
        //         group_name: that.name_group,
        //         group_sign: that.push_member_group,
        //     }

        //     $.ajax({
        //         type: "POST",
        //         url: api_host + "add_group_sign",
        //         data: JSON.stringify(data),
        //         dataType: "json",
        //         success: function (resp) {

        //             if (resp.success) {
        //                 that.get_group()
        //                 that.btn_add_group()
        //                 that.get_users()
        //                 $("#btn_group_close").click()
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("err_alert_group", "block", "none", "alert-success")
        //             }
        //             else {
        //                 $("#btn_group_close").click()
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("err_alert_group", "block", "none", "alert-danger")
        //             }
        //         },
        //         error: function (request, status, error) {
        //             //console.log("|" + status + ' |' + error);
        //         }
        //     });

        // },
        // edit_group_sign: function () {
        //     var that = this
        //     that.name_group = that.name_group.trim()

        //     if (that.name_group == "") {
        //         display_alert("er_group_add_name", "block", "none")
        //         return false
        //     }

        //     var data = {
        //         id: that.id_group,
        //         company_id: getUserField('company_id'),
        //         group_name: that.name_group,
        //         group_sign: that.push_member_group,
        //     }

        //     $.ajax({
        //         type: "POST",
        //         url: api_host + "edit_group_sign",
        //         data: JSON.stringify(data),
        //         dataType: "json",
        //         success: function (resp) {

        //             if (resp.success) {
        //                 that.get_group()
        //                 that.btn_add_group()
        //                 that.get_users()
        //                 $("#btn_group_close").click()
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("err_alert_group", "block", "none", "alert-success")
        //             }
        //             else {
        //                 $("#btn_group_close").click()
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("err_alert_group", "block", "none", "alert-danger")
        //             }
        //         },
        //         error: function (request, status, error) {
        //             //console.log("|" + status + ' |' + error);
        //         }
        //     });
        // },
        // resend_invite_user: function (email) {
        //     var that = this
        //     var data = { email: email }

        //     $.ajax({
        //         type: "POST",
        //         url: api_host + "resend_invite_company",
        //         data: JSON.stringify(data),
        //         dataType: "json",
        //         success: function (resp) {

        //             if (resp.success) {
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("er_alert", "block", "none", "alert-success")
        //             }
        //             else {
        //                 that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                 display_alert("er_alert", "block", "none", "alert-danger")
        //             }
        //         },
        //         error: function (request, status, error) {
        //             //console.log("|" + status + ' |' + error);
        //         }
        //     });

        // },
        // quit_company: function () {
        //     var that = this

        //     Swal.fire({
        //         title: "<span>คุณต้องการ <label class='cl-F0'>ถอนตัว</label> ออกจากบริษัท?</span>",
        //         text: "หากคุณถอนตัว เอกสารที่คุณทำหรือได้รับหลังจากเข้าร่วมบัญชีประเภทบริษัทเอกสารดังกล่าวจะถือว่าเป็นเอกสารของบริษัทหากออกจากกลุ่มบัญชีประเภทบริษัทไปแล้ว เอกสารจะไม่แสดงให้คุณเห็น",
        //         icon: 'warning',
        //         showDenyButton: true,
        //         confirmButtonText: "ยืนยัน",
        //         denyButtonText: "ยกเลิก",
        //     }).then((result) => {
        //         if (result.isConfirmed) {
        //             var data = {
        //                 email: getUsername() || "N/A"
        //             }

        //             $.ajax({
        //                 type: "POST",
        //                 url: api_host + "quit_company_member",
        //                 data: JSON.stringify(data),
        //                 dataType: "json",
        //                 success: function (resp) {

        //                     if (resp.success) {
        //                         that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                         display_alert("er_alert", "block", "none", "alert-success")
        //                         setTimeout(function () { document.location.reload() }, 3000);
        //                     }
        //                     else {
        //                         that.lang_err = err_s["s_" + resp.error_code][that.lang_choose]
        //                         display_alert("er_alert", "block", "none", "alert-danger")
        //                     }
        //                 },
        //                 error: function (request, status, error) {
        //                     //console.log("|" + status + ' |' + error);
        //                 }
        //             });
        //         }
        //         else if (result.isDenied) {
        //             //console.log("cancel quit company")
        //         }
        //     })
        // },
        // displayTable: function (id) {
        //     if (id == 'memberTable') {
        //         document.getElementById(memberTable).style.display = 'block';
        //         document.getElementById(ListName).style.display = 'none';
        //     }
        //     else {
        //         document.getElementById(memberTable).style.display = 'none';
        //         document.getElementById(ListName).style.display = 'blockblock';
        //     }
        // },
        // t_onover: function (id) {
        //     var selected = document.getElementById(id)
        //     var tooltip = new bootstrap.Tooltip(selected)
        //     tooltip.show()
        // }
    }
}

routes.push({ path: '/userSetting', component: Pages.UserSet })

var lang_member = {
    "user_set": { "th": "ตั้งค่าผู้ใช้งานในระบบ", "en": "User Settings" },
    "add_mail": { "th": "อีเมลที่ต้องการเพิ่มเข้า", "en": "Add email address" },
    "com_user": { "th": "สมาชิกในบริษัท", "en": "Company Users" },
    "name": { "th": "ชื่อ", "en": "Name" },
    "email": { "th": "อีเมล", "en": "Email" },
    "role": { "th": "บทบาท", "en": "Role" },
    "group_mem": { "th": "กลุ่มสมาชิก", "en": "Group Member" },
    "status": { "th": "สถานะ", "en": "Status" },
    "active": { "th": "เปิดใช้งานแล้ว", "en": "Activated" },
    "not_active": { "th": "ยังไม่เปิดใช้งาน", "en": "Not Activated" },
    "manage_group": { "th": "จัดการสมาชิกกลุ่ม", "en": "Manage Group Member" },
    "add_group": { "th": "เพิ่มกลุ่ม", "en": "Add Group" },
    "create_group": { "th": "สร้างกลุ่ม", "en": "Create Group" },
    "name_group": { "th": "ชื่อกลุ่ม", "en": "Group Name" },
    "select_user": { "th": "เลือกผู้ใช้งาน", "en": "Select User" },
    "save": { "th": "บันทึก", "en": "Save" },
    "close": { "th": "ปิด", "en": "Close" },
    "invite": { "th": "ส่งคำเชิญ", "en": "Invite" },
    "no": { "th": "ลำดับ", "en": "No." },
    "select": { "th": "เลือก", "en": "Select" },
    "invite_agian": { "th": "ส่งคำเชิญอีกครั้ง", "en": "Invite agian" },
    "reject": { "th": "ปฏิเสธ", "en": "Reject" },
    "leave": { "th": "ถอนตัว", "en": "Leave Group" },
    "Usability": { "th": "การใช้งาน", "en": "Usability" },
    "department": { "th": "แผนก / สังกัด", "en": "Department" },
    "contactList": { "th": "รายชื่อผู้ใช้งานในระบบ", "en": "Contact List" },
    "userList": { "th": "รายชื่อพนักงานทั้งหมด", "en": "User List" },
    "from": { "th": "จาก", "en": "From" },
    "to": { "th": "ถึง", "en": "To" },
    "filter": { "th": "กรอง", "en": "Filter" },
    "period": { "th": "ช่วงเวลา", "en": "Period" },
    "search_text": { "th": "ค้นหา", "en": "search" },
    "num_doc_used": { "th": "จำนวนการใช้เอกสาร", "en": "Number of documents used" },
    "num_ekyc": { "th": "จำนวนการ e-KYC", "en": "Number of e-KYC" },
    "reduce_paper": { "th": "ตอนนี้คุณช่วยลดกระดาษไปแล้ว", "en": "You have now reduced the paper" },
    "sheet": { "th": "แผ่น", "en": "Sheet" },
}
