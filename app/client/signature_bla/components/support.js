Pages.Support = {
    _template: 'support.html',
    template: '<div>this is plans.html</div>',
    data: function () {
        return {
            myEmail: getUserField("adminEmail"),
            subject: "",
            message: "",
            btn_status: true,
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_support,
        }
    },
    mounted: function () {
        window.myApp = this
    },
    methods: {
        send_mail: function () {
            var that = this

            if (that.subject == "" || that.message == "") {
                alert(that.lang_txt.A1[that.lang_choose])
                return false
            }

            var data = {
                to: that.myEmail,
                // email: "thanaphas@gmail.com",
                subject: that.subject,
                message: that.message,
            }
            data.message = that.set_br_message_mail(data.message)

            that.btn_status = false
            $.ajax({
                type: "POST",
                url: api_host + "support_mail",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        that.subject = ""
                        that.message = ""
                        Swal.fire({
                            title: that.lang_txt.A2[that.lang_choose],
                            confirmButtonText: that.lang_txt.A3[that.lang_choose],
                            icon: 'success',
                        })
                    }
                    else {
                        Swal.fire({
                            title: that.lang_txt.A4[that.lang_choose],
                            confirmButtonText: that.lang_txt.A3[that.lang_choose],
                            icon: 'error',
                        })
                      
                    }
                    that.btn_status = true

                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });

        },
        set_br_message_mail: function (str) {
            var x = str.replaceAll("\n", "<br>")
            return x
        },
    }
}

routes.push({ path: '/support', component: Pages.Support })

var lang_support = {
    "title": { "th": "ช่วยเหลือ", "en": "Help" },
    "your_email": { "th": "อีเมลของคุณ", "en": "Your email" },
    "subject": { "th": "เรื่อง", "en": "Subject" },
    "message": { "th": "ข้อความ", "en": "Message" },
    "sent": { "th": "ส่งข้อความ", "en": "Send Message" },
    "wait": { "th": "กำลังส่งข้อความ กรุณารอสักครู่", "en": "Sending, Please wait.." },
    "A1": { "th": "กรุณากรอกข้อมูลให้ครบถ้วน", "en": "Please fill out the information completely." },
    "A2": { "th": "ส่งข้อความสำเร็จ", "en": "Message sent successfully" },
    "A3": { "th": "ตกลง", "en": "Agree" },
    "A4": { "th": "ส่งข้อความไม่สำเร็จ", "en": "Failed to send message" },

}

