function openNav() {
    var nav = document.getElementById("mySidenav").style.width
    if (nav == "0px") {
        document.getElementById("mySidenav").style.width = "100%";
    }
    else if (nav == "100%") {
        document.getElementById("mySidenav").style.width = "0";
    }
    else {
        document.getElementById("mySidenav").style.width = "100%";
    }
}

function openNav2() {
    document.getElementById("mySidenav").style.width = "260px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0px";
}

function open_popup_search_filter() {
    document.getElementById("popup-filter").style.display = "unset";

}

function close_popup_search_filter() {
    document.getElementById("popup-filter").style.display = "none";
}

function full_page() {
    document.getElementById("app").style.marginLeft = "0";
    if (window.screen.width > 991) {
        document.getElementById("mySidenav").style.display = "none";
    }
}
function openLeftMenu() {
    document.getElementById("app").style.marginLeft = "260px";
    if (window.screen.width > 991) {
        document.getElementById("mySidenav").style.display = "block";
    }
}

function arrow(id) {
    var icon = document.getElementById(id)
    var open = icon.classList[2] || false

    if (open) {
        icon.className = 'fas fa-chevron-up';
    } 
    else {
        icon.className = 'fas fa-chevron-up open';
    }

    open = !open;
}

function list_document_load(id){
    var trash = document.getElementById("trash-document" + id)
    var load = document.getElementById("load-document" + id)

    if (trash && load){
        trash.style.display = "none"
        load.style.display = "block"
    }
} 

