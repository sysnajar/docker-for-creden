<!DOCTYPE html>
<html lang="th-TH" dir="ltr">

<head>
  <title>Bangkok Life | Login</title>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0,user-scalable=1">
  <meta content="156004817605-qpm22mpvfvs0ch56s95igt6b1su33g78.apps.googleusercontent.com"
    name="google-signin-client_id">

  <link rel="preconnect" href="https://fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css2?family=Kanit:wght@300;400;500&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css"
    integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
  <link rel="icon" type="image/x-icon" href="images/BLA_Icon.ico">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet"
    integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">
  <link rel="stylesheet" type="text/css" href="/bla/css/font.css?v=18">
  <link rel="stylesheet" type="text/css" href="css/index.css?v=12">

  <style>
    .bla-logo-login {
      width: 300px;
      height: 140px;
    }
  </style>
</head>

<body>

  <div id="app" v-cloak>

    <div class="container mt-5">
      <div class="row justify-content-center">
        <div class="col-6 rg16-g1">
          <div class="alert alert-danger dy-none" role="alert" id="err_danger">
            {{lang_err}}
          </div>
        </div>
      </div>
    </div>

    <div class="mb-5 box-center mt-2">
      <div class="box-login-ff d-flex flex-column justify-content-center px-3 py-5 position-relative">

        <!-- title -->
        <div class="row">
          <div class="col-12 md36-g1 text-center">
            <div v-if="lang_choose == 'th'">
              <img src="images/BLA_Logo-01.png" alt="" class="bla-logo-login ">
            </div>
            <div v-else>
              <img src="images/BLA_Logo-02.png" alt="" class="bla-logo-login ">
            </div>
          </div>

          <div class="col-12 md24-g1 text-start mt-4">
            {{lang_txt.login_title[lang_choose]}}
          </div>
        </div>

        <!-- content -->
        <div class="row">
          <div class="col-12 mt-3">
            <div class="input-group">
              <span class="input-group-text bg-FF bd-right-0">
                <i class="fas fa-user cl-G1"></i>
              </span>
              <input type="text" class="form-control bd-left-0" id="myEmail" placeholder="Email" v-model="username"
                v-on:keyup.enter="login()">
            </div>
          </div>

          <div class="col-12 mt-4">
            <div class="input-group">
              <span class="input-group-text bg-FF bd-right-0">
                <i class="fas fa-key cl-G1"></i>
              </span>
              <input type="password" class="form-control bd-left-0" placeholder="Password" v-model="password"
                v-on:keyup.enter="login()">
            </div>
          </div>

          <div class="col-12 mt-3">
            <div class="row">
              <div class="col-6">
                <!-- <div class="form-check">
                  <input class="form-check-input cs-pointer" type="checkbox" value="">
                  <label class="form-check-label lg14-g1">
                    Remember me
                  </label>
                </div> -->
              </div>
              <div class="col-6 cl-CI text-end rg18-g1">
                <span class="cs-pointer" onclick="document.location.href='forgot.html'">
                  {{lang_txt.signup_forgot_password[lang_choose]}}
                </span>

              </div>
            </div>
          </div>
          <!-- 
          <div class="col-6 mt-4 align-self-center">
            <a href="register.html" class="md24-g1 cl-CI"
              style="text-decoration-line: none;">{{lang_txt.signup[lang_choose]}}</a>
          </div> -->
          <div class="col-12 mt-4">
            <div class="row">
              <div class="col-6"></div>
              <div class="col-6">
                <button type="button" class="btn cl-bla-bg w-100 md24-g1"
                  @click="checkAD()">{{lang_txt.login_title[lang_choose]}}</button>
              </div>

            </div>
          </div>

        </div>

        <!-- lang -->
        <div class="row position-absolute start-0" style="bottom: -40px;">
          <div class="col-12 mt-2 text-end rg18-g1" v-if="lang_choose == 'th'">
            <span class="cl-CI cs-pointer">ไทย</span> |
            <span class="cs-pointer" onclick="swithLang('en')">English</span>
          </div>

          <div class="col-12 mt-2 text-end rg18-g1" v-else>
            <span class="cs-pointer" onclick="swithLang('th')">ไทย</span> |
            <span class="cl-CI cs-pointer">English</span>
          </div>
        </div>
      </div>
    </div>


    <!-- End App -->
  </div>

  <!-- Google Login -->
  <!-- <iframe id="logoutFrame" src=""></iframe> -->
  <div class="g-signin2 btn-block dy-none" data-onsuccess="onSignIn"></div>




</body>
<script type="text/javascript" src="components/common_web.js"></script>
<script type="text/javascript" src="/bla/components/error_server.js"></script>
<script type="text/javascript" src="/bla/components/error_client.js"></script>
<script async="" defer="" src="https://apis.google.com/js/platform.js"></script>
<script type="text/javascript" src="/bla/components/common.js"></script>
<script type="text/javascript" src="/bla/js/jquery-3.3.1.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.6.10/vue.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"
  integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

<!-- MY ITEM -->
<script>
  $(document).ready(function () {
    app = init_vue()

  });


  function init_vue() {
    return new Vue({
      el: '#app',
      data: {
        lang_choose: localStorage.getItem('lang_choose') || 'th',
        lang_txt: lang_login,
        lang_err: "",
        username: "",
        password: "",
        qr_id: gup("qr_id") || "",
        lg_extra: gup("login_dir") || "",
        card_reader_id: gup("id") || "",

      },
      mounted: function () {
        window.myApp = this
        var email = gup("email")
        if (email) this.username = email
      },
      methods: {

        checkAD: function(){
          var that = this
      var data = {username:that.username,password:that.password}
          $.ajax({
              type: "POST",
              url: api_host + "api_AD",
              data: JSON.stringify(data),
              dataType: "json",
              success: function (resp) {
                var r = resp.data
                if (resp.success &&  resp.validate) {
                 that.login()
                }
                else {

                }
              },
              error: function (request, status, error) {
                alert("|" + status + ' |' + error);
              }
            });
        },

        login: function () {
          var that = this
          var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
          var u = that.username
          var p = that.password
          var page = gup('p') || "signature"
          var document_id = gup("doc_id")

          if (u == "" || p == "") {
            that.lang_err = err_c.c_1001[that.lang_choose]
            display_alert("err_danger", "block", "none")
            return false
          }

          var chk_mail = filter.test(u)
          if (!chk_mail) {
            that.lang_err = err_c.c_1002[that.lang_choose]
            display_alert("err_danger", "block", "none")
            return false
          }

          $.ajax({
            type: "POST",
            url: api_host + "login",
            // data: 'login_service=esig&username=' + u + '&password=' + p,
            data: 'login_service=esig&username=' + u + '&password=' + p + '&cardreader_id=' + that.card_reader_id || '',

            dataType: "json",
            success: function (resp) {

              if (resp.success) {
                var r = resp.data
                var url = document.location.origin

                if (r && r.activeStatus == 4) {
                  localStorage.setItem("compCode", resp.custCode);
                  localStorage.setItem("username", resp.username);
                  localStorage.setItem("extend_key", resp.extend_key);
                  localStorage.setItem("register_obj", JSON.stringify(r));

                  // if (that.lg_extra == "") {

                  if (page == "signature" && document_id && document_id != "null") {
                    document.location.href = url + '/bla/#/document?id=' + document_id
                  }
                  else if (page == "signature") {
                    document.location.href = url + '/bla/#/dashboard'
                  }
                  else if (page == 'pin') {
                    document.location.href = url + '/bla/pin.html?rd=' + new Date().getTime()
                  }

                  // }
                  // else {
                  //   document.location.href = document.location.origin + '/pj/democreden01/?login_dir=success&new_parameter=creden&id=' + that.card_reader_id + '&adminEmail=' + r.adminEmail
                  // }


                }
                else {
                  document.location.href = "confirm.html?email=" + resp.email + "&t=" + resp.token + "&rd=" + new Date().getTime()
                }

              }
              else {
                that.lang_err = err_s.s_1001[that.lang_choose]
                display_alert("err_danger", "block", "none")
                localStorage.removeItem("compCode");
                localStorage.removeItem("username");
              }

            },
            error: function (request, status, error) {
              alert("|" + status + ' |' + error);
            }
          });

        },

      } //end methods
    })
  }
  var lang_login = {
    "text_signup": { "th": "หากยังไม่เป็นสมาชิก?", "en": "Don't have a Creden account?" },
    "signup": { "th": "สมัครสมาชิก", "en": "Sign up for Creden account." },
    "signup_title": { "th": "สมัครสมาชิก", "en": "Sign up for Creden account." },
    "email_login": { "th": "อีเมล", "en": "Email" },
    "signup_login": { "th": "รหัสผ่าน", "en": "Password" },
    "signup_forgot_password": { "th": "ลืมรหัสผ่าน?", "en": "Forgot your password?" },
    "login_label": { "th": "เข้าสู่ระบบ", "en": "Log in to your account." },
    "login_title": { "th": "เข้าสู่ระบบ", "en": "Login" },
    "login_with": { "th": "เข้าสู่ระบบด้วย", "en": "Log in with" },
  }

</script>





</html>