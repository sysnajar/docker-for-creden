var sdssdd
var video0
var constraints
var videoSelect
var audioSelect
var devicex;
var deviceInfo
var aaa = []
var status_cam_load;
var ctx2
Pages.Ekyc = {
    // _template : 'ekyc.html',
    _template : 'step.html',

    data : function(){
        return {
	        evtName : 'eKYC',
            pin_time:1,
            sign_type: '',
            sign_name: '',
            user_signature: [{ draw: '', gen: '', initial: '' }],
            draw: true,
            gen: true,
            initial: true,
            gen_initial: true,
            font_sing: '',
            pin_number: "",
            savelog_status:false,
            status_chk_create_signature:true,
            message_log:"",
            pin_txt:"กรุณาสร้างรหัส PIN ตัวเลข 6 หลัก",
            lang_choose : localStorage.getItem('lang_choose') || 'th',
            lang_ekyc : lang_ekyc,
            user: {},
            uplxxx:false,
            is_silkspan: false,
            text_btn_close:'',
            status_progress : 0,
            is_ghb: false,
            count_sefile:0,
			      face_pass : "N/A",
            psid:gup('psid'),
            is_mobile: mobilecheck(),
            isLoading: false,
            status_text:"aaaaa",
            text_sub:"",
            status_ekyc_text:"bbb",
            text_modal:"cccc",
            btn_take_pic:false,
            isLoading2: true,
            showCardOverlay: false,
            flag_take_or_not:false,
            id: '',
            check_kyc : false,
            results: '',
            step: 1,
            show_vdo: true,
            can_save_cam: false,
            can_cancel_cam: false,
            frontCam: null, // store front camera deviceId
            backCam: null, // back camera
            comCam: null,
            card_no: '',
            laser_code: {lc1:'', lc2:'', lc3:''},
            firstname: '',
            lastname: '',
            xCxIxDx: '',
            bxpx1xnxxo: '',
            face_pct: 0,
            faceId1: '',
            day: '',
            mon: '',
            year: '',
            fullyearxxx:'',
            randomText: '',
            arr_txt: [],
            arr_time: [3, 13, 19],
            arr_mission:[],
            can_rec: false,
            showRecOverlay: false,
            recordRTC: null,
            isConfirming: false,
            kyc: '',
            isVerified: false,
            dummy: false,
            code: '',
            vdo: '',
            province: '',
            address: '',
            sex: '',
            instruct: true,
            loadingTxt: 'Plase Wait ..',
            points_face:0,
            points:0,
            total_points:0,
            username:getUsername(),
            data_save:{},
            show_vdo2:false,
            path_name : path_name,
            isValidDopa : false,
            isNotDeath : false,
            txid : txid,
            txid_2:'',
            usr : usr,
            ekyc_status : ekyc_status,
            remarks : '',
            remark_code : '',
            reason : 0,
            pic_error : false,
            camera_error : 0,
            room: 'a',
            selfie_file: '',
            ekyc_status_detail: false,
            ekyc_level: 0,
            ekyc_level_desc: '',
            cardImg1:'',
            create_count_signature : 1,
            cardImg2:'',
            cardImg3:'',
            card_reader_id_tx:gup('card_reader_id'),
            ux:gup('u'),
            new_parameter_tx:gup('new_parameter'),
            use_card_reader : false,
            status_dipchip_try:'',
            conf:conf || {is_step4:false,face_pcts:50,redirect_url:document.location.origin+"/signature/main.html#/profile"}
        }
      },
    mounted : function (){

    
      // alert(this.ux)
      (function($) {
  $.fn.inputFilter = function(inputFilter) {
    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
      if (inputFilter(this.value)) {
        this.oldValue = this.value;
        this.oldSelectionStart = this.selectionStart;
        this.oldSelectionEnd = this.selectionEnd;
      } else if (this.hasOwnProperty("oldValue")) {
        this.value = this.oldValue;
        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
      } else {
        this.value = "";
      }
    });
  };
}(jQuery));

       if (this.lang_choose == "en"){
        this.pin_txt = "Please create  PIN "
       }else{
      this.pin_txt = "กรุณาสร้างรหัส PIN ตัวเลข 6 หลัก"
      }



     
       window.appEkyc = this
       var app = this
    
      this.check_load_webcam()
      
     
    
      
      if(this.ekyc_status == 'EDIT') {
        this.to(4)
      }

      if(!this.conf.is_step4){
        this.ekyc_level = 2.2
        this.ekyc_level_desc = 'Dip chip online (no liveness)'
      }else{
        this.ekyc_level = 2.3
        this.ekyc_level_desc = 'เทียบ Bio ด้วยระบบ (liveness)'
      }
     

          
      if(this.card_reader_id_tx==undefined || this.card_reader_id_tx==null || this.card_reader_id_tx==''){
	 

      // alert('ko1okko')
      }else{
 
        var tmp = parseInt( (localStorage[this.username+"SSSSS"] || "0"))
      var now = new Date().getTime()

      if(now - tmp > 0)
        {
           //console.log('now - tmp > 0')
           // this.to(1)
            //  alert('1234')
            if(sessionStorage.getItem('dipchip')== "true"){
              var that = this
              that.status_dipchip_try = 'dipchip'
            }
      
          //  alert(this.status_dipchip_try)

      // if(this.username == "hattaporn@creden.co") {
      //   this.to(7)
      //   setTimeout(function(){
      //     window.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
      //       backgroundColor: 'rgba(255, 255, 255, 0)',
      //       penColor: 'rgb(0, 0, 0)'
      //   });
      //   },1000)

      // }else{

           this.fillCardInfosFromCardReader()
          this.detectFaceIdFromCardReader() 
      // }
        }
      else
        {
           if (this.lang_choose == "en"){
            // alert('PIN not matched.'); 
             alert('Taking pictures of failed faces 3 Consecutive times You will not be able to verify your identity through the system in 5 minutes. ' + new Date(tmp).toString() + '\n' + ' The current time is ' + new Date().toString()  )
              window.location.href = '/bla/#/profile'

          }else{
             // alert('รหัส PIN ไม่ตรงกับที่ตั้งไว้.'); 
              alert('ดำเนินการถ่ายรูปหน้าตรงล้มเหลวเป็นจำนวน 3 ครั้งติดต่อกัน ผู้ทำรายการจะไม่สามารถดำเนินการยืนยันตัวตนผ่านระบบได้ในระยะเวลา 5 นาที ' + new Date(tmp).toString() + '\n' + 'เวลาปัจจุบันคือ ' + new Date().toString()  )
              window.location.href = '/bla/#/profile'
          
          }
           // document.location.href = appEkyc.conf.fail_url
           
        }
	 
	      
		
      
      }
    },
    methods : {
       isMobileDevice:function() {
    var check = false;
    (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
    return check;
},
      lockpin:function(){
        $(".password").inputFilter(function(value) {
    return /^\d*$/.test(value);    // Allow digits only, using a RegExp
  });
      },
      check_feild:function(){
        $("#p1").inputFilter(function(value) {
          return /^-?\d*$/.test(value); });
        $("#p2").inputFilter(function(value) {
          return /^-?\d*$/.test(value); });
        $("#p3").inputFilter(function(value) {
          return /^-?\d*$/.test(value); });
        $("#p4").inputFilter(function(value) {
          return /^-?\d*$/.test(value); });

        $("#p5").inputFilter(function(value) {
          return /^-?\d*$/.test(value); });
        $("#p6").inputFilter(function(value) {
          return /^-?\d*$/.test(value); });
      },
      show_video:function(){

        var that = this
        that.text_sub = ''


          if(that.flag_take_or_not==true){
        ctx2.clearRect(0, 0, canvas.width, canvas.height);
       // alert('retake')
         $("#canvas2").remove()
         // setTimeout(function(){
         $('#takephotox').prop('disabled', true);

         // },1000)

         // alert("226")
                         // $('<canvas id="canvas2" class="canvas" ></canvas>').appendTo('#videoxx');

        that.flag_take_or_not = false


        }
        $('<canvas id="canvas2" class="canvas" style="display: none;"></canvas>').appendTo('#videoxx');



        that.show_vdo = true
        that.can_save_cam = that.can_cancel_cam = !that.show_vdo
        // $('#text_take_step1').hide()

      
        that.btn_take_pic=true
       
        setTimeout(function(){
        video0.show()
        $("#overlay").show()
      

        if(!status_cam_load){
              chk_devices()
             }        // $(video).appendTo("#videoxx");
        var box= document.getElementById('box-content');
        var width = box.offsetWidth;
        var height = box.offsetHeight;
        $("#videoxx").css({"width": width - 100, "height": "400px"});
        $("#overlay").css({"width": width - 100, "height": "400px"});
        video0.elt.width = width - 100
        video0.elt.height = 400
          video0.id('video')
        video = video0.elt
        $(video).appendTo("#videoxx");


        },1000)

         setTimeout(function(){
   
      $('#takephotox').prop('disabled', false);

        },2600)
        $('#takephotox').show()
      
        that.uplxxx = false
        

        // initCameraDropdown(app)
       
      },
      check_load_webcam:function(){
        var that = this
        navigator.getMedia = ( navigator.getUserMedia || // use the proper vendor prefix
                       navigator.webkitGetUserMedia ||
                       navigator.mozGetUserMedia ||
                       navigator.msGetUserMedia);

        navigator.getMedia({video: true}, function() {
        
        }, function() {
          // webcam is not available
          setTimeout(function(){
               if (that.lang_choose == "en"){
                  // that.text_modal = "Please wait Processing system"
                    alert('Cannot work at this stage. Because the device used does not have a camera.')
                    window.location.href = '/bla/#/profile'
                }else{
                  // that.text_modal = 'ระบบกำลังประมวลผล'
                    alert('ไม่สามารถทำงานในขั้นตอนนี้ได้ เนื่องจากอุปกรณ์ที่ใช้ไม่มีกล้อง')
                    window.location.href = '/bla/#/profile'
                }
          

          },1000)

        });
      },

	 
	   fillCardInfosFromCardReader(){
        var that = this
       // alert(that.new_parameter_tx)
        if(that.new_parameter_tx=="self_dipchip"){
        var url = api_host + 'cardinfo_dipchipCreden'

        $.ajax({
                   url: url,
                   type: "POST",
                   data: JSON.stringify({id: that.card_reader_id_tx}),
           dataType : 'json',
                   success: function (res) {
            if(res.success){  

                         that.address = res.data.address
                         that.firstname = res.data.firstname
                         that.lastname = res.data.lastname
                         that.province = res.data.country
                         that.xCxIxDx = res.data.CID
                         that.bxpx1xnxxo  = res.data.bp1no
                         
                         that.day =  res.data.britdate.substring(0,2);
                         that.mon = res.data.britdate.substring(3,5);
                          
                         
                         
                         that.year = res.data.britdate.substring(6,10);
                         that.fullyearxxx = that.day + '/' + that.mon + '/' + that.year
                         if(res.data.sex == "ชาย"){
                          that.sex = 'male'
                         }else{
                          that.sex = 'female'
                         }
                         // that.sex = res.data.sex
                         that.card_no = res.data.Number_card
                         // alert(that.card_no)
            }else{
               // alert('Unnable to detect card info from card reader')
                if (that.lang_choose == "en"){
                  alert('Unnable to detect card info from card reader') 
                  window.location.href = '/bla/#/profile'


                }else{
                  alert('ไม่สามารถตรวจพบข้อมูลการ์ดจากเครื่องอ่านการ์ด กรุณาทำงานผ่านเครื่องอ่านการ์ดก่อนทุกครั้ง')
                  window.location.href = '/bla/#/profile'

                }
            }
                   },
                   error: function (error) {

                      // alert("Something went wrong in fillCardInfosFromCardReader !");
                   }
                }) 
        }
      },
   
	  
	   detectFaceIdFromCardReader(){
      // alert(1111)
        var that = this
        var url = api_host + 'detect_face'

        if(that.new_parameter_tx=="self_dipchip"){
        
        $.ajax({
                   url: url,
                   type: "POST",
                   data: JSON.stringify({img: 'imgcardghb'+that.card_reader_id_tx}),
           dataType : 'json',
                   success: function (res) {
            if(res.success){  
            that.faceId1 = res.faceId
            that.use_card_reader = true

              if (this.card_reader_id_tx==undefined || this.card_reader_id_tx==null){
                  that.to(3)
                  // alert('1oodosio')
              }else{
                
                 that.to(3)
                 
                //  alert('adpsaodpo')
              }
          
            }else{

               // alert('Unnable to detect face from card reader')
            }
                   },
                   error: function (error) {
                      // //console.log("Something went wrong!");
                   }
                }) 
        }

     
	  },

      pressOK : function(){
        this.instruct = false
        this.pic_error = false
        this.retake()
      },
      to: function (n) {
        this.instruct = true

        if (n === 4) {
          this.status_progress = 0
        this.savelog_status = false
        this.text_modal = ''
            // if(!this.conf.is_step4) n = n+1;
            n = n + 1;
        }
        this.step = n

        if (this.step == 6) {
          // this.show_vdo = true
          // alert('')
          setTimeout(function(){
            var that = this
          // alert('123456')
          // that.lockpin()
          $(".password").inputFilter(function(value) {
            return /^\d*$/.test(value);    // Allow digits only, using a RegExp
          });
        },1500)

         }

         if(this.step == 7){
            setTimeout(function(){
          window.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
            backgroundColor: 'rgba(255, 255, 255, 0)',
            penColor: 'rgb(0, 0, 0)'
        });
        },1000)
         }
        //  alert(n)
      },

      showOverlay: function () {
        var that = this
        this.showCardOverlay = this.showRecOverlay = false
        if ((this.step === 1 || this.step === 2) && true) {
          this.showCardOverlay = true
          setTimeout(function () { that.showCardOverlay = true }, 10000)
        }
        if (this.step === 4 && this.conf.is_step4) {
          this.showRecOverlay = true
        }
      },
      
      retake: function (n) {
        this.show_vdo = true
        this.can_save_cam = this.can_cancel_cam = !this.show_vdo
        this.showOverlay()
      },
    

      uploadPicture : function(){
        $('#exampleModal').modal('show')

        var canvas = document.getElementById('canvas2')
        var that = this;
         if (that.lang_choose == "en"){
            that.text_modal = "Please wait Processing system"
          }else{
            that.text_modal = 'ระบบกำลังประมวลผล'

          }
        // that.text_modal = 'ระบบกำลังประมวลผล'
        that.status_progress = 1
        that.isLoading = true
        that.loadingTxt = 'กำลังอัพโหลดรูป'
        that.instruct = true
 
        canvas.toBlob(function(blob){
             var form = document.getElementById('upload_form');
             var formdata = new FormData(form);
             formdata.append('myfile', blob); 
             //console.log(blob)
                var url = window.api_host + 'upload'

                $.ajax({
                   url: url,
                   type: "POST",
                   data: formdata,
                   processData: false,
                   contentType: false,
                   success: function (result) {
                      that.savePicture(JSON.parse(result).filename);
                   },
                   error: function (error) {
                      //console.log("Something went wrong!");
                   }
                }) 

        }, 'image/png');
      },
      cameraOn: function () {
        video.play()
      },

      savePicture: function (filename) {
        var is_mobile = '' + mobilecheck()
        var data = {img: filename, id: this.id, is_mobile: is_mobile}
        var that = this
        var url = window.api_host
        that.loadingTxt = 'กรุณารอสักครู่'
        if (this.step === 1) {
          this.cardImg1 = filename + '.png'
          url = url + 'demo_card2'
        } else if (this.step === 2) {
          this.cardImg2 = filename + '.png'
          url = url + 'demo_backcard'
        } else if (this.step === 3) {
          this.cardImg3 = filename + '.png'
          url = url + 'demo_selfie'
          data.faceId1 = this.faceId1
          data.use_card_reader = this.use_card_reader
          data.txid = that.txid
        }
        axios.post(
          url,
          data,
          { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
        ).then(function(response) {
          that.isLoading = false
          if (response.data.success) {
            if (that.step === 1) {
              if (response.data.card_id && response.data.card_id.length === 13 && response.data.dob) {
                that.card_no = response.data.card_id
                that.day = response.data.dob.day
                that.mon = response.data.dob.mon
                that.year = response.data.dob.year
                that.province = response.data.province
                that.sex = response.data.sex
                that.firstname = response.data.name
                that.lastname = response.data.lastname
                if (response.data.face.length > 0) {
                  that.faceId1 = response.data.face[0].faceId
                  that.savePicture2()
                } else {
                  //alert('unnable to detect face, please retake photo.')
                  that.pic_error = true
                  that.retake()
                }
              } else {
                //alert('unnable to detect id card information, please retake photo.')
                that.pic_error = true
                that.retake()
              }
            } else if (that.step === 3) {
              that.status_progress = 2
              // that.text_modal = 'สำเร็จ'
               if (that.lang_choose == "en"){
                  that.text_modal = "Successfully"
                }else{
                  that.text_modal = 'สำเร็จ'

                }
                that.face_pct = response.data.face.confidence * 100
              that.selfie_file = response.data.filename
              if (Number(that.face_pct) > Number(that.conf.face_pcts)) {
                that.isVerified = true
                setTimeout(function(){
                  $('#overlay').hide()

                    
                    that.savePicture2()
                },1300)
                
              }else{
                that.count_sefile = that.count_sefile + 1
                if(that.count_sefile >= 3 ){
                   if (that.lang_choose == "en"){
                  // that.text_modal = "Please wait Processing system"
                    alert('The result of your face verification is  '+Number(that.conf.face_pcts)+'% lower than the system config. Please take a picture again.')

                }else{
                  // that.text_modal = 'ระบบกำลังประมวลผล'
                    alert('ผลลัพธ์การตรวจสอบใบหน้าคุณต่ำกว่าระบบที่ตั้งไว้ '+Number(that.conf.face_pcts)+'% กรุณาถ่ายรูปใหม่อีกครั้ง')
                }

                  // localStorage.can_do_ekyc_after = (new Date().getTime() + (1000*60*5)) + ""
                  // alert(that.adminEmail)
                  localStorage[that.username+'SSSSS'] = (new Date().getTime() + (1000*60*5)) + ""
                   // window.location.href = '/bla/#/profile'
                  // document.location.href = 'https://hjkl.ninja/'
                  $('#exampleModal').modal({
    backdrop: 'static',
    keyboard: false  // to prevent closing with Esc button (if you want this too)

})
                  setTimeout(function(){
                    window.location.href = '/bla/#/profile'
                  },2000)

                }
                that.text_btn_close = 
                 that.status_progress = 3
                  if (that.lang_choose == "en"){
            // that.text_modal = "Please wait Processing system"
              that.text_modal = 'Failed'
              that.text_sub = 'Please go back and take pictures of your face again '+that.count_sefile+ '/3 times. (If taking pictures of the face fails 3 Consecutive times You will not be able to verify your identity through the system in 5 minutes.)'

          }else{
            // that.text_modal = 'ระบบกำลังประมวลผล'
              that.text_modal = 'ไม่สำเร็จ'
              that.text_sub = 'กรุณากลับไปถ่ายรูปหน้าตรงใหม่อีกครั้ง '+that.count_sefile+'/3 ครั้ง (หากดำเนินการถ่ายรูปหน้าตรงล้มเหลวเป็นจำนวน 3 ครั้งติดต่อกัน ผู้ทำรายการจะไม่สามารถดำเนินการยืนยันตัวตนผ่านระบบได้ในระยะเวลา 5 นาที)'


          }
            
              setTimeout(function(){
                $('#video').hide()
                $('#overlay').hide()
                // context.clearRect(0, 0, canvas.width, canvas.height);
                ctx2.clearRect(0, 0, canvas.width, canvas.height);
                $("#canvas2").remove()
                $("#videoxx").removeAttr("style");

                appEkyc.btn_take_pic = false
                status_cam_load = false
                        // $('#exampleModal').modal('hide')

                $('<canvas id="canvas2" class="canvas" ></canvas>').appendTo('#cx');
                 // that.to(3)
              },1300)
               
              }

            } else if (that.step === 2) {
              that.laser_code = response.data.laser_code
              that.savePicture2()
            }
          } else {

            that.pic_error = true
          }
        }).catch( function(postErr){
            //console.log('Post Error'+postErr)
           that.isLoading = false
           that.status_progress = 3
            ctx2.clearRect(0, 0, canvas.width, canvas.height);
                $("#canvas2").remove()
                 status_cam_load = false
                  that.count_sefile = that.count_sefile + 1

                  if (that.lang_choose == "en"){
            that.text_modal = "Failed"
            that.text_sub = "Can 't detect your face in photo "+ that.count_sefile+'/3 times' 

          }else{
            that.text_modal = 'ไม่สำเร็จ'
            that.text_sub = 'ตรวจสอบไม่พบรูปถ่ายใบหน้าของผู้ทำรายการ '+ that.count_sefile+'/3 ครั้ง' 


          }
              // that.text_modal = 'ไม่สำเร็จ'
              
               // that.count_sefile = that.count_sefile + 1
               // that.text_sub = 'ตรวจสอบไม่พบรูปถ่ายใบหน้าของผู้ทำรายการ '+ that.count_sefile+'/3 ครั้ง' 
                if(that.count_sefile >= 3 ){
                  // alert()
                     if (that.lang_choose == "en"){
                  // that.text_modal = "Please wait Processing system"
                    alert('The result of your face verification is  '+Number(that.conf.face_pcts)+'% lower than the system config. Please take a picture again.')

                }else{
                  // that.text_modal = 'ระบบกำลังประมวลผล'
                    alert('ผลลัพธ์การตรวจสอบใบหน้าคุณต่ำกว่าระบบที่ตั้งไว้ '+Number(that.conf.face_pcts)+'% กรุณาถ่ายรูปใหม่อีกครั้ง')
                }
                  // alert('ผลลัพธ์การตรวจสอบใบหน้าคุณต่ำกว่าระบบที่ตั้งไว้ '+that.face_pct+'% กรุณาถ่ายรูปใหม่อีกครั้ง')
                  // localStorage.can_do_ekyc_after = (new Date().getTime() + (1000*60*5)) + ""
                   localStorage[that.username+'SSSSS'] = (new Date().getTime() + (1000*60*5)) + ""
                   window.location.href = '/bla/#/profile'
                  // document.location.href = 'https://hjkl.ninja/'

                }

              setTimeout(function(){
                 
                  $('#video').hide()
                  $('#overlay').hide()
                  // $('#exampleModal').modal('hide')

                appEkyc.btn_take_pic = false
                // that.to(3)
              },1300)
		});
      },
      cancelx:function(){
       
          $('#exampleModal2').modal('show')


      },
      close_btn:function(){
        $('#exampleModal').modal('hide')
        var that = this 
        that.to(3)

      },
      savePicture2: function () {
        var n = 0
        if (this.step === 5) {
          n = 1
        } else {

          //if(sessionStorage.getItem('dipchip')== "true"){
          if(false){
            this.submit()
            // n = this.step + 3
          }else{
          n = this.step + 1
          }
        }
        $('#exampleModal').modal('hide')

        // this.text_modal = this.lang_ekyc
        this.to(n)
      },
      takePicture: function () {
                this.uplxxx = true

        this.flag_take_or_not = true

        var w = 640
        var h = 400

        ctx2 = document.getElementById('canvas2').getContext('2d')
        document.getElementById('canvas2').width = w
        document.getElementById('canvas2').height = h

        ctx2.drawImage(video, 0, 0, w , h ) 
        // w = parseInt(w) 
        // h = parseInt(h)
        // document.getElementById('canvas').width = w
        // document.getElementById('canvas').height = h
        
        // context.drawImage(video, 0, 0, w, h)
        $('#canvas2').removeClass('p5Canvas')
        $('#canvas2').removeAttr("style");
        $("#canvas2").addClass("canvas");
        $('#canvas2').appendTo('#videoxx');

        $('#canvas2').show()
        $('#video').hide()
        $('#takephotox').hide()
        // $('#retake').prop('disabled', false);

        this.show_vdo = false

        // alert(this.can_save_cam)
        var that = this
        that.can_save_cam = that.can_cancel_cam = !that.show_vdo


      },
      setKyc: function () {
        this.kyc = ''
        this.isVerified = false
      },
      backHome: function () {

        window.history.back();
      },
      finished: function () {
        var ldd = document.location.href
        if (ldd.indexOf('hjkl') != -1) {
        // alert('esign.ldd')
          window.location.href = '/bla/#/profile'
        }
      },
      goToProfile: function () {
        var delayInMilliseconds = 6000; //1 second

        var that = this
        setTimeout(function() {
          window.location.href = this.conf.redirect_url + '?txid='+that.txid
        }, delayInMilliseconds);
        
      },
      skipEkyc: function () {
        
            window.location.href = '/bla/#/profile'
        
      },
      refreshPage: function () {
        window.location.reload(true)
      },
      click_pin_number: function(pin){
            var that = this

            if(pin == "X") {
               var p = that.pin_number
               that.pin_number = p.slice(0, -1)
            }
            else if(that.pin_number.length < 6 && that.chk_type_pin(pin)){
                that.pin_number += pin

                if(that.pin_number.length==6)
                  { that.key_pin_finished(that.pin_number);  }
                
            }
        },

        savelog:function(){
          // alert('1234t')
         var api_host = document.location.origin+'/capi/'
        var url = api_host + 'ekyc_tx_ins'
        var that = this;
        that.savelog_status = true
        that.status_progress = 1
           if (that.lang_choose == "en"){
             that.text_modal = "Please wait the system is saving the data."

          }else{
                  // that.text_modal = 'ระบบกำลังประมวลผล'
              that.text_modal = 'ระบบกำลังบันทึกข้อมูลกรุณารอสักครู่'
          }
        // that.text_modal = 'ระบบกำลังบันทึกข้อมูลกรุณารอซักคู่'

        // alert(api_host)
        
         var objMon = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
        var dob = that.day+'/'+objMon[that.mon]+'/'+Number(that.year)
        var lc = that.laser_code.lc1 + that.laser_code.lc2 + that.laser_code.lc3
        var data = {username:that.usr,adminEmail:getUsername(), txid:that.txid, "code":code || '++', "channel": "sdk","fnameTH": that.firstname, "lnameTH": that.lastname, "dob": dob, "cardNo": that.card_no, "laserCode": lc, "isFaceIdentical":that.isVerified, "province":that.province, "address":that.address, "faceConfidence":that.face_pct || 0, "isValidDopa":that.isValidDopa, "isValid":false, "cardImg1":that.cardImg1, "cardImg2":that.cardImg2, "cardImg3":'selfie_'+that.txid+'.png',"card_reader_id":that.card_reader_id_tx,"remarks":that.message_log}
              $.ajax({
                   url: url,
                   type: "POST",
                   data:JSON.stringify(data),
                   success: function (response) {
                    var vvv = JSON.parse(response)
                    that.status_progress = 2
                    if (that.lang_choose == "en"){
                    that.text_modal = 'The system has successfully saved the data. Thank you for reporting the problem.'

                    }else{
                      that.text_modal = 'ระบบได้ทำการบันทึกข้อมูลสำเร็จ ขอบคุณที่แจ้งปัญหา'

                    }

                    //console.log(vvv)
                    window.location.href = '/bla/#/profile'
                     
                   },
                   error: function (error) {
                    that.status_progress = 3
                     if (that.lang_choose == "en"){
                    that.text_modal = 'Failed to save data Please contact the staff'

                    }else{
                      that.text_modal = 'การบันทึกข้อมูลไม่สำเร็จ กรุณาติดต่อเจ้าหน้าที่'

                    }
                    // that.text_modal = 'การบันทึกข้อมูลไม่สำเร็จ กรุณาติดต่อเจ้าหน้าที่'
                    window.location.href = '/bla/#/profile'

                   }
                }) 

        },
        

        gen_cert:function(){
        var api_host = document.location.origin+'/capi/'
        var url = api_host + 'gen_cert'
        var that = this;

        // alert(api_host)
        var data = {pin:this.pin_number}
              $.ajax({
                   url: url,
                   type: "POST",
                   data:JSON.stringify(data),
                   success: function (response) {
                    var vvv = JSON.parse(response)
                    that.status_progress = 2
                     if (that.lang_choose == "en"){
                    that.text_modal = 'Successfully'

                     }else{
                    that.text_modal = 'สำเร็จ'
                      }
                      $('#exampleModal').modal('hide')
                      // n = n + // ==> comment by boom
                      // if(that.status_dipchip_try == "dipchip"){
                      //   window.location.href = '/bla/#/profile'
                      // }else{
                      that.to(7)
                      // }
                    // window.location.href = '/bla/#/profile'

                     
                   },
                   error: function (error) {
                    that.status_progress = 3
                     if (that.lang_choose == "en"){
                      that.text_modal = 'The system cannot work.'

                     }else{
                      that.text_modal = 'ระบบไม่สามารถทำงานได้'

                     }

                      //console.log("Something went wrong!");
                      that.pin_time=1;
                      that.pin_number="";
                      $('#pin_msg1').show();
                      $('#pin_msg2').hide();
                     
                   }
                }) 

        },


        key_pin_finished:function(pin){
          //console.log('key_finished '+pin+', time = '+ this.pin_time)

          if(this.pin_time==1)
            { this.pin_number_old = pin; this.pin_number = "" ; this.pin_time = 2; //console.log('Enter Again')
              $('#pin_msg2').show();
              $('#pin_msg1').hide();
            }
          else
            {
              this.pin_time=1;
              $('#pin_msg1').show();
              $('#pin_msg2').hide();

              if(this.pin_number==this.pin_number_old)
                {this.gen_cert();}
              else
                {
                   if (this.lang_choose == "en"){
                      alert('PIN not matched.'); 

                    }else{
                       alert('รหัส PIN ไม่ตรงกับที่ตั้งไว้.'); 

                    }

                  this.pin_number="";}
              
            }

        },

        key_pin_number: function(event){
            var that = this
            var key = String.fromCharCode(event.keyCode);

            if(that.pin_number.length <= 6 && that.chk_type_pin(key)){
                //console.log("key success "+ that.pin_number.length)
                if(that.pin_number.length==6)
                  { that.key_pin_finished(that.pin_number);  }
            }
            else {
               var p = that.pin_number
               that.pin_number = p.slice(0, -1)
            }
        },
        chk_type_pin: function(pin){
            const p = /^\d$/
            return (p.test(pin))
        },
      update_profile: function () {
        var that = this
        var objMon = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
        var dob = this.day+'/'+objMon[this.mon]+'/'+Number(this.year)
        var lc = this.laser_code.lc1 + this.laser_code.lc2 + this.laser_code.lc3
        
        var url = window.api_host
        url = url + 'update_profile_esig'
	      if(getUsername!=""){
        	
		   var data = {"email":getUsername(),"FNAME": this.firstname, "LNAME": this.lastname, "date_of_birth": dob, "txtPid": this.card_no, "cvv_code": "000000000", "verified_ekyc":this.isVerified, "province":this.province}
	      //console.log(data)
	      }else{
		// alert('Emtry')
	      }
          axios.post(
            url,
            JSON.stringify(data),
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
          ).then(function (response) {
            if (response.data.success) {
      var obj = JSON.parse(localStorage.register_obj)
				obj.FNAME =  that.firstname
				obj.LNAME = that.lastname
				obj.txtP_id = that.card_no
				localStorage.register_obj = JSON.stringify(obj)

        if(sessionStorage.getItem('dipchip')== "true"){
          
          that.to(6)
        }
  
        
    

				//that.goToProfile();
	//	localStorage.register_obj.FNAME = that.firstname
	    } else {
              //console.log(response.data)
            }
          })
      },      
      submit: function () {

        var that = this
        that.instruct = true
        var objMon = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
        var dob = this.day+'/'+this.mon+'/'+Number(this.year)
        var lc = this.laser_code.lc1 + this.laser_code.lc2 + this.laser_code.lc3
       
          
          var data = {"fnameTH": this.firstname, "lnameTH": this.lastname, "dob": dob, "ChipNo": that.xCxIxDx,  "bp1no": that.bxpx1xnxxo,"pid": this.card_no, "cvv_code": lc, "txid":this.txid, "usr":this.usr}
        
           // var url = 'https://esign.ldd.go.th/capi/' + 'post_dopa_o2'
             var url = window.api_host
        url = url + 'progdopa_stepekyc'

   
        this.kyc = ''
        var that = this
        that.isLoading = true
        if (that.dummy) {
          setTimeout(function () { that.isLoading = false }, 3000)
          that.kyc = {success: true}
          if (Number(that.face_pct) > Number(that.conf.face_pcts)) {
            that.isVerified = true
          }
        } else {
          axios.post(
            url,
            data,
            { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }
          ).then(function (response) {
            if (response.data.success) {
              that.isValidDopa = true
              that.isNotDeath = true
              //console.log(response.data);
           
            if(response.data.data.remark_code == "000"){
                  that.remarks = response.data.data.remarks
              that.remark_code = response.data.data.remark_code
              that.status_text = 'สำเร็จ'
                if (that.lang_choose == "en"){
                that.status_ekyc_text = 'Successfully'

                }else{
                that.status_ekyc_text = 'สำเร็จ'
                }
              }else{
                // alert('dopa2')
                  // alert(response.data.data.remarks)
                 that.remarks = response.data.data.remarks
              that.remark_code = response.data.data.remark_code
                            // that.status_text = 'ไม่สำเร็จ'
                            // that.status_ekyc_text = 'ไม่สำเร็จ'
                            if (that.lang_choose == "en"){
                that.status_ekyc_text = 'Failed'

                }else{
                that.status_ekyc_text = 'ไม่สำเร็จ'
                }

              }
              that.kyc = response.data
              if (that.isValidDopa && Number(that.face_pct) > that.conf.face_pcts) {
                that.isVerified = true
                that.reason = 0

               // alert('ok')
              }else{
                that.reason = 1
               // alert('ok2')
              }
              that.save_ekyc()
            } else {
             // alert('ok3')
                that.reason = 2
                that.isLoading = false
                that.instruct = false
                that.kyc = response.data
                // that.remarks = response.data.remarks
                // that.remark_code = response.data.remark_code
                that.remarks = response.data.data.remarks
                that.remark_code = response.data.data.remark_code
                // alert('1094')
                              that.status_text = 'สำเร็จ'

            }
          })
        }
      
      },
      goHOME:function(){
        window.location.href = '/bla/#/profile'
      },
      init_esign: function(){
        var that = this
        $('#createSignatureModal').modal('show')
       
      
      
      },
      confirm_create_signature:function(){
        var that = this

        if (signaturePad.isEmpty()) {
          // alert('111o1oo')
          
          $('#exampleModalLabel').text('คุณไม่ได้ทำการวาดลายเซ็นกรุณาตรวจสอบอีกครั้ง')
          setTimeout(function(){
            $('#comfirm').modal('hide')
            $('#exampleModalLabel').text('คุณยืนยันที่จะทำการสร้างลายเซ็นหรือไม่')
          },1000)
          that.status_chk_create_signature = false
          // return false
        }else{

          that.status_chk_create_signature = true
          that.sign_type = 'draw';
          var data = signaturePad.toDataURL('image/png');
          data = data.split(',')
		  //alert('img = '+ data[1].substring(0, 10))	
          $('#comfirm').modal('hide')

		  var url = '/create_signature'
          var jso = { "img": data[1], "username": getUsername(), "code": getUsername(), "sign_type": "draw", "field_id": '' }
          if(that.create_count_signature == "2"){
            //var jso = { "img": data[1], "username": getUsername()+"_engPoae", "code": getUsername(), "sign_type": "draw", "field_id": '' }
		  	url = '/create_signature_en'

          }
		  //var img =jso.img;jso.img=null	
		  //alert(JSON.stringify(jso)); jso.img = img;
		 	
          $.ajax({
              type: "POST",
              url: window.api_host + url,
              data: JSON.stringify(jso),

              dataType: "json",
              success: function (data) {
                  var path = '../edoc/app-assets/images/signature/' + getUsername() + '_draw.png?rd=' + new Date().getTime()
                  that.user_signature[0].draw = path
                  that.draw = true
                
                  if(that.create_count_signature == 2){
                    window.location.href = '/bla/#/profile'

                  }
                  that.clearSignature(1)

                  that.create_count_signature = that.create_count_signature + 1
                  that.status_chk_create_signature = false
              },
              error: function (request, status, error) {
                  alert("|" + status + ' |' + error);
              }
          });
        }

      },
      createSignature: function () {
        $('#comfirm').modal('show')
       
    },
    clearSignature: function (n) {
      var that = this 
      if (n == 1 || n == '1') {
          signaturePad.clear();
          
          setTimeout(function(){
            window.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
              backgroundColor: 'rgba(255, 255, 255, 0)',
              penColor: 'rgb(0, 0, 0)'
          });
          },1000)
          
          

      } else if (n == 2 || n == '2') {
          signaturePadInitial.clear()
      }

  },
      save_ekyc: function () {
        var that = this
        var objMon = {'Jan': '01', 'Feb': '02', 'Mar': '03', 'Apr': '04', 'May': '05', 'Jun': '06', 'Jul': '07', 'Aug': '08', 'Sep': '09', 'Oct': '10', 'Nov': '11', 'Dec': '12'}
        var dob = this.day+'/'+objMon[this.mon]+'/'+Number(this.year)
        var lc = this.laser_code.lc1 + this.laser_code.lc2 + this.laser_code.lc3
        
        var url = window.api_host
        url = url + 'save_ekyc_pea'

        var data = {username:that.usr,adminEmail:getUsername(), txid:that.txid, "code":code || '++', "channel": "sdk","fnameTH": that.firstname, "lnameTH": that.lastname, "dob": dob, "cardNo": that.card_no, "laserCode": lc, "isFaceIdentical":that.isVerified, "province":that.province, "address":that.address, "faceConfidence":that.face_pct || 0, "isValidDopa":that.isValidDopa, "isValid":that.isVerified, "isNotDeath":that.isNotDeath, "ekyc_level":that.ekyc_level, "ekyc_level_desc":this.ekyc_level_desc, "cardImg1":that.cardImg1, "cardImg2":that.cardImg2, "cardImg3":'selfie_'+that.txid+'.png',"card_reader_id":that.card_reader_id_tx}
        //console.log('3')


        axios.post(
            url,
            JSON.stringify(data)
          ).then(function (response) {
            //console.log(response)
            if(response.data.success == true){
                        that.isLoading = false
                        that.instruct = false
                        that.txid = response.data.txid

                    if(getUsername=="" || getUsername=="null" || getUsername==null){
                           that.isLoading = false
                     that.instruct = false
              }else{
               
                    that.update_profile()
              }
            }else{
              that.isLoading = true
                        that.instruct = true
                           if (that.lang_choose == "en"){
                  // that.text_modal = "Please wait Processing system"
                    alert('Your Credit is out of limit, Please contact the staff.')
                    window.location.href = '/bla/#/profile'

                }else{
                  // that.text_modal = 'ระบบกำลังประมวลผล'
                    alert('Credit ของคุณหมดกรุณาติดต่อเจ้าหน้าที่')
                    window.location.href = '/bla/#/profile'

                }
                        // alert('Credit ของคุณหมดกรุณาติดต่อเจ้าหน้าที่')
                        // window.location.href = '/bla/#/profile'
            }
		  
           
          })
          .catch(function (error){
          //  that.isLoading = false
           // that.instruct = false
            //console.log(error)
          })
      },
      setpinID:function(){
        var that = this

        
        that.to(6)

        that.check_feild()
      }
    }
}

routes.push({path: '/sdk', component: Pages.Ekyc })

function setup(){
  // alert('alert1')
   if (!(/Android|webOS|iPhone|iPad|iPod|BlackBerry|BB|PlayBook|IEMobile|Windows Phone|Kindle|Silk|Opera Mini/i.test(navigator.userAgent))) {
     devicex="PC";  constraints = {
        video: {
          
        },
        audio: true
      };
   }else{
     devicex="Mobile";  constraints = {
        video: {
          
        },
        audio: true
      };
  
   }
    xxx=createCanvas(640, 480);
    xxx.id('canvas2');
    xxx.hide();

    

    hasPermission_camera = 1;
    // alert(hasPermission_camera)
    video0 = createCapture(constraints,function(stream){
     
        //console.log(stream);
        if(!sdssdd){
         hasPermission_camera = 2;
          sdssdd = true
          //alert(sdssdd)
         gotStream(stream)
       }
    });
    //  setTimeout(function(){
    //   check_permission()
    // },1000*15)
    video0.id('video')
    video0.hide()
    document.getElementById('video').muted = true;
    video = video0.elt
}
function getStream() {
  // alert('getStream')
  if (window.stream) {
    window.stream.getTracks().forEach(function(track) {

      track.stop();
       // alert('track.stop')
    });
  }

   var xconstraints = {
    audio: {
      deviceId: {exact: audioSelect.value}
    },
    video: {
      deviceId: {exact: videoSelect.value}
    },
    
  };
  // alert(JSON.stringify(xconstraints))
  navigator.mediaDevices.getUserMedia(xconstraints).
    then(gotStream);
     // $('#takephotox').prop('disabled', false);
    
}

function gotStream(stream) {
  // alert('gotStream')
 window.stream = stream; // make stream available to console
  
 video.srcObject = stream;
 localStream = stream

 
 const track = stream.getVideoTracks()[0];
 const capabilities = track.getCapabilities();
 //console.log(track)
 //console.log(capabilities)
 // Check whether focus distance is supported or not.
 if (!capabilities.focusDistance) {
   return;
 }
  const input = document.querySelector('input[type="range"]');
 input.min = capabilities.focusDistance.min;
 input.max = capabilities.focusDistance.max;
 input.step = capabilities.focusDistance.step;
 input.value = track.getSettings().focusDistance;

 input.oninput = function(event) {
   track.applyConstraints({
     advanced: [{
       focusMode: "auto",
       focusDistance: event.target.value
     }]
   });
 };
 input.hidden = false;

}




function gotDevices(deviceInfos){
  count_cam = 0
  var option
  var option_Au
    videoSelect = document.querySelector('select#videoSource');
    audioSelect = document.querySelector('select#audioSource');
    if(!status_cam_load){
      status_cam_load = true
      var has_front = false
      var has_back  = false;
  
    for (let i = 0; i !== deviceInfos.length; ++i) {
        if(deviceInfos[i].kind == "videoinput" || deviceInfos[i].kind == 'video'){
            
  
          count_cam = count_cam + i
          //console.log(count_cam)
          //console.log(deviceInfos[i])
          aaa.push(deviceInfos[i])
          var name = deviceInfos[i].label.toLowerCase()
  
          //if(deviceInfos[i].label == "camera2 1, facing front" || deviceInfos[i].label == "กล้องด้านหน้า" || deviceInfos[i].label == "FaceTime HD Camera" || deviceInfos[i].label == "Front Camera" || deviceInfos[i].label == "camera 1, facing front"){
  //         if(name=="กล้องด้านหน้า" || name == "facetime hd camera" || name == "front camera" || ((name.indexOf("camera")!=-1) && (name.indexOf("front")!=-1))){
  
           option = document.createElement('option');
            option.value = deviceInfos[i].deviceId;
              option.text = deviceInfos[i].label || 'กล้อง ' + (videoSelect.length + 1);
              
  
                 videoSelect.appendChild(option);
  
          
  
         
        }else if(deviceInfos[i].kind === 'audioinput') {
           option_Au = document.createElement('option');
           option_Au.value = deviceInfos[i].deviceId;
        option_Au.text = deviceInfos[i].label || 'microphone ' + (audioSelect.length + 1);
        audioSelect.appendChild(option_Au);
  
  
        }else{
          //console.log('not found')
        }
   
    }
  }
  
     
    videoSelect.onchange = getStream;
    audioSelect.onchange = getStream;
     // $('#takephotox').prop('disabled', false);

  }

function chk_devices(){
  navigator.mediaDevices.enumerateDevices().then(gotDevices).then(getStream)
}


function movePin(field, nextId,backID, evt){

if(evt.keyCode==8 || evt.keyCode==46)
  {
    
     var val = field.value
     if(val=="")
        {
          document.getElementById(backID).value = ""
          setTimeout(function(){
          document.getElementById(backID).focus();
          //console.log("goback")
          },100);
          
        }
      else
        {
          field.value = ""
          //console.log("delete value")
        }  
  }
  else
  {
     document.getElementById(nextId).focus();
  }

  // if(event.key === "")
  /*
    if(field.value==""){
      alert("move back to ="+delID)
      //console.log(delID)
      document.getElementById(delID).focus()
      return;
      }
  //console.log(field)
  //console.log(nextId)
    if (field.value.length == 1) {
      if(field.value.length !== 5){
        document.getElementById(nextId).focus();
      }else{
        
 
      }
    }
    alert(field.value-1)
    // if(field !== 'p1'){
    //   document.getElementById(nextId-1).focus();
    //     //console.log('remove='+field)
    // }
    */
}


function reset_pinbox()
{
  document.getElementById('p1').value=''
  document.getElementById('p2').value=''
  document.getElementById('p3').value=''
  document.getElementById('p4').value=''
  document.getElementById('p5').value=''
  document.getElementById('p6').value=''

}

function get_pinbox()
{
  var ret = ''
  ret += document.getElementById('p1').value
  ret += document.getElementById('p2').value
  ret += document.getElementById('p3').value
  ret += document.getElementById('p4').value
  ret += document.getElementById('p5').value
  ret += document.getElementById('p6').value

  return ret

}

function moveLastPin(field){
    if (field.value.length == 1) 
    {
      //alert(field)
           //console.log('moveLastPin time = '+ window.appEkyc.pin_time)
           var app = window.appEkyc
          if(app.pin_time==1)
            {  
              app.pin_number = get_pinbox() ;
              app.pin_number_old = app.pin_number; 
              app.pin_time = 2;
              reset_pinbox() 
              //console.log('Enter Again')
              // setTimeout(function(){
                 if (app.lang_choose == "en"){
                  app.pin_txt = "Please enter the new PIN code again with 6 digits."
                 }else{
                app.pin_txt = "กรุณาใส่รหัส PIN ใหม่อีกครั้ง ด้วยตัวเลข 6 หลัก"
                }
            
              document.getElementById('p1').focus();

             
            }
          else
            {
              app.pin_time=1;
              setTimeout(function(){
              //    $('#pin_msg2').show();
              // $('#pin_msg1').hide();
              },1000)
             
               app.pin_number = get_pinbox() ; 

              if(app.pin_number==app.pin_number_old)
                {
                  $('#exampleModal').modal('show')
                  app.status_progress = 1
                   if (app.lang_choose == "en"){
                    app.text_modal = 'Please wait Processing system'

                    }else{
                      app.text_modal = 'ระบบกำลังประมวลผล'

                    }
                  // app.text_modal = 'ระบบกำลังประมวลผล'
                  app.gen_cert();
                }
              else
                {
                  // alert('PIN not matched.'); 
                   if (app.lang_choose == "en"){
                      alert('PIN not matched.'); 

                    }else{
                       alert('รหัส PIN ไม่ตรงกับที่ตั้งไว้.'); 

                    }
                  app.pin_number="";
                   document.getElementById('p1').focus();
                }

              reset_pinbox() 

              
            }   

    }
}


//------------
var lang_ekyc = {
  "STEP1":{"th":"ขั้น1","en":"STEP1"},
    "STEP2":{"th":"ขั้น2","en":"STEP2"},
    "STEP3":{"th":"ขั้น3","en":"STEP3"},
    "STEP4":{"th":"ขั้น4","en":"STEP4"},
    "STEP5":{"th":"ขั้น5","en":"STEP5"},
    "idCard":{"th":"หน้าบัตร","en":"ID Card"},
    "laserCode":{"th":"หลังบัตร","en":"Laser Code"},
    "Selfie":{"th":"ถ่ายรูปหน้า","en":"Selfie"},
    "recordVdo":{"th":"บันทึกวิดีโอ","en":"Record VDO"},
    "fillInForm_step5":{"th":"ตรวจสอบและแก้ไขข้อมูลให้ถูกต้อง","en":"Check and correct information."},
    "fillInForm_step5sub":{"th":"ตรวจสอบความถูกต้องของข้อมูลคุณ","en":"Verify the accuracy of your information."},
    "message_log_txt":{"th":"กรุณาใส่ข้อความเกี่ยวกับปัญหา","en":"Please enter a message about the problem."},
    "fillInForm":{"th":"ตรวจสอบ","en":"Check-Form"},
    "takephoto":{"th":"กรุณาเตรียมบัตรประชาชน<br>ถ่ายรูปด้านหน้าของบัตรประชาชน","en":"Take a photo your front ID CARD"},
    "useWebcam":{"th":"ใช้เว็บแคมถ่ายบัตรประชาชน<br>ของคุณในแนวนอน","en":"Use a webcam to take your ID card horizontally"},
    "useCamera":{"th":"ใช้กล้องโทรศัพท์ถ่ายบัตรประชาชน<br>ของคุณในแนวตั้ง","en":"Use a camera to take your ID card vertically"},
    "takeIdNumber":{"th":"กรุณาเตรียมบัตรประชาชน<br>ถ่ายหมายเลขหลังบัตรประชาชน","en":"Take a photo your back ID CARD"},
    "laserHori":{"th":"ถ่ายหมายเลขตัวพิมพ์เลเซอร์หลังบัตรประชาชน<br>ของคุณในแนวนอน","en":"Take a photo laser code horizontally"},
    "laserVert":{"th":"ถ่ายหมายเลขตัวพิมพ์เลเซอร์<br>หลังบัตรประชาชนของคุณในแนวตั้ง","en":"Take a photo laser code vertically"},
    "takeSFace":{"th":"ถ่ายรูปหน้าตรง","en":"Take a photo straight face"}, 
    "useYourCamera":{"th":"ใช้กล้องหน้าถ่ายรูปของคุณ","en":"Use your fornt camera to take photo"},  
    "recordAndRead":{"th":"บันทึกวิดีโอและทำตามคำบอก","en":"Record video and read message"},  
    "readOnScreen":{"th":"ถ่ายวีดีโอพร้อมทั้ง<br>ทำตามคำบอกที่ขึ้นบนจอ","en":"Take a video and do missions following text on the screen"},
    "saveForm":{"th":"และบันทึกแบบฟอร์ม","en":"Save Form"},
    "completeMessage":{"th":"กรอกข้อมูลให้สมบูรณ์","en":"Complete the form"},
    "OK":{"th":"ฉันเข้าใจ","en":"OK"},
    "savex":{"th":"บันทึก","en":"Save"},
     "close_btnx":{"th":"ปิด","en":"Close"},
    
    "ChooseCamera":{"th":"เลือกกล้อง","en":"choose camera"},
    "readSlowly":{"th":"อ่านคำหรือประโยคด้านล่าง ช้าๆ และ ชัดๆ","en":"Read the words slowly"},
    "verification":{"th":"ตรวจสอบข้อมูล","en":"verification"},
    "cardNo":{"th":"เลขบัตรประจำตัวประชาชน 13 หลัก","en":"CARD NO"},
    "percent":{"th":"ร้อยละ","en":"Percent"},
    "laserNo":{"th":"เลขหลังบัตรประชาชน","en":"Laser Code"},
    "faceMatch":{"th":"ผลการตรวจสอบใบหน้า","en":"Face Verification"},
    "Fname":{"th":"ชื่อ","en":"Firstname"},
    "Lname":{"th":"นามสกุล","en":"Lastname"},
    "address":{"th":"ที่อยู่","en":"Address"},
    "Sex":{"th":"เพศ","en":"Sex"},
    "male":{"th":"ชาย","en":"male"},
    "female":{"th":"หญิง","en":"female"},
    "dateOfBirth":{"th":"วันเกิด(16/06/2560)","en":"Date Of Birth(16/06/2560)"},
    "month":{"th":"เดือน","en":"month"},
    "Jan":{"th":"มกราคม","en":"January"},
    "Feb":{"th":"กุมภาพันธ์","en":"February"},
    "confirm_create_signature":{"th":"คุณยืนยันที่จะทำการสร้างลายเซ็นหรือไม่","en":"You insisted on creating a signature or not."},
    "confirm_create_signature_yes":{"th":"ยืนยัน","en":"Yes"},
    "confirm_create_signature_no":{"th":"ยกเลิก","en":"No"},
    "not_create_signature":{"th":"คุณยังไม่ได้ทำการวาดลายเซ็น","en":"You have not made a signature."},

    "Mar":{"th":"มีนาคม","en":"March"},
    "Apr":{"th":"เมษายน","en":"April"},
    "May":{"th":"พฤษภาคม","en":"May"},
    "Jun":{"th":"มิถุนายน","en":"June"},
    "Jul":{"th":"กรกฎาคม","en":"July"},
    "Aug":{"th":"สิงหาคม","en":"August"},
    "Sep":{"th":"กันยายน","en":"September"},
    "Oct":{"th":"ตุลาคม","en":"October"},
    "Nov":{"th":"พฤศจิกายน","en":"November"},
    "Dec":{"th":"ธันวาคม","en":"December"},
    "province":{"th":"จังหวัด","en":"Province"},
    "Result":{"th":"ผลลัพธ์","en":"RESULTS"},
    "Status":{"th":"สถานะบัตร","en":"Status"},
    "tax1":{"th":"ภาษี 1 :","en":"Tax 1 :"},
    "tax2":{"th":"ภาษี 2 :","en":"Tax 2 :"},
    "noTax":{"th":"ไม่จ่ายภาษี","en":"No Tax"},
    "verified":{"th":"ยืนยันตัวตน","en":"Verified"},
    "backToSilkspan":{"th":"กลับไปที่ซิลค์สแปน","en":"Complete Back To Silkspan"},
    "Recording":{"th":"กำลังอัด...","en":"Recording..."},
    "Back":{"th":"ย้อนกลับ","en":"BACK"},
    "BackHome":{"th":"เสร็จสิ้น","en":"Finish"},
    "Home":{"th":"กลับหน้าหลัก","en":"Finish"},
    "takeNewPhoto":{"th":"ถ่ายใหม่","en":"Take a new photo"},
    "useThisPic":{"th":"ใช้รูปนี้","en":"Use this picture"},
    "photoGrap":{"th":" ถ่ายรูป","en":" Take Photo"},
    "record":{"th":"เริ่มอัด","en":"Record"},
    "recordNew":{"th":"อัดใหม่","en":"Record New"},
    "SKIP":{"th":"ข้าม","en":"SKIP"},
    "Submit":{"th":"ตกลง","en":"SUBMIT"},
    "Deny":{"th":"ปฏิเสธ","en":"Deny"},
    "comfirm_create_pin":{"th":"กรุณาใส่รหัส ใบรับรองอิเล็กเทอรนิกส์รอีกครั้ง","en":"Please create a 6 digit PIN code agin."},

    "SkipEkyc":{"th":"ข้าม eKyc","en":"Skip eKyc"},
    "LOADING":{"th":"กำลังโหลด..","en":"LOADING.."},
    "X1":{"th":"","en":"LASER CODE"},
    "X1":{"th":"","en":"SELFIE"},
    "X1":{"th":"","en":"Record VDO"},
    "X1":{"th":"คุณได้คะแนนน้อยกว่า","en":""},
    "point":{"th":"คะแนน","en":"point"},
    "read":{"th":"อ่าน","en":"read"},
    "choose":{"th":"เลือก","en":"choose"},
    "ekyc_bar":{"th":"ยืนยันตัวตน (กรุณาเตรียมบัตรประชาชน)","en":"eKYC (Prepare your ID card)"},
    "verify_detail": {"th":"ยืนยันตัวตนสำเร็จ","en":"Verification success"},
    "processing": {"th":"รอการตรวจสอบ","en":"processing"},
    "pic_error": {"th":"ไม่สามารถประมวลผลได้<br>กรุณาถ่ายใหม่อีกครั้ง","en":"Cannot process your picture<br>Please try again"},
    "camera_error1": {"th":"ไม่สามารถทำการยืนยันตัวตนได้<br>เนื่องจากไม่พบกล้องถ่ายรูปบนเครื่องของคุณ","en":"Not Support<br>No camera found"},
    "camera_error2": {"th":"ไม่สามารถทำการยืนยันตัวตนได้<br>เนื่องจาก IOS VERSIO ต่ำกว่า 11","en":"Not Support<br>Your IOS VERSION < 11"},
    "camera_error3": {"th":"ยังไม่รองรับ Browser ของคุณ<br>กรุณาใช้ Safari","en":"Not Support your browser<br>Please use Safari"},
    "camera_error4": {"th":"ยังไม่รองรับ Browser ของคุณ<br>กรุณาใช้ Google Chrome","en":"Not Support your browser<br>Please use Goole Chrome"},
    "reason_error1": {"th":"ยืนยันตัวตนไม่สำเร็จ  ผลการตรวจสอบใบหน้าต่ำกว่า ","en":"Verification failed  Face match less than 50 %"},
    "reason_error2": {"th":"ยืนยันตัวตนไม่สำเร็จ","en":"Verification failed"},
    "remark_code1": {"th":"สถานะปกติ(ข้อมูลถูกต้อง)","en":"Correct information"},
    "remark_code2": {"th":"เลขบัตรประชาชนไม่ถูกต้อง","en":"CitizenID is not valid"},
    "remark_code3": {"th":"เลขหลังบัตรไม่ถูกต้อง","en":"Laser code is not valid"},
    "remark_code4": {"th":"ชื่อ นามสกุล หรือ วันเกิด ไม่ถูกต้อง","en":"Firstname or lastname or date of birth incorrect"},
    "remark_code5": {"th":"dopa มีปัญหา กรุณาติดต่อเจ้าหน้าที่","en":"dopa There is a problem, please contact"},
    "Edit_data": {"th":"แก้ไขข้อมูล","en":"Edit"},
    "ekyc_status_detail":{"th":"ระบบกำลังตรวจสอบการยืนยันตัวตนของคุณ<br>คุณจะได้รับผลการยืนยันตัวตน<br>จากระบบการแจ้งเตือน","en":"Processing<br>Your will get result from notifiacation"},
        "start_txt": {"th":"เริ่มทำ","en":"Start"},
    "takephoto":{"th":"โปรดถ่ายรูปเซลฟีของคุณ","en":"Please take your photo selfie."},
    "takephoto_small":{"th":"รูปถ่ายจะต้องชัด และไม่มืดจนเกินไป","en":"The photos must be clear. And not too dark"},
    "head_form_txt": {"th":"ตรวจสอบและแก้ไขข้อมูลให้ถูกต้อง","en":"Check and correct information."},
    "small_form_txt": {"th":"ตรวจสอบความถูกต้องของข้อมูลของคุณ","en":"Verify the accuracy of your information."},
    "cardNo":{"th":"เลขบัตรประจำตัวประชาชน 13 หลัก","en":"CARD NO"},
    "laserNo":{"th":"เลขหลังบัตรประชาชน","en":"Laser Code"},
    "faceMatch":{"th":"ผลการตรวจสอบใบหน้า","en":"Face Verification"},
    "Fname":{"th":"ชื่อจริง","en":"Firstname"},
    "Lname":{"th":"นามสกุล","en":"Lastname"},
    "gender":{"th":"เพศ","en":"Gender"},
    "male":{"th":"ชาย","en":"male"},

    "female":{"th":"หญิง","en":"female"},
    "dateOfBirth":{"th":"วัน/เดือน/ปี พศ. เกิด)","en":"Date Of Birth(16/06/2560)"},
    "Finish":{"th":"เสร็จสิ้น","en":"Finish"},
    "Finish_text":{"th":"ผลลัพท์การยืนยันตัวตน","en":"Identity verification results"},
    "Status":{"th":"สถานะบัตร","en":"Status"},
    "Status_ekyc":{"th":"สถานะการยืนยันตัวตน","en":"StaIdentity verification statustus"},
    "text_head_pass":{"th":"ใบรับอิเล็กทรอนิกส์ของคุณ","en":"Your electronic receipt"},
    "text_small_pass":{"th":"กรุณาตั้งรหัสตัวเลข 6 หลักสำหรับใช้งานใบรับรองอิเล็กทรอนิกส์","en":"Please set a 6-digit code for the electronic certificate."},
    "create_pin":{"th":"กรุณาสร้างรหัส PIN ตัวเลข 6 หลัก","en":"Please create a 6 digit PIN code."},
    "photoGrap":{"th":" ถ่ายรูป","en":" Take Photo"},
    "verification":{"th":"ตรวจสอบข้อมูล","en":"verification"},
    "cancle":{"th":" เลิกทำ","en":"Cancle"},
    "create_pin": { "th": "สร้าง PIN", "en": "Create PIN" },
    "create_signature": { "th": "สร้างลายเซ็น", "en": "Create Signature" },

     "profile_title": { "th": "ข้อมูลส่วนตัว", "en": "Profile" },
    "your_profile": { "th": "บัญชีของคุณ", "en": "Your Creden Account" },
    "img_profile": { "th": "รูปโปรไฟล์", "en": "Profile Picture" },
    "email": { "th": "อีเมล", "en": "Email" },
    "phone": { "th": "เบอร์โทรศัพท์", "en": "Phone" },
    "fname": { "th": "ชื่อ", "en": "First Name" },
    "lname": { "th": "นามสกุล", "en": "Last Name" },
    "mname": { "th": "ชื่อกลาง", "en": "Middle Name" },
    "save": { "th": "บันทึก", "en": "Save" },
    "cancel": { "th": "ยกเลิก", "en": "Cancel" },
    "your_sig": { "th": "ลายเซ็นของคุณ", "en": "Your Signature" },
    "your_initi": { "th": "ลายเซ็นแบบย่อของคุณ", "en": "Your Initials Sign" },
    "delete": { "th": "ลบ", "en": "Delete" },
    "create_sig": { "th": "สร้างลายเซ็น", "en": "Create Signature" },
    "pass_title": { "th": "รหัสผ่านและความปลอดภัย", "en": "Password & Security" },
    "change_pass": { "th": "เปลี่ยนรหัสผ่าน", "en": "Change password" },
    "now_pass": { "th": "รหัสผ่านเดิม", "en": "Old password" },
    "new_pass": { "th": "รหัสผ่านใหม่", "en": "New password" },
    "confirm_new_pass": { "th": "ยืนยันรหัสผ่านใหม่", "en": "Confirm new password" },
    "edit": { "th": "แก้ไข", "en": "Edit" },
    "q_change_phone": { "th": "คุณต้องการแก้ไขเบอร์โทร?", "en": "Do you confirm to edit the phone number?" },
    "confirm_phone": { "th": "กรุณายืนยันว่าโทรศัพท์มือถือเดิมหมายเลข", "en": "Please confirm whether this is your registered mobile number" },
    "still_active": { "th": "ยังใช้งานได้อยู่หรือไม่?", "en": "Is this still active?" },
    "inactive": { "th": "ใช้งานไม่ได้แล้ว", "en": "This is an inactive one" },
    "active": { "th": "ยังใช้งานได้อยู่", "en": "This is an active one" },
    "identity": { "th": "ยืนยันตัวตน", "en": "Identity Verification" },
    "pls_con_email": { "th": "กรุณากรอกรหัสที่ส่งไปยังอีเมล", "en": "Please enter the verification code sent to" },
    "pls_con_phone": { "th": "กรุณากรอกรหัสที่ส่งไปยังเบอร์โทร", "en": "Please enter the verification code sent to" },
    "dont_get_code": { "th": "คุณไม่ได้รับรหัส?", "en": "Don’t you get the verification code?" },
    "resend": { "th": "ส่งอีกครั้ง", "en": "Resend" },
    "confirm": { "th": "ยืนยัน", "en": "Confirm" },
    "change_phone_new": { "th": "เปลี่ยนเบอร์โทรศัพท์ใหม่", "en": "Change and register new mobile number" },
    "please_enter_new_phone": { "th": "กรุณากรอกหมายเลขโทรศัพท์ใหม่ของคุณ", "en": "Please enter your new mobile number" },
    "strong_password": { "th": "รหัสผ่านของคุณจะต้องมี:", "en": "Strong Password is required:" },
    "max_cha": { "th": "8 ตัวอักษรมากกว่า (สูงสุดไม่เกิน 16 ตัว)", "en": "Your password must be between 8 and 16 characters and is case-sensitive" },
    "upper_case": { "th": "ตัวอักษรพิมพ์ใหญ่และพิมพ์เล็ก", "en": "Your password must contain a minimum of one uppercase letter (A-Z) and a minimum of one lowercase letter (a-z)" },
    "numeric": { "th": "อย่างน้อยหนึ่งตัวเลข (0-9)", "en": "Your password must contain a minimum of one numeric character (0-9)" },
    "confirm_pass": { "th": "ยืนยันเปลี่ยนรหัสผ่าน", "en": "Confirm Password Change" },
    "draw_sign": { "th": "วาดลายเซ็น", "en": "Draw signature" },
    "gen_sign": { "th": "สร้างลายเซ็น", "en": "Ganerate signature" },
    "font": { "th": "ฟอนต์", "en": "Font" },
    "select_font": { "th": "เลือกฟอนต์", "en": "Select Font" },
    "draw_initi": { "th": "วาดลายเซ็นย่อ", "en": "Draw Initials" },
    "gen_initi": { "th": "สร้างลายเซ็นย่อ", "en": "Generate Initials" },
    "clear": { "th": "ล้าง", "en": "Clear" },    
    "line_noti": {"th": "การแจ้งเตือนผ่าน LINE", "en": "LINE Notification"},
    "connect": {"th": "เชื่อมต่อ", "en": "Connect"},
    "connectYourLine": {"th": "เชื่อมต่อเข้ากับ LINE ID ของคุณ", "en": "Link with your LINE ID"},
    "addLine": {"th": "เพิ่มเพื่อน", "en": "Add friend"},
    "connectLine": {"th": "เชื่อมต่อ", "en": "Connect"},
    "Notification": {"th": "การแจ้งเตือน", "en": "Notification"},
    "genQR": {"th": "สร้าง token ใหม่", "en": "Create new token"},
    "takeQR": {"th": "ถ่ายภาพ QR code ส่งเข้า", "en": "Take photo of QR code and send it via"},
    
    
    };

//------------
