local upload = require "resty.upload"
local cjson = require "cjson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local prog1 = exec.new('/tmp/exec.sock')
local prog1b = exec.new('/tmp/exec.sock')

prog.timeout_fatal = false
prog1.timeout = 1000 * 60 * 30
prog1.timeout_fatal = false
prog1b.timeout_fatal = false
 local chunk_size = 8192 -- should be set to 4096 or 8192

function fileext(file)
	local i = 0
		while(true) do
			local i2 = file:find( '%.', i+1)
			if(i2) then
				i=i2
			else
				break
			end
		end
		local ret =file:sub(i)
		return ret
	
end


function mysplit (inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        local t={}
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                table.insert(t, str)
        end
        return t
end

function check_filename(file)
local ret = false
local name = file:lower()
local ext = fileext(name)


local white_list = {'.png', '.jpg', '.jpeg', '.pdf', '.doc', '.docx', '.xls', '.xlsx'}
for i,v in ipairs(white_list) do
   if(ext==v)then 
     ret = true 
     break
   end
end

ngx.log(ngx.NOTICE, 'Check filename', file , ' ext' , ext, ' ret = ', ret)
return ret
end

 function check_files( file_name , str)
   --rc:set('attachments', str)
   ngx.log(ngx.WARN, 'ATTACHMENTS >>>> ',  file_name , ' len >> ' , #str)
   if(#str>1)then
        ngx.log(ngx.WARN, 'ATTACHMENTS ', 'STR')
        local t = mysplit(str,'\n')
        ngx.log(ngx.WARN, 'ATTACHMENTS >>>> ',  't.len = ', #t)
	for i,v in ipairs(t) do
           ngx.log(ngx.WARN, 'ATTACHMENTS', i , ')', v)
	   if(i>1)then
               local file = v
	       if(not check_filename(v))then
                    local ret = {success = false, error_msg = "Unsupported Attachment", name = 'N/A', file_name  = 'N/A'}
                    ngx.say(cjson.encode(ret))
		    ngx.exit(0)

	       end
	   end
	end
    end

 end

 
function str_split(str, sep)
if sep == nil then
sep = '%s'
end

local res = {}
local func = function(w)
table.insert(res, w)
end

string.gsub(str, '[^'..sep..']+', func)
return res
end

function change_order(c)
local t = str_split(c,'\n')
local t2 = {}
for i,v in ipairs(t) do
if(i>1)then
  table.insert(t2,v)
end
end
table.insert(t2,t[1])
t = t2
t[#t] = string.gsub(t[#t],".png", "-".. (#t-1) .. ".png")

 
local res = table.concat(t,"\n")
return res
end

function rand(len)
local cmd = "od -vAn -N4 -tu4 < /dev/urandom"
local t = {}
local cnt = 0

local i = 1
repeat
 local f = assert (io.popen (cmd, 'r'))
 local line = f:read('*all'):match("%d+")
 f:close()
 cnt = cnt + #line
 table.insert(t, line)
until cnt>len


local ret = table.concat(t):gsub("\n",""):gsub(" ",""):sub(1,len)
return ret
end
 



local file
local name = rand(8) .. tostring(os.time())
local session = require "resty.session".open({secret = "4321"})
ngx.log(ngx.NOTICE,'---------------------------------- START [UPLOAD PDF] ---------------------------------- SPEED')
ngx.log(ngx.NOTICE,'SPEED CDECK ',' ', name   ,' ','START',' ',  session.data.username )
local file_name  = '/home/creden/work/app/client/face/images/card/' .. name .. ".pdf"
local file_name2 = '/home/creden/work/app/client/face/images/card/pdf' .. name
local file_name3 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. '*.png'
local UPLOAD_PHOTO = false

local upload_key = ngx.req.get_headers()['X-Upload-Key']

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)

local form, err = upload:new(chunk_size)
if not form then
   ngx.say "no form" 
end






form:set_timeout(1000) -- 1 sec
file = io.open(file_name, "w+")



local len = 0
while true do
local typ, res, err = form:read()
if not typ then
ngx.say("failed to read: ", err)
return
end

if(typ=='header')then
local file_header = res[2]
local i, j = string.find(file_header,"filename=")
if j then
local file_name = string.sub(file_header,j+2,string.len(file_header)-1):lower()
if(file_name:find('.png') or file_name:find('.jpg') or file_name:find('.jpeg') ) then
	UPLOAD_PHOTO = true
end
end

end

if(typ=='body')then
len = len + #res
file:write(res)
end
--ngx.say("read1: ", cjson.encode({typ, res}))

if typ == "eof" then
file:close()
break
end
end

local typ, res, err = form:read()


ngx.log(ngx.NOTICE,"FINISHED CMD UPLOADING")

--check attachments
if(not UPLOAD_PHOTO)then
  local res,err = prog('/bin/sh', '/home/creden/work/app/server/capi/check_attachments.sh'  , name..'.pdf') 
  ngx.log(ngx.WARN, 'check_attachment_error ATTACH', err , res.stderr)
  check_files(  name..'.pdf' , res.stdout)
end
--end check attachments


local ret2 = {success = true, error_msg = "", name = name, file_name  = file_name, upload_key = upload_key , UPLOAD_PHOTO = UPLOAD_PHOTO}
ngx.say(cjson.encode(ret2))

--local api2 = 'http://127.0.0.1:8124/capi/upload_pdf_bg' --todo eark
local api2 = 'http://127.0.0.1:8080/capi/upload_pdf_bg' --todo eark
--prog('curl', '-X', 'POST', '--data' , cjson.encode(ret2)  api2)
local api2_cmd = "curl -X POST --data '" .. cjson.encode(ret2) .. "' " .. api2 .. ' &'
ngx.log(ngx.NOTICE , api2_cmd)
os.execute(api2_cmd)
-------------------------------------------------------------------------------------------------




