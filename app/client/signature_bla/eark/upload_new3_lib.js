var esign_path = document.location.origin + '/bla' //todo eark move loc

////window.alert = function(x){   //console.log(x+'<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<123')}
function click_tab(show) {
    //console.log('click_tab B')

    if (show) $(".full-name-sign").show();
    else $(".full-name-sign").hide();
}
 
function upload_vue_original() {
    //alert('xxxxxxxxxxxxxxxx_package=' + _package.isAdvancedFunctionEnabled)
    var obj1 = {
            el: '#app',
            data: {

                // adjustedWidth : 1200,
                // totalHeight : 0,
                // pageDim : [],
                // master_group : null,
                // draw : null,
                // draggingTool : null,
                current_fac: 0,
                lang_choose: localStorage.getItem('lang_choose') || 'th',
                lang_txt: lang_step1,
                lang_page: lang_page,
                count_field: 0,
                check_choose_signature: false,
                show_initia_draw: true,
                show_initia_gen: true,
                select_initia_sing: '',
                sign_name_initial: '',
                initia_draw: "../edoc/app-assets/images/signature/" + getUsername() + "_draw_initial.png",
                initia_gen: "../edoc/app-assets/images/signature/" + getUsername() + "_gen_initial.png",
                myProfile: "",
                myFirstname: getUserField("adminFisrtname") || "",
                myLastname: getUserField("adminLastname") || "",
                myEmail: getUserField("adminEmail") || "",
                selected_template: "",
                zoomLevel: 1,
                activeSvg: null,
                //draggingTool: null,
                
                step: 1,
                list_file_pdf: [],
                subject: '',
                originalFile: '',
                pdfFile: '',
                all_original_filenames: {},
                imageFiles: [],
                display_imageFiles: [],
                build_pdf: false,
                imageMerge: '',
                all_images: {},
                templates: [],
                attachments: [],
                previewImgList: [],
                doc_id: '',
                sign_only: false,
                use_signing_order: false,
                people: [{ user_name: getUserField('adminFisrtname') + " " + getUserField('adminLastname') ,name: '', email: '', position_id: '', needToSign: true, eKyc: false, usePassword: false, password: '', title: '', order_set: '', delegates_sign: false, delegates_mail: '', delegates_name: "", is_group: false, user_group: [], least_sign: true, amount_least_sign: 1, is_inspector: false, toolName: '' }],
                is_template: false,
                upload_path: upload_path,
                totalPages: 0,
                currentPage: 0,
                //people
                selectedPersonName: "เลือกผู้เซ็น",
                selectedPersontitle: null,
                selectedPersonPositionId: null,
                selectedPersonObj: null,
                exp_date: new Date(),
                count_date: 120,
                min_date: new Date(),
                msg: "",
                chk_set_field: '',
                show_set_detail: false,
                selectedField: [{ font: 'THSarabunNew', size: '16', is_bold: false, is_italics: false, is_underline: false }],
                font: 'THSarabunNew',
                size: '16',
                toolOptionsArray: [], // for dropdown
                dropdownDefaultOption: '', // for dropdown
                group_sign: [],
                user_email: getUsername(),
                user_name: getUserField('adminFisrtname') + " " + getUserField('adminLastname') || getUsername(),
                set_oreder_user: 0,
                members: [],
                search_member_text: '',
                search_members: [],
                select_person_private_mail: 0,
                private_mail: '',
                color: [{ color: '#EB5757' }, { color: '#F2994A' }, { color: '#F2C94C' }, { color: '#2F80ED' }, { color: '#9B51E0' }, { color: '#219653' },
                    { color: '#F39B9B' }, { color: '#FFC999' }, { color: '#F8E2A0' }, { color: '#56CCF2' }, { color: '#D09AE5' }, { color: '#6FCF97' }
                ],
                history_sign: [],
                history_show: true,
                draft_id: gup('id') || '',
                imageAspectRatios: [],
                alert_no_pdf: false,
                text_alert: '',
                alert_step_2: false,
                alert_step_4: false,
                myLogo: '',
                powered_by: "",

                _id: '',
                req_attach: true,
                selectgroup: '',
                selectgroupdep: '',
                
               
                selecthis: '',
                show_list_member: [],
                show_list_members: [],
                template_id: gup('template_id') || '',
                sign_type: gup('sign_type') || 2,
                status_finish_btn: true,
                list_remove_field: [],
                remove_field_order: 0,
                chk_mail: true,
                chk_format_mail: [],
                url_viewer: '',
                setting_pdf: [],
                name_tag_folder: "Public",

                //secret
                secret_level: 0,
                can_use_iso: false,
                warning_company_mail: [],

                // CC
                list_cc: [],
                show_list_cc: [],
                show_list_cc_group: [],
                id_group_cc: '',
                name_group_cc: '',
                search_list_cc: '',
                index_num_list_cc: '',

                // Field Note
                field_note: [],
                hide_note: false,
                note_field: null,

                // ไม่ระบุผู้เซ็น
                is_no_sign: false,
                toolSignature: true,
                toolEmail: true,
                toolDate: true,
                toolName: true,
                toolText: true,
                toolAttachment: true,
                toolPhone: true,

                // clone all
                is_clone: false,
                id_field: '',
                disabled_approve: false,

                delegate_email: "",
                delegate_name: "",

                status_seclect:true,
                status_array:[],
                status_checkbox:'',

                list_member_department:[],
            },
            mounted: function() {
                window.myApp = this
                window.upload = this

                this.getProfile()
                this.upload_file()
                this.get_group()
                this.get_members()
                this.min_date = new Date(this.min_date.setDate(this.min_date.getDate() + 1))
                this.min_date = this.min_date.getFullYear() + "-" + this.$options.filters.set_zero_text((this.min_date.getMonth() + 1)) + "-" + this.$options.filters.set_zero_text(this.min_date.getDate())
                this.set_font_stlye(0, null)
                this.myProfile = this.getProfile()
                this.getUser()

                extendSession()
                if (this.draft_id !== '') { this.getDraft() }
                if (this.template_id !== '') { this.getTemplate() }

                new ClipboardJS('#btncopy');
                this.init_sigpad()

            },
            filters: {
                set_zero_text(v) {
                    var txt
                    var n = parseInt(v)
                    if (n < 10) {
                        txt = '0' + v
                    } else {
                        txt = v
                    }
                    return txt
                },
            },
            methods: {
                getTemplate() {
                    //console.log('getTemplate')
                    that = this
                    $.ajax({
                        type: "GET",
                        url: api_host + "get_template" + "?id=" + this.template_id,
                        dataType: "json",
                        success: function(res) {
                            //console.log(res)
                            that.all_images = res.data.all_images
                            that.all_original_filenames = res.data.all_original_filenames
                            that.display_imageFiles = res.data.display_imageFiles
                            that.imageFiles = res.data.imageFiles
                            that.imageMerge = res.data.imageMerge
                            that.list_file_pdf = res.data.list_file_pdf
                            that.originalFile = res.data.originalFile
                            that.owner = getUsername()
                            that.pdfFile = res.data.pdfFile
                            that.select_sign_only(that.sign_type)
                        },
                        error: function(request, status, error) {
                            alert("|" + status + ' |' + error);
                        }
                    });
                },
                getDraft() {
                    that = this
                    if (this.draft_id === '') return
                    $.ajax({
                        type: "GET",
                        url: api_host + "get_draft" + "?id=" + this.draft_id,
                        dataType: "json",
                        success: function(res) {

                            //alert('before'+.isAdvancedFunctionEnabled)    

                            if (res.data !== {}) {
                                res.data.draftJSON.lang_choose = that.$data.lang_choose
                                res.data.draftJSON.lang_txt = that.$data.lang_txt
                                res.data.draftJSON.color = that.$data.color

                                Object.assign(that.$data, res.data.draftJSON)


                                //Fixed type
                                if (that.fields.length === undefined) {
                                    //console.log('FIELD TYPE OBJ')
                                    that.fields = []
                                }

                                if (that.history_sign.length === undefined) {
                                    //console.log('history_sign TYPE OBJ')
                                    that.history_sign = []
                                }

                                that.setFieldsDraft()
                                    // that.update_group_draf()
                            }
                        },
                        error: function(request, status, error) {
                            alert("|" + status + ' |' + error);
                        }
                    });
                },
                update_group_draf: function() {
                    that = this
                    if (this.draft_id === '') return
                    var group = []
                    var get_company_id = getUserField('company_id')
                    var data = { company_id: get_company_id }
                    $.ajax({
                        type: "POST",
                        url: api_host + "get_group_sign",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function(res) {
                            if (res.success) {
                                group = res.data
                                that.group_sign = res.data

                                for (i = 0; i < that.people.length; i++) {
                                    if (that.people[i].is_group) {

                                        var chk_group = group.filter(function(g) {
                                            return that.people[i].id_group == g._id
                                        })
                                        //console.log(chk_group);
                                        that.people[i].user_group = chk_group[0].group_sign
                                    }
                                }
                            }

                        },
                        error: function(request, status, error) {
                            //console.log("|" + status + ' |' + error);
                        }
                    });



                },
                setFieldsDraft() {
                    that = this
                    if (that.step === 3 && that.draft_id != '') {

                        start_step3('3d')
                            /*
                            setTimeout(function(){ 
                                for (i = 0; i < that.fields.length; i++) {
                                    //const {x, y} = that.calsvgXY(SVG('#svg' + that.fields[i].page), that.fields[i].xp, that.fields[i].yp)
                                    const {x, y} = that.calsvgXY(SVG('#svg'), that.fields[i].xp, that.fields[i].yp, that.fields[i].page)
                                    //that.transferFieldGroup(that.fields[i], x, y) //eark mod
                                   
                                    that.transferFieldGroup(that.fields[i], x, y)
                                }
                            }, 5000)*/
                    } else {
                        //this.put_fields_again()  
                    }
                },

                getProfile: function() {
                    var p = getUserField("profile_pic") || ""
                    var f = p.search("http")
                    var img = ""

                    if (f == 0) img = p
                    else if (f == -1 && p != "") img = "../face/images/card/" + getUserField("profile_pic") + ".png"
                    else img = "images/profile.png"
                    return img
                },

                setZoomLevel: function(level) {
                    alert('not implemented22');
                    return
                    level = parseFloat(level)
                    this.zoomLevel = level
                    for (var i = 0; i < this.imageFiles.length; i++) {
                        this.init_svg(i)
                    }

                },
                get_name: function() {
                    if (getUserField('adminFisrtname') == '') {
                        this.FNAME = getUserField('adminFisrtname')
                    }
                },

                addField: function(field, n) {
                    alert('this is not used !!!!!!')
                    //console.log(' addField ')
                    //console.log(field)
                    var arr = this.fields
                    var duplicated = false



                    for (var i = 0; i < this.fields.length; i++) {
                        if (this.fields[i].id == field.id) {
                            duplicated = true
                                // alert('duplicated ' + field.id)
                        }
                    }

                    if (!duplicated) {
                        arr.push(field);
                        if (!field.email) {
                            //console.log('ERROR ' + field.email + ' ' + field.toolName + ' ' + upload.selectedPerson);
                            field.email = upload.selectedPerson
                                ////console.log("Add field !!!! >>> " + JSON.stringify(field));
                        }



                        this.delField = field;
                    }
                    this.fields = arr
                    //console.log(this.fields)

                },
                close_setting: function() {
                    this.show_set_detail = false
                        // alert(1)
                },
                removeField: function(field) {
                    //console.log(' removeField before ' + this.fields.length)
                    upload.fields.forEach(f => { //console.log(f.id) })

                    var arr = []
                    for (var i = 0; i < this.fields.length; i++) {
                        if (this.fields[i].id != field.id) {
                            arr.push(this.fields[i])
                        }
                    }
                    this.fields = arr
                    //console.log(' removeField after ' + this.fields.length)
                    this.check_fiels_no_sign(field.toolName)
                    upload.fields.forEach(f => { //console.log(f.id) })
                },

                deleteField: function() {
                    if (this.step != 3) {
                        return;
                    }
                    //console.log(' deleteField *** from UI')
                    upload.show_set_detail = false
                    if (upload.fields.length < 1) {
                        //console.log(' skip deleteField')
                        return;
                    }
                    var field = this.delField
                    //console.log(field)
                    if (field.toolName == 'Approve') {
                        this.disabled_approve = false
                    }
                    if (field.toolName == 'choice') {
                        //console.log(this.radioGroups[this.groupRadioDel].items)
                        var noDelRadio = []
                        this.radioGroups[this.groupRadioDel].items.forEach(rect => {
                            //console.log(rect.id)
                            if (rect.id != this.radioIdDel) {
                                noDelRadio.push(rect)
                            }
                        });
                        this.radioGroups[this.groupRadioDel].items = noDelRadio
                        //console.log(this.radioGroups[this.groupRadioDel].items)
                        this.computeRadioGroupBoundary(this.groupRadioDel, this.groupPlusDel)

                        if (this.radioGroups[this.groupRadioDel].items.length == 0) {

                            var choiceParentField = upload.field(field._parent_id)
                            choiceParentField.group.remove()
                            upload.removeField(choiceParentField)

                        }

                    }

                    if (field && field.group) {
                        field.group.remove()
                    }
                    if (field) {
                        this.removeField(field)
                    }
                },


                onFieldAdded: function(field, i) {
                    //console.log("onFieldAdded#" + i)
                    var svg = SVG('#svg' + i)
                    //console.log(svg)
                    //console.log(svg.x())
                    var x1 = svg.x()
                    var y1 = svg.y()

                    var x2 = field.group.x()
                    var y2 = field.group.y()

                    var xp = ((x2 - x1) * 100) / svg.width()
                    var yp = ((y2 - y1) * 100) / svg.height()

                    //console.table(x1 + "," + y1 + "\n" + x2 + "," + y2 + "+\n" + xp + "%," + yp + "%")
                    field.xp = xp
                    field.yp = yp
                        //console.table(field)
                    copyStyle(field)

                    //alert('onFieldAdded')

                },

                finish: function() {
                    var that = this
                    Swal.fire({
                        icon: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#4785D0',
                        cancelButtonColor: '#d33',
                        confirmButtonText: that.lang_txt.confirm_send[that.lang_choose],
                        cancelButtonText: that.lang_txt.cancel[that.lang_choose],
                        title: that.lang_txt.want_to_send[that.lang_choose],
                        text: that.lang_txt.want_to_sendDes[that.lang_choose],
                    }).then((result) => {
                        if (result.isConfirmed) {
                            this.status_finish_btn = false
                            var person = []
                            var person_needtosign = that.people.filter(function(p) {
                                return p.needToSign == true && p.is_inspector == false
                            })
                            for (i = 0; i < person_needtosign.length; i++) {
                                var p = {}
                                if (person_needtosign[i].is_group && person_needtosign[i].is_group == true) {
                                    for (j = 0; j < person_needtosign[i].user_group.length; j++) {
                                        p = { email: person_needtosign[i].user_group[j].email, is_viewed: false, name: person_needtosign[i].user_group[j].name }
                                        person.push(p)
                                    }
                                } else if (person_needtosign[i].delegates_sign == true) {
                                    p = { email: person_needtosign[i].delegates_mail, is_viewed: false, name: person_needtosign[i].delegates_mail }
                                    person.push(p)
                                } else {
                                    p = { email: person_needtosign[i].email, is_viewed: false, name: person_needtosign[i].name }
                                    person.push(p)
                                }

                            }
                            //console.log(person);

                            var n = 0
                            var d = new Date();
                            var t = d.getTime();
                            for (f = 0; f < that.fields.length; f++) {
                                //console.log("toolName+++++++++" + that.fields[f].toolName);
                                if (that.fields[f].is_no_sign) {
                                    if (that.fields[f].is_no_sign == true) {

                                        // if(that.fields[f].note_of){
                                        //     that.fields[f].person.concensus_id  = "Noteof" + t
                                        // }else{
                                        //     that.fields[f].person.concensus_id  = that.fields[f].toolName + t
                                        // }
                                        that.fields[f].person.concensus_id = that.fields[f].toolName + t
                                        that.fields[f].person.is_group = true
                                        that.fields[f].person.user_group = person

                                        //console.log("concensus_id+++++++++" + that.fields[f].person.concensus_id);
                                        n++
                                    }
                                } else {
                                    n++
                                }
                            }


                            that.set_data_doc()
                                // saveEdoc(doc)

                            // if (n == that.fields.length) {
                            //     setTimeout(function () {
                            //         saveEdoc(doc)
                            //     }, 2000);

                            // }



                        }
                    })

                },
                set_data_doc: function() {
                    var doc = toEdoc(this, this.imageAspectRatios)
                    for (i = 0; i < doc.signers.length; i++) {
                        var strmail = doc.signers[i].email;
                        strmail = strmail.toLowerCase();
                        doc.signers[i].email = strmail;
                    }
                    for (i = 0; i < doc.fields.length; i++) {
                        var strmail = doc.fields[i].email;
                        strmail = strmail.toLowerCase();
                        doc.fields[i].email = strmail;

                        if (doc.fields[i].toolName == "Qrcode") {
                            var url = document.location.origin
                            doc.fields[i].toolData = url + "/bla/#/document?id=" + doc.id
                        }

                    }
                    doc.setting_pdf = this.setting_pdf
                    doc.list_cc = this.list_cc
                    doc.screener_approve = false
                    saveEdoc(doc)
                },
                calsvgXY(svg, xp, yp, pageIndex) { //eark mod
                    //console.log('calsvgXY')
                    //console.log(svg)
                    svgX = svg.x()
                    svgY = svg.y()

                    //alert(JSON.stringify(this.pagesDim))

                    var dim = this.pagesDim[pageIndex]
                    w = dim.width
                    h = dim.height
                    var x = ((xp * w) / 100) + svgX
                    var y = ((yp * h) / 100) + svgY
                    var yOrigin = 0;

                    for (var i = 0; i < this.pagesDim.length; i++) {
                        var dim = this.pagesDim[i]
                        if (i < pageIndex) { yOrigin += dim.height }
                    }

                    y = y + yOrigin





                    return { x, y }
                },

                saveDraft() {
                    $("#draftModal").modal("show");
                },

                discradDraft() {
                    document.location.href = '/bla/#/dashboard'
                },

                submitDraft() {
                    //console.log('saveDraft')
                    that = this
                    if (this.display_imageFiles.length === 0) {
                        document.location.href = '/bla/#/dashboard'
                    } else {
                        if (this.draft_id === '') this.draft_id = Date.now() + '' + Math.floor(Math.random() * 10000000)
                        //console.log(this.imageAspectRatios)
                        const doc = toEdoc(this, this.imageAspectRatios)
                        const url = api_host + 'save_edoc_company_draft'
                        doc.id = this.draft_id
                        doc.is_template = upload.is_template
                        doc.document_type_id = upload.selected_template
                        doc.exp_date = upload.exp_date
                        doc.sign_only = upload.sign_only
                        doc.name = upload.user_name
                        doc.draftJSON = {}
                        Object.assign(doc.draftJSON, that.$data);
                        doc.draftJSON.imageAspectRatios = this.imageAspectRatios

                        for (i = 0; i < doc.draftJSON.fields.length; i++) {
                            doc.draftJSON.fields[i].group = null
                        }

                        doc.draftJSON.activeSvg = null
                        doc.draftJSON.delField = null
                        doc.draftJSON.lang_txt = null
                        doc.draftJSON.color = null
                        doc.draftJSON.lang_choose = null
                        doc.draftJSON.master_group = null
                        doc.draftJSON.draw = null

                        that.subjectDraft = that.originalFile

                        const data = JSON.stringify(doc).replaceAll("null", '""')

                        $.ajax({
                            url: url,
                            type: "POST",
                            data: data,
                            success: function(result) {
                                document.location.href = '/bla/#/draft'
                            },
                            error: function(error) {
                                //console.log("Save draft error: " + error)
                            }
                        })
                    }
                },
                createChoice: function(fieldPlus, x, y, group_id, gp) {
                    // Set View 
                    var svg = SVG('#svg' + fieldPlus.page)
                    var group = svg.group()
                    var color = this.getPersonColor()
                    var rect = svg.rect(20, 20).fill(color)
                    var text = svg.text('X' || "N/A")
                    group.add(rect)
                    group.add(text)
                    group.move(x, y)
                    group.draggable()

                    // Set Field Choice
                    // this.radioGroups[group_id].items.push({group:group,id:''})
                    var field = clone_field(fieldPlus, fieldPlus.page)
                    field["toolName"] = "choice"
                    field["group"] = null
                    this.addField(field)
                    this.radioGroups[group_id].items.push({ group: group, id: field.id })
                        // app.onFieldAdded(field["toolName"], field.page) 
                    window.rdg = this.radioGroups
                        //attach event
                    var _id = group_id
                    upload.computeRadioGroupBoundary(_id, gp)
                    group.on('click.namespace', (e) => {
                        app.delField = field
                        this.delField = field
                        field["group"] = group
                        this.radioIdDel = field.id // id field radio ที่ต้องการลบ
                        this.groupRadioDel = _id // id group radio ที่ต้องการลบ
                        this.groupPlusDel = gp // ปุ่มบวกของ radio ที่ต้องการลบ
                        //console.log(this.radioIdDel)
                    })

                    group.on('dragmove.namespace', (e) => {
                        upload.computeRadioGroupBoundary(_id, gp) //eark 
                        app.delField = field
                        this.delField = field
                        field["group"] = group
                        this.radioIdDel = field.id
                        this.groupRadioDel = _id
                        this.groupPlusDel = gp
                        //console.log(this.radioIdDel)
                    })


                    //end

                },

                computeRadioGroupBoundary: function(groupId, gp) {
                    var g = this.radioGroups[groupId]
                        //var svg = null 
                    var x1 = 9999
                    var y1 = 9999

                    var x2 = 0
                    var y2 = 0
                    //console.log(g.items)
                    g.items.forEach(rect => {
                        if (rect.group.x() < x1) { x1 = rect.group.x() }

                        if (rect.group.y() < y1) { y1 = rect.group.y() }


                        if (rect.group.x() > x2) { x2 = rect.group.x() }

                        if (rect.group.y() > y2) { y2 = rect.group.y() }

                    });
                    //console.log(x1, y1, x2, y2)

                    var color = 'green'
                    var svg = g.svg
                    var rect_bg = g.rect_bg || svg.rect().fill(color)
                    g.rect_bg = rect_bg

                    rect_bg.size(x2 - x1 + 20, y2 - y1 + 20)
                        // rect_bg.opacity(0.1)
                    rect_bg.move(x1, y1)
                        // rect_bg.back()
                    rect_bg.attr({
                        fill: '#f06',
                        'fill-opacity': 0.1,
                        'stroke': '#5184AF',
                        'stroke-dasharray': "5 10"
                    })

                    //console.log('ggg>>')
                    //console.log(g)
                    g.items.forEach(rect => {
                        radio = rect.group
                        radio.front()
                    });
                    gp.move((x2 + x1) / 2, y2 + 30)
                },
                /*
                showRadio2: function (i) {
                    //console.log('showRadio22222222')
                    // create Radio
                    var field = this.draggingTool
                    var color = this.getPersonColor()
                    var svg = SVG('#svg' + i)
                    var group = svg.group()
                    var rect = svg.rect(20, 20).fill(color)
                    var text = svg.text('X')
                    group.add(rect)
                    //group.add(c4)
                    
                    //group.add(text).hide()
                    group.add(text)

                    field.group = group
                    field.page = i
                    //set id Radio Groups 
                    var rGroup = {
                        id: new Date().getTime() + "",
                        items: [], svg: svg, rect_bg: null
                    }
                    this.radioGroups[rGroup.id] = rGroup

                    group.on('click.namespace', (e) => {
                        // var app = window.myApp

                        // if (app.draggingTool) { // First Drop From Panel
                        //     app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                        //     app.draggingTool = null
                        // }
                        // else { // Click event after drop from panel
                        //     var rectX = e.target.getBBox().x
                        //     var rectY = e.target.getBBox().y
                        //     this.createChoice(field, rectX, rectY, rGroup.id, group)
                        // }
                    })
                },
                */
                showRadio: function(i) {
                    // create Radio
                    var field = this.draggingTool
                    var color = this.getPersonColor()
                    var svg = SVG('#svg' + i)
                    var group = svg.group()
                    var rect = svg.rect(20, 20).fill(color)
                    var text = svg.text('+')
                    group.add(rect)
                        //group.add(c4)
                    group.add(text).hide()
                    field.group = group
                    field.page = i
                        //set id Radio Groups 
                    var rGroup = {
                        id: new Date().getTime() + "",
                        items: [],
                        svg: svg,
                        rect_bg: null
                    }
                    this.radioGroups[rGroup.id] = rGroup

                    group.on('click.namespace', (e) => {
                        var app = window.myApp

                        if (app.draggingTool) { // First Drop From Panel
                            app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                            app.draggingTool = null
                        } else { // Click event after drop from panel
                            var rectX = e.target.getBBox().x
                            var rectY = e.target.getBBox().y
                            this.createChoice(field, rectX, rectY, rGroup.id, group)
                        }
                    })
                },
                showTool: function(i) {
                    //console.log(i)
                    var field = this.draggingTool
                    var color = this.getPersonColor()

                    var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                    var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;

                    if (field.toolName == 'Radio') {
                        w = 10
                        h = 10
                    }

                    var svg = SVG('#svg' + i)
                    var group = svg.group()
                    var rect = svg.rect(w, h).fill(color)
                    var text = svg.text(field.toolName || "N/A")

                    var cr = 10 //circle redius
                        //var c4 = svg.circle(10).fill('red').move(w-cr/2, h-cr/2)


                    group.add(rect)
                        //group.add(c4)
                    group.add(text).hide()
                    group.draggable()
                    field.group = group
                    field.page = i

                    group.on('dragend.namespace', (evt) => {
                        var app = window.myApp
                        app.draggingTool = field // eark
                        app.onFieldAdded(app.draggingTool, i)
                        app.draggingTool = null
                        //console.log(field)
                    })

                    group.on('dragmove.namespace', (e) => {
                        const { handler, box } = e.detail
                        const constrains = document.getElementById('svg' + i)
                        max_y = constrains.attributes.height.value
                        max_x = constrains.attributes.width.value
                            // e.preventDefault()
                        let { x, y } = box

                        if (x < 0) {
                            e.preventDefault()
                            x = 0
                        }
                        if (box.x2 > max_x) {
                            //if (box.x2+70 > max_x) {    
                            e.preventDefault()
                            x = max_x - box.w - 5
                        }
                        if (y < 0) {
                            e.preventDefault()
                            if (i == 0) {
                                y = 0
                            } else {
                                //console.log('can previous up showTool')
                                    // alert('can previous up showTool')
                                var newField = clone_field(field, i - 1)
                                const constrains_prev = document.getElementById('svg' + i)
                                this.transferFieldGroup(newField, x, constrains.attributes.height.value - box.h - 5)
                                group.remove()
                                return;
                            }
                        }
                        if (box.y2 > max_y) {
                            e.preventDefault()
                            next_page = (parseInt(i) + 1)
                            have_next = document.getElementById('svg' + next_page)
                            if (have_next == null) {
                                y = max_y - box.h - 5
                            } else {
                                //console.log('can next page showTool')
                                    // alert('can next page showTool')                          
                                var newField = clone_field(field, next_page)
                                this.transferFieldGroup(newField, x, 0)
                                group.remove()
                                return;
                            }
                        }
                        handler.move(x, y)
                            // app.draggingTool = field
                        app.delField = field
                        copyStyleUI(field)
                    })
                },
                transferFieldGroup: function(field_clone, x1, y1) {
                    // alert('a2')
                    field_clone.group = null
                        // alert('transferFieldGroup')
                    if (this.tmp_id_transfer == JSON.stringify(field_clone)) {
                        return;
                    } else {
                        tmp_id_transfer = JSON.stringify(field_clone)
                    }
                    //console.log('transferFieldGroup is called.....')
                    this.prepare_new_tool(2, field_clone, x1, y1)
                },


                updateDrpodownDefaultOption: function() {
                    this.delField.dropdownDefaultOption = this.dropdownDefaultOption
                },

                updateDrpodownOption: function(newVal, ele) {
                    var index = $(ele).data('index')
                    this.$set(this.toolOptionsArray, index, newVal);

                    this.delField.toolOptions = this.toolOptionsArray.join()
                    //console.log('updateDropdownOption', this.delField.toolOptions)

                },

                addDropownOption: function(i) {
                    this.toolOptionsArray.push('')
                    this.delField.toolOptions = this.toolOptionsArray.join()
                },

                delDropownOption: function(n) {
                    var arr = []
                    for (var i = 0; i < this.toolOptionsArray.length; i++) {
                        if (i != n) { arr.push(this.toolOptionsArray[i]); }
                    }
                    this.toolOptionsArray = arr
                    this.delField.toolOptions = this.toolOptionsArray.join()

                },

                _showTool: function(i) {
                    var field = this.draggingTool
                    var color = this.getPersonColor()

                    var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                    var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;

                    var svg = SVG('#svg' + i)
                    var group = svg.group()
                    var rect = svg.rect(w, h).fill(color)
                    var text = svg.text(field.toolName || "N/A")

                    var cr = 10 //circle redius
                        //var c4 = svg.circle(10).fill('red').move(w-cr/2, h-cr/2)


                    group.add(rect)
                        //group.add(c4)
                    group.add(text).hide()
                    field.group = group
                    field.page = i



                    group.click(function() {
                        var app = window.myApp

                        if (app.draggingTool) {
                            app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                            app.draggingTool = null
                        } else {
                            //  alert('def')

                            app.draggingTool = field
                            app.delField = field

                            copyStyleUI(field)

                            //console.log("reset draggingTool to " + field)

                        }
                    })



                },
                set_font_stlye: function(a, ele) {
                    var that = this
                    that.selectedField[0].font = that.font || 'THSarabunNew'
                    that.selectedField[0].size = that.size || '16'
                    if (a == 1) {
                        that.selectedField[0].is_bold = true
                    } else if (a == 2) {
                        that.selectedField[0].is_italics = true
                    } else if (a == 3) {
                        that.selectedField[0].is_underline = true
                    }

                    upload.selectedField[0].styleFont = that.font || 'THSarabunNew'
                    upload.selectedField[0].fontSize = that.size || '16'

                    if (upload.delField) {
                        copyStyle(upload.delField)

                    }



                },
                clear_font_stlye: function() {
                    var that = this
                    //console.log('clear')
                    that.selectedField[0].font = 'THSarabunNew'
                    that.selectedField[0].size = '16'
                    that.selectedField[0].is_bold = false
                    that.selectedField[0].is_italics = false
                    that.selectedField[0].is_underline = false

                    this.deleteField()
                },

                init_sigpad: function() {
                    window.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
                        backgroundColor: 'rgba(55, 255, 255, 0)',
                        penColor: 'rgb(0, 0, 0)'
                    });
                    //initai 
                    window.signaturePadInitial = new SignaturePad(document.getElementById('signature-pad-initial'), {
                        backgroundColor: 'rgba(255, 255, 255, 0)',
                        penColor: 'rgb(0, 0, 0)'
                    });
                    //console.log('init_sigpad')
                },

                next_step: function() {
                    var that = this
                    if (that.sign_only && that.step == 1) {
                        that.step = 3
                        that.people[0].email = getUsername()
                        that.people[0].name = getUserField('adminFisrtname') + " " + getUserField('adminLastname') || getUsername()
                        start_step3('3a')
                        that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)
                    } else if (that.step == 1) {
                        that.step = 2
                        that.get_history()
                        that.get_group()
                        that.check_use_iso()
                    } else if (that.step == 2) {
                        if (that.use_signing_order) {
                            var check_one = that.people.filter(function(n) {
                                return parseInt(n.order_set) == 1
                            }).length
                            if (check_one == that.people.length && that.people.length > 1) {
                                var rootElement = document.documentElement
                                rootElement.scrollTo({
                                    top: 0,
                                    behavior: "smooth"
                                })
                                that.alert_step_2 = true
                                that.text_alert = that.lang_txt.unique[that.lang_choose]
                            } else {
                                that.check_math_people()
                            }

                        } else {
                            that.check_math_people()
                        }

                        that.reset_field_no_user()

                    } else if (that.step == 3) {
                        that.check_field_step()
                            // that.get_exp_date()
                            // that.step = 4
                    } else if (that.step == 4) {
                        that.sorttest()
                        that.get_exp_date()
                        document.location.href = "/bla/#/dashboard"
                            // that.step = 5
                    }
                },
                count_field_signer: function() {
                    var that = this
                    for (i = 0; i < that.people.length; i++) {
                        var field = 0
                        if (that.people[i].is_group) {

                        } else {
                            var f = that.fields.filter(function(n) {
                                return n.needToSign == true
                            }).length

                        }
                    }

                },
                reset_field_no_user: function() {
                    var that = this
                    var p = that.people
                    var f = that.fields
                    n = f.length
                    for (i = 0; i < n; i++) {
                        var ind = i
                        var bo = false

                        for (j = 0; j < p.length; j++) {
                            if (f[i].email == p[j].email) {
                                f[i].person.selectedPersonIndex = p[j].selectedPersonIndex
                                bo = true
                            }
                        }

                        //  ไม่เจอลบfieldsทิ้ง
                        if (!bo) {
                            that.fields.splice(ind, 1)
                            n--
                            i--
                        }
                    }

                    for (i = 0; i < p.length; i++) {
                        p[i].selectedPersonIndex = i;
                    }
                },
                sorttest: function() {
                    var that = this;
                    that.people.sort(function(a, b) { return parseInt(a.order_set) - parseInt(b.order_set) });
                },
                check_field_step: function() {
                    var that = this
                    let isCountValid = true;
                    const counts = {};
                    that.fields.forEach((f) => {
                        if (f.email == "") {
                            if (counts.hasOwnProperty(f.person.id_group)) {
                                counts[f.person.id_group] += 1;
                            } else {
                                counts[f.person.id_group] = 1;
                            }
                        } else {
                            counts[f.email] = 1;
                        }
                    });
                    that.people.forEach(p => {
                        if (p.needToSign) {
                            if (p.is_group) {
                                if (!(counts.hasOwnProperty(p.id_group) && counts[p.id_group] >= p.amount_least_sign)) {
                                    isCountValid = false;
                                }
                            } else {
                                if (!(counts.hasOwnProperty(p.email))) {
                                    isCountValid = false;
                                }
                            }
                        }
                    });
                    var check_people_neddtosing = that.people.filter(function(n) {
                        return n.needToSign == true
                    }).length

                    if (that.fields.length < check_people_neddtosing) {
                        Swal.fire({
                                icon: 'warning',
                                title: that.lang_txt.no_field[that.lang_choose],
                                text: that.lang_txt.WithoutFields[that.lang_choose],
                                confirmButtonText: 'OK',
                                // showCancelButton: true,
                                // cancelButtonColor: '#3085d6',
                                // confirmButtonColor: '#BDBDBD',
                                // cancelButtonText: that.lang_txt.AddFields3[that.lang_choose],
                                // reverseButtons: true,
                            })
                            // .then((result) => {
                            //     if (result.isConfirmed) {
                            //         that.get_exp_date()
                            //         that.step = 4
                            //     }
                            // });
                    } else {
                        that.get_exp_date()
                        that.step = 4
                    }


                },
                check_math_people: function() {
                    var that = this
                    var people_signer = []
                        // list email user
                    for (i = 0; i < that.people.length; i++) {
                        if (that.people[i].is_group) {
                            if (that.people[i].user_group && that.people[i].user_group.length == 1) {
                                people_signer.push(that.people[i].user_group[0].email);

                            }
                        } else {
                            people_signer.push(that.people[i].email);
                        }
                    }
                    //console.log(people_signer);

                    // check user
                    var filter_name = {}
                    var a = 0
                    for (var i = 0; i < people_signer.length; i++) {
                        if (filter_name[people_signer[i]] == undefined) {
                            filter_name[people_signer[i]] = people_signer[i]
                        } else {
                            a++
                            //console.log('name dupicate >>>>', people_signer[i])
                        }
                    }

                    if (a > 0) {
                        var rootElement = document.documentElement
                        rootElement.scrollTo({
                            top: 0,
                            behavior: "smooth"
                        })
                        that.alert_step_2 = true
                        that.text_alert = that.lang_txt.unique[that.lang_choose]
                    } else {
                        that.alert_step_2 = false
                        start_step3('3b')

                        for (i = 0; i < that.people.length; i++) {
                            if (that.people[i].delegates_sign == true && that.people[i].is_group == false) {
                                that.people[i].is_group = true

                                var delegates = { email: that.people[i].delegates_mail, name: that.people[i].delegates_mail, is_viewed: false }
                                that.people[i].user_group.push(delegates)

                                var signer = { email: that.people[i].email, name: that.people[i].name, is_viewed: false }
                                that.people[i].user_group.push(signer)

                                that.people[i].email = ""
                            }
                        }

                        that.step = 3
                            // that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)
                        //console.log("eark remove3")
                        for (i = 0; i < that.people.length; i++) {
                            if (that.people[i].needToSign == true) {
                                that.setSelectedPerson(i, that.people[i].email, that.people[i].name, that.people[i].title, that.people[i].position_id)
                                break;
                            }

                        }
                        //that.setFieldsDraft()
                    }

                },
                backward_step: function() {
                    var that = this
                    if (that.step == 1) {
                        return false
                    } else if (that.step == 2) {
                        that.step = 1
                    } else if (that.step == 3) {
                        that.get_history()
                        that.get_group()
                        that.step = 2
                    } else if (that.sign_only && that.step == 3) {
                        that.step = 1
                    } else if (that.step == 4) {
                        that.step = 3
                        start_step3('3b')
                        that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)
                            //that.setFieldsDraft()
                    }
                },
                upload_file: function() {
                    var that = this
                    var dropzone = new Dropzone(document.getElementById('file-dropzone-upload'), {
                        url: api_host + 'upload_pdf',
                        maxFilesize: 100, // MB
                        maxFiles: 50,
                        clickable: true,
                        acceptedFiles: ".jpeg,.jpg,.png,.pdf"
                    });

                    dropzone.on("addedfile", function(file) {
                        that.alert_no_pdf = false
                        $('#loadingModal').modal('show');

                    });

                    var dropzoneSuccess = function(file, json) {
                        var res = JSON.parse(json)
                        //console.log(res);
                        //console.log('res.pdf_str: ', res.pdf_str);
                        var images = res.pdf_str.split("\n");

                        //set default subject
                        if (that.list_file_pdf.length == 0) {
                            that.subject = 'Please sign '
                            that.originalFile = file.name
                            that.pdfFile = res.filename
                            that.doc_id = res.filename.substring(0, res.filename.indexOf('.'))
                        }

                        that.all_original_filenames[res.filename] = file.name
                        that.imageFiles = that.imageFiles.concat(images);
                        that.list_file_pdf.push(res.filename)
                        that.initThumbnails()
                        that.build_pdf = true
                        that.imageMerge = res.image_merge
                        that.display_imageFiles.push(images[0]);
                        that.all_images[res.filename] = images

                        var temp_pdf = { pdf_name: that.pdfFile, tag_folder: that.name_tag_folder, type_document: 'general', originalFile_name: file.name }
                        that.setting_pdf.push(temp_pdf)

                        setTimeout(function() { $("#loadingModal").modal("hide") }, 500);


                    }

                    dropzone.on("success", dropzoneSuccess);
                    dropzone.on("error", function() {
                        setTimeout(function() { $("#loadingModal").modal("hide") }, 500);
                        that.alert_no_pdf = true
                        that.text_alert = that.lang_txt.max_size[that.lang_choose]
                            // setTimeout(function(){ 
                            //     that.alert_no_pdf = false

                        // }, 3000);
                        // alert('Upload Error file size must be less than 25MB')
                    });


                },
                initThumbnails: function() {
                    var arr = this.imageFiles;
                    this.imageAspectRatios = [] //reset aspect ratio array  
                    var _done_loading = 0
                    var that = this

                    for (var i = 0; i < arr.length; i++) {
                        this.imageAspectRatios.push(0)
                        var img = new Image()
                        img._index = i
                        img.onload = function() {
                            var I = this._index
                            that.imageAspectRatios[I] = (this.width / this.height);
                            //document.getElementById('preview'+I).src = upload_path + arr[I]

                            _done_loading++
                            if (_done_loading == arr.length) { /*$("#filePreview").perfectScrollbar();*/


                            }
                        }
                        img.src = upload_path + arr[i];
                    }

                },
                getDisplayFilename: function(di) {
                    var that = this
                    var ret = "";
                    var selected_pdf = that.list_file_pdf.filter(function(_ele, i) {
                        return i == di
                    })[0];

                    ret = this.all_original_filenames[selected_pdf]

                    return ret
                },
                get_count_page_pdf: function(index) {
                    var that = this
                    var removed_pdf = that.list_file_pdf.filter(function(_ele, i) {
                        return i == index
                    })[0];

                    // find images of removed PDF 
                    var removeImages = that.all_images[removed_pdf]

                    var list = []
                        // this.previewImgIndex = 0
                    for (var i = 0; i < removeImages.length; i++) {
                        list.push(upload_path + removeImages[i])
                    }
                    return list.length || 0
                },
                showPdfImage: function(di) {
                    var that = this
                    var removed_pdf = that.list_file_pdf.filter(function(_ele, i) {
                        return i == di
                    })[0];

                    // find images of removed PDF 
                    var removeImages = that.all_images[removed_pdf]
                    that.previewImgList = []
                    for (var i = 0; i < removeImages.length; i++) {
                        that.previewImgList.push(upload_path + removeImages[i])
                    }
                    $('#showdocument').modal('show')

                },
                set_tag_folder: function(name_foler) {
                    this.name_tag_folder = name_foler;
                    this.setting_pdf.forEach((doc) => {
                        doc.tag_folder = name_foler
                    })
                },
                set_type_doc: function(index, typedoc) {
                    this.setting_pdf[index].type_document = typedoc
                },
                remove_pdf: function(di) {
                    var that = this

                    var removed_pdf = that.list_file_pdf.filter(function(_ele, i) {
                        return i == di
                    })[0];

                    // find images of removed PDF 
                    var removeImages = that.all_images[removed_pdf]

                    //reset imageFiles
                    that.imageFiles = that.imageFiles.filter(function(img, _i) {
                        var boo = true
                        for (var i = 0; i < removeImages.length; i++) {
                            if (img == removeImages[i])
                                boo = false
                        }
                        return boo
                    });


                    that.list_file_pdf = that.list_file_pdf.filter(function(_ele, i) {
                        return i != di
                    });


                    if (that.list_file_pdf.length > 0) {
                        that.originalFile = that.all_original_filenames[that.list_file_pdf[0]]
                    } else {
                        that.originalFile = ''
                    }




                    that.display_imageFiles = that.display_imageFiles.filter(function(_ele, i) {
                        return i != di
                    });

                    that.initThumbnails()
                },
                select_sign_only: function(n) {
                    var that = this
                    if (n == 1) {
                        that.sign_only = true
                    } else {
                        that.sign_only = false
                    }
                    $('.dz-preview').hide() //ปิดpreview dropzone
                    $('.dz-message').remove()
                    that.createPDF()

                },
                createPDF: function() {
                    var that = this

                    var data = { list_file: that.imageFiles, list_file_pdf: that.list_file_pdf }


                    $.ajax({
                        url: api_host + "create_pdf",
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function(result) {
                            result = JSON.parse(result)
                            data.pdfFile = result.data

                            upload.pdfFile = data.pdfFile

                            that.totalPages = that.imageFiles.length
                            that.currentPage = 0;
                            that.next_step()

                            //console.log("create_pdf >>>>>>>>>>> " + data.pdfFile)


                        },
                        error: function(error) {
                            that.alert_no_pdf = true
                            that.text_alert = that.lang_txt.Please_upload[that.lang_choose]
                                // setTimeout(function(){ that.alert_no_pdf = false}, 3000);
                            //console.log("Something went wrong!" + error);


                        }
                    })
                },
                // step 2 -- signer
                addPerson: function() {
                    var that = this
                    var n = that.people.length + 1

                    if (n > 12) {
                        var a = that.people.length - 12
                            // //console.log(a);
                        if (a < 12) {
                            var c = { color: that.color[a].color }
                            that.color.push(c)

                        } else {
                            var c = { color: '#F2C94C' }
                            that.color.push(c)

                        }


                    }

                    if (that.is_template) {
                        that.people.push({ name: 'SIGNER' + n, email: 'SIGNER' + n, needToSign: true, title: '', order_set: '', no: n, is_group: false, user_group: [], least_sign: true, amount_least_sign: 1, delegates_sign: false, delegates_mail: '', delegates_name: "", is_inspector: false })
                    } else if (that.use_signing_order) {
                        that.people.push({ name: '', email: '', needToSign: true, title: '', order_set: n, is_group: false, user_group: [], least_sign: true, amount_least_sign: 1, delegates_sign: false, delegates_mail: '', delegates_name: "", is_inspector: false })
                    } else {
                        that.people.push({ name: '', email: '', needToSign: true, title: '', order_set: '', is_group: false, user_group: [], least_sign: true, amount_least_sign: 1, delegates_sign: false, delegates_mail: '', delegates_name: "", is_inspector: false })
                    }

                    // this.check_btn_next();
                    // setTimeout(function () { that.initsuggestionbox(); that.get_Contacts(); }, 1000)

                },
                add_person_secret: function() {
                    var that = this
                    if (that.secret_level > 1) {
                        for (n = 0; n < that.people.length; n++) {
                            that.people[n].usePassword = true
                        }
                    }
                },
                selcet_needtosign: function(index, value) {
                    var that = this
                    that.people[index].is_inspector = value
                    if (value == false) {
                        that.auto_remove_field(index)
                    }

                },
                removePerson: function(i) {
                    var that = this
                    that.people.splice(i, 1);
                    that.set_oreder_user = 0

                },
                remove_use_ekyc: function(index) {
                    var that = this
                    var p = that.people

                    if (p[index].eKyc || p[index].usePassword) {
                        var str = "คุณต้องการลบการยืนยันตัวตนหรือไม่"
                        if (confirm(str)) {
                            that.people[index].eKyc = false
                            that.people[index].usePassword = false
                            that.people[index].password = ""
                            $('#check_use_ekyc' + index).collapse('toggle')
                        } else {
                            return false
                        }
                    } else {
                        $('#check_use_ekyc' + index).collapse('toggle')
                    }

                },
                clear_group: function(i) {
                    var that = this
                    that.people[i].is_group = false
                    that.people[i].user_group = []
                },
                set_number_sign_order: function() {
                    var that = this
                    for (i = 0; i < that.people.length; i++) {
                        that.people[i].order_set = i + 1
                    }
                },
                get_history: function() {
                    var that = this
                    var data = { owner: getUsername() }
                    $.ajax({
                        url: api_host + "get_history_sign",
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function(res) {
                            // //console.log(res);
                            res = JSON.parse(res)
                            that.history_sign = []
                            if (res.success) {
                                for (i = 0; i < res.data.length; i++) {
                                    that.history_sign.push(res.data[i])
                                }
                                // that.history_sign = res.data[0]
                                that.search_members = that.history_sign
                                that.show_list_members = that.history_sign
                              
                            }

                        },
                        error: function(error) {
                            alert("Something went wrong!" + error);

                        }
                    })
                },
                // step 3
                setSelectedPerson: function(i, email, name, title, position_id) {
                    var that = this
                    var p = this.people[i]
                    that.selectedPersonIndex = i
                    that.selectedPerson = email
                    that.selectedPersonName = name
                    that.selectedPersontitle = title
                    that.selectedPersonPositionId = position_id
                    if (i != undefined) {
                        p.selectedPersonIndex = i
                        that.selectedPersonObj = p
                        console.table('setSelectedPerson' + i)
                        that.is_no_sign = false
                        that.check_fiels_no_sign("")
                    } else {
                        that.setSelectedPerson_no_sign()
                    }


                },
                setSelectedPerson_no_sign: function() {
                    var that = this
                    that.is_no_sign = true
                    that.selectedPersonIndex = 11
                    that.selectedPerson = ''
                    that.selectedPersonName = ''
                    that.selectedPersontitle = ''
                    that.selectedPersonPositionId = ''

                    p = {
                        name: '',
                        email: '',
                        position_id: '',
                        needToSign: true,
                        eKyc: false,
                        usePassword: false,
                        password: '',
                        title: '',
                        order_set: '',
                        delegates_sign: false,
                        delegates_mail: '',
                        is_group: false,
                        user_group: [],
                        least_sign: true,
                        amount_least_sign: 1,
                        is_inspector: false
                    }

                    that.selectedPersonObj = p
                    that.check_fiels_no_sign("")
                },
                get_exp_date: function() {
                    var that = this
                    var date = new Date().getTime()
                    if (that.count_date > 0 && that.count_date <= 120) {
                        var a = that.count_date * 24
                        a = date + (60 * 60 * a * 1000)
                        a = new Date(a)
                        var d = a.getFullYear() + "-" + that.$options.filters.set_zero_text((a.getMonth() + 1)) + "-" + that.$options.filters.set_zero_text(a.getDate())
                        that.exp_date = d
                    } else {
                        that.count_date = 1
                    }



                },
                get_count_date: function() {
                    var that = this
                    var date = new Date(that.exp_date)
                    var now_date = new Date()
                    that.count_date = date.getTime() - now_date.getTime()
                    that.count_date = (that.count_date / (1000 * 60 * 60 * 24))
                    that.count_date = Math.round(that.count_date)
                    if (that.count_date > 120) {
                        that.count_date = 120
                        that.get_exp_date()
                    }
                },
                remove_person_group: function(p, i) {
                    var that = this
                    that.people[p].user_group.splice(i, 1)

                },
                get_group: function() {
                    var that = this
                    var get_company_id = getUserField('company_id')
                    var data = { company_id: get_company_id }
                    $.ajax({
                        type: "POST",
                        url: api_host + "get_group_sign",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function(res) {
                            if (res.success) {
                                that.group_sign = res.data
                                for(let keygroup in that.group_sign){
                                    Vue.set(that.group_sign[keygroup], 'checked', false); 

                                    for(let key in that.group_sign[keygroup].group_sign){
                                        Vue.set(that.group_sign[keygroup].group_sign[key], 'get_groupname', keygroup); 
                                        Vue.set(that.group_sign[keygroup].group_sign[key], 'checked', false); 
            
                                            // for(let keys in that.group_sign[that.selectgroup][key] ){
                                            //     //console.log(key)
                                            // }
                                    } 
                                }
                            
                                that.show_member_cc(0)
                            }

                        },
                        error: function(request, status, error) {
                            //console.log("|" + status + ' |' + error);
                        }
                    });
                },
                select_group_sign: function(g) {
                    var that = this
                    g = parseInt(g)
                    //that.get_group()
                        // that.history_show = false
                        // that.search_member_text = ''
                        //that.list_cc
                    that.people[that.set_oreder_user].is_group = true
                    that.people[that.set_oreder_user].user_group =  that.list_cc
                    that.people[that.set_oreder_user].name = that.group_sign[g].group_name
                    that.people[that.set_oreder_user].id_group = that.group_sign[g]._id
                    that.people[that.set_oreder_user].email = ''
                    that.selectgroup = ''
                        //console.log(that.people[that.set_oreder_user].user_group)
                    if (that.people[that.set_oreder_user].is_group == true && that.people[that.set_oreder_user].user_group.length > 0) {
                        delete that.people[that.set_oreder_user].is_viewed
                        for (i = 0; i < that.people[that.set_oreder_user].user_group.length; i++) {
                            that.people[that.set_oreder_user].user_group[i].is_viewed = false
                        }
                    }

                },
                set_person: function(n) {
                    var that = this
                    that.search_member_text = ''
                    $("input[name='selectgroup']").prop('checked', false)
                    $("input[name='select-person']").prop('checked', false)
                    that.set_oreder_user = n
                    that.history_show = true
                },
                get_members: function() {
                    var that = this
                    var get_company_id = getUserField('company_id')
                    var data = { company_id: get_company_id, mode: 'get' }
                    $.ajax({
                        type: "POST",
                        url: api_host + "company_member",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function(res) {
                            that.members = res.data;
                            // //console.log(res)
                        },
                        error: function(request, status, error) {
                            //console.log("|" + status + ' |' + error);
                        }
                    });
                },
                clear_modal_group: function() {
                    var that = this
                        // that.set_oreder_user = -1
                    that.search_member_text = ''
                    that.history_show = true
                },
                search_member: function() {
                    var that = this
                    var members = that.members
                   
                    var search = that.show_list_members.filter(function(m) {
                            var email = m.email.search(that.search_member_text) != -1
                             if (email == true) {
                                var ne = m.email.search(that.search_member_text) != -1
                             }else{
                                var ne = m.name.search(that.search_member_text) != -1
                             }
                            
                            return ne
                        })
                       
                   
                   
                    if (that.history_show == true) {
                        that.search_members = search
                    }else{
                        that.show_list_member = search
                    }
                    //console.log(that.show_list_member)
                    //console.log(that.search)
                    //that.history_show = true
                    $("input[name='selectgroup']").prop('checked', false)
                    that.people[that.set_oreder_user].is_group = false
                    that.people[that.set_oreder_user].user_group = []
                    that.people[that.set_oreder_user].name = ''
                    // that.people[that.set_oreder_user].user_name = getUserField('adminFisrtname') + " " + getUserField('adminLastname')
                    //that.show_history()
                },
                show_history: function() {
                    var that = this
                    that.history_show = true
                    $("input[name='selectgroup']").prop('checked', false)
                    that.people[that.set_oreder_user].is_group = false
                    that.people[that.set_oreder_user].user_group = []
                    that.people[that.set_oreder_user].name = ''
                    that.search_members = that.history_sign
                    that.show_list_members = that.history_sign
                    
                    // that.people[that.set_oreder_user].user_name = getUserField('adminFisrtname') + " " + getUserField('adminLastname')
                },
                select_mem: function() {
                    var that = this
                    that.people[that.set_oreder_user].name = that.search_members[that.selecthis].name
                    that.people[that.set_oreder_user].email = that.search_members[that.selecthis].email
                    that.people[that.set_oreder_user].is_group = false
                    that.people[that.set_oreder_user].user_group = []
                    that.selecthis = ''
                },
                select_user_modal: function() {
                    var that = this
                    //console.log("his ----- " + that.selecthis);
                    //console.log("gg ----- " + that.selectgroup);
                    if (parseInt(that.selectgroup) >= 0) {
                        that.c_selecthisDep(that.selectgroup ,that.selectgroupdep)
                    } else if (parseInt(that.selecthis) >= 0) {
                        that.select_mem()
                    }
                
                },

                
                select_member_sign: function (n, e) {
                    var that = this
                    that.people[that.set_oreder_user].name = n || e
                    that.people[that.set_oreder_user].email = e
                    //console.log(that.group_sign)
            
                },
                c_selectgroup: function() {
                    var that = this
                        // alert('gg')
                    that.selectgroup = ''
                },
                c_selecthis: function(i,status) {
                    var that = this
                    that.selectgroup = i
                    that.history_show = false
                    that.search_member_text = ''
                    that.selecthis = ''
                        that.show_list_member = that.group_sign[that.selectgroup].group_sign
                        that.show_list_members = that.group_sign[that.selectgroup].group_sign
                        
                         

                },
                c_selecthisDep: function(g,n) {
                    var that = this
                    ////console.log(that.group_sign)
                     that.selectgroup = g
                     that.selectgroupdep = n
                    // that.history_show = false
                    // that.search_member_text = ''
                    that.selecthis = ''
                    var that = this
                    that.people[that.set_oreder_user].name = that.group_sign[that.selectgroup].group_sign[n].name
                    that.people[that.set_oreder_user].email =that.group_sign[that.selectgroup].group_sign[n].email
                    that.people[that.set_oreder_user].is_group = false
                    that.people[that.set_oreder_user].user_group = []
                    that.selecthis = ''
                        
                         

                },
                cancle_member: function() {
                    var that = this
                    that.people[that.set_oreder_user].name = ''
                    that.people[that.set_oreder_user].email = ''
                    that.people[that.set_oreder_user].is_group = false
                    that.people[that.set_oreder_user].user_group = []
                    that.people[that.set_oreder_user].id_group = ''

                },
                set_private_mail: function() {
                    var that = this
                    that.people[that.select_person_private_mail].private_mail = that.private_mail
                    that.private_mail = ''
                },
                replace_number: function(i) {
                    var that = this
                    var x = that.people[i].order_set.replace(/\D/g, '');
                    if (x <= that.people.length) {
                        that.people[i].order_set = x
                    } else {
                        that.people[i].order_set = 1
                    }
                    // auto run number
                    // if( that.people[i].order_set < that.people.length){

                    //     for(n = 0; n < that.people.length ; n++){
                    //         if(n < i){
                    //          var a =  (parseInt(n) + 1) + parseInt(x)
                    //          alert('a --- '+ a)
                    //             alert('n --- '+ n)
                    //             that.people[n+2].order_set = a
                    //         }


                    //     }
                    // }

                },
                remove_history: function(e) {
                    var that = this
                    var data = { owner: getUsername(), email: e }
                    $.ajax({
                        type: "POST",
                        url: api_host + "remove_history_sign",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function(res) {
                            that.history_sign = []
                            that.get_history()
                        },
                        error: function(request, status, error) {
                            //console.log("|" + status + ' |' + error);
                        }
                    });
                },
                logout: function() {

                    localStorage.removeItem('compCode')
                    localStorage.removeItem('username')
                    localStorage.removeItem("register_obj")
                        // localStorage.removeItem("listNotification_json")

                    $.ajax({
                        type: "POST",
                        url: api_host + "logout",
                        data: JSON.stringify(''),
                        dataType: "json",
                        success: function(data) {
                            document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
                        },
                        error: function(request, status, error) {
                            //console.log("Logout Error |" + status + ' |' + error);
                            document.location.href = 'index.html'
                        }
                    });

                },
                zoomIn: function() {
                    // var newLevel = "1.0"
                    // if (this.zoomLevel == 0.8) { newLevel = "1.0" }
                    // if (this.zoomLevel == 0.5) { newLevel = "0.8" }
                    // this.setZoomLevel(newLevel)
                    // $('#zoomLevel').val(newLevel)
                    var fac = 1.1
                    var id = 'svg'
                    var draw = SVG('#' + id)
                    var w = window.svgWidth * fac
                    var h = (window.svgHeight * 2) * fac

                    draw.scale(fac)
                    $('#' + id).css('width', w + 'px')
                    $('#' + id).css('height', h + 'px')

                    var s = document.getElementById(id)
                    var box = s.getBoundingClientRect()
                    draw.translate(-box.x, -box.y)
                },
                zoomOut: function() {
                    // var newLevel = "0.5"
                    // if (this.zoomLevel == 0.8) { newLevel = "0.5" }
                    // if (this.zoomLevel == 1.0) { newLevel = "0.8" }

                    // this.setZoomLevel(newLevel)
                    // $('#zoomLevel').val(newLevel)
                    var fac = 0.9
                    var id = 'svg'
                    var draw = SVG('#' + id)
                    var w = window.svgWidth * 1
                    var h = (window.svgHeight * 2) * 1

                    draw.scale(fac)
                    $('#' + id).css('width', w + 'px')
                    $('#' + id).css('height', h + 'px')

                    var s = document.getElementById(id)
                    var box = s.getBoundingClientRect()
                    window.s = s
                    window.box = box
                    draw.translate(-box.x, -box.y)

                },
                check_choose: function() {
                    var that = this
                    if (document.getElementById('type_radio1').checked || document.getElementById('type_radio2').checked) {
                        that.check_choose_signature = true
                    }
                },
                please_choose: function() {
                    var txt = (localStorage.lang_choose == "th") ? "กรุณาเลือกลายเซ็น " : "Please choose signature"
                    alert(txt)
                },
                createSignature_initial: function() {
                    var that = this
                    var data = signaturePadInitial.toDataURL('image/png');
                    data = data.split(',')
                    $.ajax({
                        type: "POST",
                        url: "../capi/create_signature_initial",
                        data: JSON.stringify({ "img": data[1], "username": getUsername(), "code": getUsername(), "sign_type": "draw_initial", "field_id": '' }),
                        dataType: "json",
                        success: function(data) {
                            var path = '../edoc/app-assets/images/signature/' + getUsername() + '_draw_initial.png?rd=' + new Date().getTime()
                            that.initia_draw = path
                            that.show_initia_draw = true
                            alert('สร้างลายเซ็นสำเร็จ draw')
                            that.clearSignature(2)

                        },
                        error: function(request, status, error) {
                            alert("|" + status + ' |' + error);
                        }
                    });
                },
                genSignature_initial: function() {
                    var that = this
                    alert('genSignature_initial')
                    var url = "../capi/gen_signature_initial"
                    data = { "name": that.sign_name_initial, "code": getUsername(), "lang": 'en', "font_name": 'worasait.ttf', "field_id": '' }
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function(data) {
                            var path = '../edoc/app-assets/images/signature/' + getUsername() + '_gen_initial.png?rd=' + new Date().getTime()
                            that.initia_gen = path
                            that.show_initia_gen = true
                            alert('สร้างลายเซ็นสำเร็จ gen')
                        },
                        error: function(request, status, error) {
                            alert("|" + status + ' |' + error);
                        }
                    });
                },
                hide_img_sign: function(p) {
                    var that = this
                    if (p == 1 || p == '1') {
                        that.show_initia_draw = false
                    } else if (p == 2 || p == '2') {
                        that.show_initia_gen = false
                    }
                },
                getUser: function() {
                    var that = this
                    var login_service = "esig"
                    var adminEmail = getUsername()
                    $.ajax({
                        type: "POST",
                        //url: api_host + "get_users",
                        url: api_host + "check_kyc_esig",
                        data: JSON.stringify({ login_service, adminEmail }),
                        dataType: "json",
                        success: function(data) {
                            that.userField = data.data
                            //console.log(data.data)
                        },
                        error: function(request, status, error) {
                            // alert("|" + status + ' |' + error);
                            //console.log("|" + status + ' |' + error)
                        }
                    });

                },
                clearSignature: function(n) {
                    if (n == 1 || n == '1') {
                        signaturePad.clear();
                    } else if (n == 2 || n == '2') {
                        signaturePadInitial.clear()
                    }
                },
                /* 0 = choose signature , 1 = draw , 2 = generate */
                showSignatureTab: function(i) {
                    var tabID = ['home-tab', 'profile-tab', 'contact-tab']
                    var aTag = document.querySelector('#' + tabID[i])
                    var tab = new bootstrap.Tab(aTag)
                    tab.show();
                },

                signDlgDelay: function(field, field) {
                    setTimeout(function() {
                        upload.signDlg(field, field)
                    }, 1500)

                },

                signDlg: function(field, field) {
                    var that = this
                    var isMyfield = true
                    window.sign = field
                    if (field.toolName == 'Signature') {

                        checkSignature()
                            //this.showSignatureTab(1)
                            //this.showSignatureTab(0)
                        $("#editSignaturebox").modal("show");

                        if (document.getElementById('type_radio1').checked || document.getElementById('type_radio2').checked) {
                            that.check_choose_signature = true
                        }
                    } else if (field.toolName == 'Initial') {
                        $("#initialModal").modal("show");
                    }
                },
                set_sign_type(v) {
                    //console.log(v)
                    var field = window.sign
                    var url = '../edoc/app-assets/images/signature/' + getUsername() + '_' + v + '.png'
                    var group = field.group.load(url)
                    chooseSignature(v);
                    if (field.toolName == 'Initial') {
                        this.userField.initial = v
                        $("#initialModal").modal("hide");
                    } else {
                        this.userField.signature = v
                        $("#editSignaturebox").modal("hide");
                    }
                },
                sign() {
                    //console.log('sign')
                    const api_url = api_host + 'set_signature'
                    var data = { adminEmail: getUsername(), signature: upload.userField.signature, initial: upload.userField.initial }
                    $.ajax({
                        url: api_url,
                        type: "POST",
                        data: JSON.stringify(data),
                        success: function(result) {
                            //console.log(result)
                        },
                        error: function(error) {
                            //console.log("eeee draft error: " + error)
                        }
                    })

                },
                set_req_attach: function() {
                    var that = this
                    for (i = 0; i < that.fields.length; i++) {
                        if (that.fields[i].id == that.field_id) {
                            that.fields[i].required_attachment = that.req_attach
                        }
                    }
                },
                default_req_attach: function() {
                    var that = this
                    for (i = 0; i < that.fields.length; i++) {
                        if (that.fields[i].id == that.field_id) {
                            //console.log(that.fields[i].required_attachment);
                            if (that.fields[i].required_attachment != undefined) {
                                that.req_attach = that.fields[i].required_attachment
                            } else {
                                that.req_attach = true
                            }

                        }
                    }
                },
                close_alert_2: function() {
                    var that = this
                    that.alert_step_2 = false
                },
                set_form_email: function(e) {
                    var that = this
                    that.chk_mail = true

                    var re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    for (i = 0; i < that.people.length; i++) {
                        if (!that.people[i].is_group) {
                            if (!re.test(String(that.people[i].email))) {
                                that.people[i].email = that.people[i].email.trim().toLowerCase()
                                that.chk_mail = false
                                return false
                            }
                        }

                        if (that.people[i].delegates_sign) {
                            if (!re.test(String(that.people[i].delegates_mail))) {
                                that.people[i].delegates_mail = that.people[i].delegates_mail.trim().toLowerCase()
                                that.chk_mail = false
                                return false

                            }
                        }
                    }

                    if (that.chk_mail) {
                        if (that.secret_level > 1) {
                            that.check_input_password_secret()
                        } else if (that.secret_level == 1) {
                            that.check_mail_company()
                        } else {
                            that.next_step()
                        }
                    }

                },
                validate_text: function(text) {
                    var re = /[A-Za-z0-9]+/
                    var x = re.test(text)
                    return x
                },


            } //end methods
        } //obj1

    return obj1;
}

function dragTool(evt) {
    var key = upload.draggingTool.toolName + upload.selectedPersonIndex
    //console.log("dragTool = " + key)
    evt.dataTransfer.setDragImage(dragingImages[key], 70, 70);

    //evt.dataTransfer.setDragImage(dragingImages.Signature, 70, 70);
}

function allowDrop(evt) {
    event.preventDefault()
}

function filedToEdoc(obj) {
    // --------- Radio ------------
    for (var i = 0; i < obj.length; i++) {
        if (obj[i].toolName == 'Radio') {
            obj[i]['child'] = []
            for (var j = 0; j < obj.length; j++) { // add choice 
                if (obj[i].id == obj[j].root) {
                    obj[i]['child'].push(obj[j])
                }
            }
        }
    }

    var _list = obj.length
    while (i--) {
        if (obj[i].toolName == 'choice') {
            obj.splice(i, 1);
        }
    }

    return obj
}

function upload_vue_lib() {
    return {
        id: '#app',
        data: {
            adjustedWidth: 100,
            totalHeight: 0,
            pagesDim: [],
            master_group: null,
            draw: null,
            draggingTool: null,
            imageFiles: [],

            //business logics
            delField: null, //field to be deleted
            canDel: true,
            fields: [],
            selectedPersonIndex: 0,
            selectedPerson: null,
            //radio
            radioGroups: {},
            radioIdDel: '',
            groupRadioDel: '',
            groupPlusDel: {},
            radioGroupName: '',
            is_cloning: false
        },
        methods: {
            getPersonColor: function(_i) {
                var colors = this.color || ['yellow', 'blue', 'pink', 'green', 'grey', 'orange']
                    //alert('getPersonColor : '+ this.color[0].color+ this.color[1].color+ this.color[2].color)
                var color_select = this.selectedPersonIndex
                if (!isNaN(_i)) {
                    color_select = _i
                }
                var x12 = this.color[color_select]
                    //alert('aaaaa : '+ _i + ' '+ x12.color)
                return x12
            },

            put_fields_again: function() {
                //console.log('put_fields_again is called................')
                var that = this

                if (that.fields.length == 0) {
                    //console.log('skipped put_fields_again() field count = ' + that.fields.length);
                    return;
                } else {
                    setTimeout(function() {

                            for (ik = 0; ik < that.fields.length; ik++) {
                                // alert(ik)
                                //const {x, y} = that.calsvgXY(SVG('#svg' + that.fields[i].page), that.fields[i].xp, that.fields[i].yp)
                                //console.log('put_fields_again ik = ' + ik + 'bbbbbbbbbbbbbbbbb')
                                const { x, y } = that.calsvgXY(SVG('#svg'), that.fields[ik].xp, that.fields[ik].yp, that.fields[ik].page)
                                that.transferFieldGroup(that.fields[ik], x, y)
                            }
                        }, 2000) //make sure  
                }


            },
            reset_draw: function() {
                var draw = this.draw
                    //1. reset draw dimension 
                this.totalHeight = 0
                this.pagesDim = []

                //2. create a master group
                this.master_group = draw.group()

                //console.log("Reset draw !!!")
            },

            open_thumbnail(pageIndex) {
                var ret = 0
                pageIndex = parseInt(pageIndex)
                for (var i = 0; i < this.pagesDim.length; i++) {
                    if (i < pageIndex) {
                        var dim = this.pagesDim[i]
                        ret = ret + dim.height
                    }

                }
                var ele = document.getElementById('main-svg')
                ele.parentNode.parentNode.scrollTo(0, ret)
            },

            init_svg: function(i) {
                //    alert('init_svg >>> i = '+i+' len =' + this.pagesDim.length)
                var draw = this.draw
                if (i == 0) {
                    this.reset_draw()
                }
                var images = this.imageFiles.map(img => {
                        return document.location.origin + '/face/images/card/' + img
                    })
                    //var images = ['https://hjkl.ninja/face/images/card/pdf165582731614309107-0.png',
                    //'https://hjkl.ninja/face/images/card/pdf165582731614309107-1.png']

                var myImg = images[i]

                draw.image(images[i], function(evt) {
                    var group = upload.new_document_page(draw, this)
                    group.attr('id', 'page-group-' + i)
                    group.attr('name', 'page' + i)

                    upload.master_group.add(group)

                    if (i < images.length - 1) { upload.init_svg(i + 1) } //load next page
                    else {
                        //console.log("Restore fields back to svg.....")
                        upload.put_fields_again()
                    }
                })

                var svg = document.getElementById('svg')
                svg.style.width = upload.adjustedWidth + 'px'
                    // SVG('#svg').size(upload.adjustedWidth, window.svgHeight + 2)
                    // svg.parentElement.style.width = upload.adjustedWidth + 'px'


            },
            new_document_page: function(draw, img) {
                var group = draw.group()
                var adjustedWidth = this.adjustedWidth
                var adjustedHeight = adjustedWidth * (img.height() / img.width())
                img.size(adjustedWidth, adjustedHeight)
                var newTotalHeight = this.totalHeight + adjustedHeight
                draw.size(adjustedHeight, newTotalHeight)
                group.add(img)

                var border = draw.rect(adjustedWidth, adjustedHeight).fill('none').stroke({ width: 1, color: 'black' })
                group.add(border)

                group.move(0, this.totalHeight)
                this.totalHeight = newTotalHeight //Save new height value 

                upload.pagesDim.push({ width: adjustedWidth, height: adjustedHeight }) //store page dimensions


                return group
            },

            activate: function(field) {
                var oldField = this.delField
                if (oldField) {
                    var rect = oldField.group.find('rect')
                    rect.stroke({ color: 'blue', width: 0 })
                }

                this.delField = field

                var tmp = field.group
                field.group = null
                    // alert(JSON.stringify(field))
                field.group = tmp


                var rect = field.group.find('rect')
                rect.stroke({ color: 'blue', width: 1 })

                this.show_set_detail = true
                this.chk_set_field = field.toolName
                this.id_field = field.id
                //console.log('activate >>>>>>>>>>  ' + field.id + ',' + field.toolName)
                upload.field_id = field.id
                upload.default_req_attach()
                    // this.selectedPersonName = field.person.name

                if (field.person) {
                    //console.log(field.person)
                    //console.log(field.person.selectedPersonIndex)
                    this.setSelectedPerson(field.person.selectedPersonIndex, field.person.email, field.person.name, field.person.title, field.person.position_id)
                }
                //console.log('this.selectedPersonName = ', field)


                var note = null
                if (field.note_of) { note = field }
                if (field.note_field) { note = field.note_field }
                if (note) {
                    this.note_field = note;
                    //console.log("Set notefield = " + note.id)
                }
                this.chk_fields_clone(field.id)

            },
            setRadioGroup: function() {
                var field = this.delField
                field.radioGroupName = this.radioGroupName
            },
            update_loc: function(field) {
                var g = field.group
                var page = this.compute_page(g.y())
                //console.log('update_loc')
                //console.log(page)
                var dim = this.pagesDim[page]
                var offsetY = 0

                for (var i = 0; i < this.pagesDim.length; i++) {
                    if (i < page) { offsetY += this.pagesDim[i].height }
                }

                var x = g.x()
                var y = g.y() - offsetY


                if (this.sign_only && (field.toolName == 'Signature' || field._toolName == 'Initial')) {
                    y = y + (250 / 4.1)
                }

                var xp = (x * 100) / dim.width
                var yp = (y * 100) / dim.height

                /* update field loc */
                field.page = page
                field.xp = xp
                field.yp = yp
                console.table('update_loc', field)
            },
            restore_scale: function(g) {
                window.g = g
                var _field_id = g._field_id
                //console.log("restore_scale " + _field_id)
                var field = this.field(_field_id)

                //scaling
                // field.scale = 200
                // alert(field.scale+','+ g._width1)
                // var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;

                var new_width = (field.scale * g._width1) / 100
                var new_height = new_width * g._ratio
                g._rect.size(new_width, new_height)

                g._cir1.center(g._rect.x(), g._rect.y())
                g._cir2.center(g._rect.x() + g._rect.width(), g._rect.y())
                g._cir3.center(g._rect.x(), g._rect.y() + g._rect.height())
                g._cir4.center(g._rect.x() + g._rect.width(), g._rect.y() + g._rect.height())

            },

            update_scale: function(g) {

                var field = this.field(g._field_id)
                field.scale = this.compute_scale(g)

            },

            exceed_scale_limit: function(g, w) {
                return (w > g._width2 || w < g._width05)
            },

            compute_scale: function(g) {
                var rect = g.findOne('rect')
                    // alert('rect : '+rect.width())
                var wp = (rect.width() * 100) / g._width1

                if (wp > 200) { wp = 200 }
                if (wp < 50) { wp = 50 }

                return wp
            },

            compute_page: function(y) {
                var ret = -1
                var arr = this.pagesDim

                var y1 = 0
                var y2 = 0

                for (var i = 0; i < arr.length; i++) {
                    y2 = y1 + arr[i].height
                    if (y >= y1 && y <= y2) { ret = i; break; }
                    y1 = y2
                }

                if (ret < 0) ret = 0

                return ret
            },

            field: function(fieldId) {
                return this.fields.find(f => { return f.id == fieldId })
            },


            attach_drag: function(g) {
                that = this
                //console.log(g)
                g.on('click', function(evt) {

                    var field = upload.field(this._field_id)
                    if (!field) { //console.log(">>>>>>> invalid fieldIdC " + this._field_id); return; }


                    if (that.sign_only && field.toolName == 'Signature' && that.userField.signature == '') {
                        //console.log("no signature call signDlgA")
                        that.signDlg(field, field)
                    } else if (that.sign_only && field.toolName == 'Initial' && that.userField.initial == '') {
                        //console.log("no signature call signDlgB")
                        that.signDlg(field, field)
                    }

                    upload.activate(field)

                })

                g.draggable()
                g.on('dragstart.namespace', function(evt) {
                    var field = upload.field(this._field_id)
                    if (!field) { //console.log(">>>>>>> invalid fieldIdB " + this._field_id); return; }

                    upload.activate(field)


                })

                g.on('dragmove.namespace', function(evt) {
                    const { handler, box } = evt.detail
                    let { x, y } = box
                    //g.find("text").text(`${g._toolName}`) //eark
                    //g.find("text").text(`${g._toolName}\n(${parseInt(x)},${parseInt(y)})\npage ${upload.compute_page(y)}`)

                })

                g.on('dragend.namespace', function(evt) {
                    //console.log('dragend done')
                    window.dend = this;

                    var field = upload.field(this._field_id)
                    if (!field) { //console.log(">>>>>>> invalid fieldIdA " + this._field_id); return; }

                    upload.update_loc(field)



                })

                return g
            },

            set_tool: function(toolName, N) {
                var field = {
                    "id": this.fieldID(),
                    "toolName": toolName,
                    "isNew": true,
                }
                if (toolName == 'Dropdown') { field.toolOptions = '' }

                this.draggingTool = field
                //console.log('set tool to ' + field)

            },

            fieldID: function() {
                var tmp = window.TMP_FIELD_ID || new Date().getTime()
                tmp = tmp + 1
                window.TMP_FIELD_ID = tmp
                return tmp
            },

            createChoice: function(fieldPlus, x, y, group_id, gp) {
                // Set View 
                var svg = SVG('#svg')
                var group = svg.group()
                var color = this.getPersonColor()
                var rect = svg.rect(20, 20).fill(color)
                var text = svg.text('X' || "N/A")
                group.add(rect)
                group.add(text)
                group.move(x, y)
                group.draggable()

                // Set Field Choice
                // this.radioGroups[group_id].items.push({group:group,id:''})
                var field = clone_field(fieldPlus, fieldPlus.page)
                field["toolName"] = "choice"
                field["group"] = group
                    //field["group"] = null
                field["root"] = group_id
                field._parent_id = fieldPlus.id
                this.addField(field)

                field["group"] = group
                    //this.onFieldAdded(field,1)  
                this.radioGroups[group_id].items.push({ group: group, id: field.id })
                    // app.onFieldAdded(field["toolName"], field.page) 
                window.rdg = this.radioGroups
                    //attach event
                var _id = group_id
                upload.computeRadioGroupBoundary(_id, gp)
                group.on('click.namespace', (e) => {
                    app.delField = field
                    this.delField = field
                    field["group"] = group
                    this.radioIdDel = field.id // id field radio ที่ต้องการลบ
                    this.groupRadioDel = _id // id group radio ที่ต้องการลบ
                    this.groupPlusDel = gp // ปุ่มบวกของ radio ที่ต้องการลบ   
                        //this.addField(field)  //eark mod              
                        //this.onFieldAdded(field,1)
                    //console.log(this.radioIdDel)
                })

                group.on('dragmove.namespace', (e) => {
                    upload.computeRadioGroupBoundary(_id, gp) //eark 
                    app.delField = field
                    this.delField = field
                    field["group"] = group
                    this.radioIdDel = field.id
                    this.groupRadioDel = _id
                    this.groupPlusDel = gp
                        //this.addField(field)  //eark mod  
                        //this.onFieldAdded(field,1)
                    //console.log(this.radioIdDel)
                })


                //end

            },

            computeRadioGroupBoundary: function(groupId, gp) {
                var g = this.radioGroups[groupId]

                // if(g.items.length==0)
                //   {//console.log('computeRadioGroupBoundary do not move plus ');return;}

                //var svg = null 
                var x1 = 9999
                var y1 = 9999

                var x2 = 0
                var y2 = 0
                //console.log(g.items)
                g.items.forEach(rect => {
                    if (rect.group.x() < x1) { x1 = rect.group.x() }

                    if (rect.group.y() < y1) { y1 = rect.group.y() }


                    if (rect.group.x() > x2) { x2 = rect.group.x() }

                    if (rect.group.y() > y2) { y2 = rect.group.y() }

                });
                //console.log(x1, y1, x2, y2)

                var color = 'green'
                var svg = g.svg
                var rect_bg = g.rect_bg || svg.rect().fill(color)
                g.rect_bg = rect_bg

                rect_bg.size(x2 - x1 + 20, y2 - y1 + 20)
                    // rect_bg.opacity(0.1)
                rect_bg.move(x1, y1)
                    // rect_bg.back()
                rect_bg.attr({
                    fill: '#f06',
                    'fill-opacity': 0.1,
                    'stroke': '#5184AF',
                    'stroke-dasharray': "5 10"
                })

                //console.log('ggg>>')
                //console.log(g)
                g.items.forEach(rect => {
                    radio = rect.group
                    radio.front()
                });
                gp.move((x2 + x1) / 2, y2 + 30)
            },
            showRadio: function() {
                // create Radio
                var field = this.draggingTool
                var color = this.getPersonColor()
                var svg = SVG('#svg')
                var group = svg.group()
                var rect = svg.rect(20, 20).fill(color)
                var text = svg.text('+')
                group.add(rect)
                    //group.add(c4)
                group.add(text)
                field.group = group
                field.page = 0
                    //set id Radio Groups 
                var rGroup = {
                    id: field.id,
                    items: [],
                    svg: svg,
                    rect_bg: null
                }
                this.radioGroups[rGroup.id] = rGroup

                group.on('click.namespace', (e) => {
                    var app = window.myApp

                    if (app.draggingTool) { // First Drop From Panel

                        //eark mod
                        //app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                        app.draggingTool = null
                    } else { // Click event after drop from panel
                        var rectX = e.target.getBBox().x
                        var rectY = e.target.getBBox().y
                        upload.createChoice(field, rectX, rectY, rGroup.id, group)
                    }
                })
            },
            showRadio2: function() {
                // create Radio
                var field = this.draggingTool
                var color = this.getPersonColor()
                var svg = SVG('#svg')
                var group = svg.group()
                var rect = svg.rect(20, 20).fill(color)
                var text = svg.text('X')
                group.add(rect)
                group.add(text)
                group.draggable()
                group.hide()
                field.group = group
                field.page = 0
                    //set id Radio Groups 
                var rGroup = {
                    id: field.id,
                    items: [],
                    svg: svg,
                    rect_bg: null
                }
                this.radioGroups[rGroup.id] = rGroup

                if (!upload.radioGroupName) {
                    upload.radioGroupName = '1'
                    field.radioGroupName = '1'
                } else {
                    field.radioGroupName = upload.radioGroupName
                }
                field.radioGroupName = '1'
                group.on('click.namespace', (e) => {
                    // alert('x')
                    upload.delField = field
                    upload.activate(field)
                    if (!field.radioGroupName) {
                        upload.radioGroupName = '1'
                        text.text('x' + '1')
                    } else {
                        upload.radioGroupName = field.radioGroupName
                        text.text('x' + upload.radioGroupName)
                    }

                })

                group.on('dragmove.namespace', (evt) => {
                    this.update_loc(field)
                })
            },

            field_icon: function(draw, field) {
                var g = draw.group()
                var w = 130
                var h = 36 //45
                var x = 65
                var y = 27
                var color = 'pink'
                var t = field.toolName
                var visibility = 'visible'

                if (
                    field.toolName == 'Date' ||
                    field.toolName == 'Name' ||
                    field.toolName == 'Email' ||
                    field.toolName == 'Date' ||
                    field.toolName == 'Dropdown' ||
                    field.toolName == 'Text'
                ) {
                    h = 20;
                    y = 15;
                    visibility = 'hidden';
                    if (field.toolName != 'Text') { w = w * 0.66 }
                }

                if (field.toolName == 'Checkbox') {
                    w = 20;
                    h = 20;
                    x = 8;
                    y = 15;
                    t = 'X';
                    visibility = 'hidden';
                }

                if (field.toolName == 'Initial') {
                    w = w * 0.66;
                    h = 36;
                    x = 47
                }

                if (field.toolName == 'Qrcode') {
                    w = 70;
                    h = 70;
                    x = 47
                }

                if (field.toolName == 'Qrcode') {
                    t = 'QR';
                    x = 50
                }

                if (field.person) {
                    color = this.getPersonColor(field.person.selectedPersonIndex)
                } else {
                    color = this.getPersonColor()
                }
                color = color.color

                var svgString = ` 
                <rect width="${w}" height="${h}" fill="${color}" />
                    <g>
                    <path visibility="${visibility}" fill-rule="evenodd" clip-rule="evenodd" d="M24.6696 16.3631C24.9458 16.0869 24.9458 15.6407 24.6696 15.3645L23.0121 13.7072C22.8797 13.5745 22.7001 13.5 22.5127 13.5C22.3253 13.5 22.1457 13.5745 22.0133 13.7072L20.625 15.0953L23.2812 17.7513L24.6696 16.3631ZM22.5729 18.4596L19.9167 15.8036L12.9396 22.78C12.8688 22.8508 12.8333 22.9358 12.8333 23.0349V25.188C12.8333 25.3864 12.9892 25.5422 13.1875 25.5422H15.3408C15.4329 25.5422 15.525 25.5068 15.5887 25.4359L22.5729 18.4596ZM25.5833 27.667H11.4167C10.6375 27.667 10 28.3044 10 29.0835C10 29.8626 10.6375 30.5 11.4167 30.5H25.5833C26.3625 30.5 27 29.8626 27 29.0835C27 28.3044 26.3625 27.667 25.5833 27.667Z" fill="black"/>
                    <text x="${x}" y="${y}" font-size="16" text-anchor="middle" fill="black">${t}</text>
							
                    </g>
                `

                //var group = draw.group()
                var icon = g.svg(svgString)

                if (false) {
                    icon = draw.image('https://lh3.googleusercontent.com/ogw/ADGmqu_qFVLtiqL-qH1UB15vuX7D-EtXCzNhORf1QpsPWA=s32-c-mo')
                        //image.size(100, 100)
                    theImage = icon
                }

                return icon
            },

            create_field_group: function(draw, field) {
                window.field_group_count++
                    //console.log('2. field_group_count = ' + window.field_group_count + ' +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
                var g = this.field_icon(draw, field)
                g._toolName = field.toolName

                /*var color = null
                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;  //todo eark : try using rect instead of square
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;
                var rect = draw.rect(w, h).fill(color.color)
                var text = draw.text(field.toolName)
                g.add(rect)
                g.add(text)
                */

                return g
            },

            can_resize: function(field) {
                return field.toolName == 'Signature' ||
                    field.toolName == 'Initial'
            },
            prepare_new_tool: function(mode, _field, _x, _y) {
                ////console.log('prepare_new_tool is called..')
                var field = _field || this.draggingTool


                if (!field) { //user pick no tool
                    //  //console.log('prepare_new_tool skipped Reason#1')    
                    return;
                }



                var hasGroup = (field.group != null)
                    ////console.log('hasGroup = '+ hasGroup)


                if (hasGroup) {
                    ////console.log('prepare_new_tool skippedReason#2 >>> already have group')
                    return; //user already have group install
                } else {
                    //field.has_group = true
                    ////console.log('prepare_new_tool skippedReason#2 >>> already have group')
                    //field.is_preparing = true
                }

                if (field.toolName == 'Radio') {
                    //console.log('show Radio')
                    this.showRadio()
                    return;
                }

                if (field.toolName == 'Radio2') {
                    //console.log('show Radio2')
                    this.showRadio2()
                    return;
                }



                if (this.last_add_field == field.id) {
                    //console.log('>>>>>>>>>>>>>>>>>>>>>>>>> duplicate bug')
                    upload.first_group.hide()
                } else {
                    this.last_add_field = field.id
                }

                //console.log(`1 prepare_new_tool mode ${mode} field = ${field.id}, group = ${field.group} ,hasGroup = ${hasGroup}--------------------------`)

                var draw = this.draw
                var g = this.create_field_group(draw, field);
                if (upload.first_group == null) {
                    upload.first_group = g
                    upload.first_field = field
                }

                field.group = g
                    //field.group = g //trial

                var rect = g.findOne('rect')
                var text = g.findOne('text')
                var w = rect.width()
                var h = rect.height()


                //scaling
                if (!field.scale) {
                    field.scale = 100
                }
                g._width1 = w
                g._width2 = g._width1 * 2
                g._width05 = parseInt(g._width1 / 2)

                // start control size
                var cir1 = draw.circle(10, 10).attr({ //left top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var cir2 = draw.circle(10, 10).attr({ // right top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var cir3 = draw.circle(10, 10).attr({ // left bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var cir4 = draw.circle(10, 10).attr({ // right bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir1 = draw.circle(10, 10).attr({ // right top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir2 = draw.circle(10, 10).attr({ // right top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir3 = draw.circle(10, 10).attr({ // right bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir4 = draw.circle(10, 10).attr({ // right bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                g._rect = rect
                g._cir1 = cir1
                g._cir2 = cir2
                g._cir3 = cir3
                g._cir4 = cir4
                g._text = text

                // cir1.draggable()
                cir1.draggable()
                cir2.draggable()
                cir3.draggable()
                cir4.draggable()

                // set element first               
                cir1.center(rect.x(), rect.y())
                cir2.center(rect.x() + rect.width(), rect.y())
                cir3.center(rect.x(), rect.y() + rect.height())
                cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                _cir1.hide()
                _cir2.hide()
                _cir3.hide()
                _cir4.hide()

                const ratio = rect.height() / rect.width()
                g._ratio = ratio
                cir1.on('dragstart.namespace', (evt) => {
                    _cir1.center(rect.x(), rect.y())
                    _cir1.show()
                    cir1.hide()
                })

                cir1.on('dragmove.namespace', (evt) => {
                    var oldx = _cir1.x()
                    var oldy = _cir1.y()
                        //var oldP = _cir1.pointAt()

                    _cir1.center(rect.x(), rect.y())
                    const wNew = cir4.x() - cir1.x()
                        //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir1.pointAt(_cir1);return;}

                    rect.size(wNew, wNew * ratio)
                    const rad = cir4.width() / 2
                    rect.move(cir4.x() - rect.width() + rad, cir4.y() - rect.height() + rad)
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())

                    //adjustIcon(text, rect,
                    //(cir1.x() + cir2.x()) / 2, (cir1.y() + cir4.y()) / 2)

                })

                cir1.on('dragend.namespace', (evt) => {
                    // rect.move(cir1.x(),cir1.y())
                    cir1.show()
                    _cir1.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                cir2.on('dragstart.namespace', (evt) => {
                    _cir2.center(rect.x() + rect.width(), rect.y())
                    _cir2.show()
                    cir2.hide()
                })

                cir2.on('dragmove.namespace', (evt) => {
                    var oldx = _cir2.x()
                    var oldy = _cir2.y()

                    _cir2.center(rect.x() + rect.width(), rect.y())
                    const hNew = cir3.y() - cir2.y()

                    //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir2.move(oldx, oldy);return;}

                    const rad = cir3.width() / 2
                    rect.move(cir3.x() + rad, cir2.y() + rad)
                    rect.size(hNew / ratio, hNew)
                    cir1.center(rect.x(), rect.y())
                    cir4.center(rect.x() + rect.width(), rect.y() + hNew)
                        //text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                        //adjustIcon(text, rect,
                        // (cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                })

                cir2.on('dragend.namespace', (evt) => {
                    // rect.move(cir1.x(),cir1.y())
                    cir2.show()
                    _cir2.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                cir3.on('dragstart.namespace', (evt) => {
                    _cir3.center(rect.x(), rect.y() + rect.height())
                    _cir3.show()
                    cir3.hide()
                })

                cir3.on('dragmove.namespace', (evt) => {
                    var oldx = _cir3.x()
                    var oldy = _cir3.y()

                    _cir3.center(rect.x(), rect.y() + rect.height())
                    const wNew = cir2.x() - cir3.x()

                    // if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir3.move(oldx, oldy);return;}

                    const rad = cir3.width()
                    rect.move(cir3.x() + rad, cir2.y() + rad)
                    rect.size(wNew, wNew * ratio)
                    cir1.center(rect.x(), rect.y())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                        //text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                        //adjustIcon(text, rect,
                        // text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2))
                })

                cir3.on('dragend.namespace', (evt) => {
                    const { handler, box } = evt.detail
                    let { x, y } = box
                    // set element new
                    cir3.show()
                    _cir3.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                cir4.on('dragstart.namespace', (evt) => {
                    _cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    _cir4.show()
                    cir4.hide()
                })

                cir4.on('dragmove.namespace', (evt) => {
                    var oldx = _cir4.x()
                    var oldy = _cir4.y()
                    _cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    const wNew = cir4.x() - cir1.x()

                    //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir4.move(oldx, oldy);return;}

                    rect.size(wNew, wNew * ratio)
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    _cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                        //text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                        //adjustIcon(text, rect,
                        // text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2))
                })

                cir4.on('dragend.namespace', (evt) => {
                    const { handler, box } = evt.detail
                    let { x, y } = box
                    // set element new
                    // rect.move(cir1.x(),cir1.y())
                    cir4.show()
                    _cir4.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                        // cir4.origin = {x:cir4.x(), y:cir4.y()} 
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                if (upload.can_resize(field)) {
                    g.add(cir1)
                    g.add(cir2)
                    g.add(cir3)
                    g.add(cir4)
                } else {
                    cir1.hide();
                    cir2.hide();
                    cir3.hide();
                    cir4.hide();
                }
                // end control size

                g.on('click.namespace', (e) => {
                    //  alert('prepare_new_tool.click')
                })

                g.on('dragstart.namespace', (e) => {
                    // alert('prepare_new_tool.dragstart')
                })

                g.on('dragend.namespace', (e) => {

                    if (field.note_field) {
                        this.move_note_field(field)
                    }

                })

                g.on('dragmove.namespace', (e) => {
                    const { handler, box } = e.detail
                    const constrains = document.getElementById('svg')
                    max_y = constrains.attributes.height.value
                    max_x = constrains.attributes.width.value
                    let { x, y } = box
                    if (x < 0) {
                        e.preventDefault()
                        x = 0
                    }
                    if (box.x2 > upload.adjustedWidth) {
                        //if (box.x2+70 > max_x) {    
                        e.preventDefault()
                        x = upload.adjustedWidth - box.w
                    }
                    if (y < 0) {
                        e.preventDefault()
                        y = 0
                    }
                    if (box.y2 > max_y) {
                        e.preventDefault()
                        y = max_y - box.h
                    }
                    handler.move(x, y)
                    this.update_loc(field)


                })

                field.group = g

                // setTimeout(function(){
                upload.attach_drag(g)
                    // },1000)

                g._field_id = field.id
                g_field = field
                this.activate(field)

                if (mode == 2 || mode == 1) {
                    g.hide()
                }

                if (_x) {
                    //console.log(g, _x, _y)
                    g.move(_x, _y)
                    g.show()
                    upload.restore_scale(g)
                }

                //console.log("3. Exit prepare_new_tool g = " + field.group + ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>")
                //console.log(field)

                //if (field.toolName == "Signature") app.add_field_note(field)
            },
            add_field_note: function(field) {
                var draw = this.draw
                var g = draw.group()
                var w = 140
                var h = 70 //45
                var x = 65
                var y = 27
                var color = 'pink'
                var t = field.toolName
                var visibility = 'visible'

                if (field.person) {
                    color = this.getPersonColor(field.person.selectedPersonIndex)
                } else {
                    color = this.getPersonColor()
                }
                color = color.color

                var svgString = ` 
                <rect width="${w}" height="${h}" fill="${color}" />
                    <g>
                    <path visibility="${visibility}" fill-rule="evenodd" clip-rule="evenodd" d="M24.6696 16.3631C24.9458 16.0869 24.9458 15.6407 24.6696 15.3645L23.0121 13.7072C22.8797 13.5745 22.7001 13.5 22.5127 13.5C22.3253 13.5 22.1457 13.5745 22.0133 13.7072L20.625 15.0953L23.2812 17.7513L24.6696 16.3631ZM22.5729 18.4596L19.9167 15.8036L12.9396 22.78C12.8688 22.8508 12.8333 22.9358 12.8333 23.0349V25.188C12.8333 25.3864 12.9892 25.5422 13.1875 25.5422H15.3408C15.4329 25.5422 15.525 25.5068 15.5887 25.4359L22.5729 18.4596ZM25.5833 27.667H11.4167C10.6375 27.667 10 28.3044 10 29.0835C10 29.8626 10.6375 30.5 11.4167 30.5H25.5833C26.3625 30.5 27 29.8626 27 29.0835C27 28.3044 26.3625 27.667 25.5833 27.667Z" fill="black"/>
                    <text x="${x}" y="${y}" font-size="16" text-anchor="middle" fill="black">Note</text>
							
                    </g>
                `
                var icon = g.svg(svgString)

                if (false) {
                    icon = draw.image('https://lh3.googleusercontent.com/ogw/ADGmqu_qFVLtiqL-qH1UB15vuX7D-EtXCzNhORf1QpsPWA=s32-c-mo')
                    theImage = icon
                }

                g.draggable()

                // g.on('click', function () {
                //     app.activate(field)
                //     // app.field_stamp = stamp
                // })

                // g.on('dragend.namespace', (evt) => {
                //     // app.stamp_image = ""
                //     // app.stamp_id = ""
                //     app.update_loc(field)
                // })

            },
            /*    
            prepare_new_tool: function (mode, _field, _x, _y) {
                // //console.log(`prepare_new_tool mode ${mode}`)  
                var field = _field || this.draggingTool
                //console.log(field, _x, _y)
                if (!field) {
                    //  //console.log('prepare_new_tool skipped Reason#1')    
                    return; //user pick no tool
                }


                if (field.toolName == 'Radio') {
                    this.showRadio()
                    return;
                }

                if (field.toolName == 'Radio2') {
                    this.showRadio2()
                    return;
                }


                if (field.group) {
                    //console.log('prepare_new_tool skippedReason#2 >>> already have group')
                    return; //user already have group install
                }

                var draw = this.draw
                var g = draw.group()
                g._toolName = field.toolName

                if (field.person) {
                    color = this.getPersonColor(field.person.selectedPersonIndex)
                    //alert(field.person.selectedPersonIndex+':'+color.color)
                } else {
                    color = this.getPersonColor()
                }
                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;  //todo eark : try using rect instead of square
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;
                //    alert(field.person.selectedPersonIndex+":"+color.color)
                var rect = draw.rect(w, h).fill(color.color)
                // var text = draw.text(field.toolName+this.fieldID())    
                var text = draw.text(field.toolName)
                g.add(rect)
                g.add(text)
                //    g.add(draw.rect(70,70).fill(color)) 
                //    g.add(draw.text(field.toolName)) 

                //scaling
                if (!field.scale) {
                    field.scale = 100
                }
                g._width1 = w
                g._width2 = g._width1 * 2
                g._width05 = parseInt(g._width1 / 2)

                // start control size
                var cir1 = draw.circle(10, 10).attr({ //left top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var cir2 = draw.circle(10, 10).attr({ // right top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var cir3 = draw.circle(10, 10).attr({ // left bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var cir4 = draw.circle(10, 10).attr({ // right bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir1 = draw.circle(10, 10).attr({ // right top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir2 = draw.circle(10, 10).attr({ // right top
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir3 = draw.circle(10, 10).attr({ // right bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                var _cir4 = draw.circle(10, 10).attr({ // right bottom
                    fill: "red",
                    stroke: "none",
                    opacity: .5
                })
                g._rect = rect
                g._cir1 = cir1
                g._cir2 = cir2
                g._cir3 = cir3
                g._cir4 = cir4
                g._text = text

                // cir1.draggable()
                cir1.draggable()
                cir2.draggable()
                cir3.draggable()
                cir4.draggable()

                // set element first               
                cir1.center(rect.x(), rect.y())
                cir2.center(rect.x() + rect.width(), rect.y())
                cir3.center(rect.x(), rect.y() + rect.height())
                cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                _cir1.hide()
                _cir2.hide()
                _cir3.hide()
                _cir4.hide()

                const ratio = rect.height() / rect.width()
                g._ratio = ratio
                cir1.on('dragstart.namespace', (evt) => {
                    _cir1.center(rect.x(), rect.y())
                    _cir1.show()
                    cir1.hide()
                })

                cir1.on('dragmove.namespace', (evt) => {
                    var oldx = _cir1.x()
                    var oldy = _cir1.y()
                    //var oldP = _cir1.pointAt()

                    _cir1.center(rect.x(), rect.y())
                    const wNew = cir4.x() - cir1.x()
                    //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir1.pointAt(_cir1);return;}

                    rect.size(wNew, wNew * ratio)
                    const rad = cir4.width() / 2
                    rect.move(cir4.x() - rect.width() + rad, cir4.y() - rect.height() + rad)
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir4.y()) / 2)
                })

                cir1.on('dragend.namespace', (evt) => {
                    // rect.move(cir1.x(),cir1.y())
                    cir1.show()
                    _cir1.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                cir2.on('dragstart.namespace', (evt) => {
                    _cir2.center(rect.x() + rect.width(), rect.y())
                    _cir2.show()
                    cir2.hide()
                })

                cir2.on('dragmove.namespace', (evt) => {
                    var oldx = _cir2.x()
                    var oldy = _cir2.y()

                    _cir2.center(rect.x() + rect.width(), rect.y())
                    const hNew = cir3.y() - cir2.y()

                    //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir2.move(oldx, oldy);return;}

                    const rad = cir3.width() / 2
                    rect.move(cir3.x() + rad, cir2.y() + rad)
                    rect.size(hNew / ratio, hNew)
                    cir1.center(rect.x(), rect.y())
                    cir4.center(rect.x() + rect.width(), rect.y() + hNew)
                    text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                })

                cir2.on('dragend.namespace', (evt) => {
                    // rect.move(cir1.x(),cir1.y())
                    cir2.show()
                    _cir2.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                cir3.on('dragstart.namespace', (evt) => {
                    _cir3.center(rect.x(), rect.y() + rect.height())
                    _cir3.show()
                    cir3.hide()
                })

                cir3.on('dragmove.namespace', (evt) => {
                    var oldx = _cir3.x()
                    var oldy = _cir3.y()

                    _cir3.center(rect.x(), rect.y() + rect.height())
                    const wNew = cir2.x() - cir3.x()

                    // if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir3.move(oldx, oldy);return;}

                    const rad = cir3.width()
                    rect.move(cir3.x() + rad, cir2.y() + rad)
                    rect.size(wNew, wNew * ratio)
                    cir1.center(rect.x(), rect.y())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                })

                cir3.on('dragend.namespace', (evt) => {
                    const { handler, box } = evt.detail
                    let { x, y } = box
                    // set element new
                    cir3.show()
                    _cir3.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    upload.update_scale(g)
                    this.update_loc(field)
                })

                cir4.on('dragstart.namespace', (evt) => {
                    _cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    _cir4.show()
                    cir4.hide()
                })

                cir4.on('dragmove.namespace', (evt) => {
                    var oldx = _cir4.x()
                    var oldy = _cir4.y()
                    _cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    const wNew = cir4.x() - cir1.x()

                    //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir4.move(oldx, oldy);return;}

                    rect.size(wNew, wNew * ratio)
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    _cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    text.center((cir1.x() + cir2.x()) / 2, (cir1.y() + cir3.y()) / 2)
                })

                cir4.on('dragend.namespace', (evt) => {
                    const { handler, box } = evt.detail
                    let { x, y } = box
                    // set element new
                    // rect.move(cir1.x(),cir1.y())
                    cir4.show()
                    _cir4.hide()
                    cir1.center(rect.x(), rect.y())
                    cir2.center(rect.x() + rect.width(), rect.y())
                    cir3.center(rect.x(), rect.y() + rect.height())
                    cir4.center(rect.x() + rect.width(), rect.y() + rect.height())
                    // cir4.origin = {x:cir4.x(), y:cir4.y()} 
                    upload.update_scale(g)
                    this.update_loc(field)
                })


                g.add(cir1)
                g.add(cir2)
                g.add(cir3)
                g.add(cir4)
                // end control size

                g.on('click.namespace', (e) => {
                    //  alert('prepare_new_tool.click')
                })

                g.on('dragstart.namespace', (e) => {
                    // alert('prepare_new_tool.dragstart')
                })

                g.on('dragmove.namespace', (e) => {
                    const { handler, box } = e.detail
                    const constrains = document.getElementById('svg')
                    max_y = constrains.attributes.height.value
                    max_x = constrains.attributes.width.value
                    let { x, y } = box
                    if (x < 0) {
                        e.preventDefault()
                        x = 0
                    }
                    if (box.x2 > upload.adjustedWidth) {
                        //if (box.x2+70 > max_x) {    
                        e.preventDefault()
                        x = upload.adjustedWidth - box.w
                    }
                    if (y < 0) {
                        e.preventDefault()
                        y = 0
                    }
                    if (box.y2 > max_y) {
                        e.preventDefault()
                        y = max_y - box.h
                    }
                    handler.move(x, y)
                    this.update_loc(field)
                })


                upload.attach_drag(g)
                field.group = g
                g._field_id = field.id
                g_field = field
                this.activate(field)

                if (mode == 2 || mode ==1) {
                    g.hide()
                }

                if (_x) {
                    //console.log(g, _x, _y)
                    g.move(_x, _y)
                    g.show()
                    upload.restore_scale(g)
                }


            },*/
            prepare_new_tool_self_sign: function(mode) {
                //console.log(`prepare_new_tool_self_sign mode ${mode}`)
                var field = this.draggingTool
                var that = this
                if (!field) {
                    //console.log('prepare_new_tool skipped Reason#1')
                    return; //user pick no tool
                }
                if (field.group) {
                    ////console.log('prepare_new_tool skippedReason#2 >>> already have group')
                    return; //user already have group install
                }
                //console.log(this.draw)
                var draw = this.draw
                var g = null
                const api_url = api_host + 'choose_signature'
                var data = { sign_type: 'draw', field_id: field.id, code: getUsername() }
                if (field.toolName === "Signature") {
                    //console.log("Signature")
                    field.type = 'draw'
                        // var url = '../edoc/app-assets/images/signature/'+oImg.doc_field.email+oImg.doc_field.id+'_'+oImg.doc_field.type+'.png'
                    var draw_src = "../edoc/app-assets/images/signature/" + getUsername() + "_draw.png?rd=" + new Date().getTime()
                    var gen_src = "../edoc/app-assets/images/signature/" + getUsername() + "_gen.png?rd=" + new Date().getTime()

                    //var draw_sign = this.check_signature(draw_src)
                    //var gen_sign = this.check_signature(gen_src)

                    var draw_sign = (upload.userField && upload.userField.signature == 'draw')
                    var gen_sign = (upload.userField && upload.userField.signature == 'gen')

                    if (draw_sign === true || gen_sign === true) {
                        var type = (draw_sign == true) ? 'draw' : 'gen'
                        var url = '../edoc/app-assets/images/signature/' + getUsername() + '_' + type + '.png?rd=' + new Date().getTime()
                        field.type = type
                        //console.log(url)
                        g = draw.image(url).size(250, 150)
                            //g = draw.image(url).size(100, 50)

                        //g.attr('alignment-baseline','top')

                        //g = draw.foreignObject(250, 150)

                        var ele = null
                        ele = document.createElement('img')
                        if (false) {
                            ele.src = url
                            ele.width = '100'
                            ele.height = '100'
                            g.add(ele)
                        }


                        //console.log('xxx1111')
                            // call choose_signature.lua
                        $.ajax({
                            url: api_url,
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function(result) {
                                //console.log(result)
                            },
                            error: function(error) {
                                //console.log("eeee draft error: " + error)
                            }
                        })
                    } else {
                        var url = '../edoc/app-assets/images/signature/no_email.png'
                        that.signDlgDelay(field, field, 200)

                        //upload.userField.signature = '' 

                        g = draw.image(url).size(250, 150)
                        g.attr('alignment-baseline', 'top')
                        //console.log('xxx22222')
                            // call choose_signature.lua
                        $.ajax({
                            url: api_url,
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function(result) {
                                //console.log(result)
                            },
                            error: function(error) {
                                //console.log("eeee draft error: " + error)
                            }
                        })
                    }
                } else if (field.toolName === "Initial") {
                    //console.log("Initial")
                    var draw_src = "../edoc/app-assets/images/signature/" + getUsername() + "_draw_initial.png?rd=" + new Date().getTime()
                    var gen_src = "../edoc/app-assets/images/signature/" + getUsername() + "_gen_initial.png?rd=" + new Date().getTime()

                    //var draw_ini = this.check_signature(draw_src)
                    //var gen_ini = this.check_signature(gen_src)
                    //console.log(' >>>>>>>> upload.userField.initial = ' + upload.userField.initial)
                    var draw_ini = (upload.userField && upload.userField.initial == 'draw')
                    var gen_ini = (upload.userField && upload.userField.initial == 'gen')

                    field.type = 'draw_initial'
                    if (draw_ini === true || gen_ini === true) {
                        var type = (draw_ini == true) ? 'draw_initial' : 'gen_initial'
                        data.sign_type = type
                        field.type = type
                        var url = '../edoc/app-assets/images/signature/' + getUsername() + '_' + type + '.png?rd=' + new Date().getTime()
                        g = draw.image(url).size(250, 250)
                        $.ajax({
                            url: api_url,
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function(result) {
                                //console.log(result)
                            },
                            error: function(error) {
                                //console.log("Save draft error: " + error)
                            }
                        })
                    } else {
                        data.sign_type = 'draw_initial'
                            //var url = '../edoc/app-assets/images/signature/'+'thanaphas@gmail.com'+'_'+'gen_initial'+'.png'
                        var url = '../edoc/app-assets/images/signature/no_email.png'
                            //upload.userField.initial = ''

                        that.signDlgDelay(field, field, 200)



                        g = draw.image(url).size(250, 250)
                        $.ajax({
                            url: api_url,
                            type: "POST",
                            data: JSON.stringify(data),
                            success: function(result) {
                                //console.log(result)
                            },
                            error: function(error) {
                                //console.log("Save draft error: " + error)
                            }
                        })
                    }
                } else if (field.toolName === 'Date' || field.toolName === 'Name' || field.toolName === 'Email') {
                    //console.log("Date")
                    var svg = SVG('#svg')
                    //console.log(svg.width(), svg.height())
                    g = draw.foreignObject(svg.width(), svg.height())
                    g._toolName = field.toolName
                    var ele = null
                    ele = document.createElement('input')
                    if (field.toolName === 'Name') {
                        ele.value = getUserField("adminFisrtname")
                        field.toolData = getUserField("adminFisrtname")
                    } else if (field.toolName === 'Email') {
                        ele.value = getUserField("adminEmail")
                        field.toolData = getUserField("adminEmail")
                    } else {
                        ele.value = moment().format('DD/MM/YYYY')
                        field.toolData = moment().format('DD/MM/YYYY')
                        ele.readOnly = true
                    }
                    ele.style.margin = '10px'
                    ele.style.padding = '3px'
                    ele.style.border = "10px solid yellow"
                    ele.setAttribute("type", "text");
                    ele.onclick = function() {
                        g.draggable(false)
                        ele.focus()
                        upload.canDel = false
                    }
                    ele.onblur = function() {
                        upload.attach_drag(g)
                        field.toolData = ele.value
                        upload.canDel = true
                    }
                    g.add(ele)
                } else if (field.toolName === 'Text') {
                    //console.log("Text")
                    var svg = SVG('#svg')
                    g = draw.foreignObject(svg.width(), svg.height())
                    g._toolName = field.toolName
                    var ele = null
                    ele = document.createElement('textarea')
                    ele.name = 'text_val2'
                    ele.value = ''
                    ele.style.border = "10px solid yellow"


                    ele.onclick = function() {
                        g.draggable(false)
                        ele.focus()
                        upload.canDel = false
                    }
                    ele.onblur = function() {
                        upload.attach_drag(g)
                        field.toolData = ele.value
                        upload.canDel = true
                        wrapText(ele.value, field)
                    }
                    g.add(ele)
                } else if (field.toolName === 'Qrcode') {
                    //console.log("Qrcode")
                    var svg = SVG('#svg')
                    var url = 'images/field_Qrcode.png'
                    g = draw.image(url).size(130, 130)

                    //g = draw.foreignObject(svg.width(),svg.height())
                    g._toolName = field.toolName
                    field.toolData = esign_path + '/#/document?id=' + upload.pdfFile
                        //alert(field.toolData)

                } else if (field.toolName === 'Checkbox') {
                    //console.log("Checkbox")
                    var svg = SVG('#svg')
                    g = draw.foreignObject(svg.width(), svg.height())
                    g._toolName = field.toolName
                    var ele = null
                    ele = document.createElement('input')
                    ele.style.border = "3px solid yellow"
                    ele.setAttribute("type", "checkbox")
                    ele.checked = true
                    ele.value = 'X'
                    field.toolData = 'X'

                    g.add(ele)
                }

                upload.attach_drag(g)
                field.group = g
                g._field_id = field.id
                g_field = field
                this.activate(field)

                if (mode == 2 || mode == 1) {
                    g.hide()
                }
            },
            check_signature(url) {
                var http = new XMLHttpRequest();
                http.open('HEAD', url, false);
                http.send();
                return http.status != 404;
            },
            mousemove_prepare_new_tool: function(evt) {
                // //console.log('mousemove_prepare_new_tool '+evt.x+','+evt.y+'\n'+ evt.clientX+','+evt.clientY)
                var svg = document.getElementById('svg')
                var xoffset = svg.parentNode.getBoundingClientRect().x
                xmin = xoffset
                xmax = xoffset + svg.parentNode.getBoundingClientRect().width
                var a = evt.x
                    // if(app.draggingTool){
                    //     var f = app.draggingTool
                    //     var w = (f.toolName == 'Signature' || f.toolName == 'Initial') ? 70 : 150;
                    //     a = a + w   
                    //     if(a > xoffset + upload.adjustedWidth){
                    //         evt.preventDefault()
                    //         return;
                    //     }                 
                    // }               
                var field = this.draggingTool
                    //this.draggingTool = null

                if (!field) {
                    // //console.log('  mousemove_prepare_new_tool skipped')    
                    return; //user pick no tool
                } else {
                    var f = app.draggingTool
                    var w = (f.toolName == 'Signature' || f.toolName == 'Initial') ? 70 : 150;
                    a = a + w + 10
                    if (a > xoffset + upload.adjustedWidth) {
                        evt.preventDefault()
                        return;
                    }
                }
                const { x, y, clientX, clientY, offsetX, offsetY } = evt
                ////console.log(x,y , ' | ' ,clientX,clientY, '|' , offsetX , offsetY)
                if (field.isNew) {
                    field.isNew = false
                    field.group.show()
                    //console.log("show new item for the first time>>>>>>>>>>>>>")
                }
                field.group.move(offsetX, offsetY)
            },

            insert_new_tool: function() {
                //console.log("start insert_new_tool")
                var field = this.draggingTool

                if (!field || this.is_cloning) {
                    //console.log('  insert_new_tool skipped')
                    return; //user pick no tool›
                }

                this.draggingTool = null
                this.addField(field)
                if (field.person.amount_least_sign > 1) {
                    //console.log('call clone this field')
                    this.is_cloning = true // prevent duplicate field if mouse move too fast
                    cloneThisField(field)
                    this.is_cloning = false
                    this.draggingTool = null
                }
                //console.log("exit insert_new_tool " + field.group + ", dragging tool " + this.draggingTool)

            },
            addField: function(field, n) {
                //console.log(' addField ' + field.toolName)
                var d = new Date();
                var t = d.getTime();
                field.email = this.selectedPerson

                if (field.toolName == 'Approve') {
                    this.disabled_approve = true
                }
                if (this.is_no_sign) {
                    //console.log(' addField ==============' + field.toolName)
                    this.selectedPersonObj.concensus_id = field.toolName + t
                }

                field.person = this.selectedPersonObj
                field.is_no_sign = this.is_no_sign
                field.is_clone = false
                this.id_field = field.id
                this.fields.push(field)
                this.delField = field;
                this.update_loc(field)
                upload.field_id = field.id
                //console.log("New Field " + field.group + " Has been added >>>> " + this.fields.length)

                if (field.toolName == 'Signature') {
                    this.add_field_note2(field)
                }

                this.check_fiels_no_sign(field.toolName)

            },
            check_fiels_no_sign: function(toolName) {
                var that = this

                var search_people = that.people.filter(function(p) {
                    return p.needToSign == true && p.is_inspector == false
                })

                if (this.is_no_sign) {
                    var sign_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Signature'
                    })
                    if (search_people.length == sign_fields.length) {
                        that.toolSignature = false
                    } else {
                        that.toolSignature = true
                    }

                    var email_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Email'
                    })
                    if (search_people.length == email_fields.length) {
                        that.toolEmail = false
                    } else {
                        that.toolEmail = true
                    }

                    var date_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Date'
                    })
                    if (search_people.length == date_fields.length) {
                        that.toolDate = false
                    } else {
                        that.toolDate = true
                    }

                    var name_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Name'
                    })
                    if (search_people.length == name_fields.length) {
                        that.toolName = false
                    } else {
                        that.toolName = true
                    }

                    var text_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Text' && f.note_of == undefined
                    })
                    if (search_people.length == text_fields.length) {
                        that.toolText = false
                    } else {
                        that.toolText = true
                    }

                    var attachment_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Attachment'
                    })
                    if (search_people.length == attachment_fields.length) {
                        that.toolAttachment = false
                    } else {
                        that.toolAttachment = true
                    }

                    var phone_fields = that.fields.filter(function(f) {
                        return f.is_no_sign == true && f.toolName == 'Phone'
                    })
                    if (search_people.length == phone_fields.length) {
                        that.toolPhone = false
                    } else {
                        that.toolPhone = true
                    }


                } else {
                    that.toolSignature = true
                    that.toolEmail = true
                    that.toolDate = true
                    that.toolName = true
                    that.toolText = true
                    that.toolAttachment = true
                    that.toolPhone = true
                }
            },
            add_field_note2(field) {
                var field2 = {
                    "id": this.fieldID(),
                    "toolName": 'Text',
                    "isNew": false,
                    "toolData": "",
                    "isHidden": false, //note field
                    "note_of": field.id //note field
                }

                this.draggingTool = field2
                this.prepare_new_tool(1, field2)


                field2.group.show()
                var x = field.group.x() + (field.group.width())
                field2.group.move(x, field.group.y() + 0)

                this.insert_new_tool()
                this.draggingTool = null

                field.note_field = field2
                this.note_field = field2
            },

            hideNotes: function() {
                var boo = !this.hide_note
                //console.log('hide notes = ' + boo);
                for (var i = 0; i < this.fields.length; i++) {
                    var f = this.fields[i]
                    if (f.note_of) {
                        f.isHidden = boo
                        if (f.isHidden) {
                            f.group.hide()
                        } else {
                            f.group.show()

                        }
                    }


                }

            },

            move_note_field: function(field) {
                var field2 = field.note_field
                var x = field.group.x() + (field.group.width())
                field2.group.move(x, field.group.y() + 0)
                app.update_loc(field2)
            },

            dropToSvg: function(evt) {
                evt.preventDefault()
                //console.log("dropToSvg")
                window.dropEvt = evt

                var field = this.draggingTool


                if (!field) {
                    //console.log(' dropToSvg skipped')
                    return; //user pick no tool
                }
                const { x, y, clientX, clientY, offsetX, offsetY } = evt
                //console.log(x, y, ' | ', clientX, clientY, '|', offsetX, offsetY)
                field.group.move(offsetX, offsetY)
                field.group.show()
                //console.log("dropToSvg >>>> " + field.group)
            },
            auto_remove_field: function(i) {
                var that = this
                that.remove_field_order = i
                if (that.fields.length > 0) {
                    if (that.people[i].is_group && that.people[i].is_group == true) {
                        for (j = 0; j < that.fields.length; j++) {
                            if (that.fields[j].person.id_group && that.people[i].id_group == that.fields[j].person.id_group) {
                                //console.log(that.people[i].id_group + '=====' + that.fields[j].person.id_group);
                                //console.log('field---' + j);
                                that.list_remove_field.push(j)
                            }
                        }
                    } else {
                        for (j = 0; j < that.fields.length; j++) {
                            if (that.people[i].email == that.fields[j].email) {
                                //console.log(that.people[i].email + '=====' + that.fields[j].email);
                                //console.log('field---' + j);
                                that.list_remove_field.push(j)
                            }
                        }
                    }
                }
                if (that.list_remove_field.length > 0) {
                    $("#removefieldModal").modal("show");
                }
            },
            remove_field_step2: function() {
                var that = this
                for (var i = that.list_remove_field.length - 1; i >= 0; i--) {
                    that.fields.splice(that.list_remove_field[i], 1);
                }
                $("#removefieldModal").modal("hide");
                that.list_remove_field = []

            },
            no_remove_field: function() {
                var that = this
                that.people[that.remove_field_order].needToSign = true
                $("#removefieldModal").modal("hide");
                that.list_remove_field = []
            },
            select_secret_level: function(l) {
                var that = this
                that.secret_level = l

                if (that.secret_level > 1) {
                    for (n = 0; n < that.people.length; n++) {
                        that.people[n].usePassword = true
                    }
                } else {
                    for (n = 0; n < that.people.length; n++) {
                        if (that.people[n].password == "") {
                            that.people[n].usePassword = false
                        }
                    }
                }

            },
            check_input_password_secret: function() {
                var that = this
                var can_next = true
                if (that.secret_level > 1) {
                    for (i = 0; i < that.people.length; i++) {
                        if (that.people[i].password == "") {
                            can_next = false
                            $("#requespassModal").modal("show");
                            return false
                        }
                    }
                }
                if (can_next) that.next_step()
            },
            check_mail_company: function() {
                var that = this
                var can_next = true
                that.warning_company_mail = []
                var filter_company = that.people.filter(function(people_el) {
                    that.warning_company_mail[people_el.email] = false
                    return that.members.filter(function(array_member) {
                        if (people_el.email == array_member.email) that.warning_company_mail[people_el.email] = true
                        return people_el.email == array_member.email;
                    }).length == 0
                })
                //console.log(that.warning_company_mail);
                if (filter_company.length > 0) {
                    can_next = false
                } else {
                    that.warning_company_mail = []
                }
                if (can_next) that.next_step()
            },
            check_use_iso: function() {
                var that = this
                var email = getUsername()
                var n = email.search("@creden.");
                if (n > 0) {
                    that.can_use_iso = true
                }
            },
            done: function() {
                location.href = document.location.origin + '/bla/#/dashboard'
            },
            t_onover: function(id) {
                var selected = document.getElementById(id)
                var tooltip = new bootstrap.Tooltip(selected)
                tooltip.show()
            },
            change_delegates_status: function(i, s) {
                var that = this
                that.people[i].delegates_sign = s
                if (s == false) {
                    that.people[i].delegates_mail = ''
                    that.people[i].delegates_name = ''
                }

            },
            /*            add_list_cc: function (e, n) {
                            var that = this
                            var checkbox = document.getElementById("check_member_" + e)
                            var index_people = false
                            var search = that.people.filter(function (p, i) {
                                if (p.email == e && p.needToSign == false) {
                                    index_people = i
                                    return i
                                } else {
                                    return false
                                }
                            })

                            if (checkbox.checked == true && index_people == false) {
                                that.people.push({
                                    name: n,
                                    email: e,
                                    needToSign: false,
                                    title: '',
                                    order_set: '',
                                    no: '',
                                    is_group: false,
                                    user_group: [],
                                    least_sign: false,
                                    amount_least_sign: 1,
                                    delegates_sign: false,
                                    delegates_mail: '',
                                    is_inspector: false
                                })
                            } else {
                                if (search != false) {
                                    that.people.splice(index_people, 1)
                                }
                            }
                        },
            */
            add_list_cc: function(e, n, c, g, evt) {
                var that = this
                var ele = evt.target
                var boo = ele.checked
                let keyid = g
               
                 
             
                
                //var search = g.group_sign.filter(function(p, i) {
              
              //  })
              ////console.log(boo)
           
                if (boo) {
                        that.list_cc.length
                    that.list_cc.push({email:e ,name:n ,checked:true,get_groupname:g}) 
                    that.list_member_department.push(parseInt(g)) 
                    that.list_member_department = [...new Set(that.list_member_department)];                   
                }else{              
                                     
                
                    
                     for(let key in that.list_cc){
                    
                         if (that.list_cc[key].email == e) { Vue.delete(that.list_cc, key); }
                         

                     }
                     arraydep=[]
                    for(let keys in that.list_member_department ){
                       
                            for(let kryss in that.list_cc){
                              
                                if (that.list_cc[kryss].get_groupname != that.list_member_department[keys]) {
                                    
                                    
                                    arraydep.push(parseInt(that.list_cc[kryss].get_groupname))
                                }
                                    // if (that.list_cc[keyss].get_groupname == that.list_member_department[keys]) {
                                    //     //console.log(1)
                                    // }else{
                                    //     //console.log(0)
                                    // }
                            }
                            
                    }
                    //console.log( that.list_member_department)
                    that.list_member_department = [...new Set(arraydep)]; 
                    
                            
                    
                    
                 }
                 //console.log(that.group_sign)
                 ////console.log(that.show_list_cc)
                 ////console.log(that.group_sign)
                 //console.log(that.list_member_department)
                // //console.log(that.list_cc)
            },
            //check show list mail
            
            check_list_mail: function(group_id,type){
                let that = this
                let g = that.group_sign[group_id]
                that.show_list_cc = g.group_sign
                that.show_list_cc_group =g.group_sign
              
                if (type == 'list_group') { 
                    if ( that.list_member_department.length == 0 ) {
                        that.list_member_department.push(group_id)
                        g.group_sign.forEach((value, key) =>   that.list_cc.push({email:g.group_sign[key].email  ,name:g.group_sign[key].name ,checked:true,get_groupname:group_id}) );
                        
                    }else{
                       
                        if (that.list_member_department.indexOf(group_id) < 0) {
                          
                            that.list_member_department.push(group_id)
                      
                            g.group_sign.forEach((value, keys) =>   that.list_cc.push({email:g.group_sign[keys].email  ,name:g.group_sign[keys].name ,checked:true,get_groupname:group_id}) );
                        }
                      
                    }
                    that.list_member_department = [...new Set(that.list_member_department)]; 
                    that.show_list_cc.forEach((list, listkey) => Vue.set(that.show_list_cc[listkey], 'checked', true));
                   
                    //console.log(that.list_member_department)
                    //console.log(that.list_cc)
               
                }else if(type == 'move_list'){
                    //console.log(that.list_cc)
                    that.list_cc.forEach((index,keyval) => {if(that.list_cc[keyval].get_groupname == group_id){
                        //console.log(that.list_cc)
                        Vue.delete(that.list_cc, keyval)
                    }})

                    that.list_member_department.forEach((indexs,keydep)=> {if(that.list_member_department[keydep]==group_id){Vue.delete(that.list_member_department, keydep)}})
                   
                    that.show_list_cc.forEach((valcc,keycc) =>  Vue.set(that.show_list_cc[keycc], 'checked', false))
                    
                }
            
                
            },
            
            check_list_department: function(){

            },            

            //end check list mail
            add_all_list: function(g, index,rc) {
                var that = this
       
               
                let  group_id = index
                let status  = rc
              
             
            
               
              if (rc == true) {
                let type  = 'list_group'
                that.check_list_mail(group_id,type,status);
                that.check_list_department(index);
                
                   //console.log(index)          
                                   
                                    
                }else{              
                    let type  = 'move_list'       
                    that.check_list_mail(group_id,type,status);
                    that.check_list_department(index);
                                     
                    //console.log(index)     
                }
               

            },
            add_all_list_cc: function(g, index) {
                var that = this
                var checkbox = document.getElementById("check_group_" + g._id)

                if (checkbox.checked == true) {
                   
                    var group = {
                        id_group: g._id,
                        name_group: g.group_name,
                        cc: []
                    }

                    for (i = 0; i < g.group_sign.length; i++) {
                        group.cc.push({
                            name: g.group_sign[i].name,
                            email: g.group_sign[i].email
                        })
                    }
                    var status = that.list_cc.find(function(f) {
                        return f.id_group == group.id_group
                    })
                    if (!status) {
                        that.list_cc.push(group)
                        that.id_group_cc = g._id
                            // that.show_member_cc(index)
                    }
                    that.people.push({
                        name: g.group_name,
                        email: '',
                        needToSign: false,
                        title: '',
                        order_set: '',
                        no: '',
                        is_group: true,
                        user_group: g.group_sign,
                        least_sign: false,
                        amount_least_sign: 1,
                        delegates_sign: false,
                        delegates_mail: '',
                        is_inspector: false
                    })

                } else {
                    that.id_group_cc = ""
                    var index = that.list_cc.indexOf(g._id)
                    that.list_cc.splice(index, 1)
                    var index_people = false
                    var search = that.people.filter(function(p, i) {
                        if (p.name == g.group_name && p.is_group == true && p.needToSign == false) {
                            index_people = i
                            return i
                        } else {
                            return false
                        }

                    })
                    if (search != false) {
                        that.people.splice(index_people, 1)
                    }

                }

              

            },
            show_member_cc: function(index) {
                var that = this
                let g = that.group_sign[index]
                   that.show_list_cc = g.group_sign
                that.show_list_cc_group =g.group_sign
            },
            search_list_cc_mail: function() {
                var that = this
                var search = that.show_list_cc_group.filter(function(m) {
                    var email = m.email.search(that.search_list_cc) != -1
                    if (email == true) {
                        var ne = m.email.search(that.search_list_cc) != -1
                     }else{
                        var ne = m.name.search(that.search_list_cc) != -1
                     }
                    
                    return ne
                   
                })
         
          
            
                that.show_list_cc = search
             
              
                    
                
            },
            clear_list_cc: function() {
                var that = this
                that.list_cc = []
            },
            set_clone_all_field: function() {
                var that = this
                var search_field = that.fields.filter(function(f, i) {
                    if (f.id == that.id_field) {
                        that.fields[i].is_clone = that.is_clone
                    }
                    return false
                })
            },
            chk_fields_clone: function(id) {
                var that = this
                var search_field = that.fields.filter(function(f, i) {
                    if (f.id == id) {
                        that.is_clone = that.fields[i].is_clone
                    }
                    return false
                })
            },
            select_delegate: function(email, name) {
                var that = this
                that.delegate_email = email
                that.delegate_name = name

            },
            save_select_delegate: function() {
                var that = this
                var n = Number(that.set_oreder_user)
                that.people[n].delegates_mail = that.delegate_email
                that.people[n].delegates_name = that.delegate_name
            },
            // getCountTotalSign:function(email){
            //     var that = this
            //         var result = that.fields.filter(f => f.email == email && f.toolName !== 'Text');
            //         return result.length   
            // },


            getCountTotalSign: function(email, toolName) {
                var that = this
                for (var i = 0; i <= that.fields.length - 1; i++) {
                    if (that.fields[i].person.email !== '') {
                        var result = that.fields.filter(f => f.email == email && f.toolName !== 'Text');
                        if (that.fields[i].person.email == '' &&
                            that.fields[i].is_no_sign == true &&
                            that.fields[i].person.user_group.length !== 0) {
                            return result.length + 1
                        } else {
                            return result.length
                        }
                    }
                    // else if(that.fields[i].person.is_group == true){
                    //     var result = that.fields.filter(f => f.email == email && f.toolName !== 'Text');
                    //     return result.length+1
                    // }

                    // if(that.fields[i].person.is_group = true){
                    //     var result2 = that.people[i].user_group.filter(f => f.email == email && f.toolName !== 'Text');
                    //     return result2.length+1
                    // }
                }
            },

            addCC: function(email, name) {
                var that = this
                var checkbox = document.getElementById("check_member_" + email)
                that.list_cc.push(email)
            },

            // add_list_cc: function (e, n) {
            //     var that = this
            //     var checkbox = document.getElementById("check_member_" + e)
            //     var index_people = false
            //     var search = that.people.filter(function (p, i) {
            //         if (p.email == e && p.needToSign == false) {
            //             index_people = i
            //             return i
            //         } else {
            //             return false
            //         }
            //     })


        } //methods
    }
}