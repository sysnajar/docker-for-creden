Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:90
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Jun 20 15:36:33 2021 GMT
            Not After : Jun 20 15:36:33 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = bla04@creden.co
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c5:95:48:89:64:21:63:db:23:44:d2:25:9c:a7:
                    25:b7:ad:6b:71:9a:92:39:82:f1:af:5d:64:c0:e0:
                    5d:70:07:3c:71:40:d0:2d:d5:62:4a:a1:d6:3b:76:
                    f0:44:53:23:55:d4:01:81:4a:e9:7b:2b:f3:1b:5b:
                    18:23:43:60:98:93:99:3c:8b:32:13:e9:6d:4b:08:
                    97:c9:1a:d0:b1:ab:80:d6:43:cb:7b:09:57:fc:88:
                    46:6a:2b:3b:58:c2:67:0b:98:c3:0d:61:08:92:ce:
                    32:5f:d1:e5:cf:a5:31:6e:c3:20:62:37:50:09:70:
                    c2:6e:cc:4f:60:b0:b8:82:a4:62:36:0f:b0:19:e2:
                    87:b8:26:0c:4a:be:42:e8:13:76:cd:2d:b2:94:d4:
                    48:8f:93:da:5b:6f:e8:2b:20:02:42:34:ad:ef:c8:
                    e7:e4:7a:e2:38:d3:9b:4f:44:0f:8c:a1:a0:ed:e5:
                    1d:86:7a:fe:15:a2:ab:c0:69:b1:4d:cd:2f:14:00:
                    a8:f5:b8:77:61:d5:6c:1e:c3:d0:75:6e:8f:7c:8d:
                    2e:cc:b8:35:66:87:17:13:c4:ff:e4:02:c6:08:65:
                    b1:8f:5b:e7:5d:52:82:e0:4b:9e:95:c5:a2:c3:7b:
                    6c:c2:96:c7:3f:5a:08:39:89:5a:94:cd:ab:1a:1c:
                    73:51
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         b7:a1:cc:9e:86:b8:d6:70:fb:f1:24:f4:6e:49:2f:6d:06:a1:
         76:ac:29:fe:ea:28:3e:8f:4c:45:54:2c:ec:94:f0:d2:ea:de:
         28:4d:ee:b4:25:18:aa:46:a4:9a:ba:7b:e7:4e:cd:c4:53:a8:
         37:84:c3:58:b7:f9:78:0d:17:a0:a5:69:65:26:4e:ce:c0:52:
         ee:00:22:5d:77:a8:68:1c:2d:40:6f:60:f4:94:35:6e:67:4d:
         46:7f:d8:0a:aa:b0:7c:8b:f8:7c:73:f5:ce:a2:00:56:a8:1a:
         ce:7e:16:43:9e:5b:c9:42:b4:c2:e9:3a:e5:09:3b:d2:50:53:
         d6:21:d2:73:51:6b:be:4d:af:66:49:db:a1:41:68:c2:a1:0f:
         83:4c:04:28:3b:4a:9b:a1:53:6e:04:c0:b5:27:02:f4:c8:6d:
         ab:16:00:10:cb:d9:55:64:42:96:22:15:4b:ba:f7:01:f2:e1:
         6c:63:ff:e4:9a:12:91:c4:36:9d:4e:86:44:49:a7:c7:ad:16:
         7a:a8:75:fc:bb:be:e8:9a:9f:2e:83:42:43:c9:cf:79:47:3d:
         a1:28:bf:42:8f:8a:df:a5:25:dc:d9:88:93:88:a2:05:64:72:
         0f:6e:3a:32:5e:b1:b7:99:dd:92:c7:00:68:78:d1:c0:9d:b5:
         5b:61:12:e2
