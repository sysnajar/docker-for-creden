Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:3a
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Feb 15 17:37:18 2021 GMT
            Not After : Feb 15 17:37:18 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = jornket@gmail.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:dd:98:f2:a2:0b:a4:fe:f0:3e:ef:ae:89:b1:4a:
                    11:e0:0d:37:76:91:6a:ee:ac:2e:96:12:be:2f:f8:
                    11:7f:1f:bc:25:c5:49:49:7f:c1:83:b1:de:d3:f1:
                    55:46:0a:6a:fa:16:4c:8d:e9:83:ef:d1:17:09:e4:
                    3f:2c:44:af:a4:76:97:11:04:48:f6:83:54:6b:a6:
                    43:e6:5c:72:1e:21:ed:ee:11:7b:d0:4f:95:fd:4f:
                    ce:70:e2:62:f2:d8:43:03:e6:16:3f:53:18:b1:6b:
                    a5:18:b0:b4:70:a8:db:af:e0:ad:8e:dd:5d:b2:b3:
                    64:1d:b7:9e:2c:30:15:ec:41:9d:60:82:34:4b:42:
                    a0:b8:ef:6a:e3:ff:d2:df:03:59:a4:3c:e7:87:fc:
                    ce:01:24:6c:13:62:49:9f:08:1b:b9:3e:c3:34:f3:
                    56:17:fe:ef:23:e4:cb:9e:a0:33:8b:26:30:62:05:
                    ba:e6:20:29:f1:f7:b7:54:b9:93:7b:04:3d:1d:d2:
                    97:73:25:87:70:73:26:4d:5e:39:6a:c5:a2:46:9d:
                    02:e2:59:5a:a8:1b:68:d1:22:03:03:61:a6:63:3d:
                    db:86:be:e4:79:ce:97:69:df:a4:8c:b6:c4:0d:6c:
                    eb:d7:9f:6f:42:5a:ed:43:b3:c2:f2:1b:da:0e:89:
                    0d:47
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         32:ab:86:84:f9:a7:70:7c:b4:6d:fc:f9:f9:e1:88:51:7b:bc:
         c7:15:9d:e6:3b:53:f6:c1:f5:14:6f:8e:90:ec:bd:8b:4d:6f:
         bb:a7:8d:04:3f:c2:75:d5:ae:89:e7:33:e6:5c:08:5c:63:b4:
         e1:ba:6e:96:2a:9c:8b:c5:f0:9d:82:38:c3:0b:b0:ce:f9:35:
         01:9e:70:d8:16:98:68:5c:e4:ea:19:ae:e4:6c:e5:cf:be:77:
         40:ca:04:8e:53:2f:ec:f4:b1:c6:4c:a0:6d:33:da:aa:e4:32:
         cb:0a:e8:a8:0d:2b:d6:b7:01:3a:77:35:0f:16:e5:de:fb:1d:
         4e:25:32:8d:2b:93:10:4b:8b:fe:49:66:f0:51:fd:63:70:20:
         29:52:90:2e:bb:33:48:2a:38:b6:7b:35:f6:99:85:e7:08:3f:
         a1:08:8a:de:08:d1:2c:a6:b2:c7:f0:43:c2:26:f2:c9:b1:c0:
         63:62:4d:f0:bc:72:91:5a:c3:21:ff:f2:36:c7:2f:c8:e8:1c:
         40:c8:84:2b:5d:e0:62:47:ad:e6:ca:c7:33:b5:7c:e9:9f:51:
         ee:7c:c9:56:1d:4d:5a:96:e4:35:24:4b:ca:b1:b6:73:a8:4e:
         63:ec:2d:94:e6:44:d7:02:19:fb:54:a0:e9:7f:10:2b:1c:2d:
         4e:1a:97:e7
