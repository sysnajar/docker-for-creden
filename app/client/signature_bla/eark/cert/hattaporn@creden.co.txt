Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:46
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Mar 15 06:21:46 2021 GMT
            Not After : Mar 15 06:21:46 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = hattaporn@creden.co
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:bd:c0:31:d7:2b:86:5e:a7:84:9f:fe:38:f1:9c:
                    e8:89:e2:20:9d:d9:cd:f4:6e:1d:57:94:4c:41:97:
                    32:95:7a:25:4c:eb:32:34:bd:1a:d0:04:ee:2d:00:
                    ba:82:e6:bb:95:30:59:67:e9:d9:5e:f4:c0:17:c4:
                    31:9b:39:49:c8:75:82:43:87:44:7f:7d:bd:1f:d9:
                    12:49:a9:7c:56:13:88:d7:08:1e:d0:9c:8d:45:e6:
                    dd:8d:91:12:ea:85:15:51:93:ff:8f:b8:f1:0d:a0:
                    33:3d:55:84:d2:35:5e:ec:d2:82:2b:4b:27:3b:bb:
                    81:bc:ec:fd:20:01:bb:51:cb:ce:52:9e:9e:e8:d0:
                    c8:6a:ac:75:63:b8:84:84:d5:64:5e:07:2d:62:8b:
                    4b:6e:d0:cc:8b:57:1d:33:7b:a0:11:63:c8:b5:e9:
                    9e:33:f7:02:f1:57:7a:c0:17:f8:b7:c6:d6:69:08:
                    03:4b:29:d0:07:8f:e6:d1:39:74:a4:2c:33:7d:d6:
                    69:fe:2c:d9:8f:4b:a6:9f:0d:d0:1a:c6:e3:4e:f9:
                    c5:84:1d:3e:da:05:f6:13:fd:df:56:78:34:30:88:
                    45:ee:f4:79:e3:4a:fe:9d:e1:c7:10:29:2a:55:23:
                    52:62:28:27:e2:20:21:f5:42:92:bc:e1:0c:75:77:
                    fc:6b
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         b9:10:be:3c:c5:70:d2:f7:a9:b5:db:4f:48:b8:67:d9:3e:dd:
         f1:40:50:7f:21:56:e9:b5:a2:ec:fc:32:5a:6e:78:02:29:d8:
         c5:c6:e9:74:2b:65:0f:90:15:16:99:4a:67:21:47:3f:5a:aa:
         5d:56:eb:23:ad:42:d2:1e:d6:6e:0e:97:96:bf:2f:97:8e:d0:
         a4:ac:2f:da:a8:c7:fa:12:d6:06:dc:97:91:de:24:7b:c5:e8:
         df:6e:5b:c2:75:17:00:ec:c2:f6:b2:3b:47:f6:ec:83:82:fe:
         7a:db:a4:62:29:56:ce:3b:c9:1a:bb:98:f1:b2:9e:85:38:3a:
         a7:89:5c:ac:d5:f4:80:62:c3:e2:64:ea:4e:f0:1e:de:3d:2e:
         5c:4c:73:dc:1d:81:16:b5:af:e7:9f:fe:db:6a:b7:42:26:8a:
         b2:7a:2b:65:0a:01:b8:37:e6:6b:48:b9:54:5e:27:d3:d0:6d:
         3e:98:02:8d:2d:15:e0:1a:60:d9:b4:08:4a:ad:2b:10:b8:b2:
         41:9f:cf:4b:0e:b7:08:fb:2e:68:91:d9:db:88:3d:35:bb:14:
         37:f9:66:17:a6:4a:75:36:fc:9c:f1:4c:ca:45:93:fe:ae:98:
         4c:3d:52:1d:b8:58:cb:bf:54:78:b1:ce:a3:1f:31:40:69:d3:
         c9:8e:b9:ea
