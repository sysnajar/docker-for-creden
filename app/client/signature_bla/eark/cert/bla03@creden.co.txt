Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:8e
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Jun 20 08:00:09 2021 GMT
            Not After : Jun 20 08:00:09 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = bla03@creden.co
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:d6:c6:ed:44:25:aa:9e:c4:10:43:6a:26:ec:6a:
                    f4:24:dd:01:97:7b:6d:c2:4e:97:43:29:16:14:a4:
                    fe:1a:6e:52:09:0f:8a:5d:8b:b4:3f:58:41:81:e0:
                    b0:77:68:c6:5e:6e:18:d0:de:78:a9:e1:ee:37:91:
                    c4:a0:ed:a7:15:0d:0e:81:67:c2:10:15:48:de:c7:
                    f7:82:0a:0a:16:94:83:fa:5d:45:8a:d2:28:ad:db:
                    0e:0b:f8:01:5a:65:ca:ad:ae:8b:68:4b:ef:2f:0c:
                    99:13:b1:e8:eb:a0:6a:dc:9a:57:44:4d:a0:97:21:
                    23:29:dc:47:b7:55:e0:cd:6c:e0:83:b9:49:00:93:
                    7a:63:e4:18:5d:18:c7:06:41:25:f6:86:02:6a:fc:
                    e8:87:64:7b:2b:49:27:f5:b1:0d:0e:dc:5c:60:d9:
                    58:a1:84:f5:ab:eb:36:bb:3a:33:b1:88:9a:14:0f:
                    89:14:2e:5b:72:4c:5f:7c:5c:e3:b3:90:28:bd:b6:
                    95:f2:64:0a:34:37:af:70:0d:65:9d:ac:2e:df:4d:
                    16:db:48:72:be:0c:ba:24:5e:2a:be:75:5f:22:71:
                    b3:50:f3:19:bf:32:d2:86:dc:8b:a1:e8:d8:a8:6f:
                    74:54:be:43:d8:da:a5:62:8e:38:a1:fc:88:ff:4b:
                    43:bb
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         2a:e9:03:f6:09:b8:20:eb:89:9d:ae:78:17:26:c1:ec:0b:6b:
         41:cc:0d:c4:7d:2b:11:77:4d:42:ed:62:99:e1:d3:12:a6:fb:
         bd:bf:92:84:a3:45:7c:2d:c0:4b:b5:7d:71:e3:bf:8e:89:44:
         74:2f:4c:71:d3:5a:f8:f1:aa:9e:be:ed:47:b1:01:74:10:bb:
         7c:35:75:01:2e:bd:0d:5a:3a:64:47:d8:8b:8b:7a:40:d1:3c:
         b4:23:f0:e5:cb:4d:d5:86:f6:e4:e8:49:d5:c9:0a:0b:9c:75:
         02:44:bc:f8:d5:d5:b4:ab:d3:a9:9f:4a:3f:5f:51:74:f1:68:
         c2:7c:50:33:3b:20:fd:ef:c3:4f:16:1e:65:be:d2:d8:2c:56:
         bb:ec:0a:db:bb:d1:f0:61:90:4e:30:e1:50:b6:a0:8b:03:9a:
         98:3a:5e:e1:8e:84:5a:fc:06:f8:61:9b:ee:e2:98:6a:b5:1d:
         c2:d9:17:33:a2:1a:0d:7d:89:37:2a:6b:2b:66:5b:40:93:1a:
         69:f7:76:1a:b4:d0:f8:17:4e:0d:5c:d8:5a:df:ec:39:b6:36:
         af:6b:3e:b2:df:cc:73:4b:63:96:40:12:70:43:01:13:9e:6d:
         a8:12:66:5e:46:b8:6c:7d:3f:05:9a:df:2e:3e:8b:a4:d1:de:
         be:57:88:07
