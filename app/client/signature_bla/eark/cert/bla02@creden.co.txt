Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:8d
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Jun 20 07:56:57 2021 GMT
            Not After : Jun 20 07:56:57 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = bla02@creden.co
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:dd:38:e3:0a:fe:90:63:7a:f6:dd:4f:93:4e:c5:
                    44:1c:a2:52:10:4e:f4:d1:e3:4e:a0:cd:80:ce:43:
                    43:b6:12:ec:ba:20:cd:69:b6:1f:1a:2f:31:26:2f:
                    63:5a:7e:70:10:6e:71:90:f3:83:22:b3:bf:8b:29:
                    81:32:af:e9:df:21:33:37:65:1a:cd:1f:af:ee:bc:
                    28:2e:3c:49:d5:58:f2:0d:c6:5f:1f:57:7e:c2:2d:
                    b1:66:40:9b:2d:39:36:58:7e:c2:bf:af:b3:5e:be:
                    e3:21:e8:ff:36:2f:58:0e:c7:b1:52:b6:b2:6b:66:
                    ff:9c:84:15:24:78:92:c9:62:26:b6:23:61:3a:e6:
                    07:9b:11:f4:bc:f8:57:8b:c7:f1:e7:e3:b9:40:5d:
                    03:33:ca:ba:94:c3:bf:5c:88:9e:3b:78:b5:c8:92:
                    e2:d1:12:6c:65:44:21:d3:d3:fc:d9:e8:49:36:a2:
                    a9:3e:6a:24:b3:2e:6c:23:78:90:1e:4d:00:cd:12:
                    8d:ac:db:80:5c:8b:bc:d6:79:f9:7e:58:72:17:8d:
                    a3:1f:32:7d:e2:61:45:1e:ba:1e:f4:b5:b7:17:19:
                    4c:a7:6d:21:52:68:cf:c9:10:d5:45:a1:59:61:fe:
                    41:53:4e:0d:37:d5:45:e0:6a:e9:46:b8:b6:e3:3c:
                    4a:1f
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         47:78:b8:12:1f:9a:95:b1:91:a3:8a:bf:2d:37:5b:17:56:10:
         00:b2:39:c3:a7:e8:15:7f:18:dd:27:56:ac:de:b3:ff:9f:a7:
         31:f5:a9:0b:83:64:6b:4c:2c:dd:02:4f:84:4a:77:1f:16:20:
         2c:31:57:59:28:23:a1:d7:84:8a:52:76:87:31:8e:20:51:21:
         80:83:55:ae:c3:d8:23:10:2c:88:a4:70:b0:da:e5:3f:20:d5:
         1b:14:81:5f:fd:ea:9e:43:dd:01:8b:cd:be:d1:6f:bb:69:62:
         a6:f4:02:eb:26:22:83:ec:3b:f7:16:3a:7d:05:cd:20:d7:7c:
         e5:39:83:a7:47:99:b4:ac:af:17:96:82:e2:60:e3:8f:e5:87:
         92:b9:80:c6:9a:6a:50:68:3a:3f:57:ea:50:47:bb:0a:29:cf:
         29:8b:98:64:18:3f:99:25:4a:b9:00:4e:7c:2a:44:cc:a3:cb:
         c2:89:4c:97:5f:60:bb:4c:39:ab:3c:b5:8f:60:7d:2a:c5:16:
         aa:87:31:12:9d:e6:63:9f:4d:95:d5:08:6d:95:5c:93:71:2b:
         6d:6a:3d:42:e6:b1:12:70:cb:3b:7e:52:d8:85:af:74:e5:40:
         f9:4f:7c:b4:b4:30:ee:8b:42:f3:d4:c2:37:b2:cd:16:1b:5d:
         2d:aa:1f:7e
