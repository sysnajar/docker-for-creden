Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:a4
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Sep  3 08:42:32 2021 GMT
            Not After : Sep  3 08:42:32 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = numjai.son
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:c4:27:e7:69:76:5f:43:32:aa:81:6f:4b:57:0d:
                    53:f8:0c:4d:ff:db:6e:0f:b9:d5:8b:04:b6:c7:6f:
                    cb:52:be:3f:ef:70:c7:2a:7a:73:a9:5d:37:90:06:
                    7d:6a:63:ba:20:3c:54:74:ed:d9:04:ea:7f:63:35:
                    03:5c:91:ae:e6:02:71:5d:7f:d3:50:25:0e:15:d3:
                    ee:dd:7a:01:2f:41:cc:47:df:74:46:32:9e:43:91:
                    bc:9b:79:a1:c3:5b:c6:09:09:c0:c3:f1:44:b2:22:
                    1f:d0:88:96:04:72:40:49:99:ac:e2:a3:d2:5e:ab:
                    59:1d:9f:80:63:ed:56:79:31:3a:66:39:26:d3:40:
                    5d:2c:c5:7e:ca:d2:3c:96:a0:61:cc:63:3b:ac:f5:
                    c5:b7:05:39:33:bf:8c:3e:84:88:74:36:ae:c9:a9:
                    fc:24:bb:54:41:82:d8:03:38:78:6c:bf:4b:e0:80:
                    cc:52:bc:fe:f5:9c:7c:22:c9:bd:12:e9:77:3c:10:
                    ee:4d:f1:d5:da:8b:e3:3d:b0:d2:61:8e:21:ca:b4:
                    43:6c:c0:0e:ad:31:57:97:d5:47:5b:9c:85:3c:81:
                    9a:28:a1:8f:47:90:73:8e:ac:5e:8d:06:f2:94:56:
                    00:08:da:2e:61:d0:3e:1a:10:a7:9d:82:42:a0:56:
                    8c:37
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         ba:16:b1:1d:24:a7:c2:b8:79:7b:17:fb:c0:e5:34:f4:96:bd:
         2d:b0:8e:73:ba:da:d5:c7:6f:4f:d2:86:2b:47:42:0c:60:a2:
         22:2c:e7:e8:3d:52:1c:59:08:cc:7c:71:62:cf:35:c5:21:0d:
         6d:5d:fa:5a:34:55:a4:fc:a7:ae:73:61:58:59:8b:02:d8:f1:
         35:68:0a:91:e3:2e:05:46:b7:6c:33:36:5a:de:f9:12:f8:9c:
         67:14:24:b6:5a:21:8d:68:5e:34:09:9b:28:c9:dc:cc:4b:56:
         86:2f:c6:ed:e1:85:4c:63:c0:4a:21:02:4c:db:bd:64:90:bd:
         ed:b8:cb:46:4b:c7:38:1d:17:57:0e:96:63:b7:25:a3:fa:d3:
         d6:9d:e0:c8:89:cd:f4:f5:39:39:59:fc:72:15:a0:60:4e:e7:
         98:d1:85:7c:46:9d:e0:aa:f6:a4:7b:8b:d0:ff:8e:75:a6:05:
         74:d9:e6:73:45:d8:0c:b3:55:d2:d4:09:ff:6f:7f:7f:3c:f9:
         34:2f:2b:61:df:27:85:a6:55:3d:80:6b:df:d1:de:97:bf:fb:
         e5:a8:f7:35:ac:c5:2a:8f:a3:9d:da:76:7b:65:02:00:9b:e2:
         dc:36:86:64:1d:ee:de:1f:e8:53:2c:d0:ee:46:2e:0f:a6:4f:
         bb:48:01:9b
