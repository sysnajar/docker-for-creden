 
function set_sign_type(v){
      //alert('set sing type:'+ v)
      if (app != undefined) {
          app.sign_type = v;
      //    app.sign();
      }
      chooseSignature(v);
     // if(v=='draw' || v=='gen'){
     //   sign_profile(v)  
      //}

      ////console.log("set_sign_type type is " + v)
	  //$('#btn_save_sig_type').show()
  }

  function createSignature(){
    if(window.sign != null) field_id = window.sign.id
    else field_id = ''
    if (app != undefined) {
        app.sign_type = 'draw';
    }
    var data = signaturePad.toDataURL('image/png');
    data = data.split(',')
    $.ajax({
      type: "POST",
      url: "../capi/create_signature",
      data: JSON.stringify({"img":data[1],"username":getUsername(),"code":getUsername(),"sign_type":"draw", "field_id":field_id}),
      dataType: "json",
      success: function(data) {
        if (app != undefined) {app.sign();}
        checkSignature();
        sign_profile('draw');
        app.showSignatureTab(0)
        //document.getElementById('type_radio1').checked=true
      },
      error: function(request, status, error){
        alert("|"+ status + ' |' +error);
      }
    });
  }

  function chooseSignature(type){
    if(window.sign != null) field_id = window.sign.id
    else field_id = ''
    $.ajax({
      type: "POST",
      url: "../capi/choose_signature",
      data: JSON.stringify({"username":getUsername(),"code":getUsername(),"sign_type":type, "field_id":field_id}),
      dataType: "json",
      success: function(data) {
        
        if (app != undefined) { app.sign(); }
        
        if (type == 'draw' || type == 'sign')
           {
            checkSignature();
            sign_profile(type);    
           }
         else
           {
             //console.log("Skipped checkSignature and sign_profile#"+type)
           }  
        
      },
      error: function(request, status, error){
        alert("|"+ status + ' |' +error);
      }
    });
  }

  function clearSignature() {
    signaturePad.clear();
  }

  function isRTL(){
    s = getName()
    var ltrChars = /[^a-z\s]/i;
    return ltrChars.test(s);
  }

  function genSignature() {

    var name = $('#sig-fullname').val()
    if(!name && name.length<1){alert('Please enter your name');return;}

    if(window.sign != null) field_id = window.sign.id
    else field_id = ''
    if (app != undefined) {
        app.sign_type = 'gen';
    }
    $('#signature-img').show();
    lang = 'th'
    url = "../dev_capi/gen_signature_th"
    if(true){
      lang = 'en'
      url = "../capi/gen_signature"
    }
    data = {"name": name  ,"code":getUsername(),"lang":lang, "font_name":$('#signature_font').val(), "field_id":field_id}
    //console.log(data)
    $.ajax({
      type: "POST",
      url: url,
      data: JSON.stringify(data),
      dataType: "json",
      success: function(data) {
        var path = '../edoc/app-assets/images/signature/'
        $("#signature-img").attr("src",path+getUsername()+'_gen.png?rd='+ new Date().getTime());
        if(lang == 'th'){
          wgetSignature();
        }else{
          checkSignature();
        }
        //if (app != undefined) {app.sign();}
        sign_profile('gen_white');
        app.showSignatureTab(0)
        //document.getElementById('type_radio2').checked=true

      },
      error: function(request, status, error){
        alert("|"+ status + ' |' +error);
      }
    });
  }

  function wgetSignature() {
    $.ajax({
      type: "POST",
      url: "../capi/wget_signature",
      data: JSON.stringify({"code":getUsername()}),
      dataType: "json",
      success: function(data) {
        checkSignature();
      },
      error: function(request, status, error){
        alert("|"+ status + ' |' +error);
      }
    });
  }

  function checkSignature() {
     // havecount = 0;
    a = getUsername()
   $(".group-draw_img").hide();
   $(".group-gen_img").hide();
    draw_src = "../edoc/app-assets/images/signature/"+a+"_draw.png"
    gen_src = "../edoc/app-assets/images/signature/"+a+"_gen.png"

    imageExists(draw_src, document.getElementById("draw_img"), 'draw_img')
    imageExists(gen_src, document.getElementById("gen_img"), 'gen_img')
  }

   function changeTab()
  {
     $('#home-tab').click();
  }

  function imageExists(url, img, c) {
    img.src = url+'?eark='+  new Date().getTime();
    img.onload = function() {
     //if(havecount < 2){
    // havecount++
     changeTab()
        //return havecount = 2
        //havecount = 2
        $(".group-"+c).show();  
        // img.onerror = function() {
        //   $(".group-"+c).hide(); };
        //$("."+c).attr('src',url+'?eark='+  new Date().getTime())
        //alert(url+'?eark='+  new Date().getTime());
    //}
  }
}
