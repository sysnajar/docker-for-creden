var org_chart = {default_img : "https://hjkl.ninja/company/img/Poster_creden-01.jpg", default_img2 : "https://balkangraph.com/js/img/2.jpg", 
// var org_chart = {default_img : "https://esign.ldd.go.th/company/img/profile.png", default_img2 : "https://balkangraph.com/js/img/2.jpg", 
chart:null,edit_nodeId : null,all_titles:[], all_nodes:[], nodes_map:{} ,config : {},
hide_hidden_fields:true, tmp_skip_name_changed : false}

org_chart.lang_en = {create_new_title : "Create New Title?"}
org_chart.lang_th = {create_new_title : "Create New Title (TH)?"}
org_chart.lang    = org_chart['lang_'+localStorage.lang_choose] || org_chart.lang_en

org_chart.init = function(_cfg){ 
   $.extend(this.config, _cfg)  
   this.init_titles()
 }

org_chart.gup = function ( name, url ) {
         if (!url) url = location.href;
         name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
         var regexS = "[\\?&]"+name+"=([^&#]*)";
         var regex = new RegExp( regexS );
         var results = regex.exec( url );
         return results == null ? null : results[1];
}

org_chart.init_titles = function(){
	//this.all_titles = [{label:"CEO", value: "1"}, {label:"CTO", value: "2"} , {label:"Developer", value: "3"}]
     var that = this
     $.ajax({
                type: "POST",
                url: api_host + "get_company_title",
                data : "company_id="+this.config.company_id,
                dataType: "json",
                success: function(data) {
                  // console.log(data);

                    if(data.success)
                      { 
                         that.all_titles = data.data
                         that.init_persons()
                      }
                     else
                      {
                        alert("Unnable to load titles"+data.error_msg);   
                      } 
                    
                },
                error: function(request, status, error){
                    alert("Unnable to load HTTP error");  
                   
                }
                });

}

org_chart.init_persons = function(){
	//this.all_persons = [{label:"Eark", value: "x1"}, {label:"Nut", value: "x2"} , {label:"Kit", value: "x3"}]

   var that = this
     $.ajax({
                type: "POST",
                url: api_host + "get_company_person",
                data : "company_id="+this.config.company_id,
                dataType: "json",
                success: function(data) {
                  // console.log(data);

                    if(data.success)
                      { 
                         that.all_persons = data.data
                         that.init_nodes()
                      }
                     else
                      {
                        alert("Unnable to load persons"+data.error_msg);   
                      } 
                    
                },
                error: function(request, status, error){
                    alert("Unnable to load HTTP error");  
                   
                }
                });


}
org_chart.sort_keys = function(arr){
 var ret = []
 for(var i=0;i<arr.length;i++)
   {
     var o  = arr[i]
     var o2 = {id: o.id, name: o.name , title: o.title, email : o.email,  img: o.img, title_id: o.title_id, person_id: o.person_id}
     if(o.pid)o2.pid = o.pid
     //arr[i] = o2
     ret.push(o2)
   } 
   return ret

},
org_chart.init_nodes = function(){

	var demo_nodes = [
    { id: this.config.company_id, name: "" , title: "", email : '',  img:"", title_id:'', person_id:'' }
]

    var that = this
     $.ajax({
                type: "GET",
                url: api_host + "get_org_chart",
                data : "company_id="+this.config.company_id,
                dataType: "json",
                success: function(data) {
                  // console.log(data);

                    if(data.success)
                      { 
                         
                         if(!data.is_empty)
                             that.all_nodes = org_chart.sort_keys(data.nodes)
                          else
                             that.all_nodes = demo_nodes;
                          
                         for(var i=0;i<that.all_nodes.length;i++){
                            var node = that.all_nodes[i]
                            that.nodes_map[node.id] = node
                            var arr  = that.all_persons.filter(function(p){return p.value==node.person_id})
                            if(arr.length>0) node.img = arr[0].img                            
                            else node.img = org_chart.default_img
                            
                            var root = that.all_nodes.filter(function(r){return r.pid == node.id })
                            if(root.length > 0) node.tags = ["Root"]
                            else node.tags = ["Child"]
                          }
                         that.init_org_chart()
                      }
                     else
                      {
                        alert("Unnable to load persons"+data.error_msg);   
                      } 
                    
                },
                error: function(request, status, error){
                    alert("Unnable to load HTTP error");  
                   
                }
                });

}

org_chart.setFields = function(obj){
  console.log('edit_nodeId = '+this.edit_nodeId)
  var node = this.nodes_map[this.edit_nodeId]
  $.extend(node, obj);
  console.log("setFields: "+ JSON.stringify(node))
}

org_chart.getEditForm = function(){
  return $('#bgEditForm')
}
org_chart.getInput = function(label){
  var f = this.getEditForm()
  return f.find('[label="'+ label +'"]').next()
}

org_chart.addNewtitle = function(title_txt){
  //alert('addNewtitle >> ' + title_txt)
  var that = this
  var node = this.nodes_map[this.edit_nodeId] || {is_dummy:true}
 // alert('Editing node (should have many fields) :\n '+JSON.stringify(node)); //return;
  var title_obj = {label:title_txt, value:"", type:"title"}
 
  //---------------------------
    var data = {company_id: this.config.company_id, title : title_txt}
    var json = JSON.stringify(data)
   
    $.ajax({
                type: "POST",
                url: api_host + "add_company_title",
                data : json,
                dataType: "json",
                success: function(data) {
                  console.log(data);

                    if(data.success)
                      { 
                        title_obj = data.data
                        that.setFields({title_id:title_obj.value, title:title_obj.label})
                        that.all_titles.push(title_obj)
						node.title_id = title_obj.value // <--
						//alert('node is now:\n'+ JSON.stringify(node))
                      }
                     else
                      {
                        alert("Unnable to save "+data.error_msg);  
                        that.getInput('title').val('')  
                      } 
                    
                },
                error: function(request, status, error){
                    alert("Unnable to save HTTP error");  
                    that.getInput('title').val('') 
                }
                }); 
   
}

org_chart.edit_chart = function (nodeId, x)
{
 this.edit_nodeId = nodeId
  
 console.log('edit_nodeId = '+ this.edit_nodeId)
 
var fn = function(){
		var formDiv = org_chart.getInput('img').parent().parent()


    //hide fields except for "name" and "title"
    formDiv.children('div').each(function(index, ele){
      var jq = $(ele) 
      
      if(org_chart.hide_hidden_fields && jq.find('[label="name"],[label="title"],[label="email"]').length<1) 
        jq.hide() 
        
    })
     
      var email = org_chart.getInput('email').attr('readonly','true');
      email.prev().html( "email (readonly)" )  


    var f = org_chart.getEditForm();
		var title = org_chart.getInput('title');
    title.prev().html( "ตำแหน่ง" )		 
       
		//setup suggestion box
		var name = org_chart.getInput('name');
    name.prev().html( "ชื่อ" )
    //  name.prev().html( app.lang_bar.title][app.lang_bar.lang_choose] )

		name.autocomplete({
	      source: org_chart.all_persons,
          create:function(){

           $(this).data('ui-autocomplete')._renderItem  = function (ul, item) {
            //var str = item.label.substring(item.label.indexOf('.')+1)
            var str =  item.name +' ('+item.email+')'
            //var str =  item.label
              return $("<li>")
                .append(str)
                .appendTo(ul);
            };
          },

	      select: function( event, ui ) {
			var person_name = ui.item.name
			var person_id   = ui.item.value  
			  	
			var node = org_chart.nodes_map[org_chart.edit_nodeId] 
			node.person_id = person_id  

			var persons = org_chart.all_persons.filter(function(t){
				return (t.value == person_id)
			})
			
			if(persons.length>0){
		       org_chart.getInput('img').val(persons[0].img);//update hidden field : img
           org_chart.getInput('email').val(persons[0].email);//update email readonly
			}else{
			  alert('Unnable to find person : '+person_id)
			}
			org_chart.getInput('person_id').val(person_id);//update hidden field : person_id
	      	
			org_chart.setFields({person_id:person_id, name: person_name})
      org_chart.tmp_skip_name_changed = true
			setTimeout( function(){name.val(person_name)}, 50)
		  } 
	     
	    });

	    name.on('change', function(){
      if(org_chart.tmp_skip_name_changed){
        org_chart.tmp_skip_name_changed = false
         //alert('tmp_skip_name_changed')
         return
       }  

			//filter entered value with preload list
			var enteredName = name.val()

      //alert('change enteredName = ' + enteredName )

			var arr = org_chart.all_persons.filter(function(t){
				return (t.name == enteredName)
			})

			if(arr.length==0)
			  {alert('Please select person from the list');
				  name.val('')
		    	org_chart.getInput('person_id').val('');
          org_chart.getInput('img').val(''); 
          org_chart.getInput('email').val('')
			  }
			else
			  {
			  	var node = org_chart.nodes_map[org_chart.edit_nodeId] || {}
				  
				//check to see if we already picked from suggestion box
				var arr2 = arr.filter(function(t){ return t.value == node.person_id  })
				if(arr2.length>0)
				  {
             //alert('use suggestion box')
				  }
				 else
				  {
				      //alert('use auto selected '+ arr[0].email)
			  	    var person = {person_id:arr[0].value, name:arr[0].name, img : arr[0].img}
		    		  org_chart.getInput('person_id').val(arr[0].value); //update hidden field : person_id
              org_chart.getInput('img').val(arr[0].img) 
              org_chart.getInput('email').val(arr[0].email) 
					    org_chart.setFields(person)
				  }



			  }

		}),


		
		title.on('change', function(){
			var arr = org_chart.all_titles.filter(function(t){
				return (t.label==title.val())
			})

			if(arr.length==0 && confirm(org_chart.lang.create_new_title))
			 	org_chart.addNewtitle(title.val()) 
			 //else
			 	//title.val('')

		}),

		title.autocomplete({
	      source: org_chart.all_titles,
	      select: function( event, ui ) {
		    org_chart.getInput('title_id').val(ui.item.value);//update hidden field
	      	org_chart.setFields({title_id:ui.item.value, title:ui.item.label})
			setTimeout( function(){title.val(ui.item.label)}, 50)
		  } 
	     
	    });

		 

	}

 this.chart.editUI.show(nodeId);
 setTimeout(fn,100)	
}

org_chart.save = function(update)
{

	var data = {company_id: this.config.company_id, nodes : this.all_nodes}
    var is_update = false
    var is_valid_update = false

    if(update.updated_node)
	  { data.updated_node = update.updated_node
		is_update = true
		var n = update.updated_node
		if(n.id && n.title_id && n.person_id) { is_valid_update = true }
	  }

    if(update.removed_node_id)
	  { 
		is_update = is_valid_update = true
	    data.removed_node_id = update.removed_node_id 
	  }

	   if(is_update){
		   if(!is_valid_update){
          alert('Invalid update'); 
	        // alert(JSON.stringify(data.updated_node))
			   return;
		   }
	   }

    var json = JSON.stringify(data)

    $.ajax({
                type: "POST",
                url: api_host + "save_org_chart",
                data : json,
                dataType: "json",
                success: function(data) {
                  console.log(data);

                    if(data.success)
                      { 
                         
                      }
                     else
                      {
                        alert("Unnable to save "+error_msg);	 
                      } 
                    
                },
                error: function(request, status, error){
                    alert("Unnable to save HTTP error");	
                   
                }
                });
}


org_chart.init_org_chart = function() {
//alert(_cfg.company_id)	



var addIcon = '<svg width="24" height="24" viewBox="0 0 922 922"><path fill="#7A7A7A" d="M922,453V81c0-11.046-8.954-20-20-20H410c-11.045,0-20,8.954-20,20v149h318c24.812,0,45,20.187,45,45v198h149 C913.046,473.001,922,464.046,922,453z"></path><path fill="#7A7A7A" d="M557,667.001h151c11.046,0,20-8.954,20-20v-174v-198c0-11.046-8.954-20-20-20H390H216c-11.045,0-20,8.954-20,20v149h194 h122c24.812,0,45,20.187,45,45v4V667.001z"></path><path fill="#7A7A7A" d="M0,469v372c0,11.046,8.955,20,20,20h492c11.046,0,20-8.954,20-20V692v-12.501V667V473v-4c0-11.046-8.954-20-20-20H390H196 h-12.5H171H20C8.955,449,0,457.955,0,469z"></path></svg>'
var editIcon = '<svg width="24" height="24" viewBox="0 0 528.899 528.899"><path fill="#7A7A7A" d="M328.883,89.125l107.59,107.589l-272.34,272.34L56.604,361.465L328.883,89.125z M518.113,63.177l-47.981-47.981 c-18.543-18.543-48.653-18.543-67.259,0l-45.961,45.961l107.59,107.59l53.611-53.611 C532.495,100.753,532.495,77.559,518.113,63.177z M0.3,512.69c-1.958,8.812,5.998,16.708,14.811,14.565l119.891-29.069 L27.473,390.597L0.3,512.69z"></path></svg>'
org_chart.chart = new OrgChart(document.getElementById("orgchart"), {
//showYScroll: BALKANGraph.scroll.visible,
enableDragDrop: true,
enableSearch: false,
nodeMouseClickBehaviour: BALKANGraph.action.none,
nodeBinding: {
    field_0: "name",
    field_1: "title",
    field_2: "email",
    img_0: "img"
},

onAdd: function (sender, node) {
	//alert('onAdd')
    //alert('onAdd:\n\n'+JSON.stringify(node))
    //org_chart.all_nodes.push(node)
    org_chart.nodes_map[node.id] = node

    node.img = org_chart.default_img
    org_chart.save({updated_node : node})
    return true 
},
onUpdate: function (sender, oldNode, newNode) {
    var newNode2 = org_chart.nodes_map[newNode.id]
	//alert('onUpdate:\n\n'+JSON.stringify(oldNode)+'\n\n'+JSON.stringify(newNode)+'\n\n'+ JSON.stringify(newNode2))
    setTimeout(function(){org_chart.save({updated_node : newNode2})}, 100)
    
    return true      
},
onRemove: function (sender, nodeId) {
     var ok = confirm("Delete this node")
     for(var i=0;i<org_chart.all_nodes.length;i++)
       {
           var node = org_chart.all_nodes[i]
           if(node.id==nodeId)
             { //node.is_deleted = true 
			   org_chart.all_nodes.splice(i,1)
			   break;
			 }

       }

	 if(ok){
       org_chart.save({removed_node_id : nodeId})
	 }

    return ok   
},

nodes: org_chart.all_nodes,
nodeMenu: {     add: { text: "Add" },
                edit: { 
                	icon: editIcon	,
                    text: "Edit",
                    onClick: function(node_id){org_chart.edit_chart(node_id)}
                },
                remove: { text: "Remove" }
            }
});
$('#orgchart').children('a').hide();
}
