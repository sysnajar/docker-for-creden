$(document).ready(function () {
    app = init_vue()
});
function swithLang(lang) {
    localStorage.setItem('lang_choose', lang);
    window.myApp.lang_choose = lang
      
}
function init_vue() {
    return new Vue({
        el: '#app',
        data:{
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            myProfile: '',
            myFirstname: getUserField("adminFisrtname") || "",
            myLastname: getUserField("adminLastname") || "",
            myEmail: getUserField("adminEmail") || "",
            lang_txt: lang_summary,
            booking_id : gup('id') || '',
            phone_number:getUserField("comTel") || "",
            booking : {},
            text_time:'',
        },
        mounted: function () {
            window.myApp = this
            this.getProfile()
            this.myProfile = this.getProfile()
            this.get_booking_detail()
        },
        methods:{
            getProfile: function () {
                var p = getUserField("profile_pic") || ""
                var f = p.search("http")
                var img = ""
    
                if (f == 0) img = p
                else if (f == -1 && p != "") img = "../face/images/card/" + getUserField("profile_pic") + ".png"
                else img = "images/profile.png"
                return img
            },
            get_booking_detail:function(){
                var that = this
                var data = {booking_id: that.booking_id, email:that.myEmail}
                $.ajax({
                    type: "POST",
                    url: document.location.origin + "/capi/get_booking_detail",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                       if( res.success){
                            that.booking = res.data
                            if( parseInt(that.booking.booking_time) > 12 ){
                                that.text_time = "PM"
                            }else{
                                that.text_time  = "AM"
                            }
                        }
                        
                        
                    },
                    error: function (request, status, error) {
                        console.log("|" + status + ' |' + error);
                    }
                });

            },
            
        }
    })
    
}
var lang_summary ={
    "Reservations": { "th": "การจองเพื่อสมัครใบรับรองอิเล็กทรอนิกส์", "en": "Reservations to issue digital certificates" },
    "Booking": { "th": "เลือกวันที่ต้องการจอง", "en": "Choose Booking Date" },
    "Morning": { "th": "ช่วงเช้า", "en": "Morning" },
    "Contact": { "th": "เบอร์ติดต่อ", "en": "Contact number" },
    "Location": { "th": "สถานที่ ที่ให้ Messenger ไปดำเนินการ", "en": "Booking Location" },
    "Afternoon": { "th": "ช่วงบ่าย", "en": "Afternoon" },
    "Available": { "th": "ว่าง", "en": "Available" },
    "minute": { "th": "นาที", "en": "Minute" },
    "Summary": { "th": "สรุปข้อมูล", "en": "Summary of Booking" },
    "Booking_Name": { "th": "ผู้จอง", "en": "Booking Name" },
    "Booking_Information": { "th": "ข้อมูลการจอง", "en": "Booking Information" },
    "date": { "th": "วันที่จอง", "en": "Booking date" },
    "Period": { "th": "ช่วงเวลา", "en": "Period of Time" },
    "process": { "th": "กรุณาดำเนินการภายใน", "en": "Please process within" },
    "More_location": { "th": "รายละเอียดเพิ่มเติมเกี่ยวกับสถานที่", "en": "More details about Booking Location" },
    "parking": { "th": "กรุณาระบุว่า มีที่จอดรถฟรี มีที่จอดเสียเงิน ไม่มีที่จอด", "en": "Please confirm parking type: (1) Free Parking, (2) Paid Parking, (3) No Parking." },
    "Payment": { "th": "ดำเนินการชำระเงิน", "en": "Payment Process" },
    "Back": { "th": "ย้อนกลับ", "en": "Back" },
    "Complete_Payment": { "th": "การชำระเงินเสร็จสิ้น", "en": "Complete Payment" },
    "Please_check": { "th": "ระบบได้ส่งข้อมูลการจองไปให้ในอีเมลของท่านแล้ว กรุณาตรวจสอบอีเมล", "en": "Please check your mailbox. Booking information has been sent to your registered email address" },
    "Unsuccessful": { "th": "การชำระเงินไม่สำเร็จ", "en": "Unsuccessful Payment" },
    "mont1": { "th": "ใช้งานได้ 1 เดือน", "en": "1-Month Validity" },
    "mont12": { "th": "ใช้งานได้ 12 เดือน", "en": "12-Month Validity" },
    "validity": { "th": "อายุการใช้งานใบรับรองอิเล็กทรอนิกส์", "en": "Electronic certificate validity" },
}