Pages.Template = {
    _template: 'template.html',
    template: '<div>this is template.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_template,
            upload_path: upload_path,
            list_file_pdf: [],
            originalFile: '',
            pdfFile: '',
            doc_id: '',
            all_original_filenames: {},
            imageFiles: [],
            build_pdf: false,
            imageMerge: '',
            display_imageFiles: [],
            all_images: {},
            alert_no_pdf:false,
            text_alert:'',
            template_list: [],
            previews: [],
            activeTemplate: {}
        } 
    },
    mounted: function () {
        window.myApp = this
        this.upload_file()
        this.getTemplateList()
    },
    methods: {
        upload() {
            console.log('upload')
            $('#file-dropzone-template').click()
        },
        initThumbnails: function () {
            var arr = this.imageFiles;
            this.imageAspectRatios = [] //reset aspect ratio array  
            var _done_loading = 0
            var that = this

            for (var i = 0; i < arr.length; i++) {
                this.imageAspectRatios.push(0)
                var img = new Image()
                img._index = i
                img.onload = function () {
                    var I = this._index
                    that.imageAspectRatios[I] = (this.width / this.height);
                    _done_loading++
                }
                img.src = upload_path + arr[i];
            }

        },
        upload_file: function () {
            var that = this
            var dropzone = new Dropzone(document.getElementById('file-dropzone-template'), {
                url: api_host + 'upload_pdf',
                maxFilesize: 25, // MB
                maxFiles: 50,
                clickable: true,
                acceptedFiles: ".jpeg,.jpg,.png,.pdf"
            });

            dropzone.on("addedfile", function (file) {
                $('#loadingModal').modal('show');
            });

            var dropzoneSuccess = function (file, json) {
                var res = JSON.parse(json)
                var images = res.pdf_str.split("\n");

                //set default subject
                if (that.list_file_pdf.length == 0) {
                    that.originalFile = file.name
                    that.pdfFile = res.filename
                    that.doc_id = res.filename.substring(0, res.filename.indexOf('.'))
                }

                that.all_original_filenames[res.filename] = file.name
                that.imageFiles = that.imageFiles.concat(images);
                that.list_file_pdf.push(res.filename)
                that.initThumbnails()
                that.build_pdf = true
                that.imageMerge = res.image_merge
                that.display_imageFiles.push(images[0]);
                that.all_images[res.filename] = images
                that.saveTemplate()

            }

            dropzone.on("success", dropzoneSuccess);
            dropzone.on("error", function () {
                that.alert_no_pdf = true
                that.text_alert = that.lang_txt.max_size[that.lang_choose]
                setTimeout(function(){ that.alert_no_pdf = false}, 3000);
            });
        },
        getTemplateList () {
            that = this
            $.ajax({
                type: "POST",
                url: api_host + "get_template_list",
                data: JSON.stringify({email: getUsername()}),
                dataType: "json",
                success: function (res) {
                    console.log(res.data)
                    that.template_list = res.data
                },
                error: function (request, status, error) {
                    console.log("|" + status + ' |' + error);
                }
            });
        },
        saveTemplate() {
            console.log('saveTemplate')
            that = this
            const data = JSON.stringify(that.toEdoc())
            const url = api_host + 'save_template'
        
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (result) {
                    that.getTemplateList()
                    $("#loadingModal").modal("hide") 
                },
                error: function (error) {
                    console.log("Save template error: " + error)
                    $("#loadingModal").modal("hide") 
                }
            })
        },
        get_name: function () {
            if (getUserField('FNAME') == '') {
                this.FNAME = getUserField('adminFisrtname')
            }
        },
        toEdoc() {
            var that = this
            var name = getUserField('FNAME') + " " + getUserField('LNAME')
            var e = getUsername()
            var ret = {
                originalFile: that.originalFile, 
                pdfFile: that.pdfFile, 
                template_id: (Date.now() + '' + Math.floor(Math.random() * 10000000)),
                imageFiles: that.imageFiles, 
                owner: e, 
                owner_name: that.get_name(),
                display_imageFiles: that.display_imageFiles,
                all_original_filenames: that.all_original_filenames,
                all_images: that.all_images,
                list_file_pdf: that.list_file_pdf,
                imageMerge: that.imageMerge
            }
            // alert(JSON.stringify(that.all_images))
            // window.x_nut = that.all_images
            return ret
        },
        openUpload(id) {
            console.log('openUpload')
            this.selected_id = id
            $("#optionSignModal").modal("show") 
        },
        select_sign(n) {
            console.log('select_sign')
            document.location.href = "/bla/upload_new3.html?template_id=" + this.selected_id + '&sign_type='+n
        },
        showPdfImage: function (r) {
            this.previews = r.imageFiles
            $('#showdocument').modal('show')
        },
        remove_template: function (id) {
            var that = this

            Swal.fire({
                title: "คุณต้องการลบเทมเพลตหรือไม่",
                showDenyButton: true,
                confirmButtonText: "ยืนยัน",
                denyButtonText: "ยกเลิก",
            }).then((result) => {
                if (result.isConfirmed) {
                    var data = { id: id }
                    $.ajax({
                        type: "POST",
                        url: api_host + "remove_template",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (resp) {
                            if (resp.success) {
                                that.getTemplateList()
                            }
                            else {
                                console.log(resp.error_msg);
                            }
                        },
                        error: function (request, status, error) {
                            console.log("|" + status + ' |' + error);
                        }
                    });
                }
                else if (result.isDenied) {
                    console.log("cancel remove template")
                }
            })
        },
        changeName(r) {
            this.activeTemplate = r
            console.log(this.activeTemplate)
            $("#changNameModal").modal("show") 
        },
        submitChangeName() {
            console.log('submitChangeName')
            that = this
            const data = JSON.stringify(that.activeTemplate)
            const url = api_host + 'update_template'
        
            $.ajax({
                url: url,
                type: "POST",
                data: data,
                success: function (result) {
                    that.getTemplateList()
                    $("#changNameModal").modal("hide") 
                    that.activeTemplate = {}
                },
                error: function (error) {
                    console.log("Save template error: " + error)
                }
            })
        }
    }

}

routes.push({ path: '/template', component: Pages.Template })

var lang_template = {
    "title": { "th": "เทมเพลตเอกสาร", "en": "Document Template" },
    "self": { "th": "เซ็นคนเดียว", "en": "Self-Signing" },
    "multi": { "th": "เซ็นหลายคน", "en": "Multiple-Signing" },
    "choose": { "th": "กรุณาเลือกวิธีการเซ็น", "en": "Choosing signing type" },
    "preview": { "th": "ดูเอกสาร", "en": "File Preview" },
    "using": { "th": "ใช้งาน", "en": "Use" },
    "rename": { "th": "เปลี่ยนชื่อ", "en": "Rename" },
    "delete": { "th": "ลบ", "en": "Delete" },
    "cancel": { "th": "ยกเลิก", "en": "Cancel" },
    "save": { "th": "บันทึก", "en": "Save" },
    "change_name": { "th": "เปลี่ยนชื่อเอกสาร", "en": "Change Document Name" }
}