Pages.Folder = {
    _template: 'folder_doc.html',
    template: '<div>this is folder_doc.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_folder,
            display: false,
            name_folder: this.$route.query.name || "",
            list_document: [],
            list_document2: [],
            txt_search: "",
            filter: {
                doc: "",
                signer: "",
                send: "",
                date: "",
            },
            count_check: 0,
            selectDocument: [],
            mv_folder: [],
            selectFolder: "",
            list_folder:[],
        }
    },
    mounted: function () {
        window.myApp = this
        this.list_doc(this.$route.query.name, this.$route.query.id)
        this.get_folder()
    
    },
    filters: {
        date(value) {
            value = value || ''
            var date = new Date(value)
            var day = date.getDate()
            var month = date.getMonth() + 1
            var year = date.getFullYear()
            value = day + "/" + month + "/" + year
            return value || ""
        },
        time(value) {
            value = value || ''
            var date = new Date(value)
            var horus = date.getHours()
            var minutes = date.getMinutes()
            var seconds = date.getSeconds()
            value = horus + ":" + minutes + ":" + seconds
            return value || ""
        }
    },
    methods: {
        move_list_folder:function(){
            var that = this
            if(that.list_folder.length > 0){
                for(i = 0 ; i<that.list_folder.length ; i++){
                    var data = []
                    for (j = 0; j < that.selectDocument.length; j++) {
                        data.push({ id_doc: that.selectDocument[j], id_folder: that.list_folder[i]})
                    }
                    $.ajax({
                        type: "POST",
                        url: api_host+ "move_folder",
                        data: JSON.stringify(data),
                        dataType: "json",
                        success: function (resp) {
                            if(resp.success) {
                                console.log("move success")
                            }
                            else {
                                console.log("move unsuccess")
                            }
                            
                        },
                        error: function (request, status, error) {
                           
                            alert("|" + status + ' |' + error);
                        }
                    });
                }
                that.list_folder = []
                alert(that.lang_txt.complete[that.lang_choose])
            }else{
                console.log('not folder');
            }
        },
        get_folder: function () {
            var that = this
            var adminEmail = getUsername() || "N/A"
            that.mv_folder = []
            // setTimeout(function(){ 

            if (adminEmail) {
                var data = { owner_folder: adminEmail }

                $.ajax({
                    type: "POST",
                    url: api_host + "get_folder",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        that.mv_folder = res.data
                    },
                    error: function (request, status, error) {
                        // alert("|" + status + ' |' + error);
                        console.log("|" + status + ' |' + error)
                    }
                });
            }


            //  }, 3000);

        },
        openBar: function () {
            var that = this;
            that.count_check = that.selectDocument.length
        },
        count_signer: function (signer, fields) {

            if (fields.length > 0) {
                var total = 0

                for (i = 0; i < signer.length; i++) {
                    if (signer[i].needToSign == true) {

                        var f_owner = fields.filter(function (f) {
                            return (signer[i].email == f.email)
                        })

                        var f_sign = fields.filter(function (f) {
                            return (signer[i].email == f.email && f.status == 1)
                        })

                        if (f_sign.length == f_owner.length) {
                            total = total + 1
                        }

                    }
                    else {
                        total = total + 1
                    }
                }
                return total
            }
            else {
                return signer.length
            }
        },
        my_status_signer: function (signer, fields, status) {

            if (fields && fields.length > 0 && status == 0 || !status) {
                var email = getUsername() || "N/A"
                if(!fields.length) fields = []
                var my = fields.filter(function (f) {
                    return (f.email == email && f.status == "0")
                })
                var other = fields.filter(function (f) {
                    return (f.email != email && f.status == "0")
                })

                if (my.length > 0) return "0"
                else if (my.length == 0 && other.length > 0) return "1"
                else return ""

            }
            else {
                return ""
            }

        },
        gotoDoc: function (id) {
            document.location.href = "#/document?id=" + id
        },
        set_table: function () {
            var that = this
            setTimeout(function () {
              table = $('#table').DataTable({
                    paging: true,
                    searching: false,
                    ordering: false,
                    info: false,
                    lengthChange: true,
                    responsive: true,
                    lengthMenu: [ [20, 50, 100, -1], [20, 50, 100, "ทั้งหมด"] ],
                    dom: '<"top"i>rt<"bottom"flp><"clear">',
                    language: {
                        lengthMenu: "_MENU_",
                    },
                    fnDrawCallback: function( oSettings ) {
                        var list = document.getElementsByClassName("checkfile")
                        var list_check = []
                        for(i=0; i<list.length;i++) {
                            if(list[i].checked == true) list_check.push(list[i])
                        }
                        if(list_check.length == 20) document.getElementById("check_all").checked = true
                        else document.getElementById("check_all").checked = false                        
                    }
                });
            }, 100);
            that.display = true
        },
        show_all_table: function () {
            table.destroy()
        },
        search_table: function () {
            var that = this

            if (that.txt_search == "") {
                that.list_document = that.list_document2
            }
            else if (event.keyCode == "8" || event.keyCode == "46") {
                that.list_document = that.list_document2
                var list = that.list_document
                if (list.length == 0) return false

                var search = list.filter(function (s) {
                    var subject = s.subject.search(that.txt_search) != -1
                    var owner = s.owner.search(that.txt_search) != -1
                    var signers = s.signers.find(function (sig) {
                        return sig.email.search(that.txt_search) != -1
                    })
                    var list = subject || owner || signers
                    return list
                })

                that.list_document = search
            }
            else {

                var list = that.list_document
                if (list.length == 0) return false

                var search = list.filter(function (s) {
                    var subject = s.subject.search(that.txt_search) != -1
                    var owner = s.owner.search(that.txt_search) != -1
                    var signers = s.signers.find(function (sig) {
                        return sig.email.search(that.txt_search) != -1
                    })
                    var list = subject || owner || signers
                    return list
                })

                that.list_document = search

            }
        },
        fill_doc: function (value, arr) {
            if(value == "") return false
            var that = this
            var list = arr
            var fill = list.filter(function (f) {
                return f.status == value
            })
            return fill
        },
        fill_sign: function(value, arr){
            if(value == "") return false
            var that = this
            var list = arr
            var fill = list.filter(function (f) {
                return f.fields.find(function(s){
                    return s.status == value && s.email == getUsername() && that.not_field(s.toolName)
                })
            })
            return fill
        },
        not_field(field) {
            var not = ["Qrcode", "secretly_1", "secretly_2", "secretly_3", "urgent_1", "urgent_2", "urgent_3", "company"]
            var chk = (not.find(function(n){return n == field}))
            return chk
        },
        fill_send: function(value, arr){
            if(value == "") return false
            var that = this
            var list = arr
            var fill = []
            var email = getUsername() || "N/A"
            that.filter.send = value

            if(value == "0"){fill = list.filter(function(f){ return f.owner == email})}
            else if (value == "1"){ fill = list.filter(function(f){ return f.owner != email })}
            return fill
        },
        fill_date: function(value, arr){
            if(value == "") return false
            var that = this
            var list = arr
            var maxDate = Number(value)
            var fill = list.filter(function(f){
                return f.diff_date <= maxDate
            })
            return fill
        },
        cal_date: function(before){
            var today = new Date().getTime()
            var diff = (today - before)/(24*60*60*1000)
            return Math.round(diff)
        },
        set_fill:function(k, v){
            var that = this
            var p = that.filter
            if(p[k] != "" || v == "") that.list_document = that.list_document2

            p[k] = v
            for(var key in p) {  
                if(key == "doc" && p[key] != ""){
                    that.list_document = that.fill_doc(p[key], that.list_document)
                }
                else if (key == "signer" && p[key] != "") {
                    that.list_document = that.fill_sign(p[key], that.list_document)
                }
                else if (key == "send" && p[key] != "") {
                    that.list_document = that.fill_send(p[key], that.list_document)
                }
                else if (key == "date" && p[key] != "") {
                    that.list_document = that.fill_date(p[key], that.list_document)
                }
            }

            that.show_all_table()
            that.set_table()   
        },
        clear_filter: function (value) {
            var that = this
            
            if (value == "all") {
                that.list_document = that.list_document2
                that.filter = { doc: "", signer: "", send: "", date: "" }
                that.show_all_table()
                that.set_table()
            }
            else if (value == "doc") {
                that.filter.doc = ""
                that.set_fill("doc", "")
            }
            else if (value == "signer") {
                that.filter.signer = ""
                that.set_fill("signer", "")
            }
            else if (value == "send") {
                that.filter.send = ""
                that.set_fill("send", "")
            }
            else if (value == "date") {
                that.filter.date = ""
                that.set_fill("date", "")
            }
        },
        list_doc:function(name,id){
            var that = this
            var data = { id_folder: id}
            // console.log(id);
            $.ajax({
                type: "POST",
                url: api_host+ "get_doc_folder",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                    console.log(resp);

                    
                        if (!resp.data.length) resp.data = []
                        var list = resp.data

                        if (list.length > 0) {
                            list.sort(function (a, b) {
                                return b.createdDtm - a.createdDtm
                            })

                            for(i=0;i < list.length;i++) {
                                list[i].diff_date = that.cal_date(list[i].createdDtm)
                            }

                            that.list_document = list
                            that.list_document2 = list
                            that.set_table()
                        }
                        else {
                            that.set_table()
                        }
                    
                },
                error: function (request, status, error) {
                    router.push({ path: '/dashboard'})  
                    // alert("|" + status + ' |' + error);
                }
            });
        },
        remove_doc_folder:function(){
            var that = this
            var data = []
            for (j = 0; j < that.selectDocument.length; j++){
                data.push({id_doc: that.selectDocument[j], id_folder: that.$route.query.id})
            }

            $.ajax({
                type: "POST",
                url: api_host + "remove_doc_folder",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    // table.destroy()
                    // setTimeout(function(){ 
                    //     that.list_doc(that.$route.query.name, that.$route.query.id)
                    //     }, 1500);
                    
                    // that.count_check = 0 
                    // that.selectDocument = []
                    document.location.reload()
                    
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        check_all: function(s) {
            var that = this
            var all = document.getElementById("check_all")
            var list = document.getElementsByClassName("checkfile")

            if(all.checked){
                for(i=0; i< list.length; i++) {
                    list[i].checked = all.checked
                    that.selectDocument.push(list[i].value)
                }
                var uniqueChars = [...new Set(that.selectDocument)];
                that.selectDocument = uniqueChars
                that.count_check = that.selectDocument.length
            }
            else {
                var list_select = []
                for(i=0; i< list.length; i++) {
                    var select = that.selectDocument.find(function(s) {
                        return list[i].value == s
                    })
                    if(select) list_select.push(select)
                    list[i].checked = all.checked
                }
                var remove_select = that.selectDocument.filter(function(s) {
                    return list_select.indexOf(s) == -1;
                });
                that.selectDocument = remove_select
                that.count_check = that.selectDocument.length
            }
        },
        t_onover: function (id) {
            var selected = document.getElementById(id)
            var tooltip = new bootstrap.Tooltip(selected)
            tooltip.show()
        },
    }
}

routes.push({ path: '/folder_doc', component: Pages.Folder })

var lang_folder = {
    "inbox_title": { "th": "เอกสารที่ได้รับ", "en": "Inbox" },
    "adv_search": { "th": "ค้นหาขั้นสูง", "en": "Advanced search" },
    "clear": { "th": "ล้าง", "en": "Clear" },
    "des_search": { "th": "ชื่อเอกสาร, ผู้ส่ง, อีเมล", "en": "Subject, Sender, Email" },
    "doc_status": { "th": "สถานะเอกสาร", "en": "Document Status" },
    "sign_status": { "th": "สถานะการเซ็น", "en": "Signing Status" },
    "sender": { "th": "ผู้ส่ง", "en": "Sender" },
    "year": { "th": "12 เดือนย้อนหลัง", "en": "Last 12 months" },
    "six": { "th": "6 เดือนย้อนหลัง", "en": "Last 6 months" },
    "month": { "th": "30 วัน", "en": "Last 30 days" },
    "week": { "th": "1 สัปดาห์", "en": "Last 1 weeks" },
    "day": { "th": "1 วัน", "en": "Last 1 days" },
    "me": { "th": "ฉัน", "en": "Me" },
    "notme": { "th": "ไม่ใช่ฉัน", "en": "Not me" },
    "date": { "th": "วันที่", "en": "Date" },
    "subject": { "th": "เรื่อง", "en": "Subject" },
    "status": { "th": "สถานะ", "en": "Status" },
    "date_time": { "th": "วัน-เวลา", "en": "Date & Time" },
    "sign": { "th": "เซ็นเอกสาร", "en": "Sign" },
    "signed": { "th": "เซ็นแล้ว", "en": "Signed" },
    "notsign": { "th": "ยังไม่ได้เซ็น", "en": "Not Sign" },
    "exp_date": { "th": "หมดอายุเมื่อ", "en": "Expiration Date" },
    "doc_cancel": { "th": "เอกสารถูกยกเลิก", "en": "Cancelled." },
    "exp": { "th": "(หมดอายุ)", "en": "(Expired)" },
    "list": { "th": "รายการ", "en": "List" },
    "incomplete": { "th": "ยังไม่สมบูรณ์", "en": "Incomplete" },
    "exp_soon": { "th": "หมดอายุเร็วๆ นี้", "en": "Expired soon" },
    "complete": { "th": "เสร็จสมบูรณ์", "en": "Complete" },
    "move": { "th": "ย้าย", "en": "Move to folder" },
    "select_move": { "th": "แฟ้มที่ต้องการย้าย", "en": "Select Folder to Move" },
    "confirm_move": { "th": "ยืนยันย้าย", "en": "Confirm to Move" },
    "wait": { "th": "กรุณารอสักครู่..", "en": "Please wait.." },
    "des_create_folder": { "th": "โฟลเดอร์นี้ไม่มีเอกสาร", "en": "This folder is empty." },
    "expose": { "th": "นำออก", "en": "Remove" },
    "cancelled": { "th": "ถูกยกเลิก", "en": "Cancelled" },
    "action_requi": { "th": "ที่ต้องดำเนินการ", "en": "Action Required" },
    "waiting_other": { "th": "รอคนอื่นดำเนินการ", "en": "Waiting for Others" },

}