function dragTool(evt){
    var key = upload.draggingTool.toolName + upload.selectedPersonIndex
    console.log("dragTool = "+key)
    evt.dataTransfer.setDragImage(dragingImages[key] , 70, 70);

    //evt.dataTransfer.setDragImage(dragingImages.Signature, 70, 70);
}

function allowDrop(evt){
    event.preventDefault()
}

function upload_vue()
{
  return {
        id : '#app',
        data : {
        adjustedWidth : 100,
        totalHeight : 0,
        pagesDim : [],
        master_group : null ,
        draw  : null,
        draggingTool : null,
        imageFiles : [],

        //business logics
        delField: null, //field to be deleted
        fields: [], 
        selectedPersonIndex: 0,
        selectedPerson: null
        }, 
        methods : {
        getPersonColor: function () {
            var colors = this.color || ['yellow', 'blue', 'pink', 'green', 'grey', 'orange']
            return colors[this.selectedPersonIndex]
        },

       
       reset_draw : function(){
           var draw = this.draw
           //1. reset draw dimension 
           this.totalHeight = 0
           this.pagesDim = []
           
           //2. create a master group
           this.master_group = draw.group() 

           console.log("Reset draw !!!")
       },
       init_svg: function (i) {

               var draw = this.draw

               if(i==0)
                 { 
                     this.reset_draw() 
                 }
                 var images = this.imageFiles.map( img => {
                     return 'https://hjkl.ninja/face/images/card/' + img
                 })
               //var images = ['https://hjkl.ninja/face/images/card/pdf165582731614309107-0.png',
               //'https://hjkl.ninja/face/images/card/pdf165582731614309107-1.png']
             
               draw.image(images[i], function(evt){ 
                   var  group = upload.new_document_page(draw, this)  
                   upload.master_group.add(group)
                   

                   if(i<images.length-1){upload.init_svg(i+1)} //load next page
               })   
               

           },
           new_document_page : function(draw, img){
               var group = draw.group()  
               var adjustedWidth   = this.adjustedWidth
               var adjustedHeight = adjustedWidth * (img.height() / img.width()) 
                   img.size(adjustedWidth , adjustedHeight)   
                   var newTotalHeight =  this.totalHeight  + adjustedHeight
                   draw.size(adjustedHeight , newTotalHeight)
                   group.add(img)
                   group.move(0, this.totalHeight) 
                   this.totalHeight = newTotalHeight //Save new height value 

                   upload.pagesDim.push({width:adjustedWidth, height:adjustedHeight}) //store page dimensions
               

               return group  
           }, 

           activate : function(field){
            var oldField = this.delField
            if(oldField)
              {
                var rect = oldField.group.find('rect')
                rect.stroke({color: 'blue', width : 0}) 
              }   

            this.delField = field
            var rect = field.group.find('rect')
            rect.stroke({color: 'blue', width : 1})

            this.show_set_detail = true
            this.chk_set_field = field.toolName 

            console.log('activate >>>>>>>>>>  '+ field.id+','+field.toolName)
           },

           update_loc : function(field){
            var g = field.group
            var page = this.compute_page(g.y())
            var dim = this.pagesDim[page]
            var offsetY = 0

            for(var i=0;i< this.pagesDim.length;i++)
              {
                  if(i<page) { offsetY += this.pagesDim[i].height }
              }

            var x = g.x()
            var y = g.y() - offsetY
            var xp = (x * 100) / dim.width
            var yp = (y * 100) / dim.height 
             
            /* update field loc */
            field.page = page
            field.xp = xp
            field.yp = yp
            console.table('update_loc',field)
           },

           compute_page : function(y){
               var ret = -1
               var arr = this.pagesDim
                
               var y1 = 0 
               var y2 = 0

               for(var i=0; i<arr.length; i++)
                 {
                       y2 = y1 + arr[i].height  
                       if(y >= y1 && y <= y2)
                          {ret = i; break;}
                       y1 = y2   
                 }
               
                 return ret
           },

           field : function(fieldId){
               return this.fields.find(f => {return f.id == fieldId})
           },
           
           
           attach_drag : function(g){

               g.on('click', function(evt){ 

                var field = upload.field(this._field_id) 
                if(!field){ console.log(">>>>>>> invalid fieldIdC "+ this._field_id);return;}

                upload.activate(field)

                }) 

               g.draggable()  
               g.on('dragstart.namespace', function (evt) {
                var field = upload.field(this._field_id) 
                if(!field){ console.log(">>>>>>> invalid fieldIdB "+ this._field_id);return;}

                upload.activate(field)
                
               
            })

               g.on('dragmove.namespace', function (evt) {
                   const { handler, box } = evt.detail
                   let { x, y } = box
                   g.find("text").text(`${g._toolName}`)
                   //g.find("text").text(`${g._toolName}\n(${parseInt(x)},${parseInt(y)})\npage ${upload.compute_page(y)}`)
                 
               })

               g.on('dragend.namespace', function (evt) {
                console.log('dragend done')
                window.dend = this;

                var field = upload.field(this._field_id)
                if(!field){ console.log(">>>>>>> invalid fieldIdA "+ this._field_id) ;return;}

                upload.update_loc(field)
            


            })



               return g
           }, 

           set_tool : function (toolName, N) {  
           var field = {
               "id" : this.fieldID(),
               "toolName": toolName, 
               "isNew"   : true, 
           }
           if(toolName=='Dropdown'){field.toolOptions = ''}
           
           this.draggingTool = field
           console.log('set tool to '+field)

           },

           fieldID : function(){
               var tmp = window.TMP_FIELD_ID || new Date().getTime()
               tmp = tmp+1
               window.TMP_FIELD_ID = tmp    
               return tmp
           },
           prepare_new_tool : function (mode) {  
            console.log(`prepare_new_tool mode ${mode}`)  
                var field = this.draggingTool

            if(!field)
            {
                console.log('prepare_new_tool skipped Reason#1')    
                return; //user pick no tool
            }
            if(field.group)
            {
                console.log('prepare_new_tool skippedReason#2 >>> already have group')    
                return; //user already have group install
            }  
            console.log(this.draw)
            var draw  = this.draw  
            
            var g = draw.foreignObject(190,70)
            g._toolName = field.toolName
            var color = this.getPersonColor()
            var div = document.createElement('div')
            div.style.backgroundColor = color

            var input = document.createElement('input')
            input.value = ''
            input.style.margin = '10px'
            input.style.padding = '3px'
            input.setAttribute("type", "text");
            input.onclick = function () {
                g.draggable(false)
                input.focus()
            }
            input.onblur = function () {
                upload.attach_drag(g)  
                field.toolData = input.value
            }

            div.appendChild(input)
            g.add(div)

            upload.attach_drag(g)   
            field.group = g
            g._field_id = field.id
            g_field = field
            this.activate(field)

            if(mode==2)
            {
                g.hide()
            }  
           },
           mousemove_prepare_new_tool : function(evt){ 
             var field = this.draggingTool
             //this.draggingTool = null

             if(!field)
             {
                console.log('  mousemove_prepare_new_tool skipped')    
                return; //user pick no tool
             }
             const {x, y, clientX, clientY , offsetX , offsetY} = evt
             //console.log(x,y , ' | ' ,clientX,clientY, '|' , offsetX , offsetY)
             field.group.move(offsetX, offsetY)
           },
           insert_new_tool : function(){
             console.log("insert_new_tool")
             var field = this.draggingTool

             if(!field)
             {
                console.log('  insert_new_tool skipped')    
                return; //user pick no tool›
             }

             //pat todo:
             //field.group.on('click', function(){alert('show property editor now !!!')})
            this.draggingTool = null  
             this.addField(field)

           },

        addField: function (field, n) {
            console.log(' addField ')
            field.email = this.selectedPerson 
            this.fields.push(field)
            this.delField = field;
            this.update_loc(field)



            console.log("New Field Has been added >>>> "+ this.fields.length)
            
        },

           dropToSvg : function(evt){
               evt.preventDefault()
               console.log("dropToSvg")
               window.dropEvt = evt

               var field = this.draggingTool
             

             if(!field)
             {
                console.log(' dropToSvg skipped')    
                return; //user pick no tool
             }
             const {x, y, clientX, clientY , offsetX , offsetY} = evt
             console.log(x,y , ' | ' ,clientX,clientY, '|' , offsetX , offsetY)
             field.group.move(offsetX, offsetY)
             field.group.show()
           }

           
           
       } //methods
    }   
}
