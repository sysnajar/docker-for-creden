window.dragingImages = {

}

window.field_group_count = 0

function getMyText(field) {
    $.ajax({
        type: "POST",
        url: document.location.origin + "/capi/get_textmykey",
        data: JSON.stringify({ text_key: $('#text_key').val() }),
        dataType: "json",
        success: function (res) {
            if (res.text == "") {
                console.log('res.text is empty')
            }
            console.log('wrapped text = ' + res.text)
            field.toolData = res.text

            //toolsApp.setField();
            // for(var i=0;i<app.sign_obj.fields.length;i++) { console.log(i+' = '+app.sign_obj.fields[i].status)}
        },
        error: function (request, status, error) {
            alert("|" + status + ' |' + error);
        }
    });
}

function wrapText(_val, field) {
    if (_val) {
        if (_val.trim().length == 0) { _val = "text" }
        console.log('set _val = ' + _val)
        document.getElementById('multilineText').value = _val;
        var mytext = document.getElementById('text_key').value = 'mytext' + new Date().getTime();
    }

    f = document.getElementById('submitform2')
    f.onsubmit = function () { return true }

    // $(f).submit(function(evt){
    //     alert('done'+ evt)
    // })
    $(f).submit()
    setTimeout(function () { getMyText(field) }, 100);
}



/* Adjust icons after resize fields */
function adjustIcon(text, rect, cx, cy) {
    var iconAndText = text.parent() //group that holds icon and text 
    var rx = rect.x()


    var rx = rect.x()
    var ry = rect.y()
    var rx2 = rx + 20
    var ry2 = ry + (rect.height() / 3.2)
    iconAndText.move(rx2, ry2)
    text.move(rx2 + 20, ry2)


}

function zoomIn() {
    var that = upload
    if (that.current_fac == 0.2) {
        zoomLevel(0.5)
        return;
    }
    if (that.current_fac == 0) {
        zoomLevel(0.2)
        return;
    }

    if (that.current_fac == -0.2) {
        zoomLevel(0)
        return;
    }
    if (that.current_fac == -0.5) {
        zoomLevel(0.2)
        return;
    }
}

function zoomOut() {
    var that = upload
    if (that.current_fac == 0.5) {
        zoomLevel(0.2)
        return;
    }
    if (that.current_fac == 0.2) {
        zoomLevel(0)
        return;
    }
    if (that.current_fac == 0) {
        zoomLevel(-0.2)
        return;
    }
    if (that.current_fac == -0.2) {
        zoomLevel(-0.5)
        return;
    }
}
function zoomLevel(fac) {
    fac = parseFloat(fac)
    var that = upload
    var x = 0
    var y = 0

    console.log('zoomLevel fac = ' + fac)

    if (fac == 0.2 || fac == 0.5) { x = 100; y = 400 }

    //zoom back if needed
    if (that.current_fac == 0.5) {
        scale('#main-svg', -0.5, 0, 0)
        console.log('zoomOut fac = ' + fac)

    }
    if (that.current_fac == 0.2) {
        scale('#main-svg', -0.2, 0, 0)
        console.log('zoomOut fac = ' + fac)
    }

    if (that.current_fac == -0.5) {
        scale('#main-svg', 0.5, 0, 0)
        console.log('zoomIn fac = ' + fac)
    }
    if (that.current_fac == -0.2) {
        scale('#main-svg', 0.2, 0, 0)
        console.log('zoomIn fac = ' + fac)
    }

    if (fac != 0) {
        scale('#main-svg', fac, x, y)
        console.log('Scale fac = ' + fac)
    }

    that.current_fac = fac


    $('#zoomLevel').val(fac + '')
}
function scale(sel, fac, x, y) {
    $(sel).transition({ scale: ($(sel).css('scale') + fac) });
    $(sel).transition({ x: x, y: y });
}

function reset_div_height(id, min) {
    /* 
 var ele        = document.getElementById(id)
 var height     = ele.getBoundingClientRect().height
 var newHeight  = height - min
 ele.style.height = newHeight+'px'
 $(ele).css('padding-bottom', '50%')
 
 console.log(id+' height = '+ height+'\n newHeight='+ newHeight)
 */
}

function preload_svg(colors) {
    for (var i = 0; i < colors.length; i++) {
        var color = colors[i]
        var tools = ["Signature", "Initial", "Date", "Name", "Email",
            "Text", "Checkbox", "Dropdown", "Radio", "Radio2", "Attachment", "Approve", "Qrcode"]

        tools.forEach(t => {
            var svg = `<svg version="1.1"
         baseProfile="full"
         width="140" height="45"
         xmlns="http://www.w3.org/2000/svg">
            
            <rect width="100%" height="100%" fill="${color}" />
            <path fill-rule="evenodd" clip-rule="evenodd" d="M24.6696 16.3631C24.9458 16.0869 24.9458 15.6407 24.6696 15.3645L23.0121 13.7072C22.8797 13.5745 22.7001 13.5 22.5127 13.5C22.3253 13.5 22.1457 13.5745 22.0133 13.7072L20.625 15.0953L23.2812 17.7513L24.6696 16.3631ZM22.5729 18.4596L19.9167 15.8036L12.9396 22.78C12.8688 22.8508 12.8333 22.9358 12.8333 23.0349V25.188C12.8333 25.3864 12.9892 25.5422 13.1875 25.5422H15.3408C15.4329 25.5422 15.525 25.5068 15.5887 25.4359L22.5729 18.4596ZM25.5833 27.667H11.4167C10.6375 27.667 10 28.3044 10 29.0835C10 29.8626 10.6375 30.5 11.4167 30.5H25.5833C26.3625 30.5 27 29.8626 27 29.0835C27 28.3044 26.3625 27.667 25.5833 27.667Z" fill="black"/>
            <text x="65" y="27" font-size="16" text-anchor="middle" fill="black">${t}</text>
            </svg>
         `
            // tools.forEach(t => {
            //     var svg = `<svg version="1.1" width="140" height="44" baseProfile="full" viewBox="0 0 140 44" xmlns="http://www.w3.org/2000/svg">        
            //     <rect width="140" height="44" fill="${color}"/>
            //     <path fill-rule="evenodd" clip-rule="evenodd" d="M24.6696 16.3631C24.9458 16.0869 24.9458 15.6407 24.6696 15.3645L23.0121 13.7072C22.8797 13.5745 22.7001 13.5 22.5127 13.5C22.3253 13.5 22.1457 13.5745 22.0133 13.7072L20.625 15.0953L23.2812 17.7513L24.6696 16.3631ZM22.5729 18.4596L19.9167 15.8036L12.9396 22.78C12.8688 22.8508 12.8333 22.9358 12.8333 23.0349V25.188C12.8333 25.3864 12.9892 25.5422 13.1875 25.5422H15.3408C15.4329 25.5422 15.525 25.5068 15.5887 25.4359L22.5729 18.4596ZM25.5833 27.667H11.4167C10.6375 27.667 10 28.3044 10 29.0835C10 29.8626 10.6375 30.5 11.4167 30.5H25.5833C26.3625 30.5 27 29.8626 27 29.0835C27 28.3044 26.3625 27.667 25.5833 27.667Z" fill="black"/>
            //        <text text x="60" y="25" font-size="16" text-anchor="middle" fill="black">${t}</text>
            //        </svg>
            //     `

            var b64 = window.btoa(svg);
            var img = document.createElement("img");
            img.src = 'data:image/svg+xml;base64,' + b64
            var key = t + i // eg. Signature0 
            window.dragingImages[key] = img
            //console.log(key)   
            if (key == 'Signature0') { window.dragingImages["Signature"] = img }

        })

    }

}



function loadSvgImg(i, img) {
    window.myApp.init_svg(i)
}

function setActiveSvg(svg) {
    var pageIndex = svg.id.charAt(3)
    //console.table('setActiveSvg#',  pageIndex , svg)

    var app = window.myApp
    app.activeSvg = svg

    if (app.draggingTool && !app.draggingTool.isShowed) {
        app.showTool(pageIndex);
        if (app.draggingTool.person.amount_least_sign > 1) {
            cloneThisField(app.draggingTool)
        }
    }

}

function cloneThisField(f) {
    console.log(">>>>>>>> clone ths field for [" + f.person.amount_least_sign + "] times")
    var app = upload
    var t = f.person.amount_least_sign - 1
    for (var i = 0; i < t; i++) {
        var f2 = clone_field(f, 1)
        f2.id = f2.id + (i + 1)
        // app.activate(f2) // kit
        //f2.isShowed = false
        app.draggingTool = f2
        // app.showTool(f2.page) 
        app.prepare_new_tool(1, f2) //kit
        app.addField(f2)

        var rect = f2.group
        if (!rect.visible()) { rect.show() }
        var newX = f.group.x() + (10 * (i + 1))
        var newY = f.group.y() + (10 * (i + 1))
        rect.move(newX, newY)
        copyStyleUI(f2)

        // var f2 = clone_field(f, f.page)
        // f2.id = f2.id + (i + 1)
        // app.addField(f2)
        // // app.activate(f2) // kit
        // f2.isShowed = false
        // app.draggingTool = f2
        // // app.showTool(f2.page) 
        // app.prepare_new_tool(1) //kit
        // var rect = f2.group
        // if (!rect.visible()) { rect.show() }
        // var newX = f.group.x() + 10
        // var newY = f.group.y() + 10
        // rect.move(newX, newY)

        // copyStyleUI(f2)
    }

    app.draggingTool = f
    app.delTool = f

}

function copyStyle(field) {
    field.styleFont = upload.selectedField[0].styleFont
    field.fontSize = upload.selectedField[0].fontSize
    console.log("copyStyle >>>>>>>>>>>>> " + field.toolName + field.styleFont + ',' + field.fontSize)

    if (field.toolName == 'Signature' || field.toolName == 'Text' ||
        field.toolName == 'Date' || field.toolName == 'Dropdown' ||
        field.toolName == 'Attachment' || field.toolName == 'Name' || field.toolName == 'Email') {
        upload.show_set_detail = true
    } else {
        upload.show_set_detail = false
    }
    upload.chk_set_field = field.toolName
}

function copyStyleUI(field) {

    //memory
    upload.selectedField[0].styleFont = field.styleFont
    upload.selectedField[0].fontSize = field.fontSize

    //UI
    upload.size = field.fontSize
    upload.font = field.styleFont

    upload.chk_set_field = field.toolName


    if (field.toolName == 'Dropdown' && field.toolOptions) {
        upload.toolOptionsArray = field.toolOptions.split(',')
        upload.dropdownDefaultOption = field.dropdownDefaultOption
    }

    console.log("copyStyleUI >>>>>>>>>>>>> " + field.toolName + field.styleFont + ',' + field.fontSize)


}


function toEdoc(obj, imageAspectRatios) {
    var app = window.myApp
    var name = getUserField('FNAME') + " " + getUserField('LNAME')
    var e = getUsername()

    var ret = {
        originalFile: obj.originalFile, pdfFile: obj.pdfFile, status: obj.status, id: obj.pdfFile.replaceAll(".pdf", ""), imageFiles: [],
        width: 0, height: 0, signers: [], fields: [], subject: obj.subject, msg: obj.msg, owner: e
        , current_order: 1, use_signing_order: obj.use_signing_order, owner_name: app.get_name(), owner_position: app.owner_position,
        attachments: obj.attachments
    }

    //fields
    var fields = []
    for (var i = 0; i < obj.fields.length; i++) {
        var f = $.extend({}, obj.fields[i])
        f.group = undefined
        if (f.note_field) {
            f.note_field = undefined
        }
        f.status = 0
        f.n = i + 1
        //var signOrder = parseInt(f.signOrder)
        var signOrder = parseInt(f.person.order_set)
        f.signOrder = (isNaN(signOrder)) ? 1 : signOrder
        if (!f.isHidden) { ret.fields.push(f) }
    }

    //cloning
    // var cln = []
    var total_pages = app.imageFiles.length
    var total_tool = ret.fields.length
    var fId_counter = 0
    for (var i = 0; i < total_tool; i++) {
        var f = ret.fields[i]
        if (f.is_clone) {
            for (var j = 0; j < total_pages; j++) {
                console.log(f)
                if (j != f.page) {
                    var f2 = $.extend({}, f)
                    f2.page = j
                    var strDate = new Date().getTime() + ''
                    f2.id = fId_counter++
                    ret.fields.push(f2);
                }
            }
        }
    }

    // for (var i = 0; i < cln.length; i++) {
    //     ret.fields.push(cln[i])
    // }
    //end cloning

    //images
    for (var i = 0; i < obj.imageFiles.length; i++) {
        ret.imageFiles.push({ fileName: obj.imageFiles[i], aspectRatio: imageAspectRatios[i] })
    }


    //signers
    /*for (var i = 0; i < obj.people.length; i++) {
        ret.signers.push(obj.people[i])
    }*/

    //signers
    for (var i = 0; i < obj.people.length; i++) {
        var p = obj.people[i]

        if (p.least_sign && p.is_group == true) {
            p.concensus_id = "concen" + (new Date().getTime() + i)
            // p.concensus_idx =  "concen"+ (new Date().getTime() + i) 
            //p.concensusid = "1"
            //p.name2 = "cc"(new Date().getTime()+i)
            //p.name2 = "cc"(new Date().getTime()+i)

        }
        ret.signers.push(p)
    }

    //concensus
    //obj.concensus = (obj.people.filter(p => {return p.least_sign})).length>0?{}:undefined
    obj.concensus_list = (obj.people.filter(p => { return p.least_sign })).length > 0 ? [] : undefined
    //alert('concensus ' + obj.concensus)

    obj.people.forEach((p, i) => {
        if (p.least_sign) {
            obj.concensus_list.push({ concensus_id: p.concensus_id, at_least: p.amount_least_sign, total: p.user_group.length })
            //obj.concensus[p.concensus_id] = {concensus_id: p.concensus_id, at_least: p.amount_least_sign, total : p.user_group.length}
        }
    })

    ret.concensus_list = app.concensus_list
    ret.imageMerge = app.imageMerge
    return ret
}


function saveEdoc(doc) {
    var url = api_host + 'save_edoc_company_bla'
    var url_id = doc.id
    doc.is_template = upload.is_template
    doc.document_type_id = upload.selected_template
    doc.exp_date = upload.exp_date
    doc.sign_only = upload.sign_only
    doc.name = upload.user_name 
    // alert('before saveEdoc '+ doc.fields.length)
    doc.fields = filedToEdoc(doc.fields) 
    // alert('before saveEdoc '+ doc.fields.length)
    doc.draft_id = upload.draft_id
    doc.secret_level = upload.secret_level
    // doc.package_level = upload.myPackage.package_level
    //console.log(JSON.stringify(doc))
    //alert(JSON.stringify(doc))
   

    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(doc),
        success: function (result) {
            // gEvt(app.evtName , 'finish', 'Finished all steps')
            // router.push({ path: 'dashboard' })
            if (doc.sign_only !== true) {
                // location.href = document.location.origin + '/bla/#/dashboard'
                app.url_viewer = document.location.origin + '/bla/viewer.html?id=' + doc.id;
                app.next_step()
            } else {
                updateEdoc(url_id)
            }
        },
        error: function (error) {
            upload.status_finish_btn = true
            upload.alert_step_4 = true
            upload.text_alert = "Something went wrong! Please Log in"
            setTimeout(function () { upload.alert_step_4 = false }, 3000);
            // alert("Something went wrong!" + error);
        }
    })

}

function updateEdoc(id) {
    var that = upload;
    var url = window.api_host + 'update_edoc'
    var arr = upload.fields
    // var txt = (localStorage.lang_choose=="th")?"เอกสารยังไม่สมบูรณ์ ":"Not signed yet." 
    //    var arr  = that.sign_obj.fields.filter(function(field){ return field.email==getUsername() })
    var update_list = []
    arr.forEach(f1 => {
        f1.status = 1
        f1.group = null
        f1.activeSvg = null
        f1.master_group = null
        f1.draw = null
        update_list.push(f1)
    })

    var json = {
        fields: update_list,
        id: id,
        cert_pin: ''
    }

    window.update_list = update_list

    //alert(JSON.stringify(json))
    console.log(JSON.stringify(json))

    //alert('done1')
    //return ;


    //$('#openLoadding').click();
    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(json),
        success: function (result) {

            var res = JSON.parse(result)
            if (res.success) {
                // if(that.id_deal != "" || that.id_deal != undefined || that.id_deal != null){
                //   that.update_status_creden()
                // }
                //alert('done')
                var url = '../capi/pdf?file=' + id + '&stamp=true'
                window.open(url)
                //document.location.href = url;
                location.href = document.location.origin + '/bla/#/dashboard'

                //$('#closeLoadding').click();
                // that.canFinish = false 
                // on_saved()
            }
            else {
                alert(res.error_msg)
            }


        },
        error: function (error) {
            upload.status_finish_btn = true
            // hideWaiting() //got
            $('#closeLoadding').click();
            alert("Error : Something went wrong!");
        }
    })
}

function clone_field(oldField, pageIndex) {
    var newField = {
        "id": new Date().getTime(),
        "toolName": oldField.toolName,
        "toolData": oldField.toolData,
        "isShowed": false,
        "page": pageIndex,
        "email": oldField.selectedPerson,
        "person": oldField.person,
        "is_bold": oldField.is_bold,
        "is_underline": oldField.is_underline,
        "is_italics": oldField.is_italics,

        "styleFont": oldField.styleFont,
        "fontSize": oldField.fontSize,

        "signOrder": oldField.signOrder,
        "n": oldField.n,
        "field_no": oldField.field_no,
        "status": 0,
        "scale": oldField.scale
    }
    return newField

}

function leaveActiveSvg(svg) {
    var pageIndex = parseInt(svg.id.charAt(3))
    //console.table('leaveActiveSvg#',  pageIndex)
    var app = window.myApp

    if (app.draggingTool && !app.draggingTool.isShowed) {
        console.log('reset dragging tool')
        var oldField = app.draggingTool
        oldField.group.remove() //https://svgjs.com/docs/3.0/manipulating/#remove
        app.removeField(oldField)
        var newField = clone_field(oldField, pageIndex)
        app.addField(newField)
        app.draggingTool = newField
    }

}

function moveTool(svg, evt) {
    //console.log('moveTool')    
    var f = window.myApp.draggingTool
    var ele = evt.target;
    if (!f) return;
    if (!ele) return;

    var rect = f.group
    if (!rect.visible()) { rect.show() }

    var box = ele.getBoundingClientRect()

    //console.log(evt.clientX+','+evt.clientY+'clientXY')
    //console.log(box.x+','+box.y+'box.xy')
    //

    //

    var x = evt.clientX - box.x
    var y = evt.clientY - box.y
    //console.log(x+','+y)
    if (x < 20 || y < 20) {
        //console.table("++++++SKIPPED+++++++++++++++++",ele.id , evt.clientX, '-' , box.x , evt.clientY, box.y)
    }
    else {
        rect.move(x, y)
        //console.table(ele.id , evt.clientX,'-', box.x, evt.clientY, '-', box.y)
    }


}
