function init_vue() {
    
        
      
  var obj1 = upload_vue_original() //see. upload_new_3_lib.js
  var obj2 = upload_vue_lib() //see. upload_new_3_lib.js
  var obj_kit = upload_vue_kit() //see. upload_new_3_lib_kit.js
  $.extend(true, obj1, obj2)    
  $.extend(true, obj1, obj_kit)  
  return new Vue(obj1)
}


function upload_vue_kit()
{
  return {
        kit : 'a',
        is_cloning : false, //true if clone_this_field() is called/processing 
        methods : 
         {  activate : function(field){
                var oldField = this.delField
                if(oldField)
                {
                    var rect = oldField.group.find('rect')
                    rect.stroke({color: 'blue', width : 0}) 
                }   

                this.delField = field
                var rect = field.group.find('rect')
                rect.stroke({color: 'blue', width : 1})

                this.show_set_detail = true
                this.chk_set_field = field.toolName 

                console.log('activate >>>>>>>>>>  '+ field.id+','+field.toolName)
                upload.field_id = field.id
                upload.default_req_attach()
                // this.selectedPersonName = field.person.name
                console.log(field)
                if(field.person){                    
                    console.log(field.person)
                    console.log(field.person.selectedPersonIndex)
                    this.setSelectedPerson(field.person.selectedPersonIndex, field.person.email, field.person.name,field.person.title, field.person.position_id)
                }
                console.log('this.selectedPersonName = ', field)               
           },
            prepare_new_tool : function (mode,_field,_x,_y) {  
                // console.log(`prepare_new_tool mode ${mode}`)  
                var field = _field || this.draggingTool
                console.log('field.person >> ',field)
                console.log(field,_x,_y)
                if(!field)
                  {
                    //  console.log('prepare_new_tool skipped Reason#1')    
                     return; //user pick no tool
                  }
     
     
                  if(field.toolName=='Radio')
                    {
                        this.showRadio()
                        return;
                    }
     
                    if(field.toolName=='Radio2')
                    {
                        this.showRadio2()
                        return;
                    }
     
     
                  if(field.group)
                  {
                     console.log('prepare_new_tool skippedReason#2 >>> already have group')    
                     return; //user already have group install
                  }  
     
                var draw  = this.draw  
                var g = draw.group()
                g._toolName = field.toolName
                if(field.person){
                    color = this.getPersonColor(field.person.selectedPersonIndex)
                }else{
                    color = this.getPersonColor()
                }
                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;
                var rect = draw.rect(w,h).fill(color)
                 // var text = draw.text(field.toolName+this.fieldID())    
                 var text = draw.text(field.toolName)                                
                 g.add(rect)
                 g.add(text)
             //    g.add(draw.rect(70,70).fill(color)) 
             //    g.add(draw.text(field.toolName)) 
     
                //scaling
                if(!field.scale){
                    field.scale = 100                   
                }
                g._width1  = w
                g._width2  = g._width1*2
                g._width05 = parseInt(g._width1/2)
                               
                // start control size
                var cir1 = draw.circle(10,10).attr({ //left top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var cir2 = draw.circle(10,10).attr({ // right top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var cir3 = draw.circle(10,10).attr({ // left bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var cir4 = draw.circle(10,10).attr({ // right bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir1 = draw.circle(10,10).attr({ // right top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir2 = draw.circle(10,10).attr({ // right top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir3 = draw.circle(10,10).attr({ // right bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir4 = draw.circle(10,10).attr({ // right bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             g._rect = rect
             g._cir1 = cir1
             g._cir2 = cir2
             g._cir3 = cir3
             g._cir4 = cir4
             g._text = text
             
             // cir1.draggable()
             cir1.draggable()
             cir2.draggable()
             cir3.draggable()
             cir4.draggable()
             
             // set element first               
             cir1.center(rect.x(), rect.y())
             cir2.center(rect.x()+rect.width(), rect.y())              
             cir3.center(rect.x(), rect.y()+rect.height())                
             cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
             _cir1.hide()
             _cir2.hide()
             _cir3.hide()
             _cir4.hide()
     
             const ratio = rect.height()/rect.width()
             g._ratio = ratio
             cir1.on('dragstart.namespace', (evt) => { 
                 _cir1.center(rect.x(),rect.y())                                     
                 _cir1.show()                                       
                 cir1.hide()
             })
     
             cir1.on('dragmove.namespace', (evt) => {
                 var oldx = _cir1.x()
                 var oldy = _cir1.y()
                 //var oldP = _cir1.pointAt()
     
                 _cir1.center(rect.x(), rect.y())  
                 const wNew = cir4.x() - cir1.x()
                 //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir1.pointAt(_cir1);return;}
     
                 rect.size(wNew,wNew*ratio)
                 const rad = cir4.width()/2                                                          
                 rect.move(cir4.x()-rect.width()+rad,cir4.y()-rect.height()+rad) 
                 cir2.center(rect.x()+rect.width(),rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir4.y())/2)
             })
     
             cir1.on('dragend.namespace', (evt) => {
                 // rect.move(cir1.x(),cir1.y())
                 cir1.show()
                 _cir1.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
                 upload.update_scale(g)
                 this.update_loc(field)    
             })
     
             cir2.on('dragstart.namespace', (evt) => { 
                 _cir2.center(rect.x()+rect.width(), rect.y())                                     
                 _cir2.show()                                       
                 cir2.hide()
             })
     
             cir2.on('dragmove.namespace', (evt) => {
                 var oldx = _cir2.x()
                 var oldy = _cir2.y()
     
                 _cir2.center(rect.x()+rect.width(), rect.y()) 
                 const hNew = cir3.y()-cir2.y()
     
                 //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir2.move(oldx, oldy);return;}
     
                 const rad = cir3.width()/2
                 rect.move(cir3.x()+ rad,cir2.y() + rad) 
                 rect.size(hNew/ratio,hNew)
                 cir1.center(rect.x(),rect.y())
                 cir4.center(rect.x()+rect.width(),rect.y()+hNew)
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir3.y())/2)
             })
     
             cir2.on('dragend.namespace', (evt) => {
                 // rect.move(cir1.x(),cir1.y())
                 cir2.show()
                 _cir2.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
                 upload.update_scale(g)
                 this.update_loc(field)     
             })
     
             cir3.on('dragstart.namespace', (evt) => { 
                 _cir3.center(rect.x(),rect.y()+rect.height())                                     
                 _cir3.show()                                       
                 cir3.hide()
             })
     
             cir3.on('dragmove.namespace', (evt) => {
                 var oldx = _cir3.x()
                 var oldy = _cir3.y()
     
                 _cir3.center(rect.x(),rect.y()+rect.height()) 
                 const wNew = cir2.x()-cir3.x()
     
                // if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir3.move(oldx, oldy);return;}
     
                 const rad = cir3.width()
                 rect.move(cir3.x()+rad,cir2.y()+rad) 
                 rect.size(wNew,wNew*ratio)
                 cir1.center(rect.x(),rect.y())
                 cir4.center(rect.x()+rect.width(),rect.y()+rect.height())
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir3.y())/2)
             })
     
             cir3.on('dragend.namespace', (evt) => {
                 const { handler, box } = evt.detail
                 let { x, y } = box
                 // set element new
                 cir3.show()
                 _cir3.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
                 upload.update_scale(g)
                 this.update_loc(field)
             })
     
             cir4.on('dragstart.namespace', (evt) => { 
                 _cir4.center(rect.x()+rect.width(),rect.y()+rect.height())                                     
                 _cir4.show()                                       
                 cir4.hide()
             })
             
             cir4.on('dragmove.namespace', (evt) => {
                 var oldx = _cir4.x()
                 var oldy = _cir4.y()            
                 _cir4.center(rect.x()+rect.width(),rect.y()+rect.height())
                 const wNew = cir4.x()-cir1.x()
     
                 //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir4.move(oldx, oldy);return;}
     
                 rect.size(wNew, wNew *ratio)                   
                 cir2.center(rect.x()+rect.width(),rect.y())
                 cir3.center(rect.x(),rect.y()+rect.height()) 
                 _cir4.center(rect.x()+rect.width(),rect.y()+rect.height())
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir3.y())/2)
             })
     
             cir4.on('dragend.namespace', (evt) => {
                 const { handler, box } = evt.detail
                 let { x, y } = box
                 // set element new
                 // rect.move(cir1.x(),cir1.y())
                 cir4.show()
                 _cir4.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
                 // cir4.origin = {x:cir4.x(), y:cir4.y()} 
                 upload.update_scale(g)
                 this.update_loc(field)
             })
     
             
             g.add(cir1)
             g.add(cir2)
             g.add(cir3)
             g.add(cir4)
             // end control size

             g.on('click.namespace', (e) => {
                //  alert('prepare_new_tool.click')
             })

             g.on('dragstart.namespace', (e) => {
                // alert('prepare_new_tool.dragstart')
            })
     
             g.on('dragmove.namespace', (e) => {
                 const { handler, box } = e.detail
                 const constrains = document.getElementById('svg')
                 max_y = constrains.attributes.height.value
                 max_x = constrains.attributes.width.value
                 let { x, y } = box
                 if (x < 0) {
                     e.preventDefault()
                     x = 0
                 }
                 if (box.x2 > upload.adjustedWidth) {
                     //if (box.x2+70 > max_x) {    
                     e.preventDefault()
                     x = upload.adjustedWidth - box.w 
                 }
                 if (y < 0) {
                     e.preventDefault()               
                     y = 0
                 }
                 if (box.y2 > max_y) {
                     e.preventDefault()
                     y = max_y - box.h   
                 }
                 handler.move(x , y )
                 this.update_loc(field)
             })
     
                
                upload.attach_drag(g)   
                field.group = g
                g._field_id = field.id
                g_field = field
                this.activate(field)
     
                if(mode==2)
                {
                    g.hide()
                }  

                if(_x){
                    console.log(g,_x,_y)
                    g.move(_x, _y)
                    g.show()
                    upload.restore_scale(g)
                }
                // alert(1345)
     
                }  
    
       } //methods
    }   
}
