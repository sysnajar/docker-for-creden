Pages.Cert = {
    _template: 'certificate.html',
    template: '<div>this is certificate.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_certificate,
            cer_name: '',
            cer_path: '',
            file_name: '',
            list_cer: [],
            now_date: '',

        }
    },
    filters: {
        fomat_date(value) {
            // var res = value.split(" ");
            // var text = res[2] + '/' + res[0] + '/' + res[4]
            // return text || ""
            value = value || ''
            var date = new Date(value)
            var day = date.getDate()
            if (Number(day) <= 9) day = "0" + day
            var month = date.getMonth() + 1
            if (Number(month) <= 9) month = "0" + month
            var year = date.getFullYear()
            value = day + "/" + month + "/" + year
            return value || ""
        },
    },
    mounted: function () {
        window.myApp = this
        this.init_dropzone_cert()
        this.get_certificate()
    },
    methods: {
        gen_cert: function () {
            var pin = prompt("Please Enter 6 digits pin number")

            pin = parseInt(pin)

            if (isNaN(pin)) { alert("Please enter number"); return }
            pin = pin + ""

            if (pin && pin.length == 6) {
                var data = {
                    pin: pin
                }
                $.ajax({
                    type: "POST",
                    url: api_host + "gen_cert",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (resp) {

                        if (resp.success) {
                            alert("success")
                        }
                        else {

                        }

                    },
                    error: function (request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });

            }
        },
        get_certificate: function () {
            var that = this
            var data = {
                email: getUsername() || "N/A"
            }
            $.ajax({
                type: "POST",
                url: api_host + "get_certificate2",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        if (!that.list_cer.length) that.list_cer = []
                        var r = resp.data
                        var c = {
                            cer_id: r.id_cert,
                            cer_name: "Bangkok Life Assurance Certificate",
                            cer_start: r.start_date.trim(),
                            cer_exp: r.exp_date.trim(),
                        }
                        that.list_cer.push(c)
                    }
                    else {
                        console.log("not have certificate")
                    }

                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });

        },
        gotoDigital_detail: function () {
            router.push('digital_cert');
            if (window.innerWidth < 992) closeNav()
        },
        init_dropzone_cert: function () {
            var that = this
            var dropzone = new Dropzone(document.getElementById('file-dropzone-upload-certificate'), {
                url: api_host + 'upload',
                clickable: true,
                acceptedFiles: ".pfx",
                parallelUploads: 1
            });

            dropzone.on("success", function (file, json) {
                var res = JSON.parse(json)
                console.log(file);
                // that.cer_path = '../face/images/card/' + res.filename + '.png';
                that.cer_name = file.name
                that.file_name = res.filename
                var d = new Date()
                that.now_date = d.getDate() + '/' + (d.getMonth() + 1) + '/' + d.getFullYear()

                if (res) {
                    that.save_cer()
                }
            });
        },
        save_cer: function () {
            var that = this
            var data = { cer_owner: getUsername(), cer_name: that.cer_name, file_name: that.file_name, cer_date: that.now_date, cer_exp: '' }
            $.ajax({
                type: "POST",
                url: api_host + "save_upload_cer",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                    that.get_cer()
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        get_cer: function () {
            var that = this
            var data = { cer_owner: getUsername() }
            $.ajax({
                type: "POST",
                url: api_host + "get_certificate",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                    if (resp.data) {
                        that.list_cer = resp.data
                    }
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        remove_cer: function (id) {
            var that = this
            var data = { cer_owner: getUsername(), cer_id: id }
            $.ajax({
                type: "POST",
                url: api_host + "remove_cer",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                    that.get_cer()
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        }
    }
}

routes.push({ path: '/certificate', component: Pages.Cert })

var lang_certificate = {
    "digital_cer": { "th": "จัดการใบรับรองอิเล็กทรอนิกส์", "en": "Manage Certificate" },
    "account": { "th": "บัญชีของคุณ", "en": "Your Account" },
    "have_not": { "th": "คุณยังไม่ได้ออกใบรับรองอิเล็กทรอนิกส์ของครีเดน", "en": "You have not yet obtained Creden digital certificate." },
    "request_cer": { "th": "ออกใบรับรองอิเล็กทรอนิกส์ของครีเดน", "en": "Request Creden digital certificate" },
    "manage": { "th": "จัดการใบรับรองอิเล็กทรอนิกส์", "en": "Manage Digital Certificate" },
    "exp": { "th": "วันหมดอายุ", "en": "Expiration date" },
    "date": { "th": "สร้างเมื่อ", "en": "Create date" },
    "id_cert": { "th": "รหัสใบรับรองอิเล็กทรอนิกส์", "en": "Certificate ID" },
    "name_cert": { "th": "ชื่อใบรับรองอิเล็กทรอนิกส์", "en": "Certificate Name" },

}