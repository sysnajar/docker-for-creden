function init_vue() {
    
        
      
  var obj1 = upload_vue_original() //see. upload_new_3_lib.js
  var obj2 = upload_vue_lib() //see. upload_new_3_lib.js
  var obj_nut = upload_vue_nut() //see. upload_new_3_lib_kit.js
  $.extend(true, obj1, obj2)    
  $.extend(true, obj1, obj_nut)  
  return new Vue(obj1)
}


function upload_vue_nut()
{
  return {
        data: {
            template_id: gup('template_id') || '',
            sign_type: gup('sign_type') || 2,
        },
        mounted: function () {
            window.myApp = this
            window.upload = this
            if (this.template_id !== '') {
                this.getTemplate()
                this.get_group()
            }
        },
        methods : {
            getPersonColor: function (_i) {
               // alert(_i + ' ' + this.selectedPersonIndex)
                
                var colors = this.color || ['pink','yellow', 'blue', 'pink', 'green', 'grey', 'orange']
                //alert('getPersonColor : '+ this.color[0].color+ this.color[1].color+ this.color[2].color)
                var color_select = this.selectedPersonIndex
                if(!isNaN(_i)){
                    color_select = _i
                }

               
                var x12 = this.color[color_select]
                //alert('aaaaa : '+ _i + ' '+ x12.color)
                return x12
            },

            getTemplate () {
                console.log('getTemplate')
                that = this
                $.ajax({
                    type: "GET",
                    url: api_host + "get_template" + "?id=" + this.template_id,
                    dataType: "json",
                    success: function (res) {
                        console.log(res)
                        that.all_images = res.data.all_images
                        that.all_original_filenames = res.data.all_original_filenames
                        that.display_imageFiles = res.data.display_imageFiles
                        that.imageFiles = res.data.imageFiles
                        that.imageMerge = res.data.imageMerge
                        that.list_file_pdf = res.data.list_file_pdf
                        that.originalFile = res.data.originalFile
                        that.owner = getUsername()
                        that.pdfFile = res.data.pdfFile
                        that.select_sign_only(that.sign_type)
                    },
                    error: function (request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });
            },
            mousemove_prepare_new_tool : function(evt){ 
                console.log('mousemove_prepare_new_tool '+evt.x+','+evt.y+'\n'+ evt.clientX+','+evt.clientY)
                var svg = document.getElementById('svg')
                var xoffset = svg.parentNode.getBoundingClientRect().x
                xmin = xoffset
                xmax = xoffset + svg.parentNode.getBoundingClientRect().width
                var a = evt.x
                // if(app.draggingTool){
                //     var f = app.draggingTool
                //     var w = (f.toolName == 'Signature' || f.toolName == 'Initial') ? 70 : 150;
                //     a = a + w   
                //     if(a > xoffset + upload.adjustedWidth){
                //         evt.preventDefault()
                //         return;
                //     }                 
                // }               
                 var field = this.draggingTool
                 //this.draggingTool = null
    
                 if(!field)
                 {
                    console.log('  mousemove_prepare_new_tool skipped')    
                    return; //user pick no tool
                 }else{
                    var f = app.draggingTool
                    var w = (f.toolName == 'Signature' || f.toolName == 'Initial') ? 70 : 150;
                    a = a + w +10  
                    if(a > xoffset + upload.adjustedWidth){
                        evt.preventDefault()
                        return;
                    }  
                 }
                 const {x, y, clientX, clientY , offsetX , offsetY} = evt
                 //console.log(x,y , ' | ' ,clientX,clientY, '|' , offsetX , offsetY)
                 field.group.move(offsetX, offsetY)
            },
            attach_drag : function(g){
                that = this
                g.on('click', function(evt){ 
                //  alert('attach_drag')
                 var field = upload.field(this._field_id) 
                 if(!field){ console.log(">>>>>>> invalid fieldIdC "+ this._field_id);return;}
 
                 if (field.toolName == 'Signature' && that.userField.signature == '') {
                     that.signDlg(field, field)
                 } else if (field.toolName == 'Initial'  && that.userField.initial == '') {
                     that.signDlg(field, field)
                 }
 
                 upload.activate(field)
 
                 }) 
 
                g.draggable()  
                g.on('dragstart.namespace', function (evt) {
                 var field = upload.field(this._field_id) 
                 if(!field){ console.log(">>>>>>> invalid fieldIdB "+ this._field_id);return;}
 
                 upload.activate(field)
                 
                
             })
 
                g.on('dragmove.namespace', function (evt) {
                    const { handler, box } = evt.detail
                    let { x, y } = box
                    g.find("text").text(`${g._toolName}`)
                    //g.find("text").text(`${g._toolName}\n(${parseInt(x)},${parseInt(y)})\npage ${upload.compute_page(y)}`)
                  
                })
 
                g.on('dragend.namespace', function (evt) {
                 console.log('dragend done')
                 window.dend = this;
 
                 var field = upload.field(this._field_id)
                 if(!field){ console.log(">>>>>>> invalid fieldIdA "+ this._field_id) ;return;}
 
                 upload.update_loc(field)
               
             })
 
                return g
            },
        
            prepare_new_tool : function (mode) {  
                console.log(`prepare_new_tool mode ${mode}`)  
                var field = this.draggingTool
                
                if(!field)
                  {
                     console.log('prepare_new_tool skipped Reason#1')    
                     return; //user pick no tool
                  }
     
     
                  if(field.toolName=='Radio')
                    {
                        this.showRadio()
                        return;
                    }
     
                    if(field.toolName=='Radio2')
                    {
                        this.showRadio2()
                        return;
                    }
     
     
                  if(field.group)
                  {
                     console.log('prepare_new_tool skippedReason#2 >>> already have group')    
                     return; //user already have group install
                  }  
     
                var draw  = this.draw  
                var g = draw.group()
                g._toolName = field.toolName
                var color = this.getPersonColor()
                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;
                var rect = draw.rect(w,h).fill(color)
                 // var text = draw.text(field.toolName+this.fieldID())    
                 var text = draw.text(field.toolName)                                 
                 g.add(rect)
                 g.add(text)
             //    g.add(draw.rect(70,70).fill(color)) 
             //    g.add(draw.text(field.toolName)) 
     
                //scaling
                field.scale = 100
                g._width1  = 70
                g._width2  = g._width1*2
                g._width05 = parseInt(g._width1/2)
                
                
                // start control size
                var cir1 = draw.circle(10,10).attr({ //left top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var cir2 = draw.circle(10,10).attr({ // right top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var cir3 = draw.circle(10,10).attr({ // left bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var cir4 = draw.circle(10,10).attr({ // right bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir1 = draw.circle(10,10).attr({ // right top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir2 = draw.circle(10,10).attr({ // right top
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir3 = draw.circle(10,10).attr({ // right bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             var _cir4 = draw.circle(10,10).attr({ // right bottom
                 fill: "red",
                 stroke: "none",
                 opacity: .5
             })
             
             // cir1.draggable()
             cir1.draggable()
             cir2.draggable()
             cir3.draggable()
             cir4.draggable()
             
             // set element first               
             cir1.center(rect.x(), rect.y())
             cir2.center(rect.x()+rect.width(), rect.y())              
             cir3.center(rect.x(), rect.y()+rect.height())                
             cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
             _cir1.hide()
             _cir2.hide()
             _cir3.hide()
             _cir4.hide()
     
             const ratio = rect.height()/rect.width()
     
             cir1.on('dragstart.namespace', (evt) => { 
                 _cir1.center(rect.x(),rect.y())                                     
                 _cir1.show()                                       
                 cir1.hide()
             })
     
             cir1.on('dragmove.namespace', (evt) => {
                 var oldx = _cir1.x()
                 var oldy = _cir1.y()
                 //var oldP = _cir1.pointAt()
     
                 _cir1.center(rect.x(), rect.y())  
                 const wNew = cir4.x() - cir1.x()
                 //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir1.pointAt(_cir1);return;}
     
                 rect.size(wNew,wNew*ratio)
                 const rad = cir4.width()/2                                                          
                 rect.move(cir4.x()-rect.width()+rad,cir4.y()-rect.height()+rad) 
                 cir2.center(rect.x()+rect.width(),rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir4.y())/2)
             })
     
             cir1.on('dragend.namespace', (evt) => {
                 // rect.move(cir1.x(),cir1.y())
                 cir1.show()
                 _cir1.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
     
                 upload.update_scale(g)
                 
     
             })
     
             cir2.on('dragstart.namespace', (evt) => { 
                 _cir2.center(rect.x()+rect.width(), rect.y())                                     
                 _cir2.show()                                       
                 cir2.hide()
             })
     
             cir2.on('dragmove.namespace', (evt) => {
                 var oldx = _cir2.x()
                 var oldy = _cir2.y()
     
                 _cir2.center(rect.x()+rect.width(), rect.y()) 
                 const hNew = cir3.y()-cir2.y()
     
                 //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir2.move(oldx, oldy);return;}
     
                 const rad = cir3.width()/2
                 rect.move(cir3.x()+ rad,cir2.y() + rad) 
                 rect.size(hNew/ratio,hNew)
                 cir1.center(rect.x(),rect.y())
                 cir4.center(rect.x()+rect.width(),rect.y()+hNew)
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir3.y())/2)
             })
     
             cir2.on('dragend.namespace', (evt) => {
                 // rect.move(cir1.x(),cir1.y())
                 cir2.show()
                 _cir2.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
     
                 upload.update_scale(g)
     
             })
     
             cir3.on('dragstart.namespace', (evt) => { 
                 _cir3.center(rect.x(),rect.y()+rect.height())                                     
                 _cir3.show()                                       
                 cir3.hide()
             })
     
             cir3.on('dragmove.namespace', (evt) => {
                 var oldx = _cir3.x()
                 var oldy = _cir3.y()
     
                 _cir3.center(rect.x(),rect.y()+rect.height()) 
                 const wNew = cir2.x()-cir3.x()
     
                // if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir3.move(oldx, oldy);return;}
     
                 const rad = cir3.width()
                 rect.move(cir3.x()+rad,cir2.y()+rad) 
                 rect.size(wNew,wNew*ratio)
                 cir1.center(rect.x(),rect.y())
                 cir4.center(rect.x()+rect.width(),rect.y()+rect.height())
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir3.y())/2)
             })
     
             cir3.on('dragend.namespace', (evt) => {
                 const { handler, box } = evt.detail
                 let { x, y } = box
                 // set element new
                 cir3.show()
                 _cir3.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
                 upload.update_scale(g)
             })
     
             cir4.on('dragstart.namespace', (evt) => { 
                 _cir4.center(rect.x()+rect.width(),rect.y()+rect.height())                                     
                 _cir4.show()                                       
                 cir4.hide()
             })
             
             cir4.on('dragmove.namespace', (evt) => {
                 var oldx = _cir4.x()
                 var oldy = _cir4.y()            
                 _cir4.center(rect.x()+rect.width(),rect.y()+rect.height())
                 const wNew = cir4.x()-cir1.x()
     
                 //if(upload.exceed_scale_limit(g, wNew)){evt.preventDefault();_cir4.move(oldx, oldy);return;}
     
                 rect.size(wNew, wNew *ratio)                   
                 cir2.center(rect.x()+rect.width(),rect.y())
                 cir3.center(rect.x(),rect.y()+rect.height()) 
                 _cir4.center(rect.x()+rect.width(),rect.y()+rect.height())
                 text.center((cir1.x()+cir2.x())/2,(cir1.y()+cir3.y())/2)
             })
     
             cir4.on('dragend.namespace', (evt) => {
                 const { handler, box } = evt.detail
                 let { x, y } = box
                 // set element new
                 // rect.move(cir1.x(),cir1.y())
                 cir4.show()
                 _cir4.hide()
                 cir1.center(rect.x(), rect.y())
                 cir2.center(rect.x()+rect.width(), rect.y())
                 cir3.center(rect.x(), rect.y()+rect.height())
                 cir4.center(rect.x()+rect.width(), rect.y()+rect.height())
                 // cir4.origin = {x:cir4.x(), y:cir4.y()} 
     
                 upload.update_scale(g)
             })
     
             
             g.add(cir1)
             g.add(cir2)
             g.add(cir3)
             g.add(cir4)
             // end control size

             g.on('click.namespace', (e) => {
                //  alert('prepare_new_tool.click')
             })

             g.on('dragstart.namespace', (e) => {
                // alert('prepare_new_tool.dragstart')
            })
     
             g.on('dragmove.namespace', (e) => {
                 const { handler, box } = e.detail
                 const constrains = document.getElementById('svg')
                 max_y = constrains.attributes.height.value
                 max_x = constrains.attributes.width.value
                 let { x, y } = box
                 if (x < 0) {
                     e.preventDefault()
                     x = 0
                 }
                 if (box.x2 > upload.adjustedWidth) {
                     //if (box.x2+70 > max_x) {    
                     e.preventDefault()
                     x = upload.adjustedWidth - box.w 
                 }
                 if (y < 0) {
                     e.preventDefault()               
                     y = 0
                 }
                 if (box.y2 > max_y) {
                     e.preventDefault()
                     y = max_y - box.h   
                 }
                 handler.move(x , y )
                 this.update_loc(field)
             })
     
     
                upload.attach_drag(g)   
                field.group = g
                g._field_id = field.id
                g_field = field
                this.activate(field)
     
                if(mode==2)
                  {
                      g.hide()
                  }  
     
     
            },
             
    
       } //methods
    }   
}
