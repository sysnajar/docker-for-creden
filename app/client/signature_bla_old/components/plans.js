Pages.Plans = {
    _template: 'plans.html',
    template: '<div>this is plans.html</div>',
    data: function () {
        return {
            plan: [],
            package: {
                free: {
                    user_max: {
                        detail: "User Limitation",
                        value: "3 User"
                    },
                    doc_limit: {
                        detail: "Document Limitation",
                        value: "5 Documents"
                    },
                    bs_field: {
                        detail: "Basic Fields",
                    },
                    bs_admin: {
                        detail: "Basic Admin",
                    },
                    ts: {
                        detail: "Timestamp (TEDA Standard)",
                    },
                    dc_upload: {
                        detail: "Digital Certificate",
                        value: "5"
                    },
                    in_user: {
                        detail: "Internal User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                    ex_user: {
                        detail: "External User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                },
                basic: {
                    user_max: {
                        detail: "User Limitation",
                        value: "3 User"
                    },
                    storage: {
                        detail: "Storage Limitation",
                        value: "5 GB"
                    },
                    bs_field: {
                        detail: "Basic Fields",
                    },
                    bs_admin: {
                        detail: "Basic Admin",
                    },
                    doc_limit: {
                        detail: "Document Unlimited",
                    },
                    noti: {
                        detail: "Reminders & notifications"
                    },
                    ps_logo: {
                        detail: "Personalized Logo"
                    },
                    ts: {
                        detail: "Timestamp (TEDA Standard)",
                    },
                    dc_upload: {
                        detail: "Digital Certificate",
                        value: "5"
                    },
                    in_user: {
                        detail: "Internal User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                    ex_user: {
                        detail: "External User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                },
                advanced: {
                    user_max: {
                        detail: "User Limitation",
                        value: "10 User"
                    },
                    storage: {
                        detail: "Storage Limitation",
                        value: "20 GB"
                    },
                    doc_limit: {
                        detail: "Document Unlimited",
                    },
                    bs_field: {
                        detail: "Basic Fields",
                    },
                    bs_admin: {
                        detail: "Basic Admin",
                    },
                    sm_form: {
                        detail: "Document Template Upload",
                    },
                    app_fn: {
                        detail: "Approval Function",
                    },
                    doc_draft: {
                        detail: "Save Draft",
                    },
                    bs_sign_field: {
                        detail: "Basic Signer Fields",
                    },
                    advan_sign_field: {
                        detail: "Advance Signer Fields",
                    },
                    noti: {
                        detail: "Reminders & notifications"
                    },
                    ps_logo: {
                        detail: "Personalized Logo"
                    },
                    ts: {
                        detail: "Timestamp (TEDA Standard)",
                    },
                    dc_upload: {
                        detail: "Digital Certificate",
                        value: "5"
                    },
                    in_user: {
                        detail: "Internal User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                    ex_user: {
                        detail: "External User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                },
                premium: {
                    user_max: {
                        detail: "User Limitation",
                        value: "20 User"
                    },
                    storage: {
                        detail: "Storage Limitation",
                        value: "50 GB"
                    },
                    doc_limit: {
                        detail: "Document Unlimited",
                    },
                    bs_field: {
                        detail: "Basic Fields",
                    },
                    bs_admin: {
                        detail: "Basic Admin",
                    },
                    sm_form: {
                        detail: "Document Template Upload",
                    },
                    app_fn: {
                        detail: "Approval Function",
                    },
                    doc_draft: {
                        detail: "Save Draft",
                    },
                    bs_sign_field: {
                        detail: "Basic Signer Fields",
                    },
                    advan_sign_field: {
                        detail: "Advance Signer Fields",
                    },
                    noti: {
                        detail: "Reminders & notifications"
                    },
                    ps_logo: {
                        detail: "Personalized Logo"
                    },
                    ts: {
                        detail: "Timestamp (TEDA Standard)",
                    },
                    dc_upload: {
                        detail: "Digital Certificate",
                        value: "5"
                    },
                    in_user: {
                        detail: "Internal User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                    in_user_ial23: {
                        detail: "Internal User : IAL2.3 (Dip Chip + Selfie) + Digital Certificate",
                    },
                    ex_user: {
                        detail: "External User : Standard e-KYC (OCR + Selfie + DOPA Check)",
                    },
                },
            },
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_plan,
        }
    },
    filters: {
        firstUpper(txt) {
            if (txt == "free") txt = "Free"
            else if (txt == "basic") txt = "Basic"
            else if (txt == "advanced") txt = "Advanced"
            else if (txt == "premium") txt = "Premium"
            return txt
        },
        dateThai(date) {
            var d = new Date(date)
            var txt = d.toLocaleString('th-TH', {
                year: "numeric",
                month: "short",
                day: "numeric",
            })
            return txt
        },
    }, 
    mounted: function () {
        window.myApp = this
        this.get_my_plan()
    },
    methods: {
        get_my_plan: function () {
            var that = this
            var data = {}
            $.ajax({
                type: "POST",
                url: api_host + "get_my_plan",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {

                    if (resp.success) {
                        var p = resp.data
                        var gb1 = 1073741824
                        // p = {}
                        // p.status = false

                        if (p.storage && !p.storage.length) p.storage = []
                        if (p.advance_func == true || p.storage && p.storage.length > 0) p.use_add_on = true
                        if (p.status == true) {

                            p.features = that.package[p.package || "free"]

                            // Storage Cal
                            if (p.package == "basic") p.max_storage = 5
                            else if (p.package == "advanced") p.max_storage = 20
                            else if (p.package == "premium") p.max_storage = 50
                            for (i = 0; i < p.storage.length; i++) {
                                p.max_storage = p.max_storage + Number(p.storage[i].size)
                            }
                            p.max_storage_byte = (Number(p.max_storage) * gb1)
                            p.use_storage_percent = ((p.storage_used * 100) / p.max_storage_byte).toFixed(2)
                            p.use_storage_gb = formatBytes(p.storage_used || 0, 2)

                            // Document
                            p.max_doc = "UNLIMIT"
                            p.use_doc_percent = 100

                            // Member
                            p.use_members_percent = ((p.member_used * 100) / Number(p.users))

                            that.plan = p
                        }
                        else {
                            p.max_storage = 0
                            p.max_storage_byte = 0
                            p.use_storage_percent = 100
                            p.use_storage_gb = 0.00

                            p.max_doc = 5
                            p.use_doc_percent = ((p.doc_used * 100) / Number(p.max_doc))
                            p.use_members_percent = ((p.member_used * 100) / Number(p.users))

                            p.package = "free"
                            p.features = that.package["free"]
                            that.plan = p
                        }
                    }
                    else {

                    }
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        goto_pricing: function () {
            window.open("/bla/web/pricing.html?rd=" + new Date().getTime())
        }
    }
}

routes.push({ path: '/plans', component: Pages.Plans })

var lang_plan = {
    "title": { "th": "ราคาและแผนการใช้งาน", "en": "Pricing and Plan" },
    "your_pack": { "th": "แพ็กเกจที่คุณใช้งานอยู่", "en": "Your Active Package" },
    "start_active": { "th": "คุณทำการ Activate Account วันที่ ...", "en": "Your package is activated on Jan 1, 2021" },
    "pack_exp": { "th": "สามารถใช้งานได้ถึงวันที่ .....", "en": "Your package is active until .....	" },
    "upgrade": { "th": "อัพเกรดเพื่อเพิ่มฟีเจอร์ใหม่ๆ", "en": "Upgrade for more features" },
    "check_plan": { "th": "ดูแผนการใช้งาน", "en": "Check Creden Plan" },
    "doc_totle": { "th": "จำนวนเอกสารทั้งหมด", "en": "Total of documents" },
    "storage": { "th": "พื้นที่เก็บข้อมูล", "en": "Data Storage" },
    "used_title": { "th": "การใช้งาน", "en": "Current Plan Usage" },
    "count_user": { "th": "จำนวนผู้ใช้งาน", "en": "Total of Active Users" },
    "kyc": { "th": "การยืนยันตัวตน (e-KYC)", "en": "Identity Verification (e - KYC)" },
    "have_feature": { "th": "คุณมีฟีเจอร์อะไรบ้าง", "en": "What features do you have?" },
    "count_storage": { "th": "จำนวนพื้นที่เก็บข้อมูลสะสม", "en": "Cumulative Storage Capacity" },
    "dopa": { "th": "Creden Standard e-KYC (DOPA Check)", "en": "Creden Standard e-KYC (DOPA Check)" },
    "add_on": { "th": "แพ็คเกจเสริม", "en": "Additional Package" },
    "up_main": { "th": "อัพเกรดแพ็กเกจหลัก", "en": "Upgrade Main Package" },
    "buy_main": { "th": "ซื้อแพ็กเกจหลัก", "en": "Buy Main Package" },
    "buy_addon": { "th": "ซื้อแพ็กเกจเสริม", "en": "Buy Additional Package" },
    "used": { "th": "ใช้ไป", "en": "Used" },
    "from": { "th": "จาก", "en": "from" },

}
