Pages.Profile = {
    _template: 'profile.html',
    template: '<div>this is profile.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_profile,
            lastupdateekyc:'',
            cardreader_id:'',
            img_profile: '',
            user_email: getUsername(),
            fname: '',
            lname: '',
            mname: '',
            password: '',
            new_password: '',
            con_password: '',
            sign_type: '',
            sign_name: '',
            user_signature: [{ draw: '', gen: '', initial: '' }],
            draw: true,
            gen: true,
            initial: true,
            gen_initial: true,
            file_name_pic: '',
            alert: false,
            msg_alert: 'aaa',
            alert_success: true,
            sign_name_initial: '',
            font_initia: '',
            font_sing: '',
            user_phone: '',
            hide_phone: '',
            use_namber_phone: undefined,
            otp_confirm: '',
            token: '',
            change_pass: false,
            check_social_register: false,
            hash_pass: '',
            list_phone_number: [],
            new_number: false,
            new_user_phone: '',
            hide_new_phone: '',
            show_old_pass: false,
            show_pass: false,
            show_con_pass: false,
            change_pass_social:false,
            con_otp_mail:'',
            spiner:false,
            lineQR: '',
            select_sign:'',
            select_init:'',
            current_qr:'0',
            chk_kyc_status:'',
            can_kyc:'',
            verified_ekyc:false,
            cert:{},

        }
    },
    mounted: function () {
        // openLeftMenu()
        window.myApp = this
        this.init_dropzone_profile()
        this.get_signature_img()
        this.get_profile()
        this.setFormProfile()
        this.get_cer_info()
        // window.signaturePad = new SignaturePad(document.getElementById('signature-pad'), {
        //     backgroundColor: 'rgba(255, 255, 255, 0)',
        //     penColor: 'rgb(0, 0, 0)'
        // });
        // window.signaturePadInitial = new SignaturePad(document.getElementById('signature-pad-initial'), {
        //     backgroundColor: 'rgba(255, 255, 255, 0)',
        //     penColor: 'rgb(0, 0, 0)'
        // });
    },
    methods: {
        setFormProfile: function () {

            var that = this

            $.ajax({
                type: "POST",
                url: api_host + "check_kyc_esig",
                data: JSON.stringify({ adminEmail: getUsername() }),
                dataType: "json",
                success: function (data) {
                   that.chk_kyc_status = data.data.verified_ekyc

                    if (data.data.verified_ekyc == false) {
                       
                        that.textkyc = "ยืนยันตัวตน"
                     
                    } else {
                        
                        that.textkyc = "อัพเดทการยืนยันตัวตน"
                       
                    }
                    //lastupdate ekyc
                    var lastupdate_ekyc = data.data.last_kyc_dtm
                    that.lastupdateekyc = lastupdate_ekyc
                    that.cardreader_id = data.data.id
                    that.verified_ekyc = data.data.verified_ekyc
                  
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        goKyc_dipchip:function(){
            // alert(window.screen.width * window.devicePixelRatio )
           
             var that = this
             document.getElementById('ref_no').value = getUsername()
             document.getElementById('txid').value = new Date().getTime();
             document.getElementById('redirect_url').value = document.location.origin + '/bla/#/ekyc';
             document.getElementById('new_parameter').value = that.type;
             document.getElementById('card_reader_id').value = that.cardreader_id
             document.getElementById('adminEmail').value = getUsername()

            
             var dipchip = that.verified_ekyc
            if(dipchip == true && that.cardreader_id != null){
                sessionStorage.setItem('dipchip','true')
            }

            if(dipchip == false){
                sessionStorage.setItem('dipchip','false')
            }
            
            // if(sessionStorage.getItem('dipchip')== true){
            //     this.status_dipchip = 'dipchip'
            // }
           
            //  document.getElementById('ref_no').value = this.adminEmail
            //  document.getElementById('txid').value = new Date().getTime();
            //  document.getElementById('adminEmail').value = getUsername()
            //  document.getElementById('redirect_url').value = document.location.origin + '/bla/#/ekyc';
             document.ekycForm.action = document.location.origin + '/capi/ekyc_auth_self_dipchip'
             document.ekycForm.submit(); 
             
         },
        init_dropzone_profile: function () {
            var that = this
            var dropzone = new Dropzone(document.getElementById('file-dropzone-upload-profile'), {
                url: api_host + 'upload',
                clickable: true,
                acceptedFiles: ".jpg, .jpeg, .png",
                parallelUploads: 1
            });

            dropzone.on("success", function (file, json) {
                var res = JSON.parse(json)
                that.img_profile = '../face/images/card/' + res.filename + '.png';
                that.file_name_pic = res.filename;
            });
        },
        createSignature: function () {
            var that = this

            that.sign_type = 'draw';
            var data = signaturePad.toDataURL('image/png');
            data = data.split(',')
            $.ajax({
                type: "POST",
                url: "../capi/create_signature",
                data: JSON.stringify({ "img": data[1], "username": getUsername(), "code": getUsername(), "sign_type": "draw", "field_id": '' }),
                dataType: "json",
                success: function (data) {
                    var path = '../edoc/app-assets/images/signature/' + getUsername() + '_draw.png?rd=' + new Date().getTime()
                    that.user_signature[0].draw = path
                    that.draw = true
                    that.clearSignature(1)
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        createSignature_initial: function () {
            var that = this
            that.sign_type = 'draw_initial';
            var data = signaturePadInitial.toDataURL('image/png');
            data = data.split(',')
            $.ajax({
                type: "POST",
                url: "../capi/create_signature_initial",
                data: JSON.stringify({ "img": data[1], "username": getUsername(), "code": getUsername(), "sign_type": "draw_initial", "field_id": '' }),
                dataType: "json",
                success: function (data) {
                    var path = '../edoc/app-assets/images/signature/' + getUsername() + '_draw_initial.png?rd=' + new Date().getTime()
                    that.user_signature[0].initial = path
                    that.initial = true
                    that.clearSignature(2)

                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        clearSignature: function (n) {
            if (n == 1 || n == '1') {
                signaturePad.clear();
            } else if (n == 2 || n == '2') {
                signaturePadInitial.clear()
            }

        },
        get_cer_info:function(){
            var that = this
            var data = {}
            $.ajax({
                type: "POST",
                url: api_host + "get_cer_info_BLA",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (resp) {
                   if(resp.success){
                        that.cert = resp.cert
                        that.can_kyc = resp.can_kyc
		   
		   }

                   if(resp.success && resp.have_cert){
                       // var a = that.$options.filters.trim_serial_number(that.cert.serial_number)
                        // if(that.user_email == "bla04@creden.co"){
                           // var expire_date = that.cert.expire_date
                          
                            var expire_date = that.cert.expire_date
                            var expire_day = expire_date.substring(0,2)
                            var expire_month = expire_date.substring(3,5)
                            var expire_year = expire_date.substring(6,10)

                            var user_day = expire_day
                            var user_month = expire_month
                            var user_year = expire_year


                            var current_day = new Date().getDate()
                            var current_month = new Date().getMonth()
                            var current_year = new Date().getFullYear()
                            
                            var oneDay = 24 * 60 * 60 * 1000; // hours*minutes*seconds*milliseconds
                            var update_date = new Date(user_year, user_month, user_day);
                            var current_date = new Date(current_year, current_month, current_day);
                            var total = Math.round(Math.abs((update_date - current_date) / oneDay));
                            console.log(total)
                            // var d = new Date()
                            // var day = d.getDate() 
                            // total = (day1%day)
                            // console.log(total)
                        
                            // limit_day = 15
                            // total = (day%day1)
                              if(total == 0 ||  total <= 15) {
                              
                                 that.send_email_notix()
                                 console.log('send')
                              }
                            
                          
                            
                        // }else{

                        // }
                        
                    }
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        send_email_notix: function () {
            var that = this
            
           
            data = { "adminEmail": getUsername()}
            $.ajax({
                type: "POST",
                url: api_host + "send_mail_noti_BLA_NEW",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    var path = '../edoc/app-assets/images/signature/' + getUsername() + '_gen.png?rd=' + new Date().getTime()
                    that.user_signature[0].gen = path
                    that.gen = true
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        genSignature: function () {
            var that = this
            that.sign_type = 'gen';
            var url = "../capi/gen_signature"
            if (that.font_sing == '') {
                that.font_sing = 'worasait.ttf'
            }
            data = { "name": that.sign_name, "code": getUsername(), "lang": 'en', "font_name": that.font_sing, "field_id": '' }
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    var path = '../edoc/app-assets/images/signature/' + getUsername() + '_gen.png?rd=' + new Date().getTime()
                    that.user_signature[0].gen = path
                    that.gen = true
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        genSignature_initial: function () {
            var that = this
            that.sign_type = 'gen';
            if (that.font_initia == '') {
                that.font_initia = 'worasait.ttf'
            }
            var url = "../capi/gen_signature_initial"
            data = { "name": that.sign_name_initial, "code": getUsername(), "lang": 'en', "font_name": that.font_initia, "field_id": '' }
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    var path = '../edoc/app-assets/images/signature/' + getUsername() + '_gen_initial.png?rd=' + new Date().getTime()
                    that.user_signature[0].gen_initial = path
                    that.gen_initial = true
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        get_signature_img: function () {
            var that = this
            that.user_signature[0].draw = "/edoc/app-assets/images/signature/" + getUsername() + "_draw.png?rd=" + new Date().getTime()
            that.user_signature[0].gen = "/edoc/app-assets/images/signature/" + getUsername() + "_gen.png?rd=" + new Date().getTime()
            that.user_signature[0].initial = "/edoc/app-assets/images/signature/" + getUsername() + "_draw_initial.png?rd=" + new Date().getTime()
            that.user_signature[0].gen_initial = "/edoc/app-assets/images/signature/" + getUsername() + "_gen_initial.png?rd=" + new Date().getTime()
        },
        hide_img_sign: function (p) {
            var that = this
            if (p == 1 || p == '1') {
                if(that.select_sign ==  '_draw.png' ){
                    that.select_sign = ''
                    that.set_signature()
                }
                that.draw = false
            } else if (p == 2 || p == '2') {
                if(that.select_sign ==  '_gen.png' ){
                    that.select_sign = ''
                    that.set_signature()
                }
                that.gen = false
            } else if (p == 3 || p == '3') {
                if(that.initial ==  '_draw_initial.png'){
                    that.initial = ''
                    that.set_signature()
                }
                that.initial = false
            } else if (p == 4 || p == '4') {
                if(that.initial ==  '_gen_initial.png'){
                    that.initial = ''
                    that.set_signature()
                }
                that.gen_initial = false
            }
        },
        save_profile: function () {
            var that = this
            var data = { adminEmail: getUsername(), profile_pic: that.file_name_pic, adminFisrtname: that.fname, adminLastname: that.lname, adminMiddlename: that.mname, phone_number: that.user_phone }
            $.ajax({
                type: "POST",
                url: api_host + "set_profile_user",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    that.alert = true
                    if (res.success) {
                        that.alert_success = true
                        that.msg_alert = that.lang_txt.save_suc[that.lang_choose]
                        that.get_profile()
                    } else {
                        if (res.error_code == 'P00') {
                            document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
                        } else if (res.error_code == 'P02') {
                            that.alert_success = false
                            that.msg_alert = that.lang_txt.save_not_suc[that.lang_choose]
                        }
                    }

                },
                error: function (request, status, error) {
                    console.log("|" + status + ' |' + error);
                }
            });
        },
        edit_password: function (m) {
            var that = this
            if (that.new_password.length < 8 || that.con_password.length < 8) {
                that.alert = true
                that.alert_success = false
                that.msg_alert = that.lang_txt.pass_less_alert[that.lang_choose]
            } else if (that.new_password != that.con_password) {
                that.alert = true
                that.alert_success = false
                that.msg_alert = that.lang_txt.pass_not_match[that.lang_choose]
            } else {
                var data = { user: getUsername(), password: that.new_password, befor_pass: that.password, mode: m }
                $.ajax({
                    type: "POST",
                    url: api_host + 'reset_password',
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        console.log(res)
                        that.alert = true
                        if (res.success) {
                            that.alert_success = true
                            that.msg_alert = that.lang_txt.savepass_suc[that.lang_choose]
                            that.change_pass = false
                            that.save_profile()
                        } else {
                            if (res.error_code == 'P00') {
                                document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
                            } else if (res.error_code == 'P02') {
                                that.alert_success = false
                                that.msg_alert = that.lang_txt.savepass_not_suc[that.lang_choose]
                            } else if (res.error_code == 'P03') {
                                that.alert_success = false
                                that.msg_alert =  that.lang_txt.old_pass_invalid[that.lang_choose]
                            } else if (res.error_code == 'P04') {
                                that.alert_success = false
                                that.msg_alert = that.lang_txt.cant_change_pass[that.lang_choose]
                            }

                        }

                    },
                    error: function (request, status, error) {
                        console.log("|" + status + ' |' + error);
                    }
                });
            }
        },
        chk_format_password: function () {
            var that = this
            var pw = that.new_password.toString()

            if (pw.length >= 8) document.getElementById("pass_spt").style.color = "#219653";
            else document.getElementById("pass_spt").style.color = "#828282"

            var upper = /(?=.*[A-Z])/
            if (upper.test(pw)) document.getElementById("pass_t").style.color = "#219653"
            else document.getElementById("pass_t").style.color = "#828282"

            var num = /(?=.*[0-9])/
            if (num.test(pw)) document.getElementById("pass_num").style.color = "#219653"
            else document.getElementById("pass_num").style.color = "#828282"


            if (pw.length >= 8 && upper.test(pw) && num.test(pw)) {
                return true
            }
            else {
                return false
            }
        },
        chk_format_password_con: function () {
            var that = this
            var pw = that.con_password.toString()

            if (pw.length >= 8) document.getElementById("pass_spt_c").style.color = "#219653";
            else document.getElementById("pass_spt_c").style.color = "#828282"

            var upper = /(?=.*[A-Z])/
            if (upper.test(pw)) document.getElementById("pass_t_c").style.color = "#219653"
            else document.getElementById("pass_t_c").style.color = "#828282"

            var num = /(?=.*[0-9])/
            if (num.test(pw)) document.getElementById("pass_num_c").style.color = "#219653"
            else document.getElementById("pass_num_c").style.color = "#828282"


            if (pw.length >= 8 && upper.test(pw) && num.test(pw)) {
                return true
            }
            else {
                return false
            }
        },
        get_profile: function () {
            var that = this
            $.ajax({
                type: "POST",
                url: api_host + "check_kyc_esig",
                data: JSON.stringify({ adminEmail: getUsername() }),
                dataType: "json",
                success: function (data) {
                    //    console.log(data);
                    that.fname = data.data.adminFisrtname || ''
                    that.lname = data.data.adminLastname || ''
                    that.mname = data.data.adminMiddlename || ''
                    that.select_sign = data.data.signature || ''
                    that.select_init = data.data.initial || ''

                    if (data.data.comTel.charAt(0) != "0") data.data.comTel = "0" + data.data.comTel
                    that.user_phone = data.data.comTel || ''
                    that.check_social_register = data.data.is_gamil || data.data.is_fb || false
                    that.hash_pass = data.data.hashedPassword || ''
                    that.list_phone_number = data.data.list_phone_number || []

                    if (that.user_phone.length == 10) {
                        that.hide_phone = that.user_phone.substring(0, 3) + '*****' + that.user_phone.substring(8);
                    }



                    var a = JSON.parse(localStorage.getItem('register_obj'))
                    a.profile_pic = data.data.profile_pic
                    localStorage.setItem('register_obj', JSON.stringify(a))

                    that.img_profile = that.getProfile()
                    navApp.myProfile = that.img_profile
                    that.file_name_pic = data.data.profile_pic

                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        change_img_profile: function () {
            var that = this
            that.img_profile = 'images/profile.png'
        },
        getProfile: function () {
            var p = getUserField("profile_pic") || ""
            var f = p.search("http")
            var img = ""

            if (f == 0) img = p
            else if (f == -1 && p != "") img = "../face/images/card/" + getUserField("profile_pic") + ".png"
            else img = "images/profile.png"
            return img
        },
        close_alert: function () {
            var that = this
            that.alert = false
        },
        cancle_pass: function () {
            var that = this
            that.password = ''
            that.new_password = ''
            that.con_password = ''
        },
        remove_sing: function (t) {
            var that = this
            var data = { file_name: getUsername() + t }

            var r = confirm(that.lang_txt.wantto_delsign[that.lang_choose]);
            if (r == true) {
                $.ajax({
                    type: "POST",
                    url: api_host + "remove_sing",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        if(that.select_sign ==  'draw' ){
                            that.select_sign = ''
                            that.set_signature()
                        }else if(that.select_sign ==  'gen' ){
                            that.select_sign = ''
                            that.set_signature()
                        }else if(that.select_init ==  'draw_initial'){
                            that.select_init = ''
                            that.set_signature()
                        }else if(that.select_init ==  'gen_initial'){
                            that.select_init = ''
                            that.set_signature()
                        }
                       that.get_signature_img()

                    },
                    error: function (request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });
            } else {
                return false
            }

        },
        use_number: function (u) {
            var that = this
            that.use_namber_phone = u
            // that.requestOTP_mail()
            if (u) {
                that.requestOTP_mobile()
            } else {
                that.requestOTP_mail()
            }


        },
        save_form: function () {
            var that = this
            if (that.password != '' || that.new_password != '' || that.con_password != '') {
                that.edit_password()
            } else {
                that.save_profile()
            }

        },
        requestOTP_mail: function () {
            var that = this;
            var a = { use_number: that.use_namber_phone, number_phone: that.user_phone }
            that.list_phone_number.push(a)
            var data = { user: getUsername(), list_phone_number: that.list_phone_number }
            // console.log(data);
            $.ajax({
                type: "POST",
                url: api_host + "request_otp_change_moblie",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (data) {
                    console.log(data);
                    that.token = data.token
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);

                }
            });

        },
        requestOTP_mobile: function () {
            var that = this
            var data = { tel: that.user_phone }

            $.ajax({
                type: "POST",
                url: api_host + "request_otp",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    that.new_number = res.success
                    that.token = res.token
                    that.otp_confirm = ''
                    that.hide_new_phone = that.user_phone.substring(0, 3) + '*****' + that.user_phone.substring(8);
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);

                }
            });


        },
        verify_otp_mail: function (m) {
            var that = this
            var data = { user: getUsername(), otp: that.otp_confirm, number_phone: that.new_user_phone, mode: m, token: that.token }
            $.ajax({
                type: "POST",
                url: api_host + "ver_otp_edit_number_phone",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    if (res.success) {
                        if (res.error_code == 'P005') {
                            that.new_number = 'new'
                        } else if (res.error_code == 'P006') {
                            $('#editphoneModal').modal('hide')
                            that.get_profile()
                        }

                    }

                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);

                }
            });
        },
        verify_otp_mail2: function () {
            var that = this
            var data = { user: getUsername(), otp: that.otp_confirm, number_phone: that.user_phone, mode: 'mb', token: that.token }
            $.ajax({
                type: "POST",
                url: api_host + "ver_otp_edit_number_phone",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    if (res.success) {
                        that.use_namber_phone = undefined,
                            that.new_number = false,
                            $('#editphoneModal').modal('hide')
                        that.get_profile()
                        //   if(res.error_code == 'P005'){
                        //     that.new_number = 'new'
                        //   }else if(res.error_code == 'P006'){

                        //   }

                    }

                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);

                }
            });
        },
        verify_otp_mail_pass:function(){
            var that = this
            var data = {token:that.token,otp:that.con_otp_mail}
            $.ajax({
                type: "POST",
                url: api_host + "verify_otp_email",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {

                    if(res.success){
                        that.change_pass = res.success
                        that.change_pass_social = res.success
                        $('#otpChangePassModal').modal('hide')
                    }else{
                        alert('not math')
                    }
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);

                }
            });

        },
        request_otp_mobile: function () {
            var that = this
            var data = { tel: that.new_user_phone }

            $.ajax({
                type: "POST",
                url: api_host + "request_otp",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    that.new_number = res.success
                    that.token = res.token
                    that.otp_confirm = ''
                    that.hide_new_phone = that.new_user_phone.substring(0, 3) + '*****' + that.user_phone.substring(8);
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);

                }
            });


        },
        change_password_social: function () {
            var that = this
            that.spiner = true
            var data = { user: getUsername(), password: that.new_password, befor_pass: that.password, mode: 'otp' }
            if (that.check_social_register && that.hash_pass == '' ) {
                // && that.hash_pass == ''
                $.ajax({
                    type: "POST",
                    url: api_host + "reset_password",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        //   console.log(res);
                        that.change_pass = res.success
                        if(res.success){
                            $("#otpChangePassModal").modal("show");
                            that.token = res.token
                            that.spiner = false
                        }
                    },
                    error: function (request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });
            } else {
                that.change_pass = true
                that.spiner = false
            }

        },
        connect_line() {
            that = this
            const data = {"email": getUsername()}
            $.ajax({
                type: "POST",
                url: api_host + "line_connect",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    if(res.success){
                        $("#lineConnectModal").modal("show");
                        that.lineQR = res.qr
                    }
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        show_oldpass: function (a) {
            var that = this
            if (a == 1) {
                that.show_old_pass = true
            } else {
                that.show_old_pass = false
            }
        },
        showpass: function (a) {
            var that = this
            if (a == 1) {
                that.show_pass = true
            } else {
                that.show_pass = false
            }
        },
        show_conpass: function (a) {
            var that = this
            if (a == 1) {
                that.show_con_pass = true
            } else {
                that.show_con_pass = false
            }
        },
        close_div_resetpass: function () {
            var that = this
            that.change_pass = false
            that.password = ''
            that.new_password = ''
            that.con_password = ''
            that.show_old_pass = false
            that.show_pass = false
            that.show_con_pass = false

        },
        open_div_resetpass: function () {
            var that = this
            that.change_pass = true
        },
        set_signature:function(){
            var that = this
            var data = {adminEmail: getUsername(),signature:that.select_sign,initial:that.select_init}
            $.ajax({
                type: "POST",
                url: api_host + "set_signature",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    that.get_profile()
                },
                error: function (request, status, error) {
                    alert("|" + status + ' |' + error);
                }
            });
        },
        next_step_qr: function () {
            var that = this
            
            if(that.current_qr == "0"){
                that.current_qr = "1"
            }
            else{
                that.current_qr = "0"
            }

        },
    }
}
function AddDays (days) {
    var today = new Date();
    var resultDate = new Date(today);
    resultDate.setDate(today.getDate()+days);
    return resultDate;
}
routes.push({ path: '/profile', component: Pages.Profile })

var lang_profile = {
    "profile_title": { "th": "ข้อมูลส่วนตัว", "en": "Profile" },
    "your_profile": { "th": "บัญชีของคุณ", "en": "Your Creden Account" },
    "img_profile": { "th": "รูปโปรไฟล์", "en": "Profile Picture" },
    "email": { "th": "อีเมล", "en": "Email" },
    "phone": { "th": "เบอร์โทรศัพท์", "en": "Phone" },
    "fname": { "th": "ชื่อ", "en": "First Name" },
    "lname": { "th": "นามสกุล", "en": "Last Name" },
    "mname": { "th": "ชื่อกลาง", "en": "Middle Name" },
    "save": { "th": "บันทึก", "en": "Save" },
    "cancel": { "th": "ยกเลิก", "en": "Cancel" },
    "your_sig": { "th": "ลายเซ็นของคุณ", "en": "Your Signature" },
    "your_initi": { "th": "ลายเซ็นแบบย่อของคุณ", "en": "Your Initials Sign" },
    "delete": { "th": "ลบ", "en": "Delete" },
    "create_sig": { "th": "สร้างลายเซ็น", "en": "Create Signature" },
    "pass_title": { "th": "รหัสผ่านและความปลอดภัย", "en": "Password & Security" },
    "change_pass": { "th": "เปลี่ยนรหัสผ่าน", "en": "Change password" },
    "now_pass": { "th": "รหัสผ่านเดิม", "en": "Old password" },
    "new_pass": { "th": "รหัสผ่านใหม่", "en": "New password" },
    "confirm_new_pass": { "th": "ยืนยันรหัสผ่านใหม่", "en": "Confirm new password" },
    "edit": { "th": "แก้ไข", "en": "Edit" },
    "q_change_phone": { "th": "คุณต้องการแก้ไขเบอร์โทร?", "en": "Do you confirm to edit the phone number?" },
    "confirm_phone": { "th": "กรุณายืนยันว่าโทรศัพท์มือถือเดิมหมายเลข", "en": "Please confirm whether this is your registered mobile number" },
    "still_active": { "th": "ยังใช้งานได้อยู่หรือไม่?", "en": "Is this still active?" },
    "inactive": { "th": "ใช้งานไม่ได้แล้ว", "en": "This is an inactive one" },
    "active": { "th": "ยังใช้งานได้อยู่", "en": "This is an active one" },
    "identity": { "th": "ยืนยันตัวตน", "en": "Identity Verification" },
    "pls_con_email": { "th": "กรุณากรอกรหัสที่ส่งไปยังอีเมล", "en": "Please enter the verification code sent to" },
    "pls_con_phone": { "th": "กรุณากรอกรหัสที่ส่งไปยังเบอร์โทร", "en": "Please enter the verification code sent to" },
    "dont_get_code": { "th": "คุณไม่ได้รับรหัส?", "en": "Don’t you get the verification code?" },
    "resend": { "th": "ส่งอีกครั้ง", "en": "Resend" },
    "confirm": { "th": "ยืนยัน", "en": "Confirm" },
    "change_phone_new": { "th": "เปลี่ยนเบอร์โทรศัพท์ใหม่", "en": "Change and register new mobile number" },
    "please_enter_new_phone": { "th": "กรุณากรอกหมายเลขโทรศัพท์ใหม่ของคุณ", "en": "Please enter your new mobile number" },
    "strong_password": { "th": "รหัสผ่านของคุณจะต้องมี:", "en": "Strong Password is required:" },
    "max_cha": { "th": "8 ตัวอักษรมากกว่า (สูงสุดไม่เกิน 16 ตัว)", "en": "Your password must be between 8 and 16 characters and is case-sensitive" },
    "upper_case": { "th": "ตัวอักษรพิมพ์ใหญ่และพิมพ์เล็ก", "en": "Your password must contain a minimum of one uppercase letter (A-Z) and a minimum of one lowercase letter (a-z)" },
    "numeric": { "th": "อย่างน้อยหนึ่งตัวเลข (0-9)", "en": "Your password must contain a minimum of one numeric character (0-9)" },
    "confirm_pass": { "th": "ยืนยันเปลี่ยนรหัสผ่าน", "en": "Confirm Password Change" },
    "draw_sign": { "th": "วาดลายเซ็น", "en": "Draw signature" },
    "gen_sign": { "th": "สร้างลายเซ็น", "en": "Ganerate signature" },
    "font": { "th": "ฟอนต์", "en": "Font" },
    "select_font": { "th": "เลือกฟอนต์", "en": "Select Font" },
    "draw_initi": { "th": "วาดลายเซ็นย่อ", "en": "Draw Initials" },
    "gen_initi": { "th": "สร้างลายเซ็นย่อ", "en": "Generate Initials" },
    "clear": { "th": "ล้าง", "en": "Clear" },    
    "line_noti": {"th": "การแจ้งเตือนผ่าน LINE", "en": "LINE Notification"},
    "connect": {"th": "เชื่อมต่อ", "en": "Connect"},
    "connectYourLine": {"th": "เชื่อมต่อเข้ากับ LINE ID ของคุณ", "en": "Link with your LINE ID"},
    "addLine": {"th": "เพิ่มเพื่อน", "en": "Add friend"},
    "connectLine": {"th": "เชื่อมต่อ", "en": "Connect"},
    "Notification": {"th": "การแจ้งเตือน", "en": "Notification"},
    "genQR": {"th": "สร้าง token ใหม่", "en": "Create new token"},
    "takeQR": {"th": "ถ่ายภาพ QR code ส่งเข้า", "en": "Take photo of QR code and send it via"},
    "Identity": {"th": "ยืนยันตัวตนของคุณ", "en": "Identity Verification"},
    "Verification": {"th": "ระดับการยืนยันตัวตนของคุณ", "en": "Verification Standard"},
    "Verification_Update": {"th": "อัพเดตการยืนยันตัวตน DIPCHIP", "en": "Verification Update"},
    "Level": {"th": "ระดับการยืนยันตัวตนของท่านคือ", "en": "Your level of identify"},
    "Last_update": {"th": "อัปเดตล่าสุด", "en": "Last update"},
    "save_not_suc": { "th": "บันทึกไม่สำเร็จ", "en": "Save is not successful." },
    "save_suc": { "th": "บันทึกสำเร็จ", "en": "Save successful." },
    
    "savepass_not_suc": { "th": "บันทึกรหัสผ่านไม่สำเร็จ", "en": "Save password is not successful." },
    "savepass_suc": { "th": "บันทึกรหัสผ่านสำเร็จ", "en": "Save password successful." },
    "cant_change_pass": {"th": "ไม่สามารถเปลี่ยนรหัสผ่านได้เนื่องจากคุณสมัครผ่าน social", "en": "Can't change your password because you signed up through social media."},
    "wantto_delsign": {"th": "ต้องการลบลายเซ็น", "en": "Want to delete signature"},
    "pass_less_alert": { "th": "รหัสผ่านน้อยกว่า 8 ตัวอักษร", "en": "Password less than 8 characters." },
    "pass_not_match": {"th": "รหัสผ่านไม่ตรงกัน", "en": "Password not match."},
    "old_pass_invalid": {"th": "รหัสผ่านเดิมไม่ถูกต้อง", "en": "The old password was invalid."},
}
