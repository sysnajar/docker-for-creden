
function loadSvgImg(i, img) {
    //alert('loadSvgImg = '+i)
    window.upload.init_svg(i)
}

function setActiveSvg(svg) {
    var pageIndex = svg.id.charAt(3)
    //console.table('setActiveSvg#',  pageIndex , svg)

    var app = window.upload
    app.activeSvg = svg

    if (app.draggingTool && !app.draggingTool.isShowed) {
        app.showTool(pageIndex);
        if (app.draggingTool.person.amount_least_sign > 1) {
            cloneThisField(app.draggingTool)
        }
    }

}

function cloneThisField(f) {
    console.log(">>>>>>>> clone ths field for [" + f.person.amount_least_sign + "] times")
    var app = upload
    var t = f.person.amount_least_sign - 1
    for (var i = 0; i < t; i++) {
        var f2 = clone_field(f, f.page)
        f2.id = f2.id + (i + 1)
        app.addField(f2)

        f2.isShowed = false
        app.draggingTool = f2
        app.showTool(f2.page)
        var rect = f2.group
        if (!rect.visible()) { rect.show() }
        var newX = f.group.x() + 10
        var newY = f.group.y() + 10
        rect.move(newX, newY)

        copyStyleUI(f2)
    }

    app.draggingTool = f
    app.delTool = f

}

function copyStyle(field) {
    field.styleFont = upload.selectedField[0].styleFont
    field.fontSize = upload.selectedField[0].fontSize
    console.log("copyStyle >>>>>>>>>>>>> " + field.toolName + field.styleFont + ',' + field.fontSize)
}

function copyStyleUI(field) {

    //memory
    upload.selectedField[0].styleFont = field.styleFont
    upload.selectedField[0].fontSize = field.fontSize

    //UI
    upload.size = field.fontSize
    upload.font = field.styleFont

    upload.chk_set_field = field.toolName


    if (field.toolName == 'Dropdown' && field.toolOptions) {
        upload.toolOptionsArray = field.toolOptions.split(',')
        upload.dropdownDefaultOption = field.dropdownDefaultOption
    }

    console.log("copyStyleUI >>>>>>>>>>>>> " + field.toolName + field.styleFont + ',' + field.fontSize)
}


function toEdoc(obj, imageAspectRatios) {
    var app = window.upload
    var name = getUserField('FNAME') + " " + getUserField('LNAME')
    var e = getUsername()
    var ret = {
        originalFile: obj.originalFile, pdfFile: obj.pdfFile, status: obj.status, id: obj.pdfFile.replaceAll(".pdf", ""), imageFiles: [],
        width: 0, height: 0, signers: [], fields: [], subject: obj.subject, msg: obj.msg, owner: e
        , current_order: 1, use_signing_order: obj.use_signing_order, owner_name: app.get_name(), owner_position: app.owner_position,
        attachments: obj.attachments
    }

    //fields
    var fields = []
    for (var i = 0; i < obj.fields.length; i++) {
        var f = $.extend({}, obj.fields[i])
        f.group = undefined
		var signOrder = parseInt(f.signOrder)
		f.signOrder = (isNaN(signOrder))?1:f.signOrder
        ret.fields.push(f)
    }



    //images
    for (var i = 0; i < obj.imageFiles.length; i++) {
        ret.imageFiles.push({ fileName: obj.imageFiles[i], aspectRatio: imageAspectRatios[i] })
    }


    //signers
    /*for (var i = 0; i < obj.people.length; i++) {
        ret.signers.push(obj.people[i])
    }*/

    //signers
    for (var i = 0; i < obj.people.length; i++) {
        var p = obj.people[i]

        if (p.least_sign) {
            p.concensus_id = "concen" + (new Date().getTime() + i)
            // p.concensus_idx =  "concen"+ (new Date().getTime() + i) 
            //p.concensusid = "1"
            //p.name2 = "cc"(new Date().getTime()+i)
            //p.name2 = "cc"(new Date().getTime()+i)

        }
        ret.signers.push(p)
    }

    //concensus
    //obj.concensus = (obj.people.filter(p => {return p.least_sign})).length>0?{}:undefined
    obj.concensus_list = (obj.people.filter(p => { return p.least_sign })).length > 0 ? [] : undefined
    //alert('concensus ' + obj.concensus)

    obj.people.forEach((p, i) => {
        if (p.least_sign) {
            obj.concensus_list.push({ concensus_id: p.concensus_id, at_least: p.amount_least_sign, total: p.user_group.length })
            //obj.concensus[p.concensus_id] = {concensus_id: p.concensus_id, at_least: p.amount_least_sign, total : p.user_group.length}
        }
    })

    ret.concensus_list = app.concensus_list
    ret.imageMerge = app.imageMerge
    return ret
}

function saveEdoc(doc) {
    var url = api_host + 'save_edoc_company_new'
    var url_id = doc.id
    doc.is_template = upload.is_template
    doc.document_type_id = upload.selected_template
    doc.exp_date = upload.exp_date
    doc.sign_only = upload.sign_only
    doc.name = upload.user_name
    //console.log(JSON.stringify(doc))
    //alert(JSON.stringify(doc))

    $.ajax({
        url: url,
        type: "POST",
        data: JSON.stringify(doc),
        success: function (result) {
            // gEvt(app.evtName , 'finish', 'Finished all steps')
            router.push({ path: 'dashboard' })
                       
        },
        error: function (error) {
            alert("Something went wrong!" + error);
        }
    })

}

function clone_field(oldField, pageIndex) {
    var newField = {
        "id": new Date().getTime(),
        "toolName": oldField.toolName,
        "toolData": oldField.toolData,
        "isShowed": false,
        "page": pageIndex,
        "email": oldField.selectedPerson,
        "person": oldField.person,
        "is_bold": oldField.is_bold,
        "is_underline": oldField.is_underline,
        "is_italics": oldField.is_italics,

        "styleFont": oldField.styleFont,
        "fontSize": oldField.fontSize,

        "signOrder": oldField.signOrder,
        "n": oldField.n,
        "field_no": oldField.field_no,
        "status": 0,
        "scale": oldField.scale
    }
    return newField

}

function leaveActiveSvg(svg) {
    var pageIndex = parseInt(svg.id.charAt(3))
    //console.table('leaveActiveSvg#',  pageIndex)
    var app = window.upload

    if (app.draggingTool && !app.draggingTool.isShowed) {
        console.log('reset dragging tool')
        var oldField = app.draggingTool
        oldField.group.remove() //https://svgjs.com/docs/3.0/manipulating/#remove
        app.removeField(oldField)
        var newField = clone_field(oldField, pageIndex)
        app.addField(newField)
        app.draggingTool = newField
    }

}

function moveTool(svg, evt) {
    //console.log('moveTool')    
    var f = window.upload.draggingTool
    var ele = evt.target;
    if (!f) return;
    if (!ele) return;

    var rect = f.group
    if (!rect.visible()) { rect.show() }

    var box = ele.getBoundingClientRect()

    //console.log(evt.clientX+','+evt.clientY+'clientXY')
    //console.log(box.x+','+box.y+'box.xy')
    //

    //

    var x = evt.clientX - box.x
    var y = evt.clientY - box.y
    //console.log(x+','+y)
    if (x < 20 || y < 20) {
        //console.table("++++++SKIPPED+++++++++++++++++",ele.id , evt.clientX, '-' , box.x , evt.clientY, box.y)
    }
    else {
        rect.move(x, y)
        //console.table(ele.id , evt.clientX,'-', box.x, evt.clientY, '-', box.y)
    }


}

Pages.Upload = {
    _template: 'upload.html',
    template: '<div>this is upload.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_step1,
            selected_template: "",
            zoomLevel: 1,
            activeSvg: null,
            draggingTool: null,
            delField: null, //field to be deleted
            fields: [],
            step: 1,
            list_file_pdf: [],
            subject: '',
            originalFile: '',
            pdfFile: '',
            all_original_filenames: {},
            imageFiles: [],
            display_imageFiles: [],
            build_pdf: false,
            imageMerge: '',
            all_images: {},
            templates: [],
            attachments: [],
            previewImgList: [],
            doc_id: '',
            sign_only: false,
            use_signing_order: false,
            people: [{ name: '', email: '', position_id: '', needToSign: true, eKyc: false, usePassword: false, password: '', title: '', order_set: '', substitute_sign: false, is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 }],
            is_template: false,
            upload_path: upload_path,
            totalPages: 0,
            currentPage: 0,
            selectedPersonIndex: 0,
            selectedPerson: null,
            selectedPersonName: "เลือกผู้เซ็น",
            selectedPersontitle: null,
            selectedPersonPositionId: null,
            selectedPersonObj: null,
            exp_date: new Date(),
            count_date: 120,
            min_date: new Date(),
            msg: "",
            chk_set_field: '',
            show_set_detail: false,
            selectedField: [{ font: 'THSarabunNew', size: '16', is_bold: false, is_italics: false, is_underline: false }],
            font: 'THSarabunNew',
            size: '16',
            toolOptionsArray: [],       // for dropdown
            dropdownDefaultOption: '',  // for dropdown
            group_sign: [],
            user_email: getUsername(),
            user_name: getUserField('FNAME') + " " + getUserField('LNAME') || getUsername(),
            set_oreder_user: 0,
            members: [],
            search_member_text: '',
            search_members: [],
            select_person_private_mail: 0,
            private_mail: '',
            color: [{ color: '#F2C94C' }, { color: '#1667B2' }, { color: '#BB6BD9' }, { color: '#219653' }, { color: '#F2994A' }, { color: '#EB5757' },
            { color: '#F8E2A0' }, { color: '#56CCF2' }, { color: '#D09AE5' }, { color: '#6FCF97' }, { color: '#FFC999' }, { color: '#F39B9B' }],
            history_sign: [],
            history_show: true,


        }
    },
    mounted: function () {
        // $(window).on('keyup', function (evt){console.log("keyup...."+evt.keyCode+evt.target.type)  
        //     if(evt.keyCode==8 && (evt.target.type!='text')) // 8 = del , 46 = backspace
        //       {upload.deleteField();
        //        upload.draggingTool = null
        //       }
        //  })
        window.myApp = this
        full_page()
        window.upload = this
        this.upload_file()
        this.get_group()
        this.get_members()
        this.min_date = new Date(this.min_date.setDate(this.min_date.getDate() + 1))
        this.min_date = this.min_date.getFullYear() + "-" + this.$options.filters.set_zero_text((this.min_date.getMonth() + 1)) + "-" + this.$options.filters.set_zero_text(this.min_date.getDate())
        this.set_font_stlye(0, null)

    },
    filters: {
        set_zero_text(v) {
            var txt
            var n = parseInt(v)
            if (n < 10) {
                txt = '0' + v
            } else {
                txt = v
            }
            return txt
        },

    },
    methods: {
        getPersonColor: function () {
            var colors = ['yellow', 'blue', 'pink', 'green', 'grey', 'orange']
            return colors[this.selectedPersonIndex]
        },
        zoomIn: function () {
            var newLevel = "1.0"
            if (this.zoomLevel == 0.8) { newLevel = "1.0" }
            if (this.zoomLevel == 0.5) { newLevel = "0.8" }
            this.setZoomLevel(newLevel)
            $('#zoomLevel').val(newLevel)
        },

        zoomOut: function () {
            var newLevel = "0.5"
            if (this.zoomLevel == 0.8) { newLevel = "0.5" }
            if (this.zoomLevel == 1.0) { newLevel = "0.8" }

            this.setZoomLevel(newLevel)
            $('#zoomLevel').val(newLevel)

        },

        setZoomLevel: function (level) {
            alert('not implemented'); return
            level = parseFloat(level)
            this.zoomLevel = level
            for (var i = 0; i < this.imageFiles.length; i++) {
                this.init_svg(i)
            }

        },
        get_name: function () {
            if (getUserField('FNAME') == '') {
                this.FNAME = getUserField('adminFisrtname')
            }
        },

        addField: function (field, n) {
            //console.log(' addField ')
            var arr = this.fields
            var duplicated = false



            for (var i = 0; i < this.fields.length; i++) {
                if (this.fields[i].id == field.id) {
                    duplicated = true
                    alert('duplicated ' + field.id)
                }
            }

            if (!duplicated) {
                arr.push(field);
                if (!field.email) {
                    console.log('ERROR ' + field.email + ' ' + field.toolName + ' ' + upload.selectedPerson);
                    field.email = upload.selectedPerson
                    console.log("Add field !!!! >>> " + JSON.stringify(field));
                }



                this.delField = field;
            }
            this.fields = arr

            if (field.toolName == 'Signature' || field.toolName == 'Text' ||
                field.toolName == 'Date' || field.toolName == 'Dropdown' ||
                field.toolName == 'Attachment') {
                this.show_set_detail = true
            } else {
                this.show_set_detail = false
            }
            this.chk_set_field = field.toolName
        },

        removeField: function (field) {
            console.log(' removeField ')
            var arr = []
            for (var i = 0; i < this.fields.length; i++) {
                if (this.fields[i].id != field.id) {
                    arr.push(field)
                }
            }
            this.fields = arr
        },

        deleteField: function () {
            console.log(' deleteField *** from UI')
            if (upload.fields.length < 1) {
                console.log(' skip deleteField')
                return;
            }
            var field = this.delField

            if (field && field.group) {
                field.group.remove()
            }
            if (field) {
                this.removeField(field)
            }
        },


        init_svg: function (i, img) {
            //alert('init_svg'+i)
            var svg = document.getElementById('svg' + i)
            var sBox = svg.parentElement.parentElement.getBoundingClientRect(); //BoundingBox of <div class="col-12">

            var img = document.getElementById('svgImg' + i)
            var iBox = img.getBBox()

            var zoomLevel = this.zoomLevel
            var adjustedWidth = sBox.width * zoomLevel

            var adjustedHeight = adjustedWidth * (iBox.height / iBox.width)
            //alert(adjustedWidth+' X '+adjustedHeight)

            SVG('#svgImg' + i).size(adjustedWidth, adjustedHeight)
            SVG('#svg' + i).size(adjustedWidth + 0, adjustedHeight + 2)
            svg.parentElement.style.height = adjustedHeight + 'px'

        },

        onFieldAdded: function (field, i) {
            console.log("onFieldAdded#" + i)
            var svg = SVG('#svg' + i)

            var x1 = svg.x()
            var y1 = svg.y()

            var x2 = field.group.x()
            var y2 = field.group.y()

            var xp = ((x2 - x1) * 100) / svg.width()
            var yp = ((y2 - y1) * 100) / svg.height()

            console.table(x1 + "," + y1 + "\n" + x2 + "," + y2 + "+\n" + xp + "%," + yp + "%")
            field.xp = xp
            field.yp = yp

            copyStyle(field)

            //alert('onFieldAdded')

        },

        finish: function () {
            //app.btn_status_finish = false
            var doc = toEdoc(this, this.imageAspectRatios)
            for (i = 0; i < doc.signers.length; i++) {
                var strmail = doc.signers[i].email;
                strmail = strmail.toLowerCase();
                doc.signers[i].email = strmail;
            }
            for (i = 0; i < doc.fields.length; i++) {
                var strmail = doc.fields[i].email;
                strmail = strmail.toLowerCase();
                doc.fields[i].email = strmail;

                if (doc.fields[i].toolName == "Qrcode") {
                    var url = document.location.origin
                    doc.fields[i].toolData = url + "/bla/#/document?id=" + doc.id
                }

            }
            saveEdoc(doc)
        },




        updateDrpodownDefaultOption: function () {
            this.delField.dropdownDefaultOption = this.dropdownDefaultOption
        },

        updateDrpodownOption: function (newVal, ele) {
            var index = $(ele).data('index')
            this.$set(this.toolOptionsArray, index, newVal);

            this.delField.toolOptions = this.toolOptionsArray.join()
            console.log('updateDropdownOption', this.delField.toolOptions)

        },

        addDropownOption: function (i) {
            this.toolOptionsArray.push('')
            this.delField.toolOptions = this.toolOptionsArray.join()
        },

        delDropownOption: function (n) {
            var arr = []
            for (var i = 0; i < this.toolOptionsArray.length; i++) {
                if (i != n) { arr.push(this.toolOptionsArray[i]); }
            }
            this.toolOptionsArray = arr
            this.delField.toolOptions = this.toolOptionsArray.join()

        },

        set_tool: function (toolName, N) {
            if (this.selectedPerson == null) { alert("Please select person"); return; }
            console.log('set_tool = ' + toolName)


            var n = this.fields.length + 1
            var field = {
                "id": new Date().getTime(),
                "toolName": toolName,
                "toolData": '',
                "isShowed": false,
                // "page"     : pageIndex, 
                "email": this.selectedPerson,
                "person": this.selectedPersonObj,

                "is_bold": false,
                "is_underline": false,
                "is_italics": false,
                "signOrder": parseInt(this.selectedPersonObj.order_set),
                "n": n,
                "field_no": "field" + n,
                "status": 0,
                "scale": 100
            }

            if (toolName == 'Dropdown') {
                this.toolOptionsArray = [''] //reset toolOptionsArray
                field.toolOptions = this.toolOptionsArray.join()
            }


            this.addField(field, N)
            this.draggingTool = field

        },

        showTool: function (i) {
            var field = this.draggingTool
            var color = this.getPersonColor()

            var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
            var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;

            var svg = SVG('#svg' + i)
            var group = svg.group()
            var rect = svg.rect(w, h).fill(color)
            var text = svg.text(field.toolName || "N/A")

            var cr = 10 //circle redius
            //var c4 = svg.circle(10).fill('red').move(w-cr/2, h-cr/2)


            group.add(rect)
            //group.add(c4)
            group.add(text).hide()
            field.group = group
            field.page = i



            group.click(function () {
                var app = window.upload

                if (app.draggingTool) {
                    app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                    app.draggingTool = null
                    // alert('abc')
                }
                else {
                    //  alert('def')

                    app.draggingTool = field
                    app.delField = field

                    copyStyleUI(field)

                    console.log("reset draggingTool to " + field)

                }
            })



        },
        set_font_stlye: function (a, ele) {
            var that = this
            that.selectedField[0].font = that.font
            that.selectedField[0].size = that.size
            if (a == 1) {
                that.selectedField[0].is_bold = true
            } else if (a == 2) {
                that.selectedField[0].is_italics = true
            } else if (a == 3) {
                that.selectedField[0].is_underline = true
            }

            upload.selectedField[0].styleFont = that.font
            upload.selectedField[0].fontSize = that.size

            if (upload.delField) {
                copyStyle(upload.delField)

            }



        },
        clear_font_stlye: function () {
            var that = this
            console.log('clear')
            that.selectedField[0].font = 'THSarabunNew'
            that.selectedField[0].size = '16'
            that.selectedField[0].is_bold = false
            that.selectedField[0].is_italics = false
            that.selectedField[0].is_underline = false

            this.deleteField()
        },
        next_step: function () {
            var that = this
            if (that.sign_only && that.step == 1) {
                that.step = 3
                that.people[0].email = getUsername()
                that.people[0].name = getUserField('adminFisrtname') + " " + getUserField('adminLastname') || getUsername()
                that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)

            } else if (that.step == 1) {
                that.step = 2
                that.get_history()
            } else if (that.step == 2) {
                that.step = 3
                that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)
            } else if (that.step == 3) {
                that.get_exp_date()
                that.step = 4
            } else if (that.step == 4) {
                that.get_exp_date()
            }
        },
        backward_step: function () {
            var that = this
            if (that.step == 1) {
                return false
            } else if (that.step == 2) {
                that.step = 1
            } else if (that.step == 3) {
                that.step = 2
            } else if (that.sign_only && that.step == 3) {
                that.step = 1
            } else if (that.step == 4) {
                that.step = 3
            }
        },
        upload_file: function () {
            var that = this
            var dropzone = new Dropzone(document.getElementById('file-dropzone-upload'), {
                url: api_host + 'upload_pdf',
                maxFilesize: 25, // MB
                maxFiles: 50,
                clickable: true,
                acceptedFiles: ".jpeg,.jpg,.png,.pdf"
            });

            dropzone.on("addedfile", function (file) {
                $('#loadingModal').modal('show');

            });

            var dropzoneSuccess = function (file, json) {
                var res = JSON.parse(json)
                console.log(res);
                var images = res.pdf_str.split("\n");

                //set default subject
                if (that.list_file_pdf.length == 0) {
                    that.subject = 'Please sign '
                    that.originalFile = file.name
                    that.pdfFile = res.filename
                    that.doc_id = res.filename.substring(0, res.filename.indexOf('.'))
                }

                that.all_original_filenames[res.filename] = file.name
                that.imageFiles = that.imageFiles.concat(images);
                that.list_file_pdf.push(res.filename)
                that.initThumbnails()
                that.build_pdf = true
                that.imageMerge = res.image_merge
                that.display_imageFiles.push(images[0]);
                that.all_images[res.filename] = images

                setTimeout(function () { $("#loadingModal").modal("hide") }, 500);


            }

            dropzone.on("success", dropzoneSuccess);
            dropzone.on("error", function () {
                alert('Upload Error file size must be less than 25MB')
            });


        },
        initThumbnails: function () {
            var arr = this.imageFiles;
            this.imageAspectRatios = [] //reset aspect ratio array  
            var _done_loading = 0
            var that = this

            for (var i = 0; i < arr.length; i++) {
                this.imageAspectRatios.push(0)
                var img = new Image()
                img._index = i
                img.onload = function () {
                    var I = this._index
                    that.imageAspectRatios[I] = (this.width / this.height);
                    //document.getElementById('preview'+I).src = upload_path + arr[I]

                    _done_loading++
                    if (_done_loading == arr.length) {  /*$("#filePreview").perfectScrollbar();*/


                    }
                }
                img.src = upload_path + arr[i];
            }

        },
        getDisplayFilename: function (di) {
            var that = this
            var ret = "";
            var selected_pdf = that.list_file_pdf.filter(function (_ele, i) {
                return i == di
            })[0];

            ret = this.all_original_filenames[selected_pdf]

            return ret
        },
        get_count_page_pdf: function (index) {
            var that = this
            var removed_pdf = that.list_file_pdf.filter(function (_ele, i) {
                return i == index
            })[0];

            // find images of removed PDF 
            var removeImages = that.all_images[removed_pdf]

            var list = []
            // this.previewImgIndex = 0
            for (var i = 0; i < removeImages.length; i++) {
                list.push(upload_path + removeImages[i])
            }
            return list.length || 0
        },
        showPdfImage: function (di) {
            var that = this
            var removed_pdf = that.list_file_pdf.filter(function (_ele, i) {
                return i == di
            })[0];

            // find images of removed PDF 
            var removeImages = that.all_images[removed_pdf]
            that.previewImgList = []
            for (var i = 0; i < removeImages.length; i++) {
                that.previewImgList.push(upload_path + removeImages[i])
            }
            $('#showdocument').modal('show')

        },
        remove_pdf: function (di) {
            var that = this

            var removed_pdf = that.list_file_pdf.filter(function (_ele, i) {
                return i == di
            })[0];

            // find images of removed PDF 
            var removeImages = that.all_images[removed_pdf]

            //reset imageFiles
            that.imageFiles = that.imageFiles.filter(function (img, _i) {
                var boo = true
                for (var i = 0; i < removeImages.length; i++) {
                    if (img == removeImages[i])
                        boo = false
                }
                return boo
            });


            that.list_file_pdf = that.list_file_pdf.filter(function (_ele, i) {
                return i != di
            });


            if (that.list_file_pdf.length > 0) {
                that.originalFile = that.all_original_filenames[that.list_file_pdf[0]]
            } else {
                that.originalFile = ''
            }




            that.display_imageFiles = that.display_imageFiles.filter(function (_ele, i) {
                return i != di
            });

            that.initThumbnails()
        },
        select_sign_only: function (n) {
            var that = this
            if (n == 1) {
                that.sign_only = true
            } else {
                that.sign_only = false
            }
            $('.dz-preview').hide() //ปิดpreview dropzone
            $('.dz-message').remove()
            that.createPDF()

        },
        createPDF: function () {
            var that = this

            var data = { list_file: that.imageFiles, list_file_pdf: that.list_file_pdf }


            $.ajax({
                url: api_host + "create_pdf",
                type: "POST",
                data: JSON.stringify(data),
                success: function (result) {
                    result = JSON.parse(result)
                    data.pdfFile = result.data

                    upload.pdfFile = data.pdfFile

                    that.totalPages = that.imageFiles.length
                    that.currentPage = 0;
                    that.next_step()

                    console.log("create_pdf >>>>>>>>>>> " + data.pdfFile)


                },
                error: function (error) {
                    alert("Something went wrong!" + error);

                }
            })
        },
        // step 2 -- signer
        addPerson: function () {
            var that = this
            var n = that.people.length + 1

            if (n > 12) {
                var c = { color: '#1667B2' }
                that.color.push(c)

            }

            if (that.is_template) {
                that.people.push({ name: 'SIGNER' + n, email: 'SIGNER' + n, needToSign: true, title: '', order_set: '', no: n, is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 })
            } else if (that.use_signing_order) {
                that.people.push({ name: '', email: '', needToSign: true, title: '', order_set: n, is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 })
            }
            else {
                that.people.push({ name: '', email: '', needToSign: true, title: '', order_set: '', is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 })
            }

            // this.check_btn_next();
            // setTimeout(function () { that.initsuggestionbox(); that.get_Contacts(); }, 1000)

        },
        selcet_needtosign: function (index, value) {
            var that = this
            that.people[index].needToSign = value
        },
        removePerson: function (i) {
            var that = this
            that.people.splice(i, 1);

        },
        remove_use_ekyc: function (index) {
            var that = this
            var p = that.people

            if (p[index].eKyc || p[index].usePassword) {
                var str = "คุณต้องการลบการยืนยันตัวตนหรือไม่"
                if (confirm(str)) {
                    that.people[index].eKyc = false
                    that.people[index].usePassword = false
                    that.people[index].password = ""
                    $('#check_use_ekyc' + index).collapse('toggle')
                }
                else {
                    return false
                }
            }
            else {
                $('#check_use_ekyc' + index).collapse('toggle')
            }

        },
        clear_group: function (i) {
            var that = this
            that.people[i].is_group = false
            that.people[i].user_group = []
        },
        set_number_sign_order: function () {
            var that = this
            for (i = 0; i < that.people.length; i++) {
                that.people[i].order_set = i + 1
            }
        },
        get_history: function () {
            var that = this
            var data = { owner: getUsername() }
            $.ajax({
                url: api_host + "get_history_sign",
                type: "POST",
                data: JSON.stringify(data),
                success: function (res) {
                    // console.log(res);
                    res = JSON.parse(res)
                    if (res.success) {
                        for (i = 0; i < res.data.length; i++) {
                            that.history_sign.push(res.data[i])
                        }
                        // that.history_sign = res.data[0]
                        that.search_members = that.history_sign
                    }

                },
                error: function (error) {
                    alert("Something went wrong!" + error);

                }
            })
        },
        // step 3
        setSelectedPerson: function (i, email, name, title, position_id) {
            var that = this
            var p = this.people[i]
            that.selectedPersonIndex = i
            that.selectedPerson = email
            that.selectedPersonName = name
            that.selectedPersontitle = title
            that.selectedPersonPositionId = position_id

            p.selectedPersonIndex = i
            that.selectedPersonObj = p

            console.table('setSelectedPerson' + i)

        },
        get_exp_date: function () {
            var that = this
            var date = new Date().getTime()
            if (that.count_date > 0 && that.count_date <= 120) {
                var a = that.count_date * 24
                a = date + (60 * 60 * a * 1000)
                a = new Date(a)
                var d = a.getFullYear() + "-" + that.$options.filters.set_zero_text((a.getMonth() + 1)) + "-" + that.$options.filters.set_zero_text(a.getDate())
                that.exp_date = d
            } else {
                that.count_date = 1
            }



        },
        get_count_date: function () {
            var that = this
            var date = new Date(that.exp_date)
            var now_date = new Date()
            that.count_date = date.getTime() - now_date.getTime()
            that.count_date = (that.count_date / (1000 * 60 * 60 * 24))
            that.count_date = Math.round(that.count_date)


        },
        remove_person_group: function (p, i) {
            var that = this
            that.people[p].user_group.splice(i, 1)

        },
        get_group: function () {
            var that = this
            var get_company_id = getUserField('company_id')
            var data = { company_id: get_company_id }
            $.ajax({
                type: "POST",
                url: api_host + "get_group",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    if (res.success) {
                        that.group_sign = res.data

                        // console.log(that.group_sign)
                    }

                },
                error: function (request, status, error) {
                    console.log("|" + status + ' |' + error);
                }
            });
        },
        select_group_sign: function (g) {
            var that = this
            that.get_group()
            that.history_show = false
            that.search_member_text = ''
            that.people[that.set_oreder_user].is_group = true
            that.people[that.set_oreder_user].user_group = that.group_sign[g].group_sign
            that.people[that.set_oreder_user].name = that.group_sign[g].group_name
            that.people[that.set_oreder_user].id_group = that.group_sign[g]._id
        },
        set_person: function (n) {
            var that = this
            that.search_member_text = ''
            $("input[name='selectgroup']").prop('checked', false)
            that.set_oreder_user = n
        },
        get_members: function () {
            var that = this
            var get_company_id = getUserField('company_id')
            var data = { company_id: get_company_id, mode: 'get' }
            $.ajax({
                type: "POST",
                url: api_host + "company_member",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    that.members = res.data;
                    // console.log(res)
                },
                error: function (request, status, error) {
                    console.log("|" + status + ' |' + error);
                }
            });
        },
        clear_modal_group: function () {
            var that = this
            // that.set_oreder_user = -1
            that.search_member_text = ''
            that.history_show = true
        },
        search_member: function () {
            var that = this
            var members = that.members
            var search = that.history_sign.filter(function (m) {
                var email = m.email.search(that.search_member_text) != -1
                return email
            })
            // console.log(search);
            that.search_members = search

            $("input[name='selectgroup']").prop('checked', false)
            that.people[that.set_oreder_user].is_group = false
            that.people[that.set_oreder_user].user_group = []
            that.people[that.set_oreder_user].name = ''
        },
        select_member_sign: function (n, e) {
            var that = this
            that.people[that.set_oreder_user].name = n || e
            that.people[that.set_oreder_user].email = e
        },
        cancle_member: function () {
            var that = this
            that.people[that.set_oreder_user].name = ''
            that.people[that.set_oreder_user].email = ''
            that.people[that.set_oreder_user].is_group = false
            that.people[that.set_oreder_user].user_group = []
            that.people[that.set_oreder_user].id_group = ''
        },
        set_private_mail: function () {
            var that = this
            that.people[that.select_person_private_mail].private_mail = that.private_mail
            that.private_mail = ''
        },
        replace_number: function (i) {
            var that = this
            var x = that.people[i].order_set.replace(/\D/g, '');
            if (x <= that.people.length) {
                that.people[i].order_set = x
            } else {
                that.people[i].order_set = 1
            }
            // auto run number
            // if( that.people[i].order_set < that.people.length){

            //     for(n = 0; n < that.people.length ; n++){
            //         if(n < i){
            //          var a =  (parseInt(n) + 1) + parseInt(x)
            //          alert('a --- '+ a)
            //             alert('n --- '+ n)
            //             that.people[n+2].order_set = a
            //         }


            //     }
            // }

        }, 
        remove_history:function(e){
            var that = this
            var data = {owner: getUsername(),email:e }
            $.ajax({
                type: "POST",
                url: api_host + "remove_history_sign",
                data: JSON.stringify(data),
                dataType: "json",
                success: function (res) {
                    that.history_sign = []
                    that.get_history()
                },
                error: function (request, status, error) {
                    console.log("|" + status + ' |' + error);
                }
            });
        },
        t_onover: function(id) {
            var selected = document.getElementById(id)
            var tooltip = new bootstrap.Tooltip(selected)
            tooltip.show()
        } 

    }
}

routes.push({ path: '/upload', component: Pages.Upload })

var lang_step1 = {
    //step1
    "add": { "th": "เพิ่มไฟล์", "en": "Add File" },
    "upload": { "th": "อัปโหลดไฟล์", "en": "Upload File" },
    "max_size": { "th": "ขนาดไฟล์สูงสุดไม่เกิน 25 MB", "en": "File size must be less than 25 MB" },
    "view": { "th": "ดูไฟล์", "en": "View" },
    "delete": { "th": "ลบ", "en": "Delete" },
    "self": { "th": "เซ็นคนเดียว", "en": "Self-Signing" },
    "multi": { "th": "เซ็นหลายคน", "en": "Multiple-Signing" },
    "choose": { "th": "กรุณาเลือกวิธีการเซ็น", "en": "Choosing signing type" },
    "preview": { "th": "ดูเอกสาร", "en": "File Preview" },
    "view": { "th": "ดูไฟล์", "en": "View" },
    "recip": { "th": "ผู้รับเอกสาร", "en": "Recipient" },
    "field": { "th": "เพิ่มตำแหน่งการเซ็น", "en": "Add Field" },
    "review": { "th": "ตรวจสอบ", "en": "Review" },
    "undo": { "th": "เลิกทำ", "en": "Undo" },

    // step2
    "sender": { "th": "ผู้ส่งเอกสาร", "en": "Sender" },
    "sign_order": { "th": "เซ็นตามลำดับ", "en": "Signing Order" },
    "view_order": { "th": "ดูลำดับการเซ็น", "en": "View Signing Order" },
    "signer": { "th": "ผู้เซ็น", "en": "Signer" },
    "view_only": { "th": "มีสิทธิ์ดูอย่างเดียว", "en": "View Only" },
    "delete_recip": { "th": "ลบผู้รับ", "en": "Remove a recipient" },
    "recip_name": { "th": "ชื่อผู้รับ", "en": "Recipient's Name" },
    "recip_email": { "th": "อีเมลผู้รับ", "en": "Recipient's Email" },
    "sign_set": { "th": "ตั้งค่าเงื่อนไขการเซ็น", "en": "Signing Settings" },
    "req_kyc": { "th": "ต้องยืนยันตัวตน", "en": "Required e-KYC" },
    "sign_initials": { "th": "เซ็นแบบย่อ", "en": "Signing Initials" },
    "access_set": { "th": "ตั้งค่าเงื่อนไขการเข้าถึง", "en": "Accessibility Settings" },
    "pass_doc": { "th": "ใส่รหัสเอกสาร", "en": "Password Protection" },
    "pls_en_pass": { "th": "กรุณากรอกรหัสป้องกัน", "en": "Please enter password protection" },
    "provide_pass": { "th": "คุณต้องให้รหัสนี้แก่ผู้เซ็น คุณสามารถตรวจสอบรหัสนี้ได้อีกครั้งในเอกสาร", "en": "You must provide this password to the signer. You can check and view this assigned-password in a document." },
    "add_recip": { "th": "เพิ่มผู้รับเอกสาร", "en": "Add recipient" },
    "at_least": { "th": "ต้องเซ็นอย่างน้อย", "en": "Signer at least" },
    "back": { "th": "ย้อนกลับ", "en": "Back" },
    "next": { "th": "ถัดไป", "en": "Next" },
    "search_recip": { "th": "ค้นหาผู้ติดต่อ", "en": "Search Recipients" },
    "recip_list": { "th": "รายชื่อผู้รับ", "en": "Recipients’ List" },
    "name": { "th": "ชื่อ", "en": "Name" },
    "email": { "th": "อีเมล", "en": "Email" },
    "confirm": { "th": "ยืนยันเลือก", "en": "Confirm" },
    "cancel": { "th": "ยกเลิก", "en": "Cancel" },
    "group_sign": { "th": "รายชื่อกลุ่มเซ็น", "en": "Group Sign" },
    "kyc_tip": { "th": "เลือกให้ผู้รับจำเป็นต้องทำการยืนยันตัวตนก่อนการเซ็นเอกสาร", "en": "Required recipients to verify identity before signing documents." },
    "pass_tip": { "th": "การเพิ่มรหัสผ่านสำหรับผู้รับของคุณจะเพิ่มระดับความปลอดภัยให้กับเอกสารของคุณ", "en": "Adding a password for your recipients adds an extra level of security to your documents." },

    //step3
    "select_signer": { "th": "เลือกผู้เซ็นเอกสาร", "en": "Select Signer" },
    "field_title": { "th": "ฟิลด์พื้นฐาน", "en": "Basic Field" },
    "delete_btn": { "th": "สามารถลบเครื่องมือที่ไม่ต้องการโดยการกดปุ่ม Delete ที่คีย์บอร์ด", "en": "Press “DELETE” on your keyboard to remove an unwanted tool" },
    "tool_setting": { "th": "ตั้งค่าเครื่องมือ", "en": "Tool Settings" },
    "copy_all": { "th": "คัดลอกลงทุกหน้า", "en": "Copied onto every page" },
    "des_copy": { "th": "การทำงาน: คัดลอกฟิลด์เครื่องมือนี้ลงในทุกๆหน้า โดยคัดลอกลงในตำแหน่งเดียวกัน", "en": "Function: Copying this field onto every page in the same position. " },
    "remove": { "th": "นำออก", "en": "Remove" },
    "format": { "th": "รูปแบบ", "en": "Format" },
    "file_support": { "th": "รองรับไฟล์ .pdf, .png, .jpg", "en": "File support (.png, .jpg, .pdf)" },
    "option": { "th": "ตัวเลือก", "en": "Option" },
    "select_opt": { "th": "เลือกตัวเลือก", "en": "Select an option." },
    "add_opt": { "th": "+ เพิ่มตัวเลือก", "en": "+ ADD OPTION" },
    "def_opt": { "th": "ตัวเลือกเริ่มต้น", "en": "Default Option" },

    //step4
    "exp_doc": { "th": "วันหมดอายุเอกสาร", "en": "Document Expiry Date" },
    "set_exp": { "th": "กำหนดวันหมดอายุ", "en": "Setting Expiry Date" },
    "day_from_today": { "th": "วัน นับจากวันนี้", "en": "Days from Today" },
    "subject": { "th": "หัวเรื่อง", "en": "Subject" },
    "max_text": { "th": "สูงสุด", "en": "Maximum" },
    "cha": { "th": "ตัวอักษร", "en": "character" },
    "message": { "th": "ข้อความ", "en": "Message" },
    "recip_info": { "th": "ข้อมูลผู้รับ", "en": "Recipient's Information" },
    "add_pri_mess": { "th": "เพิ่มข้อความส่วนตัว", "en": "Add Private Message" },
    "need_sign": { "th": "ต้องเซ็น", "en": "Need to Sign" },
    "view_only": { "th": "ดูอย่างเดียว", "en": "View Only" },
    "secu_code": { "th": "รหัสการเข้าถึงเอกสาร", "en": "Security Code to access document" },
    "sent": { "th": "ส่ง", "en": "Sent" },
    "to": { "th": "ถึง", "en": "To" },
}
