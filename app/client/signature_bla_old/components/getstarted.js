Pages.Getstarted = {
    _template: 'getstarted.html',
    template: '<div>this is plans.html</div>',
    data: function () {
        return {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_getstart,
        }
    },
    mounted: function () {
        // this.get_last_document()
        window.myApp = this
    },
    methods: {
        get_last_document: function(){
            var dataSet = [
                [ "Tiger Nixon", "System Architect", "Edinburgh", "5421", "2011/04/25", "$320,800" ],
                [ "Garrett Winters", "Accountant", "Tokyo", "8422", "2011/07/25", "$170,750" ],
                [ "Ashton Cox", "Junior Technical Author", "San Francisco", "1562", "2009/01/12", "$86,000" ],
                [ "Cedric Kelly", "Senior Javascript Developer", "Edinburgh", "6224", "2012/03/29", "$433,060" ],
                [ "Airi Satou", "Accountant", "Tokyo", "5407", "2008/11/28", "$162,700" ],
            ];


            $('#last_document').DataTable( {
                data: dataSet,
                paging: false,
                searching: false,
                ordering:  false,
                info: false,
                // responsive: true,
                columns: [
                    { title: "เรื่อง" },
                    { title: "ผู้ส่ง" },
                    { title: "สถานะ" },
                    { title: "วัน-เวลา" },
                ],
                createdRow: function (row, data, index) {
                    console.log($(row))
                    // $(row).addClass('font-15');
                    // $('thead tr th').addClass('md18-g1');
                    // $('td', row).eq(0).addClass('cl-f05');
                },
            } );
        },
    }
}

routes.push({ path: '/getstarted', component: Pages.Getstarted })

var lang_getstart = {
    "title": { "th": "เริ่มต้นการใช้งาน", "en": "Getting Started with Creden" },
    "howto_create": { "th": "วิธีสร้างเอกสารอิเล็กทรอนิกส์", "en": "How to Create Document" },
    "howto_sign": { "th": "วิธีการเซ็นเอกสารอิเล็กทรอนิกส์", "en": "How to Sign" },
    "howto_kyc": { "th": "วิธีการพิสูจน์และการยืนยันตัวตน", "en": "How to e-KYC online" },
    "howto_create_sig": { "th": "วิธีสร้างลายเซ็น", "en": "How to Create Signature" },
}
