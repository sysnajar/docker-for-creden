function swithLang(lang) {
    localStorage.setItem('lang_choose', lang);
    app.lang_choose = lang
}

function validateEmail (sEmail) {
    var filter = /^[\w-.+]+@[a-zA-Z0-9.-]+.[a-zA-Z]{2,4}$/;
    return filter.test(sEmail)
}

function display_alert(id, before, after){
    var er = document.getElementById(id)
    er.style.display = before

    setTimeout(function(){ 
        er.style.display = after 
    }, 5000);
}