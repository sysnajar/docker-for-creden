function init_vue() {
    return new Vue({
        el: '#app',
        data: {
            lang_choose: localStorage.getItem('lang_choose') || 'th',
            lang_txt: lang_step1,
            myProfile: "",
            myFirstname: getUserField("adminFisrtname") || "",
            myLastname: getUserField("adminLastname") || "",
            myEmail: getUserField("adminEmail") || "",
            selected_template: "",
            zoomLevel: 1,
            activeSvg: null,
            draggingTool: null,
            delField: null, //field to be deleted
            fields: [],
            step: 1,
            list_file_pdf: [],
            subject: '',
            originalFile: '',
            pdfFile: '',
            all_original_filenames: {},
            imageFiles: [],
            display_imageFiles: [],
            build_pdf: false,
            imageMerge: '',
            all_images: {},
            templates: [],
            attachments: [],
            previewImgList: [],
            doc_id: '',
            sign_only: false,
            use_signing_order: false,
            people: [{ name: '', email: '', position_id: '', needToSign: true, eKyc: false, usePassword: false, password: '', title: '', order_set: '', substitute_sign: false, is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 }],
            is_template: false,
            upload_path: upload_path,
            totalPages: 0,
            currentPage: 0,
            selectedPersonIndex: 0,
            selectedPerson: null,
            selectedPersonName: "เลือกผู้เซ็น",
            selectedPersontitle: null,
            selectedPersonPositionId: null,
            selectedPersonObj: null,
            exp_date: new Date(),
            count_date: 120,
            min_date: new Date(),
            msg: "",
            chk_set_field: '',
            show_set_detail: false,
            selectedField: [{ font: 'THSarabunNew', size: '16', is_bold: false, is_italics: false, is_underline: false }],
            font: 'THSarabunNew',
            size: '16',
            toolOptionsArray: [],       // for dropdown
            dropdownDefaultOption: '',  // for dropdown
            group_sign: [],
            user_email: getUsername(),
            user_name: getUserField('FNAME') + " " + getUserField('LNAME') || getUsername(),
            set_oreder_user: 0,
            members: [],
            search_member_text: '',
            search_members: [],
            select_person_private_mail: 0,
            private_mail: '',
            color: [{ color: '#F2C94C' }, { color: '#1667B2' }, { color: '#BB6BD9' }, { color: '#219653' }, { color: '#F2994A' }, { color: '#EB5757' },
            { color: '#F8E2A0' }, { color: '#56CCF2' }, { color: '#D09AE5' }, { color: '#6FCF97' }, { color: '#FFC999' }, { color: '#F39B9B' }],
            history_sign: [],
            history_show: true,
            draft_id: gup('id') || '',
            imageAspectRatios: [],
            alert_no_pdf: false,
            text_alert: '',
            alert_step_4: false,
        },
        mounted: function () {
            window.myApp = this
            window.upload = this
            this.getProfile()
            this.upload_file()
            this.get_group()
            this.get_members()
            this.min_date = new Date(this.min_date.setDate(this.min_date.getDate() + 1))
            this.min_date = this.min_date.getFullYear() + "-" + this.$options.filters.set_zero_text((this.min_date.getMonth() + 1)) + "-" + this.$options.filters.set_zero_text(this.min_date.getDate())
            this.set_font_stlye(0, null)
            if (this.draft_id !== '') this.getDraft()
            this.myProfile = this.getProfile()
        },
        filters: {
            set_zero_text(v) {
                var txt
                var n = parseInt(v)
                if (n < 10) {
                    txt = '0' + v
                } else {
                    txt = v
                }
                return txt
            },
        },
        methods: {
            getDraft() {
                that = this
                if (this.draft_id === '') return
                $.ajax({
                    type: "GET",
                    url: api_host + "get_draft" + "?id=" + this.draft_id,
                    dataType: "json",
                    success: function (res) {
                        if (res.data !== {}) {
                            res.data.draftJSON.lang_choose = that.$data.lang_choose
                            res.data.draftJSON.lang_txt = that.$data.lang_txt
                            res.data.draftJSON.color = that.$data.color

                            Object.assign(that.$data, res.data.draftJSON)

                            //Fixed type
                            if (that.fields.length === undefined) {
                                console.log('FIELD TYPE OBJ')
                                that.fields = []
                            }

                            if (that.history_sign.length === undefined) {
                                console.log('history_sign TYPE OBJ')
                                that.history_sign = []
                            }

                            that.setFieldsDraft()
                        }
                    },
                    error: function (request, status, error) {
                        alert("|" + status + ' |' + error);
                    }
                });
            },
            setFieldsDraft() {
                that = this
                if (that.step === 3) {
                    setTimeout(function () {
                        for (i = 0; i < that.fields.length; i++) {
                            const { x, y } = that.calsvgXY(SVG('#svg' + that.fields[i].page), that.fields[i].xp, that.fields[i].yp)
                            that.transferFieldGroup(that.fields[i], x, y)
                        }
                    }, 1000)
                }
            },
            getProfile: function () {
                var p = getUserField("profile_pic") || ""
                var f = p.search("http")
                var img = ""

                if (f == 0) img = p
                else if (f == -1 && p != "") img = "../face/images/card/" + getUserField("profile_pic") + ".png"
                else img = "images/profile.png"
                return img
            },
            getPersonColor: function () {
                var colors = ['yellow', 'blue', 'pink', 'green', 'grey', 'orange']
                return colors[this.selectedPersonIndex]
            },
            zoomIn: function () {
                var newLevel = "1.0"
                if (this.zoomLevel == 0.8) { newLevel = "1.0" }
                if (this.zoomLevel == 0.5) { newLevel = "0.8" }
                this.setZoomLevel(newLevel)
                $('#zoomLevel').val(newLevel)
            },

            zoomOut: function () {
                var newLevel = "0.5"
                if (this.zoomLevel == 0.8) { newLevel = "0.5" }
                if (this.zoomLevel == 1.0) { newLevel = "0.8" }

                this.setZoomLevel(newLevel)
                $('#zoomLevel').val(newLevel)

            },

            setZoomLevel: function (level) {
                alert('not implemented'); return
                level = parseFloat(level)
                this.zoomLevel = level
                for (var i = 0; i < this.imageFiles.length; i++) {
                    this.init_svg(i)
                }

            },
            get_name: function () {
                if (getUserField('FNAME') == '') {
                    this.FNAME = getUserField('adminFisrtname')
                }
            },

            addField: function (field, n) {
                console.log(' addField ')
                console.log(field)
                var arr = this.fields
                var duplicated = false



                for (var i = 0; i < this.fields.length; i++) {
                    if (this.fields[i].id == field.id) {
                        duplicated = true
                        alert('duplicated ' + field.id)
                    }
                }

                if (!duplicated) {
                    arr.push(field);
                    if (!field.email) {
                        console.log('ERROR ' + field.email + ' ' + field.toolName + ' ' + upload.selectedPerson);
                        field.email = upload.selectedPerson
                        //console.log("Add field !!!! >>> " + JSON.stringify(field));
                    }



                    this.delField = field;
                }
                this.fields = arr
                console.log(this.fields)

            },
            close_setting: function () {
                this.show_set_detail = false
                // alert(1)
            },
            removeField: function (field) {
                console.log(' removeField ')
                var arr = []
                for (var i = 0; i < this.fields.length; i++) {
                    if (this.fields[i].id != field.id) {
                        arr.push(field)
                    }
                }
                this.fields = arr
            },

            deleteField: function () {
                console.log(' deleteField *** from UI')
                upload.show_set_detail = false
                if (upload.fields.length < 1) {
                    console.log(' skip deleteField')
                    return;
                }
                var field = this.delField

                if (field.toolName == 'choice') {
                    console.log(this.radioGroups[this.groupRadioDel].items)
                    var noDelRadio = []
                    this.radioGroups[this.groupRadioDel].items.forEach(rect => {
                        console.log(rect.id)
                        if (rect.id != this.radioIdDel) {
                            noDelRadio.push(rect)
                        }
                    });
                    this.radioGroups[this.groupRadioDel].items = noDelRadio
                    console.log(this.radioGroups[this.groupRadioDel].items)
                    this.computeRadioGroupBoundary(this.groupRadioDel, this.groupPlusDel)
                }

                if (field && field.group) {
                    field.group.remove()
                }
                if (field) {
                    this.removeField(field)
                }
            },


            init_svg: function (i, img) {
                //alert('init_svg'+i)
                var svg = document.getElementById('svg' + i) //พี่เอิ้ก
                var sBox = svg.parentElement.parentElement.getBoundingClientRect(); //BoundingBox of <div class="col-12">
                console.log(sBox);

                var img = document.getElementById('svgImg' + i)
                var iBox = img.getBBox()

                var zoomLevel = this.zoomLevel
                var adjustedWidth = sBox.width * zoomLevel

                var adjustedHeight = adjustedWidth * (iBox.height / iBox.width)
                //alert(adjustedWidth+' X '+adjustedHeight)
                $('#doc' + i).css({ 'width': adjustedWidth, 'height': adjustedHeight })
                SVG('#svgImg' + i).size(adjustedWidth, adjustedHeight)
                SVG('#svg' + i).size(adjustedWidth + 0, adjustedHeight + 2)
                svg.parentElement.style.height = adjustedHeight + 'px'

            },

            onFieldAdded: function (field, i) {
                console.log("onFieldAdded#" + i)
                var svg = SVG('#svg' + i)
                console.log(svg)
                console.log(svg.x())
                var x1 = svg.x()
                var y1 = svg.y()

                var x2 = field.group.x()
                var y2 = field.group.y()

                var xp = ((x2 - x1) * 100) / svg.width()
                var yp = ((y2 - y1) * 100) / svg.height()

                //console.table(x1 + "," + y1 + "\n" + x2 + "," + y2 + "+\n" + xp + "%," + yp + "%")
                field.xp = xp
                field.yp = yp
                //console.table(field)
                copyStyle(field)

                //alert('onFieldAdded')

            },

            finish: function () {
                //app.btn_status_finish = false
                var doc = toEdoc(this, this.imageAspectRatios)
                for (i = 0; i < doc.signers.length; i++) {
                    var strmail = doc.signers[i].email;
                    strmail = strmail.toLowerCase();
                    doc.signers[i].email = strmail;
                }
                for (i = 0; i < doc.fields.length; i++) {
                    var strmail = doc.fields[i].email;
                    strmail = strmail.toLowerCase();
                    doc.fields[i].email = strmail;

                    if (doc.fields[i].toolName == "Qrcode") {
                        var url = document.location.origin
                        doc.fields[i].toolData = url + "/bla/#/document?id=" + doc.id
                    }

                }
                saveEdoc(doc)
            },

            calsvgXY(svg, xp, yp) {
                console.log('calsvgXY')
                console.log(svg)
                svgX = svg.x()
                svgY = svg.y()
                w = svg.width()
                h = svg.height()
                const x = ((xp * w) / 100) + svgX
                const y = ((yp * h) / 100) + svgY

                return { x, y }
            },

            saveDraft() {
                $("#draftModal").modal("show");
            },

            discradDraft() {
                document.location.href = '/bla/#/dashboard'
            },

            submitDraft() {
                console.log('saveDraft')
                that = this
                if (this.display_imageFiles.length === 0) {
                    document.location.href = '/bla/#/dashboard'
                } else {
                    if (this.draft_id === '') this.draft_id = Date.now() + '' + Math.floor(Math.random() * 10000000)
                    console.log(this.imageAspectRatios)
                    const doc = toEdoc(this, this.imageAspectRatios)
                    const url = api_host + 'save_edoc_company_draft'
                    doc.id = this.draft_id
                    doc.is_template = upload.is_template
                    doc.document_type_id = upload.selected_template
                    doc.exp_date = upload.exp_date
                    doc.sign_only = upload.sign_only
                    doc.name = upload.user_name
                    doc.draftJSON = {}
                    Object.assign(doc.draftJSON, that.$data);
                    doc.draftJSON.imageAspectRatios = this.imageAspectRatios

                    for (i = 0; i < doc.draftJSON.fields.length; i++) {
                        doc.draftJSON.fields[i].group = null
                    }

                    doc.draftJSON.activeSvg = null
                    doc.draftJSON.delField = null
                    doc.draftJSON.lang_txt = null
                    doc.draftJSON.color = null
                    doc.draftJSON.lang_choose = null

                    that.subjectDraft = that.originalFile

                    const data = JSON.stringify(doc).replaceAll("null", '""')

                    $.ajax({
                        url: url,
                        type: "POST",
                        data: data,
                        success: function (result) {
                            document.location.href = '/bla/#/draft'
                        },
                        error: function (error) {
                            console.log("Save draft error: " + error)
                        }
                    })
                }
            },
            createChoice: function (fieldPlus, x, y, group_id, gp) {
                // Set View 
                var svg = SVG('#svg' + fieldPlus.page)
                var group = svg.group()
                var color = this.getPersonColor()
                var rect = svg.rect(20, 20).fill(color)
                var text = svg.text('X' || "N/A")
                group.add(rect)
                group.add(text)
                group.move(x, y)
                group.draggable()

                // Set Field Choice
                // this.radioGroups[group_id].items.push({group:group,id:''})
                var field = clone_field(fieldPlus, fieldPlus.page)
                field["toolName"] = "choice"
                field["group"] = null
                this.addField(field)
                this.radioGroups[group_id].items.push({ group: group, id: field.id })
                // app.onFieldAdded(field["toolName"], field.page) 
                window.rdg = this.radioGroups
                //attach event
                var _id = group_id
                upload.computeRadioGroupBoundary(_id, gp)
                group.on('click.namespace', (e) => {
                    app.delField = field
                    this.delField = field
                    field["group"] = group
                    this.radioIdDel = field.id // id field radio ที่ต้องการลบ
                    this.groupRadioDel = _id // id group radio ที่ต้องการลบ
                    this.groupPlusDel = gp // ปุ่มบวกของ radio ที่ต้องการลบ
                    console.log(this.radioIdDel)
                })

                group.on('dragmove.namespace', (e) => {
                    upload.computeRadioGroupBoundary(_id, gp)    //eark 
                    app.delField = field
                    this.delField = field
                    field["group"] = group
                    this.radioIdDel = field.id
                    this.groupRadioDel = _id
                    this.groupPlusDel = gp
                    console.log(this.radioIdDel)
                })


                //end

            },

            computeRadioGroupBoundary: function (groupId, gp) {
                var g = this.radioGroups[groupId]
                //var svg = null 
                var x1 = 9999
                var y1 = 9999

                var x2 = 0
                var y2 = 0
                console.log(g.items)
                g.items.forEach(rect => {
                    if (rect.group.x() < x1) { x1 = rect.group.x() }

                    if (rect.group.y() < y1) { y1 = rect.group.y() }


                    if (rect.group.x() > x2) { x2 = rect.group.x() }

                    if (rect.group.y() > y2) { y2 = rect.group.y() }

                });
                console.log(x1, y1, x2, y2)

                var color = 'green'
                var svg = g.svg
                var rect_bg = g.rect_bg || svg.rect().fill(color)
                g.rect_bg = rect_bg

                rect_bg.size(x2 - x1 + 20, y2 - y1 + 20)
                // rect_bg.opacity(0.1)
                rect_bg.move(x1, y1)
                // rect_bg.back()
                rect_bg.attr({
                    fill: '#f06'
                    , 'fill-opacity': 0.1
                    , 'stroke': '#5184AF'
                    , 'stroke-dasharray': "5 10"
                })

                console.log('ggg>>')
                console.log(g)
                g.items.forEach(rect => {
                    radio = rect.group
                    radio.front()
                });
                gp.move((x2 + x1) / 2, y2 + 30)
            },
            showRadio: function (i) {
                // create Radio
                var field = this.draggingTool
                var color = this.getPersonColor()
                var svg = SVG('#svg' + i)
                var group = svg.group()
                var rect = svg.rect(20, 20).fill(color)
                var text = svg.text('+')
                group.add(rect)
                //group.add(c4)
                group.add(text).hide()
                field.group = group
                field.page = i
                //set id Radio Groups 
                var rGroup = {
                    id: new Date().getTime() + "",
                    items: [], svg: svg, rect_bg: null
                }
                this.radioGroups[rGroup.id] = rGroup

                group.on('click.namespace', (e) => {
                    var app = window.myApp

                    if (app.draggingTool) { // First Drop From Panel
                        app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                        app.draggingTool = null
                    }
                    else { // Click event after drop from panel
                        var rectX = e.target.getBBox().x
                        var rectY = e.target.getBBox().y
                        this.createChoice(field, rectX, rectY, rGroup.id, group)
                    }
                })
            },
            showTool: function (i) {
                console.log(i)
                var field = this.draggingTool
                var color = this.getPersonColor()

                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;

                if (field.toolName == 'Radio') {
                    w = 10
                    h = 10
                }

                var svg = SVG('#svg' + i)
                var group = svg.group()
                var rect = svg.rect(w, h).fill(color)
                var text = svg.text(field.toolName || "N/A")

                var cr = 10 //circle redius
                //var c4 = svg.circle(10).fill('red').move(w-cr/2, h-cr/2)


                group.add(rect)
                //group.add(c4)
                group.add(text).hide()
                group.draggable()
                field.group = group
                field.page = i

                group.on('dragend.namespace', (evt) => {
                    var app = window.myApp
                    app.draggingTool = field // eark
                    app.onFieldAdded(app.draggingTool, i)
                    app.draggingTool = null
                    console.log(field)
                })

                group.on('dragmove.namespace', (e) => {
                    const { handler, box } = e.detail
                    const constrains = document.getElementById('svg' + i)
                    max_y = constrains.attributes.height.value
                    max_x = constrains.attributes.width.value
                    // e.preventDefault()
                    let { x, y } = box

                    if (x < 0) {
                        e.preventDefault()
                        x = 0
                    }
                    if (box.x2 > max_x) {
                        //if (box.x2+70 > max_x) {    
                        e.preventDefault()
                        x = max_x - box.w - 5
                    }
                    if (y < 0) {
                        e.preventDefault()
                        if (i == 0) {
                            y = 0
                        } else {
                            console.log('can previous up showTool')
                            // alert('can previous up showTool')
                            var newField = clone_field(field, i - 1)
                            const constrains_prev = document.getElementById('svg' + i)
                            this.transferFieldGroup(newField, x, constrains.attributes.height.value - box.h - 5)
                            group.remove()
                            return;
                        }
                    }
                    if (box.y2 > max_y) {
                        e.preventDefault()
                        next_page = (parseInt(i) + 1)
                        have_next = document.getElementById('svg' + next_page)
                        if (have_next == null) {
                            y = max_y - box.h - 5
                        } else {
                            console.log('can next page showTool')
                            // alert('can next page showTool')                          
                            var newField = clone_field(field, next_page)
                            this.transferFieldGroup(newField, x, 0)
                            group.remove()
                            return;
                        }
                    }
                    handler.move(x, y)
                    // app.draggingTool = field
                    app.delField = field
                    copyStyleUI(field)
                })
            },
            transferFieldGroup: function (field_clone, x1, y1) {
                console.log(JSON.stringify(field_clone))
                console.log(x1, y1)
                if (this.tmp_id_transfer == JSON.stringify(field_clone)) {
                    return;
                } else {
                    tmp_id_transfer = JSON.stringify(field_clone)
                }

                // var field = {"id":1614091192078,"toolName":"Signature","toolData":"","isShowed":false,"page":1,"person":{"name":"กฤษณ์ ชินวุฒินันท์","email":"kitchinvuttinunt@gmail.com","position_id":"","needToSign":true,"eKyc":false,"usePassword":false,"password":"","title":"","order_set":"","substitute_sign":false,"is_group":false,"user_group":[],"least_sign":false,"amount_least_sign":1,"selectedPersonIndex":0},"is_bold":false,"is_underline":false,"is_italics":false,"styleFont":"THSarabunNew","fontSize":"16","signOrder":null,"n":1,"field_no":"field1","status":0,"scale":100}
                var field = field_clone
                var color = this.getPersonColor()

                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;
                var svg = SVG('#svg' + field.page)
                console.log(svg)
                var group = svg.group()
                var rect = svg.rect(w, h).fill(color)
                var text = svg.text(field.toolName || "N/A")
                group.add(rect)
                group.add(text)
                group.move(x1, y1)
                group.draggable()
                field.group = group
                // var app = window.myApp
                // this.addField(field, field)
                // app.onFieldAdded(field, field.page)
                // app.delField = field
                // app.draggingTool = field
                // copyStyleUI(field) 

                group.on('dragend.namespace', (evt) => {
                    console.log('drag end')
                    var app = window.myApp
                    console.log(field)
                    app.onFieldAdded(field, field.page)
                    // app.draggingTool = null
                    app.delField = field
                    // app.draggingTool = field
                    // app.delField = field
                    // copyStyleUI(field) 
                })

                group.on('dragmove.namespace', (e) => {
                    const { handler, box } = e.detail
                    const constrains = document.getElementById('svg' + field.page)
                    max_y = constrains.attributes.height.value
                    max_x = constrains.attributes.width.value
                    // e.preventDefault()
                    let { x, y } = box

                    if (x <= 0) {
                        e.preventDefault()
                        handler.move(0, y)
                        app.delField = field
                        copyStyleUI(field)
                        return;
                    }
                    if (box.x2 > max_x) {
                        e.preventDefault()
                        handler.move(max_x - box.w - 5, y)
                        app.delField = field
                        copyStyleUI(field)
                        return;
                    }
                    if (y <= 0) {
                        e.preventDefault()
                        if (field.page == 0) {
                            e.preventDefault()
                            handler.move(x, 0)
                            app.delField = field
                            copyStyleUI(field)
                        } else {
                            console.log('can previous up tranfer')
                            // alert('can previous up tranfer')
                            var newField = clone_field(field, field.page - 1)
                            const constrains_prev = document.getElementById('svg' + field.page)
                            this.transferFieldGroup(newField, x, constrains.attributes.height.value - box.h - 5)
                            group.remove()
                            return;
                        }
                    }
                    if (box.y2 > max_y) {
                        e.preventDefault()
                        next_page = (parseInt(field.page) + 1)
                        have_next = document.getElementById('svg' + next_page)
                        if (have_next == null) {
                            y = max_y - box.h - 5
                        } else {
                            console.log('can next page tranfer')
                            // alert('can next page tranfer')                           
                            var newField = clone_field(field, next_page)
                            // this.delField()
                            group.find('rect').fill('red')
                            //group.remove()
                            this.removeField(field)
                            console.log('next_page ===' + next_page)
                            newField.page = next_page
                            newField.xp = 20
                            newField.yp = 0
                            // app.onFieldAdded(newField, field.page)
                            this.transferFieldGroup(newField, x, 0)
                            this.addField(newField, next_page)
                            // handler.move(max_x - box.w - 5 , y )                          
                            // app.onFieldAdded(newField, field.page)
                            app.delField = newField
                            copyStyleUI(field)
                            return;

                        }
                    }


                    // handler.move(x , y ) 
                })
            },


            updateDrpodownDefaultOption: function () {
                this.delField.dropdownDefaultOption = this.dropdownDefaultOption
            },

            updateDrpodownOption: function (newVal, ele) {
                var index = $(ele).data('index')
                this.$set(this.toolOptionsArray, index, newVal);

                this.delField.toolOptions = this.toolOptionsArray.join()
                console.log('updateDropdownOption', this.delField.toolOptions)

            },

            addDropownOption: function (i) {
                this.toolOptionsArray.push('')
                this.delField.toolOptions = this.toolOptionsArray.join()
            },

            delDropownOption: function (n) {
                var arr = []
                for (var i = 0; i < this.toolOptionsArray.length; i++) {
                    if (i != n) { arr.push(this.toolOptionsArray[i]); }
                }
                this.toolOptionsArray = arr
                this.delField.toolOptions = this.toolOptionsArray.join()

            },

            set_tool: function (toolName, N) {
                if (this.selectedPerson == null) { alert("Please select person"); return; }
                console.log('set_tool = ' + toolName)


                var n = this.fields.length + 1
                var field = {
                    "id": new Date().getTime(),
                    "toolName": toolName,
                    "toolData": '',
                    "isShowed": false,
                    // "page"     : pageIndex, 
                    "email": this.selectedPerson,
                    "person": this.selectedPersonObj,

                    "is_bold": false,
                    "is_underline": false,
                    "is_italics": false,
                    "signOrder": parseInt(this.selectedPersonObj.order_set),
                    "n": n,
                    "field_no": "field" + n,
                    "status": 0,
                    "scale": 100
                }

                if (toolName == 'Dropdown') {
                    this.toolOptionsArray = [''] //reset toolOptionsArray
                    field.toolOptions = this.toolOptionsArray.join()
                }


                this.addField(field, N)
                this.draggingTool = field

            },

            _showTool: function (i) {
                var field = this.draggingTool
                var color = this.getPersonColor()

                var w = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 150;
                var h = (field.toolName == 'Signature' || field.toolName == 'Initial') ? 70 : 50;

                var svg = SVG('#svg' + i)
                var group = svg.group()
                var rect = svg.rect(w, h).fill(color)
                var text = svg.text(field.toolName || "N/A")

                var cr = 10 //circle redius
                //var c4 = svg.circle(10).fill('red').move(w-cr/2, h-cr/2)


                group.add(rect)
                //group.add(c4)
                group.add(text).hide()
                field.group = group
                field.page = i



                group.click(function () {
                    var app = window.myApp

                    if (app.draggingTool) {
                        app.onFieldAdded(app.draggingTool, app.draggingTool.page)
                        app.draggingTool = null
                    }
                    else {
                        //  alert('def')

                        app.draggingTool = field
                        app.delField = field

                        copyStyleUI(field)

                        console.log("reset draggingTool to " + field)

                    }
                })



            },
            set_font_stlye: function (a, ele) {
                var that = this
                that.selectedField[0].font = that.font || 'THSarabunNew'
                that.selectedField[0].size = that.size || '16'
                if (a == 1) {
                    that.selectedField[0].is_bold = true
                } else if (a == 2) {
                    that.selectedField[0].is_italics = true
                } else if (a == 3) {
                    that.selectedField[0].is_underline = true
                }

                upload.selectedField[0].styleFont = that.font || 'THSarabunNew'
                upload.selectedField[0].fontSize = that.size || '16'

                if (upload.delField) {
                    copyStyle(upload.delField)

                }



            },
            clear_font_stlye: function () {
                var that = this
                console.log('clear')
                that.selectedField[0].font = 'THSarabunNew'
                that.selectedField[0].size = '16'
                that.selectedField[0].is_bold = false
                that.selectedField[0].is_italics = false
                that.selectedField[0].is_underline = false

                this.deleteField()
            },
            next_step: function () {
                var that = this
                if (that.sign_only && that.step == 1) {
                    that.step = 3
                    that.people[0].email = getUsername()
                    that.people[0].name = getUserField('adminFisrtname') + " " + getUserField('adminLastname') || getUsername()
                    that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)
                } else if (that.step == 1) {
                    that.step = 2
                    that.get_history()
                } else if (that.step == 2) {
                    that.step = 3
                    that.setSelectedPerson(0, that.people[0].email, that.people[0].name, that.people[0].title, that.people[0].position_id)
                    that.setFieldsDraft()
                } else if (that.step == 3) {
                    that.get_exp_date()
                    that.step = 4
                } else if (that.step == 4) {
                    that.get_exp_date()
                }
            },
            backward_step: function () {
                var that = this
                if (that.step == 1) {
                    return false
                } else if (that.step == 2) {
                    that.step = 1
                } else if (that.step == 3) {
                    that.get_history()
                    that.step = 2
                } else if (that.sign_only && that.step == 3) {
                    that.step = 1
                } else if (that.step == 4) {
                    that.step = 3
                    that.setFieldsDraft()
                }
            },
            upload_file: function () {
                var that = this
                var dropzone = new Dropzone(document.getElementById('file-dropzone-upload'), {
                    url: api_host + 'upload_pdf',
                    maxFilesize: 25, // MB
                    maxFiles: 50,
                    clickable: true,
                    acceptedFiles: ".jpeg,.jpg,.png,.pdf"
                });

                dropzone.on("addedfile", function (file) {
                    $('#loadingModal').modal('show');

                });

                var dropzoneSuccess = function (file, json) {
                    var res = JSON.parse(json)
                    console.log(res);
                    var images = res.pdf_str.split("\n");

                    //set default subject
                    if (that.list_file_pdf.length == 0) {
                        that.subject = 'Please sign '
                        that.originalFile = file.name
                        that.pdfFile = res.filename
                        that.doc_id = res.filename.substring(0, res.filename.indexOf('.'))
                    }

                    that.all_original_filenames[res.filename] = file.name
                    that.imageFiles = that.imageFiles.concat(images);
                    that.list_file_pdf.push(res.filename)
                    that.initThumbnails()
                    that.build_pdf = true
                    that.imageMerge = res.image_merge
                    that.display_imageFiles.push(images[0]);
                    that.all_images[res.filename] = images

                    setTimeout(function () { $("#loadingModal").modal("hide") }, 500);


                }

                dropzone.on("success", dropzoneSuccess);
                dropzone.on("error", function () {
                    that.alert_no_pdf = true
                    that.text_alert = that.lang_txt.max_size[that.lang_choose]
                    setTimeout(function () { that.alert_no_pdf = false }, 3000);
                    // alert('Upload Error file size must be less than 25MB')
                });


            },
            initThumbnails: function () {
                var arr = this.imageFiles;
                this.imageAspectRatios = [] //reset aspect ratio array  
                var _done_loading = 0
                var that = this

                for (var i = 0; i < arr.length; i++) {
                    this.imageAspectRatios.push(0)
                    var img = new Image()
                    img._index = i
                    img.onload = function () {
                        var I = this._index
                        that.imageAspectRatios[I] = (this.width / this.height);
                        //document.getElementById('preview'+I).src = upload_path + arr[I]

                        _done_loading++
                        if (_done_loading == arr.length) {  /*$("#filePreview").perfectScrollbar();*/


                        }
                    }
                    img.src = upload_path + arr[i];
                }

            },
            getDisplayFilename: function (di) {
                var that = this
                var ret = "";
                var selected_pdf = that.list_file_pdf.filter(function (_ele, i) {
                    return i == di
                })[0];

                ret = this.all_original_filenames[selected_pdf]

                return ret
            },
            get_count_page_pdf: function (index) {
                var that = this
                var removed_pdf = that.list_file_pdf.filter(function (_ele, i) {
                    return i == index
                })[0];

                // find images of removed PDF 
                var removeImages = that.all_images[removed_pdf]

                var list = []
                // this.previewImgIndex = 0
                for (var i = 0; i < removeImages.length; i++) {
                    list.push(upload_path + removeImages[i])
                }
                return list.length || 0
            },
            showPdfImage: function (di) {
                var that = this
                var removed_pdf = that.list_file_pdf.filter(function (_ele, i) {
                    return i == di
                })[0];

                // find images of removed PDF 
                var removeImages = that.all_images[removed_pdf]
                that.previewImgList = []
                for (var i = 0; i < removeImages.length; i++) {
                    that.previewImgList.push(upload_path + removeImages[i])
                }
                $('#showdocument').modal('show')

            },
            remove_pdf: function (di) {
                var that = this

                var removed_pdf = that.list_file_pdf.filter(function (_ele, i) {
                    return i == di
                })[0];

                // find images of removed PDF 
                var removeImages = that.all_images[removed_pdf]

                //reset imageFiles
                that.imageFiles = that.imageFiles.filter(function (img, _i) {
                    var boo = true
                    for (var i = 0; i < removeImages.length; i++) {
                        if (img == removeImages[i])
                            boo = false
                    }
                    return boo
                });


                that.list_file_pdf = that.list_file_pdf.filter(function (_ele, i) {
                    return i != di
                });


                if (that.list_file_pdf.length > 0) {
                    that.originalFile = that.all_original_filenames[that.list_file_pdf[0]]
                } else {
                    that.originalFile = ''
                }




                that.display_imageFiles = that.display_imageFiles.filter(function (_ele, i) {
                    return i != di
                });

                that.initThumbnails()
            },
            select_sign_only: function (n) {
                var that = this
                if (n == 1) {
                    that.sign_only = true
                } else {
                    that.sign_only = false
                }
                $('.dz-preview').hide() //ปิดpreview dropzone
                $('.dz-message').remove()
                that.createPDF()

            },
            createPDF: function () {
                var that = this

                var data = { list_file: that.imageFiles, list_file_pdf: that.list_file_pdf }


                $.ajax({
                    url: api_host + "create_pdf",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (result) {
                        result = JSON.parse(result)
                        data.pdfFile = result.data

                        upload.pdfFile = data.pdfFile

                        that.totalPages = that.imageFiles.length
                        that.currentPage = 0;
                        that.next_step()

                        console.log("create_pdf >>>>>>>>>>> " + data.pdfFile)


                    },
                    error: function (error) {
                        that.alert_no_pdf = true
                        that.text_alert = that.lang_txt.Please_upload[that.lang_choose]
                        setTimeout(function () { that.alert_no_pdf = false }, 3000);
                        console.log("Something went wrong!" + error);


                    }
                })
            },
            // step 2 -- signer
            addPerson: function () {
                var that = this
                var n = that.people.length + 1

                if (n > 12) {
                    var c = { color: '#1667B2' }
                    that.color.push(c)

                }

                if (that.is_template) {
                    that.people.push({ name: 'SIGNER' + n, email: 'SIGNER' + n, needToSign: true, title: '', order_set: '', no: n, is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 })
                } else if (that.use_signing_order) {
                    that.people.push({ name: '', email: '', needToSign: true, title: '', order_set: n, is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 })
                }
                else {
                    that.people.push({ name: '', email: '', needToSign: true, title: '', order_set: '', is_group: false, user_group: [], least_sign: false, amount_least_sign: 1 })
                }

                // this.check_btn_next();
                // setTimeout(function () { that.initsuggestionbox(); that.get_Contacts(); }, 1000)

            },
            selcet_needtosign: function (index, value) {
                var that = this
                that.people[index].needToSign = value
            },
            removePerson: function (i) {
                var that = this
                that.people.splice(i, 1);

            },
            remove_use_ekyc: function (index) {
                var that = this
                var p = that.people

                if (p[index].eKyc || p[index].usePassword) {
                    var str = "คุณต้องการลบการยืนยันตัวตนหรือไม่"
                    if (confirm(str)) {
                        that.people[index].eKyc = false
                        that.people[index].usePassword = false
                        that.people[index].password = ""
                        $('#check_use_ekyc' + index).collapse('toggle')
                    }
                    else {
                        return false
                    }
                }
                else {
                    $('#check_use_ekyc' + index).collapse('toggle')
                }

            },
            clear_group: function (i) {
                var that = this
                that.people[i].is_group = false
                that.people[i].user_group = []
            },
            set_number_sign_order: function () {
                var that = this
                for (i = 0; i < that.people.length; i++) {
                    that.people[i].order_set = i + 1
                }
            },
            get_history: function () {
                var that = this
                var data = { owner: getUsername() }
                $.ajax({
                    url: api_host + "get_history_sign",
                    type: "POST",
                    data: JSON.stringify(data),
                    success: function (res) {
                        // console.log(res);
                        res = JSON.parse(res)
                        that.history_sign = []
                        if (res.success) {
                            for (i = 0; i < res.data.length; i++) {
                                that.history_sign.push(res.data[i])
                            }
                            // that.history_sign = res.data[0]
                            that.search_members = that.history_sign
                        }

                    },
                    error: function (error) {
                        alert("Something went wrong!" + error);

                    }
                })
            },
            // step 3
            setSelectedPerson: function (i, email, name, title, position_id) {
                var that = this
                var p = this.people[i]
                that.selectedPersonIndex = i
                that.selectedPerson = email
                that.selectedPersonName = name
                that.selectedPersontitle = title
                that.selectedPersonPositionId = position_id

                p.selectedPersonIndex = i
                that.selectedPersonObj = p

                console.table('setSelectedPerson' + i)

            },
            get_exp_date: function () {
                var that = this
                var date = new Date().getTime()
                if (that.count_date > 0 && that.count_date <= 120) {
                    var a = that.count_date * 24
                    a = date + (60 * 60 * a * 1000)
                    a = new Date(a)
                    var d = a.getFullYear() + "-" + that.$options.filters.set_zero_text((a.getMonth() + 1)) + "-" + that.$options.filters.set_zero_text(a.getDate())
                    that.exp_date = d
                } else {
                    that.count_date = 1
                }



            },
            get_count_date: function () {
                var that = this
                var date = new Date(that.exp_date)
                var now_date = new Date()
                that.count_date = date.getTime() - now_date.getTime()
                that.count_date = (that.count_date / (1000 * 60 * 60 * 24))
                that.count_date = Math.round(that.count_date)


            },
            remove_person_group: function (p, i) {
                var that = this
                that.people[p].user_group.splice(i, 1)

            },
            get_group: function () {
                var that = this
                var get_company_id = getUserField('company_id')
                var data = { company_id: get_company_id }
                $.ajax({
                    type: "POST",
                    url: api_host + "get_group_sign",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        if (res.success) {
                            that.group_sign = res.data

                            // console.log(that.group_sign)
                        }

                    },
                    error: function (request, status, error) {
                        console.log("|" + status + ' |' + error);
                    }
                });
            },
            select_group_sign: function (g) {
                var that = this
                that.get_group()
                that.history_show = false
                that.search_member_text = ''
                that.people[that.set_oreder_user].is_group = true
                that.people[that.set_oreder_user].user_group = that.group_sign[g].group_sign
                that.people[that.set_oreder_user].name = that.group_sign[g].group_name
                that.people[that.set_oreder_user].id_group = that.group_sign[g]._id
            },
            set_person: function (n) {
                var that = this
                that.search_member_text = ''
                $("input[name='selectgroup']").prop('checked', false)
                that.set_oreder_user = n
                that.history_show = true
            },
            get_members: function () {
                var that = this
                var get_company_id = getUserField('company_id')
                var data = { company_id: get_company_id, mode: 'get' }
                $.ajax({
                    type: "POST",
                    url: api_host + "company_member",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        that.members = res.data;
                        // console.log(res)
                    },
                    error: function (request, status, error) {
                        console.log("|" + status + ' |' + error);
                    }
                });
            },
            clear_modal_group: function () {
                var that = this
                // that.set_oreder_user = -1
                that.search_member_text = ''
                that.history_show = true
            },
            search_member: function () {
                var that = this
                var members = that.members
                var search = that.history_sign.filter(function (m) {
                    var email = m.email.search(that.search_member_text) != -1
                    return email
                })
                // console.log(search);
                that.search_members = search

                $("input[name='selectgroup']").prop('checked', false)
                that.people[that.set_oreder_user].is_group = false
                that.people[that.set_oreder_user].user_group = []
                that.people[that.set_oreder_user].name = ''
            },
            select_member_sign: function (n, e) {
                var that = this
                that.people[that.set_oreder_user].name = n || e
                that.people[that.set_oreder_user].email = e
            },
            cancle_member: function () {
                var that = this
                that.people[that.set_oreder_user].name = ''
                that.people[that.set_oreder_user].email = ''
                that.people[that.set_oreder_user].is_group = false
                that.people[that.set_oreder_user].user_group = []
                that.people[that.set_oreder_user].id_group = ''
            },
            set_private_mail: function () {
                var that = this
                that.people[that.select_person_private_mail].private_mail = that.private_mail
                that.private_mail = ''
            },
            replace_number: function (i) {
                var that = this
                var x = that.people[i].order_set.replace(/\D/g, '');
                if (x <= that.people.length) {
                    that.people[i].order_set = x
                } else {
                    that.people[i].order_set = 1
                }
                // auto run number
                // if( that.people[i].order_set < that.people.length){

                //     for(n = 0; n < that.people.length ; n++){
                //         if(n < i){
                //          var a =  (parseInt(n) + 1) + parseInt(x)
                //          alert('a --- '+ a)
                //             alert('n --- '+ n)
                //             that.people[n+2].order_set = a
                //         }


                //     }
                // }

            },
            remove_history: function (e) {
                var that = this
                var data = { owner: getUsername(), email: e }
                $.ajax({
                    type: "POST",
                    url: api_host + "remove_history_sign",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        that.history_sign = []
                        that.get_history()
                    },
                    error: function (request, status, error) {
                        console.log("|" + status + ' |' + error);
                    }
                });
            },
            t_onover: function (id) {
                var selected = document.getElementById(id)
                var tooltip = new bootstrap.Tooltip(selected)
                tooltip.show()
            },
            logout: function () {

                localStorage.removeItem('compCode')
                localStorage.removeItem('username')
                localStorage.removeItem("register_obj")
                // localStorage.removeItem("listNotification_json")

                $.ajax({
                    type: "POST",
                    url: api_host + "logout",
                    data: JSON.stringify(''),
                    dataType: "json",
                    success: function (data) {
                        document.location.href = '/bla/web/login.html?rd=' + new Date().getTime()
                    },
                    error: function (request, status, error) {
                        console.log("Logout Error |" + status + ' |' + error);
                        document.location.href = 'index.html'
                    }
                });

            },


        } //end methods
    })
}

$(document).ready(function () {
    app = init_vue()
});
function swithLang(lang) {
    localStorage.setItem('lang_choose', lang);
    window.myApp.lang_choose = lang
    // reloadVuePage()    
}