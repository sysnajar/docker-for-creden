cd /home/creden/work/app/cert
echo "1.Gen PrivateKey"
openssl genrsa -passout pass:$3 -out $1.key 2048

sleep 1
echo "2.Gen CSR"
openssl req -new -key $1.key -out $1.csr -subj "/C=TH/ST=TH/L=TH/O=Creden/OU=IT/CN=$2"


sleep 1
echo "3.Gen User CERT"
openssl x509 -req -in $1.csr -CA myCA.pem -CAkey myCA.key -CAcreateserial -out $1.crt -days 365 -sha256 -passin pass:1527


sleep 1
echo "4.Export CERT"
openssl pkcs12 -export -in $1.crt -inkey $1.key -out $1.pfx -certfile myCA.pem -passout pass:$3
chmod 755 $1.pfx


sleep 1
echo "5.gen public key"
openssl rsa -pubout -in $1.key > $1.pub


sleep 1
echo "6.gen key info"
openssl x509 -in $1.crt -text -noout > $1.txt

val=$(cat $1.pub);redis-cli set "pub.$1" "$val"
val=$(cat $1.txt);redis-cli set "txt.$1" "$val"

