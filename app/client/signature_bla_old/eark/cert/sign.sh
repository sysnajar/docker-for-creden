rm -rf /home/creden/work/app/cert/jsignpdf-1.6.4/$3_signed.pdf

cd /home/creden/work/app/cert/jsignpdf-1.6.4
java -jar JSignPdf.jar -kst PKCS12 -ksf /home/creden/work/app/cert/$1.pfx -ksp $2 --append /home/creden/work/app/client/face/images/card/$3.pdf

cp /home/creden/work/app/cert/jsignpdf-1.6.4/$3_signed.pdf /home/creden/work/app/client/face/images/card/$3.pdf

chmod 777 /home/creden/work/app/client/face/images/card/$3.pdf 
#cp /home/creden/work/app/client/face/images/card/$3_signed.pdf /home/creden/work/app/client/face/images/card/$3.pdf
