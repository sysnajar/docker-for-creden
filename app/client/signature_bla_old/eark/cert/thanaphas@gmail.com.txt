Certificate:
    Data:
        Version: 1 (0x0)
        Serial Number:
            8f:19:59:80:1a:20:5e:7d
        Signature Algorithm: sha256WithRSAEncryption
        Issuer: C = TH, ST = Bangkok, L = Bangkok, O = Creden Asia co ltd, OU = IT, CN = root, emailAddress = info@creden.co
        Validity
            Not Before: Jun  7 06:56:25 2021 GMT
            Not After : Jun  7 06:56:25 2022 GMT
        Subject: C = TH, ST = TH, L = TH, O = Creden, OU = IT, CN = thanaphas@gmail.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                RSA Public-Key: (2048 bit)
                Modulus:
                    00:da:77:02:6c:59:2a:09:b0:db:77:1f:a4:9d:c6:
                    67:e7:b2:1c:39:75:e0:0c:bb:45:35:88:19:6c:d8:
                    32:f0:52:57:6f:a8:69:f2:18:8a:69:81:a9:60:aa:
                    44:0e:b3:4c:07:68:7b:71:b6:54:5e:ed:68:d5:bd:
                    e6:f3:90:1d:f7:d0:29:33:eb:5c:32:a0:a8:22:b7:
                    9e:69:3f:25:0c:20:1a:9c:e8:d2:4e:53:40:d1:f6:
                    a4:e1:fe:f7:18:99:eb:87:53:78:40:ad:b1:3d:a0:
                    d7:42:da:6f:3f:f8:b5:65:9e:5b:db:81:be:05:66:
                    c2:77:5f:17:d6:e3:19:a6:0a:64:a0:f8:8b:08:14:
                    b0:fb:aa:b8:c9:63:43:32:69:9f:4c:28:98:61:ba:
                    59:ad:75:a1:0f:c7:66:ee:9d:03:ff:fd:51:1a:ad:
                    5a:d2:4a:7b:a6:e1:c3:81:76:2f:5d:bc:d4:c8:78:
                    48:b6:a2:e9:bd:e4:1d:26:b2:1f:d8:f1:45:4b:d8:
                    7a:12:6e:0b:e7:2d:e3:24:3d:af:8a:95:00:1e:24:
                    21:c4:5a:53:b3:b5:e5:ef:c1:a5:41:51:4a:f2:0e:
                    a9:00:e4:5d:df:23:31:e7:65:7a:a0:55:88:dc:c2:
                    ac:08:ef:a8:00:06:79:80:9c:8d:80:bd:9e:58:c9:
                    e4:01
                Exponent: 65537 (0x10001)
    Signature Algorithm: sha256WithRSAEncryption
         5c:80:37:67:69:b3:48:e8:e7:d6:5d:43:04:c0:df:80:06:5d:
         2d:e8:6e:42:ef:45:7c:44:34:30:30:63:be:ae:63:b4:c7:ea:
         fc:ea:9d:c0:9c:10:3a:cf:a2:0c:f8:3a:c5:4f:83:0f:a0:c6:
         e6:d6:76:51:d1:81:f9:d6:c4:c4:b2:5d:f1:54:b1:66:90:0d:
         d5:90:ee:2b:05:54:cc:bd:ca:d3:e7:e7:68:2b:c3:7c:3c:e6:
         b0:18:9a:18:ff:f9:c4:08:30:53:6a:fd:12:8d:0f:b1:2a:e2:
         82:ad:2a:a6:a6:8e:4d:c5:c8:86:d3:fd:48:b5:b7:20:4a:90:
         7c:b0:b1:3f:d4:34:b0:a2:a1:9f:22:bc:9d:e9:63:75:c4:8c:
         b9:c6:9e:4c:db:d2:15:dd:25:e2:7a:fa:c9:dc:9f:d3:a9:fc:
         ab:e1:a9:0b:29:db:09:0a:fc:73:75:3b:27:02:e3:e0:79:ca:
         80:e5:ee:95:1d:b8:d9:53:60:da:f7:18:07:ee:36:64:7c:b0:
         8c:b7:50:1a:f2:4b:ac:1b:8d:06:66:99:11:24:a8:33:0a:48:
         82:57:5e:23:61:4c:ae:2a:30:d2:1c:aa:e8:ef:8e:2a:ce:ce:
         b2:df:6b:f2:d7:27:d6:34:32:3b:c9:bc:2b:f6:3a:1f:8c:03:
         c2:ff:29:4c
