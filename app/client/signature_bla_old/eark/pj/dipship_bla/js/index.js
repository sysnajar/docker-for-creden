new Vue({
    el: '#app',
    data() {
        return {
            id: "",
            title: "",
            first: "",
            last: "",
            phone: "",
            email: "",
		template_id :"1575879621",
         // template_id : "1566475127",
			// template_id :"1562140571",
			// template_id:"1560226475",
			//template_id:"1560143043",
			           // template_id:"1558683440",
             //template_id:"1558681868",
		//template_id:"1549973612",
			getfname : localStorage.getItem('firstname'),
			getlname : localStorage.getItem('lastname'),
        }
    },
    mounted: function () {
		
		this.id = gup('txid');
        this.get_ekyc_by_txid();

		var card_reader_id = gup('card_reader_id')
		if(card_reader_id)
		  {
		    this.email = this.id+'@demo.creden.co'
           this.first = decodeURI(gup('firstname'))
		   this.last = decodeURI(gup('lastname'))

			if(this.first=='undefined')
			 {
			    this.first = ''; this.last = '';
			 }
		  }
		this.sendForm();
		console.log(decodeURI(gup('firstname')))
//		this.first = decodeURI(gup('firstname'))
//		this.last = decodeURI(gup('lastname'))
	},
    methods: {

        get_ekyc_by_txid: function () {
            var that = this
            $.ajax({
                type: "POST",
                url: document.location.origin + "/capi/get_ekyc_by_txid",
                data: JSON.stringify({ "txid": that.id }),
                dataType: "json",
                success: function (res) {
                    if (res.success) {
						//alert(JSON.stringify(res.data))
                        that.first = res.data.fnameTH
                        that.last = res.data.lnameTH
                    } else {
                        console.log('fail get_ekyc_by_txid')
                    }
                },
                error: function (request, status, error) {
                    console.log('HTTP ERROR get_ekyc_by_txid :'+error)
                }
            })
        },
        sendForm: function () {
            var that = this;
			var form_valid = true

            if (form_valid) {

				var redirect_url = 'http://dbd.go.th/index.php'
		        var card_reader_id = gup('card_reader_id') 
				if(card_reader_id)
				  {
					if(card_reader_id.indexOf('ghb')==0)
				       redirect_url = 'https://hjkl.ninja/ok.html?txid='+that.id+'&card_reader_id_tx='+card_reader_id+"&status=1"
				    else
					   redirect_url = 'https://hjkl.ninja/demo_menu.html?txid='+that.id+'&card_reader_id_tx='+card_reader_id+"&status=1"
				  }
                 
                var data = {txid: that.id, template_id : that.template_id, signers : [{no : 1, email : this.email , name : this.first +' '+this.last}], default_field_values : {field2 : this.first + '  ' + this.last, field3 : '11            06            2562',field5:this.first+'  '+this.last,field6:'11             06             2562'}, send_email : true, redirect_url : redirect_url}


			  //data.default_field_values.field2 = "XXXXX YYYY ZZZZ"	
			  //alert('set default of name = '+ data.default_field_values.field2)

                $.ajax({
                    type: "POST",
                    headers: { 'apiKey': 'ctFp9DVnvlI0x7piQEZhJ2jkKvCRVRrq' },
                    url: document.location.origin + "/capi/create_document",
                    data: JSON.stringify(data),
                    dataType: "json",
                    success: function (res) {
                        if (res.success) {
                            //alert(res.data.signers[0].signer_link)
                            document.location.href = res.signers[0].signer_link+"&gb=true" ;
                        } else {
                            alert('Cannot save form')
                            //console.log('Cannot save form')
                        }
                    },
                    error: function (request, status, error) {
                            alert('Http Error')
                            //console.log('Http Error')
                    }
                })
            }
        },
    }
})

function gup(name, url) {
    if (!url) url = location.href;
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regexS = "[\\?&]" + name + "=([^&#]*)";
    var regex = new RegExp(regexS);
    var results = regex.exec(url);
    return results == null ? null : results[1];
}
