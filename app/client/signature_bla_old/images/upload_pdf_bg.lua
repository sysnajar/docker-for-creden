 
local cjson = require "cjson"
local exec = require'resty.exec'
local prog = exec.new('/tmp/exec.sock')
local prog1 = exec.new('/tmp/exec.sock')
local prog1b = exec.new('/tmp/exec.sock')

prog.timeout_fatal = false
prog1.timeout = 1000 * 60 * 30
prog1.timeout_fatal = false
prog1b.timeout_fatal = false 

 --page2
local config = {dpi = 300}
local page_convert_size = 30 

redis = require 'redis'
rc = redis.connect('127.0.0.1', 6379)


function change_order(c)
local t = str_split(c,'\n')
local t2 = {}
for i,v in ipairs(t) do
if(i>1)then
  table.insert(t2,v)
end
end
table.insert(t2,t[1])
t = t2
t[#t] = string.gsub(t[#t],".png", "-".. (#t-1) .. ".png")

 
local res = table.concat(t,"\n")
return res
end

function str_split(str, sep)
if sep == nil then
sep = '%s'
end

local res = {}
local func = function(w)
table.insert(res, w)
end

string.gsub(str, '[^'..sep..']+', func)
return res
end



local function basename(path)
  return path:sub(path:find("/[^/]*$") + 1)
end
 
 function compute_upload_status(key , start , total)
    local pct = ((start * 100) / total)
    rc:set(key , tostring(pct))
    ngx.log(ngx.INFO, 'upload_status >>>>>', key , ' = ', rc:get(key) or 'XXX' , 'YYYY')
 end
 
 


function get_page_count(file)

    ngx.log(ngx.NOTICE,"get_page_count  ", ', UPLOAD_PHOTO ' ,  UPLOAD_PHOTO)

    if(UPLOAD_PHOTO)then return 1 end
    
    
    local res, err = prog1('pdftk', file ,  'dump_data'  )
    prog1.stdin = res.stdout
    res,err = prog1('grep', 'NumberOfPages')
    prog1.stdin = nil
    
    if(not err)then
    return tonumber(res.stdout:match('%d+'))
    else
    return 0
    end
    
end

 --end page2


-->>> name, file_name , upload_key
ngx.req.read_body()
print = ngx.say
body = ngx.req.get_body_data()
ngx.log(ngx.NOTICE,body)
body = cjson.decode(body)

name = body.name
file_name  = body.file_name
upload_key = body.upload_key
local file_name2 = '/home/creden/work/app/client/face/images/card/pdf' .. name
local file_name3 = '/home/creden/work/app/client/face/images/card/pdf' .. name .. '*.png'
UPLOAD_PHOTO = body.UPLOAD_PHOTO


 
local cmd1,size1,err1

ngx.log(ngx.NOTICE,"PDF PAGE_COUNT  ", ', UPLOAD_PHOTO ' ,  UPLOAD_PHOTO)
local page_count = get_page_count(file_name)
local last_page_index = page_count-1
ngx.log(ngx.NOTICE,"PDF PAGE_COUNT = " .. page_count, ', UPLOAD_PHOTO ' ,  UPLOAD_PHOTO)
local start_page,end_page = 0,(page_count<page_convert_size) and last_page_index or page_convert_size-1
local stop_now = false
local fraction = false




while(not stop_now) do

if(upload_key) then
compute_upload_status(upload_key , start_page+1, page_count)
end
cmd1 = 'convert -resize 1000 -density '.. config.dpi ..' ' .. file_name .. '['..start_page..'-'..end_page..'] ' .. file_name2 ; ngx.log(ngx.NOTICE,"CMD1 = " .. cmd1)
    size2, err1 = prog1('pdftoppm','-scale-to', '1920' ,'-r', config.dpi, '-png', '-f', start_page+1, '-l', end_page+1, file_name , file_name2)
   if (page_count == 1) then
		     prog_res, err1 = prog1('mv', file_name2 .. '-1.png', file_name2 .. '.png')
   end
   ngx.log(ngx.NOTICE,"FINISHED CMD1")

 

   if page_count > 1 and start_page == end_page then
    fraction = true
  end

   --exit loop if we did last page
   if(end_page == last_page_index) then
       ngx.log(ngx.NOTICE, 'stopped conversion at ' .. end_page )
       stop_now = true
	   break
   else
       ngx.log(ngx.NOTICE, end_page..'~='..last_page_index)
   end

   -- start_page,end_page for next loop
   start_page = end_page+1
   end_page = start_page+page_convert_size-1
   if(end_page> last_page_index)then end_page = last_page_index end
end

local cmd2 = 'find '.. file_name3  ..' -printf "%f\n" | sort -V | /home/creden/scripts/rset.lua pdf'

rc:del('pdf')
if(UPLOAD_PHOTO )then
   local size, err = prog('sh', '/home/creden/work/app/server/capi/check_size_pic.sh', file_name)
   --if tonumber(size.stdout) < 600 then
   if tonumber(size.stdout) < 600 then
      ngx.log(ngx.NOTICE,'1111111111111111', "img too small", tostring(size), tostring(err))
      ngx.exit(500)
      --ngx.say (cjson.encode({success = false, msg = 'img row size'}))
      --return
   end
 
   local filename  = basename(file_name)
   local dir = '/home/creden/work/app/client/face/images/card/'
   os.execute('cp ' .. dir .. filename .. ' '.. dir .. filename ..'.png')
   pdf_str=filename .. '.png'

   cmd_img_pdf = 'convert '..file_name..' '..file_name
   os.execute(cmd_img_pdf)
end

 
os.execute(cmd2)
ngx.log(ngx.NOTICE,"FINISHED CMD2")
if(not UPLOAD_PHOTO)then
   pdf_str = rc:get('pdf')
end

--if fraction == true then
if false  then
  local list_page = str_split(pdf_str, "\n")
  local first_page = list_page[1]
  local end_page =  "pdf" .. name .. "-" .. #list_page - 1 .. ".png"
  local cmd_change_page,err = prog('sh', '/home/creden/work/app/server/capi/change_page.sh', first_page, end_page)
  pdf_str = change_order(pdf_str)
end

local all_img = ''
local image_merge = ''
local page_num = 0
--merge image
for i in string.gmatch(pdf_str, "[^\n]+") do
    page_num = page_num + 1
    all_img = all_img..'/home/creden/work/app/client/face/images/card/'..i..' '
end

ngx.log(ngx.NOTICE,"START CMD3")
if (false and page_num > 1) then
    image_merge = 'pdf'..name..'.png'
    local cmd_merge = 'convert '..all_img..' -append /home/creden/work/app/client/face/images/card/'..image_merge
    ngx.log(ngx.NOTICE,cmd_merge)
    os.execute(cmd_merge)
else
    image_merge = pdf_str
end
ngx.log(ngx.NOTICE,"FINISHED CMD4")
--ngx.log(ngx.NOTICE,'SPEED CDECK ',' ', name   ,' ','END',' ',  session.data.username )
ngx.log(ngx.NOTICE,'---------------------------------- END [UPLOAD PDF] ---------------------------------- SPEED')



-- send output
local key =  upload_key .. '_result'
local json =  cjson.encode({success = true, filename = name .. '.pdf' , pdf_str = pdf_str, image_merge = image_merge, page_num= page_num, key = key})


rc:set(key, json) --to redis
ngx.say(json)    -- to browser
