#!/bin/bash

npm install nightmare
npm install stellar-sdk@0.10.3
npm install nodemailer --save
npm install redis --save
#npm install puppeteer@5.2.1
#npm install -g peerjs
npm update

ln -s /home/creden/work/app/server/capi /home/creden/scripts


cp tmp/cpdf /usr/bin/
#cp tmp/pdftk /usr/bin/


#wkhtml
cp tmp/wkhtmltox/bin/wkhtmltopdf /usr/bin/

#pdftk
#sh /tmp/install_pdftk.sh



apt-get install qrencode -y
chmod 777 /usr/bin/cpdf
chmod 777 /usr/bin/pdftk
chmod 777 /usr/bin/wkhtmltopdf
apt-get install zbar-tools -y
